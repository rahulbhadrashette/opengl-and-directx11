#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#include<string.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

//OpenGL Related Files
#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>  //In Windows(Wgl) 

#include"vmath.h"
#include"Sphere.h"

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTCOORD0
};

//namespaces
using namespace std;
using namespace vmath;

// global variable Declaration
static GLXContext gLXContext;
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

int winWidth = giWindowWidth;
int winHeight = giWindowHeight;

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;

FILE *gpFile;

bool gbLight;
bool vertexShader = false;
bool fragmentShader = false;

GLfloat angleRotate = 0.0f;
GLfloat Co1 = 0.0f;
GLfloat Co2 = 0.0f;

vmath::mat4 modelMatrix;
vmath::mat4 viewMatrix;
vmath::mat4 projectionMatrix;

GLuint gShaderProgramObject_PV;
GLuint gShaderProgramObject_PF;

GLuint modelUniform;
GLuint viewUniform;
GLuint projectionUniform;

GLuint LaUniform_Red;
GLuint LdUniform_Red;
GLuint LMAUniform;
GLuint LMLVUinform;
GLuint lightPositionUniform_Red;

GLuint KaUniform;
GLuint KdUniform;
GLuint KsUniform;

GLuint materialShininessUniform;
GLuint lKeyIsPressedUniform;

GLuint modelUniform_RMB;
GLuint viewUniform_RMB;
GLuint projectionUniform_RMB;

GLuint LaUniform_Red_RMB;
GLuint LdUniform_Red_RMB;
GLuint LsUniform_Red_RMB;
GLuint LMAUniform_RMB;
GLuint LMLVUinform_RMB;
GLuint lightPositionUniform_Red_RMB;

GLuint KaUniform_RMB;
GLuint KdUniform_RMB;
GLuint KsUniform_RMB;

GLuint materialShininessUniform_RMB;
GLuint lKeyIsPressedUniform_RMB;


struct Light
{
	GLfloat Ambient[4];
	GLfloat Diffuse[4];
	GLfloat Position[4];
	GLfloat lightModeAmbient[4];
	GLfloat lightModelLocalViewer[1];
};

struct  Light light[1];

struct Material
{
	GLfloat Ambient[4];
	GLfloat Diffuse[4];
	GLfloat Specular[4];
	GLfloat Shininess[1];
};
struct Material material[24];

GLuint vao_Sphere;
GLuint vbo_Position_Sphere;
GLuint vbo_Normal_Sphere;
GLuint vbo_Element_Sphere;

//For Sphere
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

unsigned int gNumVertices;
unsigned int gNumElements;

int iKeyPressed;

//Entry-Point Function
int main(void)
{
	//Function Declaration
	void Resize_RMB(int, int);
	void Initialize_RMB(void);
	void Display_RMB(void);
	void CreateWindow_RMB(void);
	void ToggleFullScreen_RMB(void);
	void uninitialize_RMB(void);
	void Update_RMB(void);

	gpFile = fopen("OpenGL_Log.txt", "w");
	if(gpFile == NULL)
	{
		printf("Error in Opening Log File...!\nExitting Now....!!!");
		return(EXIT_FAILURE);
	}

	// Local Variable Declaration
	bool bDone = false;
	char keys[26];

	//Code
	CreateWindow_RMB();
	Initialize_RMB();

	XEvent event;
	KeySym keysym;

	fprintf(gpFile, "Entering into Game Loop...!!!\n");
	//Message Loop
	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					 //All System Fonts  or  fixed pitch font
				break;

				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							if(bFullScreen == false)
							{
								ToggleFullScreen_RMB();
								bFullScreen = true;
							}
							else
							{
								ToggleFullScreen_RMB();
								bFullScreen = false;
							}
							break;
					}

					XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
					switch(keys[0])
					{
						case 'E':
						case 'e':
						case 'Q':
						case 'q':
								 bDone = true;
							break;

					
						case 'F':
						case 'f':
								fragmentShader = true;
								vertexShader = false;
							break;

						case 'V':
						case 'v':
								vertexShader = true;
								fragmentShader = false;
							break;

						case 'L':
						case 'l':
								gbLight = !gbLight;
							break;

						case 'X':
						case 'x':
							iKeyPressed = 2;
							break;

						case 'Y':
						case 'y':
							iKeyPressed = 3;
							break;

						case 'Z':
						case 'z':
							iKeyPressed = 1;
							break;
					
					}
					break;

				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:   	 //Left Mouse Button
							break;
						case 2:  	 //Middle Mouse Button
							break;
						case 3:  	 //Right Mouse Button
							break;
						case 4:  	 //Wheel Up
							break;
						case 5:		 //Wheel Down
							break;
						default:
							break;
					}
					break;

				case MotionNotify:	 //WM_MOUSEMOVE:
					break;

				case ConfigureNotify://WM_SIZE:
					  winWidth = event.xconfigure.width;
					  winHeight = event.xconfigure.height;
					  Resize_RMB(winWidth, winHeight);
					break;

				case Expose:		//WM_PAINT:
					break;

				case DestroyNotify: //WM_DESTROY:
					break;

				case 33:
				     bDone = true;
				    break;

				default:
				     break;
			}
		}
		//Display Call Here And Update also
		Update_RMB();
		Display_RMB();
	}

        uninitialize_RMB();
	return(0);
}

void CreateWindow_RMB(void)
{
	//Function prototype declaration
	void uninitialize_RMB(void);

	//variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	GLXFBConfig *pGLXFBConfig = NULL;   //This is Array of FBConfig
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumberOfFBConfig = 0;

	static int frameBufferAttributes[] = { 
						  	GLX_X_RENDERABLE, True,
						  	GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
						  	GLX_RENDER_TYPE, GLX_RGBA_BIT,
						  	GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
							GLX_RED_SIZE, 8,
					       	GLX_GREEN_SIZE, 8,
					      	GLX_BLUE_SIZE, 8,
					      	GLX_ALPHA_SIZE, 8,
					      	GLX_DEPTH_SIZE, 24,
					      	GLX_STENCIL_SIZE, 8,
					      	GLX_DOUBLEBUFFER, True,
					      	None
					    	};

	//Code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	//defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	pGLXFBConfig = glXChooseFBConfig(gpDisplay,  defaultScreen, frameBufferAttributes, &iNumberOfFBConfig);
	printf("%d There are Matching FBConfig.\n", iNumberOfFBConfig);
	if(pGLXFBConfig == NULL)
	{
		printf("ERROR: Unable To pGLXFBConfig. \nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}
	//Inline variable
	int bestFBConfig = -1;
	int bestNumberOfSamples = -1;
	int worstFBConfig = -1;
	int worstNumberOfSamples = 999;
	for(int i = 0; i<iNumberOfFBConfig; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfig[i]);

		if(pTempXVisualInfo)
		{
			int sampleBuffers;
			int samples;
			//Get number of sampleBuffers from respective FBConfig[i]
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);

			//Get number of samples from respective FBConfig[i]
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLES, &samples);

			if(bestFBConfig < 0 | sampleBuffers && samples > bestNumberOfSamples)
			{
				bestFBConfig = i;
				bestNumberOfSamples = samples;
			}

			if(worstFBConfig < 0 | !sampleBuffers | samples < worstNumberOfSamples)
			{
				worstFBConfig = i;
				worstNumberOfSamples = samples;
			}
		}
		XFree(pTempXVisualInfo);
	}

	bestGLXFBConfig = pGLXFBConfig[bestFBConfig];

	gGLXFBConfig = bestGLXFBConfig;
	XFree(pGLXFBConfig) ;

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, bestGLXFBConfig);

	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);

	if(!gWindow)
	{
		printf("ERROE : Failed To Create Main Window.\nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "16-perVertex_And_PerFragmentLighting..RahulUMB...!!!");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);
}

void Initialize_RMB(void)
{
	GLenum result;
	void perVertex(void);
	void perFragment(void);
	void Resize_RMB(int, int);
	void uninitialize_RMB(void);

	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

	if(glXCreateContextAttribsARB == NULL)
	{
		printf("ERROE : Failed To glXCreateContextAttribsARB.\nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	const int Attribs[] = {
							GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
							GLX_CONTEXT_MINOR_VERSION_ARB, 5,
							GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
							None
							};

	gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, Attribs);
	if(!gGLXContext)
	{
		const int Attribs[] = {
								GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
								GLX_CONTEXT_MINOR_VERSION_ARB, 0,
								None
								};
	}

	gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, Attribs);

	if(!glXIsDirect(gpDisplay, gGLXContext))
	{
		printf("Optined Context is Not H/W Rendering Context....!\n");
	}
	else
	{
		printf("Optined Context is H/W Rendering Context....!\n");
	}


	//void uninitialize(void);
	//gLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, True);
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

	result = glewInit();
	if (result != GLEW_OK)
	{
		printf("glewInit(); is failed.\n");
		uninitialize_RMB();
		exit(1);
	}

	//Function Call per Fragment 
	perVertex();
	perFragment();
	
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	//Position
	glGenVertexArrays(1, &vao_Sphere);
	glBindVertexArray(vao_Sphere);
	glGenBuffers(1, &vbo_Position_Sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Normal
	glGenBuffers(1, &vbo_Normal_Sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Normal_Sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Element vbo
	glGenBuffers(1, &vbo_Element_Sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	
	
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	modelMatrix = vmath::mat4::identity();
	viewMatrix = vmath::mat4::identity();
	projectionMatrix = vmath::mat4::identity();
	//Warmup Call To Resize
	Resize_RMB(giWindowWidth, giWindowHeight);
}

void Resize_RMB(int Width, int Height)
{
	//code
	if (Height == 0)
	{
		Height = 1;
	}
	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);

	projectionMatrix = vmath::perspective(45.0f, (GLfloat)Width/(GLfloat)Height , 0.1f, 100.0f);
}

void Display_RMB(void)
{
	//code
	void Light_Array(void);
	void materialsFragmentShader(void);

	Light_Array();
	materialsFragmentShader();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Light_Array();

	for(int i = 0; i < 24; i++)
	{
		glViewport((i % 6) * winWidth / 6, winHeight - (i / 6 + 1) * winHeight / 4, (GLsizei)winWidth / 6, (GLsizei)winHeight / 4);

		projectionMatrix = vmath::perspective(45.0f, (GLfloat)(winWidth / 6) / (GLfloat)(winHeight / 4), 0.1f, 100.0f);


		if(fragmentShader == true)
		{
			glUseProgram(gShaderProgramObject_PF);

			if (gbLight == true)
			{
				glUniform1i(lKeyIsPressedUniform_RMB, 1);
				glUniform3fv(LaUniform_Red_RMB, 1, light[0].Ambient);
				glUniform3fv(LdUniform_Red_RMB, 1, light[0].Diffuse);
				glUniform3fv(LMAUniform, 1, light[0].lightModeAmbient);
				glUniform1fv(LMLVUinform, 1, light[0].lightModelLocalViewer);
				glUniform4fv(lightPositionUniform_Red_RMB, 1, light[0].Position);

				glUniform3fv(KaUniform_RMB, 1, material[i].Ambient);
				glUniform3fv(KdUniform_RMB, 1, material[i].Diffuse);
				glUniform3fv(KsUniform_RMB, 1, material[i].Specular);
				glUniform1fv(materialShininessUniform_RMB, 1, material[i].Shininess);
			}
			else
			{
				glUniform1i(lKeyIsPressedUniform, 0);
			}
		}
		else
		{
			glUseProgram(gShaderProgramObject_PV);

			if (gbLight == true)
			{
				glUniform1i(lKeyIsPressedUniform, 1);
				glUniform3fv(LaUniform_Red, 1, light[0].Ambient);
				glUniform3fv(LdUniform_Red, 1, light[0].Diffuse);
				glUniform3fv(LMAUniform, 1, light[0].lightModeAmbient);
				glUniform1fv(LMLVUinform, 1, light[0].lightModelLocalViewer);
				glUniform4fv(lightPositionUniform_Red, 1, light[0].Position);

				glUniform3fv(KaUniform, 1, material[i].Ambient);
				glUniform3fv(KdUniform, 1, material[i].Diffuse);
				glUniform3fv(KsUniform, 1, material[i].Specular);
				glUniform1fv(materialShininessUniform, 1, material[i].Shininess);
			}
			else
			{
				glUniform1i(lKeyIsPressedUniform, 0);
			}
		}
		
	//Declatation of Matricex
	vmath::mat4 translationMatrix;
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelMatrix = vmath::mat4::identity();
	viewMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.0f, 0.0f,-2.3f);

	//Do Necessary Matrix Multiplication
	modelMatrix = translationMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, projectionMatrix);

	glUniformMatrix4fv(modelUniform_RMB, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewUniform_RMB, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionUniform_RMB, 1, GL_FALSE, projectionMatrix);

	//BindWith vao
	glBindVertexArray(vao_Sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
	//Draw the Necessary Scnes
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//UnBind vao
	glBindVertexArray(0);
		//UnUse Program
		glUseProgram(0);
	}

	glXSwapBuffers(gpDisplay, gWindow);
}

void Update_RMB(void)
{
	angleRotate = angleRotate + 0.0005f;
	if (angleRotate > 360.0f)
	{
		angleRotate = 0.0f;
	}
	
	Co1 = (GLfloat)cos(angleRotate) * 100.0f;
	Co2 = (GLfloat)sin(angleRotate) * 100.0f;
}

void Light_Array(void)
{
	//Array Zero
	light[0].Ambient[0] = 0.0f;
	light[0].Ambient[1] = 0.0f;
	light[0].Ambient[2] = 0.0f;
	light[0].Ambient[3] = 1.0f;

	light[0].Diffuse[0] = 1.0f;
	light[0].Diffuse[1] = 1.0f;
	light[0].Diffuse[2] = 1.0f;
	light[0].Diffuse[3] = 1.0f;

	if (iKeyPressed == 2)
	{
		light[0].Position[0] = Co1;
		light[0].Position[1] = 0.0f;
		light[0].Position[2] = Co2;
		light[0].Position[3] = 1.0f;
	}
	else if (iKeyPressed == 1)
	{
		light[0].Position[0] = Co2;;
		light[0].Position[1] = Co1;
		light[0].Position[2] = 0.0f;
		light[0].Position[3] = 1.0f;
	}
	else
	{
		light[0].Position[0] = 0.0f;
		light[0].Position[1] = Co1;
		light[0].Position[2] = Co2;
		light[0].Position[3] = 1.0f;
	}

	light[0].lightModeAmbient[0] = 0.2f;
	light[0].lightModeAmbient[1] = 0.1f;
	light[0].lightModeAmbient[2] = 0.2f;
	light[0].lightModeAmbient[3] = 1.0f;

	light[0].lightModelLocalViewer[0] = 0.0f;
}

void materialsFragmentShader(void)
{
	// *** 1st Sphere On 1st Column, Emerald ****
	material[0].Ambient[0] = 0.0215f;
	material[0].Ambient[1] = 0.1745f;
	material[0].Ambient[2] = 0.0215f;
	material[0].Ambient[3] = 1.0f;

	material[0].Diffuse[0] = 0.07568f;
	material[0].Diffuse[1] = 0.61424f;
	material[0].Diffuse[2] = 0.07568f;
	material[0].Diffuse[3] = 1.0f;

	material[0].Specular[0] = 0.633f;
	material[0].Specular[1] = 0.727811f;
	material[0].Specular[2] = 0.633f;
	material[0].Specular[3] = 1.0f;

	material[0].Shininess[0] = 0.6f * 128.0f;
	
	// *** 2nd Sphere On 1st Column, Jade ****
	material[1].Ambient[0] = 0.135f;
	material[1].Ambient[1] = 0.2225f;
	material[1].Ambient[2] = 0.1575f;
	material[1].Ambient[3] = 1.0f;

	material[1].Diffuse[0] = 0.54f;
	material[1].Diffuse[1] = 0.89f;
	material[1].Diffuse[2] = 0.63f;
	material[1].Diffuse[3] = 1.0f;

	material[1].Specular[0] = 0.316228f;
	material[1].Specular[1] = 0.316228f;
	material[1].Specular[2] = 0.316228f;
	material[1].Specular[3] = 1.0f;

	material[1].Shininess[0] = 0.1f * 128.0f;

	// *** 3rd Sphere On 1st Column, Obsidian ****
	material[2].Ambient[0] = 0.05375f;
	material[2].Ambient[1] = 0.05f;
	material[2].Ambient[2] = 0.06625f;
	material[2].Ambient[3] = 1.0f;

	material[2].Diffuse[0] = 0.18275f;
	material[2].Diffuse[1] = 0.17f;
	material[2].Diffuse[2] = 0.22525f;
	material[2].Diffuse[3] = 1.0f;

	material[2].Specular[0] = 0.332741f;
	material[2].Specular[1] = 0.328634f;
	material[2].Specular[2] = 0.346435f;
	material[2].Specular[3] = 1.0f;

	material[2].Shininess[0] = 0.3f * 128.0f;

	// *** 4th Sphere On 1st Column, Pearl ****
	material[3].Ambient[0] = 0.25f;
	material[3].Ambient[1] = 0.20725f;
	material[3].Ambient[2] = 0.20725f;
	material[3].Ambient[3] = 1.0f;

	material[3].Diffuse[0] = 1.0f;
	material[3].Diffuse[1] = 0.829f;
	material[3].Diffuse[2] = 0.829f;
	material[3].Diffuse[3] = 1.0f;

	material[3].Specular[0] = 0.296648f;
	material[3].Specular[1] = 0.296648f;
	material[3].Specular[2] = 0.296648f;
	material[3].Specular[3] = 1.0f;

	material[3].Shininess[0] = 0.088f * 128.0f;

	// *** 5th Sphere On 1st Column, Ruby ****
	material[4].Ambient[0] = 0.1745f;
	material[4].Ambient[1] = 0.01175f;
	material[4].Ambient[2] = 0.01175f;
	material[4].Ambient[3] = 1.0f;

	material[4].Diffuse[0] = 0.61424f;
	material[4].Diffuse[1] = 0.04136f;
	material[4].Diffuse[2] = 0.04136f;
	material[4].Diffuse[3] = 1.0f;

	material[4].Specular[0] = 0.727811f;
	material[4].Specular[1] = 0.686959f;
	material[4].Specular[2] = 0.626959f;
	material[4].Specular[3] = 1.0f;

	material[4].Shininess[0] = 0.6f * 128.0f;

	// *** 6th Sphere On 1st Column, Tarquoise ****
	material[5].Ambient[0] = 0.1f;
	material[5].Ambient[1] = 0.18725f;
	material[5].Ambient[2] = 0.1745f;
	material[5].Ambient[3] = 1.0f;

	material[5].Diffuse[0] = 0.396f;
	material[5].Diffuse[1] = 0.74151f;
	material[5].Diffuse[2] = 0.69102f;
	material[5].Diffuse[3] = 1.0f;

	material[5].Specular[0] = 0.297254f;
	material[5].Specular[1] = 0.30829f;
	material[5].Specular[2] = 0.306678f;
	material[5].Specular[3] = 1.0f;

	material[5].Shininess[0] = 0.1f * 128.0f;

	// *** 1st Sphere On 2nd Column, Brass ****
	material[6].Ambient[0] = 0.329412f;
	material[6].Ambient[1] = 0.223529f;
	material[6].Ambient[2] = 0.027451f;
	material[6].Ambient[3] = 1.0f;

	material[6].Diffuse[0] = 0.780392f;
	material[6].Diffuse[1] = 0.568627f;
	material[6].Diffuse[2] = 0.113725f;
	material[6].Diffuse[3] = 1.0f;

	material[6].Specular[0] = 0.992157f;
	material[6].Specular[1] = 0.941176f;
	material[6].Specular[2] = 0.807843f;
	material[6].Specular[3] = 1.0f;

	material[6].Shininess[0] = 0.21794872f * 128.0f;

	// *** 2nd Sphere On 2nd Column, Bronze ****
	material[7].Ambient[0] = 0.2125f;
	material[7].Ambient[1] = 0.1275f;
	material[7].Ambient[2] = 0.054f;
	material[7].Ambient[3] = 1.0f;

	material[7].Diffuse[0] = 0.714f;
	material[7].Diffuse[1] = 0.4284f;
	material[7].Diffuse[2] = 0.18144f;
	material[7].Diffuse[3] = 1.0f;

	material[7].Specular[0] = 0.393548f;
	material[7].Specular[1] = 0.271906f;
	material[7].Specular[2] = 0.166721f;
	material[7].Specular[3] = 1.0f;

	material[7].Shininess[0] = 0.2f * 128.0f;

	// *** 3rd Sphere On 2nd Column, Chrome ****
	material[8].Ambient[0] = 0.25f;
	material[8].Ambient[1] = 0.25f;
	material[8].Ambient[2] = 0.25f;
	material[8].Ambient[3] = 1.0f;

	material[8].Diffuse[0] = 0.4f;
	material[8].Diffuse[1] = 0.4f;
	material[8].Diffuse[2] = 0.4f;
	material[8].Diffuse[3] = 1.0f;

	material[8].Specular[0] = 0.774597f;
	material[8].Specular[1] = 0.774597f;
	material[8].Specular[2] = 0.774597f;
	material[8].Specular[3] = 1.0f;

	material[8].Shininess[0] = 0.6f * 128.0f;

	// *** 4th Sphere On 2nd Column, Copper ****
	material[9].Ambient[0] = 0.19125f;
	material[9].Ambient[1] = 0.0735f;
	material[9].Ambient[2] = 0.0225f;
	material[9].Ambient[3] = 1.0f;

	material[9].Diffuse[0] = 0.7038f;
	material[9].Diffuse[1] = 0.27048f;
	material[9].Diffuse[2] = 0.0828f;
	material[9].Diffuse[3] = 1.0f;

	material[9].Specular[0] = 0.256777f;
	material[9].Specular[1] = 0.137622f;
	material[9].Specular[2] = 0.086014f;
	material[9].Specular[3] = 1.0f;

	material[9].Shininess[0] = 0.1f * 128.0f;

	// *** 5th Sphere On 2nd Column, Gold ****
	material[10].Ambient[0] = 0.24725f;
	material[10].Ambient[1] = 0.1995f;
	material[10].Ambient[2] = 0.0745f;
	material[10].Ambient[3] = 1.0f;

	material[10].Diffuse[0] = 0.75164f;
	material[10].Diffuse[1] = 0.60648f;
	material[10].Diffuse[2] = 0.22648f;
	material[10].Diffuse[3] = 1.0f;

	material[10].Specular[0] = 0.628281f;
	material[10].Specular[1] = 0.555802f;
	material[10].Specular[2] = 0.366065f;
	material[10].Specular[3] = 1.0f;

	material[10].Shininess[0] = 0.4f * 128.0f;

	// *** 6th Sphere On 2nd Column, Silver ****
	material[11].Ambient[0] = 0.19225f;
	material[11].Ambient[1] = 0.19225f;
	material[11].Ambient[2] = 0.19225f;
	material[11].Ambient[3] = 1.0f;

	material[11].Diffuse[0] = 0.50754f;
	material[11].Diffuse[1] = 0.50754f;
	material[11].Diffuse[2] = 0.50754f;
	material[11].Diffuse[3] = 1.0f;

	material[11].Specular[0] = 0.508273f;
	material[11].Specular[1] = 0.508273f;
	material[11].Specular[2] = 0.508273f;
	material[11].Specular[3] = 1.0f;

	material[11].Shininess[0] = 0.4f * 128.0f;

	// *** 1st Sphere On 3rd Column, Black ****
	material[12].Ambient[0] = 0.0f;
	material[12].Ambient[1] = 0.0f;
	material[12].Ambient[2] = 0.0f;
	material[12].Ambient[3] = 1.0f;

	material[12].Diffuse[0] = 0.01f;
	material[12].Diffuse[1] = 0.01f;
	material[12].Diffuse[2] = 0.01f;
	material[12].Diffuse[3] = 1.0f;

	material[12].Specular[0] = 0.50f;
	material[12].Specular[1] = 0.50f;
	material[12].Specular[2] = 0.50f;
	material[12].Specular[3] = 1.0f;

	material[12].Shininess[0] = 0.25f * 128.0f;

	// *** 2nd Sphere On 3rd Column, Cyan ****
	material[13].Ambient[0] = 0.0f;
	material[13].Ambient[1] = 0.1f;
	material[13].Ambient[2] = 0.06f;
	material[13].Ambient[3] = 1.0f;

	material[13].Diffuse[0] = 0.0f;
	material[13].Diffuse[1] = 0.50980392f;
	material[13].Diffuse[2] = 0.50980392f;
	material[13].Diffuse[3] = 1.0f;

	material[13].Specular[0] = 0.50196078f;
	material[13].Specular[1] = 0.50196078f;
	material[13].Specular[2] = 0.50196078f;
	material[13].Specular[3] = 1.0f;

	material[13].Shininess[0] = 0.25f * 128.0f;

	// *** 3rd Sphere On 3rd Column, Green ****
	material[14].Ambient[0] = 0.0f;
	material[14].Ambient[1] = 0.0f;
	material[14].Ambient[2] = 0.0f;
	material[14].Ambient[3] = 1.0f;

	material[14].Diffuse[0] = 0.1f;
	material[14].Diffuse[1] = 0.35f;
	material[14].Diffuse[2] = 0.1f;
	material[14].Diffuse[3] = 1.0f;

	material[14].Specular[0] = 0.45f;
	material[14].Specular[1] = 0.45f;
	material[14].Specular[2] = 0.45f;
	material[14].Specular[3] = 1.0f;

	material[14].Shininess[0] = 0.25f * 128.0f;

	// *** 4th Sphere On 3rd Column, Red ****
	material[15].Ambient[0] = 0.0f;
	material[15].Ambient[1] = 0.0f;
	material[15].Ambient[2] = 0.0f;
	material[15].Ambient[3] = 1.0f;

	material[15].Diffuse[0] = 0.5f;
	material[15].Diffuse[1] = 0.0f;
	material[15].Diffuse[2] = 0.0f;
	material[15].Diffuse[3] = 1.0f;

	material[15].Specular[0] = 0.7f;
	material[15].Specular[1] = 0.6f;
	material[15].Specular[2] = 0.6f;
	material[15].Specular[3] = 1.0f;

	material[15].Shininess[0] = 0.25f * 128.0f;

	// *** 5th Sphere On 3rd Column, White ****
	material[16].Ambient[0] = 0.0f;
	material[16].Ambient[1] = 0.0f;
	material[16].Ambient[2] = 0.0f;
	material[16].Ambient[3] = 1.0f;

	material[16].Diffuse[0] = 0.55f;
	material[16].Diffuse[1] = 0.55f;
	material[16].Diffuse[2] = 0.55f;
	material[16].Diffuse[3] = 1.0f;

	material[16].Specular[0] = 0.70f;
	material[16].Specular[1] = 0.70f;
	material[16].Specular[2] = 0.70f;
	material[16].Specular[3] = 1.0f;

	material[16].Shininess[0] = 0.25f * 128.0f;

	// *** 6th Sphere On 3rd Column, Yello ****
	material[17].Ambient[0] = 0.0f;
	material[17].Ambient[1] = 0.0f;
	material[17].Ambient[2] = 0.0f;
	material[17].Ambient[3] = 1.0f;

	material[17].Diffuse[0] = 0.5f;
	material[17].Diffuse[1] = 0.5f;
	material[17].Diffuse[2] = 0.0f;
	material[17].Diffuse[3] = 1.0f;

	material[17].Specular[0] = 0.60f;
	material[17].Specular[1] = 0.60f;
	material[17].Specular[2] = 0.50f;
	material[17].Specular[3] = 1.0f;

	material[17].Shininess[0] = 0.25f * 128.0f;

	// *** 1st Sphere On 4th Column, Black ****
	material[18].Ambient[0] = 0.02f;
	material[18].Ambient[1] = 0.02f;
	material[18].Ambient[2] = 0.02f;
	material[18].Ambient[3] = 1.0f;

	material[18].Diffuse[0] = 0.01f;
	material[18].Diffuse[1] = 0.01f;
	material[18].Diffuse[2] = 0.01f;
	material[18].Diffuse[3] = 1.0f;

	material[18].Specular[0] = 0.4f;
	material[18].Specular[1] = 0.4f;
	material[18].Specular[2] = 0.4f;
	material[18].Specular[3] = 1.0f;

	material[18].Shininess[0] = 0.078125f * 128.0f;

	// *** 2nd Sphere On 4th Column, Cyan ****
	material[19].Ambient[0] = 0.0f;
	material[19].Ambient[1] = 0.05f;
	material[19].Ambient[2] = 0.05f;
	material[19].Ambient[3] = 1.0f;

	material[19].Diffuse[0] = 0.4f;
	material[19].Diffuse[1] = 0.5f;
	material[19].Diffuse[2] = 0.5f;
	material[19].Diffuse[3] = 1.0f;

	material[19].Specular[0] = 0.04f;
	material[19].Specular[1] = 0.7f;
	material[19].Specular[2] = 0.7f;
	material[19].Specular[3] = 1.0f;

	material[19].Shininess[0] = 0.078125f * 128.0f;

	// *** 3rd Sphere On 4th Column, Green ****
	material[20].Ambient[0] = 0.0f;
	material[20].Ambient[1] = 0.05f;
	material[20].Ambient[2] = 0.0f;
	material[20].Ambient[3] = 1.0f;

	material[20].Diffuse[0] = 0.4f;
	material[20].Diffuse[1] = 0.5f;
	material[20].Diffuse[2] = 0.4f;
	material[20].Diffuse[3] = 1.0f;

	material[20].Specular[0] = 0.04f;
	material[20].Specular[1] = 0.7f;
	material[20].Specular[2] = 0.04f;
	material[20].Specular[3] = 1.0f;

	material[20].Shininess[0] = 0.078125f * 128.0f;

	// *** 4th Sphere On 4th Column, Red ****
	material[21].Ambient[0] = 0.05f;
	material[21].Ambient[1] = 0.0f;
	material[21].Ambient[2] = 0.0f;
	material[21].Ambient[3] = 1.0f;

	material[21].Diffuse[0] = 0.5f;
	material[21].Diffuse[1] = 0.4f;
	material[21].Diffuse[2] = 0.4f;
	material[21].Diffuse[3] = 1.0f;

	material[21].Specular[0] = 0.7f;
	material[21].Specular[1] = 0.04f;
	material[21].Specular[2] = 0.04f;
	material[21].Specular[3] = 1.0f;

	material[21].Shininess[0] = 0.078125f * 128.0f;

	// *** 5th Sphere On 4th Column, White ****
	material[22].Ambient[0] = 0.05f;
	material[22].Ambient[1] = 0.05f;
	material[22].Ambient[2] = 0.05f;
	material[22].Ambient[3] = 1.0f;

	material[22].Diffuse[0] = 0.5f;
	material[22].Diffuse[1] = 0.5f;
	material[22].Diffuse[2] = 0.5f;
	material[22].Diffuse[3] = 1.0f;

	material[22].Specular[0] = 0.7f;
	material[22].Specular[1] = 0.7f;
	material[22].Specular[2] = 0.7f;
	material[22].Specular[3] = 1.0f;

	material[22].Shininess[0] = 0.078125f * 128.0f;

	// *** 6th Sphere On 4th Column, Yello ****
	material[23].Ambient[0] = 0.05f;
	material[23].Ambient[1] = 0.05f;
	material[23].Ambient[2] = 0.0f;
	material[23].Ambient[3] = 1.0f;

	material[23].Diffuse[0] = 0.5f;
	material[23].Diffuse[1] = 0.5f;
	material[23].Diffuse[2] = 0.4f;
	material[23].Diffuse[3] = 1.0f;

	material[23].Specular[0] = 0.7f;
	material[23].Specular[1] = 0.7f;
	material[23].Specular[2] = 0.04f;
	material[23].Specular[3] = 1.0f;

	material[23].Shininess[0] = 0.078125f * 128.0f;
}

void uninitialize_RMB(void)
{
	GLXContext CurrentgLXContext;

	//code
	if (vbo_Element_Sphere)
	{
		glDeleteBuffers(1, &vbo_Element_Sphere);
		vbo_Element_Sphere = 0;
	}
	
	if (vbo_Position_Sphere)
	{
		glDeleteBuffers(1, &vbo_Position_Sphere);
		vbo_Position_Sphere = 0;
	}
	
	if (vbo_Normal_Sphere)
	{
		glDeleteBuffers(1, &vbo_Normal_Sphere);
		vbo_Normal_Sphere = 0;
	}
	
	if (vao_Sphere)
	{
		glDeleteVertexArrays(1, &vao_Sphere);
		vao_Sphere = 0;
	}

	
	if (gShaderProgramObject_PF)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject_PF);
		//Ask Program How Many Shaders are Attached to you
		glGetProgramiv(gShaderProgramObject_PF, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject_PF, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject_PF, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject_PF);
		gShaderProgramObject_PF = 0;
		glUseProgram(0);
	}

	if (gShaderProgramObject_PV)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject_PV);
		//Ask Program How Many Shaders are Attached to you
		glGetProgramiv(gShaderProgramObject_PV, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject_PV, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject_PV, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject_PV);
		gShaderProgramObject_PV = 0;
		glUseProgram(0);
	}

	CurrentgLXContext = glXGetCurrentContext();
	if(CurrentgLXContext != NULL && CurrentgLXContext == gLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);	
	}

	if(gGLXFBConfig)
	{
		XFree(gGLXFBConfig);
	}
	
	if(gLXContext)
	{
		glXDestroyContext(gpDisplay, gLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);  //as memory might get allocated inside choose visual
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}

void ToggleFullScreen_RMB(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), False, StructureNotifyMask, &xev);
}

void perVertex(void)
{
	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;

	//Define Vertex Shader Object
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
			"#version 450 core" \
		"\n" \
		"in vec4 vertexPosition;" \
		"in vec3 vertexNormal;" \

		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projection_matrix;" \

		"uniform vec3 u_La_Red;" \
		"uniform vec3 u_Ld_Red;" \
		"uniform vec3 u_LmaUniform;" \
		"uniform vec3 u_LmlvUinform;" \

		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \

		"uniform float u_materialShininess;" \
		"uniform int u_lKeyIsPressed;" \

		"uniform vec4 u_Light_Position_Red;" \


		"out vec3 phong_AdsLight;" \

		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"vec4 eye_coordinate = u_viewMatrix * u_modelMatrix * vertexPosition;" \
		"vec3 tnorm = normalize(mat3(u_viewMatrix * u_modelMatrix) * vertexNormal);" \
		//For Red Light
		"vec3 lightdirection_Red = normalize(vec3(u_Light_Position_Red - eye_coordinate));" \

		"float tn_dot_lightDir_Red = max(dot(lightdirection_Red, tnorm), 0.0);" \

		"vec3 reflectionVector_Red = reflect(-lightdirection_Red, tnorm);" \

		"vec3 viewerVector = normalize(vec3(-eye_coordinate.xyz));" \

		"vec3 ambient_Red = u_La_Red * u_LmaUniform * u_Ka;" \

		"vec3 diffuse_Red = u_Ld_Red * u_Kd * tn_dot_lightDir_Red;" \

		"vec3 specular_Red = u_Ks * pow(max(dot(reflectionVector_Red, viewerVector), 0.0), u_materialShininess);" \

		"phong_AdsLight = ambient_Red + diffuse_Red + specular_Red;" \

		"}" \
		"else" \
		"{" \
		"phong_AdsLight = vec3(1.0, 1.0, 1.0);" \
		"}" \

		"gl_Position = u_projection_matrix * u_viewMatrix * u_modelMatrix * vertexPosition;" \
		"}";
	
	
	//Specifing above source to the vertex shader object
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//Compile the Vertex Shader
	glCompileShader(vertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("vertexShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 phong_AdsLight;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
		"fragColor = vec4(phong_AdsLight, 1.0);" \
		"}";


	// Specifing above Source to the fragment Shader Object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//Compile the fragment Shader Object
	glCompileShader(fragmentShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("FragmentShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Create Shader Object
	gShaderProgramObject_PV = glCreateProgram();

	//Attach VertexShader to the Shader Program
	glAttachShader(gShaderProgramObject_PV, vertexShaderObject);

	//Attach fragmentShader to the Shader Program
	glAttachShader(gShaderProgramObject_PV, fragmentShaderObject);

	// Pre Linking Binding to Vertex Attribute
	glBindAttribLocation(gShaderProgramObject_PV, AMC_ATTRIBUTE_POSITION, "vertexPosition");
	glBindAttribLocation(gShaderProgramObject_PV, AMC_ATTRIBUTE_NORMAL, "vertexNormal");

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject_PV);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject_PV, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_PV, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_PV, iInfoLogLength, &written, szInfoLog);
				printf("LinkShaderProgramObject:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Post Linking Retriving Uniform Location
	modelUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_modelMatrix");
	viewUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_viewMatrix");
	projectionUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_projection_matrix");

	LaUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_La_Red");
	LdUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_Ld_Red");
	LMAUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_LmaUniform");
	LMLVUinform = glGetUniformLocation(gShaderProgramObject_PV, "u_LmlvUinform");

	KaUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_Ka");
	KdUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_Kd");
	KsUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_Ks");

	materialShininessUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_materialShininess");
	lightPositionUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_Light_Position_Red");
	lKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_lKeyIsPressed");
}

void perFragment(void)
{
	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	
	//Define Vertex Shader Object
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar* vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vertexPosition_RMB;" \
		"in vec3 vertexNormal_RMB;" \

		"uniform mat4 u_modelMatrix_RMB;" \
		"uniform mat4 u_viewMatrix_RMB;" \
		"uniform mat4 u_projection_matrix_RMB;" \

		"uniform int u_lKeyIsPressed_RMB;" \
		"uniform vec4 u_Light_Position_Red_RMB;" \

		"out vec3 tnorm_RMB;" \
		"out vec3 lightdirection_Red_RMB;" \
		"out vec3 viewerVector_RMB;" \

		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed_RMB == 1)" \
		"{" \
		"vec4 eye_coordinate_RMB = u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" \
		"tnorm_RMB = mat3(u_viewMatrix_RMB * u_modelMatrix_RMB) * vertexNormal_RMB;" \
		//For Red Light Calculations
		"lightdirection_Red_RMB = vec3(u_Light_Position_Red_RMB - eye_coordinate_RMB);" \

		"viewerVector_RMB = vec3(-eye_coordinate_RMB.xyz);" \
		"}" \

		"gl_Position = u_projection_matrix_RMB * u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" \
		"}";

	
	//Specifing above source to the vertex shader object
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//Compile the Vertex Shader
	glCompileShader(vertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("vertexShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 tnorm_RMB;" \
		"in vec3 lightdirection_Red_RMB;" \
		"in vec3 viewerVector_RMB;" \

		"uniform vec3 u_La_Red_RMB;" \
		"uniform vec3 u_Ld_Red_RMB;" \
		"uniform vec3 u_LmaUniform_RMB;" \
		"uniform vec3 u_LmlvUinform_RMB;" \

		"uniform vec3 u_Ka_RMB;" \
		"uniform vec3 u_Kd_RMB;" \
		"uniform vec3 u_Ks_RMB;" \

		"uniform float u_materialShininess_RMB;" \
		"uniform int u_lKeyIsPressed_RMB;" \


		"out vec4 fragColor_RMB;" \

		"void main(void)" \
		"{" \
		"vec3 phong_AdsLight_RMB;" \
		"if(u_lKeyIsPressed_RMB == 1)" \
		"{" \
		//For Red Light Calculations
		"vec3 normalize_tnorm_RMB = normalize(tnorm_RMB);" \

		"vec3 normalize_lightdirection_Red_RMB = normalize(lightdirection_Red_RMB);" \

		"vec3 normalize_viewerVector_RMB = normalize(viewerVector_RMB);" \

		"vec3 reflectionVector_Red_RMB = reflect(-normalize_lightdirection_Red_RMB, normalize_tnorm_RMB);" \

		"float tn_dot_lightDir_Red_RMB = max(dot(normalize_lightdirection_Red_RMB, normalize_tnorm_RMB), 0.0);" \

		"vec3 ambient_Red_RMB = u_La_Red_RMB * u_LmaUniform_RMB * u_Ka_RMB;" \

		"vec3 diffuse_Red_RMB = u_Ld_Red_RMB * u_Kd_RMB * tn_dot_lightDir_Red_RMB;" \

		"vec3 specular_Red_RMB =  u_Ks_RMB * pow(max(dot(reflectionVector_Red_RMB, normalize_viewerVector_RMB), 0.0), u_materialShininess_RMB);" \

		"phong_AdsLight_RMB = ambient_Red_RMB + diffuse_Red_RMB + specular_Red_RMB;" \

		"}" \
		"else" \
		"{" \

		"phong_AdsLight_RMB = vec3(1.0, 1.0, 1.0);" \

		"}" \

		"fragColor_RMB = vec4(phong_AdsLight_RMB, 1.0);" \
		"}";

	// Specifing above Source to the fragment Shader Object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//Compile the fragment Shader Object
	glCompileShader(fragmentShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("FragmentShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Create Shader Object
	gShaderProgramObject_PF = glCreateProgram();

	//Attach VertexShader to the Shader Program
	glAttachShader(gShaderProgramObject_PF, vertexShaderObject);

	//Attach fragmentShader to the Shader Program
	glAttachShader(gShaderProgramObject_PF, fragmentShaderObject);

	// Pre Linking Binding to Vertex Attribute
	glBindAttribLocation(gShaderProgramObject_PF, AMC_ATTRIBUTE_POSITION, "vertexPosition_RMB");
	glBindAttribLocation(gShaderProgramObject_PF, AMC_ATTRIBUTE_NORMAL, "vertexNormal_RMB");

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject_PF);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject_PF, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_PF, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_PF, iInfoLogLength, &written, szInfoLog);
				printf("LinkShaderProgramObject:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Post Linking Retriving Uniform Location
	modelUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_modelMatrix_RMB");
	viewUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_viewMatrix_RMB");
	projectionUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_projection_matrix_RMB");

	LaUniform_Red_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_La_Red_RMB");
	LdUniform_Red_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ld_Red_RMB");
	LMAUniform_RMB = glGetUniformLocation(gShaderProgramObject_PV, "u_LmaUniform_RMB");
	LMLVUinform_RMB = glGetUniformLocation(gShaderProgramObject_PV, "u_LmlvUinform_RMB");

	KaUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ka_RMB");
	KdUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Kd_RMB");
	KsUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ks_RMB");

	materialShininessUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_materialShininess_RMB");
	lightPositionUniform_Red_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Light_Position_Red_RMB");
	lKeyIsPressedUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_lKeyIsPressed_RMB");
}
