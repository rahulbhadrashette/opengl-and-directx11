#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#include<string.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

//OpenGL Related Files
#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>  //In Windows(Wgl) 

#include"vmath.h"

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTCOORD0
};


//namespaces
using namespace std;
using namespace vmath;

// global variable Declaration
static GLXContext gLXContext;
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;

FILE *gpFile;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao;
GLuint vbo;
GLuint vbo_Color;

GLuint mvpUniform;

//Entry-Point Function
int main(void)
{
	//Function Declaration
	void Resize_RMB(int, int);
	void Initialize_RMB(void);
	void Display_RMB(void);
	void CreateWindow_RMB(void);
	void ToggleFullScreen_RMB(void);
	void uninitialize_RMB(void);

	gpFile = fopen("OpenGL_Log.txt", "w");
	if(gpFile == NULL)
	{
		printf("Error in Opening Log File...!\nExitting Now....!!!");
		return(EXIT_FAILURE);
	}

	// Local Variable Declaration
	bool bDone = false;
	char keys[26];
	static int winWidth = giWindowWidth;
	static int winHeight = giWindowHeight;

	//Code
	CreateWindow_RMB();
	Initialize_RMB();


	XEvent event;
	KeySym keysym;

	fprintf(gpFile, "Entering into Game Loop...!!!\n");
	//Message Loop
	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					 //All System Fonts  or  fixed pitch font
				break;

				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							bDone = true;
							break;
					}

					XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
					switch(keys[0])
					{
					
						case 'F':
						case 'f':
							if(bFullScreen == false)
							{
								ToggleFullScreen_RMB();
								bFullScreen = true;
							}
							else
							{
								ToggleFullScreen_RMB();
								bFullScreen = false;
							}
							break;
					
					}
					break;

				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:   	 //Left Mouse Button
							break;
						case 2:  	 //Middle Mouse Button
							break;
						case 3:  	 //Right Mouse Button
							break;
						case 4:  	 //Wheel Up
							break;
						case 5:		 //Wheel Down
							break;
						default:
							break;
					}
					break;

				case MotionNotify:	 //WM_MOUSEMOVE:
					break;

				case ConfigureNotify://WM_SIZE:
					  winWidth = event.xconfigure.width;
					  winHeight = event.xconfigure.height;
					  Resize_RMB(winWidth, winHeight);
					break;

				case Expose:		//WM_PAINT:
					break;

				case DestroyNotify: //WM_DESTROY:
					break;

				case 33:
				     bDone = true;
				    break;

				default:
				     break;
			}
		}
		//Display Call Here And Update also
		Display_RMB();
	}

        uninitialize_RMB();
	return(0);
}

void CreateWindow_RMB(void)
{
	//Function prototype declaration
	void uninitialize_RMB(void);

	//variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	GLXFBConfig *pGLXFBConfig = NULL;   //This is Array of FBConfig
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumberOfFBConfig = 0;

	static int frameBufferAttributes[] = { 
						  	GLX_X_RENDERABLE, True,
						  	GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
						  	GLX_RENDER_TYPE, GLX_RGBA_BIT,
						  	GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
							GLX_RED_SIZE, 8,
					       	GLX_GREEN_SIZE, 8,
					      	GLX_BLUE_SIZE, 8,
					      	GLX_ALPHA_SIZE, 8,
					      	GLX_DEPTH_SIZE, 24,
					      	GLX_STENCIL_SIZE, 8,
					      	GLX_DOUBLEBUFFER, True,
					      	None
					    	};

	//Code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	//defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	pGLXFBConfig = glXChooseFBConfig(gpDisplay,  defaultScreen, frameBufferAttributes, &iNumberOfFBConfig);
	printf("%d There are Matching FBConfig.\n", iNumberOfFBConfig);
	if(pGLXFBConfig == NULL)
	{
		printf("ERROR: Unable To pGLXFBConfig. \nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}
	//Inline variable
	int bestFBConfig = -1;
	int bestNumberOfSamples = -1;
	int worstFBConfig = -1;
	int worstNumberOfSamples = 999;
	for(int i = 0; i<iNumberOfFBConfig; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfig[i]);

		if(pTempXVisualInfo)
		{
			int sampleBuffers;
			int samples;
			//Get number of sampleBuffers from respective FBConfig[i]
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);

			//Get number of samples from respective FBConfig[i]
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLES, &samples);

			if(bestFBConfig < 0 | sampleBuffers && samples > bestNumberOfSamples)
			{
				bestFBConfig = i;
				bestNumberOfSamples = samples;
			}

			if(worstFBConfig < 0 | !sampleBuffers | samples < worstNumberOfSamples)
			{
				worstFBConfig = i;
				worstNumberOfSamples = samples;
			}
		}
		XFree(pTempXVisualInfo);
	}

	bestGLXFBConfig = pGLXFBConfig[bestFBConfig];

	gGLXFBConfig = bestGLXFBConfig;
	XFree(pGLXFBConfig) ;

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, bestGLXFBConfig);

	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);

	if(!gWindow)
	{
		printf("ERROE : Failed To Create Main Window.\nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "05-Two2DShapes...Rahul.U.M.Bhadrashette...!!!");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);
}

void Initialize_RMB(void)
{
	GLenum result;
	void Resize_RMB(int, int);
	void uninitialize_RMB(void);

	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

	if(glXCreateContextAttribsARB == NULL)
	{
		printf("ERROE : Failed To glXCreateContextAttribsARB.\nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	const int Attribs[] = {
							GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
							GLX_CONTEXT_MINOR_VERSION_ARB, 5,
							GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
							None
							};

	gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, Attribs);
	if(!gGLXContext)
	{
		const int Attribs[] = {
								GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
								GLX_CONTEXT_MINOR_VERSION_ARB, 0,
								None
								};
	}

	gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, Attribs);

	if(!glXIsDirect(gpDisplay, gGLXContext))
	{
		printf("Optined Context is Not H/W Rendering Context....!\n");
	}
	else
	{
		printf("Optined Context is H/W Rendering Context....!\n");
	}


	//void uninitialize(void);
	//gLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, True);
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

	result = glewInit();
	if (result != GLEW_OK)
	{
		printf("glewInit(); is failed.\n");
		uninitialize_RMB();
		exit(1);
	}

	//Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode = 
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_Color;" \
		"void main(void)" \
		"{" \
			"gl_Position = u_mvp_matrix * vPosition;" \
			"out_Color = vColor;" \
		"}";

	//Specifing above source to the vertex shader object
	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//Compile the Vertex Shader
	glCompileShader(gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("vertexShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 out_Color;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
			"fragColor = out_Color;" \
		"}";


		// Specifing above Source to the fragment Shader Object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//Compile the fragment Shader Object
	glCompileShader(gFragmentShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("FragmentShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Create Shader Object
	gShaderProgramObject = glCreateProgram();

	//Attach VertexShader to the Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//Attach fragmentShader to the Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject);
	
	GLint  iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				printf("LinkShaderProgramObject:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Post Linking Retriving Uniform Location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	
	//TrangleArray
	const  GLfloat verties[] =
	{
		//First
		//right
		0.5f, 0.8f, 0.0f,
		0.8f, 0.8f, 0.0f,
		//top
		0.8f, 0.8f, 0.0f,
		0.8f, 0.3f, 0.0f,
		//left
		0.5f, 0.3f, 0.0f,
		0.8f, 0.3f, 0.0f,
		//botttom
		0.5f, 0.466f, 0.0f,
		0.8f, 0.466f, 0.0f,

		0.5f, 0.632f, 0.0f,
		0.8f, 0.632f, 0.0f,

		0.5f, 0.3f, 0.0f,
		0.5f, 0.8f, 0.0f,

		0.6f, 0.3f, 0.0f,
		0.6f, 0.8f, 0.0f,

		0.7f, 0.3f, 0.0f,
		0.7f, 0.8f, 0.0f,

		//Second
		//top
		-0.15f, 0.8f, 0.0f,
		0.15f, 0.8f, 0.0f,
		//left
		-0.15f, 0.3f, 0.0f,
		-0.15f, 0.8f, 0.0f,
		//horizental
		-0.15f, 0.466f, 0.0f,
		0.15f, 0.466f, 0.0f,
		-0.15f, 0.632f, 0.0f,
		0.15f, 0.632f, 0.0f,
		//vertical 
		-0.05f, 0.3f, 0.0f,
		-0.05f, 0.8f, 0.0f,
		0.05f, 0.3f, 0.0f,
		0.05f, 0.8f, 0.0f,
		//1
		-0.15f, 0.632f, 0.0f,
		-0.05f, 0.8f, 0.0f,
		//2
		-0.15f, 0.466f, 0.0f,
		0.05f, 0.8f, 0.0f,
		//3
		0.15f, 0.8f, 0.0f,
		-0.15f, 0.3f, 0.0f,
		//4
		0.15f, 0.632f, 0.0f,
		-0.05f, 0.3f, 0.0f,
		//5
		0.15f, 0.466f, 0.0f,
		0.05f, 0.3f, 0.0f,

		//Third
		-0.5f, 0.8f, 0.0f,
		-0.8f, 0.8f, 0.0f,

		-0.8f, 0.3f, 0.0f,
		-0.5f, 0.3f, 0.0f,

		-0.5f, 0.466f, 0.0f,
		-0.8f, 0.466f, 0.0f,

		-0.5f, 0.632f, 0.0f,
		-0.8f, 0.632f, 0.0f,

		-0.6f, 0.3f, 0.0f,
		-0.6f, 0.8f, 0.0f,

		-0.7f, 0.3f, 0.0f,
		-0.7f, 0.8f, 0.0f,

		-0.6f, 0.632f, 0.0f,
		-0.7f, 0.632f, 0.0f,

		-0.7f, 0.466f, 0.0f,
		-0.6f, 0.466f, 0.0f,

		//Forth
		//right 
		-0.5f, -0.3f, 0.0f,
		-0.5f, -0.8f, 0.0f,
		//top
		-0.5f, -0.3f, 0.0f,
		-0.8f, -0.3f, 0.0f,
		//left
		-0.8f, -0.8f, 0.0f,
		-0.8f, -0.3f, 0.0f,
		//bottom
		-0.5f, -0.8f, 0.0f,
		-0.8f, -0.8f, 0.0f,
		//vertical line
		-0.5f, -0.466f, 0.0f,
		-0.8f, -0.466f, 0.0f,
		-0.5f, -0.632f, 0.0f,
		-0.8f, -0.632f, 0.0f,
		//horizental Line
		-0.6f, -0.3f, 0.0f,
		-0.6f, -0.8f, 0.0f,
		-0.7f, -0.3f, 0.0f,
		-0.7f, -0.8f, 0.0f,
		//Cross Lines
		//1
		-0.8f, -0.466f, 0.0f,
		-0.7f, -0.3f, 0.0f,
		//2
		-0.8f, -0.632f, 0.0f,
		-0.6f, -0.3f, 0.0f,
		//3
		-0.5f, -0.3f, 0.0f,
		-0.8f, -0.8f, 0.0f,
		//4
		-0.5f, -0.466f, 0.0f,
		-0.7f, -0.8f, 0.0f,
		//5
		-0.6f, -0.8f, 0.0f,
		-0.5f, -0.632f, 0.0f,

		//Five
		//right
		0.15f, -0.8f, 0.0f,
		0.15f, -0.3f, 0.0f,
		//top
		0.15f, -0.3f, 0.0f,
		-0.15f, -0.3f, 0.0f,
		//left
		-0.15f, -0.3f, 0.0f,
		-0.15f, -0.8f, 0.0f,
		//botttom
		-0.15f, -0.8f, 0.0f,
		0.15f, -0.8f, 0.0f,
		//1
		-0.15f, -0.3f, 0.0f,
		0.15f, -0.466f, 0.0f,
		//2
		- 0.15f, -0.3f, 0.0f,
		0.15f, -0.632f, 0.0f,
		//3
		- 0.15f, -0.3f, 0.0f,
		0.15f, -0.8f, 0.0f,
		//4
		- 0.15f, -0.3f, 0.0f,
		- 0.05f, -0.8f, 0.0f,
		//5
		- 0.15f, -0.3f, 0.0f,
		0.05f, -0.8f, 0.0f,

		//For Triangles
		0.6f, -0.8f, 0.0f,
		0.6f, -0.3f, 0.0f,
		0.5f, -0.3f, 0.0f,

		0.5f, -0.3f, 0.0f,
		0.5f, -0.8f, 0.0f,
		0.6f, -0.8f, 0.0f,

		0.7f, -0.8f, 0.0f,
		0.7f, -0.3f, 0.0f,
		0.6f, -0.3f, 0.0f,

		0.6f, -0.3f, 0.0f,
		0.6f, -0.8f, 0.0f,
		0.7f, -0.8f, 0.0f,

		0.8f, -0.8f, 0.0f,
		0.8f, -0.3f, 0.0f,
		0.7f, -0.3f, 0.0f,

		0.7f, -0.3f, 0.0f,
		0.7f, -0.8f, 0.0f,
		0.8f, -0.8f, 0.0f,

		//Six
		//right
		0.5f, -0.8f, 0.0f,
		0.8f, -0.8f, 0.0f,
		//top
		0.8f, -0.8f, 0.0f,
		0.8f, -0.3f, 0.0f,
		//left
		0.5f, -0.3f, 0.0f,
		0.8f, -0.3f, 0.0f,
		//bottom
		0.5f, -0.3f, 0.0f,
		0.5f, -0.8f, 0.0f,
		//vertical Line
		//1
		0.6f, -0.3f, 0.0f,
		0.6f, -0.8f, 0.0f,
		//2
		0.7f, -0.3f, 0.0f,
		0.7f, -0.8f, 0.0f,
		//horixental line
		//1
		0.5f, -0.466f, 0.0f,
		0.8f, -0.466f, 0.0f,
		//2
		0.5f, -0.632f, 0.0f,
		0.8f, -0.632f, 0.0f,
	};

	//circle Color Array
	const int iPoints = 132;
	GLfloat color[3 * iPoints];
	for (int i = 0; i < iPoints; i++)
	{	
		if (i < 98)
		{
			color[3 * i + 0] = 1.0f;
			color[3 * i + 1] = 1.0f;
			color[3 * i + 2] = 1.0f;
		}
		else 
		{
			if (i < 104)
			{
				color[3 * i + 0] = 1.0f;
				color[3 * i + 1] = 0.0f;
				color[3 * i + 2] = 0.0f;
			}
			else if (i < 110)
			{
				color[3 * i + 0] = 0.0f;
				color[3 * i + 1] = 1.0f;
				color[3 * i + 2] = 0.0f;
			}
			else if (i < 116)
			{
				color[3 * i + 0] = 0.0f;
				color[3 * i + 1] = 0.0f;
				color[3 * i + 2] = 1.0f;
			}
			else 
			{
				color[3 * i + 0] = 1.0f;
				color[3 * i + 1] = 1.0f;
				color[3 * i + 2] = 1.0f;
			}
		}
	}

	//Create vao
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(verties), verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Color
	glGenBuffers(1, &vbo_Color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);
	
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//Warmup Call To Resize
	Resize_RMB(giWindowWidth, giWindowHeight);
}

void Resize_RMB(int Width, int Height)
{
	//code
	if (Height == 0)
	{
		Height = 1;
	}
	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
}

void Display_RMB(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	vmath::mat4 modelViewProjectionMatrix;
	modelViewProjectionMatrix = vmath::mat4::identity();

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao);

	

	//Draw the Necessary Scnes
	//Start Right Top First
	glLineWidth(2.0f);
	glDrawArrays(GL_LINES, 0, 16);
	//Second
	glDrawArrays(GL_LINES, 16, 22);
	//Third
	glPointSize(6.0f);
	glDrawArrays(GL_POINTS, 38, 16);
	//Forth
	glDrawArrays(GL_LINES, 54, 26);
	//Five
	glDrawArrays(GL_LINES, 80, 18);
	//Six
	glDrawArrays(GL_TRIANGLES, 98, 18);
	glDrawArrays(GL_LINES, 116, 16);
	
	//UnBind vao
	glBindVertexArray(0);

	//UnUse Program
	glUseProgram(0);
	glXSwapBuffers(gpDisplay, gWindow);
}

void uninitialize_RMB(void)
{
	GLXContext CurrentgLXContext;

	//code
	if (vbo_Color)
	{
		glDeleteBuffers(1, &vbo_Color);
		vbo_Color = 0;
	}
	if (vbo)
	{
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}
	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}
	
	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);
		//Ask Program How Many Shaders are Attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	CurrentgLXContext = glXGetCurrentContext();
	if(CurrentgLXContext != NULL && CurrentgLXContext == gLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);	
	}

	if(gGLXFBConfig)
	{
		XFree(gGLXFBConfig);
	}
	
	if(gLXContext)
	{
		glXDestroyContext(gpDisplay, gLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);  //as memory might get allocated inside choose visual
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}

void ToggleFullScreen_RMB(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), False, StructureNotifyMask, &xev);
}


