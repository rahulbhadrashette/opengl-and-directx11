#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#include<string.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

//OpenGL Related Files
#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>  //In Windows(Wgl) 

#include"vmath.h"
#include"Sphere.h"

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTCOORD0
};


//namespaces
using namespace std;
using namespace vmath;

// global variable Declaration
static GLXContext gLXContext;
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;

FILE *gpFile;

bool gbLight;
bool vertexShader = false;
bool fragmentShader = false;

GLfloat angleRotate = 0.0f;
GLfloat Co1 = 0.0f;
GLfloat Co2 = 0.0f;

GLuint gShaderProgramObject_PV;
GLuint gShaderProgramObject_PF;

/*GLuint modelMatrixUniform_Red;
GLuint viewMatrixUniform_Red;
GLuint projectionMatrixUniform_Red;*/

GLuint LaUniform_Red;
GLuint LdUniform_Red;
GLuint LsUniform_Red;
GLuint lightPositionUniform_Red;

GLuint LaUniform_Green;
GLuint LdUniform_Green;
GLuint LsUniform_Green;
GLuint lightPositionUniform_Green;

GLuint LaUniform_Blue;
GLuint LdUniform_Blue;
GLuint LsUniform_Blue;
GLuint lightPositionUniform_Blue;

GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionMatrixUniform;

GLuint KaUniform;
GLuint KdUniform;
GLuint KsUniform;

GLuint materialShininessUniform;
GLuint lKeyIsPressedUniform;


vmath::mat4 projectionMatrix;
vmath::mat4 modelMatrix;
vmath::mat4 viewMatrix;

GLuint vao_Sphere;
GLuint vbo_Position_Sphere;
GLuint vbo_Normal_Sphere;
GLuint vbo_Element_Sphere;

//For Sphere
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

unsigned int gNumVertices;
unsigned int gNumElements;

struct Light
{
	GLfloat Ambient[4];
	GLfloat Diffuse[4];
	GLfloat Specular[4];
	GLfloat Position[4];
};

struct  Light light[3];

GLfloat materialAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f }; //Ka
GLfloat materialDiffuse[4] = { 0.5f, 0.2f, 0.7f, 1.0f };  //Kd
GLfloat materialSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };  //Ks
GLfloat materialPosition[4] = { 100.0f, 100.0f, 100.0f, 1.0f };
GLfloat materialShininess[1] = { 128.0f }; //50.0f;

//Entry-Point Function
int main(void)
{
	//Function Declaration
	void Resize_RMB(int, int);
	void Initialize_RMB(void);
	void Display_RMB(void);
	void CreateWindow_RMB(void);
	void ToggleFullScreen_RMB(void);
	void uninitialize_RMB(void);
	void Update_RMB(void);

	gpFile = fopen("OpenGL_Log.txt", "w");
	if(gpFile == NULL)
	{
		printf("Error in Opening Log File...!\nExitting Now....!!!");
		return(EXIT_FAILURE);
	}

	// Local Variable Declaration
	bool bDone = false;
	char keys[26];
	static int winWidth = giWindowWidth;
	static int winHeight = giWindowHeight;

	//Code
	CreateWindow_RMB();
	Initialize_RMB();


	XEvent event;
	KeySym keysym;

	fprintf(gpFile, "Entering into Game Loop...!!!\n");
	//Message Loop
	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					 //All System Fonts  or  fixed pitch font
				break;

				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							if(bFullScreen == false)
							{
								ToggleFullScreen_RMB();
								bFullScreen = true;
							}
							else
							{
								ToggleFullScreen_RMB();
								bFullScreen = false;
							}
							break;
					}

					XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
					switch(keys[0])
					{
						case 'E':
						case 'e':
						case 'Q':
						case 'q':
								 bDone = true;
							break;

					
						case 'F':
						case 'f':
								fragmentShader = true;
								vertexShader = false;
							break;

						case 'V':
						case 'v':
								vertexShader = true;
								fragmentShader = false;
							break;

						case 'L':
						case 'l':
								gbLight = !gbLight;
							break;
					
					}
					break;

				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:   	 //Left Mouse Button
							break;
						case 2:  	 //Middle Mouse Button
							break;
						case 3:  	 //Right Mouse Button
							break;
						case 4:  	 //Wheel Up
							break;
						case 5:		 //Wheel Down
							break;
						default:
							break;
					}
					break;

				case MotionNotify:	 //WM_MOUSEMOVE:
					break;

				case ConfigureNotify://WM_SIZE:
					  winWidth = event.xconfigure.width;
					  winHeight = event.xconfigure.height;
					  Resize_RMB(winWidth, winHeight);
					break;

				case Expose:		//WM_PAINT:
					break;

				case DestroyNotify: //WM_DESTROY:
					break;

				case 33:
				     bDone = true;
				    break;

				default:
				     break;
			}
		}
		//Display Call Here And Update also
		Update_RMB();
		Display_RMB();
	}

        uninitialize_RMB();
	return(0);
}

void CreateWindow_RMB(void)
{
	//Function prototype declaration
	void uninitialize_RMB(void);

	//variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	GLXFBConfig *pGLXFBConfig = NULL;   //This is Array of FBConfig
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumberOfFBConfig = 0;

	static int frameBufferAttributes[] = { 
						  	GLX_X_RENDERABLE, True,
						  	GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
						  	GLX_RENDER_TYPE, GLX_RGBA_BIT,
						  	GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
							GLX_RED_SIZE, 8,
					       	GLX_GREEN_SIZE, 8,
					      	GLX_BLUE_SIZE, 8,
					      	GLX_ALPHA_SIZE, 8,
					      	GLX_DEPTH_SIZE, 24,
					      	GLX_STENCIL_SIZE, 8,
					      	GLX_DOUBLEBUFFER, True,
					      	None
					    	};

	//Code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	//defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	pGLXFBConfig = glXChooseFBConfig(gpDisplay,  defaultScreen, frameBufferAttributes, &iNumberOfFBConfig);
	printf("%d There are Matching FBConfig.\n", iNumberOfFBConfig);
	if(pGLXFBConfig == NULL)
	{
		printf("ERROR: Unable To pGLXFBConfig. \nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}
	//Inline variable
	int bestFBConfig = -1;
	int bestNumberOfSamples = -1;
	int worstFBConfig = -1;
	int worstNumberOfSamples = 999;
	for(int i = 0; i<iNumberOfFBConfig; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfig[i]);

		if(pTempXVisualInfo)
		{
			int sampleBuffers;
			int samples;
			//Get number of sampleBuffers from respective FBConfig[i]
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);

			//Get number of samples from respective FBConfig[i]
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLES, &samples);

			if(bestFBConfig < 0 | sampleBuffers && samples > bestNumberOfSamples)
			{
				bestFBConfig = i;
				bestNumberOfSamples = samples;
			}

			if(worstFBConfig < 0 | !sampleBuffers | samples < worstNumberOfSamples)
			{
				worstFBConfig = i;
				worstNumberOfSamples = samples;
			}
		}
		XFree(pTempXVisualInfo);
	}

	bestGLXFBConfig = pGLXFBConfig[bestFBConfig];

	gGLXFBConfig = bestGLXFBConfig;
	XFree(pGLXFBConfig) ;

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, bestGLXFBConfig);

	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);

	if(!gWindow)
	{
		printf("ERROE : Failed To Create Main Window.\nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "16-perVertex_And_PerFragmentLighting..RahulUMB...!!!");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);
}

void Initialize_RMB(void)
{
	GLenum result;
	void perVertex(void);
	void perFragment(void);
	void Resize_RMB(int, int);
	void uninitialize_RMB(void);

	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

	if(glXCreateContextAttribsARB == NULL)
	{
		printf("ERROE : Failed To glXCreateContextAttribsARB.\nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	const int Attribs[] = {
							GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
							GLX_CONTEXT_MINOR_VERSION_ARB, 5,
							GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
							None
							};

	gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, Attribs);
	if(!gGLXContext)
	{
		const int Attribs[] = {
								GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
								GLX_CONTEXT_MINOR_VERSION_ARB, 0,
								None
								};
	}

	gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, Attribs);

	if(!glXIsDirect(gpDisplay, gGLXContext))
	{
		printf("Optined Context is Not H/W Rendering Context....!\n");
	}
	else
	{
		printf("Optined Context is H/W Rendering Context....!\n");
	}


	//void uninitialize(void);
	//gLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, True);
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

	result = glewInit();
	if (result != GLEW_OK)
	{
		printf("glewInit(); is failed.\n");
		uninitialize_RMB();
		exit(1);
	}

	//Function Call per Fragment 
	perVertex();
	perFragment();
	
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	//Position
	glGenVertexArrays(1, &vao_Sphere);
	glBindVertexArray(vao_Sphere);
	glGenBuffers(1, &vbo_Position_Sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Normal
	glGenBuffers(1, &vbo_Normal_Sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Normal_Sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Element vbo
	glGenBuffers(1, &vbo_Element_Sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	modelMatrix = vmath::mat4::identity();
	viewMatrix = vmath::mat4::identity();
	projectionMatrix = vmath::mat4::identity();
	//Warmup Call To Resize
	Resize_RMB(giWindowWidth, giWindowHeight);
}

void Resize_RMB(int Width, int Height)
{
	//code
	if (Height == 0)
	{
		Height = 1;
	}
	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);

	projectionMatrix = vmath::perspective(45.0f, (GLfloat)Width/(GLfloat)Height , 0.1f, 100.0f);
}

void Display_RMB(void)
{
	//code
	void Light_Array(void);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Light_Array();

	if(fragmentShader == true)
	{
		glUseProgram(gShaderProgramObject_PF);

		if (gbLight == true)
		{
			glUniform1i(lKeyIsPressedUniform, 1);
			glUniform3fv(LaUniform_Red, 1, light[0].Ambient);
			glUniform3fv(LdUniform_Red, 1, light[0].Diffuse);
			glUniform3fv(LsUniform_Red, 1, light[0].Specular);
			glUniform4fv(lightPositionUniform_Red, 1, light[0].Position);

			glUniform3fv(LaUniform_Green, 1, light[1].Ambient);
			glUniform3fv(LdUniform_Green, 1, light[1].Diffuse);
			glUniform3fv(LsUniform_Green, 1, light[1].Specular);
			glUniform4fv(lightPositionUniform_Green, 1, light[1].Position);

			glUniform3fv(LaUniform_Blue, 1, light[2].Ambient);
			glUniform3fv(LdUniform_Blue, 1, light[2].Diffuse);
			glUniform3fv(LsUniform_Blue, 1, light[2].Specular);
			glUniform4fv(lightPositionUniform_Blue, 1, light[2].Position);
	
			glUniform3fv(KaUniform, 1, materialAmbient);
			glUniform3fv(KdUniform, 1, materialDiffuse);
			glUniform3fv(KsUniform, 1, materialSpecular);
			glUniform1fv(materialShininessUniform, 1, materialShininess);
		}
		else
		{
			glUniform1i(lKeyIsPressedUniform, 0);
		}
	}
	else
	{
		glUseProgram(gShaderProgramObject_PV);

		if (gbLight == true)
		{
			glUniform1i(lKeyIsPressedUniform, 1);
			glUniform3fv(LaUniform_Red, 1, light[0].Ambient);
			glUniform3fv(LdUniform_Red, 1, light[0].Diffuse);
			glUniform3fv(LsUniform_Red, 1, light[0].Specular);
			glUniform4fv(lightPositionUniform_Red, 1, light[0].Position);

			glUniform3fv(LaUniform_Green, 1, light[1].Ambient);
			glUniform3fv(LdUniform_Green, 1, light[1].Diffuse);
			glUniform3fv(LsUniform_Green, 1, light[1].Specular);
			glUniform4fv(lightPositionUniform_Green, 1, light[1].Position);

			glUniform3fv(LaUniform_Blue, 1, light[2].Ambient);
			glUniform3fv(LdUniform_Blue, 1, light[2].Diffuse);
			glUniform3fv(LsUniform_Blue, 1, light[2].Specular);
			glUniform4fv(lightPositionUniform_Blue, 1, light[2].Position);
	
			glUniform3fv(KaUniform, 1, materialAmbient);
			glUniform3fv(KdUniform, 1, materialDiffuse);
			glUniform3fv(KsUniform, 1, materialSpecular);
			glUniform1fv(materialShininessUniform, 1, materialShininess);
		}
		else
		{
			glUniform1i(lKeyIsPressedUniform, 0);
		}
	}
	
	//Declatation of Matricex
	vmath::mat4 translationMatrix;
	//Initialize of Matrix in identity
	modelMatrix = vmath::mat4::identity();
	//viewMatrix = vmath::mat4::identity();
	translationMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.0f, 0.0f, -2.0f);

	//Do Necessary Matrix Multiplication
	modelMatrix = modelMatrix * translationMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, projectionMatrix);

	//BindWith vao
	glBindVertexArray(vao_Sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
	//Draw the Necessary Scnes
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	//UnBind vao
	glBindVertexArray(0);
	//UnUse Program
	glUseProgram(0);

	glXSwapBuffers(gpDisplay, gWindow);
}

void Update_RMB(void)
{
	angleRotate = angleRotate + 0.0005f;
	if (angleRotate > 360.0f)
	{
		angleRotate = 0.0f;
	}
	
	Co1 = (GLfloat)cos(angleRotate) * 100.0f;
	Co2 = (GLfloat)sin(angleRotate) * 100.0f;
}

void Light_Array(void)
{
	//For Red Color
	light[0].Ambient[0] = 0.0f;
	light[0].Ambient[1] = 0.0f;
	light[0].Ambient[2] = 0.0f;
	light[0].Ambient[3] = 1.0f;

	light[0].Diffuse[0] = 1.0f;
	light[0].Diffuse[1] = 0.0f;
	light[0].Diffuse[2] = 0.0f;
	light[0].Diffuse[3] = 1.0f;

	light[0].Specular[0] = 1.0f;
	light[0].Specular[1] = 0.0f;
	light[0].Specular[2] = 0.0f;
	light[0].Specular[3] = 1.0f;

	light[0].Position[0] = 0.0f;
	light[0].Position[1] = Co1;
	light[0].Position[2] = Co2;
	light[0].Position[3] = 1.0f;

	//For Green Color
	light[1].Ambient[0] = 0.0f;
	light[1].Ambient[1] = 0.0f;
	light[1].Ambient[2] = 0.0f;
	light[1].Ambient[3] = 1.0f;

	light[1].Diffuse[0] = 0.0f;
	light[1].Diffuse[1] = 1.0f;
	light[1].Diffuse[2] = 0.0f;
	light[1].Diffuse[3] = 1.0f;

	light[1].Specular[0] = 0.0f;
	light[1].Specular[1] = 1.0f;
	light[1].Specular[2] = 0.0f;
	light[1].Specular[3] = 1.0f;

	light[1].Position[0] = Co1;
	light[1].Position[1] = 0.0f;
	light[1].Position[2] = Co2;
	light[1].Position[3] = 1.0f;

	//For Blue Color
	light[2].Ambient[0] = 0.0f;
	light[2].Ambient[1] = 0.0f;
	light[2].Ambient[2] = 0.0f;
	light[2].Ambient[3] = 1.0f;

	light[2].Diffuse[0] = 0.0f;
	light[2].Diffuse[1] = 0.0f;
	light[2].Diffuse[2] = 1.0f;
	light[2].Diffuse[3] = 1.0f;

	light[2].Specular[0] = 0.0f;
	light[2].Specular[1] = 0.0f;
	light[2].Specular[2] = 1.0f;
	light[2].Specular[3] = 1.0f;

	light[2].Position[0] = Co1;
	light[2].Position[1] = Co2;
	light[2].Position[2] = 0.0f;
	light[2].Position[3] = 1.0f;

}

void uninitialize_RMB(void)
{
	GLXContext CurrentgLXContext;

	//code
	if (vbo_Element_Sphere)
	{
		glDeleteBuffers(1, &vbo_Element_Sphere);
		vbo_Element_Sphere = 0;
	}
	
	if (vbo_Position_Sphere)
	{
		glDeleteBuffers(1, &vbo_Position_Sphere);
		vbo_Position_Sphere = 0;
	}
	
	if (vbo_Normal_Sphere)
	{
		glDeleteBuffers(1, &vbo_Normal_Sphere);
		vbo_Normal_Sphere = 0;
	}
	
	if (vao_Sphere)
	{
		glDeleteVertexArrays(1, &vao_Sphere);
		vao_Sphere = 0;
	}

	
	if (gShaderProgramObject_PF)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject_PF);
		//Ask Program How Many Shaders are Attached to you
		glGetProgramiv(gShaderProgramObject_PF, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject_PF, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject_PF, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject_PF);
		gShaderProgramObject_PF = 0;
		glUseProgram(0);
	}

	if (gShaderProgramObject_PV)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject_PV);
		//Ask Program How Many Shaders are Attached to you
		glGetProgramiv(gShaderProgramObject_PV, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject_PV, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject_PV, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject_PV);
		gShaderProgramObject_PV = 0;
		glUseProgram(0);
	}

	CurrentgLXContext = glXGetCurrentContext();
	if(CurrentgLXContext != NULL && CurrentgLXContext == gLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);	
	}

	if(gGLXFBConfig)
	{
		XFree(gGLXFBConfig);
	}
	
	if(gLXContext)
	{
		glXDestroyContext(gpDisplay, gLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);  //as memory might get allocated inside choose visual
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}

void ToggleFullScreen_RMB(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), False, StructureNotifyMask, &xev);
}

void perVertex(void)
{
	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;

	//Define Vertex Shader Object
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
				"#version 450 core" \
				"\n" \
				"in vec4 vertexPosition;" \
				"in vec3 vertexNormal;" \

				"uniform mat4 u_modelMatrix;" \
				"uniform mat4 u_viewMatrix;" \
				"uniform mat4 u_projection_matrix;" \

				"uniform vec3 u_La_Red;" \
				"uniform vec3 u_Ld_Red;" \
				"uniform vec3 u_Ls_Red;" \
				"uniform vec4 u_Light_Position_Red;" \

				"uniform vec3 u_La_Green;" \
				"uniform vec3 u_Ld_Green;" \
				"uniform vec3 u_Ls_Green;" \
				"uniform vec4 u_Light_Position_Green;" \

				"uniform vec3 u_La_Blue;" \
				"uniform vec3 u_Ld_Blue;" \
				"uniform vec3 u_Ls_Blue;" \
				"uniform vec4 u_Light_Position_Blue;" \

				"uniform vec3 u_Ka;" \
				"uniform vec3 u_Kd;" \
				"uniform vec3 u_Ks;" \

				"uniform float u_materialShininess;" \
				"uniform int u_lKeyIsPressed;" \

				"out vec3 phong_AdsLight;" \

				"void main(void)" \
				"{" \
					"if(u_lKeyIsPressed == 1)" \
					"{" \
						"vec4 eye_coordinate = u_viewMatrix * u_modelMatrix * vertexPosition;" \

						"vec3 tnorm = normalize(mat3(u_viewMatrix * u_modelMatrix) * vertexNormal);" \

						"vec3 viewerVector = normalize(vec3(-eye_coordinate.xyz));" \

						//Red Light
						"vec3 lightdirection_Red = normalize(vec3(u_Light_Position_Red - eye_coordinate));" \

						"float tn_dot_lightDir_Red = max(dot(lightdirection_Red, tnorm), 0.0);" \

						"vec3 reflectionVector_Red = reflect(-lightdirection_Red, tnorm);" \

						"vec3 ambient_Red = u_La_Red * u_Ka;" \

						"vec3 diffuse_Red = u_Ld_Red * u_Kd * tn_dot_lightDir_Red;" \

						"vec3 specular_Red = u_Ls_Red * u_Ks * pow(max(dot(reflectionVector_Red, viewerVector), 0.0), u_materialShininess);" \

						//For Green
						"vec3 lightdirection_Green = normalize(vec3(u_Light_Position_Green - eye_coordinate));" \

						"float tn_dot_lightDir_Green = max(dot(lightdirection_Green, tnorm), 0.0);" \

						"vec3 reflectionVector_Green = reflect(-lightdirection_Green, tnorm);" \

						"vec3 ambient_Green = u_La_Green * u_Ka;" \

						"vec3 diffuse_Green = u_Ld_Green * u_Kd * tn_dot_lightDir_Green;" \

						"vec3 specular_Green = u_Ls_Green * u_Ks * pow(max(dot(reflectionVector_Green, viewerVector), 0.0), u_materialShininess);" \

						//For Green
						"vec3 lightdirection_Blue = normalize(vec3(u_Light_Position_Blue - eye_coordinate));" \

						"float tn_dot_lightDir_Blue = max(dot(lightdirection_Blue, tnorm), 0.0);" \

						"vec3 reflectionVector_Blue = reflect(-lightdirection_Blue, tnorm);" \

						"vec3 ambient_Blue = u_La_Blue * u_Ka;" \

						"vec3 diffuse_Blue = u_Ld_Blue * u_Kd * tn_dot_lightDir_Blue;" \

						"vec3 specular_Blue = u_Ls_Blue * u_Ks * pow(max(dot(reflectionVector_Blue, viewerVector), 0.0), u_materialShininess);" \

					"phong_AdsLight = ambient_Red + diffuse_Red + specular_Red + ambient_Green + diffuse_Green + specular_Green + ambient_Blue + diffuse_Blue + specular_Blue;" \

				"}" \
				"else" \
				"{" \
						"phong_AdsLight = vec3(1.0, 1.0, 1.0);" \
				"}" \
				"gl_Position = u_projection_matrix * u_viewMatrix * u_modelMatrix * vertexPosition;" \
			"}";

	//"mat3 normalmatrix = mat3(transpose(inverse(u_mv_matrix)))" \   OR "mat3 normalmatrix = mat3(u_mv_matrix);" \

	//Specifing above source to the vertex shader object
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//Compile the Vertex Shader
	glCompileShader(vertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("vertexShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode =
				"#version 450 core" \
				"\n" \
				"in vec3 phong_AdsLight;" \
				"out vec4 fragColor;" \
				"void main(void)" \
				"{" \
					"fragColor = vec4(phong_AdsLight, 1.0);" \
				"}";


	// Specifing above Source to the fragment Shader Object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//Compile the fragment Shader Object
	glCompileShader(fragmentShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("FragmentShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Create Shader Object
	gShaderProgramObject_PV = glCreateProgram();

	//Attach VertexShader to the Shader Program
	glAttachShader(gShaderProgramObject_PV, vertexShaderObject);

	//Attach fragmentShader to the Shader Program
	glAttachShader(gShaderProgramObject_PV, fragmentShaderObject);

	// Pre Linking Binding to Vertex Attribute
	glBindAttribLocation(gShaderProgramObject_PV, AMC_ATTRIBUTE_POSITION, "vertexPosition");
	glBindAttribLocation(gShaderProgramObject_PV, AMC_ATTRIBUTE_NORMAL, "vertexNormal");

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject_PV);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject_PV, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_PV, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_PV, iInfoLogLength, &written, szInfoLog);
				printf("LinkShaderProgramObject:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Post Linking Retriving Uniform Location
	 modelMatrixUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_modelMatrix");
	 viewMatrixUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_viewMatrix");
	 projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_projection_matrix");

	 LaUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_La_Red");
	 LdUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_Ld_Red");
	 LsUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_Ls_Red");
	 lightPositionUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_Light_Position_Red");

	 LaUniform_Green = glGetUniformLocation(gShaderProgramObject_PV, "u_La_Green");
	 LdUniform_Green = glGetUniformLocation(gShaderProgramObject_PV, "u_Ld_Green");
	 LsUniform_Green = glGetUniformLocation(gShaderProgramObject_PV, "u_Ls_Green");
	 lightPositionUniform_Green = glGetUniformLocation(gShaderProgramObject_PV, "u_Light_Position_Green");

	 LaUniform_Blue = glGetUniformLocation(gShaderProgramObject_PV, "u_La_Blue");
	 LdUniform_Blue = glGetUniformLocation(gShaderProgramObject_PV, "u_Ld_Blue");
	 LsUniform_Blue = glGetUniformLocation(gShaderProgramObject_PV, "u_Ls_Blue");
	 lightPositionUniform_Blue = glGetUniformLocation(gShaderProgramObject_PV, "u_Light_Position_Blue");


	 KaUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_Ka");
	 KdUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_Kd");
	 KsUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_Ks");

	 materialShininessUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_materialShininess");
	 lKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_lKeyIsPressed");
}

void perFragment(void)
{
	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	
	//Define Vertex Shader Object
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar* vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vertexPosition;" \
		"in vec3 vertexNormal;" \

		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projection_matrix;" \

		"uniform int u_lKeyIsPressed;" \
		"uniform vec4 u_Light_Position_Red;" \
		"uniform vec4 u_Light_Position_Green;" \
		"uniform vec4 u_Light_Position_Blue;" \

		"out vec3 tnorm;" \
		"out vec3 lightdirection_Red;" \
		"out vec3 lightdirection_Green;" \
		"out vec3 lightdirection_Blue;" \
		"out vec3 viewerVector;" \

		"void main(void)" \
		"{" \
			"if(u_lKeyIsPressed == 1)" \
			"{" \
				"vec4 eye_coordinate = u_viewMatrix * u_modelMatrix * vertexPosition;" \

				"tnorm = mat3(u_viewMatrix * u_modelMatrix) * vertexNormal;" \

				"viewerVector = vec3(-eye_coordinate.xyz);" \

				//For Red Color
				"lightdirection_Red = vec3(u_Light_Position_Red - eye_coordinate);" \

				//For Green Color
				"lightdirection_Green = vec3(u_Light_Position_Green - eye_coordinate);" \

				//For Green Color
				"lightdirection_Blue = vec3(u_Light_Position_Blue - eye_coordinate);" \


			"}" \

		"gl_Position = u_projection_matrix * u_viewMatrix * u_modelMatrix * vertexPosition;" \
		"}";

	
	//Specifing above source to the vertex shader object
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//Compile the Vertex Shader
	glCompileShader(vertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("vertexShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 tnorm;" \
		"in vec3 viewerVector;" \
		"in vec3 lightdirection_Red;" \
		"in vec3 lightdirection_Green;" \
		"in vec3 lightdirection_Blue;" \

		"uniform vec3 u_La_Red;" \
		"uniform vec3 u_Ld_Red;" \
		"uniform vec3 u_Ls_Red;" \

		"uniform vec3 u_La_Green;" \
		"uniform vec3 u_Ld_Green;" \
		"uniform vec3 u_Ls_Green;" \

		"uniform vec3 u_La_Blue;" \
		"uniform vec3 u_Ld_Blue;" \
		"uniform vec3 u_Ls_Blue;" \

		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \

		"uniform float u_materialShininess;" \
		"uniform int u_lKeyIsPressed;" \

		"out vec4 fragColor;" \

		"void main(void)" \
		"{" \
			"vec3 phong_AdsLight;" \
			"if(u_lKeyIsPressed == 1)" \
			"{" \

				"vec3 normalize_tnorm = normalize(tnorm);" \

				"vec3 normalize_viewerVector = normalize(viewerVector);" \

				//For Red
				"vec3 normalize_lightdirection_Red = normalize(lightdirection_Red);" \

				"vec3 reflectionVector_Red = reflect(-normalize_lightdirection_Red, normalize_tnorm);" \

				"float tn_dot_lightDir_Red = max(dot(normalize_lightdirection_Red, normalize_tnorm), 0.0);" \

				"vec3 ambient_Red = u_La_Red * u_Ka;" \

				"vec3 diffuse_Red = u_Ld_Red * u_Kd * tn_dot_lightDir_Red;" \

				"vec3 specular_Red = u_Ls_Red * u_Ks * pow(max(dot(reflectionVector_Red, normalize_viewerVector), 0.0), u_materialShininess);" \

				//For Green Color
				"vec3 normalize_lightdirection_Green = normalize(lightdirection_Green);" \

				"vec3 reflectionVector_Green = reflect(-normalize_lightdirection_Green, normalize_tnorm);" \

				"float tn_dot_lightDir_Green = max(dot(normalize_lightdirection_Green, normalize_tnorm), 0.0);" \

				"vec3 ambient_Green = u_La_Green * u_Ka;" \

				"vec3 diffuse_Green = u_Ld_Green * u_Kd * tn_dot_lightDir_Green;" \

				"vec3 specular_Green = u_Ls_Green * u_Ks * pow(max(dot(reflectionVector_Green, normalize_viewerVector), 0.0), u_materialShininess);" \

				//For Green Color
				"vec3 normalize_lightdirection_Blue = normalize(lightdirection_Blue);" \

				"vec3 reflectionVector_Blue = reflect(-normalize_lightdirection_Blue, normalize_tnorm);" \

				"float tn_dot_lightDir_Blue = max(dot(normalize_lightdirection_Blue, normalize_tnorm), 0.0);" \

				"vec3 ambient_Blue = u_La_Blue * u_Ka;" \

				"vec3 diffuse_Blue = u_Ld_Blue * u_Kd * tn_dot_lightDir_Blue;" \

				"vec3 specular_Blue = u_Ls_Blue * u_Ks * pow(max(dot(reflectionVector_Blue, normalize_viewerVector), 0.0), u_materialShininess);" \

				"phong_AdsLight = ambient_Red + diffuse_Red + specular_Red + ambient_Green + diffuse_Green + specular_Green + ambient_Blue + diffuse_Blue + specular_Blue;" \

			"}" \
			"else" \
			"{" \

				"phong_AdsLight = vec3(1.0, 1.0, 1.0);" \

			"}" \

			"fragColor = vec4(phong_AdsLight, 1.0);" \
		"}";

	// Specifing above Source to the fragment Shader Object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//Compile the fragment Shader Object
	glCompileShader(fragmentShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("FragmentShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Create Shader Object
	gShaderProgramObject_PF = glCreateProgram();

	//Attach VertexShader to the Shader Program
	glAttachShader(gShaderProgramObject_PF, vertexShaderObject);

	//Attach fragmentShader to the Shader Program
	glAttachShader(gShaderProgramObject_PF, fragmentShaderObject);

	// Pre Linking Binding to Vertex Attribute
	glBindAttribLocation(gShaderProgramObject_PF, AMC_ATTRIBUTE_POSITION, "vertexPosition");
	glBindAttribLocation(gShaderProgramObject_PF, AMC_ATTRIBUTE_NORMAL, "vertexNormal");

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject_PF);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject_PF, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_PF, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_PF, iInfoLogLength, &written, szInfoLog);
				printf("LinkShaderProgramObject:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Post Linking Retriving Uniform Location
	modelMatrixUniform = glGetUniformLocation(gShaderProgramObject_PF, "u_modelMatrix");
	viewMatrixUniform = glGetUniformLocation(gShaderProgramObject_PF, "u_viewMatrix");
	projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject_PF, "u_projection_matrix");

	LaUniform_Red = glGetUniformLocation(gShaderProgramObject_PF, "u_La_Red");
	LdUniform_Red = glGetUniformLocation(gShaderProgramObject_PF, "u_Ld_Red");
	LsUniform_Red = glGetUniformLocation(gShaderProgramObject_PF, "u_Ls_Red");
	lightPositionUniform_Red = glGetUniformLocation(gShaderProgramObject_PF, "u_Light_Position_Red");

	LaUniform_Green = glGetUniformLocation(gShaderProgramObject_PF, "u_La_Green");
	LdUniform_Green = glGetUniformLocation(gShaderProgramObject_PF, "u_Ld_Green");
	LsUniform_Green = glGetUniformLocation(gShaderProgramObject_PF, "u_Ls_Green");
	lightPositionUniform_Green = glGetUniformLocation(gShaderProgramObject_PF, "u_Light_Position_Green");

	LaUniform_Blue = glGetUniformLocation(gShaderProgramObject_PF, "u_La_Blue");
	LdUniform_Blue = glGetUniformLocation(gShaderProgramObject_PF, "u_Ld_Blue");
	LsUniform_Blue = glGetUniformLocation(gShaderProgramObject_PF, "u_Ls_Blue");
	lightPositionUniform_Blue = glGetUniformLocation(gShaderProgramObject_PF, "u_Light_Position_Blue");

	KaUniform = glGetUniformLocation(gShaderProgramObject_PF, "u_Ka");
	KdUniform = glGetUniformLocation(gShaderProgramObject_PF, "u_Kd");
	KsUniform = glGetUniformLocation(gShaderProgramObject_PF, "u_Ks");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject_PF, "u_materialShininess");
	lKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject_PF, "u_lKeyIsPressed");
}
