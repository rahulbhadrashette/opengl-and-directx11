#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#include<string.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

//OpenGL Related Files
#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>  //In Windows(Wgl) 

#include"vmath.h"

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTCOORD0
};


//namespaces
using namespace std;
using namespace vmath;

// global variable Declaration
static GLXContext gLXContext;
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;

FILE *gpFile;

//Programmable Pipeline Variable
GLuint gShaderProgramObject;

GLuint vao_I;
GLuint vbo_Position_I;
GLuint vbo_Color_I;

GLuint vao_N;
GLuint vbo_Position_N;
GLuint vbo_Color_N;

GLuint vao_D;
GLuint vbo_Position_D;
GLuint vbo_Color_D;

GLuint vao_A_TriBand;
GLuint vbo_Position_A_TriBand;
GLuint vbo_Color_A_TriBand;

GLuint vao_A;
GLuint vbo_Position_A;
GLuint vbo_Color_A;
GLuint mvpUniform;
vmath::mat4 perspectiveProjectionMatrix;

//Entry-Point Function
int main(void)
{
	//Function Declaration
	void Resize_RMB(int, int);
	void Initialize_RMB(void);
	void Display_RMB(void);
	void CreateWindow_RMB(void);
	void ToggleFullScreen_RMB(void);
	void uninitialize_RMB(void);

	gpFile = fopen("OpenGL_Log.txt", "w");
	if(gpFile == NULL)
	{
		printf("Error in Opening Log File...!\nExitting Now....!!!");
		return(EXIT_FAILURE);
	}

	// Local Variable Declaration
	bool bDone = false;
	char keys[26];
	static int winWidth = giWindowWidth;
	static int winHeight = giWindowHeight;

	//Code
	CreateWindow_RMB();
	Initialize_RMB();


	XEvent event;
	KeySym keysym;

	fprintf(gpFile, "Entering into Game Loop...!!!\n");
	//Message Loop
	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					 //All System Fonts  or  fixed pitch font
				break;

				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							bDone = true;
							break;
					}

					XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
					switch(keys[0])
					{
					
					}
					break;

				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:   	 //Left Mouse Button
							break;
						case 2:  	 //Middle Mouse Button
							break;
						case 3:  	 //Right Mouse Button
							break;
						case 4:  	 //Wheel Up
							break;
						case 5:		 //Wheel Down
							break;
						default:
							break;
					}
					break;

				case MotionNotify:	 //WM_MOUSEMOVE:
					break;

				case ConfigureNotify://WM_SIZE:
					  winWidth = event.xconfigure.width;
					  winHeight = event.xconfigure.height;
					  Resize_RMB(winWidth, winHeight);
					break;

				case Expose:		//WM_PAINT:
					break;

				case DestroyNotify: //WM_DESTROY:
					break;

				case 33:
				     bDone = true;
				    break;

				default:
				     break;
			}
		}
		//Display Call Here And Update also
		Display_RMB();
	}

        uninitialize_RMB();
	return(0);
}

void CreateWindow_RMB(void)
{
	//Function prototype declaration
	void uninitialize_RMB(void);

	//variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	GLXFBConfig *pGLXFBConfig = NULL;   //This is Array of FBConfig
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumberOfFBConfig = 0;

	static int frameBufferAttributes[] = { 
						  	GLX_X_RENDERABLE, True,
						  	GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
						  	GLX_RENDER_TYPE, GLX_RGBA_BIT,
						  	GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
							GLX_RED_SIZE, 8,
					       	GLX_GREEN_SIZE, 8,
					      	GLX_BLUE_SIZE, 8,
					      	GLX_ALPHA_SIZE, 8,
					      	GLX_DEPTH_SIZE, 24,
					      	GLX_STENCIL_SIZE, 8,
					      	GLX_DOUBLEBUFFER, True,
					      	None
					    	};

	//Code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	//defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	pGLXFBConfig = glXChooseFBConfig(gpDisplay,  defaultScreen, frameBufferAttributes, &iNumberOfFBConfig);
	printf("%d There are Matching FBConfig.\n", iNumberOfFBConfig);
	if(pGLXFBConfig == NULL)
	{
		printf("ERROR: Unable To pGLXFBConfig. \nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}
	//Inline variable
	int bestFBConfig = -1;
	int bestNumberOfSamples = -1;
	int worstFBConfig = -1;
	int worstNumberOfSamples = 999;
	for(int i = 0; i<iNumberOfFBConfig; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfig[i]);

		if(pTempXVisualInfo)
		{
			int sampleBuffers;
			int samples;
			//Get number of sampleBuffers from respective FBConfig[i]
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);

			//Get number of samples from respective FBConfig[i]
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLES, &samples);

			if(bestFBConfig < 0 | sampleBuffers && samples > bestNumberOfSamples)
			{
				bestFBConfig = i;
				bestNumberOfSamples = samples;
			}

			if(worstFBConfig < 0 | !sampleBuffers | samples < worstNumberOfSamples)
			{
				worstFBConfig = i;
				worstNumberOfSamples = samples;
			}
		}
		XFree(pTempXVisualInfo);
	}

	bestGLXFBConfig = pGLXFBConfig[bestFBConfig];

	gGLXFBConfig = bestGLXFBConfig;
	XFree(pGLXFBConfig) ;

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, bestGLXFBConfig);

	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);

	if(!gWindow)
	{
		printf("ERROE : Failed To Create Main Window.\nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "12-StaticIndia...Rahul.U.M.Bhadrashette...!!!");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);
}

void Initialize_RMB(void)
{

	GLenum result;
	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	void Resize_RMB(int, int);
	void uninitialize_RMB(void);
	void ToggleFullScreen_RMB();

	ToggleFullScreen_RMB();

	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

	if(glXCreateContextAttribsARB == NULL)
	{
		printf("ERROE : Failed To glXCreateContextAttribsARB.\nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	const int Attribs[] = {
							GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
							GLX_CONTEXT_MINOR_VERSION_ARB, 5,
							GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
							None
							};

	gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, Attribs);
	if(!gGLXContext)
	{
		const int Attribs[] = {
								GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
								GLX_CONTEXT_MINOR_VERSION_ARB, 0,
								None
								};
	}

	gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, Attribs);

	if(!glXIsDirect(gpDisplay, gGLXContext))
	{
		printf("Optined Context is Not H/W Rendering Context....!\n");
	}
	else
	{
		printf("Optined Context is H/W Rendering Context....!\n");
	}


	//void uninitialize(void);
	//gLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, True);
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

	result = glewInit();
	if (result != GLEW_OK)
	{
		printf("glewInit(); is failed.\n");
		uninitialize_RMB();
		exit(1);
	}

	//Define Vertex Shader Object
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_Color;" \
		"void main(void)" \
		"{" \
			"gl_Position = u_mvp_matrix * vPosition;" \
			"out_Color = vColor;" \
		"}";

	//Specifing above source to the vertex shader object
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//Compile the Vertex Shader
	glCompileShader(vertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "vertexShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 out_Color;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
			"fragColor = out_Color;" \
		"}";


	// Specifing above Source to the fragment Shader Object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//Compile the fragment Shader Object
	glCompileShader(fragmentShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "FragmentShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Create Shader Object
	gShaderProgramObject = glCreateProgram();

	//Attach VertexShader to the Shader Program
	glAttachShader(gShaderProgramObject, vertexShaderObject);

	//Attach fragmentShader to the Shader Program
	glAttachShader(gShaderProgramObject, fragmentShaderObject);

	// Pre Linking Binding to Vertex Attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "LinkShaderProgramObject:- %s", szInfoLog);
				free(szInfoLog);
				uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Post Linking Retriving Uniform Location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	
	//I Vertices Array
	const GLfloat I_Verties[] =
									{
										-1.0f, 1.0f, 0.0f,
										-1.0f, -1.0f, 0.0f
									};
	//I Color Array
	const GLfloat I_Color[] = 
									{
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f
									};

	//N Vertices Array
	const GLfloat N_Verties[] =
									{
										-1.0f, 1.0f, 0.0f,
										-1.0f, -1.0f, 0.0f,
										-1.0f, 1.0f, 0.0f,
										-0.2f, -1.0f, 0.0f,
										-0.2f, 1.0f, 0.0f,
										-0.2f, -1.0f, 0.0f
									};
	// N Color Array
	const GLfloat N_Color[] =
									{
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f,
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f,
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f
									};

	//D Vertices Array
	const GLfloat D_Verties[] =
									{
										-1.0f, 0.97f, 0.0f,
										-1.0f, -0.97f, 0.0f,
										-1.2f, 0.98f, 0.0f,
										-0.2f, 0.98f, 0.0f,
										-1.2f, -0.98f, 0.0f,
										-0.2f, -0.98f, 0.0f,
										-0.2f, 1.0f, 0.0f,
										-0.2f, -1.0f, 0.0f
									};
	//D Color Array
	const GLfloat D_Color[] = 
									{
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f,
										1.0f,0.4f,0.0f,
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f,
										0.0f, 1.0f, 0.0f,
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f
									};

	//A Vertices Array
	const GLfloat A_TriBand_Verties[] =
									{
										-0.03f, -0.045f, 0.0f,
										-0.57f, -0.045f, 0.0f,
										-0.03f, -0.08f, 0.0f,
										-0.57f, -0.08f, 0.0f,
										0.0f, -0.12f, 0.0f,
										- 0.56f, -0.12f, 0.0f
									};
	//A Color Array
	const GLfloat A_TriBand_Color[] =
									{
										1.0f,0.5f,0.0f,
										1.0f,0.5f,0.0f,
										1.0f, 1.0f, 1.0f,
										1.0f, 1.0f, 1.0f,
										0.0f, 1.0f, 0.0f,
										0.0f, 1.0f, 0.0f,
									};

	//A Vertices Array
	const GLfloat A_Verties[] =
									{
										-0.4f, 1.0f, 0.0f,
										0.20f, -1.0f, 0.0f,
										-0.4f, 1.0f, 0.0f,
										-0.80f, -1.0f, 0.0f
									};
	//A Color Array
	const GLfloat A_Color[] =
									{
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f,
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f,
									};
	

	//Create vao
	glGenVertexArrays(1, &vao_I);
	glBindVertexArray(vao_I);
	glGenBuffers(1, &vbo_Position_I);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_I);
	glBufferData(GL_ARRAY_BUFFER, sizeof(I_Verties), I_Verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_I);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_I);
	glBufferData(GL_ARRAY_BUFFER, sizeof(I_Color), I_Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	//Create vao
	glGenVertexArrays(1, &vao_N);
	glBindVertexArray(vao_N);
	glGenBuffers(1, &vbo_Position_N);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_N);
	glBufferData(GL_ARRAY_BUFFER, sizeof(N_Verties), N_Verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_N);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_N);
	glBufferData(GL_ARRAY_BUFFER, sizeof(N_Color), N_Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	//Create vao
	glGenVertexArrays(1, &vao_D);
	glBindVertexArray(vao_D);
	glGenBuffers(1, &vbo_Position_D);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_D);
	glBufferData(GL_ARRAY_BUFFER, sizeof(D_Verties), D_Verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_D);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_D);
	glBufferData(GL_ARRAY_BUFFER, sizeof(D_Color), D_Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	//Create vao
	glGenVertexArrays(1, &vao_A_TriBand);
	glBindVertexArray(vao_A_TriBand);
	glGenBuffers(1, &vbo_Position_A_TriBand);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_A_TriBand);
	glBufferData(GL_ARRAY_BUFFER, sizeof(A_TriBand_Verties), A_TriBand_Verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_A_TriBand);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_A_TriBand);
	glBufferData(GL_ARRAY_BUFFER, sizeof(A_TriBand_Color), A_TriBand_Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	//Create vao
	glGenVertexArrays(1, &vao_A);
	glBindVertexArray(vao_A);
	glGenBuffers(1, &vbo_Position_A);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_A);
	glBufferData(GL_ARRAY_BUFFER, sizeof(A_Verties), A_Verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_A);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_A);
	glBufferData(GL_ARRAY_BUFFER, sizeof(A_Color), A_Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);


	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	perspectiveProjectionMatrix = vmath::mat4::identity();
	//Warmup Call To Resize
	Resize_RMB(giWindowWidth, giWindowHeight);
}

void Resize_RMB(int Width, int Height)
{
	//code
	if (Height == 0)
	{
		Height = 1;
	}
	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)Width/(GLfloat)Height , 0.1f, 100.0f);
}

void Display_RMB(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	//Declatation of Matricex
	vmath::mat4 translationMatrix;
	vmath::mat4 modelViewMatrix;
	vmath::mat4 modelViewProjectionMatrix;
	
	//For First I_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(-0.9f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glLineWidth(10.0f);
	//BindWith vao
	glBindVertexArray(vao_I);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 2);
	//UnBind vao
	glBindVertexArray(0);

	//For N_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	
	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(-0.6f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	//BindWith vao
	glBindVertexArray(vao_N);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 6);
	//UnBind vao
	glBindVertexArray(0);

	//For D_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	
	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.64f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	
	//BindWith vao
	glBindVertexArray(vao_D);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 8);
	//UnBind vao
	glBindVertexArray(0);

	//For Second I_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(1.75f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	
	//BindWith vao
	glBindVertexArray(vao_I);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 2);
	//UnBind vao
	glBindVertexArray(0);

	//For A_TriBand_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(1.8f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	
	//BindWith vao
	glBindVertexArray(vao_A_TriBand);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 6);
	//UnBind vao
	glBindVertexArray(0);

	//For A_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(1.85f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	
	//BindWith vao
	glBindVertexArray(vao_A);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 6);
	//UnBind vao
	glBindVertexArray(0);

	//UnUse Program
	glUseProgram(0);

	glXSwapBuffers(gpDisplay, gWindow);
}

void uninitialize_RMB(void)
{
	GLXContext CurrentgLXContext;

	//code
	if (vbo_Color_A_TriBand)
	{
		glDeleteBuffers(1, &vbo_Color_A_TriBand);
		vbo_Color_A_TriBand = 0;
	}
	if (vbo_Position_A_TriBand)
	{
		glDeleteBuffers(1, &vbo_Position_A_TriBand);
		vbo_Position_A_TriBand = 0;
	}
	if (vao_A_TriBand)
	{
		glDeleteVertexArrays(1, &vao_A_TriBand);
		vao_A_TriBand = 0;
	}

	if (vbo_Color_A)
	{
		glDeleteBuffers(1, &vbo_Color_A);
		vbo_Color_A = 0;
	}
	if (vbo_Position_A)
	{
		glDeleteBuffers(1, &vbo_Position_A);
		vbo_Position_A = 0;
	}
	if (vao_A)
	{
		glDeleteVertexArrays(1, &vao_A);
		vao_A = 0;
	}

	if (vbo_Color_D)
	{
		glDeleteBuffers(1, &vbo_Color_D);
		vbo_Color_D = 0;
	}
	if (vbo_Position_D)
	{
		glDeleteBuffers(1, &vbo_Position_D);
		vbo_Position_D = 0;
	}
	if (vao_D)
	{
		glDeleteVertexArrays(1, &vao_D);
		vao_D = 0;
	}

	if (vbo_Color_N)
	{
		glDeleteBuffers(1, &vbo_Color_N);
		vbo_Color_N = 0;
	}
	if (vbo_Position_N)
	{
		glDeleteBuffers(1, &vbo_Position_N);
		vbo_Position_N = 0;
	}
	if (vao_N)
	{
		glDeleteVertexArrays(1, &vao_N);
		vao_N = 0;
	}

	if (vbo_Color_I)
	{
		glDeleteBuffers(1, &vbo_Color_I);
		vbo_Color_I = 0;
	}
	if (vbo_Position_I)
	{
		glDeleteBuffers(1, &vbo_Position_I);
		vbo_Position_I = 0;
	}
	if (vao_I)
	{
		glDeleteVertexArrays(1, &vao_I);
		vao_I = 0;
	}

	
	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);
		//Ask Program How Many Shaders are Attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	CurrentgLXContext = glXGetCurrentContext();
	if(CurrentgLXContext != NULL && CurrentgLXContext == gLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);	
	}

	if(gGLXFBConfig)
	{
		XFree(gGLXFBConfig);
	}
	
	if(gLXContext)
	{
		glXDestroyContext(gpDisplay, gLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);  //as memory might get allocated inside choose visual
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}

void ToggleFullScreen_RMB(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), False, StructureNotifyMask, &xev);
}


