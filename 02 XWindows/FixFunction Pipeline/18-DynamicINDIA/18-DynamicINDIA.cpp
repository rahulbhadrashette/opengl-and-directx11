#include<iostream>
#include<stdio.h>
#define _USE_MATH_DEFINES 1
#include<stdlib.h>
#include<memory.h>
#include<string.h>
#include<math.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

//OpenGL Related Files
#include<GL/gl.h>
#include<GL/glx.h>  //In Windows(Wgl) 
#include<GL/glu.h>

//namespaces
using namespace std;

// global variable Declaration
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
static GLXContext gLXContext;

//staticIndia Flags
bool IFlag1 = true;
bool NFlag = true;
bool DFlag = true;
bool IFlag2 = true; 
bool AFlag = true;
bool RepaintINDIA = false;
bool Tri_Band_Left = false;
bool Left_Planes_NOTRepaint = false;
bool Tri_Band_Right = false;


//Entry-Point Function
int main(void)
{
	//Function Declaration
	void Resize_RMB(int, int);
	void Initialize_RMB(void);
	void Display_RMB(void);
	void CreateWindow_RMB(void);
	void ToggleFullScreen_RMB(void);
	void uninitialize_RMB(void);
	void Update_RMB(void);

	// Local Variable Declaration
	bool bDone = false;
	char keys[26];
	static int winWidth = giWindowWidth;
	static int winHeight = giWindowHeight;

	//Code
	CreateWindow_RMB();
	Initialize_RMB();

	XEvent event;
	KeySym keysym;

	//Message Loop
	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					 //All System Fonts  or  fixed pitch font
				break;

				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							  bDone = true;
							break;
					}

					/*XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
					switch(keys[0])
					{
					
						case 'F':
						case 'f':
							if(bFullScreen == false)
							{
								ToggleFullScreen_RMB();
								bFullScreen = true;
							}
							else
							{
								ToggleFullScreen_RMB();
								bFullScreen = false;
							}
							break;
					
					}
					break;*/

				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:   	 //Left Mouse Button
							break;
						case 2:  	 //Middle Mouse Button
							break;
						case 3:  	 //Right Mouse Button
							break;
						case 4:  	 //Wheel Up
							break;
						case 5:		 //Wheel Down
							break;
						default:
							break;
					}
					break;

				case MotionNotify:	 //WM_MOUSEMOVE:
					break;

				case ConfigureNotify://WM_SIZE:
						winWidth = event.xconfigure.width;
						winHeight = event.xconfigure.height;
						Resize_RMB(winWidth, winHeight);
					break;

				case Expose:		//WM_PAINT:
						break;

				case DestroyNotify: //WM_DESTROY:
					break;

				case 33:
					bDone = true;
				default:
					break;
			}
		}
		//Display Call Here And Update also
		ToggleFullScreen_RMB();
		Update_RMB();
		Display_RMB();
	}

	uninitialize_RMB();
	return(0);
}

void CreateWindow_RMB(void)
{
	//Function prototype declaration
	void uninitialize_RMB(void);

	//variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	static int frameBufferAttributes[] = { GLX_RGBA, 
						   GLX_DOUBLEBUFFER, True,
					       GLX_RED_SIZE, 8,
					       GLX_GREEN_SIZE, 8,
					       GLX_BLUE_SIZE, 8,
					       GLX_ALPHA_SIZE, 8,
					       GLX_DEPTH_SIZE,24,
					       None};

	//Code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	//defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR: Unable To gpXVisualInfo. \nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);

	if(!gWindow)
	{
		printf("ERROE : Failed To Create Main Window.\nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "09-2D-ShapesOnBlackBackground...RahulUMB...!!!");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);
}

void Initialize_RMB(void)
{
	void Resize_RMB(int, int);

	//void uninitialize(void);
	gLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, True);
	glXMakeCurrent(gpDisplay, gWindow, gLXContext);


	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	Resize_RMB(giWindowWidth, giWindowHeight);
}

void Resize_RMB(int Width, int Height)
{
	if(Height == 0)
	{
		Height = 1;
	}
	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);
}

 GLfloat PrintEnd;
 GLfloat PosX2 = 0.0f;
 GLfloat PosX1 = 4.6f;
 GLfloat posy = 3.5f;
 GLfloat Nigy = -3.5f;
 GLfloat Nigx = -3.0f;
 GLfloat Color1 = 0.0f, Color05 = 0.0f;
 GLfloat x2 = -2.5f;
 GLfloat PosX3 = -2.8f;
 GLfloat Keshari1 = 1.0f, Keshari2 = 0.5f;

 GLfloat Smoke3 = 0.0f;
 GLfloat Smoke2 = 0.0f;
 GLfloat Smoke1 = -1.8f;
 GLfloat Smoke4 = -1.8f;

 GLfloat Translate_Angle_1 = 180.8;
 GLfloat Rotate_Angles_1 = 0;
 GLfloat k1,l1;

 GLfloat Translate_Angle2 = 179.53;
 GLfloat Rotate_Angles2 = 270;
 GLfloat k2 , l2;

 GLfloat Translate_Angle_3 =  178.61;
 GLfloat Rotate_Angles_3 = 90;
 GLfloat k3 , l3;

 GLfloat Translate_Angle_4 =  89.38;
 GLfloat Rotate_Angles_4 = 360;
 GLfloat k4 , l4;

void Display_RMB(void)
{
	//Local Functions Declaration	
	void I1st_Code_RMB(void);
	void A_Code_RMB(void);
	void A_Tri_Code_RMB(void);
	void N_Code_RMB(void);
	void I2nd_code_RMB(void);
	void D_Code_RMB(void);
	void PLANE_Code_RMB(void);
	void PLANE_SMOKE_Code_RMB(void);
	void Left_PLANE_SMOKE_Curve_RMB(void);
	void Right_PLANE_SMOKE_Curve_RMB(void);
	
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	// I
	glLoadIdentity();
	glTranslatef(Nigx, 0.0f, -5.0f);
	glLineWidth(35.0f);
	I1st_Code_RMB();

	//A
	glLoadIdentity();
	glTranslatef(PosX1, 0.0f, -5.0f);
	glLineWidth(35.0f);
	A_Code_RMB();
		
	// N
	glLoadIdentity();
	glTranslatef(-0.6f, posy, -5.0f);
	glLineWidth(35.0f);
	N_Code_RMB();

	// I
	glLoadIdentity();
	glTranslatef(1.75f, Nigy, -5.0f);
	glLineWidth(35.0f);
	I2nd_code_RMB();

	//D
	glLoadIdentity();
	glTranslatef(0.64f, 0.0f, -5.0f);
	glLineWidth(35.0f);
	D_Code_RMB();

	//PLANE Curve Code
	if(Tri_Band_Left == true)
	{
		//Smoke Curve Code
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -4.0f);
		glPointSize(6.0f);
		Left_PLANE_SMOKE_Curve_RMB();

		if(Left_Planes_NOTRepaint == true)
		{
			glLoadIdentity();
			glColor3f(0.7265625f, 0.8828125f, 0.9296875f);
			glTranslatef(k2, l2, -3.0f);
			glRotatef(Rotate_Angles2,0.0f,0.0f,1.0f);
			PLANE_Code_RMB();

			glLoadIdentity();
			glColor3f(0.7265625f, 0.8828125f, 0.9296875f);
			glTranslatef(k3, l3, -3.0f);
			glRotatef(Rotate_Angles_3,0.0f,0.0f,1.0f);
			PLANE_Code_RMB();
		}
	}

	if(Tri_Band_Right == true)
	{
		//Smoke Curve Code
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -4.0f);
		glPointSize(6.0f);
		Right_PLANE_SMOKE_Curve_RMB();

		glLoadIdentity();
		glColor3f(0.7265625f, 0.8828125f, 0.9296875f);
		glTranslatef(k1, l1, -3.0f);
		glRotatef(Rotate_Angles_1,0.0f,0.0f,1.0f);
		PLANE_Code_RMB();

		glLoadIdentity();
		glColor3f(0.7265625f, 0.8828125f, 0.9296875f);
		glTranslatef(k4, l4, -3.0f);
		glRotatef(Rotate_Angles_4,0.0f,0.0f,1.0f);
		PLANE_Code_RMB();
	}	

	//Plane Code
	glLoadIdentity();
	glTranslatef(x2, -0.05f, -3.0f);
	glColor3f(0.7265625f, 0.8828125f, 0.9296875f);
	PLANE_Code_RMB();
		
	//PLANE After Tri band Code
	glLoadIdentity();
	glTranslatef(PosX3, 0.02f, -3.0f);
	glLineWidth(10.0f);
	PLANE_SMOKE_Code_RMB();

	if(RepaintINDIA == true)
	{
		// I
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(Nigx, 0.0f, -5.0f);
		glLineWidth(35.0f);
		I1st_Code_RMB();

		// A
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(PosX2, 0.0f, -3.0f);
		glLineWidth(10.0f);
		A_Tri_Code_RMB();

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(PosX1, 0.0f, -5.0f);
		glLineWidth(35.0f);
		A_Code_RMB();
		
		// N
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.6f, posy, -5.0f);
		glLineWidth(35.0f);
		N_Code_RMB();

		// I
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.75f, Nigy, -5.0f);
		glLineWidth(35.0f);
		I2nd_code_RMB();

		//D
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.64f, 0.0f, -5.0f);
		glLineWidth(35.0f);
		D_Code_RMB();
	}

	glXSwapBuffers(gpDisplay, gWindow);
}

void uninitialize_RMB(void)
{
	GLXContext CurrentgLXContext;

	glXGetCurrentContext();
	if(CurrentgLXContext != NULL && CurrentgLXContext == gLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);	
	}

	if(gLXContext)
	{
		glXDestroyContext(gpDisplay, gLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);  //as memory might get allocated inside choose visual
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}

void ToggleFullScreen_RMB(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), False, StructureNotifyMask, &xev);
}

void I1st_Code_RMB(void)
{
	//I_1
	glBegin(GL_LINES);
	glColor3f(1.0f,0.5f,0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glColor3f( 0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	glEnd();
}

void A_Tri_Code_RMB(void)
{
	glBegin(GL_LINES);

	glColor3f(Color1,Color05,0.0f);
	glVertex3f(-0.14f, -0.025f, 0.0f);
	glVertex3f(-0.45f, -0.025f, 0.0f);

	glColor3f(Color1, Color1, Color1);
	glVertex3f(-0.13f, -0.042f, 0.0f);
	glVertex3f(-0.46f, -0.042f, 0.0f);
	
	glColor3f( 0.0f, Color1, 0.0f);
	glVertex3f(-0.13f, -0.059f, 0.0f);
	glVertex3f(-0.46f, -0.059f, 0.0f);

	glEnd();
}

void A_Code_RMB(void)
{
	//A
	glBegin(GL_LINES);

	glColor3f(1.0f,0.5f,0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glColor3f( 0.0f, 1.0f, 0.0f);
	glVertex3f(0.20f, -1.0f, 0.0f);

	glColor3f(1.0f,0.5f,0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glColor3f( 0.0f, 1.0f, 0.0f);
	glVertex3f(-0.80f, -1.0f, 0.0f);
	glEnd();
}

void N_Code_RMB(void)
{
	//N	
	glBegin(GL_LINES);
	glColor3f(1.0f,0.5f,0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glColor3f( 0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);

	glColor3f(1.0f,0.5f,0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glColor3f( 0.0f, 1.0f, 0.0f);
	glVertex3f(-0.2f, -1.0f, 0.0f);

	glColor3f(1.0f,0.5f,0.0f);
	glVertex3f(-0.2f, 1.0f, 0.0f);
	glColor3f( 0.0f, 1.0f, 0.0f);
	glVertex3f(-0.2f, -1.0f, 0.0f);
	glEnd();
}

void I2nd_code_RMB(void)
{
	//I_2
	glBegin(GL_LINES);
	glColor3f(1.0f,0.5f,0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glColor3f( 0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	glEnd();
}

void D_Code_RMB(void)
{
	//D
	glBegin(GL_LINES);

	glColor3f(Color1,Color05,0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);

	glColor3f( 0.0f, Color1, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);

	glColor3f(Color1,Color05,0.0f);
	glVertex3f(-1.2f, 0.985f, 0.0f);
	glVertex3f(-0.2f, 0.985f, 0.0f);

	glColor3f( 0.0f, Color1, 0.0f);
	glVertex3f(-1.2f, -0.985f, 0.0f);
	glVertex3f(-0.2f, -0.985f, 0.0f);

	glColor3f(Color1,Color05,0.0f);
	glVertex3f(-0.2f, 1.0f, 0.0f);

	glColor3f( 0.0f, Color1, 0.0f);
	glVertex3f(-0.2f, -1.0f, 0.0f);

	glEnd();
}

void PLANE_Code_RMB(void)
{
	glBegin(GL_TRIANGLES);
	glVertex3f(0.0f, 0.08f, 0.0f);
	glVertex3f(0.0f, -0.08f, 0.0f);
	glVertex3f(0.08f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(0.0f, 0.08f, 0.0f);
	glVertex3f(-0.28f, 0.08f, 0.0f);
	glVertex3f(-0.28f, -0.08f, 0.0f);
	glVertex3f(0.0f, -0.08f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glVertex3f(-0.24f, 0.0f, 0.0f);
	glVertex3f(-0.28f, 0.17f, 0.0f);
	glVertex3f(-0.28f, -0.17f, 0.0f);
	glEnd();
		
	glBegin(GL_TRIANGLES);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glVertex3f(-0.2f, 0.18f, 0.0f);
	glVertex3f(-0.2f, -0.18f, 0.0f);
	glEnd();

	glColor3f(1.0f, 1.0f, 1.0f);
	glLineWidth(5.0f);
		//I
	glBegin(GL_LINES);
	glVertex3f(-0.2f, 0.05f, 0.0f);
	glVertex3f(-0.2f, -0.05f, 0.0f);
	glEnd();
		//A
	glBegin(GL_LINES);
	glVertex3f(-0.14f, 0.05f, 0.0f);
	glVertex3f(-0.18f, -0.05f, 0.0f);
	glVertex3f(-0.14f, 0.05f, 0.0f);
	glVertex3f(-0.1f, -0.05f, 0.0f);
	glVertex3f(-0.12f, 0.0f, 0.0f);
	glVertex3f(-0.16f, 0.0f, 0.0f);
	glEnd();
		//F
	glBegin(GL_LINES);
	glVertex3f(-0.08f, 0.05f, 0.0f);
	glVertex3f(-0.08f, -0.05f, 0.0f);
	glVertex3f(-0.08f, 0.042f, 0.0f);
	glVertex3f(-0.02f, 0.042f, 0.0f);
	glVertex3f(-0.08f, 0.0f, 0.0f);
	glVertex3f(-0.04f, 0.0f, 0.0f);
		
	glEnd();
}

void PLANE_SMOKE_Code_RMB(void)
{
	glBegin(GL_LINES);
	glColor3f(Keshari1,Keshari2,0.0f);
	glVertex3f(-0.0f, -0.045f, 0.0f);
	glVertex3f(-7.3f, -0.045f, 0.0f);

	glColor3f(Keshari1, Keshari1, Keshari1);
	glVertex3f(-0.0f, -0.063f, 0.0f);
	glVertex3f(-7.3f, -0.063f, 0.0f);
	
	glColor3f( 0.0f, Keshari1, 0.0f);
	glVertex3f(-0.0f, -0.080f, 0.0f);
	glVertex3f(-7.3f, -0.080f, 0.0f);
	glEnd();
}

void Left_PLANE_SMOKE_Curve_RMB(void)
{
	//Left Top
	glBegin(GL_POINTS);
	for (GLfloat Angle = 0.0f; Angle < Smoke2 * M_PI / 4.0f ; Angle = Angle + 0.001f)
	{
		glColor3f( 0.0f, Keshari1, 0.0f);
		glVertex3f(-cos(Angle) * 1.7f - 1.2f, -sin(Angle) * 1.8f + 1.67f, 0.0f);

		glColor3f(Keshari1, Keshari1, Keshari1);
		glVertex3f(-cos(Angle) * 1.675f - 1.2f, -sin(Angle) * 1.775f + 1.67f, 0.0f);

		glColor3f(Keshari1,Keshari2,0.0f);
		glVertex3f(-cos(Angle) * 1.65f - 1.2f, -sin(Angle) * 1.75f + 1.67f, 0.0f);
	}
	glEnd();

	//Left Bottom
	glBegin(GL_POINTS);
	for (GLfloat Angle = 0.0f; Angle < Smoke3 * M_PI / 4.0f ; Angle = Angle + 0.001f)
	{
		glColor3f(Keshari1,Keshari2,0.0f);
		glVertex3f(-cos(Angle) * 1.7f - 1.2f, sin(Angle) * 1.8f - 1.8f, 0.0f);

		glColor3f(Keshari1, Keshari1, Keshari1);
		glVertex3f(-cos(Angle) * 1.675f - 1.2f, sin(Angle) * 1.775f - 1.8f, 0.0f);

		glColor3f( 0.0f, Keshari1, 0.0f);
		glVertex3f(-cos(Angle) * 1.65f - 1.2f, sin(Angle) * 1.75f - 1.8f, 0.0f);
	}
	glEnd();
}

void Right_PLANE_SMOKE_Curve_RMB(void)
{
	//Right Top
	glBegin(GL_POINTS);
	for (GLfloat Angle = -1.4f; Angle < Smoke1 * M_PI / 4.0f ; Angle = Angle + 0.001f)
	{
		glColor3f( 0.0f, Keshari1, 0.0f);
		glVertex3f(cos(Angle) * 1.8f + 1.2f, sin(Angle) * 1.8f + 1.67, 0.0f);

		glColor3f(Keshari1, Keshari1, Keshari1);
		glVertex3f(cos(Angle) * 1.775f + 1.2f, sin(Angle) * 1.775f + 1.67, 0.0f);

		glColor3f(Keshari1,Keshari2,0.0f);
		glVertex3f(cos(Angle) * 1.75f + 1.2f, sin(Angle) * 1.75f + 1.67, 0.0f);
	}
	glEnd();

	//Right Bottom
	glBegin(GL_POINTS);
	for (GLfloat Angle = -1.4f; Angle < Smoke4 * M_PI / 4.0f ; Angle = Angle + 0.001f)
	{
		glColor3f(Keshari1,Keshari2,0.0f);
		glVertex3f(cos(Angle) * 1.8f + 1.2f, -sin(Angle) * 1.8f - 1.8f, 0.0f);

		glColor3f(Keshari1, Keshari1, Keshari1);
		glVertex3f(cos(Angle) * 1.775f + 1.2f, -sin(Angle) * 1.775f - 1.8f, 0.0f);

		glColor3f( 0.0f, Keshari1, 0.0f);
		glVertex3f(cos(Angle) * 1.75f +1.2, -sin(Angle) * 1.75f - 1.8f, 0.0f);
	}
	glEnd();

}

void Update_RMB(void)
{

	if(IFlag1 == true)
	{
	//I
		Nigx = Nigx + 0.03f;
		if(Nigx >= -0.9f)
		{
			Nigx = -0.9f;
			PrintEnd = 1.0f;
		}
	}

	if(AFlag == true && PrintEnd == 1.0f)
	{
		//A  (-)
		PosX2 = PosX2 - 0.03f;
		if (PosX2 <= 1.2f)
		{
			PosX2 = 1.2f;
		}

		//A (/\)
		PosX1 = PosX1 - 0.03f;
		if (PosX1 <= 1.85f)
		{
				PosX1 = 1.85f;
				PrintEnd = 2.0f;
		}
	}

	if (NFlag == true && PrintEnd == 2.0f)
	{
		//N
		posy = posy - 0.03f;

		if(posy <= 0.0f)
		{
			posy = 0.0f;
			PrintEnd = 3.0f;
		}
	}

	if(IFlag2 == true && PrintEnd == 3.0f)
	{
		
		//I
		Nigy = Nigy + 0.03f;
		if(Nigy >= 0.0f)
		{
			Nigy = 0.0f;
			PrintEnd = 4.0f;
		}
	}

	if(DFlag == true && PrintEnd == 4.0f)
	{ 
		//D
		Color1 = Color1 + 0.03f;
		Color05 = Color05 + 0.03f;
		if(Color1 >= 1.0f)
		{
			Color1 = 1.0f;
			Color05 = 0.5f;
			PrintEnd = 5.0f;
		}
	}

	if(PrintEnd == 5.0f)
	{
		//PLANE
		x2 = x2 + 0.002565f;
		if(x2 >= 2.6f)
		{
			x2 = 2.6f;
		}

		if(x2 >= -2.48f)
		{
			Tri_Band_Left = true;
			Left_Planes_NOTRepaint = true;
		}

		//Left Top Curve PLANE 2
		k2 = 1.8f * cos(Translate_Angle2) - 0.6f;
		l2 = 1.85f * sin(Translate_Angle2) + 1.75f;
		if(Translate_Angle2 >= 180.51f)
		{
			Translate_Angle2 = 180.51f;
		}
		Translate_Angle2 = Translate_Angle2 + 0.0015f;

		if(Rotate_Angles2 >= 360)
		{
			Rotate_Angles2 = 360;
		}
		Rotate_Angles2 = Rotate_Angles2 + 0.158;

		//2
		Smoke2 = Smoke2 + 0.0025f;
		if (Smoke2 >= 1.76f)
		{
			Smoke2 = 1.76f;
		}

		//Left Bottom Curve PLANE 3
		k3 = 1.8f * cos(Translate_Angle_3) - 0.6f;
		l3 = 1.85f * sin(Translate_Angle_3) - 1.85f;

		if(Translate_Angle_3 < 177.7f)
		{
			Translate_Angle_3 = 177.7f;
			Left_Planes_NOTRepaint = false;
		}
		Translate_Angle_3 = Translate_Angle_3 - 0.0015f;
	
		if(Rotate_Angles_3 < 0)
		{
			Rotate_Angles_3 = 0;
		}
		Rotate_Angles_3 = Rotate_Angles_3 - 0.158;

		Smoke3 = Smoke3 + 0.0025f;
		if (Smoke3 >= 1.77f)
		{
			Smoke3 = 1.77f;
		}

		if(x2 >= 1.2f)
		{
			//Right Top Curve PLANE 1
			Tri_Band_Right = true;

			k1 = 1.3 * cos(Translate_Angle_1) + 1.0f;
			l1 = 1.66 * sin(Translate_Angle_1) + 1.585f;
			if(Translate_Angle_1 >= 182.18)
			{
				Translate_Angle_1 = 182.18;
			}
			Translate_Angle_1 = Translate_Angle_1 + 0.0022f;

			if(Rotate_Angles_1 >= 75)
			{
				Rotate_Angles_1 = 75;
			}
			Rotate_Angles_1 = Rotate_Angles_1 + 0.148f;
	
			Smoke1 = Smoke1 + 0.0028f;
			if (Smoke1 >= 0.0f)
			{
				Smoke1 = 0.0f;
			}


			//Right Bottom Curve PLANE 4
			k4 = 1.3 * cos(Translate_Angle_4) + 1.0f;
			l4 = 1.66 * sin(Translate_Angle_4) - 1.69f;

			if(Translate_Angle_4 <= 88.00f)
			{
				Translate_Angle_4 = 88.00f;
			}
			Translate_Angle_4 = Translate_Angle_4 - 0.0022f;
	
			if(Rotate_Angles_4 <= 270)
			{
				Rotate_Angles_4 = 270;
			}
			Rotate_Angles_4 = Rotate_Angles_4 - 0.148f;

			Smoke4 = Smoke4 + 0.0028f;
			if (Smoke4 >= 0.0f)
			{
				Smoke4 = 0.0f;
			}
		}

	}

	if(PrintEnd == 5.0f)
	{
		//SMOKE Code
		PosX3 = PosX3 + 0.002565f;
		if (PosX3 >= 2.6f)
		{
			PosX3 = 2.6f;
			PrintEnd = 6.0f;
		}
		if(PrintEnd == 6.0f)
		{
			Keshari1 = Keshari1 - 0.0055f;
			Keshari2 = Keshari2 - 0.0055f;
			if(Keshari1 >= 1.0f)
			{
				Keshari1 = 0.0f;
				Keshari2 = 0.0f;
			}
			RepaintINDIA = true;
		}

	}

}

