#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#include<string.h>
#include<math.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

//OpenGL Related Files
#include<GL/gl.h>
#include<GL/glx.h>  //In Windows(Wgl) 
#include<GL/glu.h>
#include<SOIL/SOIL.h>

//namespaces
using namespace std;

// global variable Declaration
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
static GLXContext gLXContext;

//staticIndia Flags
bool NFlag = true;
bool DFlag = true;
bool IFlag = true;
bool AFlag = true;


//Entry-Point Function
int main(void)
{
	//Function Declaration
	void Resize_RMB(int, int);
	void Initialize_RMB(void);
	void Display_RMB(void);
	void CreateWindow_RMB(void);
	void ToggleFullScreen_RMB(void);
	void uninitialize_RMB(void);

	// Local Variable Declaration
	bool bDone = false;
	char keys[26];
	static int winWidth = giWindowWidth;
	static int winHeight = giWindowHeight;

	//Code
	CreateWindow_RMB();
	Initialize_RMB();

	XEvent event;
	KeySym keysym;

	//Message Loop
	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					 //All System Fonts  or  fixed pitch font
				break;

				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							  bDone = true;
							break;
					}

					/*XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
					switch(keys[0])
					{
					
						case 'F':
						case 'f':
							if(bFullScreen == false)
							{
								ToggleFullScreen_RMB();
								bFullScreen = true;
							}
							else
							{
								ToggleFullScreen_RMB();
								bFullScreen = false;
							}
							break;
					
					}
					break;*/

				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:   	 //Left Mouse Button
							break;
						case 2:  	 //Middle Mouse Button
							break;
						case 3:  	 //Right Mouse Button
							break;
						case 4:  	 //Wheel Up
							break;
						case 5:		 //Wheel Down
							break;
						default:
							break;
					}
					break;

				case MotionNotify:	 //WM_MOUSEMOVE:
					break;

				case ConfigureNotify://WM_SIZE:
						winWidth = event.xconfigure.width;
						winHeight = event.xconfigure.height;
						Resize_RMB(winWidth, winHeight);
					break;

				case Expose:		//WM_PAINT:
						break;

				case DestroyNotify: //WM_DESTROY:
					break;

				case 33:
					bDone = true;
				default:
					break;
			}
		}
		//Display Call Here And Update also
		ToggleFullScreen_RMB();
		Display_RMB();
	}

	uninitialize_RMB();
	return(0);
}

void CreateWindow_RMB(void)
{
	//Function prototype declaration
	void uninitialize_RMB(void);

	//variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	static int frameBufferAttributes[] = { GLX_RGBA, 
						   GLX_DOUBLEBUFFER, True,
					       GLX_RED_SIZE, 8,
					       GLX_GREEN_SIZE, 8,
					       GLX_BLUE_SIZE, 8,
					       GLX_ALPHA_SIZE, 8,
					       GLX_DEPTH_SIZE,24,
					       None};

	//Code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	//defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR: Unable To gpXVisualInfo. \nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);

	if(!gWindow)
	{
		printf("ERROE : Failed To Create Main Window.\nExitting Now...!!!\n");
		uninitialize_RMB();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "09-2D-ShapesOnBlackBackground...RahulUMB...!!!");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);
}

void Initialize_RMB(void)
{
	void Resize_RMB(int, int);

	//void uninitialize(void);
	gLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, True);
	glXMakeCurrent(gpDisplay, gWindow, gLXContext);


	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	Resize_RMB(giWindowWidth, giWindowHeight);
}

void Resize_RMB(int Width, int Height)
{
	if(Height == 0)
	{
		Height = 1;
	}
	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);
}

void Display_RMB(void)
{
	static GLfloat PrintEnd;

	glClear(GL_COLOR_BUFFER_BIT);
	
	// I
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.9f, 0.0f, -5.0f);
		glLineWidth(10.0f);
		glBegin(GL_LINES);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glEnd();

		// N
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.6f, 0.0f, -5.0f);
		glLineWidth(35.0f);
		glBegin(GL_LINES);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-0.2f, -1.0f, 0.0f);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-0.2f, 1.0f, 0.0f);
		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-0.2f, -1.0f, 0.0f);
		glEnd();

		if(DFlag == true && PrintEnd == 4.0f)
	
		//D
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.64f, 0.0f, -5.0f);
		glLineWidth(35.0f);
		glBegin(GL_LINES);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);

		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-1.2f, 0.985f, 0.0f);
		glVertex3f(-0.2f, 0.985f, 0.0f);

		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-1.2f, -0.985f, 0.0f);
		glVertex3f(-0.2f, -0.985f, 0.0f);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-0.2f, 1.0f, 0.0f);

		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-0.2f, -1.0f, 0.0f);
		glEnd();


		// I
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.75f, 0.0f, -5.0f);
		glLineWidth(35.0f);
		glBegin(GL_LINES);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glEnd();

		//A Tri Band
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.8f, 0.0f, -5.0f);
		glLineWidth(10.0f);
		glBegin(GL_LINES);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-0.04f, -0.045f, 0.0f);
		glVertex3f(-0.57f, -0.045f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.03f, -0.07f, 0.0f);
		glVertex3f(-0.578f, -0.07f, 0.0f);
	
		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-0.01f, -0.1f, 0.0f);
		glVertex3f(-0.578f, -0.1f, 0.0f);
		glEnd();
		//A
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.85f, 0.0f, -5.0f);
		glLineWidth(35.0f);
		glBegin(GL_LINES);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-0.4f, 1.0f, 0.0f);
		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(0.20f, -1.0f, 0.0f);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-0.4f, 1.0f, 0.0f);
		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-0.80f, -1.0f, 0.0f);
		glEnd();

	

	glXSwapBuffers(gpDisplay, gWindow);
}

void uninitialize_RMB(void)
{
	GLXContext CurrentgLXContext;

	glXGetCurrentContext();
	if(CurrentgLXContext != NULL && CurrentgLXContext == gLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);	
	}

	if(gLXContext)
	{
		glXDestroyContext(gpDisplay, gLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);  //as memory might get allocated inside choose visual
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}

void ToggleFullScreen_RMB(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), False, StructureNotifyMask, &xev);
}

