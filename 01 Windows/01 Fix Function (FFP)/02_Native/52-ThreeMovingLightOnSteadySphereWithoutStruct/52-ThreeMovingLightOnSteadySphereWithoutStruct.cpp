//Header Files
#include<windows.h>
#include<stdio.h>
#include<gl/GL.h>
#include<gl/GLU.h>

//Libraries
#pragma comment( lib, "OpenGL32.lib" )
#pragma comment( lib, "GLU32.lib" )
#pragma comment( lib, "user32.lib" )
#pragma comment( lib, "gdi32.lib" )
#pragma comment( lib, "kernel32.lib" )

//user define
#define WIN_WIDTH   800
#define WIN_HEIGHT  600

//Global Variable Declaration
HWND ghwnd = NULL;
DWORD dwStyle;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
FILE *gpFile = NULL;
GLfloat LightAmbientZero[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat LightDiffuseZero[] = {1.0f, 0.0f, 0.0f, 1.0f};
GLfloat LightSpecularZero[] = {1.0f, 0.0f, 0.0f, 1.0f};
GLfloat LightPositionZero[] = /*{100.0f, 100.0f, 100.0f, 1.0f};*/ {0.0f, 0.0f, 0.0f, 1.0f};

GLfloat LightAmbientOne[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat LightDiffuseOne[] = {0.0f, 1.0f, 0.0f, 1.0f};
GLfloat LightSpecularOne[] = {0.0f, 1.0f, 0.0f, 1.0f};
GLfloat LightPositionOne[] = /*{100.0f, 100.0f, 100.0f, 1.0f};*/ {0.0f, 0.0f, 0.0f, 1.0f};

GLfloat LightAmbientTwo[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat LightDiffuseTwo[] = {0.0f, 0.0f, 1.0f, 1.0f};
GLfloat LightSpecularTwo[] = {0.0f, 0.0f, 1.0f, 1.0f};
GLfloat LightPositionTwo[] = /*{100.0f, 100.0f, 100.0f, 1.0f};*/ {0.0f, 0.0f, 0.0f, 1.0f};

GLfloat MaterialAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat MaterialDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat MaterialSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat MaterialShininess[] =/* {50.0f};   */ {128.0f};
GLUquadric *quadric = NULL;
bool gbActiveWindow = false;
bool bIsFullScreen = false;
bool pLight = false;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
GLfloat LightAngleZero = 0.0f;
GLfloat LightAngleOne = 0.0f;
GLfloat LightAngleTwo  = 0.0f;
void Update(void);


//Golbal Function Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function prototype
	int Initialize_RMB(void);
	void Display_RMB(void);

	//Local Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("NativeOpenGLWindow");
	bool bDone = false;
	int iRet = 0;
	int X, Y;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log file is successfully complited\n");
	}

	//Wndclass Initialization
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//class register
	RegisterClassEx(&wndclass);

	//CenterOfTheWindowCalculation
	X = (GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2);
	Y = (GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2);


	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName, TEXT("Rahul.M.Bhadrashette...52-ThreeMovingLightOnSteadySphereWithoutStruct...!!!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X,
		Y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);


	ghwnd = hwnd;
	iRet = Initialize_RMB();
	if (iRet == -1)
	{
		fprintf_s(gpFile, " ChoosePixelFormat Failed. \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "SetPixelFormat is Failed. \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, " wglCreateContext Failed. \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "wglMakeCurrent Failed. \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "Initialize Successfully. \n");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//GameLoop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				Update(); 
			}
			Display_RMB();
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration(signature)
	void ToggleFullScreen(void);
	void Resize_RMB(int, int);
	void Display_RMB(void);
	void Uninitialize_RMB(void);

	switch (iMsg)
	{
		//code
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		Resize_RMB(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)//in past switch(LOWORD(lParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'f':
		case 'F':
			ToggleFullScreen();
			break;

		case 'L':
		case 'l':
			if(pLight == false)
			{
				pLight = true;
				glEnable(GL_LIGHTING);
			}
			else
			{
				pLight = false;
				glDisable(GL_LIGHTING);
			}

		}
		break;

	case WM_DESTROY:
		Uninitialize_RMB();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}



int Initialize_RMB(void)
{
	//function declaration
	void Resize_RMB(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	//initialization pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	//All Depth Related tests
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbientZero);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuseZero);
	glLightfv(GL_LIGHT0, GL_SPECULAR, LightSpecularZero);
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbientOne);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuseOne);
	glLightfv(GL_LIGHT1, GL_SPECULAR, LightSpecularOne);
	glEnable(GL_LIGHT1);
	glLightfv(GL_LIGHT2, GL_AMBIENT, LightAmbientTwo);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, LightDiffuseTwo);
	glLightfv(GL_LIGHT2, GL_SPECULAR, LightSpecularTwo);
	glEnable(GL_LIGHT2);
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);
	glMaterialfv(GL_FRONT,GL_DIFFUSE, MaterialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);
	
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	Resize_RMB(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

void Resize_RMB(int Width, int Height)
{
	if (Height == 0)
	{
		Height = 1;
	}

	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);
}

static GLfloat Angle = 0.0f;

void Display_RMB(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
	gluLookAt(0.0f, 0.0f, 3.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	glPushMatrix();      //First Light
	glRotatef(LightAngleZero, 1.0f, 0.0f, 0.0f);
	LightPositionZero[1] = LightAngleZero;
	glLightfv(GL_LIGHT0, GL_POSITION, LightPositionZero);
	glPopMatrix();

	glPushMatrix();   //
	glRotatef(LightAngleOne, 0.0f, 1.0f, 0.0f);
	LightPositionOne[0] = LightAngleOne;
	glLightfv(GL_LIGHT1, GL_POSITION, LightPositionOne);
	glPopMatrix();


	glPushMatrix();
	glRotatef(LightAngleTwo, 0.0f, 0.0f, 1.0f);
	LightPositionTwo[0] = LightAngleTwo;
	glLightfv(GL_LIGHT2, GL_POSITION, LightPositionTwo);
	glPopMatrix();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30.0f, 30.0f);
	glPopMatrix();

	SwapBuffers(ghdc);
	
}

void Uninitialize_RMB(void)
{
	if (bIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf_s(gpFile, "Log file is closed Successfully.\n");
		fclose(gpFile);
		gpFile = NULL;
	}

	if(quadric)
	{
		gluDeleteQuadric(quadric);
	}
}



void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(false);
		bIsFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bIsFullScreen = false;
	}

}

void Update(void)
{
	LightAngleZero = LightAngleZero + 0.1f;
	LightAngleOne = LightAngleOne + 0.1f;
	LightAngleTwo = LightAngleTwo + 0.1f;
}

