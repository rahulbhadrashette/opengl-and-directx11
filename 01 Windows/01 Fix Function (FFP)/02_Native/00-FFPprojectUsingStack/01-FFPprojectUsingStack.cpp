//Header Files
#include<windows.h>
#include<stdio.h>
#include<gl/GL.h>
#include<gl/GLU.h>

//Libraries
#pragma comment( lib, "OpenGL32.lib" )
#pragma comment( lib, "GLU32.lib" )
#pragma comment( lib, "user32.lib" )
#pragma comment( lib, "gdi32.lib" )
#pragma comment( lib, "kernel32.lib" )

//user define
#define WIN_WIDTH   800
#define WIN_HEIGHT  600

//Global Variable Declaration
HWND ghwnd = NULL;
DWORD dwStyle;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
FILE *gpFile = NULL;
bool gbActiveWindow = false;
bool bIsFullScreen = false;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

GLfloat LightAmbient[] = { 0.5f, 0.5f, 0.5f, 1.0f };
GLfloat LightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat LightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };
bool pLight = false;
GLUquadric* quadric = NULL;
static GLfloat theta[1] = { 0.0f};

bool aKeyIsPressed = false;
static bool xPos = false;
static bool zPos = false;
static bool xnPos = false;
static bool znPos = false;
static bool xpPos = false;
bool walking = false;

GLfloat xpos = 0.0f;
GLfloat zpos = 0.0f;
GLfloat xNpos = 0.0f;
GLfloat zNpos = 0.0f;
GLfloat xPpos = zNpos;
float walkingSpeed = 0.002f;

float gShoulde_L = 0.0f;
float gElbow_L = 0.0f;
float gLeg_L = 0.0f;

float gESpeed = 0.15f;
float gSpeed = 0.15f;
float trans = -5.0f;

//Golbal Function Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function prototype
	int Initialize_RMB(void);
	void Display_RMB(void);
	void Update(void);

	//Local Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("NativeOpenGLWindow");
	bool bDone = false;
	int iRet = 0;
	int X, Y;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log file is successfully complited\n");
	}

	//Wndclass Initialization
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//class register
	RegisterClassEx(&wndclass);

	//CenterOfTheWindowCalculation
	X = (GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2);
	Y = (GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2);


	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName, TEXT("Rahul.M.Bhadrashette...48-LightOnCube...!!!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X,
		Y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);


	ghwnd = hwnd;
	iRet = Initialize_RMB();
	if (iRet == -1)
	{
		fprintf_s(gpFile, " ChoosePixelFormat Failed. \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "SetPixelFormat is Failed. \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, " wglCreateContext Failed. \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "wglMakeCurrent Failed. \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "Initialize Successfully. \n");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//GameLoop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				Update(); 
			}
			Display_RMB();
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration(signature)
	void Resize_RMB(int, int);
	void Display_RMB(void);
	void Uninitialize_RMB(void);

	switch (iMsg)
	{
		//code
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;

		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;

		case WM_SIZE:
			Resize_RMB(LOWORD(lParam), HIWORD(lParam));
			break;

		case WM_ERASEBKGND:
			return(0);

		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;

		case WM_KEYDOWN:
			switch (wParam)//in past switch(LOWORD(lParam))
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;

				case VK_LEFT:
					theta[0] = theta[0] + 5.0f;
					break;

				case VK_RIGHT:
					theta[0] = theta[0] - 5.0f;
					break;
			}
			break;

		case WM_CHAR:
			switch (wParam)
			{
				case 'L':
				case 'l':
					if(pLight == false)
					{
						pLight = true;
						glEnable(GL_LIGHTING);
					}
					else
					{
						pLight = false;
						glDisable(GL_LIGHTING);
					}

				case 'W':
				case 'w':
					xPos = true;
					walking = true;
					break;

				case 'S':
				case 's':
					xPos = false;
					walking = false;
					break;
			}
			break;

		case WM_DESTROY:
			Uninitialize_RMB();
			PostQuitMessage(0);
			break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize_RMB(void)
{
	//function declaration
	void Resize_RMB(int, int);
	void ToggleFullScreen(void);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	//initialization pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);
	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);
	glEnable(GL_LIGHT0);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	Resize_RMB(WIN_WIDTH, WIN_HEIGHT);
	ToggleFullScreen();
	return(0);
}

void Resize_RMB(int Width, int Height)
{
	if (Height == 0)
	{
		Height = 1;
	}

	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);
}

void Display_RMB(void)
{
	void toras(void);
	void headFunc(void);
	void leftUpperArm(void);
	void leftLowerArm(void);
	void rightUpperArm(void);
	void rightLowerArm(void);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glColor3f(0.5f, 0.5f, 0.5f);
	glTranslatef(0.0f, 0.0f, -5.0f);

	if (xPos == true)
	{
		glTranslatef(-xpos, 0.0f, 0.0f);
		glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
	}
	if (zPos == true)
	{
		glTranslatef(zpos, 0.0f, 0.0f);
		glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
	}
	if (xnPos == true)
	{
		glTranslatef(-xNpos, 0.0f, 0.0f);
		glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
	}
	if (znPos == true)
	{
		glTranslatef(zNpos, 0.0f, 0.0f);
		glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
	}
	if (xpPos == true)
	{
		glTranslatef(xPpos, 0.0f, 0.0f);
		glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
	}
	glPushMatrix();

	//toras
	glTranslatef(0.0f, 0.0f, 0.0f);
	glRotatef(theta[0], 0.0f, 1.0f, 0.0f);
	toras();
	glPushMatrix();

	//head
	glPushMatrix();
	glTranslatef(0.0f, 0.7f, 0.0f);
	headFunc();
	glPopMatrix();

	//Left Upper Arm
	glPushMatrix();
	glTranslatef(-0.35f, 0.4f, 0.0f);
	glRotatef((GLfloat)gShoulde_L, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, -0.25f, 0.0f);
	leftUpperArm();

	//Left Lower Arm
	glTranslatef(0.0f, -0.25f, 0.0f);
	glRotatef((GLfloat)gElbow_L, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, -0.25f, 0.0f);
	leftLowerArm();
	glPopMatrix();

	//Right Upper Arm
	glPushMatrix();
	glTranslatef(0.35f, 0.4f, 0.0f);
	glRotatef((GLfloat)-gShoulde_L, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, -0.25f, 0.0f);
	rightUpperArm();

	//Right Lower Arm
	glTranslatef(0.0f, -0.25f, 0.0f);
	glRotatef((GLfloat)gElbow_L, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, -0.25f, 0.0f);
	rightLowerArm();
	glPopMatrix();

	//Left Upper Arm
	glPushMatrix();
	glTranslatef(-0.1f, -0.5f, 0.0f);
	glRotatef((GLfloat)gShoulde_L, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, -0.25f, 0.0f);
	leftUpperArm();

	//Left Lower Arm
	glTranslatef(0.0f, -0.25f, 0.0f);
	glTranslatef(0.0f, -0.25f, 0.0f);
	leftLowerArm();
	glPopMatrix();

	//Right Upper Arm
	glPushMatrix();
	glTranslatef(0.1f, -0.5f, 0.0f);
	glRotatef((GLfloat)-gShoulde_L, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, -0.25f, 0.0f);
	rightUpperArm();

	//Right Lower Arm
	glTranslatef(0.0f, -0.25f, 0.0f);
	glTranslatef(0.0f, -0.25f, 0.0f);
	rightLowerArm();
	glPopMatrix();

	glPopMatrix();

	SwapBuffers(ghdc);
}

void Uninitialize_RMB(void)
{
	if (bIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf_s(gpFile, "Log file is closed Successfully.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(false);
		bIsFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bIsFullScreen = false;
	}

}

void Update(void)
{
	static bool arm_L = true;
	static bool elbow_L = true;
	static bool leg_L = true;

	if (walking == true)
	{
		if (arm_L)
		{
			gShoulde_L = gShoulde_L + gSpeed;
			if (gShoulde_L >= 40.0f)
			{
				arm_L = false;
			}
		}
		else
		{
			gShoulde_L = gShoulde_L - gSpeed;
			if (gShoulde_L <= -50.0f)
			{
				arm_L = true;
			}
		}

		if (elbow_L)
		{
			gElbow_L = gElbow_L + gESpeed;
			if (gElbow_L >= 0.01f)
			{
				elbow_L = false;
			}
		}
		else
		{
			gElbow_L = gElbow_L - gESpeed;
			if (gElbow_L <= -25.0f)
			{
				elbow_L = true;
			}
		}

		if (xPos == true)
		{
			xpos = xpos + walkingSpeed;
			if (xpos >= 2.0f)
			{
				zPos = true;
				xpos = 2.0f;
			}
		}

		if (zPos == true)
		{
			zpos = zpos - walkingSpeed;
			if (zpos <= -2.0f)
			{
				xnPos = true;
				zpos = -2.0f;
			}
		}

		if (xnPos == true)
		{
			xNpos = xNpos + walkingSpeed;
			if (xNpos >= 4.0f)
			{
				znPos = true;
				xNpos = 4.0f;
			}
		}

		if (znPos == true)
		{
			zNpos = zNpos - walkingSpeed;
			if (zNpos <= -2.0f)
			{
				xpPos = true;
				zNpos = -2.0f;
			}
		}

		if (xpPos == true)
		{
			xPpos = xPpos - walkingSpeed;
			if (xPpos <= -2.0f)
			{

				xPos = false;
				zPos = false;
				xnPos = false;
				znPos = false;
				xpPos = false;

				xpos = 0.0f;
				zpos = 0.0f;
				xNpos = 0.0f;
				zNpos = 0.0f;
				xPpos = zNpos;
			}
		}
	}
}

void toras(void)
{
	
	glPushMatrix();
	glColor3f(1.0f, 0.0f, 0.0f);
	glScalef(0.5f, 1.0f, 0.3f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.5, 30, 30);
	glPopMatrix();
}

void headFunc(void)
{
	glPushMatrix();
	glColor3f(0.25f, 0.25f, 0.25f);
	glScalef(0.3f, 0.3f, 0.3f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.5, 30, 30);
	glPopMatrix();
}

void leftUpperArm(void)
{
	glPushMatrix();
	glColor3f(1.0f, 1.0f, 0.0f);
	glScalef(0.15f, 0.5f, 0.15f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.5, 30, 30);
	glPopMatrix();
}

void leftLowerArm(void)
{
	glPushMatrix();
	glScalef(0.15f, 0.5f, 0.15f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.5, 30, 30);
	glPopMatrix();
}

void rightUpperArm(void)
{
	//Right Upper Arm
	glPushMatrix();
	glScalef(0.15f, 0.5f, 0.15f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.5, 30, 30);
	glPopMatrix();
}

void rightLowerArm(void)
{
	//Right Lower Arm
	glPushMatrix();
	glScalef(0.15f, 0.5f, 0.15f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.5, 30, 30);
	glPopMatrix();
}
