//Header Files
#include<windows.h>
#include<stdio.h>
#define _USE_MATH_DEFINES 1
#include<math.h>
#include<gl/GL.h>
#include<gl/GLU.h>

//Libraries
#pragma comment( lib, "user32.lib" )
#pragma comment( lib, "gdi32.lib" )
#pragma comment( lib, "kernel32.lib" )
#pragma comment( lib, "OpenGL32.lib" )
#pragma comment( lib, "GLU32.lib" )

//user define
#define WIN_WIDTH   800
#define WIN_HEIGHT  600

//Global Variable Declaration
HWND ghwnd = NULL;
DWORD dwStyle;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
FILE *gpFile = NULL;
bool gbActiveWindow = false;
bool bIsFullScreen = true;
bool NFlag = true;
bool DFlag = true;
bool IFlag2 = true; 
bool AFlag = true;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
void ToggleFullScreen(void);

//Golbal Function Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function prototype
	int Initialize(void);
	void Display(void);

	//Local Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("NativeOpenGLWindow");
	bool bDone = false;
	int iRet = 0;
	int X, Y;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log file is successfully complited\n");
	}

	//Wndclass Initialization
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//class register
	RegisterClassEx(&wndclass);

	//CenterOfTheWindowCalculation
	X = (GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2);
	Y = (GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2);


	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName, TEXT("Rahul.M.Bhadrashette...34-Static_INDIA...!!!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X,
		Y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);


	ghwnd = hwnd;
	iRet = Initialize();
	if (iRet == -1)
	{
		fprintf_s(gpFile, " ChoosePixelFormat Failed. \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "SetPixelFormat is Failed. \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, " wglCreateContext Failed. \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "wglMakeCurrent Failed. \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "Initialize Successfully. \n");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//GameLoop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//Here 
			}
			Display();
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration(signature)
	void Resize(int, int);
	void Display(void);
	void Uninitialize(void);

	switch (iMsg)
	{
		//code
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)//in past switch(LOWORD(lParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize(void)
{
	//function declaration
	void Resize(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	//initialization pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	Resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

void Resize(int Width, int Height)
{
	if (Height == 0)
	{
		Height = 1;
	}

	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);
}
	
void Display(void)
{
	static GLfloat PrintEnd;
	
	ToggleFullScreen();
	glClear(GL_COLOR_BUFFER_BIT);

		// I
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.9f, 0.0f, -5.0f);
		glLineWidth(35.0f);
		glBegin(GL_LINES);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glEnd();



		// N
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.6f, 0.0f, -5.0f);
		glLineWidth(35.0f);
		glBegin(GL_LINES);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-0.2f, -1.0f, 0.0f);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-0.2f, 1.0f, 0.0f);
		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-0.2f, -1.0f, 0.0f);
		glEnd();

		if(DFlag == true && PrintEnd == 4.0f)
	
		//D
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.64f, 0.0f, -5.0f);
		glLineWidth(35.0f);
		glBegin(GL_LINES);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-1.0f, 0.9f, 0.0f);

		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -0.9f, 0.0f);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-1.2f, 0.93f, 0.0f);
		glVertex3f(-0.2f, 0.93f, 0.0f);

		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-1.2f, -0.93f, 0.0f);
		glVertex3f(-0.2f, -0.93f, 0.0f);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-0.2f, 1.0f, 0.0f);

		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-0.2f, -1.0f, 0.0f);
		glEnd();


		// I
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.75f, 0.0f, -5.0f);
		glLineWidth(35.0f);
		glBegin(GL_LINES);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glEnd();

		//A
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.8f, 0.0f, -5.0f);
		glLineWidth(10.0f);
		glBegin(GL_LINES);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(0.0f, -0.045f, 0.0f);
		glVertex3f(-0.50f, -0.045f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, -0.08f, 0.0f);
		glVertex3f(-0.50f, -0.08f, 0.0f);
	
		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(0.0f, -0.12f, 0.0f);
		glVertex3f(-0.50f, -0.12f, 0.0f);
		glEnd();
			
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.85f, 0.0f, -5.0f);
		glLineWidth(35.0f);
		glBegin(GL_LINES);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-0.4f, 1.0f, 0.0f);
		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(0.20f, -1.0f, 0.0f);

		glColor3f(1.0f,0.5f,0.0f);
		glVertex3f(-0.4f, 1.0f, 0.0f);
		glColor3f( 0.0f, 1.0f, 0.0f);
		glVertex3f(-0.80f, -1.0f, 0.0f);
		glEnd();


	SwapBuffers(ghdc);
}

void Uninitialize(void)
{
	if (bIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf_s(gpFile, "Log file is closed Successfully.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}



void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(false);
		//bIsFullScreen = false;
	}
}

  