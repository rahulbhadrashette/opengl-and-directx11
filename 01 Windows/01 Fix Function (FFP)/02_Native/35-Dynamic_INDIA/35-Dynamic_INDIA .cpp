//Header Files
#include<windows.h>
#include<stdio.h>
#define _USE_MATH_DEFINES 1
#include<math.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include"Song.h"


//Libraries
#pragma comment( lib, "OpenGL32.lib" )
#pragma comment( lib, "GLU32.lib" )
#pragma comment( lib, "user32.lib")
#pragma comment( lib, "gdi32.lib")
#pragma comment( lib, "kernel32.lib")
#pragma comment( lib, "Winmm.lib")

//user define

#define WIN_WIDTH   800
#define WIN_HEIGHT  600

//Global Variable Declaration

HWND ghwnd = NULL;
DWORD dwStyle;
HDC ghdc = NULL;
bool gbActiveWindow = false;
bool bIsFullScreen = true;
bool IFlag1 = true;
bool NFlag = true;
bool DFlag = true;
bool IFlag2 = true; 
bool AFlag = true;
bool RepaintINDIA = false;
bool Tri_Band_Left = false;
bool Left_Planes_NOTRepaint = false;
bool Tri_Band_Right = false;
HGLRC ghrc = NULL;
FILE *gpFile = NULL;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

//Golbal Function Declaration
void Update_RMB(void);
void ToggleFullScreen(void);

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function prototype
	int Initialize(void);
	void Display(void);

	//Local Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("NativeOpenGLWindow");
	bool bDone = false;
	int iRet = 0;
	int X, Y;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log file is successfully complited\n");
	}

	//Wndclass Initialization
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//class register
	RegisterClassEx(&wndclass);

	//CenterOfTheWindowCalculation
	X = (GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2);
	Y = (GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2);


	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName, TEXT("Rahul.M.Bhadrashette...31-Dynamic_INDIA...!!!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X,
		Y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);


	ghwnd = hwnd;
	iRet = Initialize();
	if (iRet == -1)
	{
		fprintf_s(gpFile, " ChoosePixelFormat Failed. \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "SetPixelFormat is Failed. \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, " wglCreateContext Failed. \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "wglMakeCurrent Failed. \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "Initialize Successfully. \n");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//GameLoop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				Update_RMB();//Here Update
			}
			ToggleFullScreen();
			Display();

		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration(signature)
	void Resize(int, int);
	void Display(void);
	void Uninitialize(void);
	//HWAVE Play;

	switch (iMsg)
	{
		//code
		case WM_CREATE:
			 PlaySound( MAKEINTRESOURCE(MY_SONG), NULL, SND_ASYNC | SND_RESOURCE | SND_NODEFAULT);
		break;

		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;

		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;

		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;

		case WM_ERASEBKGND:
			return(0);

		case WM_CLOSE:			
			DestroyWindow(hwnd);
			break;

		case WM_KEYDOWN:
			switch (wParam)//in past switch(LOWORD(lParam))
			{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			}
			break;

		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize(void)
{
	//function declaration
	void Resize(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	//initialization pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	Resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

void Resize(int Width, int Height)
{
	if (Height == 0)
	{
		Height = 1;
	}

	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);
}
	
 GLfloat PrintEnd;
 GLfloat PosX2 = 0.0f;
 GLfloat PosX1 = 4.6f;
 GLfloat posy = 3.5f;
 GLfloat Nigy = -3.5f;
 GLfloat Nigx = -3.0f;
 GLfloat Color1 = 0.0f, Color05 = 0.0f;
 GLfloat x2 = -2.5f;
 GLfloat PosX3 = -2.8f;
 GLfloat Keshari1 = 1.0f, Keshari2 = 0.5f;

 GLfloat Smoke3 = 0.0f;
 GLfloat Smoke2 = 0.0f;
 GLfloat Smoke1 = -1.8f;
 GLfloat Smoke4 = -1.8f;

 GLfloat Translate_Angle_1 = 180.8;
 GLfloat Rotate_Angles_1 = 0;
 GLfloat k1,l1;

 GLfloat Translate_Angle2 = 179.53;
 GLfloat Rotate_Angles2 = 270;
 GLfloat k2 , l2;

 GLfloat Translate_Angle_3 =  178.61;
 GLfloat Rotate_Angles_3 = 90;
 GLfloat k3 , l3;

 GLfloat Translate_Angle_4 =  89.38;
 GLfloat Rotate_Angles_4 = 360;
 GLfloat k4 , l4;


void Display(void)
{
	//Local Functions Declaration	
	void I1st_Code_RMB(void);
	void A_Code_RMB(void);
	void A_Tri_Code_RMB(void);
	void N_Code_RMB(void);
	void I2nd_code_RMB(void);
	void D_Code_RMB(void);
	void PLANE_Code_RMB(void);
	void PLANE_SMOKE_Code_RMB(void);
	void Left_PLANE_SMOKE_Curve_RMB(void);
	void Right_PLANE_SMOKE_Curve_RMB(void);
	
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	// I
	glLoadIdentity();
	glTranslatef(Nigx, 0.0f, -5.0f);
	glLineWidth(35.0f);
	I1st_Code_RMB();

	//A
	glLoadIdentity();
	glTranslatef(PosX1, 0.0f, -5.0f);
	glLineWidth(35.0f);
	A_Code_RMB();
		
	// N
	glLoadIdentity();
	glTranslatef(-0.6f, posy, -5.0f);
	glLineWidth(35.0f);
	N_Code_RMB();

	// I
	glLoadIdentity();
	glTranslatef(1.75f, Nigy, -5.0f);
	glLineWidth(35.0f);
	I2nd_code_RMB();

	//D
	glLoadIdentity();
	glTranslatef(0.64f, 0.0f, -5.0f);
	glLineWidth(35.0f);
	D_Code_RMB();

	//PLANE Curve Code
	if(Tri_Band_Left == true)
	{
		//Smoke Curve Code
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -4.0f);
		glPointSize(8.0f);
		Left_PLANE_SMOKE_Curve_RMB();

		if(Left_Planes_NOTRepaint == true)
		{
			glLoadIdentity();
			glColor3f(0.7265625f, 0.8828125f, 0.9296875f);
			glTranslatef(k2, l2, -3.0f);
			glRotatef(Rotate_Angles2,0.0f,0.0f,1.0f);
			PLANE_Code_RMB();

			glLoadIdentity();
			glColor3f(0.7265625f, 0.8828125f, 0.9296875f);
			glTranslatef(k3, l3, -3.0f);
			glRotatef(Rotate_Angles_3,0.0f,0.0f,1.0f);
			PLANE_Code_RMB();
		}
	}

	if(Tri_Band_Right == true)
	{
		//Smoke Curve Code
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -4.0f);
		glPointSize(8.0f);
		Right_PLANE_SMOKE_Curve_RMB();

		glLoadIdentity();
		glColor3f(0.7265625f, 0.8828125f, 0.9296875f);
		glTranslatef(k1, l1, -3.0f);
		glRotatef(Rotate_Angles_1,0.0f,0.0f,1.0f);
		PLANE_Code_RMB();

		glLoadIdentity();
		glColor3f(0.7265625f, 0.8828125f, 0.9296875f);
		glTranslatef(k4, l4, -3.0f);
		glRotatef(Rotate_Angles_4,0.0f,0.0f,1.0f);
		PLANE_Code_RMB();
	}	

	//Plane Code
	glLoadIdentity();
	glTranslatef(x2, -0.05f, -3.0f);
	glColor3f(0.7265625f, 0.8828125f, 0.9296875f);
	PLANE_Code_RMB();
		
	//PLANE After Tri band Code
	glLoadIdentity();
	glTranslatef(PosX3, 0.02f, -3.0f);
	glLineWidth(10.0f);
	PLANE_SMOKE_Code_RMB();

	if(RepaintINDIA == true)
	{
		// I
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(Nigx, 0.0f, -5.0f);
		glLineWidth(35.0f);
		I1st_Code_RMB();

		// A
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(PosX2, 0.0f, -3.0f);
		glLineWidth(10.0f);
		A_Tri_Code_RMB();

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(PosX1, 0.0f, -5.0f);
		glLineWidth(35.0f);
		A_Code_RMB();
		
		// N
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.6f, posy, -5.0f);
		glLineWidth(35.0f);
		N_Code_RMB();

		// I
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.75f, Nigy, -5.0f);
		glLineWidth(35.0f);
		I2nd_code_RMB();

		//D
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.64f, 0.0f, -5.0f);
		glLineWidth(35.0f);
		D_Code_RMB();
	}

	SwapBuffers(ghdc);
}

void Uninitialize(void)
{
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf_s(gpFile, "Log file is closed Successfully.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(false);
	}
}

void I1st_Code_RMB(void)
{
	//I_1
	glBegin(GL_LINES);
	glColor3f(1.0f,0.5f,0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glColor3f( 0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	glEnd();
}

void A_Tri_Code_RMB(void)
{
	glBegin(GL_LINES);

	glColor3f(Color1,Color05,0.0f);
	glVertex3f(-0.12f, -0.026f, 0.0f);
	glVertex3f(-0.47f, -0.026f, 0.0f);

	glColor3f(Color1, Color1, Color1);
	glVertex3f(-0.12f, -0.055f, 0.0f);
	glVertex3f(-0.47f, -0.055f, 0.0f);
	
	glColor3f( 0.0f, Color1, 0.0f);
	glVertex3f(-0.12f, -0.08f, 0.0f);
	glVertex3f(-0.47f, -0.08f, 0.0f);

	glEnd();
}

void A_Code_RMB(void)
{
	//A
	glBegin(GL_LINES);

	glColor3f(1.0f,0.5f,0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glColor3f( 0.0f, 1.0f, 0.0f);
	glVertex3f(0.20f, -1.0f, 0.0f);

	glColor3f(1.0f,0.5f,0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glColor3f( 0.0f, 1.0f, 0.0f);
	glVertex3f(-0.80f, -1.0f, 0.0f);
	glEnd();
}

void N_Code_RMB(void)
{
	//N	
	glBegin(GL_LINES);
	glColor3f(1.0f,0.5f,0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glColor3f( 0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);

	glColor3f(1.0f,0.5f,0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glColor3f( 0.0f, 1.0f, 0.0f);
	glVertex3f(-0.2f, -1.0f, 0.0f);

	glColor3f(1.0f,0.5f,0.0f);
	glVertex3f(-0.2f, 1.0f, 0.0f);
	glColor3f( 0.0f, 1.0f, 0.0f);
	glVertex3f(-0.2f, -1.0f, 0.0f);
	glEnd();
}

void I2nd_code_RMB(void)
{
	//I_2
	glBegin(GL_LINES);
	glColor3f(1.0f,0.5f,0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glColor3f( 0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	glEnd();
}

void D_Code_RMB(void)
{
	//D
	glBegin(GL_LINES);

	glColor3f(Color1,Color05,0.0f);
	glVertex3f(-1.0f, 0.9f, 0.0f);

	glColor3f( 0.0f, Color1, 0.0f);
	glVertex3f(-1.0f, -0.9f, 0.0f);

	glColor3f(Color1,Color05,0.0f);
	glVertex3f(-1.2f, 0.93f, 0.0f);
	glVertex3f(-0.2f, 0.93f, 0.0f);

	glColor3f( 0.0f, Color1, 0.0f);
	glVertex3f(-1.2f, -0.93f, 0.0f);
	glVertex3f(-0.2f, -0.93f, 0.0f);

	glColor3f(Color1,Color05,0.0f);
	glVertex3f(-0.2f, 1.0f, 0.0f);

	glColor3f( 0.0f, Color1, 0.0f);
	glVertex3f(-0.2f, -1.0f, 0.0f);

	glEnd();
}

void PLANE_Code_RMB(void)
{
	glBegin(GL_TRIANGLES);
	glVertex3f(0.0f, 0.08f, 0.0f);
	glVertex3f(0.0f, -0.08f, 0.0f);
	glVertex3f(0.08f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(0.0f, 0.08f, 0.0f);
	glVertex3f(-0.28f, 0.08f, 0.0f);
	glVertex3f(-0.28f, -0.08f, 0.0f);
	glVertex3f(0.0f, -0.08f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glVertex3f(-0.24f, 0.0f, 0.0f);
	glVertex3f(-0.28f, 0.17f, 0.0f);
	glVertex3f(-0.28f, -0.17f, 0.0f);
	glEnd();
		
	glBegin(GL_TRIANGLES);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glVertex3f(-0.2f, 0.18f, 0.0f);
	glVertex3f(-0.2f, -0.18f, 0.0f);
	glEnd();

	glColor3f(1.0f, 1.0f, 1.0f);
	glLineWidth(5.0f);
		//I
	glBegin(GL_LINES);
	glVertex3f(-0.2f, 0.05f, 0.0f);
	glVertex3f(-0.2f, -0.05f, 0.0f);
	glEnd();
		//A
	glBegin(GL_LINES);
	glVertex3f(-0.14f, 0.05f, 0.0f);
	glVertex3f(-0.18f, -0.05f, 0.0f);
	glVertex3f(-0.14f, 0.05f, 0.0f);
	glVertex3f(-0.1f, -0.05f, 0.0f);
	glVertex3f(-0.12f, 0.0f, 0.0f);
	glVertex3f(-0.16f, 0.0f, 0.0f);
	glEnd();
		//F
	glBegin(GL_LINES);
	glVertex3f(-0.08f, 0.05f, 0.0f);
	glVertex3f(-0.08f, -0.05f, 0.0f);
	glVertex3f(-0.08f, 0.042f, 0.0f);
	glVertex3f(-0.02f, 0.042f, 0.0f);
	glVertex3f(-0.08f, 0.0f, 0.0f);
	glVertex3f(-0.04f, 0.0f, 0.0f);
		
	glEnd();
}

void PLANE_SMOKE_Code_RMB(void)
{
	glBegin(GL_LINES);
	glColor3f(Keshari1,Keshari2,0.0f);
	glVertex3f(0.0f, -0.045f, 0.0f);
	glVertex3f(-7.3f, -0.045f, 0.0f);

	glColor3f(Keshari1, Keshari1, Keshari1);
	glVertex3f(0.0f, -0.073f, 0.0f);
	glVertex3f(-7.3f, -0.073f, 0.0f);
	
	glColor3f( 0.0f, Keshari1, 0.0f);
	glVertex3f(0.0f, -0.10f, 0.0f);
	glVertex3f(-7.3f, -0.10f, 0.0f);
	glEnd();
}

void Left_PLANE_SMOKE_Curve_RMB(void)
{
	//Left Top
	glBegin(GL_POINTS);
	for (GLfloat Angle = 0.0f; Angle < Smoke2 * M_PI / 4.0f ; Angle = Angle + 0.001f)
	{
		glColor3f( 0.0f, Keshari1, 0.0f);
		glVertex3f(-cos(Angle) * 1.7f - 1.2f, -sin(Angle) * 1.8f + 1.67f, 0.0f);

		glColor3f(Keshari1, Keshari1, Keshari1);
		glVertex3f(-cos(Angle) * 1.66f - 1.2f, -sin(Angle) * 1.76f + 1.67f, 0.0f);

		glColor3f(Keshari1,Keshari2,0.0f);
		glVertex3f(-cos(Angle) * 1.62f - 1.2f, -sin(Angle) * 1.72f + 1.67f, 0.0f);
	}
	glEnd();

	//Left Bottom
	glBegin(GL_POINTS);
	for (GLfloat Angle = 0.0f; Angle < Smoke3 * M_PI / 4.0f ; Angle = Angle + 0.001f)
	{
		glColor3f(Keshari1,Keshari2,0.0f);
		glVertex3f(-cos(Angle) * 1.7f - 1.2f, sin(Angle) * 1.8f - 1.8f, 0.0f);

		glColor3f(Keshari1, Keshari1, Keshari1);
		glVertex3f(-cos(Angle) * 1.66f - 1.2f, sin(Angle) * 1.76f - 1.8f, 0.0f);

		glColor3f( 0.0f, Keshari1, 0.0f);
		glVertex3f(-cos(Angle) * 1.62f - 1.2f, sin(Angle) * 1.72f - 1.8f, 0.0f);
	}
	glEnd();
}

void Right_PLANE_SMOKE_Curve_RMB(void)
{
	//Right Top
	glBegin(GL_POINTS);
	for (GLfloat Angle = -1.4f; Angle < Smoke1 * M_PI / 4.0f ; Angle = Angle + 0.001f)
	{
		glColor3f( 0.0f, Keshari1, 0.0f);
		glVertex3f(cos(Angle) * 1.8f + 1.2f, sin(Angle) * 1.8f + 1.67, 0.0f);

		glColor3f(Keshari1, Keshari1, Keshari1);
		glVertex3f(cos(Angle) * 1.76f + 1.2f, sin(Angle) * 1.76f + 1.67, 0.0f);

		glColor3f(Keshari1,Keshari2,0.0f);
		glVertex3f(cos(Angle) * 1.72f + 1.2f, sin(Angle) * 1.72f + 1.67, 0.0f);
	}
	glEnd();

	//Right Bottom
	glBegin(GL_POINTS);
	for (GLfloat Angle = -1.4f; Angle < Smoke4 * M_PI / 4.0f ; Angle = Angle + 0.001f)
	{
		glColor3f(Keshari1,Keshari2,0.0f);
		glVertex3f(cos(Angle) * 1.8f + 1.2f, -sin(Angle) * 1.8f - 1.8f, 0.0f);

		glColor3f(Keshari1, Keshari1, Keshari1);
		glVertex3f(cos(Angle) * 1.76f + 1.2f, -sin(Angle) * 1.76f - 1.8f, 0.0f);

		glColor3f( 0.0f, Keshari1, 0.0f);
		glVertex3f(cos(Angle) * 1.72f +1.2, -sin(Angle) * 1.72f - 1.8f, 0.0f);
	}
	glEnd();

}

void Update_RMB(void)
{

	if(IFlag1 == true)
	{
	//I
		Nigx = Nigx + 0.0024f;
		if(Nigx >= -0.9f)
		{
			Nigx = -0.9f;
			PrintEnd = 1.0f;
		}
	}

	if(AFlag == true && PrintEnd == 1.0f)
	{
		//A  (-)
		PosX2 = PosX2 - 0.0024f;
		if (PosX2 <= 1.2f)
		{
			PosX2 = 1.2f;
		}

		//A (/\)
		PosX1 = PosX1 - 0.0024f;
		if (PosX1 <= 1.85f)
		{
				PosX1 = 1.85f;
				PrintEnd = 2.0f;
		}
	}

	if (NFlag == true && PrintEnd == 2.0f)
	{
		//N
		posy = posy - 0.0024f;

		if(posy <= 0.0f)
		{
			posy = 0.0f;
			PrintEnd = 3.0f;
		}
	}

	if(IFlag2 == true && PrintEnd == 3.0f)
	{
		
		//I
		Nigy = Nigy + 0.0024f;
		if(Nigy >= 0.0f)
		{
			Nigy = 0.0f;
			PrintEnd = 4.0f;
		}
	}

	if(DFlag == true && PrintEnd == 4.0f)
	{ 
		//D
		Color1 = Color1 + 0.0024f;
		Color05 = Color05 + 0.0024f;
		if(Color1 >= 1.0f)
		{
			Color1 = 1.0f;
			Color05 = 0.5f;
			PrintEnd = 5.0f;
		}
	}

	if(PrintEnd == 5.0f)
	{
		//PLANE
		x2 = x2 + 0.0002565f;
		if(x2 >= 2.6f)
		{
			x2 = 2.6f;
		}

		if(x2 >= -2.48f)
		{
			Tri_Band_Left = true;
			Left_Planes_NOTRepaint = true;
		}

		//Left Top Curve PLANE 2
		k2 = 1.8f * cos(Translate_Angle2) - 0.6f;
		l2 = 1.85f * sin(Translate_Angle2) + 1.75f;
		if(Translate_Angle2 >= 180.51f)
		{
			Translate_Angle2 = 180.51f;
		}
		Translate_Angle2 = Translate_Angle2 + 0.00014495855f;

		if(Rotate_Angles2 >= 360)
		{
			Rotate_Angles2 = 360;
		}
		Rotate_Angles2 = Rotate_Angles2 + 0.015;

		//2
		Smoke2 = Smoke2 + 0.00026f;
		if (Smoke2 >= 1.76f)
		{
			Smoke2 = 1.76f;
		}

		//Left Bottom Curve PLANE 3
		k3 = 1.8f * cos(Translate_Angle_3) - 0.6f;
		l3 = 1.85f * sin(Translate_Angle_3) - 1.85f;

		if(Translate_Angle_3 < 177.7f)
		{
			Translate_Angle_3 = 177.7f;
			Left_Planes_NOTRepaint = false;
		}
		Translate_Angle_3 = Translate_Angle_3 - 0.00014495855f;
	
		if(Rotate_Angles_3 < 0)
		{
			Rotate_Angles_3 = 0;
		}
		Rotate_Angles_3 = Rotate_Angles_3 - 0.015;

		Smoke3 = Smoke3 + 0.00026f;
		if (Smoke3 >= 1.77f)
		{
			Smoke3 = 1.77f;
		}

		if(x2 >= 1.2f)
		{
			//Right Top Curve PLANE 1
			Tri_Band_Right = true;

			k1 = 1.3 * cos(Translate_Angle_1) + 1.0f;
			l1 = 1.66 * sin(Translate_Angle_1) + 1.585f;
			if(Translate_Angle_1 >= 182.18)
			{
				Translate_Angle_1 = 182.18;
			}
			Translate_Angle_1 = Translate_Angle_1 + 0.00025f;

			if(Rotate_Angles_1 >= 75)
			{
				Rotate_Angles_1 = 75;
			}
			Rotate_Angles_1 = Rotate_Angles_1 + 0.017f;
	
			Smoke1 = Smoke1 + 0.0003f;
			if (Smoke1 >= 0.0f)
			{
				Smoke1 = 0.0f;
			}


			//Right Bottom Curve PLANE 4
			k4 = 1.3 * cos(Translate_Angle_4) + 1.0f;
			l4 = 1.66 * sin(Translate_Angle_4) - 1.69f;

			if(Translate_Angle_4 <= 88.00f)
			{
				Translate_Angle_4 = 88.00f;
			}
			Translate_Angle_4 = Translate_Angle_4 - 0.000254f;
	
			if(Rotate_Angles_4 <= 270)
			{
				Rotate_Angles_4 = 270;
			}
			Rotate_Angles_4 = Rotate_Angles_4 - 0.017f;

			Smoke4 = Smoke4 + 0.00031f;
			if (Smoke4 >= 0.0f)
			{
				Smoke4 = 0.0f;
			}
		}

	}

	if(PrintEnd == 5.0f)
	{
		//SMOKE Code
		PosX3 = PosX3 + 0.0002565f;
		if (PosX3 >= 2.6f)
		{
			PosX3 = 2.6f;
			PrintEnd = 6.0f;
		}
		if(PrintEnd == 6.0f)
		{
			Keshari1 = Keshari1 - 0.00055f;
			Keshari2 = Keshari2 - 0.00055f;
			if(Keshari1 >= 1.0f)
			{
				Keshari1 = 0.0f;
				Keshari2 = 0.0f;
			}
			RepaintINDIA = true;
		}

	}

}


