#include<GL/freeglut.h>

bool bIsFullScreen = false;
int year = 0;
int day = 0;

int main(int argc, char * argv[])
{
	//function Declaration
	void initialize(void);
	void uninitialize(void);
	void reshape(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);

	//code
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA );
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Sphere...Rahul.M.Bhadrashette");
	 
	initialize();
	//CallBack
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);
	
	glutMainLoop();
	return(0);
}
void initialize(void)
{
	//Code
	glShadeModel(GL_FLAT);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//glClearDepth(1.0f);
	//glEnable(GL_DEPTH_TEST);
	//glDepthFunc(GL_LEQUAL);
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void uninitialize(void)
{
	//Code
}

void reshape(int Width, int Height)
{
	//Code
	if (Height == 0)
	{
		Height = 1;
	}

	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0f, (GLfloat)Width / (GLfloat)Height, 1.0f, 20.0f);
}

void display(void)
{
	//Code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0f, 0.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	glPushMatrix();
	glutWireSphere(1, 20, 16);
	glRotatef((GLfloat) year, 0.0f, 1.0f,0.0f);
	glTranslatef(2.0f, 0.0f, 0.0f);
	glRotatef((GLfloat)day, 0.0f, 1.0f, 0.0f);
	glutWireSphere(0.2f, 10, 8);
	glPopMatrix();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
		//Code
	case 27:
		glutLeaveMainLoop();
		break;

	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;

	case 'Y':
		year = (year + 5) % 360;
		glutPostRedisplay();
		break;

	case 'y':
		year = (year - 5) % 360;
		glutPostRedisplay();
		break;

	case 'D':
		day = (day + 10) % 360;
		glutPostRedisplay();
		break;

	case 'd':
		day = (day - 10) % 360;
		glutPostRedisplay();
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	
	switch (button)
	{
		//Code
	case GLUT_LEFT_BUTTON:
		break;
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	}
}