//Header Files
#include<Windows.h>
#include<stdio.h>
#include<gl/GLEW.h>
#include<gl/GL.h>
#include"vmath.h"


//Library Files
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")

//User define Micro
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTCOORD0
};

//Global  variable Declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;
FILE* gpFile = NULL;
bool gbActiveWindow = false;
bool bIsFullScreen = false;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

//Programmable Pipeline Variable
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;
GLuint vao;
GLuint vbo;
GLuint vbo_Color;
GLuint mvpUniform;

//Function Declaration(Prototype/ Signature)
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void Uninitialize_RMB(void);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	int Initialize_RMB(void);
	void Display_RMB(void);

	//Local Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR str[] = TEXT("BlueScreen");
	bool bDone = false;
	int iRet = 0;
	int X, Y;

	//File io
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created"), TEXT("Error"), MB_OK);
	}
	else
	{
		fprintf(gpFile, "Log file is successfully complited...!!!\n");
	}

	//Initialize WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = str;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register WinclassEx
	RegisterClassEx(&wndclass);

	//CenterOfTheWindowCalculation
	X = (GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2);
	Y = (GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2);

	//CreateWindow
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, str, TEXT("RahulUMB...02-BlueWindowWithSheaders...!!!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X,
		Y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = Initialize_RMB();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, " wglCreateContext is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent Failed.\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "Initializetion Successfully Complited.\n");
	}

	//Here Actual ShowWindow
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//MessageLoop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

		}
		else
		{
			if (gbActiveWindow == true)
			{
				//update();
			}
			Display_RMB();
		}
	}

	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void Resize_RMB(int, int);
	void ToggleFullScreen(void);

	switch (iMsg)
	{
		//code
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		Resize_RMB(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)  //in Past switch(LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		Uninitialize_RMB();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize_RMB(void)
{
	//Function Declaration
	void Resize_RMB(int, int);
	void ToggleFullScreen(void);

	//Local Variable Declaration
	GLenum result;
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	//Initialize pfd structure
	//ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));  or   //For only Windows
	memset((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));  //For All OS
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	//the return index is always 1 based to 38 if Zero(0) return failure 
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf_s(gpFile, "glewInit(); is Failed.\n");
		Uninitialize_RMB();
		DestroyWindow(ghwnd);
	}

	//Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar* vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_Color;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_Color = vColor;" \
		"}";

	//Specifing above source to the vertex shader object
	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//Compile the Vertex Shader
	glCompileShader(gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar* szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "vertexShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				Uninitialize_RMB();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 out_Color;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
		"fragColor = out_Color;" \
		"}";


	// Specifing above Source to the fragment Shader Object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//Compile the fragment Shader Object
	glCompileShader(gFragmentShaderObject);

	GLint ifShaderCompileStatus = 0;
	GLint ifInfoLogLength = 0;
	GLint iInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &ifShaderCompileStatus);
	if (ifShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &ifInfoLogLength);
		if (ifInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(ifInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, ifInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "FragmentShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Create Shader Object
	gShaderProgramObject = glCreateProgram();

	//Attach VertexShader to the Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//Attach fragmentShader to the Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Pre Linking Binding to Vertex Attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject);

	GLint  iProgramLinkStatus = 0;
	GLint isInfoLogLength = 0;
	GLint* szLogLength = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &isInfoLogLength);
		if (iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(isInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, isInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "LinkShaderProgramObject:- %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Post Linking Retriving Uniform Location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	//TrangleArray
	const  GLfloat verties[] =
	{
		//First
		//right
		0.5f, 0.8f, 0.0f,
		0.8f, 0.8f, 0.0f,
		//top
		0.8f, 0.8f, 0.0f,
		0.8f, 0.3f, 0.0f,
		//left
		0.5f, 0.3f, 0.0f,
		0.8f, 0.3f, 0.0f,
		//botttom
		0.5f, 0.466f, 0.0f,
		0.8f, 0.466f, 0.0f,

		0.5f, 0.632f, 0.0f,
		0.8f, 0.632f, 0.0f,

		0.5f, 0.3f, 0.0f,
		0.5f, 0.8f, 0.0f,

		0.6f, 0.3f, 0.0f,
		0.6f, 0.8f, 0.0f,

		0.7f, 0.3f, 0.0f,
		0.7f, 0.8f, 0.0f,

		//Second
		//top
		-0.15f, 0.8f, 0.0f,
		0.15f, 0.8f, 0.0f,
		//left
		-0.15f, 0.3f, 0.0f,
		-0.15f, 0.8f, 0.0f,
		//horizental
		-0.15f, 0.466f, 0.0f,
		0.15f, 0.466f, 0.0f,
		-0.15f, 0.632f, 0.0f,
		0.15f, 0.632f, 0.0f,
		//vertical 
		-0.05f, 0.3f, 0.0f,
		-0.05f, 0.8f, 0.0f,
		0.05f, 0.3f, 0.0f,
		0.05f, 0.8f, 0.0f,
		//1
		-0.15f, 0.632f, 0.0f,
		-0.05f, 0.8f, 0.0f,
		//2
		-0.15f, 0.466f, 0.0f,
		0.05f, 0.8f, 0.0f,
		//3
		0.15f, 0.8f, 0.0f,
		-0.15f, 0.3f, 0.0f,
		//4
		0.15f, 0.632f, 0.0f,
		-0.05f, 0.3f, 0.0f,
		//5
		0.15f, 0.466f, 0.0f,
		0.05f, 0.3f, 0.0f,

		//Third
		-0.5f, 0.8f, 0.0f,
		-0.8f, 0.8f, 0.0f,

		-0.8f, 0.3f, 0.0f,
		-0.5f, 0.3f, 0.0f,

		-0.5f, 0.466f, 0.0f,
		-0.8f, 0.466f, 0.0f,

		-0.5f, 0.632f, 0.0f,
		-0.8f, 0.632f, 0.0f,

		-0.6f, 0.3f, 0.0f,
		-0.6f, 0.8f, 0.0f,

		-0.7f, 0.3f, 0.0f,
		-0.7f, 0.8f, 0.0f,

		-0.6f, 0.632f, 0.0f,
		-0.7f, 0.632f, 0.0f,

		-0.7f, 0.466f, 0.0f,
		-0.6f, 0.466f, 0.0f,

		//Forth
		//right 
		-0.5f, -0.3f, 0.0f,
		-0.5f, -0.8f, 0.0f,
		//top
		-0.5f, -0.3f, 0.0f,
		-0.8f, -0.3f, 0.0f,
		//left
		-0.8f, -0.8f, 0.0f,
		-0.8f, -0.3f, 0.0f,
		//bottom
		-0.5f, -0.8f, 0.0f,
		-0.8f, -0.8f, 0.0f,
		//vertical line
		-0.5f, -0.466f, 0.0f,
		-0.8f, -0.466f, 0.0f,
		-0.5f, -0.632f, 0.0f,
		-0.8f, -0.632f, 0.0f,
		//horizental Line
		-0.6f, -0.3f, 0.0f,
		-0.6f, -0.8f, 0.0f,
		-0.7f, -0.3f, 0.0f,
		-0.7f, -0.8f, 0.0f,
		//Cross Lines
		//1
		-0.8f, -0.466f, 0.0f,
		-0.7f, -0.3f, 0.0f,
		//2
		-0.8f, -0.632f, 0.0f,
		-0.6f, -0.3f, 0.0f,
		//3
		-0.5f, -0.3f, 0.0f,
		-0.8f, -0.8f, 0.0f,
		//4
		-0.5f, -0.466f, 0.0f,
		-0.7f, -0.8f, 0.0f,
		//5
		-0.6f, -0.8f, 0.0f,
		-0.5f, -0.632f, 0.0f,

		//Five
		//right
		0.15f, -0.8f, 0.0f,
		0.15f, -0.3f, 0.0f,
		//top
		0.15f, -0.3f, 0.0f,
		-0.15f, -0.3f, 0.0f,
		//left
		-0.15f, -0.3f, 0.0f,
		-0.15f, -0.8f, 0.0f,
		//botttom
		-0.15f, -0.8f, 0.0f,
		0.15f, -0.8f, 0.0f,
		//1
		-0.15f, -0.3f, 0.0f,
		0.15f, -0.466f, 0.0f,
		//2
		- 0.15f, -0.3f, 0.0f,
		0.15f, -0.632f, 0.0f,
		//3
		- 0.15f, -0.3f, 0.0f,
		0.15f, -0.8f, 0.0f,
		//4
		- 0.15f, -0.3f, 0.0f,
		- 0.05f, -0.8f, 0.0f,
		//5
		- 0.15f, -0.3f, 0.0f,
		0.05f, -0.8f, 0.0f,

		//For Triangles
		0.6f, -0.8f, 0.0f,
		0.6f, -0.3f, 0.0f,
		0.5f, -0.3f, 0.0f,

		0.5f, -0.3f, 0.0f,
		0.5f, -0.8f, 0.0f,
		0.6f, -0.8f, 0.0f,

		0.7f, -0.8f, 0.0f,
		0.7f, -0.3f, 0.0f,
		0.6f, -0.3f, 0.0f,

		0.6f, -0.3f, 0.0f,
		0.6f, -0.8f, 0.0f,
		0.7f, -0.8f, 0.0f,

		0.8f, -0.8f, 0.0f,
		0.8f, -0.3f, 0.0f,
		0.7f, -0.3f, 0.0f,

		0.7f, -0.3f, 0.0f,
		0.7f, -0.8f, 0.0f,
		0.8f, -0.8f, 0.0f,

		//Six
		//right
		0.5f, -0.8f, 0.0f,
		0.8f, -0.8f, 0.0f,
		//top
		0.8f, -0.8f, 0.0f,
		0.8f, -0.3f, 0.0f,
		//left
		0.5f, -0.3f, 0.0f,
		0.8f, -0.3f, 0.0f,
		//bottom
		0.5f, -0.3f, 0.0f,
		0.5f, -0.8f, 0.0f,
		//vertical Line
		//1
		0.6f, -0.3f, 0.0f,
		0.6f, -0.8f, 0.0f,
		//2
		0.7f, -0.3f, 0.0f,
		0.7f, -0.8f, 0.0f,
		//horixental line
		//1
		0.5f, -0.466f, 0.0f,
		0.8f, -0.466f, 0.0f,
		//2
		0.5f, -0.632f, 0.0f,
		0.8f, -0.632f, 0.0f,
	};

	//circle Color Array
	const int iPoints = 132;
	GLfloat color[3 * iPoints];
	for (int i = 0; i < iPoints; i++)
	{	
		if (i < 98)
		{
			color[3 * i + 0] = 1.0f;
			color[3 * i + 1] = 1.0f;
			color[3 * i + 2] = 1.0f;
		}
		else 
		{
			if (i < 104)
			{
				color[3 * i + 0] = 1.0f;
				color[3 * i + 1] = 0.0f;
				color[3 * i + 2] = 0.0f;
			}
			else if (i < 110)
			{
				color[3 * i + 0] = 0.0f;
				color[3 * i + 1] = 1.0f;
				color[3 * i + 2] = 0.0f;
			}
			else if (i < 116)
			{
				color[3 * i + 0] = 0.0f;
				color[3 * i + 1] = 0.0f;
				color[3 * i + 2] = 1.0f;
			}
			else 
			{
				color[3 * i + 0] = 1.0f;
				color[3 * i + 1] = 1.0f;
				color[3 * i + 2] = 1.0f;
			}
		}
	}

	//Create vao
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(verties), verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Color
	glGenBuffers(1, &vbo_Color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//Warmup Call To Resize
	Resize_RMB(WIN_WIDTH, WIN_HEIGHT);
	ToggleFullScreen();
	return(0);
}

void Resize_RMB(int Width, int Height)
{
	if (Height == 0)
	{
		Height = -1;
	}

	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
}

void Display_RMB(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	vmath::mat4 modelViewProjectionMatrix;
	modelViewProjectionMatrix = vmath::mat4::identity();

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao);

	

	//Draw the Necessary Scnes
	//Start Right Top First
	glLineWidth(2.0f);
	glDrawArrays(GL_LINES, 0, 16);
	//Second
	glDrawArrays(GL_LINES, 16, 22);
	glPointSize(6.0f);
	glDrawArrays(GL_POINTS, 38, 16);
	//Forth
	glDrawArrays(GL_LINES, 54, 26);
	//Five
	glDrawArrays(GL_LINES, 80, 18);
	//Six
	glDrawArrays(GL_TRIANGLES, 98, 18);
	glDrawArrays(GL_LINES, 116, 16);
	//Third
	
	//UnBind vao
	glBindVertexArray(0);

	glUseProgram(0);
	SwapBuffers(ghdc);
}

void Uninitialize_RMB(void)
{
	if (vbo_Color)
	{
		glDeleteBuffers(1, &vbo_Color);
		vbo_Color = 0;
	}
	if (vbo)
	{
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}
	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);
		//Ask Program How Many Shaders are Attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	//Break the Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf_s(gpFile, "Log file is closed Successfully. \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
	}
}

