//Header Files
#include<Windows.h>
#include<stdio.h>
#include<gl/GLEW.h>
#include<gl/GL.h>
#include"vmath.h" 



//Library Files
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")

//User define Micro
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTCOORD0
};

//Global  variable Declaration
GLfloat angle = 0.0f;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;
FILE* gpFile = NULL;
bool gbActiveWindow = false;
bool bIsFullScreen = false;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

//Programmable Pipeline Variable
bool gbAnimation;
bool gbLight;

vmath::mat4 modelMatrix;
vmath::mat4 viewMatrix;
vmath::mat4 projectionMatrix;

GLuint gShaderProgramObject;

GLuint modelUniform;
GLuint viewUniform;
GLuint projectionUniform;

GLuint LaUniform_Red;
GLuint LdUniform_Red;
GLuint LsUniform_Red;
GLuint lightPositionUniform_Red;

GLuint LaUniform_Blue;
GLuint LdUniform_Blue;
GLuint LsUniform_Blue;
GLuint lightPositionUniform_Blue;

GLuint KaUniform;
GLuint KdUniform;
GLuint KsUniform;

GLuint materialShininessUniform;
GLuint lKeyIsPressedUniform;

struct Light
{
	GLfloat Ambient[4];
	GLfloat Diffuse[4];
	GLfloat Specular[4];
	GLfloat Position[4];
};

struct  Light light[2];

GLfloat materialAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f }; //Ka
GLfloat materialDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };  //Kd
GLfloat materialSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };  //Ks
GLfloat materialShininess[1] = { 50.0f }; //128.0f;

GLuint vao_Pyramid;
GLuint vbo_Position_Pyramid;
GLuint vbo_Normal_Pyramid;

//For Sphere

//Function Declaration(Prototype/ Signature)
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void Uninitialize_RMB(void);
void Light_Array(void);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	int Initialize_RMB(void);
	void Display_RMB(void);
	void Update_RMB(void);

	//Local Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR str[] = TEXT("BlueScreen");
	bool bDone = false;
	int iRet = 0;
	int X, Y;

	//File io
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created"), TEXT("Error"), MB_OK);
	}
	else
	{
		fprintf_s(gpFile, "Log file is successfully complited...!!!\n");
	}

	//Initialize WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = str;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register WinclassEx
	RegisterClassEx(&wndclass);

	//CenterOfTheWindowCalculation
	X = (GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2);
	Y = (GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2);

	//CreateWindow
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, str, TEXT("RahulUMB | Roll.No.58 ...27-SteadyLightsOnRotatingPyramid...!!!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X,
		Y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = Initialize_RMB();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "ChoosePixelFormat Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "SetPixelFormat is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, " wglCreateContext is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "wglMakeCurrent Failed.\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "Initializetion Successfully Complited.\n");
	}

	//Here Actual ShowWindow
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//MessageLoop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				Update_RMB();
			}
			Display_RMB();
		}
	}

	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void Resize_RMB(int, int);
	void ToggleFullScreen(void);
	void Update_RMB(void);

	switch (iMsg)
	{
		//code
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		Resize_RMB(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)  //in Past switch(LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
			case 'F':
			case 'f':
				ToggleFullScreen();
				break;

			case 'A':
			case 'a':
					gbAnimation = !gbAnimation;
				break;

			case 'L':
			case 'l':
					gbLight = !gbLight;
				break;
		}
		break;

	case WM_DESTROY:
		Uninitialize_RMB();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize_RMB(void)
{
	//Function Declaration
	void Resize_RMB(int, int);

	//Local Variable Declaration
	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLenum result;
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	//Initialize pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));  //or   //For only Windows
	//memset((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));  //For All OS
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	//the return index is always 1 based to 38 if Zero(0) return failure 
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf_s(gpFile, "glewInit(); is Failed.\n");
		Uninitialize_RMB();
		DestroyWindow(ghwnd);
	}

	//Define Vertex Shader Object
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
									"#version 450 core" \
									"\n" \
									"in vec4 vertexPosition;" \
									"in vec3 vertexNormal;" \

									"uniform mat4 u_modelMatrix;" \
									"uniform mat4 u_viewMatrix;" \
									"uniform mat4 u_projection_matrix;" \

									"uniform vec3 u_La_Red;" \
									"uniform vec3 u_Ld_Red;" \
									"uniform vec3 u_Ls_Red;" \

									"uniform vec3 u_La_Blue;" \
									"uniform vec3 u_Ld_Blue;" \
									"uniform vec3 u_Ls_Blue;" \

									"uniform vec3 u_Ka;" \
									"uniform vec3 u_Kd;" \
									"uniform vec3 u_Ks;" \

									"uniform float u_materialShininess;" \
									"uniform int u_lKeyIsPressed;" \
									"uniform vec4 u_Light_Position_Red;" \
									"uniform vec4 u_Light_Position_Blue;" \

									"out vec3 phong_AdsLight;" \

									"void main(void)" \
									"{" \
										"if(u_lKeyIsPressed == 1)" \
										"{" \
											"vec4 eye_coordinate = u_viewMatrix * u_modelMatrix * vertexPosition;" \

											"vec3 tnorm = normalize(mat3(u_viewMatrix * u_modelMatrix) * vertexNormal);" \
											//For Red Light
											"vec3 lightdirection_Red = normalize(vec3(u_Light_Position_Red - eye_coordinate));" \

											"float tn_dot_lightDir_Red = max(dot(lightdirection_Red, tnorm), 0.0);" \

											"vec3 reflectionVector_Red = reflect(-lightdirection_Red, tnorm);" \

											"vec3 viewerVector = normalize(vec3(-eye_coordinate.xyz));" \

											"vec3 ambient_Red = u_La_Red * u_Ka;" \

											"vec3 diffuse_Red = u_Ld_Red * u_Kd * tn_dot_lightDir_Red;" \

											"vec3 specular_Red = u_Ls_Red * u_Ks * pow(max(dot(reflectionVector_Red, viewerVector), 0.0), u_materialShininess);" \

											//For Blue Light
											"vec3 lightdirection_Blue = normalize(vec3(u_Light_Position_Blue - eye_coordinate));" \

											"float tn_dot_lightDir_Blue = max(dot(lightdirection_Blue, tnorm), 0.0);" \

											"vec3 reflectionVector_Blue = reflect(-lightdirection_Blue, tnorm);" \

											"vec3 ambient_Blue = u_La_Blue * u_Ka;" \

											"vec3 diffuse_Blue = u_Ld_Blue * u_Kd * tn_dot_lightDir_Blue;" \

											"vec3 specular_Blue = u_Ls_Blue * u_Ks * pow(max(dot(reflectionVector_Blue, viewerVector), 0.0), u_materialShininess);" \

											"phong_AdsLight = ambient_Red + diffuse_Red + specular_Red + ambient_Blue + diffuse_Blue + specular_Blue;" \

										"}" \
										"else" \
										"{" \
												"phong_AdsLight = vec3(1.0, 1.0, 1.0);" \
										"}" \

										"gl_Position = u_projection_matrix * u_viewMatrix * u_modelMatrix * vertexPosition;" \
									"}";

	//Specifing above source to the vertex shader object
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//Compile the Vertex Shader
	glCompileShader(vertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "vertexShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				Uninitialize_RMB();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode =
										"#version 450 core" \
										"\n" \
										"in vec3 phong_AdsLight;" \
										"out vec4 fragColor;" \
										"void main(void)" \
										"{" \
											"fragColor = vec4(phong_AdsLight, 1.0);" \
										"}";

	// Specifing above Source to the fragment Shader Object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//Compile the fragment Shader Object
	glCompileShader(fragmentShaderObject);

	GLint iFShaderCompileStatus = 0;
	GLint iFInfoLogLength = 0;
	GLint iInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iFShaderCompileStatus);
	if (iFShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iFInfoLogLength);
		if (iFInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iFInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iFInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "FragmentShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);

				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Create Shader Object
	gShaderProgramObject = glCreateProgram();

	//Attach VertexShader to the Shader Program
	glAttachShader(gShaderProgramObject, vertexShaderObject);

	//Attach fragmentShader to the Shader Program
	glAttachShader(gShaderProgramObject, fragmentShaderObject);

	// Pre Linking Binding to Vertex Attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vertexPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vertexNormal");

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject);

	GLint  iProgramLinkStatus = 0;
	GLint iLInfoLogLength = 0;
	GLint *szLogLength = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iLInfoLogLength);
		if (iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(iLInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iLInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "LinkShaderProgramObject:- %s", szInfoLog);
				free(szInfoLog);
				Uninitialize_RMB();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Post Linking Retriving Uniform Location
	 modelUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	 viewUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	 projectionUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	 LaUniform_Red = glGetUniformLocation(gShaderProgramObject, "u_La_Red");
	 LdUniform_Red = glGetUniformLocation(gShaderProgramObject, "u_Ld_Red");
	 LsUniform_Red = glGetUniformLocation(gShaderProgramObject, "u_Ls_Red");

	 LaUniform_Blue = glGetUniformLocation(gShaderProgramObject, "u_La_Blue");
	 LdUniform_Blue = glGetUniformLocation(gShaderProgramObject, "u_Ld_Blue");
	 LsUniform_Blue = glGetUniformLocation(gShaderProgramObject, "u_Ls_Blue");

	 KaUniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	 KdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	 KsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");

	 materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialShininess");
	 lightPositionUniform_Red = glGetUniformLocation(gShaderProgramObject, "u_Light_Position_Red");
	 lightPositionUniform_Blue = glGetUniformLocation(gShaderProgramObject, "u_Light_Position_Blue");
	 lKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyIsPressed");
	
	

	 const GLfloat pyramidVertices[] =
										 {
											 //FRONT FACE
											 0.0f, 1.0f, 0.0f,
											 -1.0f, -1.0f, 1.0f,
											 1.0f, -1.0f, 1.0f,
											 //LEFT FACE
											 0.0f, 1.0f, 0.0f,
											 1.0f, -1.0f, 1.0f,
											 1.0f, -1.0f, -1.0f,
											 //BACK FACE
											 0.0f, 1.0f, 0.0f,
											 1.0f, -1.0f, -1.0f,
											 -1.0f, -1.0f, -1.0f,
											 //RIGHT FACE
											 0.0f, 1.0f, 0.0f,
											 -1.0f, -1.0f, -1.0f,
											 -1.0f, -1.0f, 1.0f
										 };

	 const GLfloat pyramidNormal[] =
										 {
											 0.0f, 0.447214f, 0.894427f,
											 0.0f, 0.447214f, 0.894427f,
											 0.0f, 0.447214f, 0.894427f,

											 0.89427f, 0.447214f, 0.0f,
											 0.89427f, 0.447214f, 0.0f,
											 0.89427f, 0.447214f, 0.0f,

											 0.0f, 0.447214f, -0.894427f,
											 0.0f, 0.447214f, -0.894427f,
											 0.0f, 0.447214f, -0.894427f,

											 -0.894427f, 0.447214f, 0.0f,
											 -0.894427f, 0.447214f, 0.0f,
											 -0.894427f, 0.447214f, 0.0f
										 };


	//Position
	glGenVertexArrays(1, &vao_Pyramid);
	glBindVertexArray(vao_Pyramid);
	glGenBuffers(1, &vbo_Position_Pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Pyramid);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Normal
	glGenBuffers(1, &vbo_Normal_Pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Normal_Pyramid);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormal), pyramidNormal, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//glEnable(GL_CULL_FACE);
	glDisable(GL_CULL_FACE);
	//glEnable(GL_FRONT_FACE);
	//glEnable(GL_BACK_LEFT);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	modelMatrix = vmath::mat4::identity();
	viewMatrix = vmath::mat4::identity();
	projectionMatrix = vmath::mat4::identity();
	//Warmup Call To Resize
	Resize_RMB(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

void Resize_RMB(int Width, int Height)
{
	//code
	if (Height == 0)
	{
		Height = 1;
	}
	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);

	projectionMatrix = vmath::perspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);
}

void Display_RMB(void)
{
	Light_Array();
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	if (gbLight)
	{
		glUniform1i(lKeyIsPressedUniform, 1);
		glUniform3fv(LaUniform_Red, 1, light[0].Ambient);
		glUniform3fv(LdUniform_Red, 1, light[0].Diffuse);
		glUniform3fv(LsUniform_Red, 1, light[0].Specular);
		glUniform4fv(lightPositionUniform_Red, 1, light[0].Position);

		glUniform3fv(LaUniform_Blue, 1, light[1].Ambient);
		glUniform3fv(LdUniform_Blue, 1, light[1].Diffuse);
		glUniform3fv(LsUniform_Blue, 1, light[1].Specular);
		glUniform4fv(lightPositionUniform_Blue, 1, light[1].Position);

		glUniform3fv(KaUniform, 1, materialAmbient);
		glUniform3fv(KdUniform, 1, materialDiffuse);
		glUniform3fv(KsUniform, 1, materialSpecular);
		glUniform1fv(materialShininessUniform, 1, materialShininess);
	}
	else
	{
		glUniform1i(lKeyIsPressedUniform, 0);
	}

	//Declatation of Matricex
	vmath::mat4 translationMatrix;
	vmath::mat4 rotationMatrix;
	
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();
	modelMatrix = vmath::mat4::identity();
	viewMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
	rotationMatrix = vmath::rotate(angle, 0.0f, 1.0f, 0.0f);

	//Do Necessary Matrix Multiplication
	modelMatrix = modelMatrix * translationMatrix * rotationMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, projectionMatrix);

	//BindWith vao
	glBindVertexArray(vao_Pyramid);
	//Draw the Necessary Scnes
	glDrawArrays(GL_TRIANGLES, 0,12);
	
	//UnBind vao
	glBindVertexArray(0);

	//UnUse Program
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void Uninitialize_RMB(void)
{
	//code
	
	if (vbo_Position_Pyramid)
	{
		glDeleteBuffers(1, &vbo_Position_Pyramid);
		vbo_Position_Pyramid = 0;
	}
	
	if (vbo_Normal_Pyramid)
	{
		glDeleteBuffers(1, &vbo_Normal_Pyramid);
		vbo_Normal_Pyramid = 0;
	}
	
	if (vao_Pyramid)
	{
		glDeleteVertexArrays(1, &vao_Pyramid);
		vao_Pyramid = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);
		//Ask Program How Many Shaders are Attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (bIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//Break the Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf_s(gpFile, "Log file is closed Successfully. \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(false);
		bIsFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bIsFullScreen = false;
	}

}

void Light_Array(void)
{
	//Array Zero
	light[0].Ambient[0] = 0.0f;
	light[0].Ambient[1] = 0.0f;
	light[0].Ambient[2] = 0.0f;
	light[0].Ambient[3] = 1.0f;
	light[0].Diffuse[0] = 1.0f;
	light[0].Diffuse[1] = 0.0f;
	light[0].Diffuse[2] = 0.0f;
	light[0].Diffuse[3] = 1.0f;
	light[0].Specular[0] = 1.0f;
	light[0].Specular[1] = 0.0f;
	light[0].Specular[2] = 0.0f;
	light[0].Specular[3] = 1.0f;
	light[0].Position[0] = -2.0f;
	light[0].Position[1] = 0.0f;
	light[0].Position[2] = 0.0f;
	light[0].Position[3] = 1.0f;

	//Array One
	light[1].Ambient[0] = 0.0f;
	light[1].Ambient[1] = 0.0f;
	light[1].Ambient[2] = 0.0f;
	light[1].Ambient[3] = 1.0f;
	light[1].Diffuse[0] = 0.0f;
	light[1].Diffuse[1] = 0.0f;
	light[1].Diffuse[2] = 1.0f;
	light[1].Diffuse[3] = 1.0f;
	light[1].Specular[0] = 0.0f;
	light[1].Specular[1] = 0.0f;
	light[1].Specular[2] = 1.0f;
	light[1].Specular[3] = 1.0f;
	light[1].Position[0] = 2.0f;
	light[1].Position[1] = 0.0f;
	light[1].Position[2] = 0.0f;
	light[1].Position[3] = 1.0f;
}

void Update_RMB(void)
{
	if (gbAnimation)
	{
		angle = angle + 0.2f;
		if (angle >= 360.f)
		{
			angle = 0.0f;
		}
	}
	
}


