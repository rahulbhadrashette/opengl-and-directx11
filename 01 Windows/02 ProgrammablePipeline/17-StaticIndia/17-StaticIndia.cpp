//Header Files
#include<Windows.h>
#include<stdio.h>
#include<gl/GLEW.h>
#include<gl/GL.h>
#include"vmath.h"

//Library Files
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")

//User define Micro
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTCOORD0
};

//Global  variable Declaration
GLfloat anglePyramid = 0.0f;
GLfloat angleCube = 0.0f;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;
FILE* gpFile = NULL;
bool gbActiveWindow = false;
bool bIsFullScreen = false;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

//Programmable Pipeline Variable
GLuint gShaderProgramObject;

GLuint vao_I;
GLuint vbo_Position_I;
GLuint vbo_Color_I;

GLuint vao_N;
GLuint vbo_Position_N;
GLuint vbo_Color_N;

GLuint vao_D;
GLuint vbo_Position_D;
GLuint vbo_Color_D;

GLuint vao_A_TriBand;
GLuint vbo_Position_A_TriBand;
GLuint vbo_Color_A_TriBand;

GLuint vao_A;
GLuint vbo_Position_A;
GLuint vbo_Color_A;
GLuint mvpUniform;
vmath::mat4 perspectiveProjectionMatrix;

//Function Declaration(Prototype/ Signature)
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void Uninitialize_RMB(void);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	int Initialize_RMB(void);
	void ToggleFullScreen(void);
	void Display_RMB(void);

	//Local Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR str[] = TEXT("BlueScreen");
	bool bDone = false;
	int iRet = 0;
	int X, Y;

	//File io
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created"), TEXT("Error"), MB_OK);
	}
	else
	{
		fprintf_s(gpFile, "Log file is successfully complited...!!!\n");
	}

	//Initialize WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = str;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register WinclassEx
	RegisterClassEx(&wndclass);

	//CenterOfTheWindowCalculation
	X = (GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2);
	Y = (GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2);

	//CreateWindow
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, str, TEXT("RahulUMB | Roll.No.58...17-StaticIndia)...!!!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X,
		Y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = Initialize_RMB();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "ChoosePixelFormat Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "SetPixelFormat is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, " wglCreateContext is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "wglMakeCurrent Failed.\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "Initializetion Successfully Complited.\n");
	}

	//Here Actual ShowWindow
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//MessageLoop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

		}
		else
		{
			if (gbActiveWindow == true)
			{
				//Update();
			}
			ToggleFullScreen();
			Display_RMB();
		}
	}

	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void Resize_RMB(int, int);

	switch (iMsg)
	{
		//code
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		Resize_RMB(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)  //in Past switch(LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		Uninitialize_RMB();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize_RMB(void)
{
	//Function Declaration
	void Resize_RMB(int, int);

	//Local Variable Declaration
	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLenum result;
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	//Initialize pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));  //or   //For only Windows
	//memset((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));  //For All OS
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	//the return index is always 1 based to 38 if Zero(0) return failure 
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf_s(gpFile, "glewInit(); is Failed.\n");
		Uninitialize_RMB();
		DestroyWindow(ghwnd);
	}

	//Define Vertex Shader Object
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_Color;" \
		"void main(void)" \
		"{" \
			"gl_Position = u_mvp_matrix * vPosition;" \
			"out_Color = vColor;" \
		"}";

	//Specifing above source to the vertex shader object
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//Compile the Vertex Shader
	glCompileShader(vertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "vertexShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				Uninitialize_RMB();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 out_Color;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
			"fragColor = out_Color;" \
		"}";


	// Specifing above Source to the fragment Shader Object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//Compile the fragment Shader Object
	glCompileShader(fragmentShaderObject);

	GLint iFShaderCompileStatus = 0;
	GLint iFInfoLogLength = 0;
	GLint iInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iFShaderCompileStatus);
	if (iFShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iFInfoLogLength);
		if (iFInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iFInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iFInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "FragmentShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);

				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Create Shader Object
	gShaderProgramObject = glCreateProgram();

	//Attach VertexShader to the Shader Program
	glAttachShader(gShaderProgramObject, vertexShaderObject);

	//Attach fragmentShader to the Shader Program
	glAttachShader(gShaderProgramObject, fragmentShaderObject);

	// Pre Linking Binding to Vertex Attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject);

	GLint  iProgramLinkStatus = 0;
	GLint iLInfoLogLength = 0;
	GLint *szLogLength = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iLInfoLogLength);
		if (iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(iLInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iLInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "LinkShaderProgramObject:- %s", szInfoLog);
				free(szInfoLog);
				Uninitialize_RMB();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Post Linking Retriving Uniform Location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	
	//I Vertices Array
	const GLfloat I_Verties[] =
									{
										-1.0f, 1.0f, 0.0f,
										-1.0f, -1.0f, 0.0f
									};
	//I Color Array
	const GLfloat I_Color[] = 
									{
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f
									};

	//N Vertices Array
	const GLfloat N_Verties[] =
									{
										-1.0f, 1.0f, 0.0f,
										-1.0f, -1.0f, 0.0f,
										-1.0f, 1.0f, 0.0f,
										-0.2f, -1.0f, 0.0f,
										-0.2f, 1.0f, 0.0f,
										-0.2f, -1.0f, 0.0f
									};
	// N Color Array
	const GLfloat N_Color[] =
									{
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f,
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f,
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f
									};

	//D Vertices Array
	const GLfloat D_Verties[] =
									{
										-1.0f, 0.9f, 0.0f,
										-1.0f, -0.9f, 0.0f,
										-1.2f, 0.93f, 0.0f,
										-0.2f, 0.93f, 0.0f,
										-1.2f, -0.93f, 0.0f,
										-0.2f, -0.93f, 0.0f,
										-0.2f, 1.0f, 0.0f,
										-0.2f, -1.0f, 0.0f
									};
	//D Color Array
	const GLfloat D_Color[] = 
									{
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f,
										1.0f,0.4f,0.0f,
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f,
										0.0f, 1.0f, 0.0f,
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f
									};

	//A Vertices Array
	const GLfloat A_TriBand_Verties[] =
									{
										0.0f, -0.045f, 0.0f,
										-0.51f, -0.045f, 0.0f,
										0.0f, -0.08f, 0.0f,
										-0.51f, -0.08f, 0.0f,
										0.0f, -0.12f, 0.0f,
										- 0.51f, -0.12f, 0.0f
									};
	//A Color Array
	const GLfloat A_TriBand_Color[] =
									{
										1.0f,0.5f,0.0f,
										1.0f,0.5f,0.0f,
										1.0f, 1.0f, 1.0f,
										1.0f, 1.0f, 1.0f,
										0.0f, 1.0f, 0.0f,
										0.0f, 1.0f, 0.0f,
									};

	//A Vertices Array
	const GLfloat A_Verties[] =
									{
										-0.4f, 1.0f, 0.0f,
										0.20f, -1.0f, 0.0f,
										-0.4f, 1.0f, 0.0f,
										-0.80f, -1.0f, 0.0f
									};
	//A Color Array
	const GLfloat A_Color[] =
									{
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f,
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f,
									};
	

	//Create vao
	glGenVertexArrays(1, &vao_I);
	glBindVertexArray(vao_I);
	glGenBuffers(1, &vbo_Position_I);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_I);
	glBufferData(GL_ARRAY_BUFFER, sizeof(I_Verties), I_Verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_I);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_I);
	glBufferData(GL_ARRAY_BUFFER, sizeof(I_Color), I_Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	//Create vao
	glGenVertexArrays(1, &vao_N);
	glBindVertexArray(vao_N);
	glGenBuffers(1, &vbo_Position_N);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_N);
	glBufferData(GL_ARRAY_BUFFER, sizeof(N_Verties), N_Verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_N);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_N);
	glBufferData(GL_ARRAY_BUFFER, sizeof(N_Color), N_Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	//Create vao
	glGenVertexArrays(1, &vao_D);
	glBindVertexArray(vao_D);
	glGenBuffers(1, &vbo_Position_D);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_D);
	glBufferData(GL_ARRAY_BUFFER, sizeof(D_Verties), D_Verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_D);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_D);
	glBufferData(GL_ARRAY_BUFFER, sizeof(D_Color), D_Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	//Create vao
	glGenVertexArrays(1, &vao_A_TriBand);
	glBindVertexArray(vao_A_TriBand);
	glGenBuffers(1, &vbo_Position_A_TriBand);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_A_TriBand);
	glBufferData(GL_ARRAY_BUFFER, sizeof(A_TriBand_Verties), A_TriBand_Verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_A_TriBand);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_A_TriBand);
	glBufferData(GL_ARRAY_BUFFER, sizeof(A_TriBand_Color), A_TriBand_Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	//Create vao
	glGenVertexArrays(1, &vao_A);
	glBindVertexArray(vao_A);
	glGenBuffers(1, &vbo_Position_A);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_A);
	glBufferData(GL_ARRAY_BUFFER, sizeof(A_Verties), A_Verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_A);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_A);
	glBufferData(GL_ARRAY_BUFFER, sizeof(A_Color), A_Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//glEnable(GL_CULL_FACE);
	glDisable(GL_CULL_FACE);
	//glEnable(GL_FRONT_FACE);
	//glEnable(GL_BACK_LEFT);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	perspectiveProjectionMatrix = vmath::mat4::identity();
	//Warmup Call To Resize
	Resize_RMB(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

void Resize_RMB(int Width, int Height)
{
	//code
	if (Height == 0)
	{
		Height = 1;
	}
	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);
}

void Display_RMB(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	//Declatation of Matricex
	vmath::mat4 translationMatrix;
	vmath::mat4 modelViewMatrix;
	vmath::mat4 modelViewProjectionMatrix;
	
	//For First I_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(-0.9f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glLineWidth(30.0f);
	//BindWith vao
	glBindVertexArray(vao_I);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 2);
	//UnBind vao
	glBindVertexArray(0);

	//For N_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	
	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(-0.6f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glLineWidth(30.0f);
	//BindWith vao
	glBindVertexArray(vao_N);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 6);
	//UnBind vao
	glBindVertexArray(0);

	//For D_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	
	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.64f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glLineWidth(30.0f);
	//BindWith vao
	glBindVertexArray(vao_D);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 8);
	//UnBind vao
	glBindVertexArray(0);

	//For Second I_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(1.75f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glLineWidth(30.0f);
	//BindWith vao
	glBindVertexArray(vao_I);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 2);
	//UnBind vao
	glBindVertexArray(0);

	//For A_TriBand_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(1.8f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glLineWidth(10.0f);
	//BindWith vao
	glBindVertexArray(vao_A_TriBand);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 6);
	//UnBind vao
	glBindVertexArray(0);

	//For A_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(1.85f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glLineWidth(30.0f);
	//BindWith vao
	glBindVertexArray(vao_A);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 6);
	//UnBind vao
	glBindVertexArray(0);

	//UnUse Program
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void Uninitialize_RMB(void)
{
	//code
	if (vbo_Color_A_TriBand)
	{
		glDeleteBuffers(1, &vbo_Color_A_TriBand);
		vbo_Color_A_TriBand = 0;
	}
	if (vbo_Position_A_TriBand)
	{
		glDeleteBuffers(1, &vbo_Position_A_TriBand);
		vbo_Position_A_TriBand = 0;
	}
	if (vao_A_TriBand)
	{
		glDeleteVertexArrays(1, &vao_A_TriBand);
		vao_A_TriBand = 0;
	}

	if (vbo_Color_A)
	{
		glDeleteBuffers(1, &vbo_Color_A);
		vbo_Color_A = 0;
	}
	if (vbo_Position_A)
	{
		glDeleteBuffers(1, &vbo_Position_A);
		vbo_Position_A = 0;
	}
	if (vao_A)
	{
		glDeleteVertexArrays(1, &vao_A);
		vao_A = 0;
	}

	if (vbo_Color_D)
	{
		glDeleteBuffers(1, &vbo_Color_D);
		vbo_Color_D = 0;
	}
	if (vbo_Position_D)
	{
		glDeleteBuffers(1, &vbo_Position_D);
		vbo_Position_D = 0;
	}
	if (vao_D)
	{
		glDeleteVertexArrays(1, &vao_D);
		vao_D = 0;
	}

	if (vbo_Color_N)
	{
		glDeleteBuffers(1, &vbo_Color_N);
		vbo_Color_N = 0;
	}
	if (vbo_Position_N)
	{
		glDeleteBuffers(1, &vbo_Position_N);
		vbo_Position_N = 0;
	}
	if (vao_N)
	{
		glDeleteVertexArrays(1, &vao_N);
		vao_N = 0;
	}

	if (vbo_Color_I)
	{
		glDeleteBuffers(1, &vbo_Color_I);
		vbo_Color_I = 0;
	}
	if (vbo_Position_I)
	{
		glDeleteBuffers(1, &vbo_Position_I);
		vbo_Position_I = 0;
	}
	if (vao_I)
	{
		glDeleteVertexArrays(1, &vao_I);
		vao_I = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);
		//Ask Program How Many Shaders are Attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	//Break the Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf_s(gpFile, "Log file is closed Successfully. \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
	}
}

