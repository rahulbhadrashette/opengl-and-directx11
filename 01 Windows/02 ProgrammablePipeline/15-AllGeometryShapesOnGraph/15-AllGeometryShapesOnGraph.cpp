//Header Files
#include<Windows.h>
#include<stdio.h>
#define _USE_MATH_DEFINES 1
#include<math.h>
#include<gl/GLEW.h>
#include<gl/GL.h>
#include"vmath.h"


//Library Files
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")

//User define Micro
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTCOORD0
};

//Global  variable Declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;
FILE* gpFile = NULL;
bool gbActiveWindow = false;
bool bIsFullScreen = false;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

//Programmable Pipeline Variable
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;
GLuint vao_Graph;
GLuint vbo_Graph;
GLuint vbo_Color_Graph;
GLuint vao_Shapes;
GLuint vbo_Shapes;
GLuint vbo_Color_Shapes;
GLuint vao_BigCircle;
GLuint vbo_BigCircle;
GLuint vao_SmallCircle;
GLuint vbo_SmallCircle;
GLuint mvpUniform;
vmath::mat4 perspectiveProjectionMatrix;

//Function Declaration(Prototype/ Signature)
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void Uninitialize_RMB(void);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	int Initialize_RMB(void);
	void Display_RMB(void);
	void ToggleFullScreen(void);

	//Local Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR str[] = TEXT("BlueScreen");
	bool bDone = false;
	int iRet = 0;
	int X, Y;

	//File io
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created"), TEXT("Error"), MB_OK);
	}
	else
	{
		fprintf_s(gpFile, "Log file is successfully complited...!!!\n");
	}

	//Initialize WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = str;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register WinclassEx
	RegisterClassEx(&wndclass);

	//CenterOfTheWindowCalculation
	X = (GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2);
	Y = (GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2);

	//CreateWindow
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, str, TEXT("RahulUMB | Roll.No.58...15-AllGeometryShapesOnGraph...!!!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X,
		Y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = Initialize_RMB();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "ChoosePixelFormat Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "SetPixelFormat is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, " wglCreateContext is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "wglMakeCurrent Failed.\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "Initializetion Successfully Complited.\n");
	}

	//Here Actual ShowWindow
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//MessageLoop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

		}
		else
		{
			if (gbActiveWindow == true)
			{
				//update();
			}
			ToggleFullScreen();
			Display_RMB();
		}
	}

	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void Resize_RMB(int, int);
	
	switch (iMsg)
	{
		//code
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		Resize_RMB(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)  //in Past switch(LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		Uninitialize_RMB();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize_RMB(void)
{
	//Function Declaration
	void Resize_RMB(int, int);

	//Local Variable Declaration
	GLenum result;
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	//Initialize pfd structure
	//ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));  or   //For only Windows
	memset((void*)& pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));  //For All OS
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	//the return index is always 1 based to 38 if Zero(0) return failure 
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf_s(gpFile, "glewInit(); is Failed.\n");
		Uninitialize_RMB();
		DestroyWindow(ghwnd);
	}

	//Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar* vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_Color;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_Color = vColor;" \
		"}";

	//Specifing above source to the vertex shader object
	glShaderSource(gVertexShaderObject, 1, (const GLchar * *)& vertexShaderSourceCode, NULL);

	//Compile the Vertex Shader
	glCompileShader(gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar* szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "vertexShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				Uninitialize_RMB();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 out_Color;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
		"fragColor = out_Color;" \
		"}";

	// Specifing above Source to the fragment Shader Object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar * *)& fragmentShaderSourceCode, NULL);

	//Compile the fragment Shader Object
	glCompileShader(gFragmentShaderObject);

	GLint ifShaderCompileStatus = 0;
	GLint ifInfoLogLength = 0;
	GLint iInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &ifShaderCompileStatus);
	if (ifShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &ifInfoLogLength);
		if (ifInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(ifInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, ifInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "FragmentShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Create Shader Object
	gShaderProgramObject = glCreateProgram();

	//Attach VertexShader to the Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//Attach fragmentShader to the Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Pre Linking Binding to Vertex Attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject);

	GLint  iProgramLinkStatus = 0;
	GLint isInfoLogLength = 0;
	GLint* szLogLength = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &isInfoLogLength);
		if (iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(isInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, isInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "LinkShaderProgramObject:- %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Post Linking Retriving Uniform Location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	//GraphVerticesArray
	const  GLfloat graphVertices[] =
	{
		//Vertical Lines
		//44
		-1.10f, 1.0f, 0.0f,
		-1.10f, -1.0f, 0.0f,
		-1.05f, 1.0f, 0.0f,
		-1.05f, -1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		-0.95f, 1.0f, 0.0f,
		-0.95f, -1.0f, 0.0f,
		-0.90f, 1.0f, 0.0f,
		-0.90f, -1.0f, 0.0f,
		-0.85f, 1.0f, 0.0f,
		-0.85f, -1.0f, 0.0f,
		-0.80f, 1.0f, 0.0f,
		-0.80f, -1.0f, 0.0f,
		-0.75f, 1.0f, 0.0f,
		-0.75f, -1.0f, 0.0f,
		-0.70f, 1.0f, 0.0f,
		-0.70f, -1.0f, 0.0f,
		-0.65f, 1.0f, 0.0f,
		-0.65f, -1.0f, 0.0f,
		-0.60f, 1.0f, 0.0f,
		-0.60f, -1.0f, 0.0f,
		-0.55f, 1.0f, 0.0f,
		-0.55f, -1.0f, 0.0f,
		-0.50f, 1.0f, 0.0f,
		-0.50f, -1.0f, 0.0f,
		-0.45f, 1.0f, 0.0f,
		-0.45f, -1.0f, 0.0f,
		-0.40f, 1.0f, 0.0f,
		-0.40f, -1.0f, 0.0f,
		-0.35f, 1.0f, 0.0f,
		-0.35f, -1.0f, 0.0f,
		-0.30f, 1.0f, 0.0f,
		-0.30f, -1.0f, 0.0f,
		-0.25f, 1.0f, 0.0f,
		-0.25f, -1.0f, 0.0f,
		-0.20f, 1.0f, 0.0f,
		-0.20f, -1.0f, 0.0f,
		-0.15f, 1.0f, 0.0f,
		-0.15f, -1.0f, 0.0f,
		-0.10f, 1.0f, 0.0f,
		-0.10f, -1.0f, 0.0f,
		-0.05f, 1.0f, 0.0f,
		-0.05f, -1.0f, 0.0f,
		//46
		0.00f, 1.0f, 0.0f,
		0.00f, -1.0f, 0.0f,
		0.05f, 1.0f, 0.0f,
		0.05f, -1.0f, 0.0f,
		0.10f, 1.0f, 0.0f,
		0.10f, -1.0f, 0.0f,
		0.15f, 1.0f, 0.0f,
		0.15f, -1.0f, 0.0f,
		0.20f, 1.0f, 0.0f,
		0.20f, -1.0f, 0.0f,
		0.25f, 1.0f, 0.0f,
		0.25f, -1.0f, 0.0f,
		0.30f, 1.0f, 0.0f,
		0.30f, -1.0f, 0.0f,
		0.35f, 1.0f, 0.0f,
		0.35f, -1.0f, 0.0f,
		0.40f, 1.0f, 0.0f,
		0.40f, -1.0f, 0.0f,
		0.45f, 1.0f, 0.0f,
		0.45f, -1.0f, 0.0f,
		0.50f, 1.0f, 0.0f,
		0.50f, -1.0f, 0.0f,
		0.55f, 1.0f, 0.0f,
		0.55f, -1.0f, 0.0f,
		0.60f, 1.0f, 0.0f,
		0.60f, -1.0f, 0.0f,
		0.65f, 1.0f, 0.0f,
		0.65f, -1.0f, 0.0f,
		0.70f, 1.0f, 0.0f,
		0.70f, -1.0f, 0.0f,
		0.75f, 1.0f, 0.0f,
		0.75f, -1.0f, 0.0f,
		0.80f, 1.0f, 0.0f,
		0.80f, -1.0f, 0.0f,
		0.85f, 1.0f, 0.0f,
		0.85f, -1.0f, 0.0f,
		0.90f, 1.0f, 0.0f,
		0.90f, -1.0f, 0.0f,
		0.95f, 1.0f, 0.0f,
		0.95f, -1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		1.05f, 1.0f, 0.0f,
		1.05f, -1.0f, 0.0f,
		1.10f, 1.0f, 0.0f,
		1.10f, -1.0f, 0.0f,

		//Horizantal Lines  
		//24
		1.15f, -0.60f, 0.0f,
		-1.15f, -0.60f, 0.0f,
		1.15f, -0.55f, 0.0f,
		-1.15f, -0.55f, 0.0f,
		1.15f, -0.50f, 0.0f,
		-1.15f, -0.50f, 0.0f,
		1.15f, -0.45f, 0.0f,
		-1.15f, -0.45f, 0.0f,
		1.15f, -0.40f, 0.0f,
		-1.15f, -0.40f, 0.0f,
		1.15f, -0.35f, 0.0f,
		-1.15f, -0.35f, 0.0f,
		1.15f, -0.30f, 0.0f,
		-1.15f, -0.30f, 0.0f,
		1.15f, -0.25f, 0.0f,
		-1.15f, -0.25f, 0.0f,
		1.15f, -0.20f, 0.0f,
		-1.15f, -0.20f, 0.0f,
		1.15f, -0.15f, 0.0f,
		-1.15f, -0.15f, 0.0f,
		1.15f, -0.10f, 0.0f,
		-1.15f, -0.10f, 0.0f,
		1.15f, -0.05f, 0.0f,
		-1.15f, -0.05f, 0.0f,
		//26
		1.15f, 0.0f, 0.0f,
		-1.15f, 0.0f, 0.0f,
		1.15f, 0.05f, 0.0f,
		-1.15f, 0.05f, 0.0f,
		1.15f, 0.10f, 0.0f,
		-1.15f, 0.10f, 0.0f,
		1.15f, 0.15f, 0.0f,
		-1.15f, 0.15f, 0.0f,
		1.15f, 0.20f, 0.0f,
		-1.15f, 0.20f, 0.0f,
		1.15f, 0.25f, 0.0f,
		-1.15f, 0.25f, 0.0f,
		1.15f, 0.30f, 0.0f,
		-1.15f, 0.30f, 0.0f,
		1.15f, 0.35f, 0.0f,
		-1.15f, 0.35f, 0.0f,
		1.15f, 0.40f, 0.0f,
		-1.15f, 0.40f, 0.0f,
		1.15f, 0.45f, 0.0f,
		-1.15f, 0.45f, 0.0f,
		1.15f, 0.50f, 0.0f,
		-1.15f, 0.50f, 0.0f,
		1.15f, 0.55f, 0.0f,
		-1.15f, 0.55f, 0.0f,
		1.15f, 0.60f, 0.0f,
		-1.15f, 0.60f, 0.0f
	};

	//GraphVerticesArray
	const  GLfloat shapesVertices[] =
	{
		//Ractangle
		0.97f, 0.7f, 0.0f,
		-0.97f, 0.7f, 0.0f,
		-0.97f, 0.7f, 0.0f,
		-0.97f, -0.7f, 0.0f,
		-0.97f, -0.7f, 0.0f,
		0.97f, -0.7f, 0.0f,
		0.97f, 0.7f, 0.0f,
		0.97f, -0.7f, 0.0f,

		//Triangle
		0.0f, 0.7f, 0.0f,
		-0.97f, -0.7f, 0.0f,
		-0.97f, -0.7f, 0.0f,
		0.97f, -0.7f, 0.0f,
		0.97f, -0.7f, 0.0f,
		0.0f, 0.7f, 0.0f
	};

	const int ipoints = 300;
	GLfloat bigCircleVertices[3 * ipoints];
	for (int i = 0; i < ipoints; i++)
	{
		GLfloat Angle = (2.0f * (GLfloat)M_PI * i) / ipoints;

		bigCircleVertices[3 * i] = (GLfloat)cos(Angle) * 1.2f;
		bigCircleVertices[3 * i + 1] = (GLfloat)sin(Angle) * 1.2f;
		bigCircleVertices[3 * i + 2] = 0.0f;
	}

	GLfloat smallCircleVertices[3 * ipoints];
	for (int i = 0; i < ipoints; i++)
	{
		GLfloat Angle = (2.0f * (GLfloat)M_PI * i) / ipoints;

		smallCircleVertices[3 * i] = (GLfloat)cos(Angle) * 0.507f;
		smallCircleVertices[3 * i + 1] = (GLfloat)sin(Angle) * 0.507f;
		smallCircleVertices[3 * i + 2] = 0.0f;
	}

	const int Color = 140;
	GLfloat graphColor[3 * Color];
	for (int i = 0; i < Color; i++)
	{
		graphColor[3 * i] = 0.0f;
		graphColor[3 * i + 1] = 1.0f;
		graphColor[3 * i + 2] = 0.0f;
	}
	
	const int Colors = 300;
	GLfloat shapesColor[3 * Colors];
	for (int i = 0; i < Colors; i++)
	{
		shapesColor[3 * i] = 1.0f;
		shapesColor[3 * i + 1] = 1.0f;
		shapesColor[3 * i + 2] = 0.0f;
	}

	//Create vao -----> For Graph
	glGenVertexArrays(1, &vao_Graph);
	glBindVertexArray(vao_Graph);
	glGenBuffers(1, &vbo_Graph);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Graph);
	glBufferData(GL_ARRAY_BUFFER, sizeof(graphVertices), graphVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	//Color
	glGenBuffers(1, &vbo_Color_Graph);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Graph);
	glBufferData(GL_ARRAY_BUFFER, sizeof(graphColor), graphColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);


	//Create vao -----> For BigCircle
	glGenVertexArrays(1, &vao_BigCircle);
	glBindVertexArray(vao_BigCircle);
	glGenBuffers(1, &vbo_BigCircle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_BigCircle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(bigCircleVertices), bigCircleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Color
	glGenBuffers(1, &vbo_Color_Shapes);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Shapes);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shapesColor), shapesColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	//Create vao ----> Rectangle And Triangle
	glGenVertexArrays(1, &vao_Shapes);
	glBindVertexArray(vao_Shapes);
	glGenBuffers(1, &vbo_Shapes);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Shapes);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shapesVertices), shapesVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	//Color
	glGenBuffers(1, &vbo_Color_Shapes);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Shapes);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shapesColor), shapesColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	//Create vao-----> For SmallCircle
	glGenVertexArrays(1, &vao_SmallCircle);
	glBindVertexArray(vao_SmallCircle);
	glGenBuffers(1, &vbo_SmallCircle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_SmallCircle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(smallCircleVertices), smallCircleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Color
	glGenBuffers(1, &vbo_Color_Shapes);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Shapes);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shapesColor), shapesColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	perspectiveProjectionMatrix = vmath::mat4::identity();
	//Warmup Call To Resize
	Resize_RMB(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

void Resize_RMB(int Width, int Height)
{
	if (Height == 0)
	{
		Height = -1;
	}

	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);
}

void Display_RMB(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	//Declatation of Matricex
	vmath::mat4 translationMatrix;
	vmath::mat4 modelViewMatrix;
	vmath::mat4 modelViewProjectionMatrix;
	//For Graph
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.0f, 0.0f, -1.6f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glLineWidth(0.001f);
	glBindVertexArray(vao_Graph);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 500);

	//UnBind vao
	glBindVertexArray(0);

	//For Shapes
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	
	glBindVertexArray(vao_Shapes);
	glLineWidth(5.0f);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 500);
	//UnBind vao
	glBindVertexArray(0);

	//For Big Circle
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vao_BigCircle);
	glLineWidth(5.0f);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 500);
	//UnBind vao
	glBindVertexArray(0);

	//For Small Circle
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.0f, -0.193f, -3.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	
	glBindVertexArray(vao_SmallCircle);
	glLineWidth(5.0f);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 500);
	//UnBind vao
	glBindVertexArray(0);

	glUseProgram(0);
	SwapBuffers(ghdc);
}

void Uninitialize_RMB(void)
{
	if (vbo_Color_Shapes)
	{
		glDeleteBuffers(1, &vbo_Color_Shapes);
		vbo_Color_Shapes = 0;
	}

	if (vbo_SmallCircle)
	{
		glDeleteBuffers(1, &vbo_SmallCircle);
		vbo_SmallCircle = 0;
	}

	if (vbo_Shapes)
	{
		glDeleteBuffers(1, &vbo_Shapes);
		vbo_Shapes = 0;
	}

	if (vbo_BigCircle)
	{
		glDeleteBuffers(1, &vbo_BigCircle);
		vbo_BigCircle = 0;
	}

	if (vbo_Color_Graph)
	{
		glDeleteBuffers(1, &vbo_Color_Graph);
		vbo_Color_Graph = 0;
	}

	if (vbo_Graph)
	{
		glDeleteBuffers(1, &vbo_Graph);
		vbo_Graph = 0;
	}



	if (vao_SmallCircle)
	{
		glDeleteVertexArrays(1, &vao_SmallCircle);
		vao_SmallCircle = 0;
	}

	if (vao_Shapes)
	{
		glDeleteVertexArrays(1, &vao_Shapes);
		vao_Shapes = 0;
	}

	if (vao_BigCircle)
	{
		glDeleteVertexArrays(1, &vao_BigCircle);
		vao_BigCircle = 0;
	}

	if (vao_Graph)
	{
		glDeleteVertexArrays(1, &vao_Graph);
		vao_Graph = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);
		//Ask Program How Many Shaders are Attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	//Break the Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf_s(gpFile, "Log file is closed Successfully. \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
	}
}

