//Header Files
#include<Windows.h>
#include<stdio.h>  // For File I/O

#include<gl/GLEW.h> // For GLES Extention Important : This Line Should Be Before #include<gl/GL.h> And 
#include<gl/GL.h>
#include"vmath.h"

//Library Files
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")

//User define Micro
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTCOORD0
};

//Global  variable Declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;
FILE* gpFile = NULL;
bool gbActiveWindow = false;
bool bIsFullScreen = false;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

//Programmable Pipeline Variable
GLuint gVertexShaderObject;
GLuint gTessellationControlShaderObject;
GLuint gTessellationEvaluationShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao;
GLuint vbo;
GLuint mvpUniform;

GLuint gNumberOfSegmentsUniform;
GLuint gNumberOfStripsUniform;
GLuint gLineColorUniform;

vmath::mat4 perspectiveProjectionMatrix;

unsigned int gNumberOfLineSegments;

//Function Declaration(Prototype/ Signature)
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void Uninitialize_RMB(void);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	int Initialize_RMB(void);
	void Display_RMB(void);

	//Local Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR str[] = TEXT("BlueScreen");
	bool bDone = false;
	int iRet = 0;
	int X, Y;

	//File io
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created"), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file is successfully complited...!!!\n");
	}

	//Initialize WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = str;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register WinclassEx
	RegisterClassEx(&wndclass);

	//CenterOfTheWindowCalculation
	X = (GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2);
	Y = (GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2);

	//CreateWindow
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, str, TEXT("RahulUMB...30-TessellationShader...!!!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X,
		Y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = Initialize_RMB();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "ChoosePixelFormat Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "SetPixelFormat is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, " wglCreateContext is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "wglMakeCurrent Failed.\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "Initializetion Successfully Complited.\n");
	}

	//Here Actual ShowWindow
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//MessageLoop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

		}
		else
		{
			if (gbActiveWindow == true)
			{
				//update();
			}
			Display_RMB();
		}
	}

	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void Resize_RMB(int, int);
	void ToggleFullScreen(void);

	//variable declaration
	static WORD xMouse = NULL;
	static WORD yMouse = NULL;

	switch (iMsg)
	{
		//code
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;

		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;

		case WM_SIZE:
			Resize_RMB(LOWORD(lParam), HIWORD(lParam));
			break;

		case WM_ERASEBKGND:
			return(0);

		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;

		case WM_KEYDOWN:
			switch (wParam)  //in Past switch(LOWORD(wParam))
			{
				case VK_UP:
					gNumberOfLineSegments++;
					if (gNumberOfLineSegments >= 50)
					{
						gNumberOfLineSegments = 50; //reset
					}
					break;

				case VK_DOWN:
					gNumberOfLineSegments--;
					if (gNumberOfLineSegments <= 0)
					{
						gNumberOfLineSegments = 1; //reset
					}
					break;

				case VK_ESCAPE:
					ToggleFullScreen();
					break;
			}
			break;

		case WM_CHAR:
			switch (wParam)
			{
				case 'Q':
				case 'q':
				case 'E':
				case 'e':
					DestroyWindow(hwnd);
					break;
			}
			break;

		case WM_DESTROY:
			Uninitialize_RMB();
			PostQuitMessage(0);
			break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize_RMB(void)
{
	//Function Declaration
	void Resize_RMB(int, int);

	//Local Variable Declaration
	GLenum result;
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	//Initialize pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));  //or   //For only Windows
	//memset((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));  //For All OS
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	//the return index is always 1 based to 38 if Zero(0) return failure 
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf_s(gpFile, "glewInit(); is Failed.\n");
		Uninitialize_RMB();
		DestroyWindow(ghwnd);
	}

	//Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec2 vPosition;" \
		"void main(void)" \
		"{" \
			"gl_Position = vec4(vPosition, 0.0, 1.0);" \
		"}";

	//Specifing above source to the vertex shader object
	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//Compile the Vertex Shader
	glCompileShader(gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "vertexShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				Uninitialize_RMB();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//*** TESSELLATION CONTROL SHADER ***
	//Create Shader
	gTessellationControlShaderObject = glCreateShader(GL_TESS_CONTROL_SHADER);

	//Provide Source Code To Shader
	const GLchar* tessellationControlShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"layout(vertices=4)out;" \
		"uniform int numberOfSegments;" \
		"uniform int numberOfStrips;" \
		"void main(void)" \
		"{" \
			"gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;" \
			"gl_TessLevelOuter[0] = float(numberOfStrips);" \
			"gl_TessLevelOuter[1] = float(numberOfSegments);" \
		"}";
	glShaderSource(gTessellationControlShaderObject, 1, (const GLchar**)&tessellationControlShaderSourceCode, NULL);

	//Compile Shader
	glCompileShader(gTessellationControlShaderObject);
	//re-initialize
	iInfoLogLength = 0;
	iShaderCompileStatus = 0;
	szInfoLog = NULL;
	glGetShaderiv(gTessellationControlShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	{
		if (iShaderCompileStatus == GL_FALSE)
		{
			glGetShaderiv(gTessellationControlShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
			if (iInfoLogLength > 0)
			{
				szInfoLog = (char*)malloc(iInfoLogLength);
				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(gTessellationControlShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpFile, "Tessellation Control Shader Compilation Log : %s...\n", szInfoLog);
					free(szInfoLog);
					Uninitialize_RMB();
					exit(0);
				}
			}
		}
	}

	//*** TESSELLATION EVALUATION SHADER
	//Create Shader
	gTessellationEvaluationShaderObject = glCreateShader(GL_TESS_EVALUATION_SHADER);

	//Provide Source Code to Shader
	const GLchar* tessellationEvaluationShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"layout(isolines)in;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
			"float u = gl_TessCoord.x;" \
			"vec3 p0 = gl_in[0].gl_Position.xyz;" \
			"vec3 p1 = gl_in[1].gl_Position.xyz;" \
			"vec3 p2 = gl_in[2].gl_Position.xyz;" \
			"vec3 p3 = gl_in[3].gl_Position.xyz;" \
			"float u1 = (1.0 - u);" \
			"float u2 = u * u;" \
			"float b3 = u2 * u;" \
			"float b2 = 3.0 * u2 * u1;" \
			"float b1 = 3.0 * u * u1 * u1;" \
			"float b0 = u1 * u1 * u1;" \
			"vec3 p = p0 * b0 + p1 * b1 + p2 * b2 + p3 * b3;" \
			"gl_Position = u_mvp_matrix * vec4(p, 1.0);" \
		"}";

	glShaderSource(gTessellationEvaluationShaderObject, 1, (const GLchar **) & tessellationEvaluationShaderSourceCode, NULL);

	//Compile shader
	glCompileShader(gTessellationEvaluationShaderObject);
	//re-initialize
	iInfoLogLength = 0;
	iShaderCompileStatus = 0;
	szInfoLog = NULL;
	glGetShaderiv(gTessellationEvaluationShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	{
		if (iShaderCompileStatus == GL_FALSE)
		{
			glGetShaderiv(gTessellationEvaluationShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
			if(iInfoLogLength > 0)
			{
				szInfoLog = (char*)malloc(iInfoLogLength);
				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(gTessellationEvaluationShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpFile, "Tessellation Evaluation Shader Compilation Log : %s...\n", szInfoLog);
					free(szInfoLog);
					Uninitialize_RMB();
					exit(0);
				}
			}
		}
	}
	//*** Fragment Shader
	//Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"uniform vec4 lineColor;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
			"fragColor = lineColor;" \
		"}";


	// Specifing above Source to the fragment Shader Object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//Compile the fragment Shader Object
	glCompileShader(gFragmentShaderObject);

	GLint iFShaderCompileStatus = 0;
	GLint iFInfoLogLength = 0;
	GLint iInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iFShaderCompileStatus);
	if (iFShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iFInfoLogLength);
		if (iFInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iFInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iFInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "FragmentShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				Uninitialize_RMB();
				exit(0);
			}
		}
	}

	//Create Shader Object
	gShaderProgramObject = glCreateProgram();

	//Attach VertexShader to the Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//Attach Tessellation Control Shader to Shader Program 
	glAttachShader(gShaderProgramObject, gTessellationControlShaderObject);

	//Attacha Tessellation Evaluation shader to Shader Program 
	glAttachShader(gShaderProgramObject, gTessellationEvaluationShaderObject);

	//Attach fragmentShader to the Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Pre Linking Binding to Vertex Attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject);

	GLint  iProgramLinkStatus = 0;
	GLint iLInfoLogLength = 0;
	GLint *szLogLength = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iLInfoLogLength);
		if (iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(iLInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iLInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "Shader Program Link Log:- %s", szInfoLog);
				free(szInfoLog);
				Uninitialize_RMB();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Post Linking Retriving Uniform Location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	//Get number of segments uniform location
	gNumberOfSegmentsUniform = glGetUniformLocation(gShaderProgramObject, "numberOfSegments");

	//Get Number Of Strips Uniform Location
	gNumberOfStripsUniform = glGetUniformLocation(gShaderProgramObject, "numberOfStrips");

	//get line Color Uniform Location
	gLineColorUniform = glGetUniformLocation(gShaderProgramObject, "lineColor");
	
	//TrangleArray
	const  GLfloat verties[] = { -1.0f, -1.0f, -0.5f, 1.0f, 0.5f, -1.0f, 1.0f, 1.0f	};

	//Create vao
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float), verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	//Set-Up Depth Buffer
	glClearDepth(1.0f);
	//Enable Depth Testing
	glEnable(GL_DEPTH_TEST);
	//depth test to do
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	perspectiveProjectionMatrix = vmath::mat4::identity();

	gNumberOfLineSegments = 1;
	//Warmup Call To Resize
	Resize_RMB(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

void Resize_RMB(int Width, int Height)
{
	//code
	if (Height == 0)
	{
		Height = 1;
	}
	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);
}

void Display_RMB(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	//Declatation of Matrices
	vmath::mat4 modelViewMatrix;
	vmath::mat4 modelViewProjectionMatrix;

	//Initialize of Matrix in identity
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();


	//Do Necessary Transformation Code Here
	modelViewMatrix = vmath::translate(0.5f, 0.5f, -2.0f);

	//Do Necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//send other uniform
	glUniform1i(gNumberOfSegmentsUniform, gNumberOfLineSegments);
	TCHAR str[255];
	wsprintf(str, TEXT("OpenGL Programmable Pipeline Window : [Segments = %d]"), gNumberOfLineSegments);
	SetWindowText(ghwnd, str);
	glUniform1i(gNumberOfStripsUniform, 1);
	glUniform4fv(gLineColorUniform, 1, vmath::vec4(1.0f, 1.0f, 0.0f, 1.0f));
	//BindWith vao
	glBindVertexArray(vao);

	glPatchParameteri(GL_PATCH_VERTICES, 4);
	//Draw the Necessary Scnes
	glDrawArrays(GL_PATCHES, 0, 4);

	//UnBind vao
	glBindVertexArray(0);

	//UnUse Program
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void Uninitialize_RMB(void)
{
	//code
	if (vbo)
	{
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}
	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gTessellationEvaluationShaderObject);
	glDetachShader(gShaderProgramObject, gTessellationControlShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteShader(gTessellationEvaluationShaderObject);
	gTessellationEvaluationShaderObject = 0;

	glDeleteShader(gTessellationControlShaderObject);
	gTessellationControlShaderObject = 0;

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);

	if (bIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//Break the Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf_s(gpFile, "Log file is closed Successfully. \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(false);
		bIsFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bIsFullScreen = false;
	}

}

