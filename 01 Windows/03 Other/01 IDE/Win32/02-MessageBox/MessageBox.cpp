//heade file
#include<windows.h>

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//local declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR  szAppName[] = TEXT("MessageBox");

	//WndClass Initialization
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL,IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//RegisterClass
	RegisterClassEx(&wndclass);

	//CreateWindow
	hwnd = CreateWindow(szAppName,
		TEXT("MessageBox...Rahul.M.Bhadrashette...!!!"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	//Show Window 
	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//Message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

//callback
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//code
	switch (iMsg)
	{
	case WM_CREATE:
		MessageBox(hwnd, TEXT("This is CREATE...!!!"), TEXT("MessageBox...!!!"), MB_OKCANCEL);
		break;

	case WM_LBUTTONDOWN:
		MessageBox(hwnd, TEXT("This is LBUTTONDOWN...!!!"), TEXT("Message...!!!"), MB_OKCANCEL);
		break;

	case WM_RBUTTONDOWN:
		MessageBox(hwnd, TEXT("This is RBUTTONDOWN...!!!"), TEXT("MessageBox...!!!"), MB_OKCANCEL);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			MessageBox(hwnd, TEXT("This is KEYDOWN in Escape Key...!!!"), TEXT("MessageBox...!!!"), MB_OKCANCEL);
			DestroyWindow(hwnd);
			break;

		case 0x46:
			MessageBox(hwnd, TEXT("Thi is F Key Pressed...!!!"), TEXT("MessageBox...!!!"), MB_OKCANCEL);
			break;
		}
			break;

	case WM_DESTROY:
		MessageBox(hwnd, TEXT("This is DESTROY WINDOW...!!!"), TEXT("MessageBox...!!!"), MB_OKCANCEL);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
