#include<stdio.h>
int main(void)
{ 
	float Resistivity(float, float);
	float Resistance(float, float, float);

	float E, J, Answer;
	float R, A, L;

	printf("\n\n\n\n");
	printf("********  Resistivity: [Ohm Meters]  **********");
	
	printf("\n\n");
	printf("\t  Electric Field [N/C] :- ");
	scanf("%f", &E);

	printf("\n\n");
	printf("\t  Current Density [A/m2] :- ");
	scanf("%f", &J);

	Answer = Resistivity(E, J);
	printf("\n\n");
	printf("\t  Resistivity [Ohm.m] :- %f", Answer);
	printf("\n\n");

	printf("\n\n\n\n");
	printf("********  Resistance: [Ohm Meters]  ***********");
	
	printf("\n\n");
	printf("\t  Resistance [ohms] :- ");
	scanf("%f", &R);

	printf("\n\n");
	printf("\t  Area [m2] :- ");
	scanf("%f", &A);

	printf("\n\n");
	printf("\t  Length Of Conductor [m] :- ");
	scanf("%f", &L);

	Answer = Resistance(R, A, L);
	printf("\n\n");
	printf("\t  Resistance [Ohm.m] :- %f", Answer);
	printf("\n\n");

	
	return(0);
}

float Resistivity(float E, float J)
{
	float reg;

	reg = E/J;
	return(reg);

}

float Resistance(float R, float A, float L)
{
	float result;

	result = R*A/L;
	return(result);
}