#include<stdio.h>

struct MyFile
{
	int p;
	int q;
};

int main(void)
{
	struct MyFile point_a, point_b, point_c, point_d, point_e;

	printf("\n\n");
	printf("Enter X-Cordinate For Point 'a' :- ");
	scanf("%d", &point_a.p);
	printf("Enter Y-Cordinate For Point 'a' :- ");
	scanf("%d", &point_a.q);

	printf("Enter X-Cordinate For Point 'b' :- ");
	scanf("%d", &point_b.p);
	printf("Enter Y-Cordinate For Point 'b' :- ");
	scanf("%d", &point_b.q);

	printf("Enter X-Cordinate For Point 'c' :- ");
	scanf("%d", &point_c.p);
	printf("Enter Y-Cordinate For Point 'c' :- ");
	scanf("%d", &point_c.q);

	printf("Enter X-Cordinate For Point 'd' :- ");
	scanf("%d", &point_d.p);
	printf("Enter Y-Cordinate For Point 'd' :- ");
	scanf("%d", &point_d.q);

	printf("Enter X-Cordinate For Point 'e' :- ");
	scanf("%d", &point_e.p);
	printf("Enter Y-Cordinate For Point 'e' :- ");
	scanf("%d", &point_e.q);

	printf("\n\n");
	printf("Co-Ordinate (X, Y) Of Point 'A' Are : (%d, %d)\n\n", point_a.p, point_a.q );
	printf("Co-Ordinate (X, Y) Of Point 'B' Are : (%d, %d)\n\n", point_b.p, point_b.q );
	printf("Co-Ordinate (X, Y) Of Point 'C' Are : (%d, %d)\n\n", point_c.p, point_c.q );
	printf("Co-Ordinate (X, Y) Of Point 'D' Are : (%d, %d)\n\n", point_d.p, point_d.q );
	printf("Co-Ordinate (X, Y) Of Point 'E' Are : (%d, %d)\n\n", point_e.p, point_e.q );
	
	return(0);
}