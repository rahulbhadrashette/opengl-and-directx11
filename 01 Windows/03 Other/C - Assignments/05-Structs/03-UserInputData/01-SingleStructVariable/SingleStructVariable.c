#include<stdio.h>
#include<conio.h>

struct MyFile
{
	int a;
	float b;
	double c;
	char d;
};

int main(void)
{
	struct MyFile file;

	printf("\n\n");
	
	printf("Enter Integer Value For Data Member 'i' Of 'struct MyFile' : \n ");
	scanf("%d", &file.a);

	printf("Enter Floating-Point Value For Data Member 'f' Of 'struct MyFile' : \n ");
	scanf("%f", &file.b);

	printf("Enter Double Value For Data Member 'd' Of 'struct MyFile' : \n ");
	scanf("%lf", &file.c);

	printf("Enter Charcter Value For Data Member 'c' Of 'struct MyFile' : \n ");
	file.d = getch();
	
	printf("\n\n");
	printf("FILE MAMBER OF 'struct MyFile'ARE : \n\n");
	printf("a = %d\n", file.a);
	printf("b = %f\n", file.b);
	printf("c = %lf\n", file.c);
	printf("d = %c\n", file.d);
		
	return(0);
}