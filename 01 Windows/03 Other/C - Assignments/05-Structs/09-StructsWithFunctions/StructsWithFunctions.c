#include<stdio.h>

struct MyFile
{
	int a;
	float b;
	double c;
	char d;
};

int main(void)
{
	struct MyFile AddStructMember(struct MyFile, struct MyFile, struct MyFile);

	struct MyFile file1, file2, file3, Ans_file;

	//*** file1 ***
	printf("\n\n\n\n");
	printf("************ FILE 1 **************\n\n");
	printf("Enter Integer Value For 'i' Of 'struct MyFile file1' :- ");
	scanf("%d", &file1.a);

	printf("\n\n");
	printf("Enter Floating-Point Value For 'f' Of 'struct MyFile file1' :- ");
	scanf("%f", &file1.b);

	printf("\n\n");
	printf("Enter 'Double' Value For 'd' Of 'struct MyFile file1' :- ");
	scanf("%lf", &file1.c);

	printf("\n\n");
	printf("Enter Character Value For 'c' Of 'struct MyFile file1' :- ");
	file1.d = getch();
	scanf("%c", &file1.d);

	//*** file2 ***
	printf("\n\n\n\n");
	printf("************ FILE 2 **************\n\n");
	printf("Enter Integer Value For 'i' Of 'struct MyFile file2' :- ");
	scanf("%d", &file2.a);

	printf("\n\n");
	printf("Enter Floating-Point Value For 'f' Of 'struct MyFile file2' :- ");
	scanf("%f", &file2.b);

	printf("\n\n");
	printf("Enter 'Double' Value For 'd' Of 'struct MyFile file2' :- ");
	scanf("%lf", &file2.c);

	printf("\n\n");
	printf("Enter Character Value For 'c' Of 'struct MyFile file2' :- ");
	file2.d = getch();
	scanf("%c", &file2.d);

	//*** file3 ***
	printf("\n\n\n\n");
	printf("************ FILE 3 **************\n\n");
	printf("Enter Integer Value For 'i' Of 'struct MyFile file3' :- ");
	scanf("%d", &file3.a);

	printf("\n\n");
	printf("Enter Floating-Point Value For 'f' Of 'struct MyFile file3' :- ");
	scanf("%f", &file3.b);

	printf("\n\n");
	printf("Enter 'Double' Value For 'd' Of 'struct MyFile file3' :- ");
	scanf("%lf", &file3.c);

	printf("\n\n");
	printf("Enter Character Value For 'c' Of 'struct MyFile file3' :- ");
	file3.d = getch();
	scanf("%c", &file3.d);

	Ans_file = AddStructMember(file1, file2, file3);

	printf("\n\n\n\n");
	printf("******* ANSWERC *******\n\n");
	printf("Ans_file.i = %d\n", Ans_file.a);
	printf("Ans_file.b = %f\n", Ans_file.b);
	printf("Ans_file.c = %lf\n\n", Ans_file.c);

	Ans_file.d = file1.d;
	printf("Ans_file.d(from file1) = %c\n\n", Ans_file.d);

	Ans_file.d = file2.d;
	printf("Ans_file.d(from file2) = %c\n\n", Ans_file.d);

	Ans_file.d = file3.d;
	printf("Ans_file.d(from file3) = %c\n\n", Ans_file.d);

	return(0);
}

struct MyFile AddStructMember(struct MyFile md_one, struct MyFile md_two, struct MyFile md_three)
{
	struct MyFile Ans;
	
	Ans.a = md_one.a + md_two.a + md_three.a;
	Ans.b = md_one.b + md_two.b + md_three.b;
	Ans.c = md_one.c + md_two.c + md_three.c;

	return(Ans);
}