#include<stdio.h>

struct MyFile
{
	int a;
	float b;
	double c;
	char d;
};

int main(void)
{
	struct MyFile file;

	file.a = 45;
	file.b = 1.23f;
	file.c = 3.125468;
	file.d = 'M';

	printf("\n\n");
	printf("FILE MEMBER OF 'struct MyFile' Are :- \n\n");
	printf("a = %d\n\n", file.a);
	printf("b = %f\n\n", file.b);
	printf("c = %lf\n\n", file.c);
	printf("d = %c\n\n", file.d);

	printf("\n\n");
	printf("Addresses Of fileMember Of fileruct file1 Are :- \n\n");
	printf("'a' Occupies Addresses From  = %p\n\n", &file.a);
	printf("'b' Occupies Addresses From = %p\n\n", &file.b);
	printf("'c' Occupies Addresses From = %p\n\n", &file.c);
	printf("'d' Occupies Addresses = %p\n\n", &file.d);

	
	printf("Starting Address Of 'Struct MyFile' variable 'file' = %p\n\n", &file);

	return(0);
}