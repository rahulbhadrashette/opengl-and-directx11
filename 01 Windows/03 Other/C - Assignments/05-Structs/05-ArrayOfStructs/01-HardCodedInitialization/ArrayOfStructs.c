#include<stdio.h>

#define NAME_LENGTH 100
#define MARITAL_STATUS 10

struct Employee
{
	char name[NAME_LENGTH];
	int age;
	float salary;
	char sex;
	char marital_status[MARITAL_STATUS];
};

int main(void)
{
	struct Employee EmployeeRecord[5];
		
	char employee_sachin[] = "Sachin";
	char employee_sumit[] = "Sumit";
	char employee_rahul[] = "Rahul";
	char employee_vinod[] = "Vinod";
	char employee_chandu[] = "Chandrashekhar";
	
	int a;
	
	//******** Hard-Code Initialization of array of 'struct employee' *****

	//*** Employee 1 *******
	strcpy(EmployeeRecord[0].name, employee_sachin);
	EmployeeRecord[0].age = 28;
	EmployeeRecord[0].sex ='M';
	EmployeeRecord[0].salary = 100000.0f;
	strcpy(EmployeeRecord[0].marital_status, "Married");


	//*** Employee 2 *******
	strcpy(EmployeeRecord[1].name, employee_sumit);
	EmployeeRecord[1].age = 26;
	EmployeeRecord[1].sex ='M';
	EmployeeRecord[1].salary = 30000.0f;
	strcpy(EmployeeRecord[1].marital_status, "Married");


	//*** Employee 3 *******
	strcpy(EmployeeRecord[2].name, employee_rahul);
	EmployeeRecord[2].age = 25;
	EmployeeRecord[2].sex ='M';
	EmployeeRecord[2].salary = 0.0f;
	strcpy(EmployeeRecord[2].marital_status, "Unmarried");


	//*** Employee 4 *******
	strcpy(EmployeeRecord[3].name, employee_vinod);
	EmployeeRecord[3].age = 20;
	EmployeeRecord[3].sex ='M';
	EmployeeRecord[3].salary = 8000.0f;
	strcpy(EmployeeRecord[3].marital_status, "Unmarried");


	//*** Employee 5 *******
	strcpy(EmployeeRecord[4].name, employee_chandu);
	EmployeeRecord[4].age = 22;
	EmployeeRecord[4].sex ='M';
	EmployeeRecord[4].salary = 10000.0f;
	strcpy(EmployeeRecord[4].marital_status, "Unmarried");

	printf("\n\n");
	printf("******** Display Employee Records ******\n\n");
	for(a = 0; a < 5; a++)
	{
		printf("****** Employee Number %d ********\n\n", (a + 1));
		printf("Name          :- %s\n", EmployeeRecord[a].name);
		printf("Age	      :- %d years \n", EmployeeRecord[a].age);
		
		if(EmployeeRecord[a].sex == 'M' || EmployeeRecord[a].sex == 'm')
			printf("sex		:- Male \n");
		else
			printf("sex		:- Female \n");
		printf("Salery		:- Rs.%f\n", EmployeeRecord[a].salary);
		printf("Marital Status	:- %s\n", EmployeeRecord[a].marital_status);
		
		printf("\n\n");
	}
	return(0);
}