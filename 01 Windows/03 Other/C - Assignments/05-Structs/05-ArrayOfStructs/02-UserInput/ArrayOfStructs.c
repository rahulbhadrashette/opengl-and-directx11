#include<stdio.h>
#include<ctype.h>

#define NUM_EMPLOYEES 5

#define NAME_LENGTH 100
#define MARITAL_STATUS 10

struct Employee
{
	char name[NAME_LENGTH];
	int age;
	float salary;
	char sex;
	char marital_status;
};

int main(void)
{
	void MyGetStr(char[], int);

	struct Employee EmployeeRecord[NUM_EMPLOYEES];
	int a;
	
	//******** Hard-Code Initialization of array of 'struct employee' *****
	for(a = 0; a < NUM_EMPLOYEES; a++)
	{
		printf("\n\n\n");
		printf("******** Display Entry For Employee Record Number %d******\n\n",(a + 1));
		//*** Employee 1 *******
		printf("\n\n");
		printf("Enter Employee Name :- ");
		MyGetStr(EmployeeRecord[a].name, NAME_LENGTH);
	
		printf("\n\n\n");
		printf("Enter Employee's Age (In years) :- ");
		scanf("%d", &EmployeeRecord[a].age);

		printf("\n\n");
		printf("Enter Employee's Sex (M/m For Male, F/f For Female) :- ");
		EmployeeRecord[a].sex = getch();
		printf("%c", EmployeeRecord[a].sex);
		EmployeeRecord[a].sex = toupper(EmployeeRecord[a].sex);

		printf("\n\n\n");
		printf("Enter Employee's Salary (In Indian Rupees) :- ");
		scanf("%f", &EmployeeRecord[a].salary);

		printf("\n\n");
		printf("Is The Employee Married? (Y/y For Yes, N/n For No) :- ");
		EmployeeRecord[a].marital_status = getch();
		printf("%c", EmployeeRecord[a].marital_status);
		EmployeeRecord[a].marital_status = toupper(EmployeeRecord[a].marital_status);
	}

	printf("\n\n");
	printf("******** Display Employee Records ******\n\n");
	for(a = 0; a < NUM_EMPLOYEES; a++)
	{
		printf("****** Employee Number %d ********\n\n", (a + 1));
		printf("Name          :- %s\n", EmployeeRecord[a].name);
		printf("Age	      :- %d years \n", EmployeeRecord[a].age);
		
		if(EmployeeRecord[a].sex == 'M')
		{
			printf("sex		:- Male \n");
		}
		else if(EmployeeRecord[a].sex == 'F')
		{
			printf("sex		:- Female \n");
		}

		printf("Salery		:- Rs.%f\n", EmployeeRecord[a].salary);

		if(EmployeeRecord[a].marital_status == 'Y')
		{
			printf("Marital Status	:- Married\n");
		}
		else if(EmployeeRecord[a].marital_status == 'N')
		{
			printf("Marital Status	:- Unmarried\n");
		}
		else
			printf("Marital Status	:- Invalid Data Entered\n");
		
		printf("\n\n");
	}
	return(0);
}

void MyGetStr(char str[], int str_size)
{
	int a;
	char b = '\0';

	a = 0;
	do
	{
		b = getch();
		str[a] = b;
		printf("%c", str[a]);
		a++;
	}while ((b != '\r') && (a < str_size));
	
	if(a == str_size)
		str[a - 1] = '\0';
	else 
		str[a] = '\0';
}