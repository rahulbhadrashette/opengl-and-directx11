#include<stdio.h>
#include<ctype.h>

#define MAX_STRING_LENGTH 1024
	
struct CharacterCount
{
	char z;
	int z_count;
} character_and_count[] = {{ 'A', 0 }, { 'B', 0}, { 'C', 0 }, { 'D', 0}, { 'E', 0 }, { 'F', 0},
			   { 'G', 0 }, { 'H', 0}, { 'I', 0 }, { 'G', 0}, { 'K', 0 }, { 'L', 0},
			   { 'M', 0 }, { 'N', 0}, { 'O', 0 }, { 'P', 0}, { 'Q', 0 }, { 'R', 0},
			   { 'S', 0 }, { 'T', 0}, { 'U', 0 }, { 'V', 0}, { 'W', 0 }, { 'X', 0},
			   { 'Y', 0 }, { 'Z', 0}};

#define SIZE_OF_ENTIER_ARRAY_OF_STRUCTS sizeof(character_and_count)
#define SIZE_OF_ONE_STRUCT_FROM_THE_ARRAY_OF_STRUCTS sizeof(character_and_count[0])
#define NUM_ELEMENTS_IN_ARRAY (SIZE_OF_ENTIER_ARRAY_OF_STRUCTS / SIZE_OF_ONE_STRUCT_FROM_THE_ARRAY_OF_STRUCTS)

int main(void)
{
	char str[MAX_STRING_LENGTH];
	int a,b, actual_string_length = 0;

	actual_string_length = strlen(str);

	printf("\n\n");
	printf("The String You Have Entered Is :- \n\n");
	printf("%s\n\n", str);

	for(a = 0; a < actual_string_length; a++)
	{
		for (b = 0; b < NUM_ELEMENTS_IN_ARRAY; b++)
		{
			str[a] = toupper(str[a]);
		
			if(str[a] == character_and_count[b].z)
				character_and_count[b].z_count++;
		}
	}
	
	printf("\n\n");
	printf("The Number Of Occurences Of All Character From The Alphabet Are As Follows :- \n\n");
	for(a = 0; a < NUM_ELEMENTS_IN_ARRAY; a++)
	{
		printf("Character %c = %d\n", character_and_count[a].z, character_and_count[a].z_count);
	}
	printf("\n\n");

	return(0);
}