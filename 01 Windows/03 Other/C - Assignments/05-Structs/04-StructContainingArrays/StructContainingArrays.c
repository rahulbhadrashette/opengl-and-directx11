#include<stdio.h>

#define INT_ARRAY_SIZE 10
#define FLOAT_ARRAY_SIZE 5
#define CHAR_ARRAY_SIZE 26

#define NUM_STRINGS 10
#define MAX_CHARACTERS_PER_STRING 20

#define ALPHABET_BEGINNING 65

struct MyFileOne
{
	int iArray[INT_ARRAY_SIZE];
	float fArray[FLOAT_ARRAY_SIZE];
};

struct MyFileTwo
{
	int cArray[CHAR_ARRAY_SIZE];
	float strArray[NUM_STRINGS][MAX_CHARACTERS_PER_STRING];
};

int main(void)
{
	struct MyFileOne file_one;
	struct MyFileTwo file_two;
	int a;


	file_one.fArray[0] =0.1f;
	file_one.fArray[1] =1.2f;
	file_one.fArray[2] =2.4f;
	file_one.fArray[3] =3.6f;
	file_one.fArray[4] =4.8f;

	printf("\n\n");
	printf("Enter %d Integer :- \n\n", INT_ARRAY_SIZE);
	for(a = 0; a < INT_ARRAY_SIZE; a++)
		scanf("%d", &file_one.iArray[a]);
	
	for(a = 0; a < CHAR_ARRAY_SIZE; a++)
		file_two.cArray[a] = (char)(a + ALPHABET_BEGINNING);

	strcpy(file_two.strArray[0], "WelCome...");
	strcpy(file_two.strArray[1], "This...");
	strcpy(file_two.strArray[2], "Is...");
	strcpy(file_two.strArray[3], "ASTROMEDICOMP...");
	strcpy(file_two.strArray[4], "Real...");
	strcpy(file_two.strArray[5], "Time...");
	strcpy(file_two.strArray[6], "Rendering...");
	strcpy(file_two.strArray[7], "Batch...");
	strcpy(file_two.strArray[8], "Of...");
	strcpy(file_two.strArray[9], "2018-19-20...!!!");

	printf("\n\n");
	printf("Members Of 'struct FileOne' AlongWith their Assigned Values Are:- \n\n");

	printf("\n\n");
	printf("Intrger Array (file_one.fArray[]) :- \n\n");
	for(a = 0; a < INT_ARRAY_SIZE; a++)
		printf("file_one.iArray[%d] = %d\n", a, file_one.fArray[a]);

	printf("\n\n");
	printf("Floating-Point Array (file_one.fArray[]) :- \n\n");
	for(a = 0; a < FLOAT_ARRAY_SIZE; a++)
		printf("file_one.iArray[%d] = %f\n", a, file_one.fArray[a]);
	
	printf("\n\n");
	printf("Members Of 'struct FileTwo' AlongWith their Assigned Values Are:- \n\n");

	printf("\n\n");
	printf("Character Array (file_two.cArray[]) :- \n\n");
	for(a = 0; a < CHAR_ARRAY_SIZE; a++)
		printf("file_two.cArray[%d] = %d\n", a, file_two.cArray[a]);

	printf("\n\n");
	printf("String Array (file_two.strArray[]) :- \n\n");
	for(a = 0; a < NUM_STRINGS; a++)
		printf("%s ", file_two.strArray[a]);
	
	printf("\n\n");
	return(0);
}

