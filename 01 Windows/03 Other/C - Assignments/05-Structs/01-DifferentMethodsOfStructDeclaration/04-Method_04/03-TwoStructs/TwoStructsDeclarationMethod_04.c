#include<stdio.h>

struct MyPoints 
{
	int a;
	int b;
};

struct MyPointsProperties 
{
	int Quadrant;
	char axis_location[10];
};

int main(int argc, char *argv[], char *envp[])
{
	struct MyPoints poi;
	struct MyPointsProperties point_properties;
	
	printf("\n\n");
	printf("Enter X-Coordintes For a point :- ");
	scanf("%d", &poi.a);
	printf("Enter Y-Coordintes For a point :- ");
	scanf("%d", &poi.b);

	printf("\n\n");
	printf("Point Coordintes(x, y) Are:- (%d, %d)...!!!\n\n", poi.a, poi.b);

	if(poi.a == 0 && poi.b == 0)
		printf("The Point Is The Origin (%d, %d)...!!!\n", poi.a, poi.b);
	else
	{
		if(poi.a ==  0)
		{
			if(poi.b < 0)
				strcpy(point_properties.axis_location, "Negative Y");
			if(poi.b > 0)
				strcpy(point_properties.axis_location, "Positive Y");

			point_properties.Quadrant = 0;
			printf("The Point Lies On The %s Axis ...!!!\n\n", point_properties.axis_location);
		}
		else if(poi.b == 0)
		{
			if(poi.a < 0)
				strcpy(point_properties.axis_location, "Negative X");
			if(poi.a > 0)
				strcpy(point_properties.axis_location, "Positive X");

			point_properties.Quadrant = 0;
			printf("the Point Lies On The %s Axis ...!!!\n\n", point_properties.axis_location);
		}
		else
		{
			point_properties.axis_location[0] = '\0';

			if(poi.a > 0 && poi.b > 0)
				point_properties.Quadrant = 1;

			else if(poi.a < 0 && poi.b > 0)
				point_properties.Quadrant = 2;

			else if(poi.a < 0 && poi.b < 0)
				point_properties.Quadrant = 3;
			else
				point_properties.Quadrant = 4;
			printf("The Point Lies In The Quadrant number %d ...!!!\n\n", point_properties.Quadrant);
		}
	}
	return(0);
}
