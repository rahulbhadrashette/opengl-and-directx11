#include<stdio.h>

struct MyFile
{
	int a;
	float b;
	double c;
};

int main(void)
{
	struct MyFile file;
	int a_size;
	int b_size;
	int c_size;
	int struct_MyFile_size;

	file.a = 30;
	file.b = 12.45f;
	file.c = 3.14;
	
	printf("\n\n");
	printf("FILE MAMBER OF 'struct MyFile'ARE : \n\n");
	printf("a = %d\n", file.a);
	printf("b = %f\n", file.b);
	printf("c = %lf\n", file.c);

	a_size = sizeof(file.a);
	b_size = sizeof(file.b);
	c_size = sizeof(file.c);
	
	printf("\n\n");
	printf("SIZES (in bytes) OF DATA MEMBERS OF 'struct MyFile' ARE : \n\n");
	printf("Size Of 'a' = %d bytes\n", a_size);
	printf("Size Of 'b' = %d bytes\n", b_size);
	printf("Size Of 'c' = %d bytes\n", c_size);
	
	struct_MyFile_size = sizeof(struct MyFile);

	printf("\n\n");
	printf("Size of 'struct MyFile' : %d bytes \n\n", struct_MyFile_size);
		
	return(0);
}