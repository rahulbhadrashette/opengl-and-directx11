#include<stdio.h>

struct MyCoOrdinate
{
	int x;
	int y;
}Point_A, Point_B, Point_C, Point_D, Point_E;

int main(void)
{
	Point_A.x = 5;
	Point_A.y = 4;

	Point_B.x = 4;
	Point_B.y = 5;

	Point_C.x = 3;
	Point_C.y = 7;

	Point_D.x = 6;
	Point_D.y = 4;

	Point_E.x = 5;
	Point_E.y = 1;

	printf("\n\n");
	printf("Co-Ordinate(x, y) Of Point 'A' Are : (%d, %d)\n\n", Point_A.x, Point_A.y);
	printf("Co-Ordinate(x, y) Of Point 'B' Are : (%d, %d)\n\n", Point_B.x, Point_B.y);
	printf("Co-Ordinate(x, y) Of Point 'C' Are : (%d, %d)\n\n", Point_C.x, Point_C.y);
	printf("Co-Ordinate(x, y) Of Point 'D' Are : (%d, %d)\n\n", Point_D.x, Point_D.y);
	printf("Co-Ordinate(x, y) Of Point 'E' Are : (%d, %d)\n\n", Point_E.x, Point_E.y);

	return(0);
}