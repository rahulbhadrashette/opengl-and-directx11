#include<stdio.h>

union MyUnoin
{
	int a;
	float b;
	double c;
	char d;
};

int main(void)
{
	union MyUnoin Ut1, Ut2;

	printf("\n\n");
	printf("Member Of Union Ut1 Are :- \n\n");

	Ut1.a = 8;
	Ut1.b = 3.14f;
	Ut1.c = 8.1356489;
	Ut1.d = 'T';

	printf("Ut1.a = %d\n\n", Ut1.a);
	printf("Ut1.b = %f\n\n", Ut1.b);
	printf("Ut1.c = %lf\n\n", Ut1.c);
	printf("Ut1.d = %c\n\n", Ut1.d);

	printf("Addresses Of Member Of Union Ut1 Are :- \n\n");
	printf("Ut1.a = %p\n\n", &Ut1.a);
	printf("Ut1.b = %p\n\n", &Ut1.b);
	printf("Ut1.c = %p\n\n", &Ut1.c);
	printf("Ut1.d = %p\n\n", &Ut1.d);

	printf("MyUnion Ut1 = %p\n\n", &Ut1);

	// ****** MyUnion Ut2 ****
	printf("\n\n");
	printf("Member Of Union Ut2 Are :- \n\n");
	
	Ut2.a = 5;
	printf("Ut2.a = %d\n\n", Ut2.a);

	Ut2.a = 5.14f;
	printf("Ut2.b = %f\n\n", Ut2.b);

	Ut2.a = 5.32597656;
	printf("Ut2.c = %lf\n\n", Ut2.c);

	Ut2.a = 'R';
	printf("Ut2.d = %d\n\n", Ut2.d);

	printf("Addresses Of Member Of Union Ut2 Are :- \n\n");
	printf("Ut2.a = %p\n\n", &Ut2.a);
	printf("Ut2.b = %p\n\n", &Ut2.b);
	printf("Ut2.c = %p\n\n", &Ut2.c);
	printf("Ut3.d = %p\n\n", &Ut2.d);

	printf("MyUnion Ut2 = %p\n\n", &Ut2);

	return(0);
}