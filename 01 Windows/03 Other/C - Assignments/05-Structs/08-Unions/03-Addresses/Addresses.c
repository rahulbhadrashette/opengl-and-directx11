#include<stdio.h>

struct MyStruct
{
	int a;
	float b;
	double c;
	char d;
};

union MyUnoin
{
	int a;
	float b;
	double c;
	char d;
};

int main(void)
{
	struct MyStruct St;
	union MyUnoin Ut;

	printf("\n\n");
	printf("Member Of Struct Are :- \n\n");

	St.a = 45;
	St.b = 1.23;
	St.c = 3.125468;
	St.d = 'M';

	printf("St.a = %d\n\n", St.a);
	printf("St.b = %f\n\n", St.b);
	printf("St.c = %lf\n\n", St.c);
	printf("St.d = %c\n\n", St.d);

	printf("Addresses Of Member Of Struct St1 Are :- \n\n");
	printf("St.a = %p\n\n", &St.a);
	printf("St.b = %p\n\n", &St.b);
	printf("St.c = %p\n\n", &St.c);
	printf("St.d = %p\n\n", &St.d);

	printf("MyStruct St = %p\n\n", &St);

	printf("\n\n");
	printf("Member Of Union Ut Are :- \n\n");
	
	// ****** MyUnion Ut ****
	printf("\n\n");
	printf("Member Of Union Ut Are :- \n\n");
	
	Ut.a = 5;
	printf("Ut.a = %d\n\n", Ut.a);

	Ut.a = 5.14f;
	printf("Ut.b = %f\n\n", Ut.b);

	Ut.a = 5.32597656;
	printf("Ut.c = %lf\n\n", Ut.c);

	Ut.a = 'R';
	printf("Ut.d = %d\n\n", Ut.d);

	printf("Addresses Of Member Of Union Ut Are :- \n\n");
	printf("Ut.a = %p\n\n", &Ut.a);
	printf("Ut.b = %p\n\n", &Ut.b);
	printf("Ut.c = %p\n\n", &Ut.c);
	printf("Ut.d = %p\n\n", &Ut.d);

	printf("MyUnion Ut = %p\n\n", &Ut);

	return(0);
}