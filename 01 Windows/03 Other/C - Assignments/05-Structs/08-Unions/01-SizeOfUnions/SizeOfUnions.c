#include<stdio.h>

struct MyStruct
{
	int a;
	float b;
	double c;
	char d;
};

union MyUnoin
{
	int a;
	float b;
	double c;
	char d;
};

int main(void)
{
	struct MyStruct St;
	union MyUnoin Ut;

	printf("\n\n");
	printf("Size Of MyStruct = %d\n", sizeof(St));

	printf("\n\n");
	printf("Size Of MyUnion = %d\n", sizeof(Ut));

	return(0);
}