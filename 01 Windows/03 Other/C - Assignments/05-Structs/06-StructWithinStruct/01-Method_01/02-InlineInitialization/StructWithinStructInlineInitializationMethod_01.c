#include<stdio.h>

struct Rectangle
{
	struct MyFile
	{
		int a;
		int b;
	} poi_01, poi_02;
} rect = {{5, 8}, {6, 9}};

int main(void)
{
	int length, breadth, area;

	length = rect.poi_02.b - rect.poi_01.b;
	if(length < 0)
		length = length * -1;

	breadth = rect.poi_02.a - rect.poi_01.a;
	if(breadth < 0)
		breadth = breadth * -1;

	area = length * breadth;

	printf("\n\n");
	printf("Length Of Rectangle = %d \n\n", length);
	printf("Breadth Of Rectangle = %d \n\n", breadth);
	printf("Area Of Rectangle = %d \n\n", area);

	return(0);
}