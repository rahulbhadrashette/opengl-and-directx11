#include<stdio.h>

struct MyNumber
{
	int num;
	int num_table[10];
};

struct NumTable
{
	struct MyNumber p;
};

int main(void)
{
	struct NumTable table[10];
	int a,b;

	for(a = 0; a < 10; a++)
	{
		table[a].p.num = (a + 1);
	}

	for(a = 0; a < 10; a++)
	{
		printf("\n\n");
		printf("Table Of %d :- \n\n", table[a].p.num);
		for(b = 0; b < 10; b++)
		{
			table[a].p.num_table[b] = table[a].p.num * ( b + 1);
			printf("%d * %d = %d\n", table[a].p.num, (b + 1), table[a].p.num_table[b]);
		}	
	}
	return(0);
}