#include<stdio.h>

struct MyNum
{
	int number;
	int number_table[10];
};

struct NumberTables
{
	struct MyNum p;
	struct MyNum q;
	struct MyNum r;
};

int main(void)
{
	struct NumberTables table;
	int a;

	table.p.number = 3;
	for(a = 0; a < 10; a++)
	{
		table.p.number_table[a] = table.p.number * ( a + 1);
	}
	printf("\n\n");
	printf("Table Of %d :- \n\n", table.p.number);
	for(a = 0; a < 10; a++)
	{
		printf("%d * %d = %d\n", table.p.number, (a + 1), table.p.number_table[a]);
	}

	table.q.number = 4;
	for(a = 0; a < 10; a++)
	{
		table.q.number_table[a] = table.q.number * ( a + 1);
	}
	printf("\n\n");
	printf("Table Of %d :- \n\n", table.q.number);
	for(a = 0; a < 10; a++)
	{
		printf("%d * %d = %d\n", table.q.number, (a + 1), table.q.number_table[a]);
	}

	table.r.number = 5;
	for(a = 0; a < 10; a++)
	{
		table.r.number_table[a] = table.r.number * ( a + 1);
	}
	printf("\n\n");
	printf("Table Of %d :- \n\n", table.r.number);
	for(a = 0; a < 10; a++)
	{
		printf("%d * %d = %d\n", table.r.number, (a + 1), table.r.number_table[a]);
	}
	return(0);
}
