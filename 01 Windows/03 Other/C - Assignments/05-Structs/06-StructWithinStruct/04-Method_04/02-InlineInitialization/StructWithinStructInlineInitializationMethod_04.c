#include<stdio.h>


int main(void)
{

	struct MyFile
	{
		int a;
		int b;
	};

	struct Rectangle
	{
		struct MyFile poi_01;
		struct MyFile poi_02;
	};
	
	struct Rectangle rect = {{5, 8}, {6, 9}};
	
	int length, breadth, area;

	length = rect.poi_02.b - rect.poi_01.b;
	if(length < 0)
		length = length * -1;

	breadth = rect.poi_02.a - rect.poi_01.a;
	if(breadth < 0)
		breadth = breadth * -1;

	area = length * breadth;

	printf("\n\n");
	printf("Length Of Rectangle = %d \n\n", length);
	printf("Breadth Of Rectangle = %d \n\n", breadth);
	printf("Area Of Rectangle = %d \n\n", area);

	return(0);
}