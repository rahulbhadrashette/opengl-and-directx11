#include<stdio.h>

struct MyFile
{
	int a;
	int b;
} ;

struct Rectangle
{
	struct MyFile poi_01, poi_02;
};

int main(void)
{
	int length, breadth, area;
	struct Rectangle rect;

	printf("\n\n");
	printf("Enter Leftmost X-Co-Ordinate Of Rectangle :- ");
	scanf("%d", &rect.poi_01.a);

	printf("\n\n");
	printf("Enter Bottommost Y-Co-Ordinate Of Rectangle :- ");
	scanf("%d", &rect.poi_01.b);

	printf("\n\n");
	printf("Enter Rightmost X-Co-Ordinate Of Rectangle :- ");
	scanf("%d", &rect.poi_02.a);

	printf("\n\n");
	printf("Enter Topmost Y-Co-Ordinate Of Rectangle :- ");
	scanf("%d", &rect.poi_02.b);

	length = rect.poi_02.b - rect.poi_01.b;
	if(length < 0)
		length = length * -1;

	breadth = rect.poi_02.a - rect.poi_01.a;
	if(length < 0)
		breadth = breadth * -1;

	area = length * breadth;

	printf("\n\n");
	printf("Length Of Rectangle = %d \n\n", length);
	printf("Breadth Of Rectangle = %d \n\n", breadth);
	printf("Area Of Rectangle = %d \n\n", area);

	return(0);
}