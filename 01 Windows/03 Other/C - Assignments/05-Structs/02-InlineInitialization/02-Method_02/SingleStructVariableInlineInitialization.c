#include<stdio.h>

struct MyFile 
{
	int a;
	float b;
	double c;
	char d;
};

struct MyFile file = {40, 3.2f, 3.141535, 'A' };

int main(void)
{
	printf("\n\n");
	printf("FILE MEMBERS Of 'struct MyFile' ARE :- \n\n");
	printf("a = %d\n", file.a);
	printf("b = %f\n", file.b);
	printf("c = %lf\n", file.c);
	printf("d = %c\n", file.d);

	return(0);
}
