#include<stdio.h>

struct MyFile 
{
	int a;
	float b;
	double c;
	char d;
};

int main(void)
{
	struct MyFile  file_one = { 24, 3.8, 3.14514, 'A' };
	struct MyFile  file_two = { 'C', 5.7, 1.84562, 89 };
	struct MyFile  file_three = { 69, 'H' };
	struct MyFile  file_four = { 59 };

	printf("\n\n");
	printf("FILE MEMBERS Of 'struct MyFile file_one' ARE :- \n\n");
	printf("a = %d\n", file_one.a);
	printf("b = %f\n", file_one.b);
	printf("c = %lf\n", file_one.c);
	printf("d = %c\n", file_one.d);


	printf("\n\n");
	printf("FILE MEMBERS Of 'struct MyFile file_two' ARE :- \n\n");
	printf("a = %d\n", file_two.a);
	printf("b = %f\n", file_two.b);
	printf("c = %lf\n", file_two.c);
	printf("d = %c\n", file_two.d);

	printf("\n\n");
	printf("FILE MEMBERS Of 'struct MyFile file_three' ARE :- \n\n");
	printf("a = %d\n", file_three.a);
	printf("b = %f\n", file_three.b);
	printf("c = %lf\n", file_three.c);
	printf("d = %c\n", file_three.d);
	
	printf("\n\n");
	printf("FILE MEMBERS Of 'struct MyFile file_four' ARE :- \n\n");
	printf("a = %d\n", file_four.a);
	printf("b = %f\n", file_four.b);
	printf("c = %lf\n", file_four.c);
	printf("d = %c\n", file_four.d);

	return(0);
}
