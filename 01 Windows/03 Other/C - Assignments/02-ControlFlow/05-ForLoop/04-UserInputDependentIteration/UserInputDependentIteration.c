#include<stdio.h>

int main(void)

{
int i_num, num, i;

printf("\n\n");

printf("Enter an integer value form which iteration must begin :- ");
scanf("%d", &i_num);

printf("How Many digits do you want to print from %d onwards ? : " , i_num);
scanf("%d", &num);

printf("printing digits %d to %d : \n\n", i_num, (i_num + num));

for(i = i_num; i <= (i_num + num); i++)
{
	printf("\t%d\n", i);
}

printf("\n\n");

return(0);
}