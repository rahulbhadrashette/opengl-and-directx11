#include<stdio.h>

int main(void)
{
	int p,q,r;
	
	printf("\n\n");
	for(p = 1; p <= 10; p++)
	{
		printf(" P = %d\n", p);
		printf("--------\n\n");
		for(q = 1; q <= 5; q++)
		{
			printf(" \tQ = %d\n", q);
			printf(" \t-------\n\n");
			for(r = 1; r <= 3; r++)
			{
				printf(" \t\tR = %d\n", r);
			}
			printf("\n\n");
		}
		printf("\n\n");
	} 
	printf("\n\n");
	return(0);
}