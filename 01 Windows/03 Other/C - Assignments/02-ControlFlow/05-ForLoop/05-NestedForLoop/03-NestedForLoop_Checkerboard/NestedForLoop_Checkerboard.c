#include<stdio.h>

int main(void)
{
	int x,y,z;

	printf("\n\n");
	for(x = 0; x < 64; x++)
	{
		for(y = 0; y < 64; y++)
		{
			z = ((x & 0x8) == 0) ^ (( y & 0x8) == 0);
			
			if(z == 0)
			{
				 printf("	");
			}
			
			if(z == 1)
			{
				printf("  **");
			}
		}
		printf("\n\n");
	}	
	return(0);
}