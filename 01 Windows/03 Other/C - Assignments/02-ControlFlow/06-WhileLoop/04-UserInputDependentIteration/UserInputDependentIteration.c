#include<stdio.h>
int main(void)

{
	int i_number, num, a;
	printf("\n\n");

	printf("Enter An integer value from which iteration must begin :- ");
	scanf("%d", &i_number);

	printf("How many digits do you want to print from %d Onward ? :- ", i_number);
	scanf("%d", &num);
	
	printf("printing digits %d to %d :- \n\n", i_number, (i_number + num));

	a = i_number;
	while(a <= (i_number + num))
	{
		printf("\t%d\n", a);
		a++;
	}
	printf("\n\n");
	return(0);
}
