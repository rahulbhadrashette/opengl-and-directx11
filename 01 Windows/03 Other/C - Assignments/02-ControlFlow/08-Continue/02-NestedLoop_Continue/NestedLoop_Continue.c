#include<stdio.h>
int main(void)

{
	int a,b;

	printf("\n\n");

	printf("Outer loop prints odd number between 1 to 10. \n\n");
	printf("Outer loop prints odd number between 1 to 10 for every odd number printed by outer loop. \n\n");

	for(a = 1; a <= 10; a++)
	{
		if(a % 2 != 0)
		{
			printf("a = %d\n", a);
			printf("----------\n");
			for (b = 1; b <= 10; b++)
			{
				if(b % 2 == 0)
				{
					printf("\tb = %d\n", b);
				}
				else
				{
					continue;
				}
			}
			printf("\n\n");
		}
		else
		{
			continue;
		}
	}
	printf("\n\n");

	return(0);
}