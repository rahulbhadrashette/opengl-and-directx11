#include<stdio.h>

int main(void)
{
	int age;

	printf("\n\n");
	printf("Enter Age :- ");
	scanf("%d", &age);
	printf("\n\n");

	if(age >= 18)
	{
		printf("Entering if-block...!!!\n\n");
		printf("You are eligible for voting...!!!\n\n");
	}
	else
	{
		printf("Entering else-block...!!!\n\n");
		printf("You are Not eligible for voting...!!!\n\n");
	}
	printf("Good Bye...!!!\n\n");
	return(0);
}