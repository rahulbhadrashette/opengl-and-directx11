#include<stdio.h>

int main(void)
{

// Function declaration
void PrintBinaryFormOfNumber(unsigned int);

//variable declaration
unsigned int a;
unsigned int b;
unsigned int result;

//code
printf("\n\n");
printf("Enter An Integer = ");
scanf("%u", &a);

printf("\n\n");
printf("Enter Another Integer = ");
scanf("%u", &b);

printf("\n\n\n\n");
result = a ^ b;
printf("Bitwise OR Of \nA = %d (Decimal), %o (Octal), %X (Hexadecimal) and \nB = %d (Decimal), %o (Octal), %X (Hexadecimal) \nGives The Result = %d (Decimal), %o (Octal), %X (Hecadecimal).\n\n", a, a, a, b, b, b, result, result, result);
PrintBinaryFormOfNumber(a);
PrintBinaryFormOfNumber(b);
PrintBinaryFormOfNumber(result);

return(0);
}


void PrintBinaryFormOfNumber(unsigned int decimal_number)
{
//local variable declaration
unsigned int quotient, reminder;
unsigned int num;
unsigned int binary_array[8];
int i;


//code
for(i = 0; i < 8; i++)
{
	binary_array[i] = 0;
}

printf("The Binary Form Of The Decimal Intger %d Is \t = \t", decimal_number);
num = decimal_number;
i = 7;
while (num != 0)
{
	quotient = num / 2;
	reminder = num % 2;
	binary_array[i] = reminder;
	num = quotient;
	i--;
}

for(i = 0; i < 8; i++)
{
	printf("%u", binary_array[i]);
}
printf("\n\n");
}