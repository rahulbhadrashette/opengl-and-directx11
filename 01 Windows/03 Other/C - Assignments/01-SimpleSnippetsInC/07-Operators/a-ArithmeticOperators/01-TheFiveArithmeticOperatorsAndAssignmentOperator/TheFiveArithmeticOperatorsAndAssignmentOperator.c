#include<stdio.h>

int main(void)
{
//local variable declaration
int a;
int b;
int result;

//code
printf("\n\n");
printf("Enter A Number :- ");
scanf("%d", &a);

printf("\n\n");
printf("Enter Another Number :- ");
scanf("%d", &b);

printf("\n\n");

result = a + b;
printf("Addition of a = %d And b = %d And Gives = %d.\n\n", a, b, result);

result = a - b;
printf("Substract of a = %d And b = %d And Gives = %d.\n\n", a, b, result);

result = a * b;
printf("Multiplication of a = %d And b = %d And Gives = %d.\n\n", a, b, result);

result = a / b;
printf("Division of a = %d And b = %d And Gives Reminder = %d.\n\n", a, b, result);

result = a % b;
printf("Division of a =  %d And b = %d And Gives Quotient = %d.\n\n", a, b, result);

return(0);
}