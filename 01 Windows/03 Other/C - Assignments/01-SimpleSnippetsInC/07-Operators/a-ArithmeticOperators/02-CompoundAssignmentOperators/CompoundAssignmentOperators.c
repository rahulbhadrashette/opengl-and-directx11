#include<stdio.h>

int main(void)
{
//local variable declaration
int a;
int b;
int x;

//code
printf("\n\n");
printf("Enter A Number :- ");
scanf("%d", &a);

printf("\n\n");
printf("Enter Another Number :- ");
scanf("%d", &b);

printf("\n\n");

x = a;
a += b;
printf("Addition of A = %d And B = %d Gives %d.\n\n", x, b, a);

x = a;
a -= b;
printf("Substraction Of A = %d And B = %d Gives %d.\n\n", x, b, a);

x = a;
a *= b;
printf("Multiplication Of A = %d And B = %d Gives %d.\n\n", x, b, a);

x = a;
a /= b;
printf("Division Of A = %d And B = %d Gives Quotient %d.\n\n", x, b, a);

x = b;
a %= b;
printf("Division Of A = %d And B = %d Gives Reminder %d.\n\n", x, b , a);

printf("\n\n");

return(0);

}