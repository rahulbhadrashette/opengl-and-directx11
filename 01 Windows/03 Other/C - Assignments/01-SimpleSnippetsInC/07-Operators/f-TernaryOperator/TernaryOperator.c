#include<stdio.h>

int main(void)
{
//local variable declaration
int a, b;
int p, q;
char ch_result_01, ch_result_02;
int i_result_01, i_result_02;

//code
printf("\n\n");

a = 9;
b = 6;
ch_result_01 = (a > b) ? 'A' : 'B';
i_result_01 = (a > b) ? a : b;
printf("Ternary Opertor Answer 1 ------%c and %d.\n\n", ch_result_01,  i_result_01);

p = 45;
q = 45;
ch_result_02 = (p != q) ? 'P' : 'Q';
i_result_02 = (p != q) ? p : q;
printf("Ternary Operator Answer 2 ----- %c and %d. \n\n", ch_result_02, i_result_02);

printf("\n\n");
return(0);
}