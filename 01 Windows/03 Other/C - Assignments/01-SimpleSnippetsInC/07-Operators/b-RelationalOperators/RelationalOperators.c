#include<stdio.h>

int main(void)
{
//local variable declaration
int a;
int b;
int result;

//code
printf("\n\n");
printf("Enter One integer :- ");
scanf("%d", &a);

printf("\n\n");
printf("Enter Another Integer :- ");
scanf("%d", &b);

printf("\n\n");
printf("If Answer = 0, It Is 'FASE'.\n");
printf("If Answer = 1, It Is 'TRUE'.\n\n");

result = (a < b);	
printf("(a < b) A = %d Is Less Then B = %d 			\t Answer = %d\n\n", a, b, result);

result = (a > b);
printf("(a > b) A = %d Is Greater Then B = %d 			\t Answer = %d\n\n", a, b, result);

result = (a <= b);
printf("(a <= b) A = %d Is Less Then Or Equal To B = %d		 \t Answer = %d\n\n", a, b, result);

result = (a >= b);
printf("(a >= b) A = %d Is Greater Then Or Equal To B = %d	 \t Answer = %d\n\n", a, b, result);

result = (a == b);
printf("(a == b) A = %d Is Equal To B = %d 		       	\t Answer = %d\n\n", a, b, result);

result = (a != b);
printf("(a != b) A = %d Is Not Equal To B = %d			 \t Answer = %d\n", a, b, result);

result = (0);
}