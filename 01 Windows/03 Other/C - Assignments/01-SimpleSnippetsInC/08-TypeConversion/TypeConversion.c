#include<stdio.h>

int main(void)
{
//local variable declaration
int i, j;
char ch_01, ch_02;

int a, result_int;
float f, result_float;

int i_explicit;
float f_explicit;

//code
printf("\n\n");

//interconversion and implicit type casting between 'char' And 'int types...
i = 69;
ch_01 = i;
printf("I = %d\n", i);
printf("Charater 1 (after ch_01 = i) = %c\n\n", ch_01);


ch_02 = 'Q';
j = ch_02;
printf("Charater 2 = %c\n", ch_02);
printf("J (after j = ch_02) = %d\n\n", j);



//implicit conversion of 'int' to 'float'...
a = 5;
f = 7.8f;
result_float = a + f;
printf("Integer a = %d And Floating-Point Number %f Added Gives Floating-Point Sum = %f\n\n", a, f, result_float);

result_int = a + f;
printf("Integer a = %d And Floating -point Number %f Added Gives Integer Sum = %d\n\n", a, f, result_int);

//Explicit Type-casting Using Cast Operator..
f_explicit = 30.121995f;
i_explicit = (int)f_explicit;
printf("Floating point Number Type Casted Explicitly = %f\n", f_explicit);
printf("Resultant Integer After Explicit Type Casting Of %f = %d\n\n", f_explicit, i_explicit);

return(0);
}