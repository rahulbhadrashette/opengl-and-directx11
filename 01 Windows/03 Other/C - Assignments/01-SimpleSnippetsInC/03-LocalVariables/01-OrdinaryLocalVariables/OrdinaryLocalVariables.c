#include<stdio.h>

int main(void)
{

//variablae declaration
int a = 5;

//function prototype
void change_count(void);

printf("\n");
printf("A = %d\n\n",a);

change_count();
change_count();
change_count();

return(0);
}

void change_count(void)
{

//variable declaration
int local_count = 0;

//code
printf("Local Count = %d\n",local_count);
local_count = local_count + 1;
printf("Local Count = %d\n\n",local_count);
}