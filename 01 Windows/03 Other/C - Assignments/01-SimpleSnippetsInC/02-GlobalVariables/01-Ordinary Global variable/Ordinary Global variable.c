#include<stdio.h>

int global_count = 0;

int main(void)
{
	// function prototypes
	void change_count_one(void);
	void change_count_two(void);

	//code
	printf("\n");
	printf("Global Count = %d\n", global_count);
	change_count_one();
	change_count_two();
	return(0);
}

void change_count_one(void)
{
	//code
	global_count = global_count + 1;
	printf("Global Count = %d\n",global_count);
}

void change_count_two(void)
{
	//code
	global_count = global_count + 1;
	printf("Global Count = %d\n",global_count);
}