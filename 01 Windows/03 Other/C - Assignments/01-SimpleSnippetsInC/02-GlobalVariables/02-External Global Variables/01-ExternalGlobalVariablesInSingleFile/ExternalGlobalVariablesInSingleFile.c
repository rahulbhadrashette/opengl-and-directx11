#include<stdio.h>

int main(void)
{
//function prototype
void change_count(void);

//variable declaration
extern int global_count;

//code
printf("\n"); 
printf("Global Count = %d\n",global_count);
change_count();
return(0);
}

int global_count = 0;

void change_count(void)
{
//code
global_count = global_count + 1;
printf("Global Count = %d\n",global_count);
}