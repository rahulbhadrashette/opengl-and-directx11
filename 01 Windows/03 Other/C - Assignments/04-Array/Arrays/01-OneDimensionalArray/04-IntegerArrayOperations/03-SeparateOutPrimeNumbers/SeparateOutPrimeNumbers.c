#include<stdio.h>

#define DIGIT 10

int main(void)
{
	int iArr[DIGIT];
	int i, num, j, count = 0;

	printf("\n\n");
	printf("Enter Integer Elements For Array :- \n\n");

	for(i = 0; i < DIGIT; i++)
	{
		scanf("%d",&num);
		
		if(num < 0)
		{
			iArr[i] = num;
		}
		iArr[i] = num;
	}
	
	printf("\n\n");
	printf("Array Elements Are :- \n\n");
	for(i = 0; i < DIGIT; i++)
	{
		printf("%d\n", iArr[i]);
	}
	
	printf("\n\n");
	printf("Prime Numbers Amongst THe Array Elements Are :- \n\n");
	for(i = 0; i < DIGIT; i++)
	{
		for(j = 1; j <= iArr[i]; j++)
		{
			if((iArr[i] % 2) != 0)
			{
				count++;
			}
		}
		if(count == 2)
		{
			printf("%d\n", iArr[i]);
		}
		count = 0;
	}
	return(0);
}