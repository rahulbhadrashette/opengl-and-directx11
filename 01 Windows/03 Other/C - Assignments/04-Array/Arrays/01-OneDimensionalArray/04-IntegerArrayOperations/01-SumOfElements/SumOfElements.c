#include<stdio.h>

#define DIGIT 10

int main(void)
{
	int iArr[DIGIT];
	int i, num, sum = 0;

	printf("\n\n");
	printf("Enter integer Elements For Array :-  \n\n");

	for(i = 0; i < DIGIT; i++)
	{
		scanf("%d",&num);
		iArr[i] = num;
	}

	for( i = 0; i < DIGIT; i++)
	{
		sum = sum + iArr[i];
	}

	printf("\n\n");
	printf("Sum Of All Elements Of Array = %d\n\n", sum);

	return(0);
}