#include<stdio.h>

#define DIGIT 10

int main(void)
{
	int iArr[DIGIT];
	int i, num, sum = 0;

	printf("\n\n");
	printf("Enter Integer Elements For Array :- \n\n");

	for(i = 0; i < DIGIT; i++)
	{
		scanf("%d",&num);
		iArr[i] = num;
	}
	
	printf("\n\n");
	printf("Even Number Amongst The Array Elements Are :- \n\n");
	for(i = 0; i < DIGIT; i++)
	{
		if((iArr[i] % 2) == 0)
		{
			printf("%d\n", iArr[i]);
		}
	}
	
	printf("\n\n");
	printf("Odd Numbers Amongst The Array Elements Are :- \n\n");
	for(i = 0; i < DIGIT; i++)
	{
		if((iArr[i] % 2) != 0)
		{
			printf("%d\n", iArr[i]);
		}
	}
	return(0);
}