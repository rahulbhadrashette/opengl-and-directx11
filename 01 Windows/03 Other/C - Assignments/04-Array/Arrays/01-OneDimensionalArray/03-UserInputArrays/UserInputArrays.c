#include<stdio.h>

#define INT_ARR_DIGIT_ELEMENTS 3
#define FLOAT_ARR_DIGIT_ELEMENTS 6 
#define CHAR_ARR_DIGIT_ELEMENTS 9

int main(void)
{
	 //variable declaration
	int iArr[INT_ARR_DIGIT_ELEMENTS];
	float fArr[FLOAT_ARR_DIGIT_ELEMENTS];
	char cArr[CHAR_ARR_DIGIT_ELEMENTS];
	int i, num;
	
	//code
	
	//************ Array Elements I/P ******************

	printf("\n\n");
	printf("Enter Element For 'integer' Array :- \n");
	for(i = 0; i < INT_ARR_DIGIT_ELEMENTS; i++);
	{
		scanf("%d", &iArr[i]);
	}

	printf("\n\n");
	printf("Enter Element For 'Floating-point' Array :- \n");
	for(i = 0; i < FLOAT_ARR_DIGIT_ELEMENTS; i++)	
	{
		scanf("%f", &fArr[i]);
	}
	
	printf("\n\n");
	printf("Enter Element For 'Character' Array :- \n");
	for(i = 0; i < CHAR_ARR_DIGIT_ELEMENTS; i++)
	{
		cArr[i] = getch();
		printf("%c\n", cArr[i]);
	}
	
	//**********Array Element O/P *********************
	printf("\n\n");
	printf("Inter Array Entered By You :- \n\n");
	for(i = 0; i < INT_ARR_DIGIT_ELEMENTS; i++)
	{
		printf("%d\n",iArr[i]);
	}
	
	printf("\n\n");
	printf("Floating-Point Array Entered By You :- \n\n");
	for(i = 0; i < FLOAT_ARR_DIGIT_ELEMENTS; i++)
	{
		printf("%f\n", fArr[i]);
	}

	printf("\n\n");
	printf("Character Array Entered By You :- \n\n");
	for(i = 0; i < CHAR_ARR_DIGIT_ELEMENTS; i++)
	{
		printf("%c\n", cArr[i]);
	}

	return(0);
	
}