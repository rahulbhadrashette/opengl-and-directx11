#include<stdio.h>
int main(void)
{
	//variable declaraton
	int iArr_ABC[10];
	int iArr_PQR[10];
	
	//code
	
	//**********iArrayA[]**************
	iArr_ABC[0] = 3;
	iArr_ABC[1] = 6;
	iArr_ABC[2] = 9;
	iArr_ABC[3] = 12;
	iArr_ABC[4] = 15;
	iArr_ABC[5] = 18;
	iArr_ABC[6] = 21;
	iArr_ABC[7] = 24;
	iArr_ABC[8] = 27;
	iArr_ABC[9] = 30;
	
	printf("\n\n");
	printf("Pice-meal (Hard-coded) Assignment And Display Of Element to Array 'iArr_ABC[]' :- \n\n");
	printf("1st Element Of Array 'iArr_ABC[]' Or Element At 0th Index Of Array 'iArr_ABC[]'  = %d\n", iArr_ABC[0]);
	printf("2nd Element Of Array 'iArr_ABC[]' Or Element At 1st Index Of Array 'iArr_ABC[]'  = %d\n", iArr_ABC[1]);
	printf("3rd Element Of Array 'iArr_ABC[]' Or Element At 2nd Index Of Array 'iArr_ABC[]'  = %d\n", iArr_ABC[2]);
	printf("4th Element Of Array 'iArr_ABC[]' Or Element At 3rd Index Of Array 'iArr_ABC[]'  = %d\n", iArr_ABC[3]);
	printf("5th Element Of Array 'iArr_ABC[]' Or Element At 4th Index Of Array 'iArr_ABC[]'  = %d\n", iArr_ABC[4]);
	printf("6th Element Of Array 'iArr_ABC[]' Or Element At 5th Index Of Array 'iArr_ABC[]'  = %d\n", iArr_ABC[5]);
	printf("7th Element Of Array 'iArr_ABC[]' Or Element At 6th Index Of Array 'iArr_ABC[]'  = %d\n", iArr_ABC[6]);
	printf("8th Element Of Array 'iArr_ABC[]' Or Element At 7th Index Of Array 'iArr_ABC[]'  = %d\n", iArr_ABC[7]);
	printf("9th Element Of Array 'iArr_ABC[]' Or Element At 8th Index Of Array 'iArr_ABC[]'  = %d\n", iArr_ABC[8]);
	printf("10ht Element Of Array 'iArr_ABC[]' Or Element At 9th Index Of Array 'iArr_ABC[]' = %d\n\n", iArr_ABC[9]);

	//****************iArr_PQR*****************
	printf("\n\n");	
	
	printf("Enter 1st Element Of Array 'iArr_PQR[]'  :- ");
	scanf("%d", &iArr_PQR[0]);
	printf("Enter 2nd Element Of Array 'iArr_PQR[]'  :- ");
	scanf("%d", &iArr_PQR[1]);
	printf("Enter 3rd Element Of Array 'iArr_PQR[]'  :- ");
	scanf("%d", &iArr_PQR[2]);
	printf("Enter 4th Element Of Array 'iArr_PQR[]'  :- ");
	scanf("%d", &iArr_PQR[3]);
	printf("Enter 5th Element Of Array 'iArr_PQR[]'  :- ");
	scanf("%d", &iArr_PQR[4]);
	printf("Enter 6th Element Of Array 'iArr_PQR[]'  :- ");
	scanf("%d", &iArr_PQR[5]);
	printf("Enter 7th Element Of Array 'iArr_PQR[]'  :- ");
	scanf("%d", &iArr_PQR[6]);
	printf("Enter 8th Element Of Array 'iArr_PQR[]'  :- ");
	scanf("%d", &iArr_PQR[7]);
	printf("Enter 9th Element Of Array 'iArr_PQR[]'  :- ");
	scanf("%d", &iArr_PQR[8]);
	printf("Enter 10th Element Of Array 'iArr_PQR[]' :- ");
	scanf("%d", &iArr_PQR[9]);

	printf("\n\n");
	printf("Pice-meal (User Input) Assignment And Display Of Elements to Array 'iArr_PQR[]' :- \n\n");
	printf("1st Element Of Array 'iArr_PQR[]' Or Element At 0th Index Of Array 'iArr_PQR[]'  = %d\n", iArr_PQR[0]);
	printf("2nd Element Of Array 'iArr_PQR[]' Or Element At 1st Index Of Array 'iArr_PQR[]'  = %d\n", iArr_PQR[1]);
	printf("3rd Element Of Array 'iArr_PQR[]' Or Element At 2nd Index Of Array 'iArr_PQR[]'  = %d\n", iArr_PQR[2]);
	printf("4th Element Of Array 'iArr_PQR[]' Or Element At 3rd Index Of Array 'iArr_PQR[]'  = %d\n", iArr_PQR[3]);
	printf("5th Element Of Array 'iArr_PQR[]' Or Element At 4th Index Of Array 'iArr_PQR[]'  = %d\n", iArr_PQR[4]);
	printf("6th Element Of Array 'iArr_PQR[]' Or Element At 5th Index Of Array 'iArr_PQR[]'  = %d\n", iArr_PQR[5]);
	printf("7th Element Of Array 'iArr_PQR[]' Or Element At 6th Index Of Array 'iArr_PQR[]'  = %d\n", iArr_PQR[6]);
	printf("8th Element Of Array 'iArr_PQR[]' Or Element At 7th Index Of Array 'iArr_PQR[]'  = %d\n", iArr_PQR[7]);
	printf("9th Element Of Array 'iArr_PQR[]' Or Element At 8th Index Of Array 'iArr_PQR[]'  = %d\n", iArr_PQR[8]);
	printf("10th Element Of Array 'iArr_PQR[]' Or Element At 9th Index Of Array 'iArr_PQR[]' = %d\n", iArr_PQR[9]);

	return(0);
	
}