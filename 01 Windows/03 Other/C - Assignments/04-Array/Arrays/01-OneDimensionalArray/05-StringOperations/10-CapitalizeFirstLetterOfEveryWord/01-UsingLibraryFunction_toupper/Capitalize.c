#include<stdio.h>
#include<ctype.h> //for toupper()

#define MAX_STR_LEN 512

int main(void)
{
	int StrLength(char[]);
	
	char chArr[MAX_STR_LEN],chArr_MakeByProgCapitalFirstLatter[MAX_STR_LEN];
	int iStrLe;
	int a, b;
	
	//******* STR I/P ******
	printf("\n\n");	
	printf("Enter A String :- \n\n");
	gets_s(chArr, MAX_STR_LEN);
	
	iStrLe = StrLength(chArr);
	
	b = 0;
	for(a = 0; a < iStrLe; a++)
	{
		if(a == 0)
		{
			chArr_MakeByProgCapitalFirstLatter[b] = toupper(chArr[a]);
		}
		else if (chArr[a] == ' ')
		{
			chArr_MakeByProgCapitalFirstLatter[b] = chArr[a];
			chArr_MakeByProgCapitalFirstLatter[b + 1] = toupper(chArr[a + 1]);
	
			b++;
			a++;
		}
		else
		
			chArr_MakeByProgCapitalFirstLatter[b] = chArr[a];
			
		b++;
	}

	chArr_MakeByProgCapitalFirstLatter[b]  = '\0';
	
	//******* STR O/P *******
	printf("\n\n"); 
	printf("String Entered By You Is : \n\n");
	printf("%s\n", chArr);
	
	printf("\n\n");
	printf("String After MakeByProgCapitalFirstLatter[b]  Of Space Is :- \n\n");
	printf("%s\n", chArr_MakeByProgCapitalFirstLatter);
	
	return(0);

}

int StrLength(char string[])
{
	int a;
	int str_len = 0;
	
	for(a = 0; a < MAX_STR_LEN; a++)
	{
		if(string[a] == '\0')
			break;
		else
			str_len++;
	}
	return(str_len);
}
	