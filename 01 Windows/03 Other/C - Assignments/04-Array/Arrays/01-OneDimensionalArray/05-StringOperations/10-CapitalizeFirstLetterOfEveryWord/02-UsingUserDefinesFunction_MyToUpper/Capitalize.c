#include<stdio.h>


#define MAX_STR_LEN 512

#define SPACE ' '

#define FULLSTOP '.'
#define COMMA ','
#define EXCLAMATION '!'
#define QUESTION_MARK '?'

int main(void)
{
	int StrLength(char[]);
	char MyToUp(char);
	
	char chArr[MAX_STR_LEN],chArr_CapitalizeFirstLetOfEveryWorld[MAX_STR_LEN];
	int iStrLe;
	int a, b;
	
	//******* STR I/P ******
	printf("\n\n");	
	printf("Enter A String :- \n\n");
	gets_s(chArr, MAX_STR_LEN);
	
	iStrLe = StrLength(chArr);
	
	b = 0;
	for(a = 0; a < iStrLe; a++)
	{
		if(a == 0)
		{
			chArr_CapitalizeFirstLetOfEveryWorld[b] = MyToUp(chArr[a]);
		}
		else if (chArr[a] == SPACE)
		{
			chArr_CapitalizeFirstLetOfEveryWorld[b] = chArr[a];
			chArr_CapitalizeFirstLetOfEveryWorld[b + 1] = MyToUp(chArr[a + 1]);
	
			b++;
			a++;
		}
		else if ((chArr[a] == FULLSTOP || chArr[a] == COMMA || chArr[a] == EXCLAMATION || chArr[a] == QUESTION_MARK) && (chArr[a] != SPACE))
		{
		
			chArr_CapitalizeFirstLetOfEveryWorld[b] = chArr[a];
			chArr_CapitalizeFirstLetOfEveryWorld[b + 1] = SPACE;
			chArr_CapitalizeFirstLetOfEveryWorld[b + 2] = MyToUp(chArr[a + 1]);
			
			b = b + 2;
			b++;
		}
		else
			chArr_CapitalizeFirstLetOfEveryWorld[b] = chArr[a];
		b++;
	}

	chArr_CapitalizeFirstLetOfEveryWorld[b]  = '\0';
	
	//******* STR O/P *******
	printf("\n\n"); 
	printf("String Entered By You Is : \n\n");
	printf("%s\n", chArr);
	
	printf("\n\n");
	printf("String After Capitalizing First Letter Of Every Word:- \n\n");
	printf("%s\n", chArr_CapitalizeFirstLetOfEveryWorld);
	
	return(0);

}

int StrLength(char string[])
{
	int a;
	int str_len = 0;
	
	for(a = 0; a < MAX_STR_LEN; a++)
	{
		if(string[a] == '\0')
			break;
		else
			str_len++;
	}
	return(str_len);
}

char MyToUp(char Ch)
{
	int number;
	int d;

	number = 'a' - 'A';
	
	if((int) Ch >= 97 && (int)Ch <= 122)
	{
	 	d = (int)Ch - number;
		return((char)d);	
	}
	else
		return(Ch);
}