#include<stdio.h>

#define MAX_STR_LEN 512

int main(void)
{
	int StrLength(char[]);
	void StringCopy(char[], char[]);
	
	char chArr[MAX_STR_LEN],chArr_SpaceRMV[MAX_STR_LEN];
	int iStrLe;
	int a, b;
	
	//******* STR I/P ******
	printf("\n\n");	
	printf("Enter A String :- \n\n");
	gets_s(chArr, MAX_STR_LEN);
	
	iStrLe = StrLength(chArr);
	
	b = 0;
	for(a = 0; a < iStrLe; a++)
	{
		if(chArr[a] == ' ')
			continue;
		else
		{
			chArr_SpaceRMV[b] = chArr[a];
			b++;
		}
	}

	chArr_SpaceRMV[b] = '\0';
	
	//******* STR O/P *******
	printf("\n\n"); 
	printf("String Entered By You Is : \n\n");
	printf("%s\n", chArr);
	
	printf("\n\n");
	printf("String After Removal Of Space Is :- \n\n");
	printf("%s\n", chArr_SpaceRMV);
	
	return(0);

}

int StrLength(char string[])
{
	int a;
	int str_len = 0;
	
	for(a = 0; a < MAX_STR_LEN; a++)
	{
		if(string[a] == '\0')
			break;
		else
			str_len++;
	}
	return(str_len);
}
	