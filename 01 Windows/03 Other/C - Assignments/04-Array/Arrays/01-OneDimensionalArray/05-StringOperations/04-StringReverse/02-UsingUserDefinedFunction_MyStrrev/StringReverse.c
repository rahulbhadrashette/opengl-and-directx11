#include<stdio.h>

#define MAX_STR_LEN 512

int main(void)
{
	void MyStringReverse(char[], char[]);

	char chArr_Real[MAX_STR_LEN], chArr_Reversed[MAX_STR_LEN];
	
	printf("\n\n");
	printf("Enter A String :- \n\n");
	gets_s(chArr_Real, MAX_STR_LEN);

	MyStringReverse(chArr_Reversed, chArr_Real);

	printf("\n\n");
	printf("The Real String Entered By You(i.e :- 'chArr_Real[]') Is :- \n\n");
	printf("%s\n", chArr_Real);
	
	printf("\n\n");
	printf("The Reversed String (i.e : 'chArr_Reverse[]') Is :- \n\n");
	printf("%s\n", chArr_Reversed);

	return(0);
}
	
void MyStringReverse(char string_dest[], char string_Sour[])
{
	int MyStringLe(char[]);	
	
	int istrLength = 0;
	int a, b, Length;
	
	istrLength = MyStringLe(string_Sour);

	Length = istrLength - 1;
	
	for(a = 0, b = Length ; a < istrLength, b >= 0; a++, b--)
	{
		string_dest[a] = string_Sour[b];
	}
	string_dest[a] = '\0';
}

int MyStringLe(char str[])
{
	int p;
	int str_len = 0;
	
	for(p = 0; p < MAX_STR_LEN; p++)
	{
		if(str[p] == '\0')
			break;
		else
			str_len++;
	}
	return(str_len);

}