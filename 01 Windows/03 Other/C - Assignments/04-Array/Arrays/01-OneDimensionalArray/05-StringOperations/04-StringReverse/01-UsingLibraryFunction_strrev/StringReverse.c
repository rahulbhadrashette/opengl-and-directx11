#include<stdio.h>
#include<string.h>

#define MAX_STR_LEN 512

int main(void)
{	
	char chArr_Real[MAX_STR_LEN];

	printf("\n\n");
	printf("Enter A String :- \n\n");
	gets_s(chArr_Real, MAX_STR_LEN);

	printf("\n\n");
	printf("The Real String Entered By You(i.e :- 'chArr_Real[]') Is :- \n\n");
	printf("%s\n", chArr_Real);
	
	printf("\n\n");
	printf("The Reversed String (i.e : 'chArr_Reverse[]') Is :- \n\n");
	printf("%s\n", strrev(chArr_Real));

	return(0);
}