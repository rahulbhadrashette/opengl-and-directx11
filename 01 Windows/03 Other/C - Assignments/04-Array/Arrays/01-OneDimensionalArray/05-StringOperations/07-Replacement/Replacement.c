#include<stdio.h>

#define MAX_STR_LEN 512

int main(void)
{
	int MyStringLength(char[]);
	void MyStringCopy(char[], char[]);
	
	char chArr[MAX_STR_LEN], chArr_Alpha[MAX_STR_LEN];
	int iStrLen;
	int a;
	
	//********* STR I/P ************
	printf("\n\n");
	printf("Enter A String :- \n\n");
	gets_s(chArr, MAX_STR_LEN);

	//******** STR O/P ***********
	MyStringCopy(chArr_Alpha, chArr);

	iStrLen = MyStringLength(chArr_Alpha);

	for(a = 0; a < iStrLen; a++)
	{
		switch(chArr[a])
		{
		case 'P':
		case 'p':
		case 'Q':
		case 'q':
		case 'R':
		case 'r':
		case 'S':
		case 's':
		case 'T':
		case 't':
			chArr_Alpha[a] = '*';
			break;
		default:
			break;
		}
	}
	
	printf("\n\n");
	printf("The String Entered By You Is : \n\n");
	printf("%s\n", chArr);

	printf("\n\n");
	printf("The String After Replacement Of Vowels By * Is : \n\n");
	printf("%s\n", chArr_Alpha);
	
	return(0);
}

int MyStringLength(char str[])
{
	int a;
	int str_le = 0;
	
	for(a = 0; a < MAX_STR_LEN; a++)
	{
		if(str[a] == '\0')
			break;
		else
			str_le++;
	}
	return(str_le);
}
void MyStringCopy(char String_dest[], char String_sour[])	
{
	int MyStringLength(char[]);
	
	int iStrLe = 0;
	int x;
	
	iStrLe = MyStringLength(String_sour);
	for(x = 0; x < iStrLe; x++)
	{
		String_dest[x] = String_sour[x];
	}
	String_dest[x] = '\0';
}