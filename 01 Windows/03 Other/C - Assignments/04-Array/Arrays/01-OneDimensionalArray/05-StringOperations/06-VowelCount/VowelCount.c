#include<stdio.h>

#define MAX_STR_LEG 512

int main(void)
{
	int MyStringLength(char[]);
	
	char chArr[MAX_STR_LEG];
	int iStrLen;
	int count_P = 0, count_Q = 0, count_R = 0, count_S = 0, count_T = 0 ;
	int a;
	
	//********* STR I/P ************
	printf("\n\n");
	printf("Enter A String :- \n\n");
	gets_s(chArr, MAX_STR_LEG);

	//******** STR O/P ***********
	printf("\n\n");
	printf("The String Entered By You Is : \n\n");
	printf("%s\n", chArr);
	
	iStrLen = MyStringLength(chArr);

	for(a = 0; a < iStrLen; a++)
	{
		switch(chArr[a])
		{
		case 'P':
		case 'p':
			count_P++;
			break;
		case 'Q':
		case 'q':
			count_Q++;
			break;
		case 'R':
		case 'r':
			count_R++;
			break;
		case 'S':
		case 's':
			count_S++;
			break;
		case 'T':
		case 't':
			count_T++;
			break;
		default:
			break;
		}
	}

	printf("\n\n");
	printf("In The String Entered By You, The Latters And The Number Of Their Occurences Are Follows :- \n\n");
	printf("'P' Has Occured = %d Times...!!!\n\n", count_P);
	printf("'Q' Has Occured = %d Times...!!!\n\n", count_Q);
	printf("'R' Has Occured = %d Times...!!!\n\n", count_R);
	printf("'S' Has Occured = %d Times...!!!\n\n", count_S);
	printf("'T' Has Occured = %d Times...!!!\n\n", count_T);
	
	return(0);
}

int MyStringLength(char str[])
{
	int a;
	int str_le = 0;
	
	for(a = 0; a < MAX_STR_LEG; a++)
	{
		if(str[a] == '\0')
			break;
		else
			str_le++;
	}
	return(str_le);
}
	