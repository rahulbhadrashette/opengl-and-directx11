#include<stdio.h>

#define MAX_STR_LENGTH 512
	
int main(void)
{
	char chArr[MAX_STR_LENGTH];
	
	printf("\n\n");
	printf("Enter A String :- \n\n");
	gets_s(chArr, MAX_STR_LENGTH);

	printf("\n\n");
	printf("String Entered By You Is :- \n\n");
	printf("%s\n", chArr);
	return(0);
}