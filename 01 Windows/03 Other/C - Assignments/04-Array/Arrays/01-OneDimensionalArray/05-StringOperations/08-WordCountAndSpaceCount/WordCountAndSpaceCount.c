#include<stdio.h>

#define MAX_STR_LEN 512

int main(void)
{
	int StrLength(char[]);
	void StringCopy(char[], char[]);
	
	char chArr[MAX_STR_LEN];
	int iStrLe;
	int a;
	int word_count = 0, space_count = 0;

	//******* STR I/P ******
	printf("\n\n");	
	printf("Enter A String :- \n\n");
	gets_s(chArr, MAX_STR_LEN);
	
	iStrLe = StrLength(chArr);
	
	for(a = 0; a < iStrLe; a++)
	{
		switch(chArr[a])
		{
			case 32:
				space_count++;
				break;
			default:
				break;
		}

	}
	word_count = space_count + 1;
	
	//******* STR O/P *******
	printf("\n\n"); 
	printf("String Entered By You Is : \n\n");
	printf("%s\n", chArr);
	
	printf("\n\n");
	printf("Number Of Space In The I/P String = %d\n\n", space_count);
	printf("Number Of Words In The I/P String = %d\n\n", word_count);
	return(0);

}

int StrLength(char string[])
{
	int a;
	int str_len = 0;
	
	for(a = 0; a < MAX_STR_LEN; a++)
	{
		if(string[a] == '\0')
			break;
		else
			str_len++;
	}
	return(str_len);
}
	