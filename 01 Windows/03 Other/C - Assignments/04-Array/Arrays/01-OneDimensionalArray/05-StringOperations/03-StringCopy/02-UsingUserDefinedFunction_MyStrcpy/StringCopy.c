#include<stdio.h>

#define MAX_STR_LEG 512

int main(void)
{
	//function declaration
	void MyStringCopy(char[], char[]);

	char chArr_Real[MAX_STR_LEG], chArr_Copy[MAX_STR_LEG];
	
	//********* STR I/P ************
	printf("\n\n");
	printf("Enter A String :- \n\n");
	gets_s(chArr_Real, MAX_STR_LEG);

	//******** STR COPY **********
	strcpy(chArr_Copy, chArr_Real);

	//******** STR O/P ***********
	printf("\n\n");
	printf("The Real String Entered By You (i.e :- 'chArr_Real[]') Is: \n\n");
	printf("%s\n", chArr_Real);
	
	printf("\n\n");
	printf("The Copy String (i.e :- 'chArr_Real[]') Is: \n\n");
	printf("%s\n", chArr_Copy);
	
	return(0);
}

void MyStringCopy(char String_dest[], char String_Sour[] )
{
	int MyStringLength(char[]);
	
	int iStrLen = 0;
	int i;
	
	iStrLen = MyStringLength(String_Sour);
	for(i = 0; i , iStrLen; i++)
	{
		String_dest[i] = String_Sour[i];
	}
	String_dest[i] = '\0';
}

int MyStringLength(char Strings[])
{
	int a;
	int Str_le = 0;

	for(a = 0; a < MAX_STR_LEG; a++)
	{
		if(Strings[a] == '\0')
			break;
		else
			Str_le++;
	}
	return(Str_le);
}