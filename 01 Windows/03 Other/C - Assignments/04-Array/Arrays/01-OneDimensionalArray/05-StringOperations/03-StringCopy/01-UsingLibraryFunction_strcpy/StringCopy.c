#include<stdio.h>

#define MAX_STR_LEG 512

int main(void)
{
	char chArr_Real[MAX_STR_LEG], chArr_Copy[MAX_STR_LEG];
	
	//********* STR I/P ************
	printf("\n\n");
	printf("Enter A String :- \n\n");
	gets_s(chArr_Real, MAX_STR_LEG);

	//******** STR COPY **********
	strcpy(chArr_Copy, chArr_Real);

	//******** STR O/P ***********
	printf("\n\n");
	printf("The Real String Entered By You (i.e :- 'chArr_Real[]') Is: \n\n");
	printf("%s\n", chArr_Real);
	
	printf("\n\n");
	printf("The Copy String (i.e :- 'chArr_Real[]') Is: \n\n");
	printf("%s\n", chArr_Copy);
	
	return(0);
}