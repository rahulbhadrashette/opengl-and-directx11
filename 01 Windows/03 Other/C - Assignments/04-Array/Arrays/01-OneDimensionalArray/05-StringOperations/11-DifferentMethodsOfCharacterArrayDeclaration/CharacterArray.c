#include<stdio.h>

int main(void)
{
	char chArr_01[] = {'A', 'S', 'T', 'R', 'O', 'M', 'E', 'D', 'I', 'C', 'O', 'M', 'P', '\0'};
	char chArr_02[9] = {'W', 'E', 'L', 'C', 'O', 'M', 'E', 'S', '\0'};
	char chArr_03[] = {'Y', 'O', 'U', '\0'};
	char chArr_04[] = "To";
	char chArr_05[] = "REAL TIME RANDERING BATCH OF 2018-20";

	char chArr_WithoutNullTerminator[] = { 'H', 'e', 'l', 'l', 'o'};
	
	printf("\n\n");
	printf("Size Of chArr_01 : %d\n\n", sizeof(chArr_01));
	printf("Size Of chArr_02 : %d\n\n", sizeof(chArr_02));
	printf("Size Of chArr_03 : %d\n\n", sizeof(chArr_03));
	printf("Size Of chArr_04 : %d\n\n", sizeof(chArr_04));
	printf("Size Of chArr_05 : %d\n\n", sizeof(chArr_05));

	printf("\n\n");
	
	printf("The String Are : \n\n");
	printf("chArr_01 : %s\n\n", chArr_01);
	printf("chArr_02 : %s\n\n", chArr_02);
	printf("chArr_03 : %s\n\n", chArr_03);
	printf("chArr_04 : %s\n\n", chArr_04);
	printf("chArr_05 : %s\n\n", chArr_05);

	printf("\n\n");
	
	printf("Size of chArr_WithoutNullTerminator : %d\n\n", sizeof(chArr_WithoutNullTerminator));
	printf("chArr_WithoutNullTerminator : %s\n\n", chArr_WithoutNullTerminator);
	
	return(0);
}