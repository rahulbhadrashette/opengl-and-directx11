#include<stdio.h>

#define MAX_STR_LEN 512

int main(void)
{	
	char chArr_One[MAX_STR_LEN], chArr_Two[MAX_STR_LEN];

	printf("\n\n");
	printf("Enter First String :- \n\n");
	gets_s(chArr_One, MAX_STR_LEN);

	printf("\n\n");
	printf("Enter Second String :- \n\n");
	gets_s(chArr_Two, MAX_STR_LEN);

	printf("\n\n");
	printf("********** Before Concatenation *********");
	printf("\n\n");
	printf("The Real First String Entered By You(i.e :- 'chArr_One[]') Is :- \n\n");
	printf("%s\n", chArr_One);
	
	printf("\n\n");
	printf("The Real Second String Entered By You(i.e : 'chArr_Two[]') Is :- \n\n");
	printf("%s\n", chArr_Two);

	printf("\n\n");
	printf("********* After Concatenation *********");
	printf("\n\n");
	printf(" 'chArr_One[]' Is :- \n\n");
	printf("%s\n", chArr_One);
	
	printf("\n\n");
	printf(" 'chArr_Two[]' Is :- \n\n");
	printf("%s\n", chArr_Two);

	return(0);
}