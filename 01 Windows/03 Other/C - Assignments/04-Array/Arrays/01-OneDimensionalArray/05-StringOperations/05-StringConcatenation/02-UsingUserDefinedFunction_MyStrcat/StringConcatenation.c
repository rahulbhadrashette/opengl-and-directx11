#include<stdio.h>

#define MAX_STR_LEN 512

int main(void)
{	
	void MyStringCat(char[], char[]);
	
	char chArr_One[MAX_STR_LEN], chArr_Two[MAX_STR_LEN];

	printf("\n\n");
	printf("Enter First String :- \n\n");
	gets_s(chArr_One, MAX_STR_LEN);

	printf("\n\n");
	printf("Enter Second String :- \n\n");
	gets_s(chArr_Two, MAX_STR_LEN);

	printf("\n\n");
	printf("********** Before Concatenation *********");
	printf("\n\n");
	printf("The Real First String Entered By You(i.e :- 'chArr_One[]') Is :- \n\n");
	printf("%s\n", chArr_One);
	
	printf("\n\n");
	printf("The Real Second String Entered By You(i.e : 'chArr_Two[]') Is :- \n\n");
	printf("%s\n", chArr_Two)

	MyStringcat(chArr_One, chArr_Two);

	printf("\n\n");
	printf("********* After Concatenation *********");
	printf("\n\n");
	printf(" 'chArr_One[]' Is :- \n\n");
	printf("%s\n", chArr_One);
	
	printf("\n\n");
	printf(" 'chArr_Two[]' Is :- \n\n");
	printf("%s\n", chArr_Two);

	return(0);
}

void MyStringCat(char string_dest[], char string_sour[])
{
	int MyStringLength(char[]);

	int iStrLen_source = 0, iStrLen_destination = 0;
	int a, b;

	iStrLen_source = MyStringLength(string_sour);
	iStrLen_destination = MyStringLength(string_dest);

	for(a = iStrLen_destination, b = 0; b < iStrLen_Source; a++, b++)
	{
		string_dest[a] = string_sour[b];
	}
	
	string_dest[a] = '\0';
}

int MyStringLength(char str[])
{
	int a;
	int str_le = 0;
	
	for(a = 0; a < MAX_STR_LEN; a++)
	{
		if(str[a] == '\0')
			break;
		else
			str_le++;
	}
	return(str_le);
}