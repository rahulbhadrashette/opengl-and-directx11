#include<stdio.h>

#define MAX_STR_LEN 512

int main(void)
{
	char chArr[MAX_STR_LEN];
	int iStringLe = 0;
	
	printf("\n\n");
	printf("Enter A String :- \n\n");
	gets_s(chArr,MAX_STR_LEN);
	
	printf("\n\n");
	printf("String Entered By You Is :- \n\n");
	printf("%s\n",chArr);
	
	printf("\n\n");
	iStringLe = strlen(chArr);
	printf("Length Of String Is = %d Characters...!!!\n\n", iStringLe);
	return(0);
}