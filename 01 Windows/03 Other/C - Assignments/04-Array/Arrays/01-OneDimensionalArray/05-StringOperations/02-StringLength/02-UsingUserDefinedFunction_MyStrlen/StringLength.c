#include<stdio.h>

#define MAX_STR_LEN 512

int main(void)
{
	int MyStringLength(char[]);
	
	char chArr[MAX_STR_LEN];
	int iStringLe = 0;
	
	printf("\n\n");
	printf("Enter A String :- \n\n");
	gets_s(chArr,MAX_STR_LEN);
	
	printf("\n\n");
	printf("String Entered By You Is :- \n\n");
	printf("%s\n",chArr);
	
	printf("\n\n");
	iStringLe = strlen(chArr);
	printf("Length Of String Is = %d Characters...!!!\n\n", iStringLe);
	return(0);
}

int MyStringLength(char str[])
{
	int i;
	int str_le = 0;
	for(i = 0; i < MAX_STR_LEN; i++)
	{
		if(str[i] == '\0')
			break;
		else
		{
			str_le++;
		}
	}
	return(str_le);
}