#include<stdio.h>
int main(void)

{
	//variable declaration
	int iArr[] = { 9, 30, 6, 12, 98, 95, 20, 23, 2, 45};
	int int_W;
	int iArr_W;
	int iArr_Element;

	float fArr[] = {1.2f, 2.3f, 3.4f, 4.5f, 5.6f, 6.7f, 7.8f, 8.9f}; 
	int float_W;
	int fArr_W;
	int fArr_Element;

	char cArr[] = { 'A', 'S', 'T', 'R', 'O', 'M', 'E', 'D', 'I', 'C', 'O', 'M', 'P'};
	int char_W;
	int cArr_W;
	int cArr_Element;

	int i;

	//code
	
	//************iArray[]*****************
	printf("\n\n");
	printf("In-Line Initialization And Loop (for) Display Of Elements Of Array 'iArray[]' :-  \n\n");

	int_W = sizeof(int);
	iArr_W = sizeof(iArr);
	iArr_Element = iArr_W / int_W;

	for(i = 0; i < iArr_Element; i++)
	{
		printf("iArray[%d] (Element %d) = %d \n", i, (i + 1), iArr[i]);
	}

	printf("\n\n");
	printf("Size of Data type 'int' 				= %d Bytes\n", int_W);
	printf("Number Of Element In 'int' Array 'iArray[]'		= %d Elements \n", iArr_Element);
	printf("Size Of Array 'iArray[]' (%d Elements * %d Bytes)	= %d Bytes \n\n", iArr_Element, int_W, iArr_W);

	//*************fArray*****************
	printf("\n\n");
	printf("In-Line Initialization And Loop (while) Display Of Element Of Array 'fArray[]' :- \n\n");

	float_W = sizeof(float);
	fArr_W = sizeof(fArr);
	fArr_Element = fArr_W / float_W;

	i = 0;
	while(i < fArr_Element)
	{
		printf("fArray[%d] (Element %d) = %f\n", i, (i + 1), fArr[i]);
		i++;
	}

	
	printf("\n\n");
	printf("Size of Data Type 'float' 				= %d Bytes\n", float_W);
	printf("Number Of Elements In 'float' Array 'fArray[]' 		= %d Elements \n", fArr_Element);
	printf("Size Of Array 'fArray[]' (%d Element * %d Bytes)		= %d Bytes \n\n", fArr_Element, float_W, fArr_W);

	
	//**************cArray[]***************
	printf("\n\n");
	printf("In-Line Initialization And Loop (do while) Display Of Elements Of Array 'cArray[]' :- \n\n");

	char_W = sizeof(char);
	cArr_W = sizeof(cArr);
	cArr_Element = cArr_W / char_W;
	
	i = 0;
	do
	{
		printf(" cArray[%d] (Element %d) = %c\n",i, (i + 1), cArr[i]);
		i++;
	}while(i < cArr_Element);

	printf("\n\n");
	printf("Size of Data Type 'char'				= %d Bytes\n", char_W);
	printf("Number Of Element In 'char' Array 'cArray[]'		= %d Element\n",cArr_Element);
	printf("Size Of Array 'cArray[]' (%d Elenment * %d Bytes)	= %d Bytes\n\n", cArr_Element, char_W, cArr_W);

	return(0);
	
}