#include<stdio.h>
int main(void)

{
	//variable declaration
	int iArr[] = { 9, 30, 6, 12, 98, 95, 20, 23, 2, 45};
	int int_W;
	int iArr_W;
	int iArr_Element;

	float fArr[] = {1.2f, 2.3f, 3.4f, 4.5f, 5.6f, 6.7f, 7.8f, 8.9f}; 
	int float_W;
	int fArr_W;
	int fArr_Element;

	char cArr[] = { 'A', 'S', 'T', 'R', 'O', 'M', 'E', 'D', 'I', 'C', 'O', 'M', 'P'};
	int char_W;
	int cArr_W;
	int cArr_Element;

	//code
	
	//****************iArr[]*******************
	printf("\n\n");
	printf("In-Line Initialization And Pice-meal Display Of Elements Of Array 'iArray[]' :- \n\n");
	printf("iArr[0] (1st Element) = %d\n", iArr[0]);
	printf("iArr[1] (2nd Element) = %d\n", iArr[1]);
	printf("iArr[2] (3rd Element) = %d\n", iArr[2]);
	printf("iArr[3] (4th Element) = %d\n", iArr[3]);
	printf("iArr[4] (5th Element) = %d\n", iArr[4]);
	printf("iArr[5] (6th Element) = %d\n", iArr[5]);
	printf("iArr[6] (7th Element) = %d\n", iArr[6]);
	printf("iArr[7] (8th Element) = %d\n", iArr[7]);
	printf("iArr[8] (9th Element) = %d\n", iArr[8]);
	printf("iArr[9] (10th Element) = %d\n\n", iArr[9]);

	//***************************
	

	int_W = sizeof(int);
	iArr_W = sizeof(iArr);
	iArr_Element = iArr_W / int_W;
	printf("Size Of Data Type 'int'				=  %d Bytes \n", int_W);
	printf("Number Of Elements In 'int' Array 'iArr[]'      =  %d Elements \n", iArr_Element);
	printf("Size Of Array 'iArr[]' (%d Elements * %d Bytes)  =  %d Bytes\n\n", iArr_Element, int_W, iArr_W);


	//****************fArr[]*******************
	printf("\n\n");
	printf("In-Line Initialization And Pice-meal Display Of Elements Of Array 'fArray[]' :- \n\n");
	printf("fArr[0] (1st Element) = %f\n", fArr[0]);
	printf("fArr[1] (2nd Element) = %f\n", fArr[1]);
	printf("fArr[2] (3rd Element) = %f\n", fArr[2]);
	printf("fArr[3] (4th Element) = %f\n", fArr[3]);
	printf("fArr[4] (5th Element) = %f\n", fArr[4]);
	printf("fArr[5] (6th Element) = %f\n", fArr[5]);
	printf("fArr[6] (7th Element) = %f\n", fArr[6]);
	printf("fArr[7] (8th Element) = %f\n", fArr[7]);
	printf("fArr[8] (9th Element) = %f\n", fArr[8]);
	printf("fArr[9] (10th Element) = %f\n\n", fArr[9]);

	float_W = sizeof(float);
	fArr_W = sizeof(fArr);
	fArr_Element = fArr_W / float_W;
	printf("Size Of Data Type 'float'			=  %d Bytes \n", float_W);
	printf("Number Of Elements In 'float' Array 'fArr[]'    =  %d Elements \n", fArr_Element);
	printf("Size Of Array 'fArr[]' (%d Elements * %d Bytes)   =  %d Bytes\n\n", fArr_Element, float_W, fArr_W);


	//****************fArr[]*******************
	printf("\n\n");
	printf("In-Line Initialization And Pice-meal Display Of Elements Of Array 'cArray[]' :- \n\n");
	printf("cArr[0] (1st Element)   = %c\n", cArr[0]);
	printf("cArr[1] (2nd Element)   = %c\n", cArr[1]);
	printf("cArr[2] (3rd Element)   = %c\n", cArr[2]);
	printf("cArr[3] (4th Element)   = %c\n", cArr[3]);
	printf("cArr[4] (5th Element)   = %c\n", cArr[4]);
	printf("cArr[5] (6th Element)   = %c\n", cArr[5]);
	printf("cArr[6] (7th Element)   = %c\n", cArr[6]);
	printf("cArr[7] (8th Element)   = %c\n", cArr[7]);
	printf("cArr[8] (9th Element)   = %c\n", cArr[8]);
	printf("cArr[9] (10th Element)  = %c\n", cArr[9]);
	printf("cArr[10] (11th Element) = %c\n", cArr[10]);
	printf("cArr[11] (12th Element) = %c\n", cArr[11]);
	printf("cArr[12] (13th Element) = %c\n\n", cArr[12]);

	char_W = sizeof(char);
	cArr_W = sizeof(cArr);
	cArr_Element = cArr_W / char_W;
	printf("Size Of Data Type 'char'			=  %d Bytes \n", char_W);
	printf("Number Of Elements In 'char' Array 'cArr[]'     =  %d Elements \n", cArr_Element);
	printf("Size Of Array 'cArr[]' (%d Elements * %d Bytes)  =  %d Bytes\n\n", cArr_Element, char_W, cArr_W);

	return(0);
}