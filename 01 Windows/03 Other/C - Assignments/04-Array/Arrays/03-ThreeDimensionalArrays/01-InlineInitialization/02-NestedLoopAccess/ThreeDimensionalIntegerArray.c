#include<stdio.h>
int main(void)
{
	int iArr[5][3][2] = {  { {1,2}, {3, 4}, {5, 6} },
			       { {7, 8}, {9, 10}, {11, 12} },
			       { {13, 14}, {15, 16}, {17, 18} },
			       { {19, 20}, {21, 22}, {23, 24} },
			       { {25, 26}, {27, 28}, {29, 30} } };
	int int_size;
	int iArr_size;
	int iArr_num_elements, iArr_width, iArr_height, iArr_depth;
	int a, b, c;

	printf("\n\n");
	
	int_size = sizeof(int);
	
	iArr_size = sizeof(iArr);
	printf("Size Of Three Dimantional (3D) Integer Array Is = %d\n\n", iArr_size);

	iArr_width = iArr_size / sizeof(iArr[0]);
	printf("Number Of Rows (width) In Three Dimantional (3D) Integer Array Is = %d\n\n", iArr_width);
	
	iArr_height = sizeof(iArr[0]) / sizeof(iArr[0][0]); 
	printf("Number Of Columns (height) In Three Dimantional (3D) Integer Array Is = %d\n\n", iArr_height);

	iArr_depth = sizeof(iArr[0][0]) / int_size;
	printf("Depth In Three Dimantional (3D) Integer Array Is = %d\n\n", iArr_depth);

	iArr_num_elements = iArr_width * iArr_height * iArr_depth;; 
	printf("Number Of Elements In Three Dimantional (3D) Integer Array Is = %d\n\n", iArr_num_elements);

	printf("\n\n");
	printf("Elements In The 3D Array : \n\n");
	
	for(a = 0; a < iArr_width; a++)
	{
		printf("********* ROW %d **********\n",(a + 1));
		for(b = 0; b < iArr_height; b++)
		{
			printf("********* COLUMNS %d **********\n",(b + 1));
			for(c = 0; c < iArr_depth; c++)
			{
				printf("iArr[%d][%d][%d] = %d\n", a, b, c, iArr[a][b][c]);
			}
			printf("\n");
		}
		printf("\n\n");
	}
	
	return(0);

}