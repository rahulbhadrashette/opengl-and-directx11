#include<stdio.h>
int main(void)
{
	int iArr[5][3][2] = {  { {1,2}, {3, 4}, {5, 6} },
			       { {7, 8}, {9, 10}, {11, 12} },
			       { {13, 14}, {15, 16}, {17, 18} },
			       { {19, 20}, {21, 22}, {23, 24} },
			       { {25, 26}, {27, 28}, {29, 30} } };
	int int_size;
	int iArr_size;
	int iArr_num_elements, iArr_width, iArr_height, iArr_depth;

	printf("\n\n");
	
	int_size = sizeof(int);
	
	iArr_size = sizeof(iArr);
	printf("Size Of Three Dimantional (3D) Integer Array Is = %d\n\n", iArr_size);

	iArr_width = iArr_size / sizeof(iArr[0]);
	printf("Number Of Rows (width) In Three Dimantional (3D) Integer Array Is = %d\n\n", iArr_width);
	
	iArr_height = sizeof(iArr[0]) / sizeof(iArr[0][0]); 
	printf("Number Of Columns (height) In Three Dimantional (3D) Integer Array Is = %d\n\n", iArr_height);

	iArr_depth = sizeof(iArr[0][0]) * int_size;
	printf("Depth In Three Dimantional (3D) Integer Array Is = %d\n\n", iArr_depth);

	iArr_num_elements = iArr_width * iArr_height * iArr_depth;; 
	printf("Number Of Elements In Three Dimantional (3D) Integer Array Is = %d\n\n", iArr_num_elements);

	printf("\n\n");
	printf("Elements In The 3D Array : \n\n");
	
	printf("********** ROW 1 ********\n");
	printf("********** COLUMN 1 ********\n");
	printf("iArr[0][0][0] = %d\n", iArr[0][0][0]);
	printf("iArr[0][0][1] = %d\n", iArr[0][0][1]);
	printf("\n");

	printf("********** COLUMN 2 ********\n");
	printf("iArr[0][1][0] = %d\n", iArr[0][1][0]);
	printf("iArr[0][1][1] = %d\n", iArr[0][1][1]);
	printf("\n");

	printf("********** COLUMN 3 ********\n");
	printf("iArr[0][2][0] = %d\n", iArr[0][2][0]);
	printf("iArr[0][2][1] = %d\n", iArr[0][2][1]);
	printf("\n\n");

	printf("********** ROW 2 ********\n");
	printf("********** COLUMN 1 ********\n");
	printf("iArr[1][0][0] = %d\n", iArr[1][0][0]);
	printf("iArr[1][0][1] = %d\n", iArr[1][0][1]);
	printf("\n");

	printf("********** COLUMN 2 ********\n");
	printf("iArr[1][1][0] = %d\n", iArr[1][1][0]);
	printf("iArr[1][1][1] = %d\n", iArr[2][1][1]);
	printf("\n");

	printf("********** COLUMN 3 ********\n");
	printf("iArr[1][2][0] = %d\n", iArr[1][2][0]);
	printf("iArr[1][2][1] = %d\n", iArr[1][2][1]);
	printf("\n\n");
	
	printf("********** ROW 3 ********\n");
	printf("********** COLUMN 1 ********\n");
	printf("iArr[2][0][0] = %d\n", iArr[2][0][0]);
	printf("iArr[2][0][1] = %d\n", iArr[2][0][1]);
	printf("\n");

	printf("********** COLUMN 2 ********\n");
	printf("iArr[2][1][0] = %d\n", iArr[2][1][0]);
	printf("iArr[2][1][1] = %d\n", iArr[2][1][1]);
	printf("\n");

	printf("********** COLUMN 3 ********\n");
	printf("iArr[2][2][0] = %d\n", iArr[2][2][0]);
	printf("iArr[2][2][1] = %d\n", iArr[2][2][1]);
	printf("\n\n");

	printf("********** ROW 4 ********\n");
	printf("********** COLUMN 1 ********\n");
	printf("iArr[3][0][0] = %d\n", iArr[3][0][0]);
	printf("iArr[3][0][1] = %d\n", iArr[3][0][1]);
	printf("\n");

	printf("********** COLUMN 2 ********\n");
	printf("iArr[3][1][0] = %d\n", iArr[3][1][0]);
	printf("iArr[3][1][1] = %d\n", iArr[3][1][1]);
	printf("\n");

	printf("********** COLUMN 3 ********\n");
	printf("iArr[3][2][0] = %d\n", iArr[3][2][0]);
	printf("iArr[3][2][1] = %d\n", iArr[3][2][1]);
	printf("\n\n");

	printf("********** ROW 5 ********\n");
	printf("********** COLUMN 1 ********\n");
	printf("iArr[4][0][0] = %d\n", iArr[4][0][0]);
	printf("iArr[4][0][1] = %d\n", iArr[4][0][1]);
	printf("\n");

	printf("********** COLUMN 2 ********\n");
	printf("iArr[4][1][0] = %d\n", iArr[4][1][0]);
	printf("iArr[4][1][1] = %d\n", iArr[4][1][1]);
	printf("\n");

	printf("********** COLUMN 3 ********\n");
	printf("iArr[4][2][0] = %d\n", iArr[4][2][0]);
	printf("iArr[4][2][1] = %d\n", iArr[4][2][1]);
	printf("\n\n");
	
	return(0);

}