#include<stdio.h>

#define NUMBER_ROWS 5
#define NUMBER_COLUMNS 3
#define DEPTH 2

int main(void)
{
	int iArr[NUMBER_ROWS][NUMBER_COLUMNS][DEPTH] = {  { {1,2}, {3, 4}, {5, 6} },
			      			          { {7, 8}, {9, 10}, {11, 12} },
			  			          { {13, 14}, {15, 16}, {17, 18} },
			  			          { {19, 20}, {21, 22}, {23, 24} },
			    			          { {25, 26}, {27, 28}, {29, 30} } };

	int a, b, c;

	int iArr_1D[NUMBER_ROWS * NUMBER_COLUMNS * DEPTH];

	printf("\n\n");
	printf("Elements In The 3D Array : \n\n");
	
	for(a = 0; a < NUMBER_ROWS; a++)
	{
		printf("********* ROW %d **********\n",(a + 1));
		for(b = 0; b < NUMBER_COLUMNS; b++)
		{
			printf("********* COLUMNS %d **********\n",(b + 1));
			for(c = 0; c < DEPTH; c++)
			{
				printf("iArr[%d][%d][%d] = %d\n", a, b, c, iArr[a][b][c]);
			}
			printf("\n");
		}
		printf("\n");
	}

	// ******* Converting 3D to 1D ***********
	for(a = 0; a < NUMBER_ROWS; a++)
	{
		for(b = 0; b < NUMBER_COLUMNS; b++)
		{
			for(c = 0; c < DEPTH; c++)
			{
				iArr_1D[(a * NUMBER_COLUMNS * DEPTH) + (b * DEPTH) + c] = iArr[a][b][c];
			}
		}
	}

	// ******* Display 2D Array ***********
	printf("\n\n\n\n");
	printf("Elements In The 1D Array : \n\n");
	for(a = 0; a < (NUMBER_ROWS * NUMBER_COLUMNS * DEPTH); a++)
	{
		printf("iArr_1D[%d] = %d\n", a,  iArr_1D[a]);
	}
	return(0);
}