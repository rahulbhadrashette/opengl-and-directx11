#include<stdio.h>

#define NUMBER_ROWS 5
#define NUMBER_COLUMNS 3
#define DEPTH 2

int main(void)
{
	int iArr[NUMBER_ROWS][NUMBER_COLUMNS][DEPTH] = {  { {1,2}, {3, 4}, {5, 6} },
			      			          { {7, 8}, {9, 10}, {11, 12} },
			  			          { {13, 14}, {15, 16}, {17, 18} },
			  			          { {19, 20}, {21, 22}, {23, 24} },
			    			          { {25, 26}, {27, 28}, {29, 30} } };

	int a, b, c;

	int iArr_2D[NUMBER_ROWS][NUMBER_COLUMNS * DEPTH];

	printf("\n\n");
	printf("Elements In The 3D Array : \n\n");
	
	for(a = 0; a < NUMBER_ROWS; a++)
	{
		printf("********* ROW %d **********\n",(a + 1));
		for(b = 0; b < NUMBER_COLUMNS; b++)
		{
			printf("********* COLUMNS %d **********\n",(b + 1));
			for(c = 0; c < DEPTH; c++)
			{
				printf("iArr[%d][%d][%d] = %d\n", a, b, c, iArr[a][b][c]);
			}
			printf("\n");
		}
		printf("\n");
	}

	// ******* Converting 2D to 3D ***********
	for(a = 0; a < NUMBER_ROWS; a++)
	{
		for(b = 0; b < NUMBER_COLUMNS; b++)
		{
			for(c = 0; c < DEPTH; c++)
			{
				iArr_2D[a][(b * DEPTH) + c] = iArr[a][b][c];
			}
		}
	}

	// ******* Display 2D Array ***********
	printf("\n\n\n\n");
	printf("Elements In The 2D Array : \n\n");
	for(a = 0; a < NUMBER_ROWS; a++)
	{
		printf("********* ROW %d **********\n",(a + 1));
		for(b = 0; b < (NUMBER_COLUMNS * DEPTH); b++)
		{
			printf("iArr_2D[%d][%d] = %d\n", a, b, iArr_2D[a][b]);
		}
		printf("\n");
	}
	return(0);
}