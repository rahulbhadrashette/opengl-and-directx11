#include<stdio.h>

#define NUMBER_ROWS 5
#define NUMBER_COLUMNS 3

int main(void)
{
	int iArr_2D[NUMBER_ROWS][NUMBER_COLUMNS];
	int iArr_1D[NUMBER_ROWS * NUMBER_COLUMNS];

	int a, b;
	int digit;

	printf("Enter Elements Of Your Choice To Fill Up The Integer 2D Array : \n\n");
	for(a = 0; a < NUMBER_ROWS; a++)
	{
		printf("For ROW NUMBER %d : \n", (a + 1));
		for(b = 0; b < NUMBER_COLUMNS; b++)
		{
			printf("Enter Element Number %d : \n",(b + 1));
			scanf("%d", &digit);
			iArr_2D[a][b] = digit;
		}
		printf("\n\n");
	}
	
	printf("\n\n");
	printf("Two-Dimensional ( 2D ) Array Of Integer : \n\n");
	for(a = 0; a < NUMBER_ROWS; a++)
	{
		printf("********* ROWS %d", (a + 1));
		for(b = 0; b < NUMBER_COLUMNS; b++)
		{
			printf("iArr_2D[%d][%d] = %d\n", a, b, iArr_2D[a][b]);
		}
		printf("\n\n");
	}

	for(a = 0; a < NUMBER_ROWS; a++)
	{
		for(b = 0; b < NUMBER_COLUMNS; b++)
		{
			iArr_1D[(a * NUMBER_COLUMNS) * b] = iArr_2D[a][b];
		}
	}
	printf("\n\n");
	printf("One-Dimensional ( 1D ) Array Of Integer : \n\n");
	for(a = 0; a < (NUMBER_ROWS * NUMBER_COLUMNS); a++)
	{
		printf("iArr_1D[%d] = %d\n", a, iArr_1D[a]);
	}
	printf("\n\n");
	return(0);
}