#include<stdio.h>
int main(void)
{
	int iArr[5][3] = { {1,2,3}, {4,5,6}, {7,8,9}, {4,8,12}, {5,10,15} };
	int int_Size;
	int iArr_Size;
	int iArr_num_elements, iArr_num_rows, iArr_num_Columns;

	printf("\n\n");
	
	int_Size = sizeof(int);
	
	iArr_Size = sizeof(iArr);
	printf("Size Of Two Dimantional (2D) Integer Array Is = %d\n\n", iArr_Size);

	iArr_num_rows = iArr_Size / sizeof(iArr[0]);
	printf("Number Of Rows In Two Dimantional (2D) Integer Array Is = %d\n\n", iArr_num_rows);
	
	iArr_num_Columns = sizeof(iArr[0]) / int_Size; 
	printf("Number Of Columns In Two Dimantional (2D) Integer Array Is = %d\n\n", iArr_num_Columns);

	iArr_num_elements = iArr_num_rows * iArr_num_Columns;
	printf("Number Of Elements In Two Dimantional (2D) Integer Array Is = %d\n\n", iArr_num_elements);

	printf("\n\n");
	printf("Elements In The 2D Array : \n\n");
	
	printf("********** ROW 1 ********\n");
	printf("iArr[0][0] = %d\n", iArr[0][0]);
	printf("iArr[0][1] = %d\n", iArr[0][1]);
	printf("iArr[0][2] = %d\n", iArr[0][2]);

	printf("\n\n");
	printf("********** ROW 2 ********\n");
	printf("iArr[1][0] = %d\n", iArr[1][0]);
	printf("iArr[1][1] = %d\n", iArr[1][1]);
	printf("iArr[1][2] = %d\n", iArr[1][2]);

	printf("\n\n");
	printf("********** ROW 3 ********\n");
	printf("iArr[2][0] = %d\n", iArr[2][0]);
	printf("iArr[2][1] = %d\n", iArr[2][1]);
	printf("iArr[2][2] = %d\n", iArr[2][2]);

	printf("\n\n");
	printf("********** ROW 4 ********\n");
	printf("iArr[3][0] = %d\n", iArr[3][0]);
	printf("iArr[3][1] = %d\n", iArr[3][1]);
	printf("iArr[3][2] = %d\n", iArr[3][2]);

	printf("\n\n");
	printf("********** ROW 5 ********\n");
	printf("iArr[4][0] = %d\n", iArr[4][0]);
	printf("iArr[4][1] = %d\n", iArr[4][1]);
	printf("iArr[4][2] = %d\n", iArr[4][2]);

	printf("\n\n");
	return(0);

}