#include<stdio.h>
int main(void)
{
	int iArr[5][3] = { {1,2,3}, {4,5,6}, {7,8,9}, {4,8,12}, {5,10,15} };
	int int_Size;
	int iArr_Size;
	int iArr_num_elements, iArr_num_rows, iArr_num_Columns;
	int a, b;

	printf("\n\n");
	
	int_Size = sizeof(int);
	
	iArr_Size = sizeof(iArr);
	printf("Size Of Two Dimantional (2D) Integer Array Is = %d\n\n", iArr_Size);

	iArr_num_rows = iArr_Size / sizeof(iArr[0]);
	printf("Number Of Rows In Two Dimantional (2D) Integer Array Is = %d\n\n", iArr_num_rows);
	
	iArr_num_Columns = sizeof(iArr[0]) / int_Size; 
	printf("Number Of Columns In Two Dimantional (2D) Integer Array Is = %d\n\n", iArr_num_Columns);

	iArr_num_elements = iArr_num_rows * iArr_num_Columns;
	printf("Number Of Elements In Two Dimantional (2D) Integer Array Is = %d\n\n", iArr_num_elements);

	printf("\n\n");
	printf("Elements In The 2D Array : \n\n");

	for(a = 0; a < iArr_num_rows; a++)
	{
		printf("*********** ROW %d ***********\n", (a + 1));
		for(b = 0; b < iArr_num_Columns; b++)
		{
			printf("iArr[%d][%d] = %d\n", a, b, iArr[a][b]);
		}
		printf("\n\n");
	}
	return(0);

}