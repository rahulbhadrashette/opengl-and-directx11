#include<stdio.h>

#define MAX_STR_LEN 512

int main(void)
{	
	int MyStrlength(char[]);
	
	char struArr[10][15] = { "Hello", "WelCome", "To", "Real", "Time", "Rendering", "Batch", "(2018-19-20)", "Of", "ASTROMEDICOMP."};
	int char_Size;
	int struArr_Size;
	int struArr_num_elements, struArr_num_rows, struArr_num_Columns;
	int struActual_num_chars = 0;
	int a;

	printf("\n\n");
	
	char_Size = sizeof(char);
	
	struArr_Size = sizeof(struArr);
	printf("Size Of Two Dimantional (2D) Character Array (String Array) Is = %d\n\n", struArr_Size);

	struArr_num_rows = struArr_Size / sizeof(struArr[0]);
	printf("Number Of Rows (Strings) In Two Dimantional (2D) Character Array (String Array) Is = %d\n\n", struArr_num_rows);
	
	struArr_num_Columns = sizeof(struArr[0]) / char_Size; 
	printf("Number Of Columns In Two Dimantional (2D) Character Array (String Array) Is = %d\n\n", struArr_num_Columns);

	struArr_num_elements = struArr_num_rows * struArr_num_Columns;
	printf("Maximun Number Of Elements (Character) In Two Dimantional (2D) Character Array (String Array) Is = %d\n\n", struArr_num_elements);

	for(a = 0; a < struArr_num_rows; a++)
	{
		struActual_num_chars = struActual_num_chars + MyStrlength(struArr[a]);
	}
	printf("Actual Number Of Elements (Character) In Two Dimantional (2D) Character Array (String Array) Is = %d\n\n", struActual_num_chars);

	printf("\n\n");
	printf("Strings In The 2D Array : \n\n");
	
	printf("%s", struArr[0]);
	printf("%s", struArr[1]);
	printf("%s", struArr[2]);
	printf("%s", struArr[3]);
	printf("%s", struArr[4]);
	printf("%s", struArr[5]);
	printf("%s", struArr[6]);
	printf("%s", struArr[7]);
	printf("%s", struArr[8]);
	printf("%s\n\n", struArr[9]);
	
	return(0);

}

int MyStrlength(char stru[])
{
	int a;
	int str_length = 0;
	
	for(a = 0; a < MAX_STR_LEN; a++)
	{
		if(stru[a] == '\0')
			break;
		else
			str_length++;
	}
	return(str_length);
}