#include<stdio.h>

#define MAX_STR_LEN 512

int main(void)
{	
	int MyStrlength(char[]);
	
	char struArr[10][15] = { "Hello", "WelCome", "To", "Real", "Time", "Rendering", "Batch", "(2018-19-20)", "Of", "ASTROMEDICOMP."};
	int istruLen[10];
	int struArr_size;
	int struArr_num_rows;
	int a,b;
	
	struArr_size = sizeof(struArr);
	struArr_num_rows = struArr_size / sizeof(struArr[0]);
	
	for(a = 0; a < struArr_num_rows; a++)
	{
		istruLen[a] = MyStrlength(struArr[a]);
	}

	printf("\n\n");
	printf("The Entire String Array : \n\n");
	for(a = 0; a < struArr_num_rows; a++)
	{
		printf("%s", struArr[a]);
	}

	printf("\n\n");
	printf("Strings In The 2D Array : \n\n");
	
	for(a = 0; a < struArr_num_rows; a++)
	{
		printf("String Number %d => %s \n\n", (a + 1), struArr[a]);
		for(b = 0; b < istruLen[a]; b++)
		{
			printf("Character %d = %c\n", (b + 1), struArr[a][b]);
		}
		printf("\n\n");
	}
	
	return(0);

}

int MyStrlength(char stru[])
{
	int a;
	int str_length = 0;
	
	for(a = 0; a < MAX_STR_LEN; a++)
	{
		if(stru[a] == '\0')
			break;
		else
			str_length++;
	}
	return(str_length);
}