#include<stdio.h>
int main(void)
{
	int iArr[3][5];
	int int_Size;
	int iArr_Size;
	int iArr_num_elements, iArr_num_rows, iArr_num_Columns;
	int a, b;

	printf("\n\n");
	
	int_Size = sizeof(int);
	
	iArr_Size = sizeof(iArr);
	printf("Size Of Two Dimantional (2D) Integer Array Is = %d\n\n", iArr_Size);

	iArr_num_rows = iArr_Size / sizeof(iArr[0]);
	printf("Number Of Rows In Two Dimantional (2D) Integer Array Is = %d\n\n", iArr_num_rows);
	
	iArr_num_Columns = sizeof(iArr[0]) / int_Size; 
	printf("Number Of Columns In Two Dimantional (2D) Integer Array Is = %d\n\n", iArr_num_Columns);

	iArr_num_elements = iArr_num_rows * iArr_num_Columns;
	printf("Number Of Elements In Two Dimantional (2D) Integer Array Is = %d\n\n", iArr_num_elements);

	printf("\n\n");
	printf("Elements In The 2D Array : \n\n");
	
	//printf("********** ROW 1 ********\n");
	iArr[0][0] = 21;
	iArr[0][1] = 42;
	iArr[0][2] = 63;
	iArr[0][3] = 84;
	iArr[0][4] = 105;

	//********** ROW 2 ********
	iArr[1][0] = 22;
	iArr[1][1] = 44;
	iArr[1][2] = 66;
	iArr[1][3] = 88;
	iArr[1][4] = 110;

	//********** ROW 3 ********
	iArr[2][0] = 23;
	iArr[2][1] = 46;
	iArr[2][2] = 69;
	iArr[2][3] = 92;
	iArr[2][4] = 115;

	for(a = 0; a < iArr_num_rows; a++)
	{
		printf("********** ROW %d *********\n", (a + 1));
		for(b = 0; b < iArr_num_Columns; b++)
		{
			printf("iArr[%d][%d] = %d\n", a, b, iArr[a][b]);
		}
		printf("\n\n");
	}
	return(0);

}