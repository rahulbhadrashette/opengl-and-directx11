#include<stdio.h>

#define MAX_STR_LEN 512

int main(void)
{	
	void MyStrCopy(char[], char[]);
	
	char struArr[5][10];
	int char_Size;
	int struArr_Size;
	int struArr_num_elements, struArr_num_rows, struArr_num_Columns;
	int a;

	printf("\n\n");
	
	char_Size = sizeof(char);
	
	struArr_Size = sizeof(struArr);
	printf("Size Of Two Dimantional (2D) Character Array (String Array) Is = %d\n\n", struArr_Size);

	struArr_num_rows = struArr_Size / sizeof(struArr[0]);
	printf("Number Of Rows (Strings) In Two Dimantional (2D) Character Array (String Array) Is = %d\n\n", struArr_num_rows);
	
	struArr_num_Columns = sizeof(struArr[0]) / char_Size; 
	printf("Number Of Columns In Two Dimantional (2D) Character Array (String Array) Is = %d\n\n", struArr_num_Columns);

	struArr_num_elements = struArr_num_rows * struArr_num_Columns;
	printf("Maximun Number Of Elements (Character) In Two Dimantional (2D) Character Array (String Array) Is = %d\n\n", struArr_num_elements);

	//***********************
	MyStrCopy(struArr[0], "My");
	MyStrCopy(struArr[1], "Name");
	MyStrCopy(struArr[2], "Is");
	MyStrCopy(struArr[3], "Rahul.M.");
	MyStrCopy(struArr[4], "Bhadrashette");

	printf("\n\n");
	printf("The Strings In The 2D Character Array Are: \n\n");

	for(a = 0; a < struArr_num_rows; a++)
	{
		printf("%s", struArr[a]);
	}
	printf("\n\n");
	return(0);

}

void MyStrCopy(char stru_dest[], char stru_source[])
{
	int MyStrLength(char[]);
	
	int a;
	int str_length = 0;
	
	str_length =  MyStrLength(stru_source);
	for(a = 0; a < MAX_STR_LEN; a++)
	{
		stru_dest[a] = stru_source[a];
	}
	stru_dest[a] = '\0';
}

int MyStrLength(char stru[])
{
	
	int a;
	int str_length = 0;
	
	for(a = 0; a < MAX_STR_LEN; a++)
	{
		if(stru[a] == '\0')
			break;
		else
			str_length++;
	}
	return(str_length);
}