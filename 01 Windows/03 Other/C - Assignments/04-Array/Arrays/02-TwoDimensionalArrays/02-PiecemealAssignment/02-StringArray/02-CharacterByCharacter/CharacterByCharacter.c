#include<stdio.h>

#define MAX_STR_LEN 512

int main(void)
{	
	int MyStrlength(char[]);
	
	char struArr[5][10];
	int char_Size;
	int struArr_Size;
	int struArr_num_elements, struArr_num_rows, struArr_num_Columns;
	int a;

	printf("\n\n");
	
	char_Size = sizeof(char);
	
	struArr_Size = sizeof(struArr);
	printf("Size Of Two Dimantional (2D) Character Array (String Array) Is = %d\n\n", struArr_Size);

	struArr_num_rows = struArr_Size / sizeof(struArr[0]);
	printf("Number Of Rows (Strings) In Two Dimantional (2D) Character Array (String Array) Is = %d\n\n", struArr_num_rows);
	
	struArr_num_Columns = sizeof(struArr[0]) / char_Size; 
	printf("Number Of Columns In Two Dimantional (2D) Character Array (String Array) Is = %d\n\n", struArr_num_Columns);

	struArr_num_elements = struArr_num_rows * struArr_num_Columns;
	printf("Maximun Number Of Elements (Character) In Two Dimantional (2D) Character Array (String Array) Is = %d\n\n", struArr_num_elements);

	//******** ROW 1 STRING 1 **********
	struArr[0][0] = 'M';
	struArr[0][1] = 'y';
	struArr[0][2] = '\0';

	//******** ROW 2 STRING 2 **********
	struArr[1][0] = 'N';
	struArr[1][1] = 'a';
	struArr[1][2] = 'm';
	struArr[1][3] = 'e';
	struArr[1][4] = '\0';

	//******** ROW 3 STRING 3 **********
	struArr[2][0] = 'I';
	struArr[2][1] = 's';
	struArr[2][2] = '\0';


	//******** ROW 4 STRING 4 **********
	struArr[3][0] = 'R';
	struArr[3][1] = 'a';
	struArr[3][2] = 'h';
	struArr[3][3] = 'u';
	struArr[3][4] = 'l';
	struArr[3][5] = '.';
	struArr[3][6] = 'M';
	struArr[3][7] = '.';
	struArr[3][8] = '\0';

	//******** ROW 5 STRING 5 **********
	struArr[4][0] = 'B';
	struArr[4][1] = 'h';
	struArr[4][2] = 'a';
	struArr[4][3] = 'd';
	struArr[4][4] = 'r';
	struArr[4][5] = 'a';
	struArr[4][6] = 's';
	struArr[4][7] = 'h';
	struArr[4][8] = 'e';
	struArr[4][9] = 't';
	struArr[4][10] = 't';
	struArr[4][11] = 'e';
	struArr[4][12]= '\0';


	printf("\n\n");
	printf("Strings In The 2D Array : \n\n");
	
	for(a = 0; a <struArr_num_rows; a++)
	{
		printf("%s", struArr[a]);
	}
	printf("\n\n");
	return(0);

}

int MyStrlength(char stru[])
{
	int a;
	int str_length = 0;
	
	for(a = 0; a < MAX_STR_LEN; a++)
	{
		if(stru[a] == '\0')
			break;
		else
			str_length++;
	}
	return(str_length);
}