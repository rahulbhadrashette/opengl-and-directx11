#include<stdio.h>

int main(int argc, char *argv[], char *envp[])
{
	void MyAdditionRMB(void);
	int MySubtractionRMB(void);
	void MyMultiplicationRMB(int, int);
	int MyDivisionRMB(int, int);

	int result_sub;
	int a_multi, b_multi;
	int a_divi, b_divi,result_divi;

	MyAdditionRMB();

	result_sub = MySubtractionRMB();
	printf("\n\n");
	printf("Subtraction Yield Result = %d\n", result_sub);

	printf("\n\n");
	printf("Enter Integer Value For 'A' For Multiplication :- ");
	scanf("%d", &a_multi);

	printf("\n\n");
	printf("Enter Integer Value For 'B' For Multiplication :- ");
	scanf("%d", &b_multi);

	MyMultiplicationRMB(a_multi, b_multi);

	printf("\n\n");
	printf("Enter Integer Value For 'A' For Division :- ");
	scanf("%d", &a_divi);

	printf("\n\n");
	printf("Enter Integer Value For 'B' For Division :- ");
	scanf("%d", &b_divi);
	
	result_divi = MyDivisionRMB(a_divi, b_divi);
	printf("\n\n");
	printf("Division of %d And %d Gives = %d (Quotient)\n", a_divi, b_divi, result_divi);
	printf("\n\n");
	return(0);
}

void MyAdditionRMB(void)
{
	int x, y, sum;

	printf("\n\n");
	printf("Enter Integer Value For 'A' For Addition :- ");
	scanf("%d", &x);

	printf("\n\n");
	printf("Enter Integer Value For 'B' For Addition :- ");
	scanf("%d", &y);

	sum = x + y;

	printf("\n\n");
	printf("Sum Of %d And %d = %d\n\n", x, y, sum);

}

int MySubtractionRMB(void)
{
	int x, y, sub;

	printf("\n\n");
	printf("Enter Integer Value For 'A' For Subtraction :- ");
	scanf("%d", &x);

	printf("\n\n");
	printf("Enter Integer Value For 'B' For Subtraction :- ");
	scanf("%d", &y);

	sub = x - y;
	return(sub);

}

void MyMultiplicationRMB(int x, int y)
{
	int Multi;

	Multi = x * y;

	printf("\n\n");
	printf("Multiplication Of %d And %d = %d\n\n", x, y, Multi);

}

int MyDivisionRMB(int x, int y)
{
	int divi_quotient;

	if(x > y)
		divi_quotient = x / y;
	else
		divi_quotient = y / x;
	return(divi_quotient);
}