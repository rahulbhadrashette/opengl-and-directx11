#include<stdio.h>

int main(int argc, char *argv[], char *envp[])
{
	
	int MyAdditionRMB(int, int);

	int x, y, Answer;
	
	printf("\n\n");
	printf("Enter integer Value For 'A' :- ");
	scanf("%d", &x);

	printf("\n\n");
	printf("Enter integer Value For 'B' :- ");
	scanf("%d", &y);

	Answer = MyAdditionRMB(x, y);
	printf("\n\n");
	printf("Sum of %d And %d = %d\n\n", x, y, Answer);
	return(0);
}

int MyAdditionRMB(int x, int y)
{
	int Answer;

	Answer = x + y;
	return(Answer);
}