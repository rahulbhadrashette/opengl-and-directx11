#include<stdio.h>
#include<stdlib.h>

#define TRUE 1
#define FALSE 0
#define SUCCESS 1
#define FAILURE 0
#define DATA_NOT_FOUND 1
#define LIST_EMPTY 2

struct node;
typedef struct node ap_node;
typedef ap_node ap_list;
typedef int bool;
typedef int ap_data;
typedef int ap_ret;
typedef int ap_len;

//internel Layout
struct node
{
	ap_data data;
	struct node *prev;
	struct node *next;
};

//interface function

ap_list *create_list(void);

ap_ret insert_beginning(ap_list *, ap_data);
ap_ret insert_end(ap_list *, ap_data);
ap_ret insert_after_data(ap_list *, ap_data, ap_data);
ap_ret insert_before_data(ap_list *, ap_data, ap_data);

ap_ret delete_beginning(ap_list *);
ap_ret delete_end(ap_list *);
ap_ret delete_data(ap_list *, ap_data);

bool find(ap_list *, ap_data);
bool is_empty(ap_list *);
ap_len len(ap_list *);
void display(ap_list *);

ap_ret examine_beginning(ap_list *, ap_data *);
ap_ret examine_end(ap_list *, ap_data *);
ap_ret examine_and_delete_beginning(ap_list *, ap_data *);
ap_ret examine_and_delete_end(ap_list *, ap_data *);

ap_ret destroy(ap_list **);

//Auxilary Function
static void generic_insert(ap_node *, ap_node *, ap_node *);
static void generic_delete(ap_node *);
static ap_node *search_node(ap_list *, ap_data);
static ap_node *get_node(ap_data);

static void *xcalloc(size_t, size_t);

//Client side
int main(void)
{
	//variable declaration 
	int i;
	ap_data data;
	ap_ret ret;
	ap_list *pList = create_list();

	//code
	for(data = 0;data < 5; data++)
	{
		insert_beginning(pList, data);
	}
	display(pList);
	for(data = 5;data < 10;data++)
	{
		insert_end(pList, data);
	}
	display(pList);

	insert_after_data(pList, 0, 100);
	display(pList);
	insert_before_data(pList, 0, 200);
	display(pList);

	find(pList, 0);
	printf("\nLength(pList) : %d\n", len(pList));

	puts("\nBefore Examine The Routines :- ");
	display(pList);
	data = -1;
	ret = examine_beginning(pList,&data);
	printf("\nBeginning :- %d\n", data);
	display(pList);

	data = -1;
	ret = examine_end(pList, &data);
	printf("\nEnd :- %d\n", data);
	display(pList);

	data = -1;
	ret = examine_and_delete_beginning(pList, &data);
	printf("\nPrev Beginning :- %d\n", data);
	display(pList);

	data = -1;
	ret = examine_and_delete_end(pList, &data);
	printf("\nPrev End :- %d\n", data);
	display(pList);

	ret = destroy(&pList);
	puts("\nList Destroyed\n");


}

//Server side

ap_list *create_list(void)
{
	ap_list *head_node = get_node(0);
	head_node->next = head_node;
	head_node->prev = head_node;
	return(head_node);
}

ap_ret insert_beginning(ap_list *pList, ap_data new_data)
{
	ap_node *new_node = get_node(new_data);
	generic_insert(pList, new_node, pList->next);
	return(SUCCESS);
}

ap_ret insert_end(ap_list *pList, ap_data new_data)
{
	ap_node *new_node = get_node(new_data);
	generic_insert(pList->prev, new_node, pList);
	return(SUCCESS);
}

ap_ret insert_after_data(ap_list *pList, ap_data existing_data, ap_data new_data)
{
	ap_node *existing_node = NULL;

	existing_node = search_node(pList, existing_data);
	if(existing_node == NULL)
	{
		return(DATA_NOT_FOUND);
	}
	ap_node *new_node = get_node(new_data);
	generic_insert(existing_node, new_node, existing_node->next);
	return(SUCCESS);
}
ap_ret insert_before_data(ap_list *pList, ap_data existing_data, ap_data new_data)
{
	ap_node *existing_node = NULL;

	existing_node = search_node(pList, existing_data);
	if(existing_node == NULL)
	{
		return(DATA_NOT_FOUND);
	}
	ap_node *new_node = get_node(new_data);
	generic_insert(existing_node->prev, new_node, existing_node);
	return(SUCCESS);
}
ap_ret delete_beginning(ap_list *pList)
{
	if(is_empty(pList))
	{
		return(LIST_EMPTY);
	}
	generic_delete(pList->next);
	return(SUCCESS);
}

ap_ret delete_end(ap_list *pList)
{
	if(is_empty(pList))
	{
		return(LIST_EMPTY);
	}
	generic_delete(pList->prev);
	return(SUCCESS);
}

ap_ret delete_data(ap_list *pList, ap_data data_to_be_deleted)
{
	ap_node *existing_node = NULL;
	existing_node = search_node(pList, data_to_be_deleted);
	if(existing_node == NULL)
	{
		return(DATA_NOT_FOUND);
	}
	generic_delete(existing_node);
	return(SUCCESS);
}

bool find(ap_list *pList, ap_data find_data)
{
	return(search_node(pList, find_data) != NULL);
}
bool is_empty(ap_list *pList)
{
	return(pList->prev == pList&& pList->next == pList);
}
ap_len len(ap_list *pList)
{
	ap_node *run = NULL;
	ap_len len = 0;
	run = pList->next;
	while(run != pList)
	{
		len = len + 1;
		run = run->next;
	}
	return(len);
}

void display(ap_list *pList)
{
	ap_node *run = NULL;
	printf("[BEGINNING]<->");
	run = pList->next;
	while(run != pList)
	{
		printf("[%d]<->",run->data);
		run =run->next;
	}
	printf("[END]\n");
}

ap_ret examine_beginning(ap_list *pList, ap_data *data_to_be_examined)
{
	if(is_empty(pList))
	{
		return(LIST_EMPTY);
	}
	*data_to_be_examined = pList->next->data;
	return(SUCCESS);
}

ap_ret examine_end(ap_list *pList, ap_data *data_to_be_examined)
{
	if(is_empty(pList))
	{
		return(LIST_EMPTY);
	}
	*data_to_be_examined = pList->prev->data;
	return(SUCCESS);
}

ap_ret examine_and_delete_beginning(ap_list *pList, ap_data *data_to_be_examined)
{
	if(is_empty(pList))
	{
		return(LIST_EMPTY);
	}
	*data_to_be_examined = pList->next->data;
	generic_delete(pList->next);
	return(SUCCESS);
}

ap_ret examine_and_delete_end(ap_list *pList, ap_data *data_to_be_examined)
{
	if(is_empty(pList))
	{
		return(LIST_EMPTY);
	}
	*data_to_be_examined = pList->prev->data;
	generic_delete(pList->prev);
	return(SUCCESS);
}

ap_ret destroy(ap_list **pList)
{
	ap_node *head_node = NULL;
	ap_node *run = NULL;
	ap_node *run_next = NULL;

	head_node = *pList;
	run = head_node->next;

	while(run != head_node)
	{
		run_next = run->next;
		free(run);
		run = run_next;
	}
	free(head_node);
	*pList = NULL;
	return(SUCCESS);
}

//Auxilary Function
static void generic_insert(ap_node *pBeg, ap_node *pMid, ap_node *pEnd)
{
	pMid->next = pEnd;
	pMid->prev = pBeg;
	pBeg->next = pMid;
	pEnd->prev = pMid;
}

static void generic_delete(ap_node *pNode)
{
	pNode->next->prev = pNode->prev;
	pNode->prev->next = pNode->next;
	free(pNode);
	pNode = NULL;
}

static ap_node *search_node(ap_list *pList, ap_data data_to_be_searched)
{
	ap_node *run = NULL;
	run = pList->next;
	while(run != pList)
	{
		if(run->data == data_to_be_searched)
		{
			return(run);
		}
		run = run->next;
	}
	return(NULL);
}

static ap_node *get_node(ap_data data)
{
	ap_node *new_node = (ap_node *)xcalloc(1, sizeof(ap_node));
	new_node->data = data;
	return(new_node);
}

static void *xcalloc(size_t units, size_t size_per_units)
{
	void *p = calloc(units, size_per_units);
	if(p == NULL)
	{
		printf("Memory Allocation Is Failed...!!!\n");
		exit(0);
	}
	return(p);
}

