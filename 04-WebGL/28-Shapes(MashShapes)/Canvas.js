//global variable
var canvas = null;
var gl=null; //For WebGL Context
var bFullScreen=false;
var canvas_orignal_width;
var canvas_orignal_height;

const WebGLMacros=//when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE:3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao;
var vbo;
var vbo_Color;

var mvpUniform;

//To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

//To stop animation : To have cancleAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;;

//onload function
function main()
{
	//get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed...!!!\n");
	else
		console.log("Obtaining Canvas Succeeded...!!!\n");

	canvas_orignal_width = canvas.width;
	canvas_orignal_height = canvas.height;
	//print canvas width and height on console
	console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//initialize WebGL
	init();

	//start drawing here as warning-up
	resize();
	display();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenelement ||
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;

	// if not fullScreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
			bFullScreen = false;
	}
}

function init()
{
	//code
	//get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if(gl==null) //failed to get context
	{
		console.log("Failed to get the rendering context for WebGL...!!!");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	var vertexShaderSourceCode=
			"#version 300 es" +
			"\n" +
            "in vec4 vPosition;" +
            "in vec4 vColor;" +
            "uniform mat4 u_mvp_matrix;" +
            "out vec4 out_Color;" +
            "void main(void)" +
            "{" +
                "gl_Position = u_mvp_matrix * vPosition;" +
                "gl_PointSize = 5.0;" +
                "out_Color = vColor;" +
            "}"


		vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
		gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
		gl.compileShader(vertexShaderObject);
		if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false);
		{
			var error=gl.getShaderInfoLog(vertexShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

	 //Write Fragment Shader Code
    var fragmentShaderSourceCode =
    		"#version 300 es" +
			"\n" +
			"precision highp float;" +
            "in vec4 out_Color;" +
            "out vec4 fragColor;" +
            "void main(void)" +
            "{" +
               "fragColor = out_Color;" +
            "}"

		fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
		gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		gl.compileShader(fragmentShaderObject);
		if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false);
		{
			var error=gl.getShaderInfoLog(fragmentShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}


		//shader program
		shaderProgramObject=gl.createProgram();
		gl.attachShader(shaderProgramObject, vertexShaderObject);
		gl.attachShader(shaderProgramObject, fragmentShaderObject);

		//pre-link binding of shader program object with vertex shader attribute
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");


		//linking
		gl.linkProgram(shaderProgramObject);
		if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
		{
			var error = gl.getProgramInfoLog(shaderProgramObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

	var graphVertices = new Float32Array
    							([
									//First
									//right
									0.5, 0.8, 0.0,
									0.8, 0.8, 0.0,
									//top
									0.8, 0.8, 0.0,
									0.8, 0.3, 0.0,
									//left
									0.5, 0.3, 0.0,
									0.8, 0.3, 0.0,
									//botttom
									0.5, 0.466, 0.0,
									0.8, 0.466, 0.0,

									0.5, 0.632, 0.0,
									0.8, 0.632, 0.0,

									0.5, 0.3, 0.0,
									0.5, 0.8, 0.0,

									0.6, 0.3, 0.0,
									0.6, 0.8, 0.0,

									0.7, 0.3, 0.0,
									0.7, 0.8, 0.0,

									//Second
									//top
									-0.15, 0.8, 0.0,
									0.15, 0.8, 0.0,
									//left
									-0.15, 0.3, 0.0,
									-0.15, 0.8, 0.0,
									//horizental
									-0.15, 0.466, 0.0,
									0.15, 0.466, 0.0,
									-0.15, 0.632, 0.0,
									0.15, 0.632, 0.0,
									//vertical 
									-0.05, 0.3, 0.0,
									-0.05, 0.8, 0.0,
									0.05, 0.3, 0.0,
									0.05, 0.8, 0.0,
									//1
									-0.15, 0.632, 0.0,
									-0.05, 0.8, 0.0,
									//2
									-0.15, 0.466, 0.0,
									0.05, 0.8, 0.0,
									//3
									0.15, 0.8, 0.0,
									-0.15, 0.3, 0.0,
									//4
									0.15, 0.632, 0.0,
									-0.05, 0.3, 0.0,
									//5
									0.15, 0.466, 0.0,
									0.05, 0.3, 0.0,

									//Third
									-0.5, 0.8, 0.0,
									-0.8, 0.8, 0.0,

									-0.8, 0.3, 0.0,
									-0.5, 0.3, 0.0,

									-0.5, 0.466, 0.0,
									-0.8, 0.466, 0.0,

									-0.5, 0.632, 0.0,
									-0.8, 0.632, 0.0,

									-0.6, 0.3, 0.0,
									-0.6, 0.8, 0.0,

									-0.7, 0.3, 0.0,
									-0.7, 0.8, 0.0,

									-0.6, 0.632, 0.0,
									-0.7, 0.632, 0.0,

									-0.7, 0.466, 0.0,
									-0.6, 0.466, 0.0,

									//Forth
									//right 
									-0.5, -0.3, 0.0,
									-0.5, -0.8, 0.0,
									//top
									-0.5, -0.3, 0.0,
									-0.8, -0.3, 0.0,
									//lett
									-0.8, -0.8, 0.0,
									-0.8, -0.3, 0.0,
									//bottom
									-0.5, -0.8, 0.0,
									-0.8, -0.8, 0.0,
									//vertical line
									-0.5, -0.466, 0.0,
									-0.8, -0.466, 0.0,
									-0.5, -0.632, 0.0,
									-0.8, -0.632, 0.0,
									//horizental Line
									-0.6, -0.3, 0.0,
									-0.6, -0.8, 0.0,
									-0.7, -0.3, 0.0,
									-0.7, -0.8, 0.0,
									//Cross Lines
									//1
									-0.8, -0.466, 0.0,
									-0.7, -0.3, 0.0,
									//2
									-0.8, -0.632, 0.0,
									-0.6, -0.3, 0.0,
									//3
									-0.5, -0.3, 0.0,
									-0.8, -0.8, 0.0,
									//4
									-0.5, -0.466, 0.0,
									-0.7, -0.8, 0.0,
									//5
									-0.6, -0.8, 0.0,
									-0.5, -0.632, 0.0,

									//Five
									//right
									0.15, -0.8, 0.0,
									0.15, -0.3, 0.0,
									//top
									0.15, -0.3, 0.0,
									-0.15, -0.3, 0.0,
									//left
									-0.15, -0.3, 0.0,
									-0.15, -0.8, 0.0,
									//botttom
									-0.15, -0.8, 0.0,
									0.15, -0.8, 0.0,
									//1
									-0.15, -0.3, 0.0,
									0.15, -0.466, 0.0,
									//2
									-0.15, -0.3, 0.0,
									0.15, -0.632, 0.0,
									//3
									-0.15, -0.3, 0.0,
									0.15, -0.8, 0.0,
									//4
									-0.15, -0.3, 0.0,
									-0.05, -0.8, 0.0,
									//5
									-0.15, -0.3, 0.0,
									0.05, -0.8, 0.0,

									//For Triangles
									0.6, -0.8, 0.0,
									0.6, -0.3, 0.0,
									0.5, -0.3, 0.0,

									0.5, -0.3, 0.0,
									0.5, -0.8, 0.0,
									0.6, -0.8, 0.0,

									0.7, -0.8, 0.0,
									0.7, -0.3, 0.0,
									0.6, -0.3, 0.0,

									0.6, -0.3, 0.0,
									0.6, -0.8, 0.0,
									0.7, -0.8, 0.0,

									0.8, -0.8, 0.0,
									0.8, -0.3, 0.0,
									0.7, -0.3, 0.0,

									0.7, -0.3, 0.0,
									0.7, -0.8, 0.0,
									0.8, -0.8, 0.0,

									//Six
									//right
									0.5, -0.8, 0.0,
									0.8, -0.8, 0.0,
									//top
									0.8, -0.8, 0.0,
									0.8, -0.3, 0.0,
									//left
									0.5, -0.3, 0.0,
									0.8, -0.3, 0.0,
									//bottom
									0.5, -0.3, 0.0,
									0.5, -0.8, 0.0,
									//vertical Line
									//1
									0.6, -0.3, 0.0,
									0.6, -0.8, 0.0,
									//2
									0.7, -0.3, 0.0,
									0.7, -0.8, 0.0,
									//horixental line
									//1
									0.5, -0.466, 0.0,
									0.8, -0.466, 0.0,
									//2
									0.5, -0.632, 0.0,
									0.8, -0.632, 0.0,
    							]);

    var Color = 132;
    var graphColor = new Float32Array(3*Color);
    for (var i = 0; i < Color; i++)
	{
		if (i < 98)
		{
			graphColor[3 * i + 0] = 1.0;
			graphColor[3 * i + 1] = 1.0;
			graphColor[3 * i + 2] = 1.0;
		}
		else 
		{
			if (i < 104)
			{
				graphColor[3 * i + 0] = 1.0;
				graphColor[3 * i + 1] = 0.0;
				graphColor[3 * i + 2] = 0.0;
			}
			else if (i < 110)
			{
				graphColor[3 * i + 0] = 0.0;
				graphColor[3 * i + 1] = 1.0;
				graphColor[3 * i + 2] = 0.0;
			}
			else if (i < 116)
			{
				graphColor[3 * i + 0] = 0.0;
				graphColor[3 * i + 1] = 0.0;
				graphColor[3 * i + 2] = 1.0;
			}
			else 
			{
				graphColor[3 * i + 0] = 1.0;
				graphColor[3 * i + 1] = 1.0;
				graphColor[3 * i + 2] = 1.0;
			}
		}
	}

   
    vao = gl.createVertexArray();
    gl.bindVertexArray(vao);

    vbo = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
    gl.bufferData(gl.ARRAY_BUFFER, graphVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our graphVertices array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_Color = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Color);
    gl.bufferData(gl.ARRAY_BUFFER, graphColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3,//3 is for X, Y, Z co-ordinates in our graphColor array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

	//set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);  //blue
}

function resize()
{
	//code
	if(bFullScreen == true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_orignal_width;
		canvas.height=canvas_orignal_height;
	}

	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);
}

function display()
{
	//code
	gl.clear(gl.COLOR_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	//For Triangle
	var modelViewProjectionMatrix=mat4.create();

	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao);
	//Draw the Necessary Scnes
	//Start Right Top First
	gl.drawArrays(gl.LINES, 0, 16);
	//Second
	gl.drawArrays(gl.LINES, 16, 22);
	//Third
	gl.drawArrays(gl.POINTS, 38, 16);
	//Forth
	gl.drawArrays(gl.LINES, 54, 26);
	//Five
	gl.drawArrays(gl.LINES, 80, 18);
	//Six
	gl.drawArrays(gl.TRIANGLES, 98, 18);
	gl.drawArrays(gl.LINES, 116, 16);

	gl.bindVertexArray(null);

	gl.useProgram(null);

	//animation loop
	requestAnimationFrame(display, canvas);
}

function uninitialize()
{
	//code

	if(vbo_Color)
	{
		gl.deleteBuffer(vbo_Color);
		vbo_Color=null;
	}

	if(vbo)
	{
		gl.deleteBuffer(vbo);
		vbo=null;
	}

	if(vao)
	{
		gl.deleteVertexArray(vao);
		vao=null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:  // Escape
				//uninitialize
				uninitialize();
				//close our application's tab
				window.close();  //may not work in firefox but works in safari and chrome
				break;

		case 70: //for 'F' or 'f'
		toggleFullScreen();
		break;
	}
}

function mouseDown()
{
	//code
}
