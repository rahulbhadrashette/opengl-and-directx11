// global variables
var canvas=null;
var gl=null;
var bFullscreen = false;

var canvas_original_width;
var canvas_original_height;

const WebGLMacros = {
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject_PV;
var shaderProgramObject_PF;


//For Vertex shader Variables
var model_matrix_uniform;
var view_matrix_uniform;
var projection_matrix_uniform;

var La_uniform;
var Ld_uniform;
var Ls_uniform;
var light_position_uniform;

var Ka_uniform;
var Kd_uniform;
var Ks_uniform;
var material_shininess_uniform;
var light_enable_uniform;

//For fragment Shader Variables
var model_matrix_uniform_RMB;
var view_matrix_uniform_RMB;
var projection_matrix_uniform_RMB;

var La_uniform_RMB;
var Ld_uniform_RMB;
var Ls_uniform_RMB;
var light_position_uniform_RMB;

var Ka_uniform_RMB;
var Kd_uniform_RMB;
var Ks_uniform_RMB;
var material_shininess_uniform_RMB;

var light_enable_uniform_RMB;

var blKeyPressed = false ; // for lights
var fragmentShaderladder = false;

var perspectiveProjectionMatrix;

var light_ambient=[0.0,0.0,1.0];
var light_diffuse=[1.0,1.0,1.0];
var light_specular=[1.0,1.0,1.0];
var light_position=[100.0,100.0,100.0,1.0];

var material_ambient= [0.0,0.0,0.0];
var material_diffuse= [0.5,0.2,0.7];
var material_specular= [1.0,1.0,1.0];
var material_shininess= 50.0;

//to start animation
var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame;


function main() 
{
	canvas = document.getElementById('AMC')
	if(!canvas) {
		console.log("Obtaining canvas failed.\n");
	} else {
		console.log("Obtaining canvas succeeded.\n");
	}

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	//print canvas height and width
	console.log('Canvas width : '+canvas.width +' And canvas height : ' + canvas.height);

	window.addEventListener('keydown', keydown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    init();

    resize();
    draw();
}

function compileShader(shader, shaderType) {
	var shaderObject =  gl.createShader(shaderType);
	gl.shaderSource(shaderObject, shader);
	gl.compileShader(shaderObject);
	if(gl.getShaderParameter(shaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(shaderObject);
		if(error.length >0) {
			console.log(error);
			uninitialize();
		}
	}
	return shaderObject;
}

function init() 
{
	gl = canvas.getContext('webgl2');
	if(gl==null) {
		console.log("Failed to get rendering context..\n");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;


	vertexShader();
	fragmentShader() 

	sphere=new Mesh();
    makeSphere(sphere,2.0,30,30);

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.enable(gl.CULL_FACE);

	gl.clearColor(0.0,0.0,0.0, 1.0);

	perspectiveProjectionMatrix = mat4.create();

}

function resize() 
{
	if(bFullscreen==true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	} else {
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	console.log("In resize	 : "+ canvas.width);
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat(canvas.width)/parseFloat(canvas.height)), 0.1, 100.0);
}

function draw() 
{
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	if(fragmentShaderladder == true)
	{
		gl.useProgram(shaderProgramObject_PF);

		if(blKeyPressed)
	    {
	        gl.uniform1i(light_enable_uniform_RMB, 1);

	        gl.uniform3fv(La_uniform_RMB, light_ambient);
	        gl.uniform3fv(Ld_uniform_RMB, light_diffuse);
	        gl.uniform3fv(Ls_uniform_RMB, light_specular);
	        gl.uniform4fv(light_position_uniform_RMB, light_position);

	        gl.uniform3fv(Ka_uniform_RMB, material_ambient);
	        gl.uniform3fv(Kd_uniform_RMB, material_diffuse);
	        gl.uniform3fv(Ks_uniform_RMB, material_specular);
	        gl.uniform1f(material_shininess_uniform_RMB, material_shininess);
	    }
	    else
	    {
	        gl.uniform1i(light_enable_uniform_RMB, 0);
	    }
	}
	else
	{
		gl.useProgram(shaderProgramObject_PV)

		if(blKeyPressed)
	    {
	        gl.uniform1i(light_enable_uniform, 1);

	        gl.uniform3fv(La_uniform, light_ambient);
	        gl.uniform3fv(Ld_uniform, light_diffuse);
	        gl.uniform3fv(Ls_uniform, light_specular);
	        gl.uniform4fv(light_position_uniform, light_position);

	        gl.uniform3fv(Ka_uniform, material_ambient);
	        gl.uniform3fv(Kd_uniform, material_diffuse);
	        gl.uniform3fv(Ks_uniform, material_specular);
	        gl.uniform1f(material_shininess_uniform, material_shininess);
	    }
	    else
	    {
	        gl.uniform1i(light_enable_uniform, 0);
	    }
	}
	

    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);

	gl.uniformMatrix4fv(model_matrix_uniform, false, modelMatrix);
	gl.uniformMatrix4fv(view_matrix_uniform, false, viewMatrix);
	gl.uniformMatrix4fv(projection_matrix_uniform, false, perspectiveProjectionMatrix);

	gl.uniformMatrix4fv(model_matrix_uniform_RMB, false, modelMatrix);
	gl.uniformMatrix4fv(view_matrix_uniform_RMB, false, viewMatrix);
	gl.uniformMatrix4fv(projection_matrix_uniform_RMB, false, perspectiveProjectionMatrix);

	sphere.draw();

	gl.useProgram(null);
	requestAnimationFrame (draw, canvas);

}

function uninitialize() 
{

	if(sphere)
    {
        sphere.deallocate();
        sphere=null;
    }

	if(shaderProgramObject_PF) {
		if(vertexShaderObject) {
			gl.detachShader(shaderProgramObject_PF, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		if(fragmentShaderObject) {
			gl.detachShader(shaderProgramObject_PF, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
	}
	gl.deleteProgram(shaderProgramObject_PF);
	shaderProgramObject_PF = null;

	if(shaderProgramObject_PV) {
		if(vertexShaderObject) {
			gl.detachShader(shaderProgramObject_PV, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		if(fragmentShaderObject) {
			gl.detachShader(shaderProgramObject_PV, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
	}

	gl.deleteProgram(shaderProgramObject_PV);
	shaderProgramObject_PV = null;
}

function keydown(event) 
{
	switch(event.keyCode)
	{
		case 27:  // Escape
				toggleFullScreen();
				break;

		case 69:
				//uninitialize
				uninitialize();
				//close our application's tab
				window.close();  //may not work in firefox but works in safari and chrome
				break;

		case 76:  //for 'l' Or 'L'--> Light
				if(blKeyPressed == false)
				{
					blKeyPressed = true;
				}
				else
				{
					blKeyPressed = false;
				}
				break;

		case 70:
				fragmentShaderladder = true;
				break;

		case 86:
				fragmentShaderladder = false;
				break;
	}
}

function mouseDown() {
	//alert('mouse is clicked.');
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenelement ||
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;

	// if not fullScreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
			bFullScreen = false;
	}
}

function vertexShader()
{
	// code
		var vertexShaderSourceCode = 
			"#version 300 es" +
            "\n" +
            "in vec4 vPosition;" +
            "in vec3 vNormal;" +
            "uniform mat4 u_model_matrix;" +
            "uniform mat4 u_view_matrix;" +
            "uniform mat4 u_projection_matrix;" +
            "uniform int u_lighting_enabled;" +
            "uniform vec3 u_La;" +
            "uniform vec3 u_Ld;" +
            "uniform vec3 u_Ls;" +
            "uniform vec4 u_light_position;" +
            "uniform vec3 u_Ka;" +
            "uniform vec3 u_Kd;" +
            "uniform vec3 u_Ks;" +
            "uniform float u_material_shininess;" +
            "out vec3 phong_ads_color;" +
            "void main(void)" +
            "{" +
	            "if(u_lighting_enabled == 1)" +
	            "{" +
		            "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
		            "vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" +
		            "vec3 light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz);" +
		            "float tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);" +
		            "vec3 ambient = u_La * u_Ka;" +
		            "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" +
		            "vec3 reflection_vector = reflect(-light_direction, transformed_normals);" +
		            "vec3 viewer_vector = normalize(-eye_coordinates.xyz);" +
		            "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);" +
		            "phong_ads_color = ambient + diffuse + specular;" +
	            "}" +
	            "else" +
	            "{" +
	            	"phong_ads_color = vec3(1.0, 1.0, 1.0);" +
	            "}" +
            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
            "}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false);
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	var fragmentShaderSourceCode = 
			"#version 300 es" +
            "\n" +
            "precision highp float;" +
            "in vec3 phong_ads_color;" +
            "out vec4 FragColor;" +
            "void main(void)" +
            "{" +
            "	FragColor = vec4(phong_ads_color, 1.0);" +
            "}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false);
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject_PV = gl.createProgram();

	gl.attachShader(shaderProgramObject_PV, vertexShaderObject);
	gl.attachShader(shaderProgramObject_PV, fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject_PV, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_PV, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");

		//linking
	gl.linkProgram(shaderProgramObject_PV);
	if(!gl.getProgramParameter(shaderProgramObject_PV, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_PV);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	model_matrix_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_model_matrix");
    view_matrix_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_view_matrix");
    projection_matrix_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_projection_matrix");
    light_enable_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_lighting_enabled");
    La_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_La");
    Ld_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_Ld");
    Ls_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_Ls");
    light_position_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_light_position");

    Ka_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_Ka");
    Kd_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_Kd");
    Ks_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_Ks");
    material_shininess_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_material_shininess");
}

function fragmentShader() 
{
	// code
	var vertexShaderSourceCode = 
		"#version 300 es" +
        "\n" +
		"in vec4 vertexPosition_RMB;" +
		"in vec3 vertexNormal_RMB;" +

		"uniform mat4 u_modelMatrix_RMB;" +
		"uniform mat4 u_viewMatrix_RMB;" +
		"uniform mat4 u_projection_matrix_RMB;" +

		"uniform mediump int u_lKeyIsPressed_RMB;" +
		"uniform vec4 u_Light_Position_RMB;" +

		"out vec3 tnorm_RMB;" +
		"out vec3 lightdirection_RMB;" +
		"out vec3 viewerVector_RMB;" +

		"void main(void)" +
		"{" +
			"if(u_lKeyIsPressed_RMB == 1)" +
			"{" +
				"vec4 eye_coordinate_RMB = u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" +

				"tnorm_RMB = mat3(u_viewMatrix_RMB * u_modelMatrix_RMB) * vertexNormal_RMB;" +

				"lightdirection_RMB = vec3(u_Light_Position_RMB - eye_coordinate_RMB);" +

				"viewerVector_RMB = vec3(-eye_coordinate_RMB.xyz);" +

			"}" +

		"gl_Position = u_projection_matrix_RMB * u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" +
		"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false);
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	var fragmentShaderSourceCode = 
		"#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec3 tnorm_RMB;" +
		"in vec3 lightdirection_RMB;" +
		"in vec3 viewerVector_RMB;" +

		"uniform vec3 u_La_RMB;" +
		"uniform vec3 u_Ld_RMB;" +
		"uniform vec3 u_Ls_RMB;" +

		"uniform vec3 u_Ka_RMB;" +
		"uniform vec3 u_Kd_RMB;" +
		"uniform vec3 u_Ks_RMB;" +

		"uniform float u_materialShininess_RMB;" +
		"uniform mediump int u_lKeyIsPressed_RMB;" +

		"out vec4 fragColor;" +

		"void main(void)" +
		"{" +
			"vec3 phong_AdsLight_RMB;" +
			"if(u_lKeyIsPressed_RMB == 1)" +
			"{" +

				"vec3 normalize_tnorm_RMB = normalize(tnorm_RMB);" +

				"vec3 normalize_lightdirection_RMB = normalize(lightdirection_RMB);" +

				"vec3 normalize_viewerVector_RMB = normalize(viewerVector_RMB);" +

				"vec3 reflectionVector_RMB = reflect(-normalize_lightdirection_RMB, normalize_tnorm_RMB);" +

				"float tn_dot_lightDir_RMB = max(dot(normalize_lightdirection_RMB, normalize_tnorm_RMB), 0.0);" +

				"vec3 ambient_RMB = u_La_RMB * u_Ka_RMB;" +

				"vec3 diffuse_RMB = u_Ld_RMB * u_Kd_RMB * tn_dot_lightDir_RMB;" +

				"vec3 specular_RMB = u_Ls_RMB * u_Ks_RMB * pow(max(dot(reflectionVector_RMB, normalize_viewerVector_RMB), 0.0), u_materialShininess_RMB);" +

				"phong_AdsLight_RMB = ambient_RMB + diffuse_RMB + specular_RMB;" +

			"}" +
			"else" +
			"{" +

				"phong_AdsLight_RMB = vec3(1.0, 1.0, 1.0);" +

			"}" +

			"fragColor = vec4(phong_AdsLight_RMB, 1.0);" +
		"}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false);
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject_PF = gl.createProgram();

	gl.attachShader(shaderProgramObject_PF, vertexShaderObject);
	gl.attachShader(shaderProgramObject_PF, fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject_PF, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vertexPosition_RMB");
	gl.bindAttribLocation(shaderProgramObject_PF, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vertexNormal_RMB");

		//linking
	gl.linkProgram(shaderProgramObject_PF);
	if(!gl.getProgramParameter(shaderProgramObject_PF, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_PF);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	model_matrix_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_modelMatrix_RMB");
    view_matrix_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_viewMatrix_RMB");
    projection_matrix_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_projection_matrix_RMB");
    light_enable_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_lKeyIsPressed_RMB");
    La_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_La_RMB");
    Ld_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Ld_RMB");
    Ls_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Ls_RMB");
    light_position_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Light_Position_RMB");

    Ka_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Ka_RMB");
    Kd_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Kd_RMB");
    Ks_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Ks_RMB");
    material_shininess_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_materialShininess_RMB");

}