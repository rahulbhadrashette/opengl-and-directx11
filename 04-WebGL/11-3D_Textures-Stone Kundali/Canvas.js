//global variable
var canvas = null;
var gl=null; //For WebGL Context
var bFullScreen=false;
var canvas_orignal_width;
var canvas_orignal_height;

const WebGLMacros=//when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_Pyramid;
var vbo_Position_Pyramid;
var vbo_Texture_Pyramid;

var vao_Cube;
var vbo_Position_Cube;
var vbo_Texture_Cube;

var anglePyramid = 0.0;
var angleCube = 0.0;

var pyramidTexture = 0;
var cubeTexture = 0;

var uniformTextureSampler;

var mvpUniform;
var perspectiveProjectionMatrix;

//To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

//To stop animation : To have cancleAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;;

//onload function
function main()
{
	//get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed...!!!\n");
	else
		console.log("Obtaining Canvas Succeeded...!!!\n");

	canvas_orignal_width = canvas.width;
	canvas_orignal_height = canvas.height;
	//print canvas width and height on console
	console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//initialize WebGL
	init();

	//start drawing here as warning-up
	resize();
	display();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenelement ||
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;

	// if not fullScreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
			bFullScreen = false;
	}
}

function init()
{
	//code
	//get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if(gl==null) //failed to get context
	{
		console.log("Failed to get the rendering context for WebGL...!!!");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	var vertexShaderSourceCode=
			"#version 300 es" +
			"\n" +
            "in vec4 vPosition;" +
            "in vec2 vTexCoord;" +
            "out vec2 out_TexCoord;" +
            "uniform mat4 u_mvp_matrix;" +
            "void main(void)" +
            "{" +
                "gl_Position = u_mvp_matrix * vPosition;" +
                "out_TexCoord = vTexCoord;" +
            "}";


		vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
		gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
		gl.compileShader(vertexShaderObject);
		if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false);
		{
			var error=gl.getShaderInfoLog(vertexShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

	 //Write Fragment Shader Code
    var fragmentShaderSourceCode =
    		"#version 300 es" +
			"\n" +
			"precision highp float;" +
            "in vec2 out_TexCoord;" +
            "uniform highp sampler2D u_TextureSampler;" +
            "out vec4 fragColor;" +
            "void main(void)" +
            "{" +
               "fragColor = texture(u_TextureSampler, out_TexCoord);" +
            "}";

		fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
		gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		gl.compileShader(fragmentShaderObject);
		if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false);
		{
			var error=gl.getShaderInfoLog(fragmentShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}


		//shader program
		shaderProgramObject=gl.createProgram();
		gl.attachShader(shaderProgramObject, vertexShaderObject);
		gl.attachShader(shaderProgramObject, fragmentShaderObject);

		//pre-link binding of shader program object with vertex shader attribute
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexCoord");


		//linking
		gl.linkProgram(shaderProgramObject);
		if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
		{
			var error = gl.getProgramInfoLog(shaderProgramObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}


	//Load Pyramid Texture
	pyramidTexture = gl.createTexture();
	pyramidTexture.image = new Image();
	pyramidTexture.image.src="stone.png";
	pyramidTexture.image.onload = function()
	{
		gl.bindTexture(gl.TEXTURE_2D, pyramidTexture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, pyramidTexture.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}

	cubeTexture = gl.createTexture();
	cubeTexture.image = new Image();
	cubeTexture.image.src="Vijay_Kundali.png";
	cubeTexture.image.onload = function()
	{
		gl.bindTexture(gl.TEXTURE_2D, cubeTexture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, cubeTexture.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}


	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	uniformTextureSampler = gl.getUniformLocation(shaderProgramObject, "u_TextureSampler");

	var pyramidVertices = new Float32Array
    	([
    		//FRONT FACE
			0.0, 1.0, 0.0,
			-1.0, -1.0, 1.0,
			1.0, -1.0, 1.0,
			//LEFT FACE
			0.0, 1.0, 0.0,
			1.0, -1.0, 1.0,
			1.0, -1.0, -1.0,
			//BACK FACE
			0.0, 1.0, 0.0,
			1.0, -1.0, -1.0,
			-1.0, -1.0, -1.0,
			//RIGHT FACE
			0.0, 1.0, 0.0,
			-1.0, -1.0, -1.0,
			-1.0, -1.0, 1.0
    	]);

    var pyramidTexCoord = new Float32Array
    	([
    		0.5, 1.0,
    		0.0, 0.0,
    		1.0, 0.0,

    		0.5, 1.0,
    		1.0, 0.0,
    		0.0, 0.0,

    		0.5, 1.0,
    		1.0, 0.0,
    		0.0, 0.0,

    		0.5, 1.0,
    		0.0, 0.0,
    		1.0, 0.0
    	]);

    var cubeVertices = new Float32Array
    	([
    		//TOP FACE
			1.0, 1.0, -1.0,
			-1.0, 1.0, -1.0,
			-1.0, 1.0, 1.0,
			1.0, 1.0, 1.0,
			//BOTTOM FACE
			1.0, -1.0, -1.0,
			-1.0, -1.0, -1.0,
			-1.0, -1.0, 1.0,
			1.0, -1.0, 1.0,
			//FRONT FACE
			1.0, 1.0, 1.0,
			-1.0, 1.0, 1.0,
			-1.0, -1.0, 1.0,
			1.0, -1.0, 1.0,
			//BACK FACE
			1.0, 1.0, -1.0,
			-1.0, 1.0, -1.0,
			-1.0, -1.0, -1.0,
			1.0, -1.0, -1.0,
			//RIGHT FACE
			1.0, 1.0, -1.0,
			1.0, 1.0, 1.0,
			1.0, -1.0, 1.0,
			1.0, -1.0, -1.0,
			//LEFT FACE
			-1.0, 1.0, -1.0,
			-1.0, 1.0, 1.0,
			-1.0, -1.0, 1.0,
			-1.0, -1.0, -1.0
    	]);

    	//If above -1.0 Or +1.0 Values Make Cube Much Larger Than Pyramid, 
    	//than follow the code in following loop which will convert all 1s and -1s to -0.75 Or +0.75
    	/*for(var i=0; i<72; i++)
    	{
    		if(cubeVertices[i]<0.0)
    		{
    			cubeVertices[i]=cubeVertices[i]+0.25;
    		}
    		else if(cubeVertices[i]>0.0)
    		{
    			cubeVertices[i]=cubeVertices[i]-0.25;
    		}
    		else
    		{
    			cubeVertices[i]=cubeVertices[i]; //no change
    		}
    	}*/

    var cubeTexCoord = new Float32Array
    	([
    		0.0, 0.0,
    		1.0, 0.0,
    		1.0, 1.0,
    		0.0, 1.0,

    		0.0, 0.0,
    		1.0, 0.0,
    		1.0, 1.0,
    		0.0, 1.0,

    		0.0, 0.0,
    		1.0, 0.0,
    		1.0, 1.0,
    		0.0, 1.0,

    		0.0, 0.0,
    		1.0, 0.0,
    		1.0, 1.0,
    		0.0, 1.0,

    		0.0, 0.0,
    		1.0, 0.0,
    		1.0, 1.0,
    		0.0, 1.0,

    		0.0, 0.0,
    		1.0, 0.0,
    		1.0, 1.0,
    		0.0, 1.0
    	]);

    vao_Pyramid = gl.createVertexArray();
    gl.bindVertexArray(vao_Pyramid);

    vbo_Position_Pyramid = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Position_Pyramid);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our pyramidVertices array
    															gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_Texture_Pyramid = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Texture_Pyramid);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidTexCoord, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, //2 is for S and T co-ordinates in our cubeTexCoord array
    															gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);


    vao_Cube = gl.createVertexArray();
    gl.bindVertexArray(vao_Cube);

    vbo_Position_Cube = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Position_Cube);
    gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our cubeVertices array
    															 gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_Texture_Cube = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Texture_Cube);
    gl.bufferData(gl.ARRAY_BUFFER, cubeTexCoord, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2,//2 is for S and T co-ordinates in our cubeTexCoord array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

	//set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);  //blue

	//Depth test will always be enabled
	gl.enable(gl.DEPTH_TEST);

	////We will always cill back faces for better performance
	//gl.enable(gl.CULL_FACE);

	//initialize projection matrix
	perspectiveProjectionMatrix=mat4.create();

}

function resize()
{
	//code
	if(bFullScreen == true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_orignal_width;
		canvas.height=canvas_orignal_height;
	}

	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	//Perspective Graphic Projection => left, rigth, bottom, top
	
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function display()
{
	//code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	//For Triangle
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [-1.6, 0.0, -5.0])
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(anglePyramid));

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindTexture(gl.TEXTURE_2D, pyramidTexture);
	gl.uniform1i(uniformTextureSampler, 0);

	gl.bindVertexArray(vao_Pyramid);
	gl.drawArrays(gl.TRIANGLES, 0, 12);
	gl.bindVertexArray(null);

	//For Rectangle
	mat4.identity(modelViewMatrix); //reset to identity matrix
	mat4.identity(modelViewProjectionMatrix); //reset to identity matrix

	mat4.translate(modelViewMatrix, modelViewMatrix, [1.6, 0.0, -5.0])
	mat4.scale(modelViewMatrix, modelViewMatrix, [0.75, 0.75, 0.75]);
	mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRad(angleCube));
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleCube));
	mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angleCube));

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindTexture(gl.TEXTURE_2D, cubeTexture);
	gl.uniform1i(uniformTextureSampler, 0);
	
	gl.bindVertexArray(vao_Cube);

	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);

	gl.bindVertexArray(null);

	gl.useProgram(null);

	anglePyramid = anglePyramid+2.0;
	if(anglePyramid>=360.0)
	{
		anglePyramid = anglePyramid - 360.0;
	}

	angleCube = angleCube+1.0;
	if(angleCube>=360.0)
	{
		angleCube = angleCube - 360.0;
	}

	//animation loop
	requestAnimationFrame(display, canvas);

}

function uninitialize()
{
	//code

	if(vbo_Texture_Cube)
	{
		gl.deleteVertexArray(vbo_Texture_Cube);
		vbo_Texture_Cube=null;
	}

	if(vbo_Position_Cube)
	{
		gl.deleteVertexArray(vbo_Position_Cube);
		vbo_Position_Cube=null;
	}

	if(vao_Cube)
	{
		gl.deleteVertexArray(vao_Cube);
		vao_Cube=null;
	}

	if(vbo_Texture_Pyramid)
	{
		gl.deleteVertexArray(vbo_Texture_Pyramid);
		vbo_Texture_Pyramid=null;
	}

	if(vbo_Position_Pyramid)
	{
		gl.deleteVertexArray(vbo_Position_Pyramid);
		vbo_Position_Pyramid=null;
	}

	if(vao_Pyramid)
	{
		gl.deleteVertexArray(vao_Pyramid);
		vao_Pyramid=null;
	}


	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:  // Escape
				//uninitialize
				uninitialize();
				//close our application's tab
				window.close();  //may not work in firefox but works in safari and chrome
				break;

		case 70: //for 'F' or 'f'
		toggleFullScreen();
		break;
	}
}

function mouseDown()
{
	//code
}

function degToRad(degrees)
{
	//code
	return(degrees * Math.PI / 180);
}

