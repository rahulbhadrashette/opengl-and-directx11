// global variables
var canvas=null;
var gl=null;
var bFullscreen = false;

var canvas_original_width;
var canvas_original_height;

const WebGLMacros = {
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3
};

var angle = 0.0;
var Co1 = 0.0;
var Co2 = 0.0;

var light_ambient_Red = new Float32Array(3);
var light_diffuse_Red = new Float32Array(3);
var light_specular_Red = new Float32Array(3);
var light_position_Red = new Float32Array(4);

var light_ambient_Green = new Float32Array(3);
var light_diffuse_Green = new Float32Array(3);
var light_specular_Green = new Float32Array(3);
var light_position_Green = new Float32Array(4);

var light_ambient_Blue = new Float32Array(3);
var light_diffuse_Blue = new Float32Array(3);
var light_specular_Blue = new Float32Array(3);
var light_position_Blue = new Float32Array(4);

var material_ambient =  [0.0, 0.0, 0.0];
var material_diffuse =  [1.0, 1.0, 1.0];
var material_specular = [1.0, 1.0 ,1.0];
var material_shininess = 128.0;

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject_PV;
var shaderProgramObject_PF;
//***********************************************
//For Vertex shader Variables
var model_matrix_uniform;
var view_matrix_uniform;
var projection_matrix_uniform;

var Ka_uniform;
var Kd_uniform;
var Ks_uniform;
var material_shininess_uniform;
var light_enable_uniform;

var La_uniform_Red;
var Ld_uniform_Red;
var Ls_uniform_Red;
var light_position_uniform_Red;

var La_uniform_Green;
var Ld_uniform_Green;
var Ls_uniform_Green;
var light_position_uniform_Green;

var La_uniform_Blue;
var Ld_uniform_Blue;
var Ls_uniform_Blue;
var light_position_uniform_Blue;
//********************************************
//For fragment Shader Variables
var model_matrix_uniform_RMB;
var view_matrix_uniform_RMB;
var projection_matrix_uniform_RMB;

var Ka_uniform_RMB;
var Kd_uniform_RMB;
var Ks_uniform_RMB;
var material_shininess_uniform_RMB;

var La_uniform_Red_RMB;
var Ld_uniform_Red_RMB;
var Ls_uniform_Red_RMB;
var light_position_uniform_Red_RMB;

var La_uniform_Green_RMB;
var Ld_uniform_Green_RMB;
var Ls_uniform_Green_RMB;
var light_position_uniform_Green_RMB;

var La_uniform_Blue_RMB;
var Ld_uniform_Blue_RMB;
var Ls_uniform_Blue_RMB;
var light_position_uniform_Blue_RMB;
//********************************************
var light_enable_uniform_RMB;
var blKeyPressed = false ; // for lights
var fragmentShaderladder = false;

var perspectiveProjectionMatrix;
//********************************************
//to start animation
var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame;


function main() 
{
	canvas = document.getElementById('AMC')
	if(!canvas) {
		console.log("Obtaining canvas failed.\n");
	} else {
		console.log("Obtaining canvas succeeded.\n");
	}

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	//print canvas height and width
	console.log('Canvas width : '+canvas.width +' And canvas height : ' + canvas.height);

	window.addEventListener('keydown', keydown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    init();

    resize();
    draw();
}

function compileShader(shader, shaderType) {
	var shaderObject =  gl.createShader(shaderType);
	gl.shaderSource(shaderObject, shader);
	gl.compileShader(shaderObject);
	if(gl.getShaderParameter(shaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(shaderObject);
		if(error.length >0) {
			console.log(error);
			uninitialize();
		}
	}
	return shaderObject;
}

function init() 
{
	gl = canvas.getContext('webgl2');
	if(gl==null) {
		console.log("Failed to get rendering context..\n");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;


	vertexShader();
	fragmentShader() 

	sphere=new Mesh();
    makeSphere(sphere,2.0,30,30);

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.enable(gl.CULL_FACE);

	gl.clearColor(0.0,0.0,0.0, 1.0);

	perspectiveProjectionMatrix = mat4.create();

}

function resize() 
{
	if(bFullscreen==true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	} else {
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	console.log("In resize	 : "+ canvas.width);
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat(canvas.width)/parseFloat(canvas.height)), 0.1, 100.0);
}

function draw() 
{
	light_Array();
	gl.clear(gl.COLOR_BUFFER_BIT);
	//console.log('Co-Ordinate : '+Co1 +' And Co-Ordinate : ' + Co2);

	if(fragmentShaderladder == true)
	{
		gl.useProgram(shaderProgramObject_PF);

		if(blKeyPressed)
	    {
	        gl.uniform1i(light_enable_uniform_RMB, 1);
	        //For Red Color Lighting
	        gl.uniform3fv(La_uniform_Red_RMB, light_ambient_Red);
	        gl.uniform3fv(Ld_uniform_Red_RMB, light_diffuse_Red);
	        gl.uniform3fv(Ls_uniform_Red_RMB, light_specular_Red);
	        gl.uniform4fv(light_position_uniform_Red_RMB, light_position_Red);
	        //For Green Color Lighting
	        gl.uniform3fv(La_uniform_Green_RMB, light_ambient_Green);
	        gl.uniform3fv(Ld_uniform_Green_RMB, light_diffuse_Green);
	        gl.uniform3fv(Ls_uniform_Green_RMB, light_specular_Green);
	        gl.uniform4fv(light_position_uniform_Green_RMB, light_position_Green);
	        //For Blue Color Lighting
	        gl.uniform3fv(La_uniform_Blue_RMB, light_ambient_Blue);
	        gl.uniform3fv(Ld_uniform_Blue_RMB, light_diffuse_Blue);
	        gl.uniform3fv(Ls_uniform_Blue_RMB, light_specular_Blue);
	        gl.uniform4fv(light_position_uniform_Blue_RMB, light_position_Blue);

	        gl.uniform3fv(Ka_uniform_RMB, material_ambient);
	        gl.uniform3fv(Kd_uniform_RMB, material_diffuse);
	        gl.uniform3fv(Ks_uniform_RMB, material_specular);
	        gl.uniform1f(material_shininess_uniform_RMB, material_shininess);
	    }
	    else
	    {
	        gl.uniform1i(light_enable_uniform_RMB, 0);
	    }
	}
	else
	{
		gl.useProgram(shaderProgramObject_PV)

		if(blKeyPressed)
	    {
	        gl.uniform1i(light_enable_uniform, 1);
	        //For Red Color Lighting
	        gl.uniform3fv(La_uniform_Red, light_ambient_Red);
	        gl.uniform3fv(Ld_uniform_Red, light_diffuse_Red);
	        gl.uniform3fv(Ls_uniform_Red, light_specular_Red);
	        gl.uniform4fv(light_position_uniform_Red, light_position_Red);
	        //For Green Color Lighting
	        gl.uniform3fv(La_uniform_Green, light_ambient_Green);
	        gl.uniform3fv(Ld_uniform_Green, light_diffuse_Green);
	        gl.uniform3fv(Ls_uniform_Green, light_specular_Green);
	        gl.uniform4fv(light_position_uniform_Green, light_position_Green);
	        //For Blue Color Lighting
	        gl.uniform3fv(La_uniform_Blue, light_ambient_Blue);
	        gl.uniform3fv(Ld_uniform_Blue, light_diffuse_Blue);
	        gl.uniform3fv(Ls_uniform_Blue, light_specular_Blue);
	        gl.uniform4fv(light_position_uniform_Blue, light_position_Blue);

	        gl.uniform3fv(Ka_uniform, material_ambient);
	        gl.uniform3fv(Kd_uniform, material_diffuse);
	        gl.uniform3fv(Ks_uniform, material_specular);
	        gl.uniform1f(material_shininess_uniform, material_shininess);
	    }
	    else
	    {
	        gl.uniform1i(light_enable_uniform, 0);
	    }
	}
	

    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);

	gl.uniformMatrix4fv(model_matrix_uniform, false, modelMatrix);
	gl.uniformMatrix4fv(view_matrix_uniform, false, viewMatrix);
	gl.uniformMatrix4fv(projection_matrix_uniform, false, perspectiveProjectionMatrix);

	gl.uniformMatrix4fv(model_matrix_uniform_RMB, false, modelMatrix);
	gl.uniformMatrix4fv(view_matrix_uniform_RMB, false, viewMatrix);
	gl.uniformMatrix4fv(projection_matrix_uniform_RMB, false, perspectiveProjectionMatrix);

	sphere.draw();

	gl.useProgram(null);

	angle = angle + 0.01;
	if(angle>=360.0)
	{
		angle = angle - 360.0;
	}
	
	Co1 = Math.cos(angle) * 100.0;
	Co2 = Math.sin(angle) * 100.0;

	requestAnimationFrame (draw, canvas);

}

function light_Array()
{
	light_ambient_Red[0] = 0.0;
	light_ambient_Red[1] = 0.0;
	light_ambient_Red[2] = 0.0;

	light_diffuse_Red[0] = 1.0;
	light_diffuse_Red[1] = 0.0;
	light_diffuse_Red[2] = 0.0;

	light_specular_Red[0] = 1.0;
	light_specular_Red[1] = 0.0;
	light_specular_Red[2] = 0.0;

	light_position_Red[0] = 0.0;
	light_position_Red[1] = Co1;
	light_position_Red[2] = Co2;
	light_position_Red[3] = 1.0;

	//For Green Color Lighting
	light_ambient_Green[0] = 0.0;
	light_ambient_Green[1] = 0.0;
	light_ambient_Green[2] = 0.0;

	light_diffuse_Green[0] = 0.0;
	light_diffuse_Green[1] = 1.0;
	light_diffuse_Green[2] = 0.0;

	light_specular_Green[0] = 0.0;
	light_specular_Green[1] = 1.0;
	light_specular_Green[2] = 0.0;

	light_position_Green[0] = Co1;
	light_position_Green[1] = 0.0;
	light_position_Green[2] = Co2;
	light_position_Green[3] = 1.0;

	//For Blue Color Lighting
	light_ambient_Blue[0] = 0.0;
	light_ambient_Blue[1] = 0.0;
	light_ambient_Blue[2] = 0.0;

	light_diffuse_Blue[0] = 0.0;
	light_diffuse_Blue[1] = 0.0;
	light_diffuse_Blue[2] = 1.0;

	light_specular_Blue[0] = 0.0;
	light_specular_Blue[1] = 0.0;
	light_specular_Blue[2] = 1.0;

	light_position_Blue[0] = Co1;
	light_position_Blue[1] = Co2;
	light_position_Blue[2] = 0.0;
	light_position_Blue[3] = 1.0;
}

function uninitialize() 
{

	if(sphere)
    {
        sphere.deallocate();
        sphere=null;
    }

	if(shaderProgramObject_PF) {
		if(vertexShaderObject) {
			gl.detachShader(shaderProgramObject_PF, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		if(fragmentShaderObject) {
			gl.detachShader(shaderProgramObject_PF, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
	}
	gl.deleteProgram(shaderProgramObject_PF);
	shaderProgramObject_PF = null;

	if(shaderProgramObject_PV) {
		if(vertexShaderObject) {
			gl.detachShader(shaderProgramObject_PV, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		if(fragmentShaderObject) {
			gl.detachShader(shaderProgramObject_PV, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
	}

	gl.deleteProgram(shaderProgramObject_PV);
	shaderProgramObject_PV = null;
}

function keydown(event) 
{
	switch(event.keyCode)
	{
		case 27:  // Escape
				toggleFullScreen();
				break;

		case 69:
				//uninitialize
				uninitialize();
				//close our application's tab
				window.close();  //may not work in firefox but works in safari and chrome
				break;

		case 76:  //for 'l' Or 'L'--> Light
				if(blKeyPressed == false)
				{
					blKeyPressed = true;
				}
				else
				{
					blKeyPressed = false;
				}
				break;

		case 70:
				fragmentShaderladder = true;
				break;

		case 86:
				fragmentShaderladder = false;
				break;
	}
}

function mouseDown() {
	//alert('mouse is clicked.');
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenelement ||
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;

	// if not fullScreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
			bFullScreen = false;
	}
}

function vertexShader()
{
	// code
		var vertexShaderSourceCode = 
			"#version 300 es" +
            "\n" +
            "in vec4 vPosition;" +
            "in vec3 vNormal;" +
            "uniform mat4 u_model_matrix;" +
            "uniform mat4 u_view_matrix;" +
            "uniform mat4 u_projection_matrix;" +

            "uniform vec3 u_Ka;" +
            "uniform vec3 u_Kd;" +
            "uniform vec3 u_Ks;" +

            "uniform vec3 u_La_Red;" +
            "uniform vec3 u_Ld_Red;" +
            "uniform vec3 u_Ls_Red;" +
            "uniform vec4 u_light_position_Red;" +

            "uniform vec3 u_La_Green;" +
            "uniform vec3 u_Ld_Green;" +
            "uniform vec3 u_Ls_Green;" +
            "uniform vec4 u_light_position_Green;" +

            "uniform vec3 u_La_Blue;" +
            "uniform vec3 u_Ld_Blue;" +
            "uniform vec3 u_Ls_Blue;" +
            "uniform vec4 u_light_position_Blue;" +

            "uniform float u_material_shininess;" +
            "uniform int u_lighting_enabled;" +

            "out vec3 phong_ads_color;" +
            "void main(void)" +
            "{" +
	            "if(u_lighting_enabled == 1)" +
	            "{" +
		            "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
		            "vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" +
		            "vec3 viewer_vector = normalize(-eye_coordinates.xyz);" +
		            //fOR Red Color Lighting
		            "vec3 light_direction_Red = normalize(vec3(u_light_position_Red) - eye_coordinates.xyz);" +
		            "float tn_dot_ld_Red = max(dot(transformed_normals, light_direction_Red), 0.0);" +
		            "vec3 reflection_vector_Red = reflect(-light_direction_Red, transformed_normals);" +
		            "vec3 ambient_Red = u_La_Red * u_Ka;" +
		            "vec3 diffuse_Red = u_Ld_Red * u_Kd * tn_dot_ld_Red;" +
		            "vec3 specular_Red = u_Ls_Red * u_Ks * pow(max(dot(reflection_vector_Red, viewer_vector), 0.0), u_material_shininess);" +
		            
		            //fOR Green Color Lighting
		            "vec3 light_direction_Green = normalize(vec3(u_light_position_Green) - eye_coordinates.xyz);" +
		            "float tn_dot_ld_Green = max(dot(transformed_normals, light_direction_Green), 0.0);" +
		            "vec3 reflection_vector_Green = reflect(-light_direction_Green, transformed_normals);" +
		            "vec3 ambient_Green = u_La_Green * u_Ka;" +
		            "vec3 diffuse_Green = u_Ld_Green * u_Kd * tn_dot_ld_Green;" +
		            "vec3 specular_Green = u_Ls_Green * u_Ks * pow(max(dot(reflection_vector_Green, viewer_vector), 0.0), u_material_shininess);" +

		            //fOR Blue Color Lighting
		            "vec3 light_direction_Blue = normalize(vec3(u_light_position_Blue) - eye_coordinates.xyz);" +
		            "float tn_dot_ld_Blue = max(dot(transformed_normals, light_direction_Blue), 0.0);" +
		            "vec3 reflection_vector_Blue = reflect(-light_direction_Blue, transformed_normals);" +
		            "vec3 ambient_Blue = u_La_Blue * u_Ka;" +
		            "vec3 diffuse_Blue = u_Ld_Blue * u_Kd * tn_dot_ld_Blue;" +
		            "vec3 specular_Blue = u_Ls_Blue * u_Ks * pow(max(dot(reflection_vector_Blue, viewer_vector), 0.0), u_material_shininess);" +

		            "phong_ads_color = ambient_Red + diffuse_Red + specular_Red + ambient_Green + diffuse_Green + specular_Green + ambient_Blue + diffuse_Blue + specular_Blue;" +
	            "}" +
	            "else" +
	            "{" +
	            	"phong_ads_color = vec3(1.0, 1.0, 1.0);" +
	            "}" +
            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
            "}";


            //ambient + diffuse + specular
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false);
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	var fragmentShaderSourceCode = 
			"#version 300 es" +
            "\n" +
            "precision highp float;" +
            "in vec3 phong_ads_color;" +
            "out vec4 FragColor;" +
            "void main(void)" +
            "{" +
            "	FragColor = vec4(phong_ads_color, 1.0);" +
            "}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false);
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject_PV = gl.createProgram();

	gl.attachShader(shaderProgramObject_PV, vertexShaderObject);
	gl.attachShader(shaderProgramObject_PV, fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject_PV, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_PV, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");

		//linking
	gl.linkProgram(shaderProgramObject_PV);
	if(!gl.getProgramParameter(shaderProgramObject_PV, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_PV);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	model_matrix_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_model_matrix");
    view_matrix_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_view_matrix");
    projection_matrix_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_projection_matrix");
    light_enable_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_lighting_enabled");

    Ka_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_Ka");
    Kd_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_Kd");
    Ks_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_Ks");
    material_shininess_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_material_shininess");

    La_uniform_Red = gl.getUniformLocation(shaderProgramObject_PV, "u_La_Red");
    Ld_uniform_Red = gl.getUniformLocation(shaderProgramObject_PV, "u_Ld_Red");
    Ls_uniform_Red = gl.getUniformLocation(shaderProgramObject_PV, "u_Ls_Red");
    light_position_uniform_Red = gl.getUniformLocation(shaderProgramObject_PV, "u_light_position_Red");

    La_uniform_Green = gl.getUniformLocation(shaderProgramObject_PV, "u_La_Green");
    Ld_uniform_Green = gl.getUniformLocation(shaderProgramObject_PV, "u_Ld_Green");
    Ls_uniform_Green = gl.getUniformLocation(shaderProgramObject_PV, "u_Ls_Green");
    light_position_uniform_Green = gl.getUniformLocation(shaderProgramObject_PV, "u_light_position_Green");

    La_uniform_Blue = gl.getUniformLocation(shaderProgramObject_PV, "u_La_Blue");
    Ld_uniform_Blue = gl.getUniformLocation(shaderProgramObject_PV, "u_Ld_Blue");
    Ls_uniform_Blue = gl.getUniformLocation(shaderProgramObject_PV, "u_Ls_Blue");
    light_position_uniform_Blue = gl.getUniformLocation(shaderProgramObject_PV, "u_light_position_Blue");
}

function fragmentShader() 
{
	// code
	var vertexShaderSourceCode = 
		"#version 300 es" +
        "\n" +
		"in vec4 vertexPosition_RMB;" +
		"in vec3 vertexNormal_RMB;" +

		"uniform mat4 u_modelMatrix_RMB;" +
		"uniform mat4 u_viewMatrix_RMB;" +
		"uniform mat4 u_projection_matrix_RMB;" +

		"uniform mediump int u_lKeyIsPressed_RMB;" +

		"uniform vec4 u_Light_Position_Red_RMB;" +
		"uniform vec4 u_Light_Position_Green_RMB;" +
		"uniform vec4 u_Light_Position_Blue_RMB;" +

		"out vec3 tnorm_RMB;" +
		"out vec3 viewerVector_RMB;" +
		"out vec3 lightdirection_Red_RMB;" +
		"out vec3 lightdirection_Green_RMB;" +
		"out vec3 lightdirection_Blue_RMB;" +
		

		"void main(void)" +
		"{" +
			"if(u_lKeyIsPressed_RMB == 1)" +
			"{" +
				"vec4 eye_coordinate_RMB = u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" +

				"tnorm_RMB = mat3(u_viewMatrix_RMB * u_modelMatrix_RMB) * vertexNormal_RMB;" +

				"viewerVector_RMB = vec3(-eye_coordinate_RMB.xyz);" +

				"lightdirection_Red_RMB = vec3(u_Light_Position_Red_RMB - eye_coordinate_RMB);" +
				"lightdirection_Green_RMB = vec3(u_Light_Position_Green_RMB - eye_coordinate_RMB);" +
				"lightdirection_Blue_RMB = vec3(u_Light_Position_Blue_RMB - eye_coordinate_RMB);" +
			"}" +

		"gl_Position = u_projection_matrix_RMB * u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" +
		"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false);
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	var fragmentShaderSourceCode = 
		"#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec3 tnorm_RMB;" +
		"in vec3 lightdirection_Red_RMB;" +
		"in vec3 lightdirection_Green_RMB;" +
		"in vec3 lightdirection_Blue_RMB;" +
		"in vec3 viewerVector_RMB;" +

		"uniform vec3 u_La_Red_RMB;" +
		"uniform vec3 u_Ld_Red_RMB;" +
		"uniform vec3 u_Ls_Red_RMB;" +

		"uniform vec3 u_La_Green_RMB;" +
		"uniform vec3 u_Ld_Green_RMB;" +
		"uniform vec3 u_Ls_Green_RMB;" +

		"uniform vec3 u_La_Blue_RMB;" +
		"uniform vec3 u_Ld_Blue_RMB;" +
		"uniform vec3 u_Ls_Blue_RMB;" +

		"uniform vec3 u_Ka_RMB;" +
		"uniform vec3 u_Kd_RMB;" +
		"uniform vec3 u_Ks_RMB;" +

		"uniform float u_materialShininess_RMB;" +
		"uniform mediump int u_lKeyIsPressed_RMB;" +

		"out vec4 fragColor;" +

		"void main(void)" +
		"{" +
			"vec3 phong_AdsLight_RMB;" +
			"if(u_lKeyIsPressed_RMB == 1)" +
			"{" +

				"vec3 normalize_tnorm_RMB = normalize(tnorm_RMB);" +
				"vec3 normalize_viewerVector_RMB = normalize(viewerVector_RMB);" +
				//For Red Color Lighting
				"vec3 normalize_lightdirection_Red_RMB = normalize(lightdirection_Red_RMB);" +

				"vec3 reflectionVector_Red_RMB = reflect(-normalize_lightdirection_Red_RMB, normalize_tnorm_RMB);" +

				"float tn_dot_lightDir_Red_RMB = max(dot(normalize_lightdirection_Red_RMB, normalize_tnorm_RMB), 0.0);" +

				"vec3 ambient_Red_RMB = u_La_Red_RMB * u_Ka_RMB;" +

				"vec3 diffuse_Red_RMB = u_Ld_Red_RMB * u_Kd_RMB * tn_dot_lightDir_Red_RMB;" +

				"vec3 specular_Red_RMB = u_Ls_Red_RMB * u_Ks_RMB * pow(max(dot(reflectionVector_Red_RMB, normalize_viewerVector_RMB), 0.0), u_materialShininess_RMB);" +

				//For Green Color Lighting
				"vec3 normalize_lightdirection_Green_RMB = normalize(lightdirection_Green_RMB);" +

				"vec3 reflectionVector_Green_RMB = reflect(-normalize_lightdirection_Green_RMB, normalize_tnorm_RMB);" +

				"float tn_dot_lightDir_Green_RMB = max(dot(normalize_lightdirection_Green_RMB, normalize_tnorm_RMB), 0.0);" +

				"vec3 ambient_Green_RMB = u_La_Green_RMB * u_Ka_RMB;" +

				"vec3 diffuse_Green_RMB = u_Ld_Green_RMB * u_Kd_RMB * tn_dot_lightDir_Green_RMB;" +

				"vec3 specular_Green_RMB = u_Ls_Green_RMB * u_Ks_RMB * pow(max(dot(reflectionVector_Green_RMB, normalize_viewerVector_RMB), 0.0), u_materialShininess_RMB);" +

				//For Blue Color Lighting
				"vec3 normalize_lightdirection_Blue_RMB = normalize(lightdirection_Blue_RMB);" +

				"vec3 reflectionVector_Blue_RMB = reflect(-normalize_lightdirection_Blue_RMB, normalize_tnorm_RMB);" +

				"float tn_dot_lightDir_Blue_RMB = max(dot(normalize_lightdirection_Blue_RMB, normalize_tnorm_RMB), 0.0);" +

				"vec3 ambient_Blue_RMB = u_La_Blue_RMB * u_Ka_RMB;" +

				"vec3 diffuse_Blue_RMB = u_Ld_Blue_RMB * u_Kd_RMB * tn_dot_lightDir_Blue_RMB;" +

				"vec3 specular_Blue_RMB = u_Ls_Blue_RMB * u_Ks_RMB * pow(max(dot(reflectionVector_Blue_RMB, normalize_viewerVector_RMB), 0.0), u_materialShininess_RMB);" +

				"phong_AdsLight_RMB = ambient_Red_RMB + diffuse_Red_RMB + specular_Red_RMB + ambient_Green_RMB + diffuse_Green_RMB + specular_Green_RMB + ambient_Blue_RMB + diffuse_Blue_RMB + specular_Blue_RMB;" + 

			"}" +
			"else" +
			"{" +

				"phong_AdsLight_RMB = vec3(1.0, 1.0, 1.0);" +

			"}" +

			"fragColor = vec4(phong_AdsLight_RMB, 1.0);" +
		"}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false);
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject_PF = gl.createProgram();

	gl.attachShader(shaderProgramObject_PF, vertexShaderObject);
	gl.attachShader(shaderProgramObject_PF, fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject_PF, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vertexPosition_RMB");
	gl.bindAttribLocation(shaderProgramObject_PF, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vertexNormal_RMB");

		//linking
	gl.linkProgram(shaderProgramObject_PF);
	if(!gl.getProgramParameter(shaderProgramObject_PF, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_PF);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	model_matrix_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_modelMatrix_RMB");
    view_matrix_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_viewMatrix_RMB");
    projection_matrix_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_projection_matrix_RMB");
    light_enable_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_lKeyIsPressed_RMB");

    Ka_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Ka_RMB");
    Kd_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Kd_RMB");
    Ks_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Ks_RMB");
    material_shininess_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_materialShininess_RMB");
    //For Red Color Lighting
    La_uniform_Red_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_La_Red_RMB");
    Ld_uniform_Red_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Ld_Red_RMB");
    Ls_uniform_Red_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Ls_Red_RMB");
    light_position_uniform_Red_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Light_Position_Red_RMB");
    //For green Color Lighting
    La_uniform_Green_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_La_Green_RMB");
    Ld_uniform_Green_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Ld_Green_RMB");
    Ls_uniform_Green_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Ls_Green_RMB");
    light_position_uniform_Green_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Light_Position_Green_RMB");
    //For Blue Color Lighting
    La_uniform_Blue_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_La_Blue_RMB");
    Ld_uniform_Blue_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Ld_Blue_RMB");
    Ls_uniform_Blue_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Ls_Blue_RMB");
    light_position_uniform_Blue_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Light_Position_Blue_RMB");
}