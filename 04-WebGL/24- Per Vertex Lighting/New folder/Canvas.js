// global variables
var canvas=null;
var context=null;
var gl=null;
var bFullscreen = false;

var canvas_original_width;
var canvas_original_height;

const WebGLMacros = {
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

var La_uniform;
var Ld_uniform;
var Ls_uniform;
var light_position_uniform;

var Ka_uniform;
var Kd_uniform;
var Ks_uniform;
var material_shininess_uniform;

var light_enable_uniform;

var lightEnabled = false ; // for lights

var perspectiveProjectionMatrix;

var light_ambient=[0.0,0.0,0.0];
var light_diffuse=[1.0,1.0,1.0];
var light_specular=[1.0,1.0,1.0];
var light_position=[100.0,100.0,100.0,1.0];

var material_ambient= [0.0,0.0,0.0];
var material_diffuse= [1.0,1.0,1.0];
var material_specular= [1.0,1.0,1.0];
var material_shininess= 50.0;

//To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

//To stop animation : To have cancleAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;;

//onload function
function main()
{
	//get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed...!!!\n");
	else
		console.log("Obtaining Canvas Succeeded...!!!\n");

	canvas_orignal_width = canvas.width;
	canvas_orignal_height = canvas.height;
	//print canvas width and height on console
	console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//initialize WebGL
	init();

	//start drawing here as warning-up
	resize();
	draw();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenelement ||
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;

	// if not fullScreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
			bFullScreen = false;
	}
}

function init() 
{
	gl = canvas.getContext('webgl2');
	if(gl==null) {
		console.log("Failed to get rendering context..\n");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	var vertexShaderSourceCode = 
			"#version 300 es" +
            "\n" +
            "in vec4 vPosition;" +
            "in vec3 vNormal;" +
            "uniform mat4 u_model_matrix;" +
            "uniform mat4 u_view_matrix;" +
            "uniform mat4 u_projection_matrix;" +
            "uniform int u_lighting_enabled;" +
            "uniform vec3 u_La;" +
            "uniform vec3 u_Ld;" +
            "uniform vec3 u_Ls;" +
            "uniform vec4 u_light_position;" +
            "uniform vec3 u_Ka;" +
            "uniform vec3 u_Kd;" +
            "uniform vec3 u_Ks;" +
            "uniform float u_material_shininess;" +
            "out vec3 phong_ads_color;" +
            "void main(void)" +
            "{" +
            "if(u_lighting_enabled == 1)" +
            "{" +
            "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
            "vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" +
            "vec3 light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz);" +
            "float tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);" +
            "vec3 ambient = u_La * u_Ka;" +
            "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" +
            "vec3 reflection_vector = reflect(-light_direction, transformed_normals);" +
            "vec3 viewer_vector = normalize(-eye_coordinates.xyz);" +
            "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);" +
            "phong_ads_color = ambient + diffuse + specular;" +
            "}" +
            "else" +
            "{" +
            "phong_ads_color = vec3(1.0, 1.0, 1.0);" +
            "}" +
            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
            "}";

		vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
		gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
		gl.compileShader(vertexShaderObject);
		if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false);
		{
			var error=gl.getShaderInfoLog(vertexShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

	var fragmentShaderSourceCode = 
			"#version 300 es" +
            "\n" +
            "precision highp float;" +
            "in vec3 phong_ads_color;" +
            "out vec4 FragColor;" +
            "void main(void)" +
            "{" +
            "FragColor = vec4(phong_ads_color, 1.0);" +
            "}";

		fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
		gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		gl.compileShader(fragmentShaderObject);
		if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false);
		{
			var error=gl.getShaderInfoLog(fragmentShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

		//shader program
		shaderProgramObject=gl.createProgram();
		gl.attachShader(shaderProgramObject, vertexShaderObject);
		gl.attachShader(shaderProgramObject, fragmentShaderObject);

		//pre-link binding of shader program object with vertex shader attribute
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

		//linking
		gl.linkProgram(shaderProgramObject);
		if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
		{
			var error = gl.getProgramInfoLog(shaderProgramObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}
	
	model_matrix_uniform = gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
    view_matrix_uniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
    projection_matrix_uniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
    light_enable_uniform = gl.getUniformLocation(shaderProgramObject, "u_lighting_enabled");
    La_uniform = gl.getUniformLocation(shaderProgramObject, "u_La");
    Ld_uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld");
    Ls_uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls");
    light_position_uniform = gl.getUniformLocation(shaderProgramObject, "u_light_position");

    Ka_uniform = gl.getUniformLocation(shaderProgramObject, "u_Ka");
    Kd_uniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
    Ks_uniform = gl.getUniformLocation(shaderProgramObject, "u_Ks");
    material_shininess_uniform = gl.getUniformLocation(shaderProgramObject, "u_material_shininess");


	sphere=new Mesh();
    makeSphere(sphere,2.0,30,30);

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.enable(gl.CULL_FACE);

	gl.clearColor(0.0,0.0,0.0, 1.0);

	perspectiveProjectionMatrix = mat4.create();

}

function resize()
{
	//code
	if(bFullscreen == true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_orignal_width;
		canvas.height=canvas_orignal_height;
	}

	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	//Perspective Graphic Projection => left, rigth, bottom, top
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function draw() {
	gl.clear(gl.COLOR_BUFFER_BIT);
	gl.useProgram(shaderProgramObject)
	if(lightEnabled)
    {
        gl.uniform1i(light_enable_uniform, 1);

        gl.uniform3fv(La_uniform, light_ambient);
        gl.uniform3fv(Ld_uniform, light_diffuse);
        gl.uniform3fv(Ls_uniform, light_specular);
        gl.uniform4fv(light_position_uniform, light_position);

        gl.uniform3fv(Ka_uniform, material_ambient);
        gl.uniform3fv(Kd_uniform, material_diffuse);
        gl.uniform3fv(Ks_uniform, material_specular);
        gl.uniform1f(material_shininess_uniform, material_shininess);
    }
    else
    {
        gl.uniform1i(light_enable_uniform, 0);
    }

    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);

	gl.uniformMatrix4fv(model_matrix_uniform, false, modelMatrix);
	gl.uniformMatrix4fv(view_matrix_uniform, false, viewMatrix);
	gl.uniformMatrix4fv(projection_matrix_uniform, false, perspectiveProjectionMatrix);

	sphere.draw();

	gl.useProgram(null);
	requestAnimationFrame (draw, canvas);

}

function uninitialize() {

	if(sphere)
    {
        sphere.deallocate();
        sphere=null;
    }

	if(shaderProgramObject) {
		if(vertexShaderObject) {
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		if(fragmentShaderObject) {
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
	}

	gl.deleteProgram(shaderProgramObject);
	shaderProgramObject = null;
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:  // Escape
				//uninitialize
				uninitialize();
				//close our application's tab
				window.close();  //may not work in firefox but works in safari and chrome
				break;

		case 76:  //for 'l' Or 'L'--> Light
				if(blKeyPressed == false)
				{
					blKeyPressed = true;
				}
				else
				{
					blKeyPressed = false;
				}
				break;

		case 70: //for 'F' or 'f'
		toggleFullScreen();
		break;
	}
}

function mouseDown()
{
	//code
}

