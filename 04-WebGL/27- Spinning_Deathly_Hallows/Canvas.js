//global variable
var canvas = null;
var gl=null; //For WebGL Context
var bFullScreen=false;
var canvas_orignal_width;
var canvas_orignal_height;

const WebGLMacros=//when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

//For Line
var vao_Line;
var vbo_Line;
var vbo_Color_Line;
//For Triangle
var vao_Triangle;
var vbo_Triangle;
var vbo_Color_Triangle;
//For Circle
var vao_Circle;
var vbo_Circle;
var vbo_Color_Circle;

var pos_Yl = 4.0;
var neg_Xt = -4.0, neg_Yt = -4.0;
var pos_Xc = 3.795, neg_Yc = -4.1;
var angle = 0.0;
var triangle = false;
var circle = false;

var mvpUniform;
var perspectiveProjectionMatrix;

//To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

//To stop animation : To have cancleAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;;

//onload function
function main()
{
	//get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed...!!!\n");
	else
		console.log("Obtaining Canvas Succeeded...!!!\n");

	canvas_orignal_width = canvas.width;
	canvas_orignal_height = canvas.height;
	//print canvas width and height on console
	console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//initialize WebGL
	init();

	//start drawing here as warning-up
	resize();
	display();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenelement ||
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;

	// if not fullScreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
			bFullScreen = false;
	}
}

function init()
{
	//code
	//get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if(gl==null) //failed to get context
	{
		console.log("Failed to get the rendering context for WebGL...!!!");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	var vertexShaderSourceCode=
			"#version 300 es" +
			"\n" +
            "in vec4 vPosition;" +
            "in vec4 vColor;" +
            "out vec4 out_Color;" +
            "uniform mat4 u_mvp_matrix;" +
            "void main(void)" +
            "{" +
                "gl_Position = u_mvp_matrix * vPosition;" +
                "out_Color = vColor;" +
            "}";


		vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
		gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
		gl.compileShader(vertexShaderObject);
		if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false);
		{
			var error=gl.getShaderInfoLog(vertexShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

	 //Write Fragment Shader Code
    var fragmentShaderSourceCode =
    		"#version 300 es" +
			"\n" +
			"precision highp float;" +
            "in vec4 out_Color;" +
            "out vec4 fragColor;" +
            "void main(void)" +
            "{" +
               "fragColor = out_Color;" +
            "}";

		fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
		gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		gl.compileShader(fragmentShaderObject);
		if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false);
		{
			var error=gl.getShaderInfoLog(fragmentShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}


		//shader program
		shaderProgramObject=gl.createProgram();
		gl.attachShader(shaderProgramObject, vertexShaderObject);
		gl.attachShader(shaderProgramObject, fragmentShaderObject);

		//pre-link binding of shader program object with vertex shader attribute
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");


		//linking
		gl.linkProgram(shaderProgramObject);
		if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
		{
			var error = gl.getProgramInfoLog(shaderProgramObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}


	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	
	//Line Vertices
	var lineVertices = new Float32Array
    	([
    		//Line
			0.0, 0.8,0.0,
			0.0, -0.8, 0.0
    	]);
    //Line Color Yellow
	var lineColor = new Float32Array
    	([
    		//Line
			1.0, 1.0,0.0,
			1.0, 1.0, 0.0	
    	]);

    //Triangle Vertices
    var triangleVertices = new Float32Array
    	([
    		//Triangle
			0.0, 0.8, 0.0,
			-0.8, -0.8, 0.0,
			-0.8, -0.8, 0.0,
			0.8, -0.8, 0.0,
			0.8, -0.8, 0.0,
			0.0, 0.8, 0.0
    	]);

    //Triangle Color Yellow
    var Color = 6;
    var triangleColor = new Float32Array(3 * Color);

    for(var i = 0; i < Color; i++)
    {
    	triangleColor[3 * i + 0] = 1.0;
    	triangleColor[3 * i + 1] = 1.0;
    	triangleColor[3 * i + 2] = 0.0;
    }
    

    //Circle Vertices Array
    var iPoints = 300;
    var circleVertices = new Float32Array(3 * iPoints);

    for(var i = 0; i < iPoints; i++)
    {
    	var AngleCircle = (2.0 * Math.PI * i) / iPoints;
    	circleVertices[3 * i + 0] = Math.cos(AngleCircle) * 0.494;
    	circleVertices[3 * i + 1] = Math.sin(AngleCircle) * 0.494;
    	circleVertices[3 * i + 2] = 0.0;
    }

    //Triangle Color Yellow
    var circleColor = new Float32Array(3 * iPoints);

    for(var i = 0; i < iPoints; i++)
    {
    	circleColor[3 * i + 0] = 1.0;
    	circleColor[3 * i + 1] = 1.0;
    	circleColor[3 * i + 2] = 0.0;
    }

    //For Line vao
    vao_Line = gl.createVertexArray();
    gl.bindVertexArray(vao_Line);
    //For Line vbo Position
    vbo_Line = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Line);
    gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our lineVertices array
    															gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //For Line vbo Color
    vbo_Color_Line = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Color_Line);
    gl.bufferData(gl.ARRAY_BUFFER, lineColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, //2 is for S and T co-ordinates in our rectangleTexCoord array
    															gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

    //For Triangle vao
    vao_Triangle = gl.createVertexArray();
    gl.bindVertexArray(vao_Triangle);
    //For Triangle vbo Position
    vbo_Triangle = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Triangle);
    gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our lineVertices array
    															gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //For Triangle vbo color
    vbo_Color_Triangle = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Color_Triangle);
    gl.bufferData(gl.ARRAY_BUFFER, triangleColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, //2 is for S and T co-ordinates in our rectangleTexCoord array
    															gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

    //For Circle vao
    vao_Circle = gl.createVertexArray();
    gl.bindVertexArray(vao_Circle);
    //For Circle vbo Position
    vbo_Circle = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Circle);
    gl.bufferData(gl.ARRAY_BUFFER, circleVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our lineVertices array
    															gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //For Circle vbo color
    vbo_Color_Circle = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Color_Circle);
    gl.bufferData(gl.ARRAY_BUFFER, circleColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, //2 is for S and T co-ordinates in our rectangleTexCoord array
    															gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);


	//set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);  //blue

	//Depth test will always be enabled
	gl.enable(gl.DEPTH_TEST);

	////We will always cill back faces for better performance
	gl.enable(gl.CULL_FACE);

	//initialize projection matrix
	perspectiveProjectionMatrix=mat4.create();

}

function resize()
{
	//code
	if(bFullScreen == true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_orignal_width;
		canvas.height=canvas_orignal_height;
	}

	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	//Perspective Graphic Projection => left, rigth, bottom, top
	
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function display()
{
	//code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	//For Line
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, pos_Yl, -5.0])

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_Line);
	
	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	//For Triangle
	mat4.identity(modelViewMatrix); //reset to matrix
	mat4.identity(modelViewProjectionMatrix) // reset to matrix

	mat4.translate(modelViewMatrix, modelViewMatrix, [neg_Xt, neg_Yt, -5.0])
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angle))

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_Triangle);

	gl.drawArrays(gl.LINES, 0, 6);

	gl.bindVertexArray(null);

	//For Circle
	mat4.identity(modelViewMatrix); //reset to matrix
	mat4.identity(modelViewProjectionMatrix) // reset to matrix

	mat4.translate(modelViewMatrix, modelViewMatrix, [pos_Xc, neg_Yc, -5.0])
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angle))

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_Circle);

	gl.drawArrays(gl.LINES, 0, 300);

	gl.bindVertexArray(null);


	gl.useProgram(null);

	Update_RMB();
	//animation loop
	requestAnimationFrame(display, canvas);

}

function uninitialize()
{
	//code
	//For Line Uninitialization
	if(vbo_Color_Line)
	{
		gl.deleteVertexArray(vbo_Color_Line);
		vbo_Color_Line=null;
	}

	if(vbo_Line)
	{
		gl.deleteVertexArray(vbo_Line);
		vbo_Line=null;
	}

	if(vao_Line)
	{
		gl.deleteVertexArray(vao_Line);
		vao_Line=null;
	}

	//For Triangle Uninitialization
	if(vbo_Color_Triangle)
	{
		gl.deleteVertexArray(vbo_Color_Triangle);
		vbo_Color_Line=null;
	}

	if(vbo_Triangle)
	{
		gl.deleteVertexArray(vbo_Triangle);
		vbo_Triangle=null;
	}

	if(vao_Triangle)
	{
		gl.deleteVertexArray(vao_Triangle);
		vao_Triangle=null;
	}

	//For Circle Uninitialization
	if(vbo_Color_Circle)
	{
		gl.deleteVertexArray(vbo_Color_Circle);
		vbo_Color_Circle=null;
	}

	if(vbo_Circle)
	{
		gl.deleteVertexArray(vbo_Circle);
		vbo_Circle=null;
	}

	if(vao_Circle)
	{
		gl.deleteVertexArray(vao_Circle);
		vao_Circle=null;
	}


	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:  // Escape
				//uninitialize
				uninitialize();
				//close our application's tab
				window.close();  //may not work in firefox but works in safari and chrome
				break;

		case 70: //for 'F' or 'f'
		toggleFullScreen();
		break;
	}
}

function mouseDown()
{
	//code
}

function Update_RMB()
{
	//code
	//For Angle
	angle = angle + 1.5;
	if (angle > 360.0)
	{
		angle = 0.0;
	}

	//For Line Translation
	pos_Yl = pos_Yl - 0.01;
	if (pos_Yl <= 0.0)
	{
		pos_Yl = 0.0;
		triangle = true;
	}

	//For Triangle Translation
	if (triangle == true)
	{
		neg_Xt = neg_Xt + 0.01;
		neg_Yt = neg_Yt + 0.01;
		if (neg_Xt >= 0.0 || neg_Yt >= 0.0)
		{
			neg_Xt = 0.0;
			neg_Yt = 0.0;
			circle = true;
		}
	}

	//For Circle Translation
	if (circle == true)
	{
		pos_Xc = pos_Xc - 0.01;
		if (pos_Xc <= 0.0)
		{
			pos_Xc = 0.0;
		}
		neg_Yc = neg_Yc + 0.01;
		if (neg_Yc >= -0.306)
		{
			neg_Yc = -0.306;
		}
	}
}

function degToRad(degrees)
{
	//code
	return(degrees * Math.PI / 180);
}