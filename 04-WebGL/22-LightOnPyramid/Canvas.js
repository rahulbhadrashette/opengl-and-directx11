//global variable
var canvas = null;
var gl=null; //For WebGL Context
var bFullScreen=false;
var canvas_orignal_width;
var canvas_orignal_height;

const WebGLMacros=//when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE:3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var angleCube = 0.0;

var vao_Pyramid;
var vbo_Position_Pyramid;
var vbo_Normal_Pyramid;

var modelViewMatrixUniform, projectionMatrixUniform;
var ldUniform, kdUniform, lightPositionUniform;
var lKeyIsPressedUniform;

var blKeyPressed = false;

var perspectiveProjectionMatrix;

//To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

//To stop animation : To have cancleAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;;

//onload function
function main()
{
	//get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed...!!!\n");
	else
		console.log("Obtaining Canvas Succeeded...!!!\n");

	canvas_orignal_width = canvas.width;
	canvas_orignal_height = canvas.height;
	//print canvas width and height on console
	console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//initialize WebGL
	init();

	//start drawing here as warning-up
	resize();
	display();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenelement ||
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;

	// if not fullScreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
			bFullScreen = false;
	}
}

function init()
{
	//code
	//get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if(gl==null) //failed to get context
	{
		console.log("Failed to get the rendering context for WebGL...!!!");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	var vertexShaderSourceCode=
			"#version 300 es" +
            "\n" +
            "in vec4 vPosition;" +
            "in vec3 vNormal;" +
            "uniform mat4 u_mv_matrix;" +
            "uniform mat4 u_p_matrix;" +
            "uniform mediump int u_lKeyIsPressed;" +
            "uniform vec3 u_Ld;" +
            "uniform vec3 u_Kd;" +
            "uniform vec4 u_Light_Position;" +
            "out vec3 diffuse_Color;" +

            "void main(void)" +
            "{" +

                "if(u_lKeyIsPressed == 1)" +
                "{" +
                    "vec4 eye_coordinate = u_mv_matrix * vPosition;" +
                    "mat3 normalmatrix = mat3(transpose(inverse(u_mv_matrix)));" +
                    "vec3 tnorm = normalize(normalmatrix * vNormal);" +
                    "vec3 source = normalize(vec3(u_Light_Position - eye_coordinate));" +
                    "diffuse_Color = u_Ld * u_Kd * max(dot(source, tnorm), 0.0);" +
                "}" +
                "gl_Position = u_p_matrix * u_mv_matrix * vPosition;" +

            "}";

		vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
		gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
		gl.compileShader(vertexShaderObject);
		if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false);
		{
			var error=gl.getShaderInfoLog(vertexShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

	 //Write Fragment Shader Code
     var fragmentShaderSourceCode =
    		"#version 300 es" +
            "\n" +
            "precision highp float;" +
            "in vec3 diffuse_Color;" +
            "out vec4 fragColor;" +
            "uniform int u_lKeyIsPressed;" +

            "void main(void)" +
            "{" +
                
                "if(u_lKeyIsPressed == 1)" +
                "{" +
                    "fragColor = vec4(diffuse_Color, 1.0);" +
                "}" +
                "else" +
                "{" +
                    "fragColor = vec4(1.0, 1.0, 1.0, 1.0);" +
                "}" +

            "}";

		fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
		gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		gl.compileShader(fragmentShaderObject);
		if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false);
		{
			var error=gl.getShaderInfoLog(fragmentShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

		//shader program
		shaderProgramObject=gl.createProgram();
		gl.attachShader(shaderProgramObject, vertexShaderObject);
		gl.attachShader(shaderProgramObject, fragmentShaderObject);

		//pre-link binding of shader program object with vertex shader attribute
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

		//linking
		gl.linkProgram(shaderProgramObject);
		if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
		{
			var error = gl.getProgramInfoLog(shaderProgramObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

	modelViewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_mv_matrix");
	projectionMatrixUniform  = gl.getUniformLocation(shaderProgramObject, "u_p_matrix");
	ldUniform = gl.getUniformLocation(shaderProgramObject, "u_Ld");
	kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_Light_Position");
	lKeyIsPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_lKeyIsPressed");

    var pyramidVertices = new Float32Array
    	([
    		//FRONT FACE
			0.0, 1.0, 0.0,
			-1.0, -1.0, 1.0,
			1.0, -1.0, 1.0,
			 //LEFT FACE
			0.0, 1.0, 0.0,
			1.0, -1.0, 1.0,
			1.0, -1.0, -1.0,
			//BACK FACE
			0.0, 1.0, 0.0,
			1.0, -1.0, -1.0,
			-1.0, -1.0, -1.0,
			 //RIGHT FACE
			0.0, 1.0, 0.0,
			-1.0, -1.0, -1.0,
			-1.0, -1.0, 1.0
    	]);

    var pyramidNormal = new Float32Array
    	([
    		0.0, 0.447214, 0.894427,
			0.0, 0.447214, 0.894427,
			0.0, 0.447214, 0.894427,

			0.89427, 0.447214, 0.0,
			0.89427, 0.447214, 0.0,
			0.89427, 0.447214, 0.0,

			0.0, 0.447214, -0.894427,
			0.0, 0.447214, -0.894427,
			0.0, 0.447214, -0.894427,
			-0.894427, 0.447214, 0.0,
			-0.894427, 0.447214, 0.0,
			-0.894427, 0.447214, 0.0
    	]);


    vao_Pyramid = gl.createVertexArray();
    gl.bindVertexArray(vao_Pyramid);

    vbo_Position_Pyramid = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Position_Pyramid);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our pyramidVertices array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_Normal_Pyramid = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Normal_Pyramid);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidNormal, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3,//3 is for X, Y, Z co-ordinates in our pyramidNormal array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

	//set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);  //blue

	//Depth test will always be enabled
	gl.enable(gl.DEPTH_TEST);

	//depth test to do 
	gl.depthFunc(gl.LEQUAL);

	////We will always cill back faces for better performance
	//gl.enable(gl.CULL_FACE);

	//initialize projection matrix
	perspectiveProjectionMatrix=mat4.create();

}

function resize()
{
	//code
	if(bFullScreen == true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_orignal_width;
		canvas.height=canvas_orignal_height;
	}

	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	//Perspective Graphic Projection => left, rigth, bottom, top
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function display()
{
	//code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	if(blKeyPressed == true)
	{
		gl.uniform1i(lKeyIsPressedUniform, 1);
		//setting light properties
		gl.uniform3f(ldUniform, 1.0, 1.0, 1.0);// diffuse intensity of light
		//setting material properties
		gl.uniform3f(kdUniform, 0.5, 0.5, 0.5); //diffuse reflectivity of material
		var lightPosition = [0.0, 0.0, 2.0, 1.0];
		gl.uniform4fv(lightPositionUniform, lightPosition); //light position 
	}
	else
	{
		gl.uniform1i(lKeyIsPressedUniform, 0);
	}

	//For Rectangle
	var modelViewMatrix = mat4.create(); //itself create identity matrix

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -4.0]);
	mat4.scale(modelViewMatrix, modelViewMatrix, [0.75, 0.75, 0.75]);
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleCube));

	gl.uniformMatrix4fv(modelViewMatrixUniform, false, modelViewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);

	gl.bindVertexArray(vao_Pyramid);

	gl.drawArrays(gl.TRIANGLES, 0, 12);
	
	gl.bindVertexArray(null);

	gl.useProgram(null);

	angleCube = angleCube+1.0;
	if(angleCube>=360.0)
	{
		angleCube = angleCube - 360.0;
	}

	//animation loop
	requestAnimationFrame(display, canvas);
}

function uninitialize()
{
	//code

	if(vbo_Normal_Pyramid)
	{
		gl.deleteVertexArray(vbo_Normal_Pyramid);
		vbo_Normal_Pyramid=null;
	}

	if(vbo_Position_Pyramid)
	{
		gl.deleteVertexArray(vbo_Position_Pyramid);
		vbo_Position_Pyramid=null;
	}

	if(vao_Pyramid)
	{
		gl.deleteVertexArray(vao_Pyramid);
		vao_Pyramid=null;
	}


	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:  // Escape
				//uninitialize
				uninitialize();
				//close our application's tab
				window.close();  //may not work in firefox but works in safari and chrome
				break;

		case 76:  //for 'l' Or 'L'--> Light
				if(blKeyPressed == false)
				{
					blKeyPressed = true;
				}
				else
				{
					blKeyPressed = false;
				}
				break;

		case 70: //for 'F' or 'f'
		toggleFullScreen();
		break;
	}
}

function mouseDown()
{
	//code
}

function degToRad(degrees)
{
	//code
	return(degrees * Math.PI / 180);
}

