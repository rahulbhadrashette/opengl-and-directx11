//global variable
var canvas = null;
var gl=null; //For WebGL Context
var bFullScreen=false;
var canvas_orignal_width;
var canvas_orignal_height;

const WebGLMacros=//when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE:3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_Cube;
var vbo_Cube_Position;
var vbo_Cube_Normal;

var perspectiveProjectionMatrix;

var modelViewMatrixUniform, ProjectionMatrixUniform;
var ldUniform, kdUniform, lightPositionUniform;
var lKeyIsPressedUniform;

var angle = 0.0;
var bLKeyIsPressed = false;


//To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

//To stop animation : To have cancleAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;;

//onload function
function main()
{
	//get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed...!!!\n");
	else
		console.log("Obtaining Canvas Succeeded...!!!\n");

	canvas_orignal_width = canvas.width;
	canvas_orignal_height = canvas.height;

	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//initialize WebGL
	init();

	//start drawing here as warning-up
	resize();
	display();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenelement ||
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;

	// if not fullScreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
			bFullScreen = false;
	}
}

function init()
{
	//code
	//get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if(gl==null) //failed to get context
	{
		console.log("Failed to get the rendering context for WebGL...!!!");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	var vertexShaderSourceCode=
			"#version 300 es" +
            "\n" +
            "in vec4 vPosition;" +
            "in vec3 vNormal;" +
            "uniform mat4 u_mv_matrix;" +
            "uniform mat4 u_p_matrix;" +
            "uniform mediump int u_lKeyIsPressed;" +
            "uniform vec3 u_Ld;" +
            "uniform vec3 u_Kd;" +
            "uniform vec4 u_Light_Position;" +
            "out vec3 diffuse_Color;" +

            "void main(void)" +
            "{" +

                "if(u_lKeyIsPressed == 1)" +
                "{" +
                    "vec4 eye_coordinate = u_mv_matrix * vPosition;" +
                    "mat3 normalmatrix = mat3(transpose(inverse(u_mv_matrix)));" +
                    "vec3 tnorm = normalize(normalmatrix * vNormal);" +
                     "vec3 source = normalize(vec3(u_Light_Position - eye_coordinate));" +
                    "diffuse_Color = u_Ld * u_Kd * max(dot(source, tnorm));" +
                "}" +
                "gl_Position = u_p_matrix * u_mv_matrix * vPosition;" +

            "}"

		vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
		gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
		gl.compileShader(vertexShaderObject);
		if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
		{
			var error=gl.getShaderInfoLog(vertexShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

	 //Write Fragment Shader Code
    var fragmentShaderSourceCode =
    		"#version 300 es" +
            "\n" +
            "precision highp float;" +
            "in vec3 diffuse_Color;" +
            "out vec4 fragColor;" +
            "uniform int u_lKeyIsPressed;" +

            "void main(void)" +
            "{" +
            	"vec4 color;"+
                
                "if(u_lKeyIsPressed == 1)" +
                "{" +
                    "color = vec4(diffuse_Color, 1.0);" +
                "}" +
                "else" +
                "{" +
                    "color = vec4(1.0, 1.0, 1.0, 1.0);" +
                "}" +
                "fragColor = color;"+

            "}"
    	

		fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
		gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		gl.compileShader(fragmentShaderObject);
		if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
		{
			var error=gl.getShaderInfoLog(fragmentShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}


		//shader program
		shaderProgramObject=gl.createProgram();
		gl.attachShader(shaderProgramObject, vertexShaderObject);
		gl.attachShader(shaderProgramObject, fragmentShaderObject);

		//pre-link binding of shader program object with vertex shader attribute
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

		//linking
		gl.linkProgram(shaderProgramObject);
		if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
		{
			var error = gl.getProgramInfoLog(shaderProgramObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

	modelViewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_mv_matrix");
	ProjectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_p_matrix");
	lKeyIsPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_lKeyIsPressed");
	ldUniform = gl.getUniformLocation(shaderProgramObject, "u_Ld");
	kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_Light_Position");




	var cubeVertices = new Float32Array
    	([
    		 //TOP FACE
                                        1.0, 1.0, -1.0,
                                        -1.0, 1.0, -1.0,
                                        -1.0, 1.0, 1.0,
                                        1.0, 1.0, 1.0,
                                        //BOTTOM FACE
                                        1.0, -1.0, -1.0,
                                        -1.0, -1.0, -1.0,
                                        -1.0, -1.0, 1.0,
                                        1.0, -1.0, 1.0,
                                        //FRONT FACE
                                        1.0, 1.0, 1.0,
                                        -1.0, 1.0, 1.0,
                                        -1.0, -1.0, 1.0,
                                        1.0, -1.0, 1.0,
                                        //BACK FACE
                                        1.0, 1.0, -1.0,
                                        -1.0, 1.0, -1.0,
                                        -1.0, -1.0, -1.0,
                                        1.0, -1.0, -1.0,
                                        //RIGHT FACE
                                        1.0, 1.0, -1.0,
                                        1.0, 1.0, 1.0,
                                        1.0, -1.0, 1.0,
                                        1.0, -1.0, -1.0,
                                        //LEFT FACE
                                        -1.0, 1.0, -1.0,
                                        -1.0, 1.0, 1.0,
                                        -1.0, -1.0, 1.0,
                                        -1.0, -1.0, -1.0
    	]);

    	var cubeNormal = new Float32Array
    	([
    									0.0, 1.0, 0.0,
    									0.0, 1.0, 0.0,
    									0.0, 1.0, 0.0,
    									0.0, 1.0, 0.0,
                                       
                                        0.0, -1.0, 0.0,
                                        0.0, -1.0, 0.0,
                                        0.0, -1.0, 0.0,
                                        0.0, -1.0, 0.0,
                                       
                                        0.0, 0.0, 1.0,
                                        0.0, 0.0, 1.0,
                                        0.0, 0.0, 1.0,
                                        0.0, 0.0, 1.0,
                                       
                                        0.0, 0.0, -1.0,
                                        0.0, 0.0, -1.0,
                                        0.0, 0.0, -1.0,
                                        0.0, 0.0, -1.0,
                                       
                                        1.0, 0.0, 0.0,
                                        1.0, 0.0, 0.0,
                                        1.0, 0.0, 0.0,
                                        1.0, 0.0, 0.0,
                                       
                                        -1.0, 0.0, 0.0,
                                        -1.0, 0.0, 0.0,
                                        -1.0, 0.0, 0.0,
                                        -1.0, 0.0, 0.0,
        ]);


    vao_Cube = gl.createVertexArray();
    gl.bindVertexArray(vao_Cube);

    vbo_Cube_Position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Cube_Position);
    gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our triangleVertices array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_Cube_Normal = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Cube_Normal);
    gl.bufferData(gl.ARRAY_BUFFER, cubeNormal, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3,//3 is for X, Y, Z co-ordinates in our triangleVertices array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

	//set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);  //blue

	//Depth test will always be enabled
	gl.enable(gl.DEPTH_TEST);

	//Depth test to do
	gl.depthFunc(gl.LEQUAL);

	//we will always cull back face for better performance
	gl.enable(gl.CULL_FACE);

	//initialize projection matrix
	perspectiveProjectionMatrix=mat4.create();
}

function resize()
{
	//code
	if(bFullScreen == true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_orignal_width;
		canvas.height=canvas_orignal_height;
	}

	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function display()
{
	//code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	if(bLKeyIsPressed == true)
	{
		gl.uniform1i(lKeyIsPressedUniform, 1);

		//setting light properties
		gl.uniform3f(ldUniform, 1.0, 1.0, 1.0); //diffuse intensity of light
		//setting material properties
		gl.uniform3f(kdUniform, 0.5, 0.5, 0.5); //diffuse reflectivity of material

		var lightPosition = [0.0, 0.0, 2.0, 1.0];
		gl.uniform4f(lightPositionUniform, lightPosition); // light position
	}
	else
	{
		gl.uniform1i(lKeyIsPressedUniform, 0);
	}

	var modelViewMatrix=mat4.create(); //itself creates identity matrix

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -4.0]);

	mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRad(angle));
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angle));
	mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angle));

	mat4.multiply(modelViewProjectionMatrix, orthoGraphicProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(modelViewMatrixUniform, false, modelViewMatrix);
	gl.uniformMatrix4fv(ProjectionMatrixUniform, false, perspectiveProjectionMatrix);
	
	gl.bindVertexArray(vao_Cube);

	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);

	gl.bindVertexArray(null);

	gl.useProgram(null);

	angle = angle+2.0;
	if(angle>=360.0)
	{
		angle=angle-360.0;
	}

	//animation loop
	requestAnimationFrame(display, canvas);
}

function uninitialize()
{
	//code
	if(vao_Cube)
	{
		gl.deleteVertexArray(vao_Cube);
		vao_Cube=null;
	}

	if(vbo_Cube_Normal)
	{
		gl.deleteBuffer(vbo_Cube_Normal);
		vbo_Cube_Normal=null;
	}

	if(vbo_Cube_Position)
	{
		gl.deleteBuffer(vbo_Cube_Position);
		vbo_Cube_Position=null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:  // Escape
				//uninitialize
				uninitialize();
				//close our application's tab
				window.close();  //may not work in firefox but works in safari and chrome
				break;

		case 70: //for 'F' or 'f'
				toggleFullScreen();
				break;

		case 76:  //for 'L' or 'l'
				if(bLKeyIsPressed == false)
				{
					bLKeyIsPressed = true;
				}
				else
				{
					bLKeyIsPressed = false;
				}
				break;


	}
}

function mouseDown()
{
	//code
}

function degToRed(degree)
{
	//code
	return(degree * Math.PI / 180);
}
