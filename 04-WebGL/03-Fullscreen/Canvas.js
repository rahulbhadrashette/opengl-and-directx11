//global variable
var canvas = null;
var context = null;

//onload function
function main()
{
	//get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed...!!!\n");
	else
		console.log("Obtaining Canvas Succeeded...!!!\n");

	//print canvas width and height on console
	console.log("Canvas Width : "+canvas.width+"And Canvas Height : "+canvas.height);

	context=canvas.getContext("2d");
	if(!context)
		console.log("Obtaining 2D Context Failed...!!!\n");
	else
		console.log("Obtaining 2D Context Succeeded...!!!\n");

	//fill canvas with black color
	context.fillStyle="black";
	context.fillRect(0,0,canvas.width,canvas.height)

	//draw text
	drawText("Hello World...!!!");

	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
}

function drawText(text)
{
	console.log(" drawText...!!!\n");
	//code
	//center the text
	context.textAlign="center"; //center horizentally
	context.textBaseline="middle"; //center vertically

	//text font
	context.font="72px sans-serif";

	//text color
	context.fillStyle="white";

	//display the text in center
	context.fillText(text,canvas.width/2,canvas.height/2);
}

function toggleFullScreen()
{
	console.log("toggleFullScreen...!!!\n");
	//code
	var fullscreen_element = document.fullscreenelement ||
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;

	// if not fullScreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
	}
}

function keyDown(event)
{
	console.log("keyDown...!!!\n");
	//code
	switch(event.keyCode)
	{
		case 70: //for 'F' or 'f'
		toggleFullScreen();

		//repaint
		drawText("Hello World...!!!");
		break;
	}
}

function mouseDown()
{
	//code
}
