// global variables
var canvas=null;
var gl=null;
var bFullscreen = false;

var canvas_original_width;
var canvas_original_height;

const WebGLMacros = {
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var angleCube = 0.0;

var modelViewMatrixUniform, projectionMatrixUniform;
var ldUniform, kdUniform, lightPositionUniform;
var lKeyIsPressedUniform;

var blKeyPressed = false ; // for lights

var perspectiveProjectionMatrix;

//to start animation
var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame;


function main() 
{
	canvas = document.getElementById('AMC')
	if(!canvas) {
		console.log("Obtaining canvas failed.\n");
	} else {
		console.log("Obtaining canvas succeeded.\n");
	}

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	//print canvas height and width
	console.log('Canvas width : '+canvas.width +' And canvas height : ' + canvas.height);

	window.addEventListener('keydown', keydown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    init();

    resize();
    draw();
}

function compileShader(shader, shaderType) {
	var shaderObject =  gl.createShader(shaderType);
	gl.shaderSource(shaderObject, shader);
	gl.compileShader(shaderObject);
	if(gl.getShaderParameter(shaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(shaderObject);
		if(error.length >0) {
			console.log(error);
			uninitialize();
		}
	}
	return shaderObject;
}

function init() 
{
	gl = canvas.getContext('webgl2');
	if(gl==null) {
		console.log("Failed to get rendering context..\n");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	var vertexShaderSourceCode = 
			"#version 300 es" +
            "\n" +
            "in vec4 vPosition;" +
            "in vec3 vNormal;" +
            "uniform mat4 u_mv_matrix;" +
            "uniform mat4 u_p_matrix;" +
            "uniform mediump int u_lKeyIsPressed;" +
            "uniform vec3 u_Ld;" +
            "uniform vec3 u_Kd;" +
            "uniform vec4 u_Light_Position;" +
            "out vec3 diffuse_Color;" +

            "void main(void)" +
            "{" +

                "if(u_lKeyIsPressed == 1)" +
                "{" +
                    "vec4 eye_coordinate = u_mv_matrix * vPosition;" +
                    "mat3 normalmatrix = mat3(transpose(inverse(u_mv_matrix)));" +
                    "vec3 tnorm = normalize(normalmatrix * vNormal);" +
                    "vec3 source = normalize(vec3(u_Light_Position - eye_coordinate));" +
                    "diffuse_Color = u_Ld * u_Kd * max(dot(source, tnorm), 0.0);" +
                "}" +
                "gl_Position = u_p_matrix * u_mv_matrix * vPosition;" +

            "}";
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false);
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	var fragmentShaderSourceCode = 
			"#version 300 es" +
            "\n" +
            "precision highp float;" +
            "in vec3 diffuse_Color;" +
            "out vec4 fragColor;" +
            "uniform int u_lKeyIsPressed;" +

            "void main(void)" +
            "{" +
                
                "if(u_lKeyIsPressed == 1)" +
                "{" +
                    "fragColor = vec4(diffuse_Color, 1.0);" +
                "}" +
                "else" +
                "{" +
                    "fragColor = vec4(1.0, 1.0, 1.0, 1.0);" +
                "}" +

            "}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false);
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject = gl.createProgram();

	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");

		//linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	modelViewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_mv_matrix");
	projectionMatrixUniform  = gl.getUniformLocation(shaderProgramObject, "u_p_matrix");
	ldUniform = gl.getUniformLocation(shaderProgramObject, "u_Ld");
	kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_Light_Position");
	lKeyIsPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_lKeyIsPressed");

	sphere=new Mesh();
    makeSphere(sphere,2.0,30,30);

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.enable(gl.CULL_FACE);

	gl.clearColor(0.0,0.0,0.0, 1.0);

	perspectiveProjectionMatrix = mat4.create();

}

function resize() 
{
	if(bFullscreen==true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	} else {
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	console.log("In resize	 : "+ canvas.width);
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat(canvas.width)/parseFloat(canvas.height)), 0.1, 100.0);
}

function draw() 
{
	gl.clear(gl.COLOR_BUFFER_BIT);
	gl.useProgram(shaderProgramObject)
	
	if(blKeyPressed == true)
	{
		gl.uniform1i(lKeyIsPressedUniform, 1);
		//setting light properties
		gl.uniform3f(ldUniform, 1.0, 1.0, 1.0);// diffuse intensity of light
		//setting material properties
		gl.uniform3f(kdUniform, 0.5, 0.5, 0.5); //diffuse reflectivity of material
		var lightPosition = [0.0, 0.0, 2.0, 1.0];
		gl.uniform4fv(lightPositionUniform, lightPosition); //light position 
	}
	else
	{
		gl.uniform1i(lKeyIsPressedUniform, 0);
	}

    //For Rectangle
	var modelViewMatrix = mat4.create(); //itself create identity matrix

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -6.0]);

	gl.uniformMatrix4fv(modelViewMatrixUniform, false, modelViewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);


	sphere.draw();

	gl.useProgram(null);
	requestAnimationFrame (draw, canvas);

}

function uninitialize() 
{

	if(sphere)
    {
        sphere.deallocate();
        sphere=null;
    }

	if(shaderProgramObject) {
		if(vertexShaderObject) {
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		if(fragmentShaderObject) {
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
	}

	gl.deleteProgram(shaderProgramObject);
	shaderProgramObject = null;
}

function keydown(event) 
{
	switch(event.keyCode)
	{
		case 27:  // Escape
				//uninitialize
				uninitialize();
				//close our application's tab
				window.close();  //may not work in firefox but works in safari and chrome
				break;

		case 76:  //for 'l' Or 'L'--> Light
				if(blKeyPressed == false)
				{
					blKeyPressed = true;
				}
				else
				{
					blKeyPressed = false;
				}
				break;

		case 70: //for 'F' or 'f'
		toggleFullScreen();
		break;
	}
}

function mouseDown() {
	//alert('mouse is clicked.');
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenelement ||
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;

	// if not fullScreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
			bFullScreen = false;
	}
}
