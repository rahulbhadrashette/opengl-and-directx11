//global variable
var canvas = null;
var gl=null; //For WebGL Context
var bFullScreen=false;
var canvas_orignal_width;
var canvas_orignal_height;

const WebGLMacros=//when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE:3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var angle = 0.0;

var vao_Pyramid;
var vbo_Position_Pyramid;
var vbo_Normal_Pyramid;

var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
var laUniform_Red, ldUniform_Red, lsUniform_Red, lightPositionUniform_Red;
var laUniform_Blue, ldUniform_Blue, lsUniform_Blue, lightPositionUniform_Blue;
var kaUniform, kdUniform, ksUniform, materialShininessUniform;
var lKeyIsPressedUniform;

var blKeyPressed = false;
var bAnimation = false;

var lightAmbient_Red = [0.0, 0.0, 0.0];
var lightDiffuse_Red = [1.0, 0.0, 0.0,];
var lightSpecular_Red = [1.0, 0.0, 0.0];
var lightPosition_Red = [-2.0, 0.0, 0.0, 1.0];

var lightAmbient_Blue = [0.0, 0.0, 0.0];
var lightDiffuse_Blue = [0.0, 0.0, 1.0,];
var lightSpecular_Blue = [0.0, 0.0, 1.0];
var lightPosition_Blue = [2.0, 0.0, 0.0, 1.0];

var materialAmbient = [0.0, 0.0, 0.0]; //Ka
var materialDiffuse = [1.0, 1.0, 1.0];  //Kd
var materialSpecular = [1.0, 1.0, 1.0];  //Ks
var materialShininess = [50.0]; //128.0f;


var perspectiveProjectionMatrix;

//To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

//To stop animation : To have cancleAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;;

//onload function
function main()
{
	//get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed...!!!\n");
	else
		console.log("Obtaining Canvas Succeeded...!!!\n");

	canvas_orignal_width = canvas.width;
	canvas_orignal_height = canvas.height;
	//print canvas width and height on console
	console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//initialize WebGL
	init();

	//start drawing here as warning-up
	resize();
	display();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenelement ||
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;

	// if not fullScreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
			bFullScreen = false;
	}
}

function init()
{
	//code
	//get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if(gl==null) //failed to get context
	{
		console.log("Failed to get the rendering context for WebGL...!!!");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	var vertexShaderSourceCode=
			"#version 300 es" +
            "\n" +
            "in vec4 vertexPosition;" +
			"in vec3 vertexNormal;" +

			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projection_matrix;" +

			"uniform vec3 u_La_Red;" +
			"uniform vec3 u_Ld_Red;" +
			"uniform vec3 u_Ls_Red;" +

			"uniform vec3 u_La_Blue;" +
			"uniform vec3 u_Ld_Blue;" +
			"uniform vec3 u_Ls_Blue;" +

			"uniform vec3 u_Ka;" +
			"uniform vec3 u_Kd;" +
			"uniform vec3 u_Ks;" +

			"uniform float u_materialShininess;" +
			"uniform mediump int u_lKeyIsPressed;" +
			"uniform vec4 u_Light_Position_Red;" +
			"uniform vec4 u_Light_Position_Blue;" +

			"out vec3 phong_AdsLight;" +

			"void main(void)" +
			"{" +
				"if(u_lKeyIsPressed == 1)" +
				"{" +
					"vec4 eye_coordinate = u_viewMatrix * u_modelMatrix * vertexPosition;" +

					"vec3 tnorm = normalize(mat3(u_viewMatrix * u_modelMatrix) * vertexNormal);" +
					//For Red Light
					"vec3 lightdirection_Red = normalize(vec3(u_Light_Position_Red - eye_coordinate));" +

					"float tn_dot_lightDir_Red = max(dot(lightdirection_Red, tnorm), 0.0);" +

					"vec3 reflectionVector_Red = reflect(-lightdirection_Red, tnorm);" +

					"vec3 viewerVector = normalize(vec3(-eye_coordinate.xyz));" +

					"vec3 ambient_Red = u_La_Red * u_Ka;" +

					"vec3 diffuse_Red = u_Ld_Red * u_Kd * tn_dot_lightDir_Red;" +

					"vec3 specular_Red = u_Ls_Red * u_Ks * pow(max(dot(reflectionVector_Red, viewerVector), 0.0), u_materialShininess);" +

					//For Blue Light
					"vec3 lightdirection_Blue = normalize(vec3(u_Light_Position_Blue - eye_coordinate));" +

					"float tn_dot_lightDir_Blue = max(dot(lightdirection_Blue, tnorm), 0.0);" +

					"vec3 reflectionVector_Blue = reflect(-lightdirection_Blue, tnorm);" +

					"vec3 ambient_Blue = u_La_Blue * u_Ka;" +

					"vec3 diffuse_Blue = u_Ld_Blue * u_Kd * tn_dot_lightDir_Blue;" +

					"vec3 specular_Blue = u_Ls_Blue * u_Ks * pow(max(dot(reflectionVector_Blue, viewerVector), 0.0), u_materialShininess);" +

					"phong_AdsLight = ambient_Red + diffuse_Red + specular_Red + ambient_Blue + diffuse_Blue + specular_Blue;" +

					"}" +
					"else" +
					"{" +
							"phong_AdsLight = vec3(1.0, 1.0, 1.0);" +
					"}" +
					"gl_Position = u_projection_matrix * u_viewMatrix * u_modelMatrix * vertexPosition;" +
				"}";


		vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
		gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
		gl.compileShader(vertexShaderObject);
		if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false);
		{
			var error=gl.getShaderInfoLog(vertexShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

	 //Write Fragment Shader Code
     var fragmentShaderSourceCode =
    		"#version 300 es" +
            "\n" +
            "precision highp float;" +
            "in vec3 phong_AdsLight;" +
			"out vec4 fragColor;" +
			"void main(void)" +
			"{" +
				"fragColor = vec4(phong_AdsLight, 1.0);" +
			"}";

		fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
		gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		gl.compileShader(fragmentShaderObject);
		if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false);
		{
			var error=gl.getShaderInfoLog(fragmentShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

		//shader program
		shaderProgramObject=gl.createProgram();
		gl.attachShader(shaderProgramObject, vertexShaderObject);
		gl.attachShader(shaderProgramObject, fragmentShaderObject);

		//pre-link binding of shader program object with vertex shader attribute
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vertexPosition");
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vertexNormal");

		//linking
		gl.linkProgram(shaderProgramObject);
		if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
		{
			var error = gl.getProgramInfoLog(shaderProgramObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_modelMatrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_viewMatrix");
	projectionMatrixUniform  = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");

	laUniform_Red = gl.getUniformLocation(shaderProgramObject, "u_La_Red");
	ldUniform_Red = gl.getUniformLocation(shaderProgramObject, "u_Ld_Red");
	lsUniform_Red = gl.getUniformLocation(shaderProgramObject, "u_Ls_Red");
	lightPositionUniform_Red = gl.getUniformLocation(shaderProgramObject, "u_Light_Position_Red");

	laUniform_Blue = gl.getUniformLocation(shaderProgramObject, "u_La_Blue");
	ldUniform_Blue = gl.getUniformLocation(shaderProgramObject, "u_Ld_Blue");
	lsUniform_Blue = gl.getUniformLocation(shaderProgramObject, "u_Ls_Blue");
	lightPositionUniform_Blue = gl.getUniformLocation(shaderProgramObject, "u_Light_Position_Blue");

	kaUniform = gl.getUniformLocation(shaderProgramObject, "u_Ka");
	kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
	ksUniform = gl.getUniformLocation(shaderProgramObject, "u_Ks");
	materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_materialShininess");

	lKeyIsPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_lKeyIsPressed");

    var pyramidVertices = new Float32Array
    	([
    		//FRONT FACE
			0.0, 1.0, 0.0,
			-1.0, -1.0, 1.0,
			1.0, -1.0, 1.0,
			 //LEFT FACE
			0.0, 1.0, 0.0,
			1.0, -1.0, 1.0,
			1.0, -1.0, -1.0,
			//BACK FACE
			0.0, 1.0, 0.0,
			1.0, -1.0, -1.0,
			-1.0, -1.0, -1.0,
			 //RIGHT FACE
			0.0, 1.0, 0.0,
			-1.0, -1.0, -1.0,
			-1.0, -1.0, 1.0
    	]);

    var pyramidNormal = new Float32Array
    	([
    		0.0, 0.447214, 0.894427,
			0.0, 0.447214, 0.894427,
			0.0, 0.447214, 0.894427,

			0.89427, 0.447214, 0.0,
			0.89427, 0.447214, 0.0,
			0.89427, 0.447214, 0.0,

			0.0, 0.447214, -0.894427,
			0.0, 0.447214, -0.894427,
			0.0, 0.447214, -0.894427,
			-0.894427, 0.447214, 0.0,
			-0.894427, 0.447214, 0.0,
			-0.894427, 0.447214, 0.0
    	]);


    vao_Pyramid = gl.createVertexArray();
    gl.bindVertexArray(vao_Pyramid);

    vbo_Position_Pyramid = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Position_Pyramid);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our pyramidVertices array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_Normal_Pyramid = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Normal_Pyramid);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidNormal, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3,//3 is for X, Y, Z co-ordinates in our pyramidNormal array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

	//set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);  //blue

	//Depth test will always be enabled
	gl.enable(gl.DEPTH_TEST);

	//depth test to do 
	gl.depthFunc(gl.LEQUAL);

	////We will always cill back faces for better performance
	gl.enable(gl.CULL_FACE);

	//initialize projection matrix
	perspectiveProjectionMatrix=mat4.create();

}

function resize()
{
	//code
	if(bFullScreen == true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_orignal_width;
		canvas.height=canvas_orignal_height;
	}

	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	//Perspective Graphic Projection => left, rigth, bottom, top
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function display()
{
	//code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	if(blKeyPressed == true)
	{
		gl.uniform1i(lKeyIsPressedUniform, 1);
		gl.uniform3fv(laUniform_Red, lightAmbient_Red);
		gl.uniform3fv(ldUniform_Red, lightDiffuse_Red);
		gl.uniform3fv(lsUniform_Red, lightSpecular_Red);
		gl.uniform4fv(lightPositionUniform_Red, lightPosition_Red);

		gl.uniform3fv(laUniform_Blue, lightAmbient_Blue);
		gl.uniform3fv(ldUniform_Blue, lightDiffuse_Blue);
		gl.uniform3fv(lsUniform_Blue, lightSpecular_Blue);
		gl.uniform4fv(lightPositionUniform_Blue, lightPosition_Blue);

		gl.uniform3fv(kaUniform, materialAmbient);
		gl.uniform3fv(kdUniform, materialDiffuse);
		gl.uniform3fv(ksUniform, materialSpecular);
		gl.uniform1fv(materialShininessUniform, materialShininess);
	}
	else
	{
		gl.uniform1i(lKeyIsPressedUniform, 0);
	}

	//For Rectangle
	var modelMatrix = mat4.create(); //itself create identity matrix
	var viewMatrix = mat4.create();

	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -4.0]);
	mat4.rotateY(modelMatrix, modelMatrix, degToRad(angle));

	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);

	gl.bindVertexArray(vao_Pyramid);

	gl.drawArrays(gl.TRIANGLES, 0, 12);
	
	gl.bindVertexArray(null);

	gl.useProgram(null);

	if(bAnimation)
	{
		angle = angle+1.0;
		if(angle>=360.0)
		{
			angle = angle - 360.0;
		}
	}
	//animation loop
	requestAnimationFrame(display, canvas);
}

function uninitialize()
{
	//code

	if(vbo_Normal_Pyramid)
	{
		gl.deleteVertexArray(vbo_Normal_Pyramid);
		vbo_Normal_Pyramid=null;
	}

	if(vbo_Position_Pyramid)
	{
		gl.deleteVertexArray(vbo_Position_Pyramid);
		vbo_Position_Pyramid=null;
	}

	if(vao_Pyramid)
	{
		gl.deleteVertexArray(vao_Pyramid);
		vao_Pyramid=null;
	}


	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:  // Escape
				//uninitialize
				uninitialize();
				//close our application's tab
				window.close();  //may not work in firefox but works in safari and chrome
				break;

		case 76:  //for 'l' Or 'L'--> Light
				if(blKeyPressed == false)
				{
					blKeyPressed = true;
				}
				else
				{
					blKeyPressed = false;
				}
				break;

		case 65: //For Animation
			   bAnimation = !bAnimation;
			   break;

		case 70: //for 'F' or 'f'
				toggleFullScreen();
				break;
	}
}

function mouseDown()
{
	//code
}

function degToRad(degrees)
{
	//code
	return(degrees * Math.PI / 180);
}

