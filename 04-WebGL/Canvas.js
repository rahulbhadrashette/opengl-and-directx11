// global variables
var canvas=null;
var gl=null;
var bFullscreen = false;

var canvas_original_width;
var canvas_original_height;

const WebGLMacros = {
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3
};

var angle = 0.0;
var Co1 = 0.0;
var Co2 = 0.0;

var lightAmbient_Red = new Float32Array(4);
var lightDiffuse_Red = new Float32Array(4);
var lightPosition_Red = new Float32Array(4);

var materialAmbient =  new Array(24);
var materialDiffuse =  new Array(24);
var materialSpecular =  new Array(24);
var materialShininess =  new Array(24);

/*for(var i = 0; i < 24; i++)
{
	materialAmbient =  new Array(4);
 	materialDiffuse =  new Array(4);
 	materialSpecular =  new Array(4);
	materialShininess =  new Array(1);
}*/
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject_PV;
var shaderProgramObject_PF;
//***********************************************
//For Vertex shader Variables
var model_matrix_uniform;
var view_matrix_uniform;
var projection_matrix_uniform;

var Ka_uniform;
var Kd_uniform;
var Ks_uniform;
var material_shininess_uniform;
var light_enable_uniform;

var La_uniform_Red;
var Ld_uniform_Red;
var Ls_uniform_Red;
var light_position_uniform_Red;

//********************************************
//For fragment Shader Variables
var model_matrix_uniform_RMB;
var view_matrix_uniform_RMB;
var projection_matrix_uniform_RMB;

var Ka_uniform_RMB;
var Kd_uniform_RMB;
var Ks_uniform_RMB;
var material_shininess_uniform_RMB;

var La_uniform_Red_RMB;
var Ld_uniform_Red_RMB;
var Ls_uniform_Red_RMB;
var light_position_uniform_Red_RMB;

//********************************************
var light_enable_uniform_RMB;
var blKeyPressed = false ; // for lights
var fragmentShaderladder = false;

var perspectiveProjectionMatrix;

var iWinWidth;
var iWinHeight;
//********************************************
//to start animation
var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame;


function main() 
{
	canvas = document.getElementById('AMC')
	if(!canvas) {
		console.log("Obtaining canvas failed.\n");
	} else {
		console.log("Obtaining canvas succeeded.\n");
	}

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	//print canvas height and width
	console.log('Canvas width : '+canvas.width +' And canvas height : ' + canvas.height);

	window.addEventListener('keydown', keydown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    iWinWidth = window.innerWidth;
    iWinHeight = window.innerHeight;

    init();

    resize();
    draw();
}

function compileShader(shader, shaderType) {
	var shaderObject =  gl.createShader(shaderType);
	gl.shaderSource(shaderObject, shader);
	gl.compileShader(shaderObject);
	if(gl.getShaderParameter(shaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(shaderObject);
		if(error.length >0) {
			console.log(error);
			uninitialize();
		}
	}
	return shaderObject;
}

function init() 
{
	gl = canvas.getContext('webgl2');
	if(gl==null) {
		console.log("Failed to get rendering context..\n");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;


	vertexShader();
	fragmentShader() 

	sphere=new Mesh();
    makeSphere(sphere,2.0,30,30);

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.enable(gl.CULL_FACE);

	gl.clearColor(0.25,0.25,0.25, 1.0);

	perspectiveProjectionMatrix = mat4.create();

}

function resize() 
{
	if(bFullscreen==true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	} else {
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	console.log("In resize	 : "+ canvas.width);
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat(canvas.width)/parseFloat(canvas.height)), 0.1, 100.0);
}

function draw() 
{
	light_Array();
	gl.clear(gl.COLOR_BUFFER_BIT);
	//console.log('Co-Ordinate : '+Co1 +' And Co-Ordinate : ' + Co2);
	for(var i = 0; i < 24; i++)
	{
		gl.viewport((i % 6) * iWinWidth / 6, iWinHeight - (i / 6 + 1) * iWinHeight / 4, (int)iWinWidth / 6, (int)iWinHeight / 4);

		mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat(iWinWidth / 6)/parseFloat(iWinHeight) / 4), 0.1, 100.0);

		console.log('Canvas width : '+iWinWidth +' And canvas height : ' +iWinHeight);
		if(fragmentShaderladder == true)
		{
			gl.useProgram(shaderProgramObject_PF);

			if(blKeyPressed)
		    {
		        gl.uniform1i(light_enable_uniform_RMB, 1);
		        //For Red Color Lighting
		        gl.uniform3fv(La_uniform_Red_RMB, lightAmbient_Red);
		        gl.uniform3fv(Ld_uniform_Red_RMB, lightDiffuse_Red);
		        gl.uniform4fv(light_position_uniform_Red_RMB, lightPosition_Red);

		        for(var j = 0; j < 4; j++)
		        {
		        	gl.uniform3fv(Ka_uniform_RMB, materialAmbient[i][j]);
			        gl.uniform3fv(Kd_uniform_RMB, materialDiffuse[i][j]);
			        gl.uniform3fv(Ks_uniform_RMB, materialSpecular[i][j]);
			        gl.uniform1f(material_shininess_uniform_RMB, materialShininess[i][0]);
		        }
		    }
		    else
		    {
		        gl.uniform1i(light_enable_uniform_RMB, 0);
		    }
		}
		else
		{
			gl.useProgram(shaderProgramObject_PV)

			if(blKeyPressed)
		    {
		        gl.uniform1i(light_enable_uniform, 1);
		        //For Red Color Lighting
		        gl.uniform3fv(La_uniform_Red, lightAmbient_Red);
		        gl.uniform3fv(Ld_uniform_Red, lightDiffuse_Red);
		        gl.uniform4fv(light_position_uniform_Red, lightPosition_Red);
		        for(var j = 0; j < 4; j++)
		        {
			        gl.uniform3fv(Ka_uniform, materialAmbient[i][j]);
			        gl.uniform3fv(Kd_uniform, materialDiffuse[i][j]);
			        gl.uniform3fv(Ks_uniform, materialSpecular[i][j]);
			        gl.uniform1f(material_shininess_uniform, materialShininess[i][0]);
		    	}
		    }
		    else
		    {
		        gl.uniform1i(light_enable_uniform, 0);
		    }
		}
		

	    var modelMatrix = mat4.create();
	    var viewMatrix = mat4.create();
		
		mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);

		gl.uniformMatrix4fv(model_matrix_uniform, false, modelMatrix);
		gl.uniformMatrix4fv(view_matrix_uniform, false, viewMatrix);
		gl.uniformMatrix4fv(projection_matrix_uniform, false, perspectiveProjectionMatrix);

		gl.uniformMatrix4fv(model_matrix_uniform_RMB, false, modelMatrix);
		gl.uniformMatrix4fv(view_matrix_uniform_RMB, false, viewMatrix);
		gl.uniformMatrix4fv(projection_matrix_uniform_RMB, false, perspectiveProjectionMatrix);

		sphere.draw();

	}
	
	gl.useProgram(null);

	angle = angle + 0.007;
	if(angle>=360.0)
	{
		angle = angle - 360.0;
	}
	
	Co1 = Math.cos(angle) * 100.0;
	Co2 = Math.sin(angle) * 100.0;

	requestAnimationFrame (draw, canvas);

}

function light_Array()
{
	//********** For Red Color Lighting Array ********
        lightAmbient_Red[0] = 0.0;
        lightAmbient_Red[1] = 0.0;
        lightAmbient_Red[2] = 0.0;
        lightAmbient_Red[3] = 1.0;

        lightDiffuse_Red[0] = 1.0;
        lightDiffuse_Red[1] = 1.0;
        lightDiffuse_Red[2] = 1.0;
        lightDiffuse_Red[3] = 1.0;

        //if(keyIncrement == 1)
        //{
            lightPosition_Red[0] = co1;
            lightPosition_Red[1] = 0.0;
            lightPosition_Red[2] = co2;
            lightPosition_Red[3] = 0.0;
        /*}
        else if(keyIncrement == 2)
        {
            lightPosition_Red[0] = co2;
            lightPosition_Red[1] = co1;
            lightPosition_Red[2] = 0.0;
            lightPosition_Red[3] = 0.0;
        }
        else
        {
            lightPosition_Red[0] = 0.0;
            lightPosition_Red[1] = co1;
            lightPosition_Red[2] = co2;
            lightPosition_Red[3] = 0.0;
        }*/

        // *** 1st Sphere On 1st Column, Emerald ****
        materialAmbient[0][0] = 0.0215;
        materialAmbient[0][1] = 0.1745;
        materialAmbient[0][2] = 0.0215;
        materialAmbient[0][3] = 1.0;

        materialDiffuse[0][0] = 0.07568;
        materialDiffuse[0][1] = 0.61424;
        materialDiffuse[0][2] = 0.07568;
        materialDiffuse[0][3] = 1.0;

        materialSpecular[0][0] = 0.633;
        materialSpecular[0][1] = 0.727811;
        materialSpecular[0][2] = 0.633;
        materialSpecular[0][3] = 1.0;

        materialShininess[0][0] = 0.6 * 128.0;

        // *** 2nd Sphere On 1st Column, Jade ****
        materialAmbient[1][0] = 0.135;
        materialAmbient[1][1] = 0.2225;
        materialAmbient[1][2] = 0.1575;
        materialAmbient[1][3] = 1.0;

        materialDiffuse[1][0] = 0.54;
        materialDiffuse[1][1] = 0.89;
        materialDiffuse[1][2] = 0.63;
        materialDiffuse[1][3] = 1.0;

        materialSpecular[1][0] = 0.316228;
        materialSpecular[1][1] = 0.316228;
        materialSpecular[1][2] = 0.316228;
        materialSpecular[1][3] = 1.0;

        materialShininess[1][0] = 0.1 * 128.0;

        // *** 3rd Sphere On 1st Column, Obsidian ****
        materialAmbient[2][0] = 0.05375;
        materialAmbient[2][1] = 0.05;
        materialAmbient[2][2] = 0.06625;
        materialAmbient[2][3] = 1.0;

        materialDiffuse[2][0] = 0.18275;
        materialDiffuse[2][1] = 0.17;
        materialDiffuse[2][2] = 0.22525;
        materialDiffuse[2][3] = 1.0;

        materialSpecular[2][0] = 0.332741;
        materialSpecular[2][1] = 0.328634;
        materialSpecular[2][2] = 0.346435;
        materialSpecular[2][3] = 1.0;

        materialShininess[2][0] = 0.3 * 128.0;

        // *** 4th Sphere On 1st Column, Pearl ****
        materialAmbient[3][0] = 0.25;
        materialAmbient[3][1] = 0.20725;
        materialAmbient[3][2] = 0.20725;
        materialAmbient[3][3] = 1.0;

        materialDiffuse[3][0] = 1.0;
        materialDiffuse[3][1] = 0.829;
        materialDiffuse[3][2] = 0.829;
        materialDiffuse[3][3] = 1.0;

        materialSpecular[3][0] = 0.296648;
        materialSpecular[3][1] = 0.296648;
        materialSpecular[3][2] = 0.296648;
        materialSpecular[3][3] = 1.0;

        materialShininess[3][0] = 0.088 * 128.0;

        // *** 5th Sphere On 1st Column, Ruby ****
        materialAmbient[4][0] = 0.1745;
        materialAmbient[4][1] = 0.01175;
        materialAmbient[4][2] = 0.01175;
        materialAmbient[4][3] = 1.0;

        materialDiffuse[4][0] = 0.61424;
        materialDiffuse[4][1] = 0.04136;
        materialDiffuse[4][2] = 0.04136;
        materialDiffuse[4][3] = 1.0;

        materialSpecular[4][0] = 0.727811;
        materialSpecular[4][1] = 0.686959;
        materialSpecular[4][2] = 0.626959;
        materialSpecular[4][3] = 1.0;

        materialShininess[4][0] = 0.6 * 128.0;

        // *** 6th Sphere On 1st Column, Tarquoise ****
        materialAmbient[5][0] = 0.1;
        materialAmbient[5][1] = 0.18725;
        materialAmbient[5][2] = 0.1745;
        materialAmbient[5][3] = 1.0;

        materialDiffuse[5][0] = 0.396;
        materialDiffuse[5][1] = 0.74151;
        materialDiffuse[5][2] = 0.69102;
        materialDiffuse[5][3] = 1.0;

        materialSpecular[5][0] = 0.297254;
        materialSpecular[5][1] = 0.30829;
        materialSpecular[5][2] = 0.306678;
        materialSpecular[5][3] = 1.0;

        materialShininess[5][0] = 0.1 * 128.0;

        // *** 1st Sphere On 2nd Column, Brass ****
        materialAmbient[6][0] = 0.329412;
        materialAmbient[6][1] = 0.223529;
        materialAmbient[6][2] = 0.027451;
        materialAmbient[6][3] = 1.0;

        materialDiffuse[6][0] = 0.780392;
        materialDiffuse[6][1] = 0.568627;
        materialDiffuse[6][2] = 0.113725;
        materialDiffuse[6][3] = 1.0;

        materialSpecular[6][0] = 0.992157;
        materialSpecular[6][1] = 0.941176;
        materialSpecular[6][2] = 0.807843;
        materialSpecular[6][3] = 1.0;

        materialShininess[6][0] = 0.21794872 * 128.0;

        // *** 2nd Sphere On 2nd Column, Bronze ****
        materialAmbient[7][0] = 0.2125;
        materialAmbient[7][1] = 0.1275;
        materialAmbient[7][2] = 0.054;
        materialAmbient[7][3] = 1.0;

        materialDiffuse[7][0] = 0.714;
        materialDiffuse[7][1] = 0.4284;
        materialDiffuse[7][2] = 0.18144;
        materialDiffuse[7][3] = 1.0;

        materialSpecular[7][0] = 0.393548;
        materialSpecular[7][1] = 0.271906;
        materialSpecular[7][2] = 0.166721;
        materialSpecular[7][3] = 1.0;

        materialShininess[7][0] = 0.2 * 128.0;

        // *** 3rd Sphere On 2nd Column, Chrome ****
        materialAmbient[8][0] = 0.25;
        materialAmbient[8][1] = 0.25;
        materialAmbient[8][2] = 0.25;
        materialAmbient[8][3] = 1.0;

        materialDiffuse[8][0] = 0.4;
        materialDiffuse[8][1] = 0.4;
        materialDiffuse[8][2] = 0.4;
        materialDiffuse[8][3] = 1.0;

        materialSpecular[8][0] = 0.774597;
        materialSpecular[8][1] = 0.774597;
        materialSpecular[8][2] = 0.774597;
        materialSpecular[8][3] = 1.0;

        materialShininess[8][0] = 0.6 * 128.0;

        // *** 4th Sphere On 2nd Column, Copper ****
        materialAmbient[9][0] = 0.19125;
        materialAmbient[9][1] = 0.0735;
        materialAmbient[9][2] = 0.0225;
        materialAmbient[9][3] = 1.0;

        materialDiffuse[9][0] = 0.7038;
        materialDiffuse[9][1] = 0.27048;
        materialDiffuse[9][2] = 0.0828;
        materialDiffuse[9][3] = 1.0;

        materialSpecular[9][0] = 0.256777;
        materialSpecular[9][1] = 0.137622;
        materialSpecular[9][2] = 0.086014;
        materialSpecular[9][3] = 1.0;

        materialShininess[9][0] = 0.1 * 128.0;

        // *** 5th Sphere On 2nd Column, Gold ****
        materialAmbient[10][0] = 0.24725;
        materialAmbient[10][1] = 0.1995;
        materialAmbient[10][2] = 0.0745;
        materialAmbient[10][3] = 1.0;

        materialDiffuse[10][0] = 0.75164;
        materialDiffuse[10][1] = 0.60648;
        materialDiffuse[10][2] = 0.22648;
        materialDiffuse[10][3] = 1.0;

        materialSpecular[10][0] = 0.628281;
        materialSpecular[10][1] = 0.555802;
        materialSpecular[10][2] = 0.366065;
        materialSpecular[10][3] = 1.0;

        materialShininess[10][0] = 0.4 * 128.0;

        // *** 6th Sphere On 2nd Column, Silver ****
        materialAmbient[11][0] = 0.19225;
        materialAmbient[11][1] = 0.19225;
        materialAmbient[11][2] = 0.19225;
        materialAmbient[11][3] = 1.0;

        materialDiffuse[11][0] = 0.50754;
        materialDiffuse[11][1] = 0.50754;
        materialDiffuse[11][2] = 0.50754;
        materialDiffuse[11][3] = 1.0;

        materialSpecular[11][0] = 0.508273;
        materialSpecular[11][1] = 0.508273;
        materialSpecular[11][2] = 0.508273;
        materialSpecular[11][3] = 1.0;

        materialShininess[11][0] = 0.4 * 128.0;

        // *** 1st Sphere On 3rd Column, Black ****
        materialAmbient[12][0] = 0.0;
        materialAmbient[12][1] = 0.0;
        materialAmbient[12][2] = 0.0;
        materialAmbient[12][3] = 1.0;

        materialDiffuse[12][0] = 0.01;
        materialDiffuse[12][1] = 0.01;
        materialDiffuse[12][2] = 0.01;
        materialDiffuse[12][3] = 1.0;

        materialSpecular[12][0] = 0.50;
        materialSpecular[12][1] = 0.50;
        materialSpecular[12][2] = 0.50;
        materialSpecular[12][3] = 1.0;

        materialShininess[12][0] = 0.25 * 128.0;

        // *** 2nd Sphere On 3rd Column, Cyan ****
        materialAmbient[13][0] = 0.0;
        materialAmbient[13][1] = 0.1;
        materialAmbient[13][2] = 0.06;
        materialAmbient[13][3] = 1.0;

        materialDiffuse[13][0] = 0.0;
        materialDiffuse[13][1] = 0.50980392;
        materialDiffuse[13][2] = 0.50980392;
        materialDiffuse[13][3] = 1.0;

        materialSpecular[13][0] = 0.50196078;
        materialSpecular[13][1] = 0.50196078;
        materialSpecular[13][2] = 0.50196078;
        materialSpecular[13][3] = 1.0;

        materialShininess[13][0] = 0.25 * 128.0;

        // *** 3rd Sphere On 3rd Column, Green ****
        materialAmbient[14][0] = 0.0;
        materialAmbient[14][1] = 0.0;
        materialAmbient[14][2] = 0.0;
        materialAmbient[14][3] = 1.0;

        materialDiffuse[14][0] = 0.1;
        materialDiffuse[14][1] = 0.35;
        materialDiffuse[14][2] = 0.1;
        materialDiffuse[14][3] = 1.0;

        materialSpecular[14][0] = 0.45;
        materialSpecular[14][1] = 0.45;
        materialSpecular[14][2] = 0.45;
        materialSpecular[14][3] = 1.0;

        materialShininess[14][0] = 0.25 * 128.0;

        // *** 4th Sphere On 3rd Column, Red ****
        materialAmbient[15][0] = 0.0;
        materialAmbient[15][1] = 0.0;
        materialAmbient[15][2] = 0.0;
        materialAmbient[15][3] = 1.0;

        materialDiffuse[15][0] = 0.5;
        materialDiffuse[15][1] = 0.0;
        materialDiffuse[15][2] = 0.0;
        materialDiffuse[15][3] = 1.0;

        materialSpecular[15][0] = 0.7;
        materialSpecular[15][1] = 0.6;
        materialSpecular[15][2] = 0.6;
        materialSpecular[15][3] = 1.0;

        materialShininess[15][0] = 0.25 * 128.0;

        // *** 5th Sphere On 3rd Column, White ****
        materialAmbient[16][0] = 0.0;
        materialAmbient[16][1] = 0.0;
        materialAmbient[16][2] = 0.0;
        materialAmbient[16][3] = 1.0;

        materialDiffuse[16][0] = 0.55;
        materialDiffuse[16][1] = 0.55;
        materialDiffuse[16][2] = 0.55;
        materialDiffuse[16][3] = 1.0;

        materialSpecular[16][0] = 0.70;
        materialSpecular[16][1] = 0.70;
        materialSpecular[16][2] = 0.70;
        materialSpecular[16][3] = 1.0;

        materialShininess[16][0] = 0.25 * 128.0;

        // *** 6th Sphere On 3rd Column, Yello ****
        materialAmbient[17][0] = 0.0;
        materialAmbient[17][1] = 0.0;
        materialAmbient[17][2] = 0.0;
        materialAmbient[17][3] = 1.0;

        materialDiffuse[17][0] = 0.5;
        materialDiffuse[17][1] = 0.5;
        materialDiffuse[17][2] = 0.0;
        materialDiffuse[17][3] = 1.0;

        materialSpecular[17][0] = 0.60;
        materialSpecular[17][1] = 0.60;
        materialSpecular[17][2] = 0.50;
        materialSpecular[17][3] = 1.0;

        materialShininess[17][0] = 0.25 * 128.0;

        // *** 1st Sphere On 4th Column, Black ****
        materialAmbient[18][0] = 0.02;
        materialAmbient[18][1] = 0.02;
        materialAmbient[18][2] = 0.02;
        materialAmbient[18][3] = 1.0;

        materialDiffuse[18][0] = 0.01;
        materialDiffuse[18][1] = 0.01;
        materialDiffuse[18][2] = 0.01;
        materialDiffuse[18][3] = 1.0;

        materialSpecular[18][0] = 0.4;
        materialSpecular[18][1] = 0.4;
        materialSpecular[18][2] = 0.4;
        materialSpecular[18][3] = 1.0;

        materialShininess[18][0] = 0.078125 * 128.0;


        // *** 2nd Sphere On 4th Column, Cyan ****
        materialAmbient[19][0] = 0.0;
        materialAmbient[19][1] = 0.05;
        materialAmbient[19][2] = 0.05;
        materialAmbient[19][3] = 1.0;

        materialDiffuse[19][0] = 0.4;
        materialDiffuse[19][1] = 0.5;
        materialDiffuse[19][2] = 0.5;
        materialDiffuse[19][3] = 1.0;

        materialSpecular[19][0] = 0.04;
        materialSpecular[19][1] = 0.7;
        materialSpecular[19][2] = 0.7;
        materialSpecular[19][3] = 1.0;

        materialShininess[19][0] = 0.078125 * 128.0;

        // *** 3rd Sphere On 4th Column, Green ****
        materialAmbient[20][0] = 0.0;
        materialAmbient[20][1] = 0.05;
        materialAmbient[20][2] = 0.0;
        materialAmbient[20][3] = 1.0;

        materialDiffuse[20][0] = 0.4;
        materialDiffuse[20][1] = 0.5;
        materialDiffuse[20][2] = 0.4;
        materialDiffuse[20][3] = 1.0;

        materialSpecular[20][0] = 0.04;
        materialSpecular[20][1] = 0.7;
        materialSpecular[20][2] = 0.04;
        materialSpecular[20][3] = 1.0;

        materialShininess[20][0] = 0.078125 * 128.0;

        // *** 4th Sphere On 4th Column, Red ****
        materialAmbient[21][0] = 0.05;
        materialAmbient[21][1] = 0.0;
        materialAmbient[21][2] = 0.0;
        materialAmbient[21][3] = 1.0;

        materialDiffuse[21][0] = 0.5;
        materialDiffuse[21][1] = 0.4;
        materialDiffuse[21][2] = 0.4;
        materialDiffuse[21][3] = 1.0;

        materialSpecular[21][0] = 0.7;
        materialSpecular[21][1] = 0.04;
        materialSpecular[21][2] = 0.04;
        materialSpecular[21][3] = 1.0;

        materialShininess[21][0] = 0.078125 * 128.0;

        // *** 5th Sphere On 4th Column, White ****
        materialAmbient[22][0] = 0.05;
        materialAmbient[22][1] = 0.05;
        materialAmbient[22][2] = 0.05;
        materialAmbient[22][3] = 1.0;

        materialDiffuse[22][0] = 0.5;
        materialDiffuse[22][1] = 0.5;
        materialDiffuse[22][2] = 0.5;
        materialDiffuse[22][3] = 1.0;

        materialSpecular[22][0] = 0.7;
        materialSpecular[22][1] = 0.7;
        materialSpecular[22][2] = 0.7;
        materialSpecular[22][3] = 1.0;

        materialShininess[22][0] = 0.078125 * 128.0;

        // *** 6th Sphere On 4th Column, Yello ****
        materialAmbient[23][0] = 0.05;
        materialAmbient[23][1] = 0.05;
        materialAmbient[23][2] = 0.0;
        materialAmbient[23][3] = 1.0;

        materialDiffuse[23][0] = 0.5;
        materialDiffuse[23][1] = 0.5;
        materialDiffuse[23][2] = 0.4;
        materialDiffuse[23][3] = 1.0;

        materialSpecular[23][0] = 0.7;
        materialSpecular[23][1] = 0.7;
        materialSpecular[23][2] = 0.04;
        materialSpecular[23][3] = 1.0;

        materialShininess[23][0] = 0.078125 * 128.0;
}

function uninitialize() 
{

	if(sphere)
    {
        sphere.deallocate();
        sphere=null;
    }

	if(shaderProgramObject_PF) {
		if(vertexShaderObject) {
			gl.detachShader(shaderProgramObject_PF, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		if(fragmentShaderObject) {
			gl.detachShader(shaderProgramObject_PF, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
	}
	gl.deleteProgram(shaderProgramObject_PF);
	shaderProgramObject_PF = null;

	if(shaderProgramObject_PV) {
		if(vertexShaderObject) {
			gl.detachShader(shaderProgramObject_PV, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		if(fragmentShaderObject) {
			gl.detachShader(shaderProgramObject_PV, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
	}

	gl.deleteProgram(shaderProgramObject_PV);
	shaderProgramObject_PV = null;
}

function keydown(event) 
{
	switch(event.keyCode)
	{
		case 27:  // Escape
				toggleFullScreen();
				break;

		case 69:
				//uninitialize
				uninitialize();
				//close our application's tab
				window.close();  //may not work in firefox but works in safari and chrome
				break;

		case 76:  //for 'l' Or 'L'--> Light
				if(blKeyPressed == false)
				{
					blKeyPressed = true;
				}
				else
				{
					blKeyPressed = false;
				}
				break;

		case 70:
				fragmentShaderladder = true;
				break;

		case 86:
				fragmentShaderladder = false;
				break;
	}
}

function mouseDown() {
	//alert('mouse is clicked.');
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenelement ||
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;

	// if not fullScreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
			bFullScreen = false;
	}
}

function vertexShader()
{
	// code
		var vertexShaderSourceCode = 
			"#version 300 es" +
            "\n" +
            "in vec4 vPosition;" +
            "in vec3 vNormal;" +
            "uniform mat4 u_model_matrix;" +
            "uniform mat4 u_view_matrix;" +
            "uniform mat4 u_projection_matrix;" +

            "uniform vec3 u_Ka;" +
            "uniform vec3 u_Kd;" +
            "uniform vec3 u_Ks;" +

            "uniform vec3 u_La_Red;" +
            "uniform vec3 u_Ld_Red;" +
            "uniform vec3 u_Ls_Red;" +
            "uniform vec4 u_light_position_Red;" +

            "uniform float u_material_shininess;" +
            "uniform int u_lighting_enabled;" +

            "out vec3 phong_ads_color;" +
            "void main(void)" +
            "{" +
	            "if(u_lighting_enabled == 1)" +
	            "{" +
		            "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
		            "vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" +
		            "vec3 viewer_vector = normalize(-eye_coordinates.xyz);" +
		            //fOR Red Color Lighting
		            "vec3 light_direction_Red = normalize(vec3(u_light_position_Red) - eye_coordinates.xyz);" +
		            "float tn_dot_ld_Red = max(dot(transformed_normals, light_direction_Red), 0.0);" +
		            "vec3 reflection_vector_Red = reflect(-light_direction_Red, transformed_normals);" +
		            "vec3 ambient_Red = u_La_Red * u_Ka;" +
		            "vec3 diffuse_Red = u_Ld_Red * u_Kd * tn_dot_ld_Red;" +
		            "vec3 specular_Red = u_Ls_Red * u_Ks * pow(max(dot(reflection_vector_Red, viewer_vector), 0.0), u_material_shininess);" +
		            
		      
		            "phong_ads_color = ambient_Red + diffuse_Red + specular_Red;" +
	            "}" +
	            "else" +
	            "{" +
	            	"phong_ads_color = vec3(1.0, 1.0, 1.0);" +
	            "}" +
            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
            "}";


            //ambient + diffuse + specular
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false);
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	var fragmentShaderSourceCode = 
			"#version 300 es" +
            "\n" +
            "precision highp float;" +
            "in vec3 phong_ads_color;" +
            "out vec4 FragColor;" +
            "void main(void)" +
            "{" +
            "	FragColor = vec4(phong_ads_color, 1.0);" +
            "}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false);
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject_PV = gl.createProgram();

	gl.attachShader(shaderProgramObject_PV, vertexShaderObject);
	gl.attachShader(shaderProgramObject_PV, fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject_PV, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_PV, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");

		//linking
	gl.linkProgram(shaderProgramObject_PV);
	if(!gl.getProgramParameter(shaderProgramObject_PV, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_PV);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	model_matrix_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_model_matrix");
    view_matrix_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_view_matrix");
    projection_matrix_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_projection_matrix");
    light_enable_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_lighting_enabled");

    Ka_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_Ka");
    Kd_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_Kd");
    Ks_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_Ks");
    material_shininess_uniform = gl.getUniformLocation(shaderProgramObject_PV, "u_material_shininess");

    La_uniform_Red = gl.getUniformLocation(shaderProgramObject_PV, "u_La_Red");
    Ld_uniform_Red = gl.getUniformLocation(shaderProgramObject_PV, "u_Ld_Red");
    Ls_uniform_Red = gl.getUniformLocation(shaderProgramObject_PV, "u_Ls_Red");
    light_position_uniform_Red = gl.getUniformLocation(shaderProgramObject_PV, "u_light_position_Red");
}

function fragmentShader() 
{
	// code
	var vertexShaderSourceCode = 
		"#version 300 es" +
        "\n" +
		"in vec4 vertexPosition_RMB;" +
		"in vec3 vertexNormal_RMB;" +

		"uniform mat4 u_modelMatrix_RMB;" +
		"uniform mat4 u_viewMatrix_RMB;" +
		"uniform mat4 u_projection_matrix_RMB;" +

		"uniform mediump int u_lKeyIsPressed_RMB;" +

		"uniform vec4 u_Light_Position_Red_RMB;" +

		"out vec3 tnorm_RMB;" +
		"out vec3 viewerVector_RMB;" +
		"out vec3 lightdirection_Red_RMB;" +

		"void main(void)" +
		"{" +
			"if(u_lKeyIsPressed_RMB == 1)" +
			"{" +
				"vec4 eye_coordinate_RMB = u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" +

				"tnorm_RMB = mat3(u_viewMatrix_RMB * u_modelMatrix_RMB) * vertexNormal_RMB;" +

				"viewerVector_RMB = vec3(-eye_coordinate_RMB.xyz);" +

				"lightdirection_Red_RMB = vec3(u_Light_Position_Red_RMB - eye_coordinate_RMB);" +
			"}" +

		"gl_Position = u_projection_matrix_RMB * u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" +
		"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false);
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	var fragmentShaderSourceCode = 
		"#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec3 tnorm_RMB;" +
		"in vec3 lightdirection_Red_RMB;" +
		"in vec3 viewerVector_RMB;" +

		"uniform vec3 u_La_Red_RMB;" +
		"uniform vec3 u_Ld_Red_RMB;" +
		"uniform vec3 u_Ls_Red_RMB;" +

		"uniform vec3 u_Ka_RMB;" +
		"uniform vec3 u_Kd_RMB;" +
		"uniform vec3 u_Ks_RMB;" +

		"uniform float u_materialShininess_RMB;" +
		"uniform mediump int u_lKeyIsPressed_RMB;" +

		"out vec4 fragColor;" +

		"void main(void)" +
		"{" +
			"vec3 phong_AdsLight_RMB;" +
			"if(u_lKeyIsPressed_RMB == 1)" +
			"{" +

				"vec3 normalize_tnorm_RMB = normalize(tnorm_RMB);" +
				"vec3 normalize_viewerVector_RMB = normalize(viewerVector_RMB);" +
				//For Red Color Lighting
				"vec3 normalize_lightdirection_Red_RMB = normalize(lightdirection_Red_RMB);" +

				"vec3 reflectionVector_Red_RMB = reflect(-normalize_lightdirection_Red_RMB, normalize_tnorm_RMB);" +

				"float tn_dot_lightDir_Red_RMB = max(dot(normalize_lightdirection_Red_RMB, normalize_tnorm_RMB), 0.0);" +

				"vec3 ambient_Red_RMB = u_La_Red_RMB * u_Ka_RMB;" +

				"vec3 diffuse_Red_RMB = u_Ld_Red_RMB * u_Kd_RMB * tn_dot_lightDir_Red_RMB;" +

				"vec3 specular_Red_RMB = u_Ls_Red_RMB * u_Ks_RMB * pow(max(dot(reflectionVector_Red_RMB, normalize_viewerVector_RMB), 0.0), u_materialShininess_RMB);" +

				"phong_AdsLight_RMB = ambient_Red_RMB + diffuse_Red_RMB + specular_Red_RMB;" + 

			"}" +
			"else" +
			"{" +

				"phong_AdsLight_RMB = vec3(1.0, 1.0, 1.0);" +

			"}" +

			"fragColor = vec4(phong_AdsLight_RMB, 1.0);" +
		"}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false);
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject_PF = gl.createProgram();

	gl.attachShader(shaderProgramObject_PF, vertexShaderObject);
	gl.attachShader(shaderProgramObject_PF, fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject_PF, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vertexPosition_RMB");
	gl.bindAttribLocation(shaderProgramObject_PF, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vertexNormal_RMB");

		//linking
	gl.linkProgram(shaderProgramObject_PF);
	if(!gl.getProgramParameter(shaderProgramObject_PF, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_PF);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	model_matrix_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_modelMatrix_RMB");
    view_matrix_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_viewMatrix_RMB");
    projection_matrix_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_projection_matrix_RMB");
    light_enable_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_lKeyIsPressed_RMB");

    Ka_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Ka_RMB");
    Kd_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Kd_RMB");
    Ks_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Ks_RMB");
    material_shininess_uniform_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_materialShininess_RMB");
    //For Red Color Lighting
    La_uniform_Red_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_La_Red_RMB");
    Ld_uniform_Red_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Ld_Red_RMB");
    Ls_uniform_Red_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Ls_Red_RMB");
    light_position_uniform_Red_RMB = gl.getUniformLocation(shaderProgramObject_PF, "u_Light_Position_Red_RMB");
}