//global variable
var canvas = null;
var gl=null; //For WebGL Context
var bFullScreen=false;
var canvas_orignal_width;
var canvas_orignal_height;

const WebGLMacros=//when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE:3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_I;
var vbo_Position_I;
var vbo_Color_I;

var vao_N;
var vbo_Position_N;
var vbo_Color_N;

var vao_D;
var vbo_Position_D;
var vbo_Color_D;

var vao_A_TriBand;
var vbo_Position_A_TriBand;
var vbo_Color_A_TriBand;

var vao_A;
var vbo_Position_A;
var vbo_Color_A;

var mvpUniform;
var perspectiveProjectionMatrix;

//To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

//To stop animation : To have cancleAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;;

//onload function
function main()
{
	//get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed...!!!\n");
	else
		console.log("Obtaining Canvas Succeeded...!!!\n");

	canvas_orignal_width = canvas.width;
	canvas_orignal_height = canvas.height;
	//print canvas width and height on console
	console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//initialize WebGL
	init();

	//start drawing here as warning-up
	resize();
	display();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenelement ||
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;

	// if not fullScreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
			bFullScreen = false;
	}
}

function init()
{
	//code
	//get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if(gl==null) //failed to get context
	{
		console.log("Failed to get the rendering context for WebGL...!!!");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	var vertexShaderSourceCode=
			"#version 300 es" +
			"\n" +
            "in vec4 vPosition;" +
            "in vec4 vColor;" +
            "uniform mat4 u_mvp_matrix;" +
            "out vec4 out_Color;" +
            "void main(void)" +
            "{" +
                "gl_Position = u_mvp_matrix * vPosition;" +
                "out_Color = vColor;" +
            "}";


		vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
		gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
		gl.compileShader(vertexShaderObject);
		if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false);
		{
			var error=gl.getShaderInfoLog(vertexShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

	 //Write Fragment Shader Code
    var fragmentShaderSourceCode =
    		"#version 300 es" +
			"\n" +
			"precision highp float;" +
            "in vec4 out_Color;" +
            "out vec4 fragColor;" +
            "void main(void)" +
            "{" +
               "fragColor = out_Color;" +
            "}";

		fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
		gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		gl.compileShader(fragmentShaderObject);
		if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false);
		{
			var error=gl.getShaderInfoLog(fragmentShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}


		//shader program
		shaderProgramObject=gl.createProgram();
		gl.attachShader(shaderProgramObject, vertexShaderObject);
		gl.attachShader(shaderProgramObject, fragmentShaderObject);

		//pre-link binding of shader program object with vertex shader attribute
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");


		//linking
		gl.linkProgram(shaderProgramObject);
		if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
		{
			var error = gl.getProgramInfoLog(shaderProgramObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

	 //For I vao
	var I_Verties = new Float32Array
    	([
    		-1.0, 1.0, 0.0,
			-1.0, -1.0, 0.0
    	]);

    var I_Color = new Float32Array
    	([
    		1.0, 0.4, 0.0,
			0.0, 1.0, 0.0
    	]);

    vao_I = gl.createVertexArray();
    gl.bindVertexArray(vao_I);

    vbo_Position_I = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Position_I);
    gl.bufferData(gl.ARRAY_BUFFER, I_Verties, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our I_Verties array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_Color_I = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Color_I);
    gl.bufferData(gl.ARRAY_BUFFER, I_Color, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3,//3 is for X, Y, Z co-ordinates in our I_Color array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

     //For N vao
    var N_Verties = new Float32Array
    	([
    		-1.0, 1.0, 0.0,
			-1.0, -1.0, 0.0,
			-1.0, 1.0, 0.0,
			-0.2, -1.0, 0.0,
			-0.2, 1.0, 0.0,
			-0.2, -1.0, 0.0
    	]);

    var N_Color = new Float32Array
    	([
    		1.0, 0.4, 0.0,
			0.0, 1.0, 0.0,
			1.0, 0.4, 0.0,
			0.0, 1.0, 0.0,
			1.0, 0.4, 0.0,
			0.0, 1.0, 0.0
    	]);

    vao_N = gl.createVertexArray();
    gl.bindVertexArray(vao_N);

    vbo_Position_N = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Position_N);
    gl.bufferData(gl.ARRAY_BUFFER, N_Verties, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our N_Verties array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_Color_N = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Color_N);
    gl.bufferData(gl.ARRAY_BUFFER, N_Color, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3,//3 is for X, Y, Z co-ordinates in our N_Color array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

    //For D vao
    var D_Verties = new Float32Array
    	([
    		-1.0, 0.995, 0.0,
			-1.0, -0.995, 0.0,

			-1.2, 0.995, 0.0,
			-0.2, 0.995, 0.0,

			-1.2, -0.995, 0.0,
			-0.2, -0.995, 0.0,

			-0.2, 1.0, 0.0,
			-0.2, -1.0, 0.0
    	]);

    var D_Color = new Float32Array
    	([
    		1.0,0.4,0.0,
			0.0, 1.0, 0.0,
			1.0,0.4,0.0,
			1.0,0.4,0.0,
			0.0, 1.0, 0.0,
			0.0, 1.0, 0.0,
			1.0,0.4,0.0,
			0.0, 1.0, 0.0
    	]);

    vao_D = gl.createVertexArray();
    gl.bindVertexArray(vao_D);

    vbo_Position_D = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Position_D);
    gl.bufferData(gl.ARRAY_BUFFER, D_Verties, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our D_Verties array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_Color_D = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Color_D);
    gl.bufferData(gl.ARRAY_BUFFER, D_Color, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3,//3 is for X, Y, Z co-ordinates in our D_Color array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

    //For A_TriBand vao
    var A_TriBand_Verties = new Float32Array
    	([
    		-0.03, -0.045, 0.0,
			-0.56, -0.045, 0.0,
			-0.02, -0.08, 0.0,
			-0.57, -0.08, 0.0,
			-0.01, -0.12, 0.0,
			-0.58, -0.12, 0.0
    	]);

    var A_TriBand_Color = new Float32Array
    	([
    		1.0, 0.5, 0.0,
			1.0, 0.5, 0.0,
			1.0, 1.0, 1.0,
			1.0, 1.0, 1.0,
			0.0, 1.0, 0.0,
			0.0, 1.0, 0.0,
    	]);

    vao_A_TriBand = gl.createVertexArray();
    gl.bindVertexArray(vao_A_TriBand);

    vbo_Position_A_TriBand = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Position_A_TriBand);
    gl.bufferData(gl.ARRAY_BUFFER, A_TriBand_Verties, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our A_TriBand_Verties array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_Color_A_TriBand = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Color_A_TriBand);
    gl.bufferData(gl.ARRAY_BUFFER, A_TriBand_Color, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3,//3 is for X, Y, Z co-ordinates in our A_TriBand_Color array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

    //For A vao
    var A_Verties = new Float32Array
    	([
    		-0.4, 1.0, 0.0,
			0.20, -1.0, 0.0,
			-0.4, 1.0, 0.0,
			-0.80, -1.0, 0.0
    	]);

    var A_Color = new Float32Array
    	([
    		1.0, 0.4,0.0,
			0.0, 1.0, 0.0,
			1.0, 0.4,0.0,
			0.0, 1.0, 0.0,
    	]);

    vao_A = gl.createVertexArray();
    gl.bindVertexArray(vao_A);

    vbo_Position_A = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Position_A);
    gl.bufferData(gl.ARRAY_BUFFER, A_Verties, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our A_Verties array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_Color_A = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Color_A);
    gl.bufferData(gl.ARRAY_BUFFER, A_Color, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3,//3 is for X, Y, Z co-ordinates in our A_Color array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

	//set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);  //blue

	//Depth test will always be enabled
	gl.enable(gl.DEPTH_TEST);

	////We will always cill back faces for better performance
	//gl.enable(gl.CULL_FACE);

	//initialize projection matrix
	perspectiveProjectionMatrix=mat4.create();

}

function resize()
{
	//code
	if(bFullScreen == true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_orignal_width;
		canvas.height=canvas_orignal_height;
	}

	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	//Perspective Graphic Projection => left, rigth, bottom, top
	
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function display()
{
	//code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	//For First I Draw
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [-0.9, 0.0, -5.0])
	
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_I);
	gl.drawArrays(gl.LINES, 0, 2);
	gl.bindVertexArray(null);

	//For N Draw
	mat4.identity(modelViewMatrix); //reset to identity matrix
	mat4.identity(modelViewProjectionMatrix); //reset to identity matrix

	mat4.translate(modelViewMatrix, modelViewMatrix, [-0.6, 0.0, -5.0])

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_N);

	gl.drawArrays(gl.LINES, 0, 6);
	
	gl.bindVertexArray(null);

	//For D Draw
	mat4.identity(modelViewMatrix); //reset to identity matrix
	mat4.identity(modelViewProjectionMatrix); //reset to identity matrix

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.64, 0.0, -5.0])

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_D);

	gl.drawArrays(gl.LINES, 0, 8);
	
	gl.bindVertexArray(null);

	//For Second I Draw
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [1.75, 0.0, -5.0])
	
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_I);
	gl.drawArrays(gl.LINES, 0, 2);
	gl.bindVertexArray(null);

	//For A_TriBand Draw
	mat4.identity(modelViewMatrix); //reset to identity matrix
	mat4.identity(modelViewProjectionMatrix); //reset to identity matrix

	mat4.translate(modelViewMatrix, modelViewMatrix, [1.80, 0.0, -5.0])

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_A_TriBand);

	gl.drawArrays(gl.LINES, 0, 6);
	
	gl.bindVertexArray(null);

	//For A Draw
	mat4.identity(modelViewMatrix); //reset to identity matrix
	mat4.identity(modelViewProjectionMatrix); //reset to identity matrix

	mat4.translate(modelViewMatrix, modelViewMatrix, [1.85, 0.0, -5.0])

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_A);

	gl.drawArrays(gl.LINES, 0, 4);
	
	gl.bindVertexArray(null);

	gl.useProgram(null);
	//animation loop
	requestAnimationFrame(display, canvas);

}

function uninitialize()
{
	//code

	if(vbo_Color_I)
	{
		gl.deleteBuffers(vbo_Color_I);
		vbo_Color_I=null;
	}

	if(vbo_Position_I)
	{
		gl.deleteBuffers(vbo_Position_I);
		vbo_Position_I=null;
	}

	if(vao_I)
	{
		gl.deleteVertexArrays(vao_I);
		vao_I=null;
	}

	if(vbo_Color_N)
	{
		gl.deleteBuffers(vbo_Color_N);
		vbo_Color_N=null;
	}

	if(vbo_Position_N)
	{
		gl.deleteBuffers(vbo_Position_N);
		vbo_Position_N=null;
	}

	if(vao_N)
	{
		gl.deleteVertexArrays(vao_N);
		vao_N=null;
	}


	if(vbo_Color_D)
	{
		gl.deleteBuffers(vbo_Color_D);
		vbo_Color_D=null;
	}

	if(vbo_Position_D)
	{
		gl.deleteBuffers(vbo_Position_D);
		vbo_Position_D=null;
	}

	if(vao_D)
	{
		gl.deleteVertexArrays(vao_D);
		vao_D=null;
	}

	if(vbo_Color_A)
	{
		gl.deleteBuffers(vbo_Color_A);
		vbo_Color_A=null;
	}

	if(vbo_Position_A)
	{
		gl.deleteBuffers(vbo_Position_A);
		vbo_Position_A=null;
	}

	if(vao_A)
	{
		gl.deleteVertexArrays(vao_A);
		vao_A=null;
	}


	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:  // Escape
				//uninitialize
				uninitialize();
				//close our application's tab
				window.close();  //may not work in firefox but works in safari and chrome
				break;

		case 70: //for 'F' or 'f'
		toggleFullScreen();
		break;
	}
}

function mouseDown()
{
	//code
}


