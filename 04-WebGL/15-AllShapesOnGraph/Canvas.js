//global variable
var canvas = null;
var gl=null; //For WebGL Context
var bFullScreen=false;
var canvas_orignal_width;
var canvas_orignal_height;

const WebGLMacros=//when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE:3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_Graph;
var vbo_Graph;
var vbo_Color_Graph;

var vao_Shapes;
var vbo_Shapes;
var vbo_Color_Shapes;

var vao_BigCircle;
var vbo_BigCircle;
var vbo_BigCircle_Color;

var vao_SmallCircle;
var vbo_SmallCircle;

var mvpUniform;
var perspectiveProjectionMatrix;

//To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

//To stop animation : To have cancleAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;;

//onload function
function main()
{
	//get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed...!!!\n");
	else
		console.log("Obtaining Canvas Succeeded...!!!\n");

	canvas_orignal_width = canvas.width;
	canvas_orignal_height = canvas.height;
	//print canvas width and height on console
	console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//initialize WebGL
	init();

	//start drawing here as warning-up
	resize();
	display();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenelement ||
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;

	// if not fullScreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
			bFullScreen = false;
	}
}

function init()
{
	//code
	//get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if(gl==null) //failed to get context
	{
		console.log("Failed to get the rendering context for WebGL...!!!");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	var vertexShaderSourceCode=
			"#version 300 es" +
			"\n" +
            "in vec4 vPosition;" +
            "in vec4 vColor;" +
            "uniform mat4 u_mvp_matrix;" +
            "out vec4 out_Color;" +
            "void main(void)" +
            "{" +
                "gl_Position = u_mvp_matrix * vPosition;" +
                "out_Color = vColor;" +
            "}"


		vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
		gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
		gl.compileShader(vertexShaderObject);
		if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false);
		{
			var error=gl.getShaderInfoLog(vertexShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

	 //Write Fragment Shader Code
    var fragmentShaderSourceCode =
    		"#version 300 es" +
			"\n" +
			"precision highp float;" +
            "in vec4 out_Color;" +
            "out vec4 fragColor;" +
            "void main(void)" +
            "{" +
               "fragColor = out_Color;" +
            "}"

		fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
		gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		gl.compileShader(fragmentShaderObject);
		if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false);
		{
			var error=gl.getShaderInfoLog(fragmentShaderObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}


		//shader program
		shaderProgramObject=gl.createProgram();
		gl.attachShader(shaderProgramObject, vertexShaderObject);
		gl.attachShader(shaderProgramObject, fragmentShaderObject);

		//pre-link binding of shader program object with vertex shader attribute
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
		gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");


		//linking
		gl.linkProgram(shaderProgramObject);
		if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
		{
			var error = gl.getProgramInfoLog(shaderProgramObject);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}

	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

	var graphVertices = new Float32Array
    							([
    								//Vertical Lines
									//44
									-1.10, 1.0, 0.0,
									-1.10, -1.0, 0.0,
									-1.05, 1.0, 0.0,
									-1.05, -1.0, 0.0,
									-1.0, 1.0, 0.0,
									-1.0, -1.0, 0.0,
									-0.95, 1.0, 0.0,
									-0.95, -1.0, 0.0,
									-0.90, 1.0, 0.0,
									-0.90, -1.0, 0.0,
									-0.85, 1.0, 0.0,
									-0.85, -1.0, 0.0,
									-0.80, 1.0, 0.0,
									-0.80, -1.0, 0.0,
									-0.75, 1.0, 0.0,
									-0.75, -1.0, 0.0,
									-0.70, 1.0, 0.0,
									-0.70, -1.0, 0.0,
									-0.65, 1.0, 0.0,
									-0.65, -1.0, 0.0,
									-0.60, 1.0, 0.0,
									-0.60, -1.0, 0.0,
									-0.55, 1.0, 0.0,
									-0.55, -1.0, 0.0,
									-0.50, 1.0, 0.0,
									-0.50, -1.0, 0.0,
									-0.45, 1.0, 0.0,
									-0.45, -1.0, 0.0,
									-0.40, 1.0, 0.0,
									-0.40, -1.0, 0.0,
									-0.35, 1.0, 0.0,
									-0.35, -1.0, 0.0,
									-0.30, 1.0, 0.0,
									-0.30, -1.0, 0.0,
									-0.25, 1.0, 0.0,
									-0.25, -1.0, 0.0,
									-0.20, 1.0, 0.0,
									-0.20, -1.0, 0.0,
									-0.15, 1.0, 0.0,
									-0.15, -1.0, 0.0,
									-0.10, 1.0, 0.0,
									-0.10, -1.0, 0.0,
									-0.05, 1.0, 0.0,
									-0.05, -1.0, 0.0,
									//46
									0.00, 1.0, 0.0,
									0.00, -1.0, 0.0,
									0.05, 1.0, 0.0,
									0.05, -1.0, 0.0,
									0.10, 1.0, 0.0,
									0.10, -1.0, 0.0,
									0.15, 1.0, 0.0,
									0.15, -1.0, 0.0,
									0.20, 1.0, 0.0,
									0.20, -1.0, 0.0,
									0.25, 1.0, 0.0,
									0.25, -1.0, 0.0,
									0.30, 1.0, 0.0,
									0.30, -1.0, 0.0,
									0.35, 1.0, 0.0,
									0.35, -1.0, 0.0,
									0.40, 1.0, 0.0,
									0.40, -1.0, 0.0,
									0.45, 1.0, 0.0,
									0.45, -1.0, 0.0,
									0.50, 1.0, 0.0,
									0.50, -1.0, 0.0,
									0.55, 1.0, 0.0,
									0.55, -1.0, 0.0,
									0.60, 1.0, 0.0,
									0.60, -1.0, 0.0,
									0.65, 1.0, 0.0,
									0.65, -1.0, 0.0,
									0.70, 1.0, 0.0,
									0.70, -1.0, 0.0,
									0.75, 1.0, 0.0,
									0.75, -1.0, 0.0,
									0.80, 1.0, 0.0,
									0.80, -1.0, 0.0,
									0.85, 1.0, 0.0,
									0.85, -1.0, 0.0,
									0.90, 1.0, 0.0,
									0.90, -1.0, 0.0,
									0.95, 1.0, 0.0,
									0.95, -1.0, 0.0,
									1.0, 1.0, 0.0,
									1.0, -1.0, 0.0,
									1.05, 1.0, 0.0,
									1.05, -1.0, 0.0,
									1.10, 1.0, 0.0,
									1.10, -1.0, 0.0,

									//Horizantal Lines  
									//24
									1.15, -0.60, 0.0,
									-1.15, -0.60, 0.0,
									1.15, -0.55, 0.0,
									-1.15, -0.55, 0.0,
									1.15, -0.50, 0.0,
									-1.15, -0.50, 0.0,
									1.15, -0.45, 0.0,
									-1.15, -0.45, 0.0,
									1.15, -0.40, 0.0,
									-1.15, -0.40, 0.0,
									1.15, -0.35, 0.0,
									-1.15, -0.35, 0.0,
									1.15, -0.30, 0.0,
									-1.15, -0.30, 0.0,
									1.15, -0.25, 0.0,
									-1.15, -0.25, 0.0,
									1.15, -0.20, 0.0,
									-1.15, -0.20, 0.0,
									1.15, -0.15, 0.0,
									-1.15, -0.15, 0.0,
									1.15, -0.10, 0.0,
									-1.15, -0.10, 0.0,
									1.15, -0.05, 0.0,
									-1.15, -0.05, 0.0,
									//26
									1.15, 0.0, 0.0,
									-1.15, 0.0, 0.0,
									1.15, 0.05, 0.0,
									-1.15, 0.05, 0.0,
									1.15, 0.10, 0.0,
									-1.15, 0.10, 0.0,
									1.15, 0.15, 0.0,
									-1.15, 0.15, 0.0,
									1.15, 0.20, 0.0,
									-1.15, 0.20, 0.0,
									1.15, 0.25, 0.0,
									-1.15, 0.25, 0.0,
									1.15, 0.30, 0.0,
									-1.15, 0.30, 0.0,
									1.15, 0.35, 0.0,
									-1.15, 0.35, 0.0,
									1.15, 0.40, 0.0,
									-1.15, 0.40, 0.0,
									1.15, 0.45, 0.0,
									-1.15, 0.45, 0.0,
									1.15, 0.50, 0.0,
									-1.15, 0.50, 0.0,
									1.15, 0.55, 0.0,
									-1.15, 0.55, 0.0,
									1.15, 0.60, 0.0,
									-1.15, 0.60, 0.0
    							]);

    var Color = 140;
    var graphColor = new Float32Array(3*Color);
    for (var i = 0; i < Color; i++)
	{
		graphColor[3 * i] = 0.0;
		graphColor[3 * i + 1] = 1.0;
		graphColor[3 * i + 2] = 0.0;
	}

   
    vao_Graph = gl.createVertexArray();
    gl.bindVertexArray(vao_Graph);

    vbo_Graph = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Graph);
    gl.bufferData(gl.ARRAY_BUFFER, graphVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our graphVertices array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_Color_Graph = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Color_Graph);
    gl.bufferData(gl.ARRAY_BUFFER, graphColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3,//3 is for X, Y, Z co-ordinates in our graphColor array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

    //For Triangle And Rectangle
    var shapesVertices = new Float32Array
    							([
    								//Ractangle
									0.97, 0.7, 0.0,
									-0.97, 0.7, 0.0,
									-0.97, 0.7, 0.0,
									-0.97, -0.7, 0.0,
									-0.97, -0.7, 0.0,
									0.97, -0.7, 0.0,
									0.97, 0.7, 0.0,
									0.97, -0.7, 0.0,

									//Triangle
									0.0, 0.7, 0.0,
									-0.97, -0.7, 0.0,
									-0.97, -0.7, 0.0,
									0.97, -0.7, 0.0,
									0.97, -0.7, 0.0,
									0.0, 0.7, 0.0
								]);

	var sColor = 14;
    var shapesColor = new Float32Array(3*sColor);
    for (var i = 0; i < sColor; i++)
	{
		shapesColor[3 * i + 0] = 1.0;
		shapesColor[3 * i + 1] = 1.0;
		shapesColor[3 * i + 2] = 0.0;
	}

    vao_Shapes = gl.createVertexArray();
    gl.bindVertexArray(vao_Shapes);

    vbo_Shapes = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Shapes);
    gl.bufferData(gl.ARRAY_BUFFER, shapesVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our shapesVertices array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_Color_Shapes = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Color_Shapes);
    gl.bufferData(gl.ARRAY_BUFFER, shapesColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3,//3 is for X, Y, Z co-ordinates in our shapesColor array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

    //For Big Circle
    var circleColor = 300;
    var bigCircleVertices = new Float32Array(3*circleColor);
    for (var i = 0; i < circleColor; i++)
	{
		var AngleCircle = (2.0 * Math.PI * i) / circleColor;
		bigCircleVertices[3 * i + 0] = Math.cos(AngleCircle) * 1.2;
    	bigCircleVertices[3 * i + 1] = Math.sin(AngleCircle) * 1.2;
    	bigCircleVertices[3 * i + 2] = 0.0;
	}

    var shapesColor = new Float32Array(3*circleColor);
    for (var i = 0; i < circleColor; i++)
	{
		shapesColor[3 * i + 0] = 1.0;
		shapesColor[3 * i + 1] = 1.0;
		shapesColor[3 * i + 2] = 0.0;
	}

	vao_BigCircle = gl.createVertexArray();
    gl.bindVertexArray(vao_BigCircle);

    vbo_BigCircle = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_BigCircle);
    gl.bufferData(gl.ARRAY_BUFFER, bigCircleVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our bigCircleVertices array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_BigCircle_Color = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_BigCircle_Color);
    gl.bufferData(gl.ARRAY_BUFFER, shapesColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3,//3 is for X, Y, Z co-ordinates in our shapesColor array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

    //For Small Circle
    var smallCircleVertices = new Float32Array(3*circleColor);
    for (var i = 0; i < circleColor; i++)
	{
		var AngleCircle = (2.0 * Math.PI * i) / circleColor;
		smallCircleVertices[3 * i + 0] = Math.cos(AngleCircle) * 0.507;
    	smallCircleVertices[3 * i + 1] = Math.sin(AngleCircle) * 0.507;
    	smallCircleVertices[3 * i + 2] = 0.0;
	}

	vao_SmallCircle = gl.createVertexArray();
    gl.bindVertexArray(vao_SmallCircle);

    vbo_SmallCircle = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_SmallCircle);
    gl.bufferData(gl.ARRAY_BUFFER, smallCircleVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,//3 is for X, Y, Z co-ordinates in our smallCircleVertices array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_BigCircle_Color = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_BigCircle_Color);
    gl.bufferData(gl.ARRAY_BUFFER, shapesColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3,//3 is for X, Y, Z co-ordinates in our shapesColor array
    						gl.FLOAT, false, 0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

	//set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);  //blue

	//initialize projection matrix
	perspectiveProjectionMatrix=mat4.create();
}

function resize()
{
	//code
	if(bFullScreen == true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_orignal_width;
		canvas.height=canvas_orignal_height;
	}

	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	//Perspective Graphic Projection => left, rigth, bottom, top
	
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function display()
{
	//code
	gl.clear(gl.COLOR_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	//For Triangle
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -1.2])

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_Graph);
	gl.drawArrays(gl.LINES, 0, 140);
	gl.bindVertexArray(null);

	//For Rectangle
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0])

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.lineWidth(5.0);
	gl.bindVertexArray(vao_Shapes);
	gl.drawArrays(gl.LINES, 0, 14);
	gl.bindVertexArray(null);

	//For Big Circle
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0])

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_BigCircle);
	gl.drawArrays(gl.LINES, 0, 300);
	gl.bindVertexArray(null);

	//For Small Circle
	//For Big Circle
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, -0.193, -3.0])

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_SmallCircle);
	gl.drawArrays(gl.LINES, 0, 300);
	gl.bindVertexArray(null);

	gl.useProgram(null);

	//animation loop
	requestAnimationFrame(display, canvas);
}

function uninitialize()
{
	//code

	if(vbo_Color_Graph)
	{
		gl.deleteBuffer(vbo_Color_Graph);
		vbo_Color_Graph=null;
	}

	if(vbo_Graph)
	{
		gl.deleteBuffer(vbo_Graph);
		vbo_Graph=null;
	}

	if(vao_Graph)
	{
		gl.deleteVertexArray(vao_Graph);
		vao_Graph=null;
	}

	
	if(vbo_Color_Shapes)
	{
		gl.deleteBuffer(vbo_Color_Shapes);
		vbo_Color_Shapes=null;
	}

	if(vbo_Shapes)
	{
		gl.deleteBuffer(vbo_Shapes);
		vbo_Shapes=null;
	}

	if(vao_Shapes)
	{
		gl.deleteVertexArray(vao_Shapes);
		vao_Shapes=null;
	}

	if(vbo_BigCircle_Color)
	{
		gl.deleteBuffer(vbo_BigCircle_Color);
		vbo_BigCircle_Color=null;
	}

	if(vbo_SmallCircle)
	{
		gl.deleteBuffer(vbo_SmallCircle);
		vbo_SmallCircle=null;
	}

	if(vao_BigCircle)
	{
		gl.deleteVertexArray(vao_BigCircle);
		vao_BigCircle=null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:  // Escape
				//uninitialize
				uninitialize();
				//close our application's tab
				window.close();  //may not work in firefox but works in safari and chrome
				break;

		case 70: //for 'F' or 'f'
		toggleFullScreen();
		break;
	}
}

function mouseDown()
{
	//code
}
