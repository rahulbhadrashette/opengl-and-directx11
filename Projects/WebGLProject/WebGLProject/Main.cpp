// Header files
#pragma once

#include "Main.h"
#include "SkyBox.h"
#include "Terrain.h"
#include "ModelLoading.h"
#include "FontRendering.h"
#include "MyOpenAL.h"
#include "FlagRendering.h"
#include "Camera.h"
#include"Song.h"

// global variable declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

#pragma comment (lib, "OpenAL32.lib")
#pragma comment( lib, "Winmm.lib")

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

#define SUN_DIST 100.0f
#define SUN_UPDATE_RADIAN 0.001f
#define SUN_UPDATE_DEGREE (90.0f * SUN_UPDATE_RADIAN * 2.0f) / (GLfloat)M_PI
#define SUN_YAW 45.0f

GLfloat angleSun = 0.0f;
glm::vec4 GLOBAL_LIGHT_POSISION = glm::vec4(
	SUN_DIST * glm::cos(glm::radians(angleSun)) * glm::cos(glm::radians(SUN_YAW)),
	SUN_DIST * glm::cos(glm::radians(angleSun)),
	SUN_DIST * glm::cos(glm::radians(angleSun)) * glm::sin(glm::radians(SUN_YAW)),
	0.0f
);

// timing
float deltaTime = 0.0f;
float displayFrequency = 60.0f;
bool firstMouse = true;

float lastX = (float)WIN_WIDTH / 2.0;
float lastY = (float)WIN_HEIGHT / 2.0;

int giWinWidth = WIN_WIDTH;
int giWinHeight = WIN_HEIGHT;

// Camera
Camera camera(glm::vec3(6.0f, 0.0f, 6.0f));
bool gbDisableInittial = true;
bool bterrain = false;
bool keyPress = false;

static unsigned int counter = 0;
static unsigned long long int counter2 = 0;

glm::mat4 projectionMatrix;

#define CREDIT_ROLL_SIZE 0.2f
#define RED_COLOR vec3(1.0f, 0.0f, 0.0f)
#define GREEN_COLOR vec3(0.0f, 1.0f, 0.0f)
#define BLUE_COLOR vec3(0.0f, 0.0f, 1.0f)
#define ORANGE_COLOR vec3(1.0f, 0.5f, 0.0f)
#define WHITE_COLOR vec3(1.0f, 1.0f, 1.0f)

extern unsigned int SKC_SourceId;
extern unsigned int SKC_SourceId2;
bool sPlaySound = true;
bool sPlaySound2 = true;

bool bShowFlag = false;
bool bShowSoldier = false;

#define BUFFER_SIZE 256
FILE* gpCameraCoordinates = NULL;
char line[BUFFER_SIZE];
char* token = NULL;
char* next_token = NULL;

int scene = -1;
const float rotation = 0.001f;

float angle = 0.0f;

// function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declaration
	int initialize(void);
	void display(void);
	void update(void);

	// local variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szApp[] = TEXT("OpenGLPP");

	bool bDone = false;
	int iRet = 0;
	int WIN_WIDTH_X, WIN_HEIGHT_Y;

	uint64_t i_Sys_Frequency = 0;
	uint64_t i_Prev_Time = 0;
	uint64_t i_Curr_Time = 0;

	Log_Initialize();

	if (fopen_s(&gpCameraCoordinates, "CameraCordinates.txt", "w") != NULL)
	{
		LogFile("CameraCordinates creation Fail.\n");
	}
	else
	{
		LogFile("CameraCordinates creation Successfulley.\n");
	}

	// initialization members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szApp;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// Registering Class
	RegisterClassEx(&wndclass);

	// Centerof the window calculation
	WIN_WIDTH_X = (GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2);
	WIN_HEIGHT_Y = (GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2);

	// Here Create actual Window in Memory 
	// Parallel to glutInitWindowSize(), glutInitWindowPosition(), and glutCreateWindow() all three together
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szApp, TEXT("Raster Group : RTR3_2020_MidProject2021"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		WIN_WIDTH_X,
		WIN_HEIGHT_Y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	iRet = initialize();
	if (iRet == -1)
	{
		LogFile("ChoosePixelFormat Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		LogFile("SetPixelFormat is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		LogFile("wglCreateContext is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		LogFile("wglMakeCurrent Failed.\n");
		DestroyWindow(hwnd);
	}
	else
	{
		LogFile("Initializetion Successfully Complited.\n");
	}

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				
			}
			update();
			QueryPerformanceCounter((LARGE_INTEGER*)&i_Curr_Time);
			if (((double)(i_Curr_Time - i_Prev_Time) / (double)i_Sys_Frequency) >= 1.0f / displayFrequency)
			{
				display();

				deltaTime = (GLfloat)(i_Curr_Time - i_Prev_Time);
				i_Prev_Time = i_Curr_Time;
			}

		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void resize(int, int);
	void uninitialize(void);
	void toggleFullScreen(void);
	void mouse_move(GLfloat, GLfloat);

	//code
	switch (iMsg)
	{
	case WM_CREATE:
		PostMessage(hwnd, WM_KEYDOWN, 0x46, NULL);
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case VK_UP:
		case 0x57:
			camera.CalculationPosition(FORWARD, deltaTime / 1000000);
			break;

		case VK_DOWN:
		case 0x53:
			camera.CalculationPosition(BACKWARD, deltaTime / 1000000);
			break;

		case VK_LEFT:
		case 0x41:
			camera.CalculationPosition(LEFT, deltaTime / 1000000);
			break;

		case VK_RIGHT:
		case 0x44:
			camera.CalculationPosition(RIGHT, deltaTime / 1000000);
			break;

		case 0x46:
			toggleFullScreen();
			break;

		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'E':
		case 'e':
			gbDisableInittial = false;
			bterrain = false;
			keyPress = true;
			break;

		case 'O':
		case 'o':
			bterrain = true;
			break;

		case 'u':
		case 'U':
			bShowSoldier = true;
			break;

		case 'i':
		case 'I':
			bShowFlag = true;
			break;

		case 'p':
		case 'P':
			fprintf_s(gpCameraCoordinates, " P %f %f %f %f %f %f %f %f %f\n", camera.Position.x, camera.Position.y, camera.Position.z, camera.Position.x + camera.Front.x, camera.Position.y + camera.Front.y, camera.Position.z + camera.Front.z, camera.Up.x, camera.Up.y, camera.Up.z);
			break;
		}
		break;

	case WM_MOUSEMOVE:
		mouse_move(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void mouse_move(GLfloat x_pos, GLfloat y_pos)
{
	if (firstMouse)
	{
		lastX = x_pos;
		lastY = y_pos;

		firstMouse = FALSE;
	}

	float x_offset = x_pos - lastX;
	float y_offset = lastY - y_pos;

	lastX = x_pos;
	lastY = y_pos;

	if (x_pos == 0 || (int)x_pos == giWinWidth - 1)
	{
		SetCursorPos(giWinWidth / 2, (int)lastY);
		lastX = (float)(giWinWidth / 2);
	}

	if (y_pos == 0 || (int)y_pos == giWinHeight - 1)
	{
		SetCursorPos((int)lastX, giWinHeight / 2);
		lastY = (float)(giWinHeight / 2);
	}

	camera.processMouseMove(x_offset, y_offset);
}

void toggleFullScreen(void)
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

int initialize(void)
{
	//function declaration
	void resize(int, int);
	void toggleFullScreen(void);

	//local variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLenum result;

	{
		//code
		//pfd structure make zero here
		ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

		// pfd structure initialize
		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 32;
		pfd.cRedBits = 8;
		pfd.cGreenBits = 8;
		pfd.cBlueBits = 8;
		pfd.cAlphaBits = 8;
		pfd.cDepthBits = 32;

		ghdc = GetDC(ghwnd);
		iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
		if (iPixelFormatIndex == 0)
		{
			return(-1);
		}

		if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
		{
			return(-2);
		}

		ghrc = wglCreateContext(ghdc);
		if (ghrc == NULL)
		{
			return(-3);
		}

		if (wglMakeCurrent(ghdc, ghrc) == FALSE)
		{
			return(-4);
		}

		result = glewInit();
		if (result != GLEW_OK)
		{
			LogFile("glewInit() is Failed.\n");
			DestroyWindow(ghwnd);
		}
	}

	GLfloat points[2] = { -0.5f, -0.5f };

	FontRenderingInitialize(ghwnd);
	OpenALInitialize();
	SkyBoxInitialize(ghwnd);
	TerrainInitialize(ghwnd, ".//resources//textures//terrain//HeightMap.png", points);
	//ModelLoadingInitialize(ghwnd);
	//FlagInitialization(ghwnd);

	//PlaySound(MAKEINTRESOURCE(MY_SONG), NULL, SND_ASYNC | SND_RESOURCE | SND_NODEFAULT);

	// configure global opengl state
	// -----------------------------
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);

	projectionMatrix = glm::mat4(1.0f);

	// Warmup call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);
	//toggleFullScreen();
	return(0);
}

void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	projectionMatrix = glm::perspective(glm::radians(camera.Zoom), (GLfloat)width / (GLfloat)height, 0.1f, 1000.0f);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Declatation of Matrices And Initialize of Matrix in identity
	glm::mat4 viewMatrixT = glm::mat4(1.0f);
	glm::mat4 viewMatrixSkyBox = glm::mat4(1.0f);
	glm::mat4 viewMatrixForFont = glm::mat4(1.0f);
	static bool once = true;
	static glm::mat4 projMat;

	if (once)
	{
		projMat = projectionMatrix;
		once = false;
	}

	viewMatrixForFont = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -30.0f));

	if (gbDisableInittial == true)
	{
		counter++;
		if (counter <= 120)
		{
			if (sPlaySound == true)
			{
				//alSourcePlay(SKC_SourceId);                                    // void alSourcePlay(ALuint source);
				sPlaySound = false;
			}
			FontRenderingDisplay(viewMatrixForFont, projMat, "AstroMediComp", -17.5f, 0.0f, 0.1f, RED_COLOR, 1.0f, 1.0f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Presents", -7.0f, -5.0f, 0.1f, GREEN_COLOR, 1.0f, 1.0f);

		}
		if (counter >= 120 && counter <= 240)
		{
			FontRenderingDisplay(viewMatrixForFont, projMat, "Raster", -7.0f, 0.0f, 0.1f, RED_COLOR, 1.0f, 1.0f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Group", -5.0f, -5.0f, 0.1f, GREEN_COLOR, 0.5f, 0.5f);
		}
		if (counter >= 240)
		{
			FontRenderingDisplay(viewMatrixForFont, projMat, "Dedicated To", -17.0f, 0.0f, 0.1f, RED_COLOR, 0.8f, 0.8f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Indian Army", -5.0f, -5.0f, 0.1f, GREEN_COLOR, 0.7f, 0.7f);

			if (counter == 360)
			{
				gbDisableInittial = false;
				scene = 0;
				bterrain = true;
			}
		}
	}
	else if (bterrain == true)
	{
		// remove translation from the view matrix
		viewMatrixSkyBox = glm::mat4(glm::mat3(camera.GetViewMatrix()));
		viewMatrixT = glm::lookAt(vec3(camera.Position.x, camera.Position.y, camera.Position.z), vec3(camera.Position.x + camera.Front.x, camera.Position.y + camera.Front.y, camera.Position.z + camera.Front.z), vec3(camera.Up.x, camera.Up.y, camera.Up.z));
		//viewMatrixT = glm::lookAt(camera.Position, camera.Position + camera.Front, camera.Up);
		glm::mat4 view = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -10.0f, -30.0f));

		SkyBoxDisplay(viewMatrixSkyBox, projMat);
		TerrainDisplay(viewMatrixT, projMat, GLOBAL_LIGHT_POSISION);
		//ModelLoadingDisplay(viewMatrixT, projMat);

		if (bShowFlag == true)
		{
			//FlagDisplay(viewMatrixT, projMat);
		}
	}
	else if (keyPress == true)
	{

		if (sPlaySound2 == true)
		{
			//alSourceStop(SKC_SourceId);
			alSourcePlay(SKC_SourceId);
			sPlaySound2 = false;
		}

		counter2++;
		if (counter2 <= 1000)
		{
			FontRenderingDisplay(viewMatrixForFont, projMat, "RTR 3 Batch", -14.0f, 15.f, 0.1f, RED_COLOR, 0.3f, 0.3f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Raster Group", -14.0f, 4.4f, 0.1f, GREEN_COLOR, 0.5f, 0.5f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Powered By", -12.0f, 0.0f, 0.1f, BLUE_COLOR, 0.3f, 0.3f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "AstroMediComp", -17.5f, -5.0f, 0.1f, GREEN_COLOR, 0.5f, 0.5f);
		}
		else if (counter2 >= 1000 && counter2 <= 2000)
		{
			FontRenderingDisplay(viewMatrixForFont, projMat, "Thank you", -14.0f, 14.0f, 0.1f, RED_COLOR, 0.3f, 0.3f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "---------------", -14.7f, 11.5f, 0.1f, RED_COLOR, 0.3f, 0.3f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Guruji", -9.0f, 2.5f, 0.1f, RED_COLOR, 0.5f, 0.5f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Dr. Vijay D Gokhale Sir", -25.0f, -3.0f, 0.1f, GREEN_COLOR, 0.7f, 0.7f);
		}
		else if (counter2 >= 2000 && counter2 <= 3000)
		{
			viewMatrixForFont = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, 0.0f, -30.0f));
			FontRenderingDisplay(viewMatrixForFont, projMat, "OS            :- Windows", -35.0f, 30.0f, 0.1f, RED_COLOR, CREDIT_ROLL_SIZE, CREDIT_ROLL_SIZE);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Language   :- C / CPP", -35.7f, 25.0f, 0.1f, RED_COLOR, CREDIT_ROLL_SIZE, CREDIT_ROLL_SIZE);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Referance :- OpenGL Programming Guide by Dave Shreiner,", -35.0f, 20.0f, 0.1f, RED_COLOR, CREDIT_ROLL_SIZE, CREDIT_ROLL_SIZE);
			FontRenderingDisplay(viewMatrixForFont, projMat, "                    Graham Sellers, and John M. Kessenich", -35.0f, 15.0f, 0.1f, RED_COLOR, CREDIT_ROLL_SIZE, CREDIT_ROLL_SIZE);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Referance :- OpenGL Superbible by Graham Sellers,", -35.0f, 10.0f, 0.1f, RED_COLOR, CREDIT_ROLL_SIZE, CREDIT_ROLL_SIZE);
			FontRenderingDisplay(viewMatrixForFont, projMat, "                    Richards Wrights,jr, Nicholas Haemel", -35.0f, 5.0f, 0.1f, RED_COLOR, CREDIT_ROLL_SIZE, CREDIT_ROLL_SIZE);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Referance :- OpenGL Shading Language by Randi J. Rost,", -35.0f, 0.0f, 0.1f, RED_COLOR, CREDIT_ROLL_SIZE, CREDIT_ROLL_SIZE);
			FontRenderingDisplay(viewMatrixForFont, projMat, "                    Bill Licea-Kane, Dan Ginsburg, John Kessenich", -35.0f, -5.0f, 0.1f, RED_COLOR, CREDIT_ROLL_SIZE, CREDIT_ROLL_SIZE);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Track       :- Ae Mere Watan Ke Logon By Saregama Music", -35.0f, -10.0f, 0.1f, RED_COLOR, CREDIT_ROLL_SIZE, CREDIT_ROLL_SIZE);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Singer       :- Lata Mangeshkar", -35.0f, -15.0f, 0.1f, RED_COLOR, CREDIT_ROLL_SIZE, CREDIT_ROLL_SIZE);
			FontRenderingDisplay(viewMatrixForFont, projMat, "8D Sounds YouTube Channel     :- War sound track", -35.0f, -20.0f, 0.1f, RED_COLOR, CREDIT_ROLL_SIZE, CREDIT_ROLL_SIZE);
		}
		else if (counter2 >= 3000 && counter2 <= 4000)
		{
			viewMatrixForFont = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, 0.0f, -30.0f));
			FontRenderingDisplay(viewMatrixForFont, projMat, "Effects", -35.0f, 32.0f, 0.1f, RED_COLOR, CREDIT_ROLL_SIZE, CREDIT_ROLL_SIZE);
			FontRenderingDisplay(viewMatrixForFont, projMat, "1. Cloth Rendering", -35.0f, 25.0f, 0.1f, GREEN_COLOR, CREDIT_ROLL_SIZE, CREDIT_ROLL_SIZE);
			FontRenderingDisplay(viewMatrixForFont, projMat, "2. Model Loading Using Assimp Library", -35.0f, 20.0f, 0.1f, GREEN_COLOR, CREDIT_ROLL_SIZE, CREDIT_ROLL_SIZE);
			FontRenderingDisplay(viewMatrixForFont, projMat, "3. Sky Box", -35.0f, 15.0f, 0.1f, GREEN_COLOR, CREDIT_ROLL_SIZE, CREDIT_ROLL_SIZE);
			FontRenderingDisplay(viewMatrixForFont, projMat, "4. Terrain Rendering", -35.0f, 10.0f, 0.1f, GREEN_COLOR, CREDIT_ROLL_SIZE, CREDIT_ROLL_SIZE);
		}
		else if (counter2 >= 4000 && counter2 <= 5000)
		{
			FontRenderingDisplay(viewMatrixForFont, projMat, "Group Members", -14.0f, 0.0f, 0.1f, BLUE_COLOR, 0.5f, 0.5f);
		}
		else if (counter2 >= 5000 && counter2 <= 6000)
		{
			FontRenderingDisplay(viewMatrixForFont, projMat, "Swapnil Mane", -7.0f, -8.0f, 0.1f, RED_COLOR, 0.5f, 0.5f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Sarvesh Chougule", -7.0f, -12.5f, 0.1f, GREEN_COLOR, 0.5f, 0.5f);
		}
		else if (counter2 >= 6000 && counter2 <= 7000)
		{
			FontRenderingDisplay(viewMatrixForFont, projMat, "Anmol Kumbhar", -33.0f, 10.0f, 0.1f, RED_COLOR, 0.5f, 0.5f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Shreyas Borle", -33.0f, 14.5f, 0.1f, GREEN_COLOR, 0.5f, 0.5f);
		}
		else if (counter2 >= 7000 && counter2 <= 8000)
		{
			FontRenderingDisplay(viewMatrixForFont, projMat, "Raster Group Leader", -22.0f, 2.5f, 0.1f, RED_COLOR, 0.3f, 0.3f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Rahul M Bhadrashette", -25.0f, -3.2f, 0.1f, GREEN_COLOR, 0.4f, 0.4f);
		}
		else if (counter2 >= 8000 && counter2 <= 9000)
		{
			FontRenderingDisplay(viewMatrixForFont, projMat, "Powered By", -12.0f, 0.0f, 0.1f, RED_COLOR, 1.0f, 1.0f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "AstroMediComp", -17.5f, -5.0f, 0.1f, GREEN_COLOR, 1.0f, 1.0f);
		}
		else if (counter2 >= 9000 && counter2 <= 10000)
		{
			FontRenderingDisplay(viewMatrixForFont, projMat, "Thank you", -14.0f, 10.0f, 0.1f, glm::vec3(1.0f, 0.0f, 0.0f), 0.3f, 0.3f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "----------------", -14.7f, 7.0f, 0.1f, glm::vec3(1.0f, 0.0f, 0.0f), 0.3f, 0.3f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Rama Gokhale Madam", -18.0f, 2.0f, 0.1f, GREEN_COLOR, 0.3f, 0.3f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Pradnya Gokhale Tai", -18.0f, -3.0f, 0.1f, GREEN_COLOR, 0.3f, 0.3f);
		}
		else if (counter2 >= 10000 && counter2 <= 11000)
		{
			FontRenderingDisplay(viewMatrixForFont, projMat, "Thank you", -14.0f, 8.0f, 0.1f, glm::vec3(1.0f, 0.0f, 0.0f), 0.3f, 0.3f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "----------------", -14.7f, 5.5f, 0.1f, glm::vec3(1.0f, 0.0f, 0.0f), 0.3f, 0.3f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Radhika Shukl Madam", -18.0f, 0.0f, 0.1f, GREEN_COLOR, 0.3f, 0.3f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Yogeshwar Shukl Sir", -18.0f, -5.0f, 0.1f, GREEN_COLOR, 0.3f, 0.3f);
		}
		else if (counter2 >= 11000 && counter2 <= 12000)
		{
			FontRenderingDisplay(viewMatrixForFont, projMat, "Thank you", -14.0f, 15.0f, 0.1f, glm::vec3(1.0f, 0.0f, 0.0f), 0.3f, 0.3f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "----------------", -14.7f, 10.5f, 0.1f, glm::vec3(1.0f, 0.0f, 0.0f), 0.3f, 0.3f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Sachin Bhadrashette", -22.5f, 5.0f, 0.1f, ORANGE_COLOR, 0.3f, 0.3f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Saurabh Bhalgat", -22.5f, 0.0f, 0.1f, WHITE_COLOR, 0.3f, 0.3f);
			FontRenderingDisplay(viewMatrixForFont, projMat, "Samarth Mabrukar", -22.5f, -5.0f, 0.1f, GREEN_COLOR, 0.3f, 0.3f);
		}
		else if (counter2 >= 12000 && counter <= 13000)
		{
			FontRenderingDisplay(viewMatrixForFont, projMat, "Thank You...!!!", -14.0f, 0.0f, 0.1f, glm::vec3(1.0f, 1.0f, 1.0f), 0.5f, 0.5f);

		}
		else if (counter2 == 14000)
		{
			//alSourceStop(SKC_SourceId2);
			DestroyWindow(ghwnd);

		}
	}

	SwapBuffers(ghdc);
}

void update(void)
{
	ModelLoadingUpdate();
	FontRenderingUpdate();
	FlagUpdate();

	if (scene == 0)
	{
		scene = 1;
	}
	else if (scene == 1)
	{
		glm::vec3 fv = glm::vec3(-27.696676, -4.094745, 62.839241) - glm::vec3(0.000000, 0.000000, 3.000000);
		fv = glm::normalize(fv);
		camera.Front = fv;
		camera.CalculationPosition(FORWARD, deltaTime / 1000000);
		if (camera.Position.x <= -27.696676 || camera.Position.y <= -4.094745 || camera.Position.z >= 62.839241)
		{
			scene = 2;
		}
	}
	else if (scene == 2)
	{
		camera.Yaw = camera.Yaw + 0.001f;
		camera.updateCameraVectors();
		if (camera.Yaw >= 20.0f)
		{		
			scene = 3;
		}
	}
	else if (scene == 3)
	{
		
		glm::vec3 fv = glm::vec3(-21.006899, -2.516730, 98.012405) - glm::vec3(-27.696676, -4.094745, 62.839241);
		fv = glm::normalize(fv);
		camera.Front = fv;
		camera.CalculationPosition(FORWARD, deltaTime / 1000000);
		if (camera.Position.x <= -21.006899 || camera.Position.y <= -2.516730 || camera.Position.z <= 98.012405)
		{
			scene = 4;
		}
	}
	else if(scene == 4)
	{
		camera.Yaw = camera.Yaw + 0.001f;
		camera.updateCameraVectors();
		if (camera.Yaw >= 45.0f)
		{
			scene = 5;
		}
	}
}

void uninitialize(void)
{
	SkyBoxUninitialize();
	TerrainUninitialize();
	ModelLoadingUninitialize();
	FontRenderingUninitialize();
	FlagUninitialize();

	if (gbFullScreen)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	// Break the Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
	}

	if (gpCameraCoordinates)
	{
		fclose(gpCameraCoordinates);
		gpCameraCoordinates = NULL;
	}
}

