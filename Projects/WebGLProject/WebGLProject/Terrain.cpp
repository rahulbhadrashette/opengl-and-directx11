#pragma once
#include "Terrain.h"

// library files
#pragma comment (lib, "user32.lib")
#pragma comment (lib, "gdi32.lib")
#pragma comment (lib, "kernel32.lib")

#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")

#define TERRAIN_SIZE					300.0f

#define TERRAIN_MAX_HEIGHT				10.0f

#define TERRAIN_LIGHT_AMBIENT			glm::value_ptr(glm::vec4(0.0f))
#define TERRAIN_LIGHT_DIFFUSE			glm::value_ptr(glm::vec4(1.0f))
#define TERRAIN_LIGHT_SPECULAR			glm::value_ptr(glm::vec4(1.0f))

#define TERRAIN_MATERIAL_AMBIENT		glm::value_ptr(glm::vec4(0.0f))
#define TERRAIN_MATERIAL_DIFFUSE		glm::value_ptr(glm::vec4(1.0f))
#define TERRAIN_MATERIAL_SPECULAR		glm::value_ptr(glm::vec4(1.0f))
#define TERRAIN_MATERIAL_SHININESS		64.0f

struct Terrain
{
	bool loaded;

	GLfloat X;
	GLfloat Z;

	GLfloat fWidth;
	GLfloat fHeight;

	// Height Map
	int iHeightMap_Height;
	int iHeightMap_Width;
	int iHeightMap_Chennels;

	unsigned char* ucHeightMap_ImageData;

	GLuint vao;
	GLuint vbo_Vertices;
	GLuint vbo_Normals;
	GLuint vbo_TexCoords;
	GLuint vbo_Elements;

	GLuint numVertices;
	GLuint numElements;

	GLuint lightAmbientUniform;
	GLuint lightDiffuseUniform;
	GLuint lightSpecularUniform;
	GLuint lightPositionUniform;

	GLuint materialAmbientUniform;
	GLuint materialDiffuseUniform;
	GLuint materialSpecularUniform;
	GLuint materialPositionUniform;

	GLuint modelUniform;
	GLuint viewUniform;
	GLuint perspectiveUniform;
	//GLuint shadowUniform;

	GLuint gShaderProgramObject;

	GLuint belndImageSamplerUniform;
	//GLuint blendMapNormalMapSamplerUniform;
	GLuint cementSamplerUniform;
	GLuint dirtSamplerUniform;
	GLuint gressSamplerUniform;
	GLuint mudSamplerUniform;
	GLuint shadowSamplerUniform;

	GLuint blendMapTexture;
	//GLuint blendMapNormalMapTexture;
	GLuint gressTexture;
	GLuint mudTexture;
	GLuint dirtTexture;
	GLuint cenentTexture;

	GLfloat* heightInfo = NULL; // 2D_Matrix

	GLuint gTileShaderProgramObject;
	GLuint vao_tile;
	GLuint vbo_tile_vertices;

	GLuint u_Model_Tile;
	GLuint u_view_Tile;
	GLuint u_Perspective_Tile;

	std::vector<glm::vec3> vegetationPoints;
};

static struct Terrain tr;

int TerrainInitialize(HWND ghwnd, const char* imagePath, float* coords)
{
	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;

	GLint iShaderCompileStatus;
	GLint iProgramLinkStatus;
	GLint iInfoLogLength;
	GLchar* szInfoLog;

	tr.loaded = true;

	// Image height map
	tr.ucHeightMap_ImageData = stbi_load(imagePath, &tr.iHeightMap_Width, &tr.iHeightMap_Height, &tr.iHeightMap_Chennels, 0);

	// Define vertex shader object
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// write vertex shader code
	const GLchar* vertexShaderSourceCode = {

		"#version 460 core"
		"\n"
		"\n in vec4 vPosition;"
		"\n in vec3 vNormal;"
		"\n in vec2 vTexCoord;"
		"\n"
		"\n uniform mat4 u_modelMatrix;"
		"\n uniform mat4 u_viewMatrix;"
		"\n uniform mat4 u_perspectiveMatrix;"
		//"\n uniform mat4 u_shadowMatrix;"
		"\n"
		"\n uniform vec4 u_lightPosition;"
		"\n"
		"\n out vec2 out_texCooeds;"
		"\n out vec3 surfaceNormal;"
		"\n out vec3 toLightVector;"
		"\n out vec3 toCameraVector;"
		//"\n out vec4 shodowCoords;"

		"\n void main(void)"
		"\n {"
		"\n		vec4 worldPosition = u_viewMatrix * u_modelMatrix * vPosition;"	
		"\n		surfaceNormal = mat3(u_viewMatrix * u_modelMatrix) * vNormal.xyz;"	
		"\n		toLightVector = u_lightPosition.xyz - worldPosition.xyz;"
		"\n		toCameraVector = -worldPosition.xyz;"	
		//"\n		shodowCoords = u_shadowMatrix * vPosition;"	
		"\n 	gl_Position = u_perspectiveMatrix * worldPosition;"
		"\n		out_texCooeds = vTexCoord;"	
		"\n }"
	};

	// Specifing above source to the vertex shader object
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// Compile the vertex shader
	glCompileShader(vertexShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				LogFile("Terrain VS :- %s ");
				LogFile((char*)szInfoLog);
				tr.loaded = false;
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Define Fragment Shader Object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// Write Fragment Shader Code
	const GLchar* fragmentShaderSourceCode = {
		"#version 460 core"
		"\n"
		"\n vec3 phong_Ads_Light;"
		"\n vec4 totalTextureColor;"
		"\n"
		"\n in vec2 out_texCooeds;"
		"\n in vec3 surfaceNormal;"
		"\n in vec3 toLightVector;"
		"\n in vec3 toCameraVector;"
		//"\n in vec4 shodowCoords;"
		"\n"
		"\n uniform sampler2D u_blendSampler;"
		"\n uniform sampler2D u_cementSampler;"
		"\n uniform sampler2D u_dirtSampler;"
		"\n uniform sampler2D u_grassSampler;"
		"\n uniform sampler2D u_mudSampler;"
		//"\n uniform sampler2DShadow u_ShadowSampler;"
		"\n"
		"\n uniform vec3 u_lightAmbient;"
		"\n uniform vec3 u_lightDiffuse;"
		"\n uniform vec3 u_lightSpecular;"

		"\n uniform vec3 u_k_materialAmbient;"
		"\n uniform vec3 u_k_materialDiffuse;"
		"\n uniform vec3 u_k_materialSpecular;"
		"\n uniform float u_k_materialShininess;"

		"\n out vec4 fragColor;"

		"\n void main(void)"
		"\n {"
		"\n		vec3 unitNormal = normalize(surfaceNormal);"
		"\n		vec3 unitLightVector = normalize(toLightVector);"
		"\n"
		"\n     vec3 ambient = u_lightAmbient * u_k_materialAmbient;"
		"\n"
		"\n		float unitNormal_dot_unitLightVector = max(dot(unitNormal, unitLightVector), 0.2);"
		"\n		vec3 diffuse = u_lightDiffuse * u_k_materialDiffuse * unitNormal_dot_unitLightVector;"
		"\n"
		"\n		vec3 unitVectorToCamera = normalize(toCameraVector);"
		"\n		vec3 lightDirection = -unitLightVector;"
		"\n		vec3 reflectionLightDirection = reflect(lightDirection, unitNormal);"
		"\n		vec3 specular = u_lightSpecular * u_k_materialSpecular * pow(max(dot(reflectionLightDirection, unitVectorToCamera), 0.0), u_k_materialShininess);"
		"\n"
		"\n		phong_Ads_Light = ambient + diffuse;"
		"\n"
		"\n		vec4 blendMapColor = texture(u_blendSampler, out_texCooeds);"
		"\n		float redAmount = blendMapColor.r;"
		"\n		float greenAmount = blendMapColor.g;"
		"\n		float blueAmount = blendMapColor.b;"
		"\n		float blackAmount = 1.0 - (blueAmount + redAmount + greenAmount) / 3.0;"
		"\n		float whiteAmount = (blueAmount + redAmount + greenAmount) / 3.0;"
		"\n"
		"\n		vec4 mudColor = texture(u_mudSampler, 60.0f * out_texCooeds) * redAmount;"
		"\n		vec4 grassColor = texture(u_grassSampler, 40.0f * out_texCooeds) * greenAmount;"
		"\n		vec4 cementColor = texture(u_cementSampler, 128.0f * out_texCooeds) * blueAmount;"
		"\n		vec4 roadColor = texture(u_dirtSampler, 128.0f * out_texCooeds) * blackAmount;"
		"\n"
		"\n		if(blackAmount > 0.75)"
		"\n		{"
		"\n			totalTextureColor = mudColor + grassColor + roadColor * 0.25;"
		"\n		}"
		"\n		else"
		"\n		{"
		"\n			totalTextureColor = cementColor + mudColor + grassColor;"
		"\n		}"
		"\n"
		//"\n		float shadowColor = textureProj(u_ShadowSampler, shodowCoords);"
		"\n"
		"\n 	fragColor = vec4(1.0) * totalTextureColor * vec4(phong_Ads_Light, 1.0);"
		//"\n 	fragColor = /*(shadowColor < 0.35 ? 0.35 : shadowColor) */ vec4(1.0, 1.0, 1.0, 1.0) * totalTextureColor * vec4(phong_Ads_Light, 1.0);"
		"\n 	fragColor.a = 1.0;"
		"\n }"
	};

	// Specifing above source to the Fragment Shader Object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	// Compile the Fragment Shader Object
	glCompileShader(fragmentShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				LogFile("Terrain FS :- ");
				LogFile((char*)szInfoLog);
				tr.loaded = false;
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Create Shader Object
	tr.gShaderProgramObject = glCreateProgram();

	// Attach vertexShaderObject to ShaderProgramObject
	glAttachShader(tr.gShaderProgramObject, vertexShaderObject);
	// Attach fragmentShaderObject to ShaderProgramObject
	glAttachShader(tr.gShaderProgramObject, fragmentShaderObject);

	// Pre Linking Binding to vertex attribute
	glBindAttribLocation(tr.gShaderProgramObject, RMB_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(tr.gShaderProgramObject, RMB_ATTRIBUTE_NORMAL, "vNormal");
	glBindAttribLocation(tr.gShaderProgramObject, RMB_ATTRIBUTE_TEXTURE0, "vTexCoord");

	//Link the shader program
	glLinkProgram(tr.gShaderProgramObject);

	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(tr.gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(tr.gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(tr.gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				LogFile("Terrain LS :- %s ");
				LogFile((char*)szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Post Linking Retriving Uniform Location
	tr.modelUniform = glGetUniformLocation(tr.gShaderProgramObject, "u_modelMatrix");
	tr.viewUniform = glGetUniformLocation(tr.gShaderProgramObject, "u_viewMatrix");
	tr.perspectiveUniform = glGetUniformLocation(tr.gShaderProgramObject, "u_perspectiveMatrix");
	//shadowUniform = glGetUniformLocation(gShaderProgramObject, "u_shadowMatrix");

	tr.lightAmbientUniform = glGetUniformLocation(tr.gShaderProgramObject, "u_lightAmbient");
	tr.lightDiffuseUniform = glGetUniformLocation(tr.gShaderProgramObject, "u_lightDiffuse");
	tr.lightSpecularUniform = glGetUniformLocation(tr.gShaderProgramObject, "u_lightSpecular");
	tr.lightPositionUniform = glGetUniformLocation(tr.gShaderProgramObject, "u_lightPosition"); 

	tr.materialAmbientUniform = glGetUniformLocation(tr.gShaderProgramObject, "u_k_materialAmbient");
	tr.materialDiffuseUniform = glGetUniformLocation(tr.gShaderProgramObject, "u_k_materialDiffuse");
	tr.materialSpecularUniform = glGetUniformLocation(tr.gShaderProgramObject, "u_k_materialSpecular");
	tr.materialPositionUniform = glGetUniformLocation(tr.gShaderProgramObject, "u_k_materialShininess");

	tr.belndImageSamplerUniform = glGetUniformLocation(tr.gShaderProgramObject, "u_blendSampler");
	tr.cementSamplerUniform = glGetUniformLocation(tr.gShaderProgramObject, "u_cementSampler");
	tr.dirtSamplerUniform = glGetUniformLocation(tr.gShaderProgramObject, "u_dirtSampler");
	tr.gressSamplerUniform = glGetUniformLocation(tr.gShaderProgramObject, "u_grassSampler");
	tr.mudSamplerUniform = glGetUniformLocation(tr.gShaderProgramObject, "u_mudSampler");
	//shadowSamplerUniform = glGetUniformLocation(gShaderProgramObject, "u_ShadowSampler");

	generateTerrainFromHeightMap(coords);
	//getVegatationPointsFromImage();

	return(0);
}

void TerrainDisplay(glm::mat4 viewMatrix, glm::mat4 perspectiveMatrix, /*glm::mat4 shadowSBPVMatrix, GLuint depthTex, */ glm::vec4 lightPos)
{
	glUseProgram(tr.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity
	glm::mat4 modelMatrix;// = glm::mat4(1.0);
	glm::mat4 translationMatrix = glm::mat4(1.0);

	//Do Necessary Transformation Code Here
	modelMatrix = glm::translate(translationMatrix, glm::vec3(0.0f, 0.0f, 0.0f));

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(tr.modelUniform, 1, GL_FALSE, glm::value_ptr(modelMatrix));
	glUniformMatrix4fv(tr.viewUniform, 1, GL_FALSE, glm::value_ptr(viewMatrix));
	glUniformMatrix4fv(tr.perspectiveUniform, 1, GL_FALSE, glm::value_ptr(perspectiveMatrix));
	
	//glUniformMatrix4fv(shadowUniform, 1, GL_FALSE, glm::value_ptr(shadowSBPVMatrix * modelMatrix));

	glUniform3fv(tr.lightAmbientUniform, 1, TERRAIN_LIGHT_AMBIENT);
	glUniform3fv(tr.lightDiffuseUniform, 1, TERRAIN_LIGHT_DIFFUSE);
	glUniform3fv(tr.lightSpecularUniform, 1, TERRAIN_LIGHT_SPECULAR);
	glUniform3fv(tr.lightPositionUniform, 1, glm::value_ptr(lightPos));

	glUniform3fv(tr.materialAmbientUniform, 1, TERRAIN_MATERIAL_AMBIENT);
	glUniform3fv(tr.materialDiffuseUniform, 1, TERRAIN_MATERIAL_DIFFUSE);
	glUniform3fv(tr.materialSpecularUniform, 1, TERRAIN_MATERIAL_SPECULAR);
	glUniform1f(tr.materialSpecularUniform, TERRAIN_MATERIAL_SHININESS);

	glUniform1i(tr.belndImageSamplerUniform, 0);
	glUniform1i(tr.cementSamplerUniform, 1);
	glUniform1i(tr.dirtSamplerUniform, 2);
	glUniform1i(tr.gressSamplerUniform, 3);
	glUniform1i(tr.mudSamplerUniform, 4);
	//glUniform1i(shadowSamplerUniform, 5);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tr.blendMapTexture);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, tr.cenentTexture);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, tr.dirtTexture);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, tr.gressTexture);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, tr.mudTexture);

	//glActiveTexture(GL_TEXTURE5);
	//glBindTexture(GL_TEXTURE_2D, depthTex);

	//BindWith vao
	glBindVertexArray(tr.vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tr.vbo_Elements);
	glDrawElements(GL_TRIANGLES, tr.numElements, GL_UNSIGNED_INT, (void*)NULL);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	//UnBind vao
	glBindVertexArray(0);

	//UnUse Program
	glUseProgram(0);
}

void TerrainUninitialize(void)
{
	// code
	if (tr.vao)
	{
		glDeleteVertexArrays(1, &tr.vao);
		tr.vao = 0;

		if (tr.vbo_Vertices)
		{
			glDeleteBuffers(1, &tr.vbo_Vertices);
			tr.vbo_Vertices = 0;
		}

		if (tr.vbo_Normals)
		{
			glDeleteBuffers(1, &tr.vbo_Normals);
			tr.vbo_Normals = 0;
		}

		if (tr.vbo_TexCoords)
		{
			glDeleteBuffers(1, &tr.vbo_TexCoords);
			tr.vbo_TexCoords = 0;
		}

		if (tr.vbo_Elements)
		{
			glDeleteBuffers(1, &tr.vbo_Elements);
			tr.vbo_Elements = 0;
		}
	}

	if (tr.gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(tr.gShaderProgramObject);

		// Ask program how many shader are attached to you
		glGetProgramiv(tr.gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(tr.gShaderProgramObject, shaderCount, &shaderCount, pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(tr.gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(tr.gShaderProgramObject);
		tr.gShaderProgramObject = 0;

		glUseProgram(0);
	}
}

void TerrainUpdate(void)
{

}

void generateTerrainFromHeightMap(float* coords)
{
	tr.numVertices = tr.iHeightMap_Width * tr.iHeightMap_Height;
	tr.numElements = (tr.iHeightMap_Width - 1) * (tr.iHeightMap_Height - 1) * 6;

	tr.heightInfo = (GLfloat*)calloc(tr.numVertices, sizeof(GLfloat));

	tr.X = coords[0] * TERRAIN_SIZE;
	tr.Z = coords[1] * TERRAIN_SIZE;

	tr.fWidth = TERRAIN_SIZE;
	tr.fHeight = TERRAIN_SIZE;

	GLfloat* vertices = (GLfloat*)calloc(tr.numVertices * 3, sizeof(GLfloat));
	GLfloat* normals = (GLfloat*)calloc(tr.numVertices * 3, sizeof(GLfloat));
	GLfloat* texCoords = (GLfloat*)calloc(tr.numVertices * 2, sizeof(GLfloat));
	GLuint* elements = (GLuint*)calloc(tr.numElements, sizeof(GLuint));

	int vertexPointer = 0;
	for (int p = 0; p < tr.iHeightMap_Height; p++)
	{
		for (int q = 0; q < tr.iHeightMap_Width; q++)
		{
			vertices[vertexPointer * 3 + 0] = (GLfloat)q / ((GLfloat)tr.iHeightMap_Width - 1) * TERRAIN_SIZE + tr.X;
			vertices[vertexPointer * 3 + 1] = getHeightFromHeightMap(q, p);
			vertices[vertexPointer * 3 + 2] = (GLfloat)p / ((GLfloat)tr.iHeightMap_Height - 1) * TERRAIN_SIZE + tr.Z;

			tr.heightInfo[vertexPointer] = vertices[vertexPointer * 3 + 1];
		
			texCoords[vertexPointer * 2 + 0] = (GLfloat)q / ((GLfloat)tr.iHeightMap_Width - 1);
			texCoords[vertexPointer * 2 + 1] = (GLfloat)p / ((GLfloat)tr.iHeightMap_Width - 1);

			vertexPointer++;
		}
	}

	for (int p = 0; p < tr.iHeightMap_Height - 1; p++)
	{
		for (int q = 0; q < tr.iHeightMap_Width - 1; q++)
		{
			int leftTop = (p * tr.iHeightMap_Height + q) * 3;
			int rightTop = (p * tr.iHeightMap_Height + q + 1) * 3;
			int rightBottom = ((p + 1) * tr.iHeightMap_Height + q + 1) * 3;
			int leftBottom = ((p + 1) * tr.iHeightMap_Height + q) * 3;

			glm::vec3 rightTop_v3 = glm::vec3(vertices[rightTop + 0], vertices[rightTop + 1], vertices[rightTop + 2]);
			glm::vec3 leftTop_v3 = glm::vec3(vertices[leftTop + 0], vertices[leftTop + 1], vertices[leftTop + 2]);
			glm::vec3 leftBottom_v3 = glm::vec3(vertices[leftBottom + 0], vertices[leftBottom + 1], vertices[leftBottom + 2]);
			glm::vec3 rightBottom_v3 = glm::vec3(vertices[rightBottom + 0], vertices[rightBottom + 1], vertices[rightBottom + 2]);

			glm::vec3 n1 = glm::normalize(getTerrainNormalVector(rightTop_v3, leftTop_v3, leftBottom_v3));
			glm::vec3 n2 = glm::normalize(getTerrainNormalVector(rightTop_v3, leftBottom_v3, rightBottom_v3));

			glm::vec3 n_lt_v3 = glm::vec3(normals[leftTop + 0], normals[leftTop + 1], normals[leftTop + 2]);
			glm::vec3 n_rt_v3 = glm::vec3(normals[rightTop + 0], normals[rightTop + 1], normals[rightTop + 2]);
			glm::vec3 n_rb_v3 = glm::vec3(normals[rightBottom + 0], normals[rightBottom + 1], normals[rightBottom + 2]);
			glm::vec3 n_lb_v3 = glm::vec3(normals[leftBottom + 0], normals[leftBottom + 1], normals[leftBottom + 2]);

			glm::vec3 fn_rt_v3 = glm::normalize(n_rt_v3 + n1 + n2);
			glm::vec3 fn_lt_v3 = glm::normalize(n_lt_v3 + n1);
			glm::vec3 fn_lb_v3 = glm::normalize(n_lb_v3 + n1 + n2);
			glm::vec3 fn_rb_v3 = glm::normalize(n_rb_v3 + n2);

			normals[leftTop + 0] = fn_lt_v3.x;
			normals[leftTop + 1] = fn_lt_v3.y;
			normals[leftTop + 2] = fn_lt_v3.z;

			normals[rightTop + 0] = fn_rt_v3.x;
			normals[rightTop + 1] = fn_rt_v3.y;
			normals[rightTop + 2] = fn_rt_v3.z;

			normals[rightBottom + 0] = fn_rb_v3.x;
			normals[rightBottom + 1] = fn_rb_v3.y;
			normals[rightBottom + 2] = fn_rb_v3.z;

			normals[leftBottom + 0] = fn_lb_v3.x;
			normals[leftBottom + 1] = fn_lb_v3.y;
			normals[leftBottom + 2] = fn_lb_v3.z;
		}
	}

	vertexPointer = 0;
	for (int q = 0; q < tr.iHeightMap_Height; q++)
	{
		for (int p = 0; p < tr.iHeightMap_Width; p++)
		{
			GLfloat h = getHeight(p, q);

			GLfloat hR = getHeight(p - 1, q);
			GLfloat hL = getHeight(p + 1, q);
			GLfloat hU = getHeight(p, q - 1);
			GLfloat hD = getHeight(p, q + 1);

			GLfloat cTR = getHeight(p - 1, q - 1);
			GLfloat cTL = getHeight(p + 1, q - 1);
			GLfloat cBR = getHeight(p - 1, q + 1);
			GLfloat cBL = getHeight(p + 1, q + 1);

			GLfloat finalHeight = h / 4.0f + (hR + hL + hU + hD) / 16.0f + (cTR + cTL + cBR + cBL) / 32.0f;
			int pos = p + q * tr.iHeightMap_Width;

			tr.heightInfo[pos] = finalHeight;
			vertices[vertexPointer * 3 + 1] = finalHeight;

			vertexPointer++;
		}
	}

	int elementPointer = 0;
	for (int p = 0; p < tr.iHeightMap_Height - 1; p++)
	{
		for (int q = 0; q < tr.iHeightMap_Width - 1; q++)
		{
			int topLeft = (p * tr.iHeightMap_Width) + q;
			int topRight = topLeft + 1;
			int bottomLeft = ((p + 1) * tr.iHeightMap_Width + q);
			int bottomRight = bottomLeft + 1;

			elements[elementPointer++] = topLeft;
			elements[elementPointer++] = bottomLeft;
			elements[elementPointer++] = topRight;
			elements[elementPointer++] = topRight;
			elements[elementPointer++] = bottomLeft;
			elements[elementPointer++] = bottomRight;
		}
	}

	glCreateVertexArrays(1, &tr.vao);
	glBindVertexArray(tr.vao);

	glCreateBuffers(1, &tr.vbo_Vertices);
	glBindBuffer(GL_ARRAY_BUFFER, tr.vbo_Vertices);
	glBufferData(GL_ARRAY_BUFFER, (tr.numVertices) * 3 * sizeof(GLfloat), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(RMB_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, (void*)NULL);
	glEnableVertexAttribArray(RMB_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glCreateBuffers(1, &tr.vbo_Normals);
	glBindBuffer(GL_ARRAY_BUFFER, tr.vbo_Normals);
	glBufferData(GL_ARRAY_BUFFER, (tr.numVertices) * 3 * sizeof(GLfloat), normals, GL_STATIC_DRAW);
	glVertexAttribPointer(RMB_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, (void*)NULL);
	glEnableVertexAttribArray(RMB_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glCreateBuffers(1, &tr.vbo_TexCoords);
	glBindBuffer(GL_ARRAY_BUFFER, tr.vbo_TexCoords);
	glBufferData(GL_ARRAY_BUFFER, (tr.numVertices) * 2 * sizeof(GLfloat), texCoords, GL_STATIC_DRAW);
	glVertexAttribPointer(RMB_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, (void*)NULL);
	glEnableVertexAttribArray(RMB_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glCreateBuffers(1, &tr.vbo_Elements);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tr.vbo_Elements);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, tr.numElements * sizeof(GLfloat), elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	
	tr.blendMapTexture = terrainLoadTexture(".//resources//textures//terrain//BM2.png");
	tr.cenentTexture = terrainLoadTexture(".//resources//textures//terrain//Blood.png");
	tr.dirtTexture = terrainLoadTexture(".//resources//textures//terrain//Dirt_256x256.png");
	tr.gressTexture = terrainLoadTexture(".//resources//textures//terrain//Grass_256x256.png");
	tr.mudTexture = terrainLoadTexture(".//resources//textures//terrain//Dirt_256x256.png");

	free(vertices);
	vertices = NULL;

	free(normals);
	normals = NULL;

	free(texCoords);
	texCoords = NULL;

	free(elements);
	elements = NULL;
}

glm::vec3 getTerrainNormalVector(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3)
{
	glm::vec3 normal(0.0f);

	float x12 = p1.x - p2.x;
	float x32 = p3.x - p2.x;

	float y12 = p1.y - p2.y;
	float y32 = p3.y - p2.y;

	float z12 = p1.z - p2.z;
	float z32 = p3.z - p2.z;

	normal.x = y32 * z12 - z32 * y12;
	normal.y = -1 * (x32 * z12 - z32 * x12);
	normal.z = x32 * y12 - y32 * x12;

	return(normal);
}

GLfloat getHeightFromHeightMap(int x, int z)
{
	GLfloat maxPixelColor = 3.0f;

	if (x < 0 || x >= tr.iHeightMap_Width || z < 0 || z >= tr.iHeightMap_Height)
	{
		return(0.0f);
	}

	unsigned int pos = (x + z * tr.iHeightMap_Width) * tr.iHeightMap_Chennels;
	unsigned char r = tr.ucHeightMap_ImageData[pos + 0];
	unsigned char g = tr.ucHeightMap_ImageData[pos + 1];
	unsigned char b = tr.ucHeightMap_ImageData[pos + 2];
	GLfloat height = ((GLfloat)r + (GLfloat)g + (GLfloat)b) / 256.0f - 1.5f;

	return(height * TERRAIN_MAX_HEIGHT);
}

GLfloat getHeight(int x, int z) 
{
	if (x < 0 || x >= tr.iHeightMap_Width || z < 0 || z >= tr.iHeightMap_Height)
	{
		return(0.0f);
	}

	int pos = x + tr.iHeightMap_Width * z;

	return(tr.heightInfo[pos]);
}

GLfloat barryCentric(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec2 pos)
{
	float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);

	float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
	float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
	float l3 = 1.0f - l1 - l2;

	return(l1 * p1.y + l2 * p2.y + l3 * p3.y);
}

GLfloat getTerrainHeightAtPosition(GLfloat x, GLfloat z)
{
	GLfloat terrainX = x - tr.X;
	GLfloat terrainZ = z - tr.Z;

	float gridSquareSize = TERRAIN_SIZE / ((float)tr.iHeightMap_Width - 1);

	int xPart = (int)glm::floor(terrainX / gridSquareSize);
	int zPart = (int)glm::floor(terrainZ / gridSquareSize);

	if (xPart >= tr.iHeightMap_Width - 1 || xPart < 0 || zPart < 0 || zPart >= tr.iHeightMap_Height)
	{
		return(0.0f);
	}

	float xCoord = (terrainX - (int)(terrainX / gridSquareSize) * gridSquareSize) / gridSquareSize;
	float zCoord = (terrainZ - (int)(terrainZ / gridSquareSize) * gridSquareSize) / gridSquareSize;

	float answer = 0.0f;

	int pos_x_z = xPart + zPart * tr.iHeightMap_Width;
	int pos_xp1_z = xPart + 1 + zPart * tr.iHeightMap_Width;
	int pos_x_zp1 = xPart + (zPart + 1) * tr.iHeightMap_Width;
	int pos_xp1_zp1 = xPart + 1 + (zPart + 1) * tr.iHeightMap_Width;

	if (xCoord <= (1 - zCoord))
	{
		answer = barryCentric(
			glm::vec3(0, tr.heightInfo[pos_x_z], 0),
			glm::vec3(0, tr.heightInfo[pos_x_zp1], 1),
			glm::vec3(1, tr.heightInfo[pos_xp1_z], 0),
			glm::vec2(xCoord, zCoord)
		);
	}
	else
	{
		answer = barryCentric(
			glm::vec3(0, tr.heightInfo[pos_x_zp1], 1),
			glm::vec3(1, tr.heightInfo[pos_xp1_zp1], 1),
			glm::vec3(1, tr.heightInfo[pos_xp1_z], 0),
			glm::vec2(xCoord, zCoord)
		);
	}

	return(answer);
}

/*void getVegatationPointsFromImage(void)
{
	int height, width, chennels;

	unsigned char* vegetationHeightData = stbi_load(".//resources//textures//terrain//BM2.png", &width, &height, &chennels, 0);

	for (int p = 0; p < height; p = p + 4)
	{
		for (int q = 0; q < width; q = q + 4)
		{
			if (p < width && q < height)
			{
				unsigned int pos = (q + p * width) * chennels;
				unsigned char r = vegetationHeightData[pos + 0];
				unsigned char g = vegetationHeightData[pos + 1];
				unsigned char b = vegetationHeightData[pos + 2];

				GLfloat float_r = (GLfloat)r;
				GLfloat float_g = (GLfloat)g;
				GLfloat float_b = (GLfloat)b;

				if (float_g > 192.0f)
				{
					GLfloat x = (GLfloat)p * TERRAIN_SIZE / (GLfloat)width - 150.0f;
					GLfloat z = (GLfloat)q * TERRAIN_SIZE / (GLfloat)height - 150.0f;

					tr.vegetationPoints.push_back(glm::vec3(x, getTerrainHeightAtPosition(x, z) + 0.4f, z));
				}
			}
		}
	}
}*/

void drawForDepth(void)
{
	glBindVertexArray(tr.vao);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tr.vbo_Elements);
	glDrawElements(GL_TRIANGLES, tr.numElements, GL_UNSIGNED_INT, (void*)NULL);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
}

unsigned int terrainLoadTexture(char const* path)
{
	unsigned int textureID;

	glGenTextures(1, &textureID);

	int width, height, nrComponents;
	unsigned char* data = stbi_load(path, &width, &height, &nrComponents, 0);
	if (data)
	{
		GLenum format;
		if (nrComponents == 1)
		{
			format = GL_RED;
		}
		else if (nrComponents == 3)
		{
			format = GL_RGB;
		}
		else if (nrComponents == 4)
		{
			format = GL_RGBA;
		}

		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, /*format == GL_RGBA ? GL_CLAMP_TO_EDGE :*/ GL_REPEAT);
		// for this tutorial: use GL_CLAMP_TO_EDGE to prevent semi-transparent borders. 
		//Due to interpolation it takes texels from next repeat 
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, /*format == GL_RGBA ? GL_CLAMP_TO_EDGE :*/ GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}
	else
	{
		stbi_image_free(data);
	}

	return(textureID);
}
