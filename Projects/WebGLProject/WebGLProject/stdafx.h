
#ifndef STDAFX_H 
#define STDAFX_H

#pragma once

#include <windows.h>

#include <stdio.h>

#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>

#include <gl/GLEW.h>
#include <gl/GL.h>

#include <ft2build.h>	// Free type 2 build as FT_FREETYPE_H
#include FT_FREETYPE_H

#include "LogFile.h"

#include "stb_image.h"
#include "Camera.h"

#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtc/type_ptr.hpp"
#include "gtc/quaternion.hpp"

#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

// screen size settings
#define WIN_HEIGHT   600
#define WIN_WIDTH    800

enum
{
	RMB_ATTRIBUTE_POSITION = 0,
	RMB_ATTRIBUTE_COLOR,
	RMB_ATTRIBUTE_NORMAL,
	RMB_ATTRIBUTE_TEXTURE0
};


#endif

