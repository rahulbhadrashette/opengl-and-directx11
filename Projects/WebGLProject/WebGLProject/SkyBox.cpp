#pragma once
#include "SkyBox.h"

using namespace std;

struct SKYBOX
{
	GLuint gShaderProgramObject;

	GLuint vaoSkyBox;
	GLuint vboSkyBox;

	GLuint viewUniform;
	GLuint projectionUniform;

	GLuint cubemapTexture;
	GLuint samplerUniform;
};

struct SKYBOX skyBox_t;

unsigned int loadCubemap(vector<std::string> faces);

int SkyBoxInitialize(HWND ghwnd)
{
    GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLint iShaderCompileStatus;
	GLint iProgramLinkStatus;
	GLint iInfoLogLength;
	GLchar *szInfoLog;
    // Define vertex shader object
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// write vertex shader code
	const GLchar *vertexShaderSourceCode = {

		" #version 460 core 														\n"

		" in vec3 vPosition; 														\n"

		" out vec3 texCoord;														\n"

		" uniform mat4 viewMatrix;													\n"
		" uniform mat4 projectionMatrix;											\n"

		" void main(void)															\n"
		" {																			\n"
		"	texCoord = vPosition;													\n"
		" 	vec4 position = projectionMatrix * viewMatrix * vec4(vPosition, 1.0);	\n"
		"	gl_Position = position.xyww;											\n"
		" }																			\n"
	};

	// Specifing above source to the vertex shader object
	glShaderSource(vertexShaderObject, 1, (const GLchar**) &vertexShaderSourceCode, NULL);

	// Compile the vertex shader
	glCompileShader(vertexShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if(iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				LogFile("SkyBox VS :- ");
				LogFile((char*)szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Define Fragment Shader Object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode = {
		
		" #version 460 core													\n"

		" in vec3 texCoord;													\n"

		" uniform samplerCube samplerSkybox;										\n"

		" out vec4 fragColor;												\n"

		" void main(void)													\n"
		" {																	\n"
		" 	fragColor = texture(samplerSkybox, texCoord);					\n"
		" }																	\n"
	};

	// Specifing above source to the Fragment Shader Object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**) &fragmentShaderSourceCode, NULL);

	// Compile the Fragment Shader Object
	glCompileShader(fragmentShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if(iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				LogFile("SkyBox FS :- ");
				LogFile((char*)szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Create Shader Object
	skyBox_t.gShaderProgramObject = glCreateProgram();

	// Attach vertexShaderObject to ShaderProgramObject
	glAttachShader(skyBox_t.gShaderProgramObject, vertexShaderObject);
	// Attach fragmentShaderObject to ShaderProgramObject
	glAttachShader(skyBox_t.gShaderProgramObject, fragmentShaderObject);

	// Pre Linking Binding to vertex attribute
	glBindAttribLocation(skyBox_t.gShaderProgramObject, RMB_ATTRIBUTE_POSITION, "vPosition");

	//Link the shader program
	glLinkProgram(skyBox_t.gShaderProgramObject);

	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(skyBox_t.gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if(iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(skyBox_t.gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(skyBox_t.gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				LogFile("SkyBox LS :- ");
				LogFile((char*)szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Post Linking Retriving Uniform Location
	skyBox_t.viewUniform = glGetUniformLocation(skyBox_t.gShaderProgramObject, "viewMatrix");
	skyBox_t.projectionUniform = glGetUniformLocation(skyBox_t.gShaderProgramObject, "projectionMatrix");

	skyBox_t.samplerUniform = glGetUniformLocation(skyBox_t.gShaderProgramObject, "samplerSkybox");

	// triangle Array
	const GLfloat skyboxVertices[] = {
										// positions          
										-1.0f,  1.0f, -1.0f,
										-1.0f, -1.0f, -1.0f,
										1.0f, -1.0f, -1.0f,
										1.0f, -1.0f, -1.0f,
										1.0f,  1.0f, -1.0f,
										-1.0f,  1.0f, -1.0f,

										-1.0f, -1.0f,  1.0f,
										-1.0f, -1.0f, -1.0f,
										-1.0f,  1.0f, -1.0f,
										-1.0f,  1.0f, -1.0f,
										-1.0f,  1.0f,  1.0f,
										-1.0f, -1.0f,  1.0f,

										1.0f, -1.0f, -1.0f,
										1.0f, -1.0f,  1.0f,
										1.0f,  1.0f,  1.0f,
										1.0f,  1.0f,  1.0f,
										1.0f,  1.0f, -1.0f,
										1.0f, -1.0f, -1.0f,

										-1.0f, -1.0f,  1.0f,
										-1.0f,  1.0f,  1.0f,
										1.0f,  1.0f,  1.0f,
										1.0f,  1.0f,  1.0f,
										1.0f, -1.0f,  1.0f,
										-1.0f, -1.0f,  1.0f,

										-1.0f,  1.0f, -1.0f,
										1.0f,  1.0f, -1.0f,
										1.0f,  1.0f,  1.0f,
										1.0f,  1.0f,  1.0f,
										-1.0f,  1.0f,  1.0f,
										-1.0f,  1.0f, -1.0f,

										-1.0f, -1.0f, -1.0f,
										-1.0f, -1.0f,  1.0f,
										1.0f, -1.0f, -1.0f,
										1.0f, -1.0f, -1.0f,
										-1.0f, -1.0f,  1.0f,
										1.0f, -1.0f,  1.0f
    							};

	// Create vao
	glGenVertexArrays(1, &skyBox_t.vaoSkyBox);
	glBindVertexArray(skyBox_t.vaoSkyBox);

	glGenBuffers(1, &skyBox_t.vboSkyBox);
	glBindBuffer(GL_ARRAY_BUFFER, skyBox_t.vboSkyBox);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), skyboxVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(RMB_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)NULL);
	glEnableVertexAttribArray(RMB_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	vector<std::string> faces
	{
		".//resources//textures//skybox//skyright.jpg",
		".//resources//textures//skybox//skyleft.jpg",
		".//resources//textures//skybox//skytop.jpg",
		".//resources//textures//skybox//skybottom.jpg",
		".//resources//textures//skybox//skyback.jpg",
		".//resources//textures//skybox//skyfront.jpg"
	};


	skyBox_t.cubemapTexture = loadCubemap(faces);

    return(0);
}

void SkyBoxDisplay(glm::mat4 viewMatrix, glm::mat4 projectionMatrix)
{
    
	glDepthFunc(GL_LEQUAL);
    glUseProgram(skyBox_t.gShaderProgramObject);

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(skyBox_t.viewUniform, 1, GL_FALSE, glm::value_ptr(viewMatrix));
	glUniformMatrix4fv(skyBox_t.projectionUniform, 1, GL_FALSE, glm::value_ptr(projectionMatrix));

	glUniform1i(skyBox_t.samplerUniform, 0);

	//BindWith vao
	glBindVertexArray(skyBox_t.vaoSkyBox);

	glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skyBox_t.cubemapTexture);

	//Draw the Necessary Scnes
	glDrawArrays(GL_TRIANGLES, 0, 36);
	//UnBind vao
	glBindVertexArray(0);
	//glDepthMask(GL_TRUE);
	glDepthFunc(GL_LESS); // set depth function back to default

	//UnUse Program
	glUseProgram(0);
}

void SkyBoxUninitialize(void)
{
    // code
	if(skyBox_t.vaoSkyBox)
	{
		if(skyBox_t.vboSkyBox)
		{
			glDeleteBuffers(1, &skyBox_t.vboSkyBox);
			skyBox_t.vboSkyBox = 0;
		}

		glDeleteVertexArrays(1, &skyBox_t.vaoSkyBox);
		skyBox_t.vaoSkyBox = 0;
	}

	if(skyBox_t.gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(skyBox_t.gShaderProgramObject);

		// Ask program how many shader are attached to you
		glGetProgramiv(skyBox_t.gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);

		if(pShaders)
		{
			glGetAttachedShaders(skyBox_t.gShaderProgramObject, shaderCount, &shaderCount, pShaders);

			for(shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(skyBox_t.gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(skyBox_t.gShaderProgramObject);
		skyBox_t.gShaderProgramObject = 0;

		glUseProgram(0);
	}
}

void SkyBoxUpdate(void)
{

}

// loads a cubemap texture from 6 individual texture faces
// order:
// +X (right)
// -X (left)
// +Y (top)
// -Y (bottom)
// +Z (front) 
// -Z (back)
// -------------------------------------------------------
unsigned int loadCubemap(vector<std::string> faces)
{
    unsigned int textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

    int width, height, nrComponents;
    for (unsigned int i = 0; i < faces.size(); i++)
    {
        unsigned char *data = stbi_load(faces[i].c_str(), &width, &height, &nrComponents, 0);
        if (data)
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
            stbi_image_free(data);
            LogFile("Success to load textures\n");
        }
        else
        {
            LogFile("Failed to load textures\n");
            stbi_image_free(data);
        }
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    return textureID;
}



