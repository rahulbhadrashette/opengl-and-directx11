#ifndef MODELLOADING_H 
#define MODELLOADING_H

#pragma once

#include "stdafx.h"
#include "Main.h"

int ModelLoadingInitialize(HWND);
void ModelLoadingDisplay(glm::mat4, glm::mat4);
void ModelLoadingUninitialize(void);
void ModelLoadingUpdate(void);

#endif

