#pragma once
#include "stdafx.h"

FILE* gpFile = NULL;
char rmb_LogFile[] = "Logfile.txt";

void Log_Initialize(void)
{
	//code
	// Create File io code
	if (fopen_s(&gpFile, rmb_LogFile, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created...!!!"), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log file is created successfully...!!!\n");
		fclose(gpFile);
	}

}

void LogFile(const char* str)
{
	fopen_s(&gpFile, rmb_LogFile, "a+");
	fprintf_s(gpFile, str);
	fclose(gpFile);
}
