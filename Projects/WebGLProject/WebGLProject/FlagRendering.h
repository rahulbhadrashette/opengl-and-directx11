
#include "stdafx.h"
using std::endl;
using std::cerr;

using std::vector;

using glm::vec3;

void FlagInitialization(HWND);
void FlagDisplay(glm::mat4, glm::mat4);
void FlagUninitialize(void);
void FlagUpdate(void);

