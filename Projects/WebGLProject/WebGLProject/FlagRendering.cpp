#pragma once
#include "FlagRendering.h"
#include "LogFile.h"

#define PRIM_RESTART 0xffffff
#define BUFFER_SIZE		256
#define S_EQUAL			0

#define NR_TEXTURE_COORDS	2
#define NR_POINT_COORDS		3
#define NR_NORMAL_COORDS	3
#define NR_FACE_TOKENS		3

FILE *gpMeshFile_Flag;

GLuint gShaderProgramObject;
GLuint gComputeProgramObject;
GLuint gNormalComputeProgramObject;

GLuint clothVao;
GLuint clothVbo[7];

GLuint positionBuffers[2];
GLuint velocityBuffers[2];

GLuint normalBufffer;
GLuint elementBuffer;
GLuint textureBuffer;
GLuint readBuffer;

// Number of particales ineach dimention
glm::ivec2 numParticles(40, 40); 	// Number of particales ineach dimention
glm::vec2 clothSize(6.0f, 4.0f);		// Size of cloth in x and y
GLuint numElements = 0;

glm::vec3 gravity(7.0f, 0.6f, 0.0f);

float dx = (clothSize.x / (numParticles.x-1));
float dy = (clothSize.y / (numParticles.y-1));
float ds = (1.0f / (numParticles.x - 1));
float dt = (1.0f / (numParticles.y - 1));

GLfloat time = 0.0f;
GLfloat deltaT = 0.0f;
GLfloat speed = 200.0f;

GLfloat fAngle = 0.0f;
GLfloat t = 0.0f;

GLuint ModelUniform;
GLuint ViewUniform;
GLuint ProjectionUniform;

// lights
GLuint laUniform;
GLuint ldUniform;
GLuint lsUniform;

GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;

GLuint lKeyPressed;

GLuint lightPositionUniform;
GLuint shininessUniform;

//texture
GLuint texturePole;
GLuint textureBase;
GLuint textureTriColor;
GLuint samplerUniform;

// compute shader uniform	
GLuint resetLengthHorizUniform;
GLuint resetLengthVertUniform;
GLuint resetLengthDiagUniform;

GLuint vao_Quad;
GLuint vbo_QuadPosition;
GLuint vbo_QuadTexture;
GLuint vbo_QuadNormal;

std::vector<glm::vec3> vertices_Flag;
std::vector<glm::vec3> normals_Flag;
std::vector<glm::vec2> uvs_Flag;

char gLine[BUFFER_SIZE];

void FlagInitialization(HWND ghwnd)
{
    unsigned int loadTexture(char const* path);
    void vertexfragmentShaderFunction(HWND);
    void computeShaderFunction(HWND);
    void normalComputeShaderFunction(HWND);
    void initBuffer(void);

    vertexfragmentShaderFunction(ghwnd);
    computeShaderFunction(ghwnd);
    normalComputeShaderFunction(ghwnd);
    initBuffer();

    textureBase = loadTexture(".//resources//textures//red.png");
	texturePole = loadTexture(".//resources//textures//metal.png");
	textureTriColor = loadTexture(".//resources//textures//Tiranga.jpg");

    // Activate the compute program and bind the position and velocity buffers
	glUseProgram(gComputeProgramObject);
	glUniform1f(resetLengthHorizUniform, dx);
	glUniform1f(resetLengthVertUniform, dy);
	glUniform1f(resetLengthDiagUniform, sqrtf(dx * dy + dy * dx));
}

void FlagDisplay(glm::mat4 view,glm::mat4 projection)
{
    // Activate the compute program and bind the position and velocity buffers
	glUseProgram(gComputeProgramObject);

	for( int i = 0; i < 1000; i++ ) 
	{
		glDispatchCompute(numParticles.x / 10, numParticles.y/10, 1);
		glMemoryBarrier( GL_SHADER_STORAGE_BARRIER_BIT );

		// Swap buffers
		readBuffer = 1 - readBuffer;
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, positionBuffers[readBuffer]);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, positionBuffers[1-readBuffer]);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, velocityBuffers[readBuffer]);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, velocityBuffers[1-readBuffer]);
	}

	//UnUse Program
	glUseProgram(0);

	// Clear, select the rendering program and draw a full screen quad
	glUseProgram(gNormalComputeProgramObject);

	glDispatchCompute(numParticles.x/10, numParticles.y/10, 1);
	glMemoryBarrier( GL_SHADER_STORAGE_BARRIER_BIT );
	//UnUse Program
	glUseProgram(0);

	// Now draw the scene
	glUseProgram(gShaderProgramObject);

	// Light Unifroms
	//glUniform1i(lKeyPressed, 1);
	
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(6.1f, -9.4f, -3.1f));

	glUniformMatrix4fv(ModelUniform, 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(ViewUniform, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(ProjectionUniform, 1, GL_FALSE, glm::value_ptr(projection));

	// setting light's properties
	glUniform3fv(laUniform, 1, glm::value_ptr(glm::vec3(0.0f)));
	glUniform3fv(ldUniform, 1, glm::value_ptr(glm::vec3(1.0f)));
	glUniform3fv(lsUniform, 1, glm::value_ptr(glm::vec3(0.7f)));
	glUniform4fv(lightPositionUniform, 1, glm::value_ptr(glm::vec4(0.0f, 1000.0f, 1000.0f, 1.0f)));

	// setting material's properties
	glUniform3fv(kaUniform, 1, glm::value_ptr(glm::vec3(1.0f)));
	glUniform3fv(kdUniform, 1, glm::value_ptr(glm::vec3(1.0f)));
	glUniform3fv(ksUniform, 1, glm::value_ptr(glm::vec3(0.0f)));
	glUniform1f(shininessUniform, 50.0f);

	// Draw the cloth
	glBindVertexArray(clothVao);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureTriColor);
	glUniform1i(samplerUniform, 0);

	glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_INT, 0);

	glBindVertexArray(0);

	//UnUse Program
	glUseProgram(0);
}

void FlagUpdate(void)
{
    if(time == 0.0f)
	{
		deltaT = 0.0f;

	}
	else
	{
		deltaT = t - time;
	}
	time = t;

	fAngle = fAngle + 0.2f;
	if(fAngle >= 360.0f)
	{
		fAngle = 0.0f;
	}
}

void FlagUninitialize(void)
{
    if(clothVao)
	{
		if (clothVbo)
		{
			glDeleteBuffers(1, &clothVbo[0]);
			glDeleteBuffers(1, &clothVbo[1]);
			glDeleteBuffers(1, &clothVbo[2]);
			glDeleteBuffers(1, &clothVbo[3]);
			glDeleteBuffers(1, &clothVbo[4]);
			glDeleteBuffers(1, &clothVbo[5]);
			glDeleteBuffers(1, &clothVbo[6]);

			clothVbo[0] = 0;
			clothVbo[1] = 0;
			clothVbo[2] = 0;
			clothVbo[3] = 0;
			clothVbo[4] = 0;
			clothVbo[5] = 0;
			clothVbo[6] = 0;
		}

		glDeleteVertexArrays(1, &clothVao);
		clothVao = 0;
	}

	if(gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);

		// Ask program how many shader are attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);

		if(pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);

			for(shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		glUseProgram(0);
	}

	if(gComputeProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gComputeProgramObject);

		// Ask program how many shader are attached to you
		glGetProgramiv(gComputeProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);

		if(pShaders)
		{
			glGetAttachedShaders(gComputeProgramObject, shaderCount, &shaderCount, pShaders);

			for(shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gComputeProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(gComputeProgramObject);
		gComputeProgramObject = 0;

		glUseProgram(0);
	}
}

void vertexfragmentShaderFunction(HWND ghwnd)
{
	// Cloth
	glEnable(GL_PRIMITIVE_RESTART);
	glPrimitiveRestartIndex(PRIM_RESTART);

	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;

	GLint iShaderCompileStatus;
	GLint iProgramLinkStatus;
	GLint iInfoLogLength;
	GLchar *szInfoLog;
	
	// Define vertex shader object
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// write vertex shader code
	const GLchar *vertexShaderSourceCode = {

		"#version 460 core \n"

		"in vec4 vPosition;	\n"
		"in vec3 vNormal; 	\n"
		"in vec2 vTexCoord; \n"

		"uniform mat4 modelMatrix; 		\n"
		"uniform mat4 viewMatrix; 		\n"
		"uniform mat4 projectionMatrix; \n"

		"out vec3 transformedNormal; \n"
		"out vec3 lightDirection; 	 \n"
		"out vec3 viewerVector;		 \n"
		"out vec2 TexCoord; 		 \n"

		"uniform vec4 lightPosition; \n"
		"uniform int lKeyPressed; 	 \n"

		"void main() \n"
		"{ \n"
			//"if(lKeyPressed == 1)\n"
			//"{\n"
				"vec4 eye_Coordinates = viewMatrix * modelMatrix * vPosition; \n"
				"transformedNormal = mat3(viewMatrix * modelMatrix) * vNormal;\n"
				"lightDirection = vec3(lightPosition) - eye_Coordinates.xyz;\n"
				"viewerVector = -eye_Coordinates.xyz;"
			//"}\n"

			"TexCoord = vTexCoord; \n"
			"gl_Position = projectionMatrix * viewMatrix * modelMatrix * vPosition; \n"
		"} \n"
	};

	// Specifing above source to the vertex shader object
	glShaderSource(vertexShaderObject, 1, (const GLchar**) &vertexShaderSourceCode, NULL);

	// Compile the vertex shader
	glCompileShader(vertexShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if(iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				LogFile(" ff VS :- %s ");
				LogFile((char*)szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Define Fragment Shader Object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode = {
		
		"#version 460 core \n"

		"in vec3 transformedNormal; \n"
		"in vec3 lightDirection; 	\n"
		"in vec3 viewerVector;		\n"
		"in vec2 TexCoord; 		 	\n"

		"uniform vec3 La; \n"      		// Ambient reflectivity
		"uniform vec3 Ld; \n"     		// Diffuse reflectivity
		"uniform vec3 Ls; \n"      		// Specular reflectivity

		"uniform vec3 Ka; \n"      		// Ambient reflectivity
		"uniform vec3 Kd; \n"     		// Diffuse reflectivity
		"uniform vec3 Ks; \n"      		// Specular reflectivity

		"uniform float materialShininess; \n"   // Specular shininess factor
		"uniform int lKeyPressed; \n"

		"uniform sampler2D sampler; \n"
		"out vec4 FragColor;	  \n"

		"void main() \n"
		"{ \n"
		"	vec3 phong_Ads_Color;"
		//"	if(lKeyPressed == 1)\n"
		//"	{\n"
		"		vec3 normTransformedNormal = normalize(transformedNormal); 					\n"
		"		vec3 normLightDirection = normalize(lightDirection); 							\n"
		"		vec3 normViewerVector = normalize(viewerVector); 								\n"

		"		float tn_dot_ld = max(dot(normTransformedNormal, normLightDirection), 0.0); 	\n"
		"		vec3 reflectionVector = reflect(-normLightDirection, normTransformedNormal);	\n"

		"		vec3 ambient = La * Ka; 														\n"
		"		vec3 diffuse = Ld * Kd * tn_dot_ld;												\n"
		"		vec3 specular = Ls * Ks * pow(max(dot(reflectionVector, normViewerVector), 0.0), materialShininess);\n"
		"		phong_Ads_Color = ambient + diffuse + specular; 								\n"
		//"	}\n"
		//"	else \n"
		//"	{\n"
		//"		phong_Ads_Color = vec3(1.0, 1.0, 1.0);"
		//"	}\n"
		"	vec4 tex = texture(sampler, TexCoord);\n"
		"	FragColor = vec4(1.0) * tex;\n"
		"} \n"
	};

	// Specifing above source to the Fragment Shader Object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**) &fragmentShaderSourceCode, NULL);

	// Compile the Fragment Shader Object
	glCompileShader(fragmentShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if(iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				LogFile("FS :- %s ");
				LogFile((char*) szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Create Shader Object
	gShaderProgramObject = glCreateProgram();

	// Attach vertexShaderObject to ShaderProgramObject
	glAttachShader(gShaderProgramObject, vertexShaderObject);
	// Attach fragmentShaderObject to ShaderProgramObject
	glAttachShader(gShaderProgramObject, fragmentShaderObject);

	// Pre Linking Binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, RMB_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, RMB_ATTRIBUTE_NORMAL, "vNormal");
	glBindAttribLocation(gShaderProgramObject, RMB_ATTRIBUTE_TEXTURE0, "vTexCoord");

	//Link the shader program
	glLinkProgram(gShaderProgramObject);

	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if(iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				LogFile("LS :- %s ");
				LogFile((char*)szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Post Linking Retriving Uniform Location
	ModelUniform = glGetUniformLocation(gShaderProgramObject, "modelMatrix");
	ViewUniform = glGetUniformLocation(gShaderProgramObject, "viewMatrix");
	ProjectionUniform = glGetUniformLocation(gShaderProgramObject, "projectionMatrix");

	laUniform = glGetUniformLocation(gShaderProgramObject, "La");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "Ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "Ls");

	kaUniform = glGetUniformLocation(gShaderProgramObject, "Ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "Kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "Ks");

	lKeyPressed = glGetUniformLocation(gShaderProgramObject, "lKeyPressed");

	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "lightPosition");
	shininessUniform = glGetUniformLocation(gShaderProgramObject, "materialShininess");
	samplerUniform = glGetUniformLocation(gShaderProgramObject, "sampler");
}

void computeShaderFunction(HWND ghwnd)
{
	GLuint computeShaderObject;

	GLint iShaderCompileStatus;
	GLint iProgramLinkStatus;
	GLint iInfoLogLength;
	GLchar *szInfoLog;

	// Define vertex shader object
	computeShaderObject = glCreateShader(GL_COMPUTE_SHADER);

	// Write Fragment Shader Code
	const GLchar *computeShaderSourceCode = {
		
		"#version 430 core \n"

		"layout( local_size_x = 10, local_size_y = 10 ) in; \n"

		"uniform vec3 Gravity = vec3(7, 0, 0); 		\n"
		"uniform float ParticleMass = 0.1; 			\n"
		"uniform float ParticleInvMass = 1.0 / 0.1; \n"
		"uniform float SpringK = 700.0; 			\n"
		"uniform float RestLengthHoriz; 			\n"
		"uniform float RestLengthVert; 				\n"
		"uniform float RestLengthDiag; 				\n"
		"uniform float DeltaT = 0.000005; 			\n"
		"uniform float DampingConst = 0.2; 			\n"

		"layout(std430, binding=0) buffer PosIn { vec4 PositionIn[]; }; 	\n"
		"layout(std430, binding=1) buffer PosOut { vec4 PositionOut[]; }; 	\n"
		"layout(std430, binding=2) buffer VelIn { vec4 VelocityIn[]; }; 	\n"
		"layout(std430, binding=3) buffer VelOut { vec4 VelocityOut[]; }; 	\n"

		"void main() \n"
		"{ \n"
			"uvec3 nParticles = gl_NumWorkGroups * gl_WorkGroupSize; \n"
			"uint idx = gl_GlobalInvocationID.y * nParticles.x + gl_GlobalInvocationID.x; \n"

			"vec3 p = vec3(PositionIn[idx]); \n"
			"vec3 v = vec3(VelocityIn[idx]), r; \n"

			// Start with gravitational acceleration and add the spring
			// forces from each neighbor
			"vec3 force = Gravity * ParticleMass; \n"

			// Particle above
			"if( gl_GlobalInvocationID.y < nParticles.y - 1 ) \n"
			"{ \n"
				"r = PositionIn[idx + nParticles.x].xyz - p; \n"
				"force += normalize(r) * SpringK * (length(r) - RestLengthVert); \n"
			"} \n"
			// Below
			"if( gl_GlobalInvocationID.y > 0 ) \n"
				"{ \n"
				"r = PositionIn[idx - nParticles.x].xyz - p; \n"
				"force += normalize(r) * SpringK * (length(r) - RestLengthVert); \n"
				"} \n"
			// Left
			"if( gl_GlobalInvocationID.x > 0 ) \n"
			"{ \n"
				"r = PositionIn[idx-1].xyz - p; \n"
				"force += normalize(r) * SpringK * (length(r) - RestLengthHoriz); \n"
			"} \n"
			// Right
			"if( gl_GlobalInvocationID.x < nParticles.x - 1 ) \n"
			"{ \n"
				"r = PositionIn[idx + 1].xyz - p; \n"
				"force += normalize(r) * SpringK * (length(r) - RestLengthHoriz); \n"
			"} \n"

			// Diagonals
			// Upper-left
			"if( gl_GlobalInvocationID.x > 0 && gl_GlobalInvocationID.y < nParticles.y - 1 ) \n"
			"{ \n"
				"r = PositionIn[idx + nParticles.x - 1].xyz - p; \n"
				"force += normalize(r) * SpringK * (length(r) - RestLengthDiag); \n"
			"} \n"
			// Upper-right
			"if( gl_GlobalInvocationID.x < nParticles.x - 1 && gl_GlobalInvocationID.y < nParticles.y - 1) \n"
			"{ \n"
				"r = PositionIn[idx + nParticles.x + 1].xyz - p; \n"
				"force += normalize(r) * SpringK * (length(r) - RestLengthDiag); \n"
			"} \n"
			// lower -left
			"if( gl_GlobalInvocationID.x > 0 && gl_GlobalInvocationID.y > 0 ) \n"
			"{ \n"
				"r = PositionIn[idx - nParticles.x - 1].xyz - p; \n" 
				"force += normalize(r) * SpringK * (length(r) - RestLengthDiag); \n"
			"} \n"
			// lower-right
			"if( gl_GlobalInvocationID.x < nParticles.x - 1 && gl_GlobalInvocationID.y > 0 ) \n"
			"{ \n"
				"r = PositionIn[idx - nParticles.x + 1].xyz - p; \n"
				"force += normalize(r) * SpringK * (length(r) - RestLengthDiag); \n"
			"} \n"

		"force += -DampingConst * v; \n"

		// Apply simple Euler integrator
		"vec3 a = force * ParticleInvMass; \n"
		"PositionOut[idx] = vec4(p + v * DeltaT + 0.5 * a * DeltaT * DeltaT, 1.0); \n"
		"VelocityOut[idx] = vec4( v + a * DeltaT, 0.0); \n"

		// Pin a few of the top verts
		//"if( gl_GlobalInvocationID.y == nParticles.y - 1 && (gl_GlobalInvocationID.x == 0 || gl_GlobalInvocationID.x == nParticles.x / 4 || \n"
		//	"gl_GlobalInvocationID.x == nParticles.x * 2 / 4 || gl_GlobalInvocationID.x == nParticles.x * 3 / 4 || gl_GlobalInvocationID.x == nParticles.x - 1)) \n"
		
		"if( gl_GlobalInvocationID.x == 0 && (gl_GlobalInvocationID.y == 0 ||  gl_GlobalInvocationID.y == nParticles.y * 1 / 5 || \n"
			"gl_GlobalInvocationID.y == nParticles.y * 2 / 5 || gl_GlobalInvocationID.y == nParticles.y * 3 / 5 || \n"
			"gl_GlobalInvocationID.y == nParticles.y * 4 / 5 || gl_GlobalInvocationID.y == nParticles.y - 1)) \n" \

			"{ \n"
				"PositionOut[idx] = vec4(p,1.0); \n"
				"VelocityOut[idx] = vec4(0,0,0,0); \n"
			"} \n"
		"} \n"
	};

	// Specifing above source to the vertex shader object
	glShaderSource(computeShaderObject, 1, (const GLchar**) &computeShaderSourceCode, NULL);

	// Compile the vertex shader
	glCompileShader(computeShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(computeShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if(iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(computeShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(computeShaderObject, iInfoLogLength, &written, szInfoLog);
				LogFile("Flag Compute Shader Compile");
				LogFile((char*)szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Create Shader Object
	gComputeProgramObject = glCreateProgram();

	// Attach vertexShaderObject to ShaderProgramObject
	glAttachShader(gComputeProgramObject, computeShaderObject);

	//Link the shader program
	glLinkProgram(gComputeProgramObject);

	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gComputeProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if(iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gComputeProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gComputeProgramObject, iInfoLogLength, &written, szInfoLog);
				LogFile("Flag Compute Shader LS:- %s ");
				LogFile((char*)szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	resetLengthHorizUniform = glGetUniformLocation(gComputeProgramObject, "RestLengthHoriz");
	resetLengthVertUniform = glGetUniformLocation(gComputeProgramObject, "RestLengthVert");
	resetLengthDiagUniform = glGetUniformLocation(gComputeProgramObject, "RestLengthDiag");
}

void normalComputeShaderFunction(HWND ghwnd)
{
	
	GLint iShaderCompileStatus;
	GLint iProgramLinkStatus;
	GLint iInfoLogLength;
	GLchar *szInfoLog;

	// Define compute shader object
	GLuint normalComputeShaderObject = glCreateShader(GL_COMPUTE_SHADER);

	// Write Fragment Shader Code
	const GLchar *normalComputeShaderSourceCode = {
		
		"#version 460 core \n"

		"layout( local_size_x = 10, local_size_y = 10 ) in; \n"
		"layout(std430, binding=0) buffer PosIn { vec4 Position[]; }; \n"
		"layout(std430, binding=4) buffer NormOut  { vec4 Normal[]; }; \n"

		"void main() \n"
		"{ \n"
			"uvec3 nParticles = gl_NumWorkGroups * gl_WorkGroupSize; \n"
			"uint idx = gl_GlobalInvocationID.y * nParticles.x + gl_GlobalInvocationID.x; \n"

			"vec3 p = vec3(Position[idx]); \n"
			"vec3 n = vec3(0); \n"
			"vec3 a, b, c; \n"

			"if( gl_GlobalInvocationID.y < nParticles.y - 1) \n"
			"{ \n"
				"c = Position[idx + nParticles.x].xyz - p; \n"
				"if( gl_GlobalInvocationID.x < nParticles.x - 1 ) \n"
				"{ \n"
					"a = Position[idx + 1].xyz - p; \n"
					"b = Position[idx + nParticles.x + 1].xyz - p; \n"
					"n += cross(a,b); \n"
					"n += cross(b,c); \n"
				"} \n"
				"if( gl_GlobalInvocationID.x > 0 ) \n"
				"{ \n"
					"a = c; \n"
					"b = Position[idx + nParticles.x - 1].xyz - p; \n"
					"c = Position[idx - 1].xyz - p; \n"
					"n += cross(a,b); \n"
					"n += cross(b,c); \n"
				"} \n"
			"}\n"

			"if( gl_GlobalInvocationID.y > 0 ) \n"
			"{ \n"
				"c = Position[idx - nParticles.x].xyz - p; \n"
				"if( gl_GlobalInvocationID.x > 0 ) \n"
				"{ \n"
					"a = Position[idx - 1].xyz - p; \n"
					"b = Position[idx - nParticles.x - 1].xyz - p; \n"
					"n += cross(a,b); \n"
					"n += cross(b,c); \n"
				"} \n"
				"if( gl_GlobalInvocationID.x < nParticles.x - 1 ) \n"
				"{ \n"
					"a = c; \n"
					"b = Position[idx - nParticles.x + 1].xyz - p; \n"
					"c = Position[idx + 1].xyz - p; \n"
					"n += cross(a,b); \n"
					"n += cross(b,c); \n"
				"} \n"
			"} \n"

			"Normal[idx] = vec4(normalize(n), 0.0); \n"
		"} \n"
	};

	// Specifing above source to the vertex shader object
	glShaderSource(normalComputeShaderObject, 1, (const GLchar**) &normalComputeShaderSourceCode, NULL);

	// Compile the vertex shader
	glCompileShader(normalComputeShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(normalComputeShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if(iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(normalComputeShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(normalComputeShaderObject, iInfoLogLength, &written, szInfoLog);
				LogFile("Flag Normal Compute Shader ");
				LogFile((char*)szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Create Shader Object
	gNormalComputeProgramObject = glCreateProgram();

	// Attach vertexShaderObject to ShaderProgramObject
	glAttachShader(gNormalComputeProgramObject, normalComputeShaderObject);

	//Link the shader program
	glLinkProgram(gNormalComputeProgramObject);

	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gNormalComputeProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if(iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gNormalComputeProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gNormalComputeProgramObject, iInfoLogLength, &written, szInfoLog);
				LogFile("Flag Normal Compute Shader LS ");
				LogFile((char*)szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}
}

unsigned int loadTexture(char const* path)
{
	unsigned int textureID;

	glGenTextures(1, &textureID);

	int width, height, nrComponents;
	unsigned char* data = stbi_load(path, &width, &height, &nrComponents, 0);
	if (data)
	{
		GLenum format;
		if (nrComponents == 1)
		{
			format = GL_RED;
		}
		else if (nrComponents == 3)
		{
			format = GL_RGB;
		}
		else if (nrComponents == 4)
		{
			format = GL_RGBA;
		}

		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, /*format == GL_RGBA ? GL_CLAMP_TO_EDGE :*/ GL_REPEAT);
		// for this tutorial: use GL_CLAMP_TO_EDGE to prevent semi-transparent borders. 
		//Due to interpolation it takes texels from next repeat 
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, /*format == GL_RGBA ? GL_CLAMP_TO_EDGE :*/ GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}
	else
	{
		stbi_image_free(data);
	}

	return(textureID);
}

void initBuffer(void)
{		
	// Initial positions of the particles
	vector<GLfloat> initPos;
	vector<GLfloat> initVel(numParticles.x * numParticles.y * 4, 0.0f);
	vector<GLfloat> initTc;

	glm::vec4 p(0.0f, 0.0f, 0.0f, 1.0f);

	// Initial transform
	glm::mat4 transf = glm::translate(glm::mat4(1.0f), glm::vec3(6.0f, 11.f, -6.0f));
			  transf = glm::translate(transf, glm::vec3(-clothSize.x, 0.0f, 0.0f));
			  transf = glm::rotate(transf, glm::radians(-80.0f), glm::vec3(0.0f, 1.0f, 0.0f));
			  transf = glm::translate(transf, glm::vec3(clothSize.x, 0.0f, 0.0f));
	
	for( int i = 0; i < numParticles.y; i++ ) 
	{
		for( int j = 0; j < numParticles.x; j++ ) 
		{
			p.x = dx * j;
			p.y = dy * i;
			p.z = 1.0f;
			p = transf * p;
			initPos.push_back(p.x);
			initPos.push_back(p.y);
			initPos.push_back(p.z);
			initPos.push_back(1.0f);

			initTc.push_back(ds * j);
			initTc.push_back(dt * i);
		}
	}

	// Every row is one triangle strip
	vector<GLuint> el;

	for( int row = 0; row < numParticles.y - 1; row++ ) 
	{
		for( int col = 0; col < numParticles.x; col++ ) 
		{
			el.push_back( (row+1) * numParticles.x + (col  ) );
			el.push_back( (row  ) * numParticles.x + (col  ) );
		}
		el.push_back(PRIM_RESTART);
	}

	// We need buffers for position (2), element index,
	// velocity (2), normal, and texture coordinates.
	glGenBuffers(7, clothVbo);

	positionBuffers[0] = clothVbo[0];
	positionBuffers[1] = clothVbo[1];
	velocityBuffers[0] = clothVbo[2];
	velocityBuffers[1] = clothVbo[3];
	normalBufffer = clothVbo[4];
	elementBuffer = clothVbo[5];
	textureBuffer = clothVbo[6];

	GLuint parts = numParticles.x * numParticles.y;

	// The buffers for positions
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, positionBuffers[0]);
	glBufferData(GL_SHADER_STORAGE_BUFFER, parts * 4 * sizeof(GLfloat), &initPos[0], GL_DYNAMIC_DRAW);

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, positionBuffers[1]);
	glBufferData(GL_SHADER_STORAGE_BUFFER, parts * 4 * sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);

	// Velocities
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, velocityBuffers[0]);
	glBufferData(GL_SHADER_STORAGE_BUFFER, parts * 4 * sizeof(GLfloat), &initVel[0], GL_DYNAMIC_COPY);

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, velocityBuffers[1]);
	glBufferData(GL_SHADER_STORAGE_BUFFER, parts * 4 * sizeof(GLfloat), NULL, GL_DYNAMIC_COPY);

	// Normal buffer
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, normalBufffer);
	glBufferData(GL_SHADER_STORAGE_BUFFER, parts * 4 * sizeof(GLfloat), NULL, GL_DYNAMIC_COPY);

	// Element indicies
	glBindBuffer(GL_ARRAY_BUFFER, elementBuffer);
	glBufferData(GL_ARRAY_BUFFER, el.size() * sizeof(GLuint), &el[0], GL_DYNAMIC_COPY);

	// Texture coordinates
	glBindBuffer(GL_ARRAY_BUFFER, textureBuffer);
	glBufferData(GL_ARRAY_BUFFER, initTc.size() * sizeof(GLfloat), &initTc[0], GL_STATIC_DRAW);

	numElements = GLuint(el.size());

	// Set up the VAO
	glGenVertexArrays(1, &clothVao);
	glBindVertexArray(clothVao);

	glBindBuffer(GL_ARRAY_BUFFER, positionBuffers[0]);
	glVertexAttribPointer(RMB_ATTRIBUTE_POSITION, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(RMB_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, normalBufffer);
	glVertexAttribPointer(RMB_ATTRIBUTE_NORMAL, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(RMB_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, textureBuffer);
	glVertexAttribPointer(RMB_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(RMB_ATTRIBUTE_TEXTURE0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);
	glBindVertexArray(0);

	const GLfloat quadVertices[] =
	{
		-1.0f, 0.0f, 2.0f,
		-1.15f, 0.0f, -1.0f,
		-1.15f, -0.6f, -1.0f,
		-1.0f, -0.6f, 2.0f,

		1.15f, 0.0f, -1.0f,
		-1.15f, 0.0f, -1.0f, //front
		-1.0f, 0.0f, 2.0f,
		1.0f, 0.0f, 2.0f,

		1.15f, 0.0f, -1.0f,
		1.0f, 0.0f, 2.0f,
		1.0f, -0.6f, 2.0f,
		1.15f, -0.6f, -1.0f
	};

	const GLfloat quadTexcoords[] =
	{
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,

		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,

		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
	};

	const GLfloat quadNormals[] =
	{
		-1.0f, 0.0f, 0.7f,
		-1.0f, 0.0f, 0.7f,
		-1.0f, 0.0f, 0.7f,
		-1.0f, 0.0f, 0.7f,

		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		1.0f, 0.0f, 0.7f,
		1.0f, 0.0f, 0.7f,
		1.0f, 0.0f, 0.7f,
		1.0f, 0.0f, 0.7f
	};

	//Vao_quad
	glGenVertexArrays(1, &vao_Quad);
	glBindVertexArray(vao_Quad);

	glGenBuffers(1, &vbo_QuadPosition);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_QuadPosition);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(RMB_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexArrayAttrib(vao_Quad, RMB_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_QuadTexture);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_QuadTexture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadTexcoords), quadTexcoords, GL_STATIC_DRAW);
	glVertexAttribPointer(RMB_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexArrayAttrib(vao_Quad, RMB_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_QuadNormal);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_QuadNormal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadNormals), quadNormals, GL_STATIC_DRAW);
	glVertexAttribPointer(RMB_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexArrayAttrib(vao_Quad, RMB_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

