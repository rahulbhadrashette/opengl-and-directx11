#ifndef FONTRENDERING_H
#define FONTRENDERING_H

#pragma once

#include "stdafx.h"
#include "Main.h"

int FontRenderingInitialize(HWND);
void FontRenderingDisplay(glm::mat4, glm::mat4, std::string, float, float, float, glm::vec3, float, float);
void FontRenderingUninitialize(void);
void FontRenderingUpdate(void);

#endif // !FONTRENDERING_H


