#pragma once
// For OpenAL 
#include <stdio.h>
#include <fstream>
#include <string.h>
#include "al.h"     	// For OpenAL  sudo apt-get install libopenal-dev
#include "alc.h" // For OpenAL Context
#include "LogFile.h"

using namespace std;

int OpenALInitialize(void);
void OpenALUninitialize(void);
// For OpenAL 
char* loadWAV_CPP(const char*, int&, int&, int&, int&);
int convertToInt(char* SKC_buffer, int SKC_length);
bool isBigEndian(void);

