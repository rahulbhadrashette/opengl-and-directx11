#ifndef LOGFILE_H 
#define LOGFILE_H

#pragma once

void Log_Initialize(void);
void LogFile(const char*);

#endif
