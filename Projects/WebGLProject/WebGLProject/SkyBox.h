#ifndef SKYBOX_H 
#define SKYBOX_H

#pragma once

#include "stdafx.h"
#include "Main.h"

int SkyBoxInitialize(HWND);
void SkyBoxDisplay(glm::mat4, glm::mat4);
void SkyBoxUninitialize(void);
void SkyBoxUpdate(void);

#endif
