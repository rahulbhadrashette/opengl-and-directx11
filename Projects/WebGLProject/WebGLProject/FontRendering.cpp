#pragma once
#include "FontRendering.h"

struct FONTRENDERING
{
	GLuint SKC_gVertexShaderObject;
	GLuint SKC_gFragmentShaderObject;
	GLuint SKC_gShaderProgramObject;

	GLuint SKC_gVao;					// global Vertex Array Object.
	GLuint SKC_gVbo_Position;			// global Vertex Buffer Object For Position.

	GLuint SKC_gMVPUniform;				// global Model View Projection Uniform.
	GLuint SKC_gTextColor;				// global Texture Color Uniform.
	GLuint SKC_gTextureSamplerUniform;			// global Texture Sampler Uniform.
};

struct Character {

    unsigned int SKC_TextureID;  // ID handle of the glyph texture
    glm::ivec2   SKC_Size;       // Size of glyph
    glm::ivec2   SKC_Bearing;    // Offset from baseline to left/top of glyph
    unsigned int SKC_Advance;    // Offset to advance to next glyph

};

std::map<char, Character> SKC_Characters;
struct FONTRENDERING font_t;

int FontRenderingInitialize(HWND SKC_ghwnd)
{
	// Loading Font Texture for Rendering in Shaders
	void loadFontTextureRendering(HWND);

	loadFontTextureRendering(SKC_ghwnd);

	// **** VERTEX SHADER ****
	// Create Empty Shader 
	font_t.SKC_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// Provide Source Code to Shader		// #version is driver version for 4.5 called 450, core => use core profile..., compatibility profile => old pipeline(FPP) / Hybrid pipeline (FPP and PP).
	const GLchar* SKC_vertexShaderSourceCode =
		"#version 450 core"\
		"\n "\
		"\n in vec4 SKC_vPosition;"\
		"\n uniform mat4 SKC_u_mvpmatrix;"\
		"\n out vec2 SKC_TexCoords;"\
		"\n	void main(void)"\
		"\n {"\
		"\n 	gl_Position = SKC_u_mvpmatrix * vec4(SKC_vPosition.xy, 0.0, 1.0);"\
		"\n 	SKC_TexCoords = SKC_vPosition.zw;"\
		"\n }";

	// Enter all Shader Source code with length in empty newly Created Shader...
	glShaderSource(font_t.SKC_gVertexShaderObject, 1, (const GLchar**)&SKC_vertexShaderSourceCode, NULL);
	//glShaderSource(Empty Shader Object/page, always less then 3 codes, String which has code, it is a Array in which String Length of that code);

	// Compile Shader (Driver Section)
	glCompileShader(font_t.SKC_gVertexShaderObject);

	// Error Checking Program for Compiling of shader.
	GLint SKC_iInfoLogLength = 0;
	GLint SKC_iShaderCompiledStatus = 0;
	char* SKC_szInfoLog = NULL;				// String Buffer.

	glGetShaderiv(font_t.SKC_gVertexShaderObject, GL_COMPILE_STATUS, &SKC_iShaderCompiledStatus);
	// get Shader Integer vertex from SKC_gVertexShaderObject give GL Compile Status in &SKC_iShaderCompiledStatus 
	if (SKC_iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(font_t.SKC_gVertexShaderObject, GL_INFO_LOG_LENGTH, &SKC_iInfoLogLength);
		// get Shader Integer vertex from SKC_gVertexShaderObject give GL Information Log length in &SKC_iInfoLogLength;

		if (SKC_iInfoLogLength > 0)
		{
			SKC_szInfoLog = (char*)malloc(((sizeof(char) * SKC_iInfoLogLength) + 1));
			// assign memory to buffer of string (sz) for Info Log...
			if (SKC_szInfoLog != NULL)
			{
				GLsizei SKC_written;
				// how much buffer is SKC_written Info Log that information is collected in SKC_written Variable...
				glGetShaderInfoLog(font_t.SKC_gVertexShaderObject, SKC_iInfoLogLength, &SKC_written, SKC_szInfoLog);
				// get Shader Information Log from gVertexShaderObject and integer Info Log Length, give How much bytes are written and give in string Info Log buffer. 
				LogFile("FontRendering VS :- ");
				LogFile((char*)SKC_szInfoLog);
				free(SKC_szInfoLog);
				// Free the String Buffer...
				DestroyWindow(SKC_ghwnd);
			}
		}
	}

	// **** FRAGMENT SHADER ****
	// Create Shader
	font_t.SKC_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// Provide Source Code to Shader		// #version is driver version for 4.5 called 450
	const GLchar* SKC_fragmentShaderSourceCode =
		"#version 450 core"\
		"\n "\
		"\n in vec2 SKC_TexCoords;"\
		"\n out vec4 SKC_FragColor;"\
		"\n uniform sampler2D SKC_u_Texture_Sampler;"\
		"\n uniform vec3 SKC_textColor;"\
		"\n void main(void)"\
		"\n {"\
		"\n		vec4 SKC_sampled = vec4(1.0, 1.0, 1.0, texture(SKC_u_Texture_Sampler, SKC_TexCoords).r);"\
		"\n 	SKC_FragColor = vec4(SKC_textColor, 1.0) * SKC_sampled;"\
		"\n }";

	// Enter all Shader Source code with length in empty newly Created Shader...
	glShaderSource(font_t.SKC_gFragmentShaderObject, 1, (const GLchar**)&SKC_fragmentShaderSourceCode, NULL);
	//glShaderSource(Empty Shader Object/page, always less then 3 codes, String which has code, it is a Array in which String Length of that code);

	// Compile Shader
	glCompileShader(font_t.SKC_gFragmentShaderObject);

	// Error Checking Program for Compiling of shader.
	SKC_iInfoLogLength = 0;
	SKC_iShaderCompiledStatus = 0;
	SKC_szInfoLog = NULL;				// String Buffer.

	glGetShaderiv(font_t.SKC_gFragmentShaderObject, GL_COMPILE_STATUS, &SKC_iShaderCompiledStatus);
	// Get Shader Integer Vector from SKC_gFragmentShaderObject to check the compile status, and return output in &SKC_iShaderCompiledStatus...
	if (SKC_iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(font_t.SKC_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &SKC_iInfoLogLength);
		// Get Shader integer Vector from SKC_gFragmentShaderObject using GL_INFO_LOG_LENGTH and Return Output in SKC_iInfoLogLength
		if (SKC_iInfoLogLength > 0)
		{
			SKC_szInfoLog = (char*)malloc(((sizeof(char) * SKC_iInfoLogLength) + 1));
			// Assign Memory to String Buffer to store temporary Error Output...
			if (SKC_szInfoLog != NULL)
			{
				GLsizei SKC_written;
				// How much bytes SKC_written in buffer...
				glGetShaderInfoLog(font_t.SKC_gFragmentShaderObject, SKC_iInfoLogLength, &SKC_written, SKC_szInfoLog);
				// Get Shader Information Log from SKC_gFragmentShaderObject, by using iInfoLogLength and return bytes written in SKC_szInfoLog...
				LogFile("SkyBox FS :- ");
				LogFile((char*)SKC_szInfoLog);
				free(SKC_szInfoLog);
				// Free the String Buffer...
				DestroyWindow(SKC_ghwnd);
			}
		}
	}

	// **** SHADER PROGRAM ****
	// Create a Progam which contains all Shader Programs in One Shader Program...
	font_t.SKC_gShaderProgramObject = glCreateProgram();

	// Attach Vertex Shader to Shader Program
	glAttachShader(font_t.SKC_gShaderProgramObject, font_t.SKC_gVertexShaderObject);

	// Attach Fragment Shader to Shader Program
	glAttachShader(font_t.SKC_gShaderProgramObject, font_t.SKC_gFragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(font_t.SKC_gShaderProgramObject, RMB_ATTRIBUTE_POSITION, "SKC_vPosition");		// ALERT DANGER --- BLIND OUTPUT MAY COME....

	// Link Shader
	glLinkProgram(font_t.SKC_gShaderProgramObject);

	SKC_iInfoLogLength = 0;
	SKC_szInfoLog = NULL;				// String Buffer.
	GLint SKC_iShaderProgramLinkStatus = 0;

	glGetProgramiv(font_t.SKC_gShaderProgramObject, GL_LINK_STATUS, &SKC_iShaderProgramLinkStatus);
	// Get Program Integer Vector from using SKC_gShaderProgramObject for GL LINK STATUS and return output in SKC_iShaderProgramLinkStatus

	if (SKC_iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(font_t.SKC_gShaderProgramObject, GL_INFO_LOG_LENGTH, &SKC_iInfoLogLength);
		// Get Program Integer Vector from using SKC_gShaderProgramObject for GL INFO LOG LENGTH and return Output in SKC_iInfoLogLength
		if (SKC_iInfoLogLength > 0)
		{
			SKC_szInfoLog = (char*)malloc(((sizeof(char) * SKC_iInfoLogLength) + 1));
			// Assign Memory to String Buffer...
			if (SKC_szInfoLog != NULL)
			{
				GLsizei SKC_written;
				// How Much Bytes are SKC_written variable....
				glGetProgramInfoLog(font_t.SKC_gShaderProgramObject, SKC_iInfoLogLength, &SKC_written, SKC_szInfoLog);
				// Get Program Info Log from SKC_gShaderProgramObject using SKC_iInfoLogLength length and return how many bytes are written and return output in String Buffer...
				LogFile("SkyBox LS :- ");
				LogFile((char*)SKC_szInfoLog);
				free(SKC_szInfoLog);
				// Free the String Buffer...
				DestroyWindow(SKC_ghwnd);
			}
		}
	}

	// get MVP uniform location
	font_t.SKC_gMVPUniform = glGetUniformLocation(font_t.SKC_gShaderProgramObject, "SKC_u_mvpmatrix");
	// Uniformly taking input to maintain the dynamic ness of input variable in shader program...

	// get MVP uniform location
	font_t.SKC_gTextureSamplerUniform = glGetUniformLocation(font_t.SKC_gShaderProgramObject, "SKC_u_Texture_Sampler");
	// uniform taking input to maintain the dynamic ness of input variable in shader program...

	font_t.SKC_gTextColor = glGetUniformLocation(font_t.SKC_gShaderProgramObject, "SKC_textColor");

	glGenVertexArrays(1, &font_t.SKC_gVao);
	// Generate Vertices Array, How many you want, and their address variables.
	glBindVertexArray(font_t.SKC_gVao);
	// Bind Vertex Array, for which variable.

	glGenBuffers(1, &font_t.SKC_gVbo_Position);
	glBindBuffer(GL_ARRAY_BUFFER, font_t.SKC_gVbo_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(RMB_ATTRIBUTE_POSITION, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(RMB_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	// Close Bind Vertex Array Recording by giving 0...

	return(0);
}

void FontRenderingDisplay(glm::mat4 viewMatrix, glm::mat4 SKC_gPerspectiveProjectionMatrix, std::string dStr, float posX, float posY, float posZ, glm::vec3 Color, float xScale, float yScale)
{
	// Local Function Declaration SKC
	void RenderText(std::string, float, float, float, glm::vec3);
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Start Using OpenGL Program Object 
	glUseProgram(font_t.SKC_gShaderProgramObject);

	// OpenGL Drawing 
	// Set modelview & modelviewprojection matrices to identity
	glm::mat4 SKC_modelMatrix = glm::mat4(1.0f);
	glm::mat4 SKC_modelViewProjectionMatrix = glm::mat4(1.0);


	// Transfer all Model Translation, Scaling and Rotation Matrix into ModelViewMatrix...
	SKC_modelMatrix = scale(SKC_modelMatrix, glm::vec3(xScale, yScale, 0.0f));

	// multiply the modelview and orthographic matrix to get SKC_modelViewProjectionMatrix ...
	SKC_modelViewProjectionMatrix = SKC_gPerspectiveProjectionMatrix * viewMatrix * SKC_modelMatrix;	// ORDER IS IMPORTANT

	// pass above SKC_modelViewProjectionMatrix to the vertex shader in 'SKC_u_mvpmatrix' Shader Variable...
	// Whose Position value we already calculated in initWithFrame() by using glGetUniformLocation() ...
	glUniformMatrix4fv(font_t.SKC_gMVPUniform, 1, GL_FALSE, value_ptr(SKC_modelViewProjectionMatrix));
	// 3rd parameter do you want to do transpose of matrices... (transpose => make rows to columns and columns to rows...)

	RenderText(dStr, posX, posY, posZ, Color);
	
	// Stop Using OpenGL Program Object
	glUseProgram(0);
	glDisable(GL_BLEND);
}

void FontRenderingUninitialize(void)
{
	// Destroy SKC_gVao
	if (font_t.SKC_gVao)
	{
		glDeleteVertexArrays(1, &font_t.SKC_gVao);
		font_t.SKC_gVao = 0;
	}

	// Destroy SKC_gVbo_Position
	if (font_t.SKC_gVbo_Position)
	{
		glDeleteBuffers(1, &font_t.SKC_gVbo_Position);
		font_t.SKC_gVbo_Position = 0;
	}

	// Safe Shader Clean Up in Uninitialize()
	if (font_t.SKC_gShaderProgramObject)
	{
		// To Start Again Because in Draw Function glUseProgram(0) was Closed so.
		glUseProgram(font_t.SKC_gShaderProgramObject);

		GLsizei SKC_shaderCount;

		glGetProgramiv(font_t.SKC_gShaderProgramObject, GL_ATTACHED_SHADERS, &SKC_shaderCount);

		GLuint* SKC_pShaders = NULL;

		SKC_pShaders = (GLuint*)malloc((sizeof(GLuint) * SKC_shaderCount));

		// Error Checking
		if (SKC_pShaders == NULL)
		{
			LogFile("Malloc For SKC_pShaders in uninitialize Failed ...");
		}

		glGetAttachedShaders(font_t.SKC_gShaderProgramObject, SKC_shaderCount, &SKC_shaderCount, SKC_pShaders);

		for (GLsizei SKC_i = 0; SKC_i < SKC_shaderCount; SKC_i++)
		{
			glDetachShader(font_t.SKC_gShaderProgramObject, SKC_pShaders[SKC_i]);
			glDeleteShader(SKC_pShaders[SKC_i]);
			SKC_pShaders[SKC_i] = 0;
		}

		free(SKC_pShaders);
		SKC_pShaders = NULL;

		glDeleteProgram(font_t.SKC_gShaderProgramObject);
		font_t.SKC_gShaderProgramObject = 0;

		glUseProgram(0);
	}

	// To Delete all map data structure memory...
	SKC_Characters.clear();
}

void FontRenderingUpdate(void)
{
	// code
}

void loadFontTextureRendering(HWND SKC_ghwnd)
{
	// Code SKC
	FT_Library SKC_Ft;
	if (FT_Init_FreeType(&SKC_Ft))
	{
		LogFile("ERROR::FREETYPE: Could not init FreeType Library ...");
		DestroyWindow(SKC_ghwnd);
	}

	FT_Face SKC_Face;
	if (FT_New_Face(SKC_Ft, "./resources/Font/GermaniaOne-Regular.ttf", 0, &SKC_Face))		// Woodlook, ProgressPersonal, Pinewood, DragonWings, BillSmith
	{
		LogFile("ERROR::FREETYPE: Failed to load font ...");
		DestroyWindow(SKC_ghwnd);
	}

	FT_Set_Pixel_Sizes(SKC_Face, 0, 48);

	if (FT_Load_Char(SKC_Face, 'X', FT_LOAD_RENDER))
	{
		LogFile("ERROR::FREETYTPE: Failed to load Glyph ...");
		DestroyWindow(SKC_ghwnd);
	}

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // disable byte-alignment restriction

	for (unsigned char SKC_c = 0; SKC_c < 128; SKC_c++)
	{
		// load character glyph 
		if (FT_Load_Char(SKC_Face, SKC_c, FT_LOAD_RENDER))
		{
			LogFile("ERROR::FREETYTPE: Failed to load Glyph ...");
			continue;
		}
		// generate texture
		GLuint SKC_texture;
		glGenTextures(1, &SKC_texture);
		glBindTexture(GL_TEXTURE_2D, SKC_texture);
		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			SKC_Face->glyph->bitmap.width,
			SKC_Face->glyph->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			SKC_Face->glyph->bitmap.buffer
		);

		// set texture options
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// now store character for later use
		Character SKC_character = {
			SKC_texture,
			glm::ivec2(SKC_Face->glyph->bitmap.width, SKC_Face->glyph->bitmap.rows),
			glm::ivec2(SKC_Face->glyph->bitmap_left, SKC_Face->glyph->bitmap_top),
			(GLuint)SKC_Face->glyph->advance.x
		};
		SKC_Characters.insert(std::pair<char, Character>(SKC_c, SKC_character));
	}

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	FT_Done_Face(SKC_Face);
	FT_Done_FreeType(SKC_Ft);
}

void RenderText(std::string SKC_text, float SKC_x, float SKC_y, float SKC_scale, glm::vec3 SKC_color)
{
	// activate corresponding render state	
	glUniform3f(font_t.SKC_gTextColor, SKC_color.x, SKC_color.y, SKC_color.z);
	glActiveTexture(GL_TEXTURE0);
	glUniform1i(font_t.SKC_gTextureSamplerUniform, 0);
	glBindVertexArray(font_t.SKC_gVao);

	// iterate through all characters
	std::string::const_iterator SKC_c;
	for (SKC_c = SKC_text.begin(); SKC_c != SKC_text.end(); SKC_c++)
	{
		Character SKC_ch = SKC_Characters[*SKC_c];

		float SKC_xpos = SKC_x + SKC_ch.SKC_Bearing.x * SKC_scale;
		float SKC_ypos = SKC_y - (SKC_ch.SKC_Size.y - SKC_ch.SKC_Bearing.y) * SKC_scale;

		float SKC_w = SKC_ch.SKC_Size.x * SKC_scale;
		float SKC_h = SKC_ch.SKC_Size.y * SKC_scale;

		// update VBO for each character
		float SKC_vertices[6][4] = {
			{ SKC_xpos,     	SKC_ypos + SKC_h,   0.0f, 0.0f },
			{ SKC_xpos,     	SKC_ypos,       	0.0f, 1.0f },
			{ SKC_xpos + SKC_w, SKC_ypos,       	1.0f, 1.0f },

			{ SKC_xpos,     	SKC_ypos + SKC_h,   0.0f, 0.0f },
			{ SKC_xpos + SKC_w, SKC_ypos,       	1.0f, 1.0f },
			{ SKC_xpos + SKC_w, SKC_ypos + SKC_h,   1.0f, 0.0f }
		};

		// render glyph texture over quad
		glBindTexture(GL_TEXTURE_2D, SKC_ch.SKC_TextureID);

		// update content of VBO memory
		glBindBuffer(GL_ARRAY_BUFFER, font_t.SKC_gVbo_Position);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(SKC_vertices), SKC_vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		// render quad
		glDrawArrays(GL_TRIANGLES, 0, 6);
		// now advance cursors for next glyph (note that advance is number of 1/64 pixels)
		SKC_x += (SKC_ch.SKC_Advance >> 6)* SKC_scale; // bitshift by 6 to get value in pixels (2^6 = 64)
	}
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

