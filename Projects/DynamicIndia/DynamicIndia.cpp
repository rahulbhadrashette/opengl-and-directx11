//Header Files
#include<Windows.h>
#include<stdio.h>
#include<gl/GLEW.h>
#include<gl/GL.h>
#include"vmath.h"
#include"Song.h"

//Library Files
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")
#pragma comment( lib, "Winmm.lib")

//User define Micro
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTCOORD0
};

//Global  variable Declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;
FILE* gpFile = NULL;
bool gbActiveWindow = false;
bool bIsFullScreen = false;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

//Programmable Pipeline Variable
GLuint gShaderProgramObject;
GLuint mvpUniform;
vmath::mat4 perspectiveProjectionMatrix;

GLuint vao_I;
GLuint vbo_Position_I;
GLuint vbo_Color_I;

GLuint vao_N;
GLuint vbo_Position_N;
GLuint vbo_Color_N;

GLuint vao_D;
GLuint vbo_Position_D;
GLuint vbo_Color_D;

GLuint vao_A_TriBand;
GLuint vbo_Position_A_TriBand;
GLuint vbo_Color_A_TriBand;

GLuint vao_A;
GLuint vbo_Position_A;
GLuint vbo_Color_A;

GLuint vao_Plane;
GLuint vbo_Position_Plane;
GLuint vbo_Color_Plane;

GLuint vao_Smoke_Keshari;
GLuint vbo_Position_Smoke_Keshari;
GLuint vbo_Color_Smoke_Keshari;

GLuint vao_Smoke_White;
GLuint vbo_Position_Smoke_White;
GLuint vbo_Color_Smoke_White;

GLuint vao_Smoke_Green;
GLuint vbo_Position_Smoke_Green;
GLuint vbo_Color_Smoke_Green;

GLuint vao_Smoke;
GLuint vbo_Position_Smoke;
GLuint vbo_Color_Smoke;

bool IFlag1 = true;
bool NFlag = true;
bool DFlag = true;
bool IFlag2 = true;
bool AFlag = true;
bool PlaneFlag = true;
bool DrawTriBand = true;
bool repaintINDIA = false;
bool leftPlanesAndSmokes = true;
bool rightPlanesAndSmokes = false;
bool hidePlane = false;

GLfloat Translate_Angle_1 = 180.8f;
GLfloat Rotate_Angles_1 = 0.0f;
GLfloat k1, l1;

GLfloat Translate_Angle2 = 179.53f;
GLfloat Rotate_Angles2 = 270.0f;
GLfloat k2, l2;

GLfloat Translate_Angle_3 = 178.61f;
GLfloat Rotate_Angles_3 = 90.0f;
GLfloat k3, l3;

GLfloat Translate_Angle_4 = 89.38f;
GLfloat Rotate_Angles_4 = 360.0f;
GLfloat k4, l4;

GLfloat Keshari1 = 1.0f, Keshari2 = 0.5f;
GLfloat PrintEnd;
GLfloat nig_X = -3.0f;
GLfloat pos_X = 4.6f;
GLfloat pos_Y = 3.5f;
GLfloat neg_Y = -3.5f;
GLfloat one = 0.0f, pointFour = 0.0f;
GLfloat x2 = -2.5f;
GLfloat PosX3 = -2.8f;

GLfloat Smoke1 = 0.0f;
GLfloat Smoke2 = 0.0f;

const int points = 500;

//Function Declaration(Prototype/ Signature)
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void Uninitialize_RMB(void);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	int Initialize_RMB(void);
	void Display_RMB(void);
	void Update_RMB(void);

	//Local Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR str[] = TEXT("BlueScreen");
	bool bDone = false;
	int iRet = 0;
	int X, Y;

	//File io
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created"), TEXT("Error"), MB_OK);
	}
	else
	{
		fprintf_s(gpFile, "Log file is successfully complited...!!!\n");
	}

	//Initialize WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = str;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register WinclassEx
	RegisterClassEx(&wndclass);

	//CenterOfTheWindowCalculation
	X = (GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2);
	Y = (GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2);

	//CreateWindow
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, str, TEXT("RahulUMB |...18-DynamicIndia)...!!!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X,
		Y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = Initialize_RMB();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "ChoosePixelFormat Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "SetPixelFormat is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, " wglCreateContext is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "wglMakeCurrent Failed.\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "Initializetion Successfully Complited.\n");
	}

	//Here Actual ShowWindow
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//MessageLoop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				Update_RMB();
			}
			Display_RMB();
		}
	}

	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void Resize_RMB(int, int);

	switch (iMsg)
	{
		//code
		case WM_CREATE:
			PlaySound(MAKEINTRESOURCE(MY_SONG), NULL, SND_ASYNC | SND_RESOURCE | SND_NODEFAULT);
			break;

		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;

		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;

		case WM_SIZE:
			Resize_RMB(LOWORD(lParam), HIWORD(lParam));
			break;

		case WM_ERASEBKGND:
			return(0);

		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;

		case WM_KEYDOWN:
			switch (wParam)  //in Past switch(LOWORD(wParam))
			{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			}
			break;

		case WM_DESTROY:
			Uninitialize_RMB();
			PostQuitMessage(0);
			break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize_RMB(void)
{
	//Function Declaration
	void Resize_RMB(int, int);
	void ToggleFullScreen(void);
	void initializeIndia(void);
	void initializeCentralPlaneCode(void);
	void initializeCurvePlain(void);

	//Local Variable Declaration
	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLenum result;
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	//Initialize pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));  //or   //For only Windows
	//memset((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));  //For All OS
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	//the return index is always 1 based to 38 if Zero(0) return failure 
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf_s(gpFile, "glewInit(); is Failed.\n");
		Uninitialize_RMB();
		DestroyWindow(ghwnd);
	}

	//Define Vertex Shader Object
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_Color;" \
		"void main(void)" \
		"{" \
			"gl_Position = u_mvp_matrix * vPosition;" \
			"out_Color = vColor;" \
		"}";

	//Specifing above source to the vertex shader object
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//Compile the Vertex Shader
	glCompileShader(vertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "vertexShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				Uninitialize_RMB();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 out_Color;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
			"fragColor = out_Color;" \
		"}";


	// Specifing above Source to the fragment Shader Object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//Compile the fragment Shader Object
	glCompileShader(fragmentShaderObject);

	GLint iFShaderCompileStatus = 0;
	GLint iFInfoLogLength = 0;
	GLint iInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iFShaderCompileStatus);
	if (iFShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iFInfoLogLength);
		if (iFInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iFInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iFInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "FragmentShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);

				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Create Shader Object
	gShaderProgramObject = glCreateProgram();

	//Attach VertexShader to the Shader Program
	glAttachShader(gShaderProgramObject, vertexShaderObject);

	//Attach fragmentShader to the Shader Program
	glAttachShader(gShaderProgramObject, fragmentShaderObject);

	// Pre Linking Binding to Vertex Attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject);

	GLint  iProgramLinkStatus = 0;
	GLint iLInfoLogLength = 0;
	GLint *szLogLength = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iLInfoLogLength);
		if (iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(iLInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iLInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "LinkShaderProgramObject:- %s", szInfoLog);
				free(szInfoLog);
				Uninitialize_RMB();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Post Linking Retriving Uniform Location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	
	initializeIndia();
	initializeCentralPlaneCode();
	initializeCurvePlain();
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix = vmath::mat4::identity();
	//Warmup Call To Resize
	Resize_RMB(WIN_WIDTH, WIN_HEIGHT);
	ToggleFullScreen();
	return(0);
}

void Resize_RMB(int Width, int Height)
{
	//code
	if (Height == 0)
	{
		Height = 1;
	}
	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);
}

void Display_RMB(void)
{
	//function declaration
	void drawIndia(void);
	void drawRepaintIndia(void);
	void drawCentralPlane(void);
	void drawleftTopPlain(void);
	void drawleftBottomPlain(void);
	void drawRightBottomPlain(void);
	void drawRightTopPlain(void);

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//Use Program
	glUseProgram(gShaderProgramObject);

	//function Call
	if(repaintINDIA == true)
	{
		drawRepaintIndia();
	}
	else
	{
		drawIndia();
		if (leftPlanesAndSmokes == false)
		{
			drawCentralPlane();
			drawleftTopPlain();
			drawleftBottomPlain();
		}
		if (rightPlanesAndSmokes == true)
		{
			drawRightBottomPlain();
			drawRightTopPlain();
		}
	}
	
	//UnUse Program
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void Uninitialize_RMB(void)
{
	//code
	if (vbo_Color_A_TriBand)
	{
		glDeleteBuffers(1, &vbo_Color_A_TriBand);
		vbo_Color_A_TriBand = 0;
	}
	if (vbo_Position_A_TriBand)
	{
		glDeleteBuffers(1, &vbo_Position_A_TriBand);
		vbo_Position_A_TriBand = 0;
	}
	if (vao_A_TriBand)
	{
		glDeleteVertexArrays(1, &vao_A_TriBand);
		vao_A_TriBand = 0;
	}

	if (vbo_Color_A)
	{
		glDeleteBuffers(1, &vbo_Color_A);
		vbo_Color_A = 0;
	}
	if (vbo_Position_A)
	{
		glDeleteBuffers(1, &vbo_Position_A);
		vbo_Position_A = 0;
	}
	if (vao_A)
	{
		glDeleteVertexArrays(1, &vao_A);
		vao_A = 0;
	}

	if (vbo_Color_D)
	{
		glDeleteBuffers(1, &vbo_Color_D);
		vbo_Color_D = 0;
	}
	if (vbo_Position_D)
	{
		glDeleteBuffers(1, &vbo_Position_D);
		vbo_Position_D = 0;
	}
	if (vao_D)
	{
		glDeleteVertexArrays(1, &vao_D);
		vao_D = 0;
	}

	if (vbo_Color_N)
	{
		glDeleteBuffers(1, &vbo_Color_N);
		vbo_Color_N = 0;
	}
	if (vbo_Position_N)
	{
		glDeleteBuffers(1, &vbo_Position_N);
		vbo_Position_N = 0;
	}
	if (vao_N)
	{
		glDeleteVertexArrays(1, &vao_N);
		vao_N = 0;
	}

	if (vbo_Color_I)
	{
		glDeleteBuffers(1, &vbo_Color_I);
		vbo_Color_I = 0;
	}
	if (vbo_Position_I)
	{
		glDeleteBuffers(1, &vbo_Position_I);
		vbo_Position_I = 0;
	}
	if (vao_I)
	{
		glDeleteVertexArrays(1, &vao_I);
		vao_I = 0;
	}

	if (vbo_Color_Plane)
	{
		glDeleteBuffers(1, &vbo_Color_Plane);
		vbo_Color_Plane = 0;
	}
	if (vbo_Position_Plane)
	{
		glDeleteBuffers(1, &vbo_Position_Plane);
		vbo_Position_Plane = 0;
	}
	if (vao_Plane)
	{
		glDeleteVertexArrays(1, &vao_Plane);
		vao_Plane = 0;
	}

	if (vbo_Color_Smoke_Keshari)
	{
		glDeleteBuffers(1, &vbo_Color_Smoke_Keshari);
		vbo_Color_Smoke_Keshari = 0;
	}
	if (vbo_Position_Smoke_Keshari)
	{
		glDeleteBuffers(1, &vbo_Position_Smoke_Keshari);
		vbo_Position_Smoke_Keshari = 0;
	}
	if (vao_Smoke_Keshari)
	{
		glDeleteVertexArrays(1, &vao_Smoke_Keshari);
		vao_Smoke_Keshari = 0;
	}

	if (vbo_Color_Smoke_White)
	{
		glDeleteBuffers(1, &vbo_Color_Smoke_White);
		vbo_Color_Smoke_White = 0;
	}
	if (vbo_Position_Smoke_White)
	{
		glDeleteBuffers(1, &vbo_Position_Smoke_White);
		vbo_Position_Smoke_White = 0;
	}
	if (vao_Smoke_White)
	{
		glDeleteVertexArrays(1, &vao_Smoke_White);
		vao_Smoke_White = 0;
	}

	if (vbo_Color_Smoke_Green)
	{
		glDeleteBuffers(1, &vbo_Color_Smoke_Green);
		vbo_Color_Smoke_Green = 0;
	}
	if (vbo_Position_Smoke_Green)
	{
		glDeleteBuffers(1, &vbo_Position_Smoke_Green);
		vbo_Position_Smoke_Green = 0;
	}
	if (vao_Smoke_Green)
	{
		glDeleteVertexArrays(1, &vao_Smoke_Green);
		vao_Smoke_Green = 0;
	}
	if (vbo_Color_Smoke)
	{
		glDeleteBuffers(1, &vbo_Color_Smoke);
		vbo_Color_Smoke = 0;
	}
	if (vbo_Position_Smoke)
	{
		glDeleteBuffers(1, &vbo_Position_Smoke);
		vbo_Position_Smoke = 0;
	}
	if (vao_Smoke)
	{
		glDeleteVertexArrays(1, &vao_Smoke);
		vao_Smoke = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);
		//Ask Program How Many Shaders are Attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	//Break the Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf_s(gpFile, "Log file is closed Successfully. \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void Update_RMB(void)
{
	//************* India *************************
	//Variables
	//For First I
	nig_X = nig_X + 0.0024f;
	if (nig_X >= -0.9f)
	{
		nig_X = -0.9f;
		AFlag = false;
	}
	//For A
	if (AFlag == false)
	{
		pos_X = pos_X - 0.0024f;
		if (pos_X <= 1.85f)
		{
			pos_X = 1.85f;
			NFlag = false;
		}
	}
	//For N
	if (NFlag == false)
	{
		pos_Y = pos_Y - 0.0024f;
		if (pos_Y <= 0.0f)
		{
			pos_Y = 0.0f;
			IFlag2 = false;
		}
	}
	//For Second I
	if (IFlag2 == false)
	{
		neg_Y = neg_Y + 0.0024f;
		if (neg_Y >= 0.0f)
		{
			neg_Y = 0.0f;
			DFlag = false;
		}
	}
	//For D
	if (DFlag == false)
	{
		one = one + 0.00036f;
		pointFour = pointFour + 0.00036f;
		if (one >= 1.0f)
		{
			one = 1.0f;
			pointFour = 0.5f;
			PlaneFlag = false;
			leftPlanesAndSmokes = false;
		}
	}

	//*********** Center Plane ***************************
	if (PlaneFlag == false)
	{
		x2 = x2 + 0.0002565f;
		if (x2 >= 2.6f)
		{
			x2 = 2.6f;
			DrawTriBand = false;
		}

		PosX3 = PosX3 + 0.0002565f;
		if (PosX3 >= 2.6f)
		{
			PosX3 = 2.6f;
			repaintINDIA = true;
		}

		if (x2 >= 1.2f)
		{
			rightPlanesAndSmokes = true;
		}
	}

	//********** Curve Plane Code Start ********************
	if (leftPlanesAndSmokes == false)
	{
		//************Left Top Curve PLANE *********************
		k2 = 1.85f * (GLfloat)cos(Translate_Angle2) - 0.67f;
		l2 = 1.77f * (GLfloat)sin(Translate_Angle2) + 1.70f;
		if (Translate_Angle2 >= 180.51f)
		{
			Translate_Angle2 = 180.51f;
		}
		Translate_Angle2 = Translate_Angle2 + 0.0001449585f;

		if (Rotate_Angles2 >= 360.0f)
		{
			Rotate_Angles2 = 360.0f;
		}
		Rotate_Angles2 = Rotate_Angles2 + 0.0185f;

		//************ Left Bottom Curve PLANE *********************
		//Left Bottom Curve PLANE 3
		k3 = 1.85f * (GLfloat)cos(Translate_Angle_3) - 0.67f;
		l3 = 1.85f * (GLfloat)sin(Translate_Angle_3) - 1.85f;

		if (Translate_Angle_3 < 177.7f)
		{
			Translate_Angle_3 = 177.7f;
			hidePlane = true;
		}
		Translate_Angle_3 = Translate_Angle_3 - 0.000144959f;

		if (Rotate_Angles_3 < 0)
		{
			Rotate_Angles_3 = 0;
		}
		Rotate_Angles_3 = Rotate_Angles_3 - 0.015f;

		//*********** Smoke Curve Code*************
		Smoke1 = Smoke1 + 0.00026f;
		if (Smoke1 >= 1.76f)
		{
			Smoke1 = 1.76f;
		}
	}

	////*************** Right Top Curve PLANE ************************
	if (rightPlanesAndSmokes == true)
	{
		k1 = 1.55f * (GLfloat)cos(Translate_Angle_1) + 1.0f;
		l1 = 1.66f * (GLfloat)sin(Translate_Angle_1) + 1.585f;
		if (Translate_Angle_1 >= 182.18f)
		{
			Translate_Angle_1 = 182.18f;
		}
		Translate_Angle_1 = Translate_Angle_1 + 0.0002f;

		if (Rotate_Angles_1 >= 75.0f)
		{
			Rotate_Angles_1 = 75.0f;
		}
		Rotate_Angles_1 = Rotate_Angles_1 + 0.018f;

		//*************** Right Bottom Curve PLANE ************************
		k4 = 1.55f * (GLfloat)cos(Translate_Angle_4) + 1.0f;
		l4 = 1.66f * (GLfloat)sin(Translate_Angle_4) - 1.69f;
		if (Translate_Angle_4 <= 88.00f)
		{
			Translate_Angle_4 = 88.00f;
		}
		Translate_Angle_4 = Translate_Angle_4 - 0.0002f;

		if (Rotate_Angles_4 <= 270)
		{
			Rotate_Angles_4 = 270;
		}
		Rotate_Angles_4 = Rotate_Angles_4 - 0.015f;

		//*********** Smoke Curve Code*************
		Smoke2 = Smoke2 + 0.00026f;
		if (Smoke2 >= 1.76f)
		{
			Smoke2 = 1.76f;
		}
	}
}
void initializeIndia(void)
{
	//I Vertices Array
	const GLfloat I_Verties[] =
									{
										-1.0f, 1.0f, 0.0f,
										-1.0f, -1.0f, 0.0f
									};
	//I Color Array
	const GLfloat I_Color[] =
									{
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f
									};

	//Create vao
	glGenVertexArrays(1, &vao_I);
	glBindVertexArray(vao_I);
	glGenBuffers(1, &vbo_Position_I);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_I);
	glBufferData(GL_ARRAY_BUFFER, sizeof(I_Verties), I_Verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_I);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_I);
	glBufferData(GL_ARRAY_BUFFER, sizeof(I_Color), I_Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	//N Vertices Array
	const GLfloat N_Verties[] =
									{
										-1.0f, 1.0f, 0.0f,
										-1.0f, -1.0f, 0.0f,
										-1.0f, 1.0f, 0.0f,
										-0.2f, -1.0f, 0.0f,
										-0.2f, 1.0f, 0.0f,
										-0.2f, -1.0f, 0.0f
									};
	// N Color Array
	const GLfloat N_Color[] =
									{
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f,
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f,
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f
									};

	//Create vao
	glGenVertexArrays(1, &vao_N);
	glBindVertexArray(vao_N);
	glGenBuffers(1, &vbo_Position_N);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_N);
	glBufferData(GL_ARRAY_BUFFER, sizeof(N_Verties), N_Verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_N);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_N);
	glBufferData(GL_ARRAY_BUFFER, sizeof(N_Color), N_Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	//D Vertices Array
	const GLfloat D_Verties[] =
									{
										-1.0f, 0.9f, 0.0f,
										-1.0f, -0.9f, 0.0f,
										-1.2f, 0.93f, 0.0f,
										-0.2f, 0.93f, 0.0f,
										-1.2f, -0.93f, 0.0f,
										-0.2f, -0.93f, 0.0f,
										-0.2f, 1.0f, 0.0f,
										-0.2f, -1.0f, 0.0f
									};

	//Create vao
	glGenVertexArrays(1, &vao_D);
	glBindVertexArray(vao_D);
	glGenBuffers(1, &vbo_Position_D);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_D);
	glBufferData(GL_ARRAY_BUFFER, sizeof(D_Verties), D_Verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_D);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_D);
	glBufferData(GL_ARRAY_BUFFER, 8 * 3 * sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	//A Vertices Array
	const GLfloat A_TriBand_Verties[] =
									{
										0.0f, -0.045f, 0.0f,
										-0.51f, -0.045f, 0.0f,
										0.0f, -0.08f, 0.0f,
										-0.51f, -0.08f, 0.0f,
										0.0f, -0.12f, 0.0f,
										-0.51f, -0.12f, 0.0f
									};
	//A Color Array
	const GLfloat A_TriBand_Color[] =
									{
										1.0f,0.5f,0.0f,
										1.0f,0.5f,0.0f,
										1.0f, 1.0f, 1.0f,
										1.0f, 1.0f, 1.0f,
										0.0f, 1.0f, 0.0f,
										0.0f, 1.0f, 0.0f,
									};

	//Create vao
	glGenVertexArrays(1, &vao_A_TriBand);
	glBindVertexArray(vao_A_TriBand);
	glGenBuffers(1, &vbo_Position_A_TriBand);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_A_TriBand);
	glBufferData(GL_ARRAY_BUFFER, sizeof(A_TriBand_Verties), A_TriBand_Verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_A_TriBand);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_A_TriBand);
	glBufferData(GL_ARRAY_BUFFER, sizeof(A_TriBand_Color), A_TriBand_Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	//A Vertices Array
	const GLfloat A_Verties[] =
									{
										-0.4f, 1.0f, 0.0f,
										0.20f, -1.0f, 0.0f,
										-0.4f, 1.0f, 0.0f,
										-0.80f, -1.0f, 0.0f
									};
	//A Color Array
	const GLfloat A_Color[] =
									{
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f,
										1.0f,0.4f,0.0f,
										0.0f, 1.0f, 0.0f,
									};

	//Create vao
	glGenVertexArrays(1, &vao_A);
	glBindVertexArray(vao_A);
	glGenBuffers(1, &vbo_Position_A);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_A);
	glBufferData(GL_ARRAY_BUFFER, sizeof(A_Verties), A_Verties, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_A);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_A);
	glBufferData(GL_ARRAY_BUFFER, sizeof(A_Color), A_Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0); 

}

void drawIndia(void)
{
	//variable declaration
	GLfloat D_Color[24];

	//Declatation of Matricex
	vmath::mat4 translationMatrix;
	vmath::mat4 modelViewMatrix;
	vmath::mat4 modelViewProjectionMatrix;

	//For First I_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(nig_X, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glLineWidth(30.0f);
	//BindWith vao
	glBindVertexArray(vao_I);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 2);
	//UnBind vao
	glBindVertexArray(0);

	if (NFlag == false)
	{
		//For N_Draw
		//Initialize of Matrix in identity
		translationMatrix = vmath::mat4::identity();
		modelViewMatrix = vmath::mat4::identity();
		modelViewProjectionMatrix = vmath::mat4::identity();

		//Do Necessary Transformation Code Here
		translationMatrix = vmath::translate(-0.6f, pos_Y, -5.0f);

		//Do Necessary Matrix Multiplication
		modelViewMatrix = translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//Send Necessary Matrix to Shader in Respective Uniform
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		glLineWidth(30.0f);
		//BindWith vao
		glBindVertexArray(vao_N);
		//Draw the Necessary Scnes
		glDrawArrays(GL_LINES, 0, 6);
		//UnBind vao
		glBindVertexArray(0);
	}

	if (DFlag == false)
	{
		//For D_Draw
		//Initialize of Matrix in identity
		translationMatrix = vmath::mat4::identity();
		modelViewMatrix = vmath::mat4::identity();
		modelViewProjectionMatrix = vmath::mat4::identity();

		//Do Necessary Transformation Code Here
		translationMatrix = vmath::translate(0.64f, 0.0f, -5.0f);

		//Do Necessary Matrix Multiplication
		modelViewMatrix = translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//Send Necessary Matrix to Shader in Respective Uniform
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		glLineWidth(30.0f);
		//BindWith vao
		glBindVertexArray(vao_D);

		D_Color[0] = one;
		D_Color[1] = pointFour;
		D_Color[2] = 0.0f;
		D_Color[3] = 0.0f;
		D_Color[4] = one;
		D_Color[5] = 0.0f;
		D_Color[6] = one;
		D_Color[7] = pointFour;
		D_Color[8] = 0.0f;
		D_Color[9] = one;
		D_Color[10] = pointFour;
		D_Color[11] = 0.0f;
		D_Color[12] = 0.0f;
		D_Color[13] = one;
		D_Color[14] = 0.0f;
		D_Color[15] = 0.0f;
		D_Color[16] = one;
		D_Color[17] = 0.0f;
		D_Color[18] = one;
		D_Color[19] = pointFour;
		D_Color[20] = 0.0f;
		D_Color[21] = 0.0f;
		D_Color[22] = one;
		D_Color[23] = 0.0f;

		glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_D);
		glBufferData(GL_ARRAY_BUFFER, sizeof(D_Color), D_Color, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

		//Draw the Necessary Scnes
		glDrawArrays(GL_LINES, 0, 8);
		//UnBind vao
		glBindVertexArray(0);
	}

	if (IFlag2 == false)
	{
		//For Second I_Draw
		//Initialize of Matrix in identity
		translationMatrix = vmath::mat4::identity();
		modelViewMatrix = vmath::mat4::identity();
		modelViewProjectionMatrix = vmath::mat4::identity();

		//Do Necessary Transformation Code Here
		translationMatrix = vmath::translate(1.75f, neg_Y, -5.0f);

		//Do Necessary Matrix Multiplication
		modelViewMatrix = translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//Send Necessary Matrix to Shader in Respective Uniform
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		glLineWidth(30.0f);
		//BindWith vao
		glBindVertexArray(vao_I);
		//Draw the Necessary Scnes
		glDrawArrays(GL_LINES, 0, 2);
		//UnBind vao
		glBindVertexArray(0);
	}

	if (DrawTriBand == false)
	{
		//For A_TriBand_Draw
		//Initialize of Matrix in identity
		translationMatrix = vmath::mat4::identity();
		modelViewMatrix = vmath::mat4::identity();
		modelViewProjectionMatrix = vmath::mat4::identity();

		//Do Necessary Transformation Code Here
		translationMatrix = vmath::translate(1.8f, 0.0f, -5.0f);

		//Do Necessary Matrix Multiplication
		modelViewMatrix = translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//Send Necessary Matrix to Shader in Respective Uniform
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		glLineWidth(10.0f);
		//BindWith vao
		glBindVertexArray(vao_A_TriBand);
		//Draw the Necessary Scnes
		glDrawArrays(GL_LINES, 0, 6);
		//UnBind vao
		glBindVertexArray(0);

	}

	if (AFlag == false)
	{
		//For A_Draw
		//Initialize of Matrix in identity
		translationMatrix = vmath::mat4::identity();
		modelViewMatrix = vmath::mat4::identity();
		modelViewProjectionMatrix = vmath::mat4::identity();

		//Do Necessary Transformation Code Here
		translationMatrix = vmath::translate(pos_X, 0.0f, -5.0f);

		//Do Necessary Matrix Multiplication
		modelViewMatrix = translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//Send Necessary Matrix to Shader in Respective Uniform
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		glLineWidth(30.0f);
		//BindWith vao
		glBindVertexArray(vao_A);
		//Draw the Necessary Scnes
		glDrawArrays(GL_LINES, 0, 6);
		//UnBind vao
		glBindVertexArray(0);
	}
}

void drawRepaintIndia(void)
{
	//variable declaration
	GLfloat D_Color[24];

	//Declatation of Matricex
	vmath::mat4 translationMatrix;
	vmath::mat4 modelViewMatrix;
	vmath::mat4 modelViewProjectionMatrix;

	//For First I_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(-0.9f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glLineWidth(30.0f);
	//BindWith vao
	glBindVertexArray(vao_I);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 2);
	//UnBind vao
	glBindVertexArray(0);

	//For N_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(-0.6f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glLineWidth(30.0f);
	//BindWith vao
	glBindVertexArray(vao_N);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 6);
	//UnBind vao
	glBindVertexArray(0);

	//For D_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.64f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glLineWidth(30.0f);
	//BindWith vao
	glBindVertexArray(vao_D);

	D_Color[0] = 1.0f;
	D_Color[1] = 0.5f;
	D_Color[2] = 0.0f;
	D_Color[3] = 0.0f;
	D_Color[4] = 1.0f;
	D_Color[5] = 0.0f;
	D_Color[6] = 1.0f;
	D_Color[7] = 0.5f;
	D_Color[8] = 0.0f;
	D_Color[9] = 1.0f;
	D_Color[10] = 0.5f;
	D_Color[11] = 0.0f;
	D_Color[12] = 0.0f;
	D_Color[13] = 1.0f;
	D_Color[14] = 0.0f;
	D_Color[15] = 0.0f;
	D_Color[16] = 1.0f;
	D_Color[17] = 0.0f;
	D_Color[18] = 1.0f;
	D_Color[19] = 0.5f;
	D_Color[20] = 0.0f;
	D_Color[21] = 0.0f;
	D_Color[22] = 1.0f;
	D_Color[23] = 0.0f;

	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_D);
	glBufferData(GL_ARRAY_BUFFER, sizeof(D_Color), D_Color, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 8);
	//UnBind vao
	glBindVertexArray(0);

	//For Second I_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(1.75f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glLineWidth(30.0f);
	//BindWith vao
	glBindVertexArray(vao_I);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 2);
	//UnBind vao
	glBindVertexArray(0);


	//For A_TriBand_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(1.8f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glLineWidth(10.0f);
	//BindWith vao
	glBindVertexArray(vao_A_TriBand);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 6);
	//UnBind vao
	glBindVertexArray(0);

	//For A_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(1.85f, 0.0f, -5.0f);

	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glLineWidth(30.0f);
	//BindWith vao
	glBindVertexArray(vao_A);
	//Draw the Necessary Scnes
	glDrawArrays(GL_LINES, 0, 6);
	//UnBind vao
	glBindVertexArray(0);
}

void initializeCentralPlaneCode(void)
{
	const GLfloat plane_vertices[] =
	{

		//Lines vertices --->> IAF
		-0.2f, 0.05f, 0.0f,
		-0.2f, -0.05f, 0.0f,

		-0.14f, 0.05f, 0.0f,
		-0.18f, -0.05f, 0.0f,
		-0.14f, 0.05f, 0.0f,
		-0.1f, -0.05f, 0.0f,
		-0.12f, 0.0f, 0.0f,
		-0.16f, 0.0f, 0.0f,

		-0.08f, 0.05f, 0.0f,
		-0.08f, -0.05f, 0.0f,
		-0.08f, 0.042f, 0.0f,
		-0.02f, 0.042f, 0.0f,
		-0.08f, 0.0f, 0.0f,
		-0.04f, 0.0f, 0.0f,

		//Triangles vertices
		0.0f, 0.08f, 0.0f,
		0.0f, -0.08f, 0.0f,
		0.08f, 0.0f, 0.0f,

		-0.24f, 0.0f, 0.0f,
		-0.28f, 0.17f, 0.0f,
		-0.28f, -0.17f, 0.0f,

		0.1f, 0.0f, 0.0f,
		-0.2f, 0.18f, 0.0f,
		-0.2f, -0.18f, 0.0f,

		//Quad vertices
		0.0f, 0.08f, 0.0f,
		-0.28f, 0.08f, 0.0f,
		-0.28f, -0.08f, 0.0f,
		0.0f, -0.08f, 0.0f


	};
	const GLfloat plane_Color[] =
	{
		0.7265625f, 0.8828125f, 0.9296875f,
		0.7265625f, 0.8828125f, 0.9296875f,
		0.7265625f, 0.8828125f, 0.9296875f,
		0.7265625f, 0.8828125f, 0.9296875f,
		0.7265625f, 0.8828125f, 0.9296875f,
		0.7265625f, 0.8828125f, 0.9296875f,
		0.7265625f, 0.8828125f, 0.9296875f,
		0.7265625f, 0.8828125f, 0.9296875f,
		0.7265625f, 0.8828125f, 0.9296875f,
		0.7265625f, 0.8828125f, 0.9296875f,
		0.7265625f, 0.8828125f, 0.9296875f,
		0.7265625f, 0.8828125f, 0.9296875f,
		0.7265625f, 0.8828125f, 0.9296875f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
	};

	//Create vao-- For Plane 
	glGenVertexArrays(1, &vao_Plane);
	glBindVertexArray(vao_Plane);
	glGenBuffers(1, &vbo_Position_Plane);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Plane);
	glBufferData(GL_ARRAY_BUFFER, sizeof(plane_vertices), plane_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_Plane);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Plane);
	glBufferData(GL_ARRAY_BUFFER, sizeof(plane_Color), plane_Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);


	const GLfloat smoke_vertices[] =
	{
		0.0f, -0.045f, 0.0f,
		-7.3f, -0.045f, 0.0f,
		0.0f, -0.073f, 0.0f,
		-7.3f, -0.073f, 0.0f,
		0.0f, -0.10f, 0.0f,
		-7.3f, -0.10f, 0.0f
	};

	const GLfloat smoke_Color[] =
	{
		Keshari1,Keshari2,0.0f,
		Keshari1,Keshari2,0.0f,

		Keshari1, Keshari1, Keshari1,
		Keshari1, Keshari1, Keshari1,

		0.0f, Keshari1, 0.0f,
		0.0f, Keshari1, 0.0f
	};

	//Create vao-- For Smoke
	glGenVertexArrays(1, &vao_Smoke);
	glBindVertexArray(vao_Smoke);
	glGenBuffers(1, &vbo_Position_Smoke);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Smoke);
	glBufferData(GL_ARRAY_BUFFER, sizeof(smoke_vertices), smoke_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_Smoke);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Smoke);
	glBufferData(GL_ARRAY_BUFFER, sizeof(smoke_Color), smoke_Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);
}

void drawCentralPlane(void)
{
	//if (PlaneFlag == false)
	//{
		//Declatation of Matricex
		vmath::mat4 translationMatrix;
		vmath::mat4 modelViewMatrix;
		vmath::mat4 modelViewProjectionMatrix;

		//For Smoke_Draw
		//Initialize of Matrix in identity
		translationMatrix = vmath::mat4::identity();
		modelViewMatrix = vmath::mat4::identity();
		modelViewProjectionMatrix = vmath::mat4::identity();

		//Do Necessary Transformation Code Here
		translationMatrix = vmath::translate(PosX3, 0.023f, -3.0f);

		//Do Necessary Matrix Multiplication
		modelViewMatrix = translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//Send Necessary Matrix to Shader in Respective Uniform
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		//BindWith vao
		glBindVertexArray(vao_Smoke);
		//Draw the Necessary Scnes
		glLineWidth(10.0f);
		glDrawArrays(GL_LINES, 0, 6);

		//UnBind vao
		glBindVertexArray(0);

		//For Plane_Draw
		//Initialize of Matrix in identity
		translationMatrix = vmath::mat4::identity();
		modelViewMatrix = vmath::mat4::identity();
		modelViewProjectionMatrix = vmath::mat4::identity();

		//Do Necessary Transformation Code Here
		translationMatrix = vmath::translate(x2, -0.05f, -3.0f);

		//Do Necessary Matrix Multiplication
		modelViewMatrix = translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//Send Necessary Matrix to Shader in Respective Uniform
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		//BindWith vao
		glBindVertexArray(vao_Plane);
		//Draw the Necessary Scnes
		glLineWidth(10.0f);
		glDrawArrays(GL_TRIANGLES, 14, 9);
		glDrawArrays(GL_QUADS, 23, 4);
		glLineWidth(5.0f);
		glDrawArrays(GL_LINES, 0, 14);

		//UnBind vao
		glBindVertexArray(0);
	//}
}

void initializeCurvePlain(void)
{
	const int poi = 500;
	GLfloat Keshari[3 * poi];
	for (int i = 0; i < poi; i++)
	{
		Keshari[3 * i + 0] = Keshari1;
		Keshari[3 * i + 1] = Keshari2;
		Keshari[3 * i + 2] = 0.0f;
	}
	
	//Create vao-- For Curve Smoke 
	glGenVertexArrays(1, &vao_Smoke_Keshari);
	glBindVertexArray(vao_Smoke_Keshari);
	//position
	glGenBuffers(1, &vbo_Position_Smoke_Keshari);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Smoke_Keshari);
	glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(GL_FLOAT), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Color
	glGenBuffers(1, &vbo_Color_Smoke_Keshari);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Smoke_Keshari);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Keshari), Keshari, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);

	GLfloat white[3 * poi];
	for (int i = 0; i < poi; i++)
	{
		white[3 * i + 0] = Keshari1;
		white[3 * i + 1] = Keshari1;
		white[3 * i + 2] = Keshari1;
	}
	//Create vao-- For Curve Smoke 
	glGenVertexArrays(1, &vao_Smoke_White);
	glBindVertexArray(vao_Smoke_White);
	//position
	glGenBuffers(1, &vbo_Position_Smoke_White);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Smoke_White);
	glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(GL_FLOAT), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Color
	glGenBuffers(1, &vbo_Color_Smoke_White);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Smoke_White);
	glBufferData(GL_ARRAY_BUFFER, sizeof(white), white, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);


	GLfloat green[3 * poi];
	for (int i = 0; i < poi; i++)
	{
		green[3 * i + 0] = 0.0f;
		green[3 * i + 1] = Keshari1;
		green[3 * i + 2] = 0.0f;
	}

	//Create vao-- For Curve Smoke 
	glGenVertexArrays(1, &vao_Smoke_Green);
	glBindVertexArray(vao_Smoke_Green);
	//vbo position
	glGenBuffers(1, &vbo_Position_Smoke_Green);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Smoke_Green);
	glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(GL_FLOAT), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Color
	glGenBuffers(1, &vbo_Color_Smoke_Green);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Smoke_Green);
	glBufferData(GL_ARRAY_BUFFER, sizeof(green), green, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//vao
	glBindVertexArray(0);
}

void drawleftTopPlain(void)
{
	//Declatation of Matricex
	vmath::mat4 translationMatrix;
	vmath::mat4 rotationMatrix;
	vmath::mat4 modelViewMatrix;
	vmath::mat4 modelViewProjectionMatrix;

	//For Smoke_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(-0.1f, 3.0f, -4.0f);
	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//BindWith vao
	glBindVertexArray(vao_Smoke_Keshari);

	GLfloat curve0[3 * points];
	for (int i = 0; i < points; i++)
	{
		GLfloat Angle = (Smoke1 * (GLfloat)M_PI / 4 * i) / points;
		curve0[3 * i + 0] = -(GLfloat)cos(Angle) * 1.62f - 1.2f;
		curve0[3 * i + 1] = -(GLfloat)sin(Angle) * 1.38f - 1.67f;
		curve0[3 * i + 2] = 0.0f;
	}
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Smoke_Keshari);
	glBufferData(GL_ARRAY_BUFFER, sizeof(curve0), curve0, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Draw the Necessary Scnes
	glPointSize(8.0f);
	glDrawArrays(GL_POINTS, 0, 500);
	//UnBind vao
	glBindVertexArray(0);

	//For Smoke_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(-0.1f, 3.0f, -4.0f);
	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//BindWith vao
	glBindVertexArray(vao_Smoke_White);

	GLfloat curve1[3 * points];
	for (int i = 0; i < points; i++)
	{
		GLfloat Angle = (Smoke1 * (GLfloat)M_PI / 4 * i) / points;
		curve1[3 * i + 0] = -(GLfloat)cos(Angle) * 1.66f - 1.2f;
		curve1[3 * i + 1] = -(GLfloat)sin(Angle) * 1.42f - 1.67f;
		curve1[3 * i + 2] = 0.0f;
	}
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Smoke_White);
	glBufferData(GL_ARRAY_BUFFER, sizeof(curve1), curve1, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Draw the Necessary Scnes
	glPointSize(8.0f);
	glDrawArrays(GL_POINTS, 0, 500);
	//UnBind vao
	glBindVertexArray(0);

	//For Smoke_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(-0.1f, 3.0f, -4.0f);
	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//BindWith vao
	glBindVertexArray(vao_Smoke_Green);

	GLfloat curve2[3 * points];
	for (int i = 0; i < points; i++)
	{
		GLfloat Angle = (Smoke1 * (GLfloat)M_PI / 4 * i) / points;
		curve2[3 * i + 0] = -(GLfloat)cos(Angle) * 1.7f - 1.2f;
		curve2[3 * i + 1] = -(GLfloat)sin(Angle) * 1.46f - 1.67f;
		curve2[3 * i + 2] = 0.0f;
	}
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Smoke_Green);
	glBufferData(GL_ARRAY_BUFFER, sizeof(curve2), curve2, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Draw the Necessary Scnes
	glPointSize(8.0f);
	glDrawArrays(GL_POINTS, 0, 500);
	//UnBind vao
	glBindVertexArray(0);

	if (hidePlane  == false)
	{
		//For Plane_Draw
	//Initialize of Matrix in identity
		translationMatrix = vmath::mat4::identity();
		rotationMatrix = vmath::mat4::identity();
		modelViewMatrix = vmath::mat4::identity();
		modelViewProjectionMatrix = vmath::mat4::identity();

		//Do Necessary Transformation Code Here
		translationMatrix = vmath::translate(k2, l2, -3.0f);
		rotationMatrix = vmath::rotate(Rotate_Angles2, 0.0f, 0.0f, 1.0f);
		//Do Necessary Matrix Multiplication
		modelViewMatrix = translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix * rotationMatrix;

		//Send Necessary Matrix to Shader in Respective Uniform
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		//BindWith vao
		glBindVertexArray(vao_Plane);
		//Draw the Necessary Scnes
		glLineWidth(10.0f);
		glDrawArrays(GL_TRIANGLES, 14, 9);
		glDrawArrays(GL_QUADS, 23, 4);
		glLineWidth(5.0f);
		glDrawArrays(GL_LINES, 0, 14);
		//UnBind vao
		glBindVertexArray(0);
	}
}

void drawleftBottomPlain(void)
{
	//Declatation of Matricex
	vmath::mat4 translationMatrix;
	vmath::mat4 rotationMatrix;
	vmath::mat4 modelViewMatrix;
	vmath::mat4 modelViewProjectionMatrix;

	//For Smoke_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(-0.1f, 0.2f, -4.0f);
	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//BindWith vao
	glBindVertexArray(vao_Smoke_Keshari);

	GLfloat curve0[3 * points];
	for (int i = 0; i < points; i++)
	{
		GLfloat Angle = (Smoke1 * (GLfloat)M_PI / 4 * i) / points;
		curve0[3 * i + 0] = -(GLfloat)cos(Angle) * 1.7f - 1.2f;
		curve0[3 * i + 1] = (GLfloat)sin(Angle) * 1.46f - 1.67f;
		curve0[3 * i + 2] = 0.0f;
	}
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Smoke_Keshari);
	glBufferData(GL_ARRAY_BUFFER, sizeof(curve0), curve0, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Draw the Necessary Scnes
	glPointSize(8.0f);
	glDrawArrays(GL_POINTS, 0, 500);
	//UnBind vao
	glBindVertexArray(0);

	//For Smoke_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(-0.1f, 0.2f, -4.0f);
	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//BindWith vao
	glBindVertexArray(vao_Smoke_White);

	GLfloat curve1[3 * points];
	for (int i = 0; i < points; i++)
	{
		GLfloat Angle = (Smoke1 * (GLfloat)M_PI / 4 * i) / points;
		curve1[3 * i + 0] = -(GLfloat)cos(Angle) * 1.66f - 1.2f;
		curve1[3 * i + 1] = (GLfloat)sin(Angle) * 1.42f - 1.67f;
		curve1[3 * i + 2] = 0.0f;
	}
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Smoke_White);
	glBufferData(GL_ARRAY_BUFFER, sizeof(curve1), curve1, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Draw the Necessary Scnes
	glPointSize(8.0f);
	glDrawArrays(GL_POINTS, 0, 500);
	//UnBind vao
	glBindVertexArray(0);

	//For Smoke_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(-0.1f, 0.2f, -4.0f);
	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//BindWith vao
	glBindVertexArray(vao_Smoke_Green);

	GLfloat curve2[3 * points];
	for (int i = 0; i < points; i++)
	{
		GLfloat Angle = (Smoke1 * (GLfloat)M_PI / 4 * i) / points;
		curve2[3 * i + 0] = -(GLfloat)cos(Angle) * 1.62f - 1.2f;
		curve2[3 * i + 1] = (GLfloat)sin(Angle) * 1.38f - 1.67f;
		curve2[3 * i + 2] = 0.0f;
	}
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Smoke_Green);
	glBufferData(GL_ARRAY_BUFFER, sizeof(curve2), curve2, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Draw the Necessary Scnes
	glPointSize(8.0f);
	glDrawArrays(GL_POINTS, 0, 500);
	//UnBind vao
	glBindVertexArray(0);

	if (hidePlane == false)
	{
		//For Plane_Draw
	//Initialize of Matrix in identity
		translationMatrix = vmath::mat4::identity();
		rotationMatrix = vmath::mat4::identity();
		modelViewMatrix = vmath::mat4::identity();
		modelViewProjectionMatrix = vmath::mat4::identity();

		//Do Necessary Transformation Code Here
		translationMatrix = vmath::translate(k3, l3, -3.0f);
		rotationMatrix = vmath::rotate(Rotate_Angles_3, 0.0f, 0.0f, 1.0f);
		//Do Necessary Matrix Multiplication
		modelViewMatrix = translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix * rotationMatrix;

		//Send Necessary Matrix to Shader in Respective Uniform
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		//BindWith vao
		glBindVertexArray(vao_Plane);
		//Draw the Necessary Scnes
		glLineWidth(10.0f);
		glDrawArrays(GL_TRIANGLES, 14, 9);
		glDrawArrays(GL_QUADS, 23, 4);
		glLineWidth(5.0f);
		glDrawArrays(GL_LINES, 0, 14);
		//UnBind vao
		glBindVertexArray(0);
	}
}

void drawRightBottomPlain(void)
{
	//Declatation of Matricex
	vmath::mat4 translationMatrix;
	vmath::mat4 rotationMatrix;
	vmath::mat4 modelViewMatrix;
	vmath::mat4 modelViewProjectionMatrix;

	//For Smoke_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.45f, -3.16f, -4.0f);
	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//BindWith vao
	glBindVertexArray(vao_Smoke_Keshari);

	GLfloat curve0[3 * points];
	for (int i = 0; i < points; i++)
	{
		GLfloat Angle = (Smoke2 * (GLfloat)M_PI / 4 * i) / points;
		curve0[3 * i + 0] = (GLfloat)sin(Angle) * 1.45f + 1.2f;
		curve0[3 * i + 1] = (GLfloat)cos(Angle) * 1.46f + 1.67f;
		curve0[3 * i + 2] = 0.0f;
	}
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Smoke_Keshari);
	glBufferData(GL_ARRAY_BUFFER, sizeof(curve0), curve0, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Draw the Necessary Scnes
	glPointSize(8.0f);
	glDrawArrays(GL_POINTS, 0, 500);
	//UnBind vao
	glBindVertexArray(0);

	//For Smoke_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.45f, -3.16f, -4.0f);
	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//BindWith vao
	glBindVertexArray(vao_Smoke_White);

	GLfloat curve1[3 * points];
	for (int i = 0; i < points; i++)
	{
		GLfloat Angle = (Smoke2 * (GLfloat)M_PI / 4 * i) / points;
		curve1[3 * i + 0] = (GLfloat)sin(Angle) * 1.41f + 1.2f;
		curve1[3 * i + 1] = (GLfloat)cos(Angle) * 1.42f + 1.67f;
		curve1[3 * i + 2] = 0.0f;
	}
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Smoke_White);
	glBufferData(GL_ARRAY_BUFFER, sizeof(curve1), curve1, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Draw the Necessary Scnes
	glPointSize(8.0f);
	glDrawArrays(GL_POINTS, 0, 500);
	//UnBind vao
	glBindVertexArray(0);

	//For Smoke_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.45f, -3.16f, -4.0f);
	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//BindWith vao
	glBindVertexArray(vao_Smoke_Green);

	GLfloat curve2[3 * points];
	for (int i = 0; i < points; i++)
	{
		GLfloat Angle = (Smoke2 * (GLfloat)M_PI / 4 * i) / points;
		curve2[3 * i + 0] = (GLfloat)sin(Angle) * 1.37f + 1.2f;
		curve2[3 * i + 1] = (GLfloat)cos(Angle) * 1.38f + 1.67f;
		curve2[3 * i + 2] = 0.0f;
	}
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Smoke_Green);
	glBufferData(GL_ARRAY_BUFFER, sizeof(curve2), curve2, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Draw the Necessary Scnes
	glPointSize(8.0f);
	glDrawArrays(GL_POINTS, 0, 500);
	//UnBind vao
	glBindVertexArray(0);
	
	//For Plane_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(k4, l4, -3.0f);
	rotationMatrix = vmath::rotate(Rotate_Angles_4, 0.0f, 0.0f, 1.0f);
	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix * rotationMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//BindWith vao
	glBindVertexArray(vao_Plane);
	//Draw the Necessary Scnes
	glLineWidth(10.0f);
	glDrawArrays(GL_TRIANGLES, 14, 9);
	glDrawArrays(GL_QUADS, 23, 4);
	glLineWidth(5.0f);
	glDrawArrays(GL_LINES, 0, 14);
	//UnBind vao
	glBindVertexArray(0);
}

void drawRightTopPlain(void)
{

	//Declatation of Matricex
	vmath::mat4 translationMatrix;
	vmath::mat4 rotationMatrix;
	vmath::mat4 modelViewMatrix;
	vmath::mat4 modelViewProjectionMatrix;

	//For Smoke_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.45f, -0.315f, -4.0f);
	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//BindWith vao
	glBindVertexArray(vao_Smoke_Keshari);

	GLfloat curve0[3 * points];
	for (int i = 0; i < points; i++)
	{
		GLfloat Angle = (Smoke2 * (GLfloat)M_PI / 4 * i) / points;
		curve0[3 * i + 0] = (GLfloat)sin(Angle) * 1.37f + 1.2f;
		curve0[3 * i + 1] = -(GLfloat)cos(Angle) * 1.38f + 1.67f;
		curve0[3 * i + 2] = 0.0f;
	}
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Smoke_Keshari);
	glBufferData(GL_ARRAY_BUFFER, sizeof(curve0), curve0, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Draw the Necessary Scnes
	glPointSize(8.0f);
	glDrawArrays(GL_POINTS, 0, 500);
	//UnBind vao
	glBindVertexArray(0);

	//For Smoke_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.45f, -0.315f, -4.0f);
	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//BindWith vao
	glBindVertexArray(vao_Smoke_White);

	GLfloat curve1[3 * points];
	for (int i = 0; i < points; i++)
	{
		GLfloat Angle = (Smoke2 * (GLfloat)M_PI / 4 * i) / points;
		curve1[3 * i + 0] = (GLfloat)sin(Angle) * 1.41f + 1.2f;
		curve1[3 * i + 1] = -(GLfloat)cos(Angle) * 1.42f + 1.67f;
		curve1[3 * i + 2] = 0.0f;
	}
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Smoke_White);
	glBufferData(GL_ARRAY_BUFFER, sizeof(curve1), curve1, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Draw the Necessary Scnes
	glPointSize(8.0f);
	glDrawArrays(GL_POINTS, 0, 500);
	//UnBind vao
	glBindVertexArray(0);

	//For Smoke_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.45f, -0.315f, -4.0f);
	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//BindWith vao
	glBindVertexArray(vao_Smoke_Green);

	GLfloat curve2[3 * points];
	for (int i = 0; i < points; i++)
	{
		GLfloat Angle = (Smoke2 * (GLfloat)M_PI / 4 * i) / points;
		curve2[3 * i + 0] = (GLfloat)sin(Angle) * 1.45f + 1.2f;
		curve2[3 * i + 1] = -(GLfloat)cos(Angle) * 1.46f + 1.67f;
		curve2[3 * i + 2] = 0.0f;
	}
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Smoke_Green);
	glBufferData(GL_ARRAY_BUFFER, sizeof(curve2), curve2, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Draw the Necessary Scnes
	glPointSize(8.0f);
	glDrawArrays(GL_POINTS, 0, 500);
	//UnBind vao
	glBindVertexArray(0);


	//For Plane_Draw
	//Initialize of Matrix in identity
	translationMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(k1, l1, -3.0f);
	rotationMatrix = vmath::rotate(Rotate_Angles_1, 0.0f, 0.0f, 1.0f);
	//Do Necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix * rotationMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//BindWith vao
	glBindVertexArray(vao_Plane);
	//Draw the Necessary Scnes
	glLineWidth(10.0f);
	glDrawArrays(GL_TRIANGLES, 14, 9);
	glDrawArrays(GL_QUADS, 23, 4);
	glLineWidth(5.0f);
	glDrawArrays(GL_LINES, 0, 14);
	//UnBind vao
	glBindVertexArray(0);
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(false);
	}
}
