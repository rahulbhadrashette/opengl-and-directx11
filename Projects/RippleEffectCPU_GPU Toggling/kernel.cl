#define DIM 1024

__kernel 
void kernelRipple(__global unsigned char *ptr, float time) 
{
    //pixel position
    int x = get_global_id(0);
    int y = get_global_id(1);

    int width = 1024; //offset = x + y;

    // now calculate the value at that position
    float fx = x - DIM/2;
    float fy = y - DIM/2;

    float d = sqrt( fx * fx + fy * fy );
    unsigned char grey = (unsigned char)(100.0f + 200.0f *
                                         cos(d/10.0f - time/7.0f) /
                                         (d/10.0f + 1.0f));    
    //ptr[offset*4 + 0] = grey;
    //ptr[offset*4 + 1] = grey;
    //ptr[offset*4 + 2] = grey;
    //ptr[offset*4 + 3] = 255;
    // write output vertex
    ptr[y*DIM+x] = (unsigned char)(grey, grey, grey, 255);
}
