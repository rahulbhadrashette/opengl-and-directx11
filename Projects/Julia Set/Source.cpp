// Header files
#include <windows.h>
#include <stdio.h>

#include <gl/GLEW.h>
#include <gl/GL.h>
#include "vmath.h"
#include "palette.h"

// library files
#pragma comment (lib, "user32.lib")
#pragma comment (lib, "gdi32.lib")
#pragma comment (lib, "kernel32.lib")

#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")

// #define Macros
#define WIN_HEIGHT   600
#define WIN_WIDTH    800

enum
{
	RMB_ATTRIBUTE_POSITION = 0,
	RMB_ATTRIBUTE_COLOR,
	RMB_ATTRIBUTE_NORMAL,
	RMB_ATTRIBUTE_TEXTURE0
};

// global variable declaration
FILE* gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

GLuint gShaderProgramObject;

GLuint vao;
GLuint vbo;
GLuint mvpUniform;
GLuint zoomUniform;
GLuint offsetUniform;
GLuint C_Uniform;
GLuint samplerUniform;

float time_offset = 0.0f;
float x_offset = 0.0f;
float y_offset = 0.0f;
bool paused = false;
float RMB_zoom = 1.0f;
GLuint palette_texture;

float fAngle = 0.0f;
vmath::mat4 perspectiveProjectionMatrix;

// function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declaration
	int initialize(void);
	void display(void);

	// local variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szApp[] = TEXT("OpenGLPP");

	bool bDone = false;
	int iRet = 0;
	int WIN_WIDTH_X, WIN_HEIGHT_Y;

	//code
	// Create File io code
	if(fopen_s(&gpFile, "Logfile.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created...!!!"), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log file is created successfully...!!!\n");
	}

	// initialization members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szApp;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// Registering Class
	RegisterClassEx(&wndclass);

	// Centerof the window calculation
	WIN_WIDTH_X = (GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2);
	WIN_HEIGHT_Y = (GetSystemMetrics(SM_CYSCREEN) /2) - (WIN_HEIGHT / 2);

	// Here Create actual Window in Memory 
	// Parallel to glutInitWindowSize(), glutInitWindowPosition(), and glutCreateWindow() all three together
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szApp, TEXT("Raster Group Leader : Rahul_U_M_B :- Julia Fractal"),
	WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
	WIN_WIDTH_X,
	WIN_HEIGHT_Y,
	WIN_WIDTH,
	WIN_HEIGHT,
	NULL, 
	NULL,
	hInstance,
	NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	iRet = initialize();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "ChoosePixelFormat Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "SetPixelFormat is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, " wglCreateContext is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "wglMakeCurrent Failed.\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "Initializetion Successfully Complited.\n");
	}

	while(bDone == false)
	{
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbActiveWindow == true)
			{
				//
			}
			display();
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void resize(int, int);
	void toggleFullScreen(void);
	void uninitialize(void);

	//code
	switch(iMsg)
	{
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;

		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;

		case WM_SIZE:
			resize(LOWORD(lParam), HIWORD(lParam));
			break;

		case WM_ERASEBKGND:
			return(0);

		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;

		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
			}
			break;

		case WM_CHAR:
			switch(wParam)
			{
				case 'f':
				case 'F':
					toggleFullScreen();
					break;

				case 'p':
				case 'P':
					paused = !paused;
					break;

				case '+':
					time_offset = time_offset - 0.00001f;
					break;
				case '-':
					time_offset = time_offset + 0.00001f;
					break;
				case '9':
					time_offset = time_offset - 0.0001f;
					break;
				case '3':
					time_offset = time_offset + 0.0001f;
					break;
				case '8':
					time_offset = time_offset - 0.01f;
					break;
				case '2':
					time_offset = time_offset + 0.01f;
					break;
				case '7':
					time_offset = time_offset - 1.0f;
					break;
				case '1':
					time_offset = time_offset + 1.0f;
					break;
				case 'z':
					RMB_zoom = RMB_zoom * 1.02f;
					break;
				case 'Z':
					RMB_zoom = RMB_zoom / 1.02f;
					break;
				case 'y':
					y_offset = y_offset - RMB_zoom * 0.02f;
					break;
				case 'Y':
					y_offset = y_offset + RMB_zoom * 0.02f;
					break;
				case 'x':
					x_offset = x_offset - RMB_zoom * 0.02f;
					break;
				case 'X':
					x_offset = x_offset + RMB_zoom * 0.02f;
					break;
				
			}
			break;

		case WM_DESTROY:
			uninitialize();
			PostQuitMessage(0);
			break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int initialize(void)
{
	//function declaration
	void resize(int, int);
	void LoadTexture(void);

	//local variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLenum result;

	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLint iShaderCompileStatus;
	GLint iProgramLinkObject;
	GLint iInfoLogLength;
	GLchar *szInfoLog;

	//code
	//pfd structure make zero here
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// pfd structure initialize
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if(iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if(SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if(ghrc == NULL)
	{
		return(-3);
	}

	if(wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	result = glewInit();
	if(result != GLEW_OK)
	{
		fprintf_s(gpFile, "glewInit() is Failed.\n");
		DestroyWindow(ghwnd);
	}

	// Define vertex shader object
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// write vertex shader code
	const GLchar *vertexShaderSourceCode = {

		"#version 460 core"
		"\n"
		"\n in vec4 vPosition;"

		"\n uniform mat4 u_mvp_matrix;"
		"\n uniform float zoom;"
		"\n uniform vec2 offset;"
		
		"\n out vec2 initial_z;"

		"\n void main(void)"
		"\n {"
		"\n		initial_z = (vPosition.xy * zoom) + offset;"
		"\n 	gl_Position = u_mvp_matrix * vPosition;"
		"\n }"
	};

	// Specifing above source to the vertex shader object
	glShaderSource(vertexShaderObject, 1, (const GLchar**) &vertexShaderSourceCode, NULL);

	// Compile the vertex shader
	glCompileShader(vertexShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if(iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, " VS :- %s ", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Define Fragment Shader Object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode = {
		
		"#version 460 core"

		"\n in vec2 initial_z;"
		
		"\n uniform sampler1D tex_gradient;"
		"\n uniform vec2 C;"

		"\n out vec4 fragColor;"

		"\n void main(void)"
		"\n {"
		"\n 	vec2 Z = initial_z;"
		"\n		int iterations = 0;"
		"\n		const float threshold_squared = 32.0;"
		"\n		const int max_iterations = 512;"
		"\n 	while(iterations < max_iterations && dot(Z, Z) < threshold_squared)"
		"\n		{"
		"\n			vec2 Z_squared;"
		"\n			Z_squared.x = Z.x * Z.x - Z.y * Z.y;"
		"\n			Z_squared.y = 2.0 * Z.x * Z.y;"
		"\n			Z = Z_squared + C;"
		"\n			iterations++;"
		"\n		}"
		"\n 	if(iterations == max_iterations)"
		"\n		{"
		"\n			fragColor = vec4(0.0, 0.0, 0.0, 1.0);"
		"\n		}"
		"\n 	else"
		"\n		{"
		"\n 		fragColor = texture(tex_gradient, float(iterations) / float(max_iterations));"
		"\n		}"
		"\n }"
	};

	// Specifing above source to the Fragment Shader Object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**) &fragmentShaderSourceCode, NULL);

	// Compile the Fragment Shader Object
	glCompileShader(fragmentShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if(iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "FS :- %s ", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Create Shader Object
	gShaderProgramObject = glCreateProgram();

	// Attach vertexShaderObject to ShaderProgramObject
	glAttachShader(gShaderProgramObject, vertexShaderObject);
	// Attach fragmentShaderObject to ShaderProgramObject
	glAttachShader(gShaderProgramObject, fragmentShaderObject);

	// Pre Linking Binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, RMB_ATTRIBUTE_POSITION, "vPosition");

	//Link the shader program
	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if(iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, " LS :- %s ", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Post Linking Retriving Uniform Location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	samplerUniform = glGetUniformLocation(gShaderProgramObject, "tex_gradient");
	zoomUniform = glGetUniformLocation(gShaderProgramObject, "zoom");
	offsetUniform = glGetUniformLocation(gShaderProgramObject, "offset");
	C_Uniform = glGetUniformLocation(gShaderProgramObject, "C");

	// triangle Array
	const GLfloat vertices[] =
								{
									1.8f, 1.0f, 0.5f,
									-1.8f, 1.0f, 0.5f,
									-1.8f, -1.0f, 0.5f,
									1.8f, -1.0f, 0.5f,
								};

	// Create vao
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(RMB_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(RMB_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix = vmath::mat4::identity();
	LoadTexture();
	// Warmup call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);
	
	return(0);
}

void LoadTexture(void)
{
    glGenTextures(1, &palette_texture);
    glBindTexture(GL_TEXTURE_1D, palette_texture);
    glTexStorage1D(GL_TEXTURE_1D, 8, GL_RGB8, 512);
    glTexSubImage1D(GL_TEXTURE_1D, 0, 0, 512, GL_RGB, GL_UNSIGNED_BYTE, palette);
    glGenerateMipmap(GL_TEXTURE_1D);
}

void resize(int width, int height)
{
	if(height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	// function declaration
	void update(void);
	// function call
	update();
	
	float r = fAngle + time_offset;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity
	vmath::mat4 modelViewMatrix = vmath::mat4::identity();;
	vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
	vmath::mat4 translationMatrix = vmath::mat4::identity();
	
	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	
	//Do Necessary Matrix Multiplication
	modelViewMatrix = modelViewMatrix * translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform2f(offsetUniform, x_offset, y_offset);
	glUniform2f(C_Uniform, (sinf(r * 0.1f) + cosf(r * 0.23f)) * 0.5f, (cosf(r * 0.13f) + sinf(r * 0.21f)) * 0.5f);
	glUniform1f(zoomUniform, RMB_zoom);

	//Similarly Bind With Textures
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_1D, palette_texture);
	glUniform1i(samplerUniform, 0);

	//BindWith vao
	glBindVertexArray(vao);

	//Draw the Necessary Scnes
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	//UnBind vao
	glBindVertexArray(0);

	//UnUse Program
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void uninitialize(void)
{
	// code
	if(vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;

		if(vbo)
		{
			glDeleteBuffers(1, &vbo);
			vbo = 0;
		}
	}

	if(gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);

		// Ask program how many shader are attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);

		if(pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);

			for(shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		glUseProgram(0);
	}

	if(gbFullScreen)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	// Break the Current Context
	if(wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if(ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if(ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
	}

	if(gpFile)
	{
		fprintf_s(gpFile, "Log file is closed successfully.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void toggleFullScreen(void)
{
	MONITORINFO mi;
	if(gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if(GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
								mi.rcMonitor.left,
								mi.rcMonitor.top,
								mi.rcMonitor.right - mi.rcMonitor.left,
								mi.rcMonitor.bottom - mi.rcMonitor.top,
								SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void update(void)
{
	if(!paused)
	{
		fAngle = fAngle +0.005f;
		if(fAngle >= 360.0f)
		{
			fAngle = 0.0f;
		}
	}	
}
