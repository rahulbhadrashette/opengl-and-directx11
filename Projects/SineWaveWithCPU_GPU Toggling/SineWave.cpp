//Header Files
#include <Windows.h>
#include <stdio.h>
#include <gl/GLEW.h>
#include <gl/GL.h>
#include <CL/opencl.h>
#include "vmath.h"

//Library Files
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")
#pragma comment (lib, "OpenCL.lib")

//User define Micro
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTCOORD0
};

//Global  variable Declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;
FILE* gpFile = NULL;
bool gbActiveWindow = false;
bool bIsFullScreen = false;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

//Programmable Pipeline Variable
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao;
GLuint vbo_CPU;
GLuint vbo_GPU;

GLuint mvpUniform;
vmath::mat4 perspectiveProjectionMatrix;

//OpenCL
const int gMesh_Width = 1024;
const int gMesh_Height = 1024;
#define MYARRAYSIZE [gMesh_Width]*[gMesh_Height]*4;

float pos[gMesh_Width][gMesh_Height][4];

float animationFlag = 0.0f;

bool OnGPU = false;
bool onCPU = false;

// global OpenCL variables
cl_int ret_ocl;
cl_platform_id oclPlatformID;
cl_device_id oclComputeDeviceID; // compute device id

cl_context oclContext; // compute context
cl_command_queue oclCommandQueue; // compute command queue
cl_program oclProgram; // compute program
cl_kernel oclKernel; // compute kernel

size_t sizeKernelCodeLength;
char *oclSourceCode=NULL;
GLuint texture;

cl_mem cl_VBO_Mem=NULL;
cl_mem deviceOutput=NULL;

size_t localWorkSize[2];
size_t globalWorkSize[2];

//Function Declaration(Prototype/ Signature)
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void Uninitialize_RMB(void);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	int Initialize_RMB(void);
	void Display_RMB(void);

	//Local Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR str[] = TEXT("BlueScreen");
	bool bDone = false;
	int iRet = 0;
	int X, Y;

	//File io
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created"), TEXT("Error"), MB_OK);
	}
	else
	{
		fprintf_s(gpFile, "Log file is successfully complited...!!!\n");
		fflush(gpFile);
	}

	//Initialize WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = str;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register WinclassEx
	RegisterClassEx(&wndclass);

	//CenterOfTheWindowCalculation
	X = (GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2);
	Y = (GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2);

	//CreateWindow
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, str, TEXT("RahulUMB...08-3DPyramidAndCube)...!!!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X,
		Y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = Initialize_RMB();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "ChoosePixelFormat Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "SetPixelFormat is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, " wglCreateContext is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "wglMakeCurrent Failed.\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "Initializetion Successfully Complited.\n");
	}

	//Here Actual ShowWindow
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//MessageLoop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

		}
		else
		{
			if (gbActiveWindow == true)
			{
				//Update();
			}
			Display_RMB();
		}
	}

	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void Resize_RMB(int, int);
	void ToggleFullScreen(void);

	switch (iMsg)
	{
		//code
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;

		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;

		case WM_SIZE:
			Resize_RMB(LOWORD(lParam), HIWORD(lParam));
			break;

		case WM_ERASEBKGND:
			return(0);

		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;

		case WM_KEYDOWN:
			switch (wParam)  //in Past switch(LOWORD(wParam))
			{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			}
			break;

		case WM_CHAR:
			switch (wParam)
			{
				case 'F':
				case 'f':
					ToggleFullScreen();
					break;

				case 'c':
				case 'C':
					OnGPU = false;
					onCPU = true;
					break;

				case 'g':
				case 'G':
					onCPU = false;
					OnGPU = true;
					break;
			}
			break;

		case WM_DESTROY:
			Uninitialize_RMB();
			PostQuitMessage(0);
			break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize_RMB(void)
{
	//Function Declaration
	void Resize_RMB(int, int);
	char* loadOclProgramSource(const char *, const char *, size_t *);

	//Local Variable Declaration
	GLenum result;
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
    
	//code
	//Initialize pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));  //or   //For only Windows
	//memset((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));  //For All OS
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;
	
	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	//the return index is always 1 based to 38 if Zero(0) return failure 
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf_s(gpFile, "glewInit(); is Failed.\n");
		Uninitialize_RMB();
		DestroyWindow(ghwnd);
	}

	// get OpenCL supporting platform's ID
    ret_ocl=clGetPlatformIDs(1,&oclPlatformID,NULL);
    if(ret_ocl != CL_SUCCESS)
    {
        fprintf_s(gpFile,"OpenCL Error - clGetDeviceIDs() Failed : %d. Exitting Now ...\n",ret_ocl);
        Uninitialize_RMB();
		DestroyWindow(ghwnd);
    }
    
    // get OpenCL supporting GPU device's ID
    ret_ocl=clGetDeviceIDs(oclPlatformID,CL_DEVICE_TYPE_GPU,1,&oclComputeDeviceID,NULL);
    if(ret_ocl != CL_SUCCESS)
    {
        fprintf_s(gpFile,"OpenCL Error - clGetDeviceIDs() Failed : %d. Exitting Now ...\n",ret_ocl);
        Uninitialize_RMB();
		DestroyWindow(ghwnd);
    }
    
    char gpu_name[255];
    clGetDeviceInfo(oclComputeDeviceID,CL_DEVICE_NAME,sizeof(gpu_name),&gpu_name,NULL);
    fprintf_s(gpFile,"CPU or GPU :- %s\n",gpu_name);
    
    cl_context_properties contextProperties[] =
    {
		CL_GL_CONTEXT_KHR, (cl_context_properties)wglGetCurrentContext(),
		CL_WGL_HDC_KHR, (cl_context_properties)wglGetCurrentDC(),
		CL_CONTEXT_PLATFORM, (cl_context_properties)oclPlatformID,
		0
	};
    // create OpenCL compute context
    oclContext=clCreateContext(contextProperties, 1, &oclComputeDeviceID, NULL, NULL,&ret_ocl);
    if(ret_ocl!=CL_SUCCESS)
    {
    	fprintf_s(gpFile,"Could not create GPU context, trying CPU...");
    	fprintf_s(gpFile,"OpenCL Error - clCreateContext() Failed : %d. Exitting Now ...\n",ret_ocl);
        oclContext = clCreateContextFromType(contextProperties, CL_DEVICE_TYPE_CPU, NULL, NULL, &ret_ocl);
        if (ret_ocl!=CL_SUCCESS)
        {
            fprintf_s(gpFile,"clCreateContextFromType() Failed to create an OpenCL GPU or CPU context.");
            return NULL;
        }
    }

    // create command queue
    oclCommandQueue=clCreateCommandQueue(oclContext,oclComputeDeviceID,0,&ret_ocl);
    if(ret_ocl!=CL_SUCCESS)
    {
        fprintf_s(gpFile,"OpenCL Error - clCreateCommandQueue() Failed : %d. Exitting Now ...\n",ret_ocl);
        Uninitialize_RMB();
		DestroyWindow(ghwnd);
    }
    
     // create OpenCL program from .cl
    oclSourceCode=loadOclProgramSource("SineWave.cl","",&sizeKernelCodeLength);
    
    cl_int status=0;
    oclProgram = clCreateProgramWithSource(oclContext, 1, (const char **)&oclSourceCode, &sizeKernelCodeLength, &ret_ocl);
    if(ret_ocl!=CL_SUCCESS)
    {
        fprintf_s(gpFile,"OpenCL Error - clCreateProgramWithSource() Failed : %d. Exitting Now ...\n",ret_ocl);
        Uninitialize_RMB();
		DestroyWindow(ghwnd);
    }
    
    // build OpenCL program
    ret_ocl=clBuildProgram(oclProgram,0,NULL,NULL,NULL,NULL);
    if(ret_ocl!=CL_SUCCESS)
    {
        fprintf_s(gpFile,"OpenCL Error - clBuildProgram() Failed : %d. Exitting Now ...\n",ret_ocl);
        
        size_t len;
        char buffer[2048];
        clGetProgramBuildInfo(oclProgram,oclComputeDeviceID,CL_PROGRAM_BUILD_LOG,sizeof(buffer),buffer,&len);
        fprintf_s(gpFile,"OpenCL Program Build Log : %s\n",buffer);
        Uninitialize_RMB();
		DestroyWindow(ghwnd);
    }
    
     // create OpenCL kernel by passing kernel function name that we used in .cl file
    oclKernel=clCreateKernel(oclProgram,"simpleGL",&ret_ocl);
    if(ret_ocl!=CL_SUCCESS)
    {
        fprintf_s(gpFile,"OpenCL Error - clCreateKernel() Failed : %d. Exitting Now ...\n",ret_ocl);
        Uninitialize_RMB();
		DestroyWindow(ghwnd);
    }


	//Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
			"gl_Position = u_mvp_matrix * vPosition;" \
		"}";

	//Specifing above source to the vertex shader object
	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//Compile the Vertex Shader
	glCompileShader(gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "vertexShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);
				Uninitialize_RMB();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
			"fragColor = vec4(1.0, 0.0, 0.0, 0.0);" \
		"}";


	// Specifing above Source to the fragment Shader Object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//Compile the fragment Shader Object
	glCompileShader(gFragmentShaderObject);

	GLint iFShaderCompileStatus = 0;
	GLint iFInfoLogLength = 0;
	GLint iInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iFShaderCompileStatus);
	if (iFShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iFInfoLogLength);
		if (iFInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iFInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iFInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "FragmentShaderInfoLog:- %s", szInfoLog);
				free(szInfoLog);

				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Create Shader Object
	gShaderProgramObject = glCreateProgram();

	//Attach VertexShader to the Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//Attach fragmentShader to the Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Pre Linking Binding to Vertex Attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	
	//Link the Shader Program
	glLinkProgram(gShaderProgramObject);

	GLint  iProgramLinkStatus = 0;
	GLint iLInfoLogLength = 0;
	GLint *szLogLength = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iLInfoLogLength);
		if (iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(iLInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iLInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "LinkShaderProgramObject:- %s", szInfoLog);
				free(szInfoLog);
				Uninitialize_RMB();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Post Linking Retriving Uniform Location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	
	//TrangleArray
	//const GLfloat position[1024*1024*4] = 
	for(int i = 0; i < gMesh_Width; i++)
	{
		for(int j = 0; j <gMesh_Height; j++)
		{
			for(int k = 0; k < 4; k++)
			{
				pos[i][j][k] = 0.0f;
			}
		}
	}

	//Create vao
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo_CPU);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_CPU);
	glBufferData(GL_ARRAY_BUFFER, gMesh_Width * gMesh_Height * 4 * sizeof(float), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_GPU);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_GPU);
	glBufferData(GL_ARRAY_BUFFER, gMesh_Width * gMesh_Height * 4 * sizeof(float), NULL, GL_DYNAMIC_DRAW);
	
	GLint bSize;
	glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &bSize);
	if (bSize != (1024 * 1024 * 4 * sizeof(float))) {
		fprintf(gpFile, "VertexBufferObject- vbo_gpu has incorrect size: %d", bSize);
		Uninitialize_RMB();
		DestroyWindow(ghwnd);
		exit(0);
	}
	
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	cl_VBO_Mem = clCreateFromGLBuffer(oclContext, CL_MEM_READ_WRITE, vbo_GPU, &ret_ocl);
	if(ret_ocl != CL_SUCCESS)
    {
        fprintf_s(gpFile,"OpenCL Error - clCreateFromGLBuffer() Failed : %d. Exitting Now ...\n",ret_ocl);
       	Uninitialize_RMB();
		DestroyWindow(ghwnd);
    }
	
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Unbind  vao
	glBindVertexArray(0);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glDisable(GL_CULL_FACE);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	perspectiveProjectionMatrix = vmath::mat4::identity();
	//Warmup Call To Resize
	Resize_RMB(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}


void Resize_RMB(int Width, int Height)
{
	//code
	if (Height == 0)
	{
		Height = 1;
	}
	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);
}

void Display_RMB(void)
{
	size_t roundGlobalSizeToNearestMultipleOfLocalSize(int, unsigned int);
	void launchCPUKernel(unsigned int, unsigned int, float);
	void cleanup(void);
	float *pPos = NULL;
	size_t byteCount;


	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	//Declatation of Matrices
	vmath::mat4 modelViewMatrix;
	vmath::mat4 modelViewProjectionMatrix;

	//Initialize of Matrix in identity
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	//Do Necessary Transformation Code Here
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.6f);

	//Do Necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	if(OnGPU)
	{
		// set OpenCL kernel arguments. Our OpenCL kernel has 4 arguments 0,1,2,3
	    // set 0 based 0th argument i.e. deviceInput
	    ret_ocl=clSetKernelArg(oclKernel,0,sizeof(cl_mem),(void *)&cl_VBO_Mem); // 'deviceInput' maps to 'in1' param of kernel function in .cl file
	    if(ret_ocl != CL_SUCCESS)
	    {
	        fprintf_s(gpFile,"OpenCL Error - clSetKernelArg() Failed For 1st Argument : %d. Exitting Now ...\n",ret_ocl);
	        Uninitialize_RMB();
			DestroyWindow(ghwnd);
	    }

	    ret_ocl=clSetKernelArg(oclKernel,1,sizeof(cl_int),(void *)&gMesh_Width); // 'deviceInput' maps to 'in1' param of kernel function in .cl file
	    if(ret_ocl != CL_SUCCESS)
	    {
	        fprintf_s(gpFile,"OpenCL Error - clSetKernelArg() Failed For 1st Argument : %d. Exitting Now ...\n",ret_ocl);
	        Uninitialize_RMB();
			DestroyWindow(ghwnd);
	    }

	    ret_ocl=clSetKernelArg(oclKernel,2,sizeof(cl_int),(void *)&gMesh_Height); // 'deviceInput' maps to 'in1' param of kernel function in .cl file
	    if(ret_ocl != CL_SUCCESS)
	    {
	        fprintf_s(gpFile,"OpenCL Error - clSetKernelArg() Failed For 1st Argument : %d. Exitting Now ...\n",ret_ocl);
	        Uninitialize_RMB();
			DestroyWindow(ghwnd);
	    }

	    ret_ocl=clSetKernelArg(oclKernel,3,sizeof(cl_float),(void *)&animationFlag); // 'deviceInput' maps to 'in1' param of kernel function in .cl file
	    if(ret_ocl != CL_SUCCESS)
	    {
	        fprintf_s(gpFile,"OpenCL Error - clSetKernelArg() Failed For 1st Argument : %d. Exitting Now ...\n",ret_ocl);
	        Uninitialize_RMB();
			DestroyWindow(ghwnd);
	    }
	    
	    // Acquire the GL Object
		ret_ocl = clEnqueueAcquireGLObjects(oclCommandQueue, 1, &cl_VBO_Mem, 0, NULL, NULL );
		if(ret_ocl != CL_SUCCESS)
	    {
	        fprintf_s(gpFile,"OpenCL Error - clEnqueueAcquireGLObjects() Failed : %d. Exitting Now ...\n",ret_ocl);
	        Uninitialize_RMB();
			DestroyWindow(ghwnd);
	    }

	    // Queue the kernel up for execution across the array
	    localWorkSize[0] = localWorkSize[1] = 8;
	    globalWorkSize[0] = roundGlobalSizeToNearestMultipleOfLocalSize(localWorkSize[0], 1024);
	    globalWorkSize[1] = roundGlobalSizeToNearestMultipleOfLocalSize(localWorkSize[1], 1024);
    	ret_ocl = clEnqueueNDRangeKernel(oclCommandQueue, oclKernel, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
    	if(ret_ocl != CL_SUCCESS)
	    {
	        fprintf_s(gpFile,"OpenCL Error - clEnqueueNDRangeKernel() Failed : %d. Exitting Now ...\n",ret_ocl);
	        Uninitialize_RMB();
			DestroyWindow(ghwnd);
	    }

	    // Release the GL Object
		// Note, we should ensure OpenCL is finished with any commands that might affect the VBO
    	ret_ocl = clEnqueueReleaseGLObjects(oclCommandQueue, 1, &cl_VBO_Mem, 0, NULL, NULL );
    	if(ret_ocl != CL_SUCCESS)
	    {
	        fprintf_s(gpFile,"OpenCL Error - clEnqueueReleaseGLObjects() Failed : %d. Exitting Now ...\n",ret_ocl);
	        Uninitialize_RMB();
			DestroyWindow(ghwnd);
	    }

		clFinish(oclCommandQueue);
	}
	else
	{
		launchCPUKernel(gMesh_Height, gMesh_Width, animationFlag);

	}

	glBindVertexArray(vao);
	if(OnGPU)
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbo_GPU);
	}
	else
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbo_CPU);

		glBufferData(GL_ARRAY_BUFFER, gMesh_Width * gMesh_Height * 4 * sizeof(float), pos, GL_DYNAMIC_DRAW);
	}
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	
	//Draw the Necessary Scnes
	glDrawArrays(GL_POINTS, 0, gMesh_Width * gMesh_Height);
	//UnBind vao
	glBindVertexArray(0);

	//UnUse Program
	glUseProgram(0);

	SwapBuffers(ghdc);

	animationFlag = animationFlag +0.1f;
}

void Uninitialize_RMB(void)
{
	//code//
	//OpenGL
	if (vbo_CPU)
	{
		glDeleteBuffers(1, &vbo_CPU);
		vbo_CPU = 0;
	}
	if (vbo_GPU)
	{
		glDeleteBuffers(1, &vbo_GPU);
		vbo_GPU = 0;
	}
	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}
	
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	if (bIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//Break the Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// OpenCL cleanup
    if(oclSourceCode)
    {
        free((void *)oclSourceCode);
        oclSourceCode=NULL;
    }
    
    if(oclKernel)
    {
        clReleaseKernel(oclKernel);
        oclKernel=NULL;
    }
    
    if(oclProgram)
    {
        clReleaseProgram(oclProgram);
        oclProgram=NULL;
    }
    
    if(oclCommandQueue)
    {
        clReleaseCommandQueue(oclCommandQueue);
        oclCommandQueue=NULL;
    }
    
    if(oclContext)
    {
        clReleaseContext(oclContext);
        oclContext=NULL;
    }

    // free allocated device-memory
   
    
    if(deviceOutput)
    {
        clReleaseMemObject(deviceOutput);
        deviceOutput=NULL;
    }

	if (gpFile)
	{
		fprintf_s(gpFile, "Log file is closed Successfully. \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(false);
		bIsFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bIsFullScreen = false;
	}

}

void Update(void)
{
	
}

char* loadOclProgramSource(const char *filename, const char *preamble, size_t *sizeFinalLength)
{
    // locals
    FILE *pFile=NULL;
    size_t sizeSourceLength;
    
    pFile=fopen(filename,"rb"); // binary read
    if(pFile==NULL)
        return(NULL);
    
    size_t sizePreambleLength=(size_t)strlen(preamble);
    
    // get the length of the source code
    fseek(pFile,0,SEEK_END);
    sizeSourceLength=ftell(pFile);
    fseek(pFile,0,SEEK_SET); // reset to beginning
    
    // allocate a buffer for the source code string and read it in
    char *sourceString=(char *)malloc(sizeSourceLength+sizePreambleLength+1);
    memcpy(sourceString, preamble, sizePreambleLength);
    if(fread((sourceString)+sizePreambleLength,sizeSourceLength,1,pFile)!=1)
    {
        fclose(pFile);
        free(sourceString);
        return(0);
    }
    
    // close the file and return the total length of the combined (preamble + source) string
    fclose(pFile);
    if(sizeFinalLength != 0)
    {
        *sizeFinalLength = sizeSourceLength + sizePreambleLength;
    }
    sourceString[sizeSourceLength + sizePreambleLength]='\0';
    
    return(sourceString);
}

void launchCPUKernel(unsigned int width, unsigned int height, float time)
{
	for(int i = 0; i < width; i++)
	{
		for(int j = 0; j <height; j++)
		{
			for(int k = 0; k < 4; k++)
			{
				float u = i /(float)width;
				float v = j /(float)height;

				u = (u * 2.0) - 1.0;
				v = (v * 2.0) - 1.0;

				float frequency = 4.0f;
				float w = sin(frequency * u + time) * cos(frequency * v + time) * 0.5f;

				if(k == 0)
				{
					pos[i][j][k] = u;	
				}

				if( k == 1)
				{
					pos[i][j][k] = w;
				}

				if(k == 2)
				{
					pos[i][j][k] = v;
				}

				if(k == 3)
				{
					pos[i][j][k] = 1.0;
				}
				
			}
		}
	}
}

size_t roundGlobalSizeToNearestMultipleOfLocalSize(int local_size, unsigned int global_size)
{
    // code
    unsigned int r = global_size % local_size;
    if(r == 0)
    {
        return(global_size);
    }
    else
    {
        return(global_size + local_size - r);
    }
}
