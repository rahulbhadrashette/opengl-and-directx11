package com.astromedicomp.dynamicindia;
//added by me
import android.content.Context;
import android.view.Gravity;
import android.graphics.Color; // for "Color" class
import android.view.MotionEvent; // for "MotionEvent" class
import android.view.GestureDetector; // for "GestureDetector"
import android.view.GestureDetector.OnGestureListener; //for "OnGestureListener"  
import android.view.GestureDetector.OnDoubleTapListener; // for "OnDoubleTapListener" 

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import java.nio.ByteBuffer;		//for OpenGL Buffers
import java.nio.ByteOrder;      //for OpenGL Buffers
import java.nio.FloatBuffer;	//for OpenGL Buffers
import java.lang.Math; 

import android.opengl.Matrix;   //for matrix math

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int[] vao_I = new int[1];
    private int[] vbo_I_Position = new int[1];
    private int[] vbo_I_Color = new int[1];

    private int[] vao_N = new int[1];
    private int[] vbo_N_Position = new int[1];
    private int[] vbo_N_Color = new int[1];

    private int[] vao_D = new int[1];
    private int[] vbo_D_Position = new int[1];
    private int[] vbo_D_Color = new int[1];

    private int[] vao_A = new int[1];
    private int[] vbo_A_Position = new int[1];
    private int[] vbo_A_Color = new int[1];

    private int[] vao_A_TriBand = new int[1];
    private int[] vbo_A_TriBand_Position = new int[1];
    private int[] vbo_A_TriBand_Color = new int[1];

    float PrintEnd;
    float nig_X = -3.0f;
    float pos_X = 4.6f;
    float pos_Y = 3.5f;
    float neg_Y = -3.5f;
    float one = 0.0f, pointFour = 0.0f;

    boolean IFlag1 = true;
    boolean NFlag = true;
    boolean DFlag = true;
    boolean IFlag2 = true;
    boolean AFlag = true;

    private int mvpUniform;
    private float[] perspectiveProjectionMatrix = new float[16];  // 4 x 4  Matrix


	public GLESView(Context dwContext)
	{
		super(dwContext);

        context = dwContext;

		setEGLContextClientVersion(3); //version 3.x pahije manun
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(dwContext, this, null, false);  //this means 'handler' i.e who is going to handle
		gestureDetector.setOnDoubleTapListener(this);   // this means 'handler' i.e. who is going to handle
    
	}
	// Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
    	//code
    	int eventaction = event.getAction();
    	if(!gestureDetector.onTouchEvent(event))
    		super.onTouchEvent(event);
    	return(true);
    }

     // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
    	 // Do Not Write Any code Here Because Already Written 'onDoubleTap'
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
    	// Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
    	Uninitialize_RMB();
    	System.exit(0);
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
    	return(true);
    }

    //Impliment GLSurfaceView.Renderer Mathods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig congif)  //WM_CREATE OR Initialize
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR:" +version);
        String version1 = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR:" +version1);
        initialize_RMB();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int Width, int Height)  //WM_SIZE or RESIZE
    {
        Resize_RMB(Width, Height);
    }

    @Override
    public void onDrawFrame(GL10 unused)  //Display
    {
        Update_RMB();
        Display_RMB();
        System.out.println("RTR: onDrawFrame");
    }

    //Our Custome Methods
    private void initialize_RMB()
    {
    	vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
    	final String vertexShaderSourceCode = String.format 
    	(
    		"#version 320 es" +
            "\n" +
            "in vec4 vPosition;" +
            "in vec4 vColor;" +
            "uniform mat4 u_mvp_matrix;" +
            "out vec4 out_Color;" +
            "void main(void)" +
            "{" +
            "gl_Position = u_mvp_matrix * vPosition;" +
            "out_Color = vColor;" +
            "}"

    	);

        //Specifing above source to the vertex shader object
    	GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

        //Compile the Vertex Shader
        GLES32.glCompileShader(vertexShaderObject);

    	int[] iShaderCompileStatus = new int[1];
    	int[] iInfoLogLength = new int[1];
    	String szInfoLog = null;

    	GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
    	if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);

    			System.out.println("RTR: vertexShaderInfoLog" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

        //Define Fragment Shader Object
    	fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        //Write Fragment Shader Code
    	final String fragmentShaderSourceCode = String.format
    	(
    		"#version 320 es" +
			"\n" +
			"precision highp float;" +
            "in vec4 out_Color;" +
			"out vec4 fragColor;" +
			"void main(void)" +
			"{" +
				"fragColor = out_Color;" +
			"}"
    	);

        // Specifing above Source to the fragment Shader Object
    	GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
       
        //Compile the fragment Shader Object
    	GLES32.glCompileShader(fragmentShaderObject);

    	iShaderCompileStatus[0] = 0;
    	iInfoLogLength[0] = 0;
    	szInfoLog = null;

    	GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
    	if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);

    			System.out.println("RTR: FragmentShaderInfoLog" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

    	shaderProgramObject = GLES32.glCreateProgram();
    	GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
    	GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

    	//pre Link Binding to attributes
    	GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_COLOR, "vColor");
    	//Link the Shader Program
    	GLES32.glLinkProgram(shaderProgramObject);

    	int[] iProgramLinkStatus  = new int[1];
    	iInfoLogLength[0] = 0;
    	szInfoLog = null;

    	GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus,0);
    	if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			GLES32.glGetProgramInfoLog(shaderProgramObject);

    			System.out.println("RTR: ProgramLinkStatus" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

    	//Post Linking Retriving Uniform Location
    	mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

        //Array For I Vertices
    	final float[] iVertices = new float[]
                                	{
                                    	-1.0f, 1.0f, 0.0f,
                                        -1.0f, -1.0f, 0.0f
        	                        };
        //Array For I Color
        final float[] iColor = new  float[]
                                    {
                                        //Line
                                        1.0f,0.4f,0.0f,
                                        0.0f, 1.0f, 0.0f
                                    };

        //Array For N Vertices
        final float[] nVertices = new float[] 
                                    {
                                        -1.0f, 1.0f, 0.0f,
                                        -1.0f, -1.0f, 0.0f,
                                        -1.0f, 1.0f, 0.0f,
                                        -0.2f, -1.0f, 0.0f,
                                        -0.2f, 1.0f, 0.0f,
                                        -0.2f, -1.0f, 0.0f
                                    };

         //Array For N Color
        final float[] nColor = new float[]
                                    {
                                        1.0f,0.4f,0.0f,
                                        0.0f, 1.0f, 0.0f,
                                        1.0f,0.4f,0.0f,
                                        0.0f, 1.0f, 0.0f,
                                        1.0f,0.4f,0.0f,
                                        0.0f, 1.0f, 0.0f
                                    };
        //Array For D Vertices
        final float[] dVertices = new  float[]
                                    {
                                        -1.0f, 0.98f, 0.0f,
                                        -1.0f, -0.98f, 0.0f,
                                        -1.2f, 0.985f, 0.0f,
                                        -0.2f, 0.985f, 0.0f,
                                        -1.2f, -0.98f, 0.0f,
                                        -0.2f, -0.98f, 0.0f,
                                        -0.2f, 1.0f, 0.0f,
                                        -0.2f, -1.0f, 0.0f
                                    };
        
        //Array For A_TriBand Vertices
        final float[] a_TriBandVertices = new  float[]
                                    {
                                        -0.05f, -0.045f, 0.0f,
                                        -0.55f, -0.045f, 0.0f,
                                        -0.04f, -0.075f, 0.0f,
                                        -0.56f, -0.075f, 0.0f,
                                        -0.02f, -0.105f, 0.0f,
                                        - 0.58f, -0.105f, 0.0f
                                    };

        //Array For A_TriBand Color
        final float[] a_TriBandColor = new  float[]
                                    {
                                        1.0f,0.5f,0.0f,
                                        1.0f,0.5f,0.0f,
                                        1.0f, 1.0f, 1.0f,
                                        1.0f, 1.0f, 1.0f,
                                        0.0f, 1.0f, 0.0f,
                                        0.0f, 1.0f, 0.0f,
                                    };

        //Array For A Vertices
        final float[] aVertices = new float[]
                                    {
                                        -0.4f, 1.0f, 0.0f,
                                        0.20f, -1.0f, 0.0f,
                                        -0.4f, 1.0f, 0.0f,
                                        -0.80f, -1.0f, 0.0f
                                    };
        //Array For A Color
        final float[] aColor = new  float[]
                                    {
                                        1.0f,0.4f,0.0f,
                                        0.0f, 1.0f, 0.0f,
                                        1.0f,0.4f,0.0f,
                                        0.0f, 1.0f, 0.0f,
                                    };

        //vao For I
        GLES32.glGenVertexArrays(1, vao_I,0);
    	GLES32.glBindVertexArray(vao_I[0]);
    	GLES32.glGenBuffers(1, vbo_I_Position,0);
    	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_I_Position[0]);
    	ByteBuffer byteBuffer = ByteBuffer.allocateDirect(iVertices.length * 4);
    	byteBuffer.order(ByteOrder.nativeOrder());
    	FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
    	positionBuffer.put(iVertices);
    	positionBuffer.position(0);
    	GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, iVertices.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
    	GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
    	GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
    	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glGenBuffers(1, vbo_I_Color,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_I_Color[0]);
        ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(iColor.length * 4);
        byteBuffer1.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer1 = byteBuffer1.asFloatBuffer();
        positionBuffer1.put(iColor);
        positionBuffer1.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, iColor.length * 4, positionBuffer1, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
    	GLES32.glBindVertexArray(0);

        //vao For N
        GLES32.glGenVertexArrays(1, vao_N,0);
        GLES32.glBindVertexArray(vao_N[0]);
        GLES32.glGenBuffers(1, vbo_N_Position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_N_Position[0]);
        ByteBuffer byteBuffer3 = ByteBuffer.allocateDirect(nVertices.length * 4);
        byteBuffer3.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer3 = byteBuffer3.asFloatBuffer();
        positionBuffer3.put(nVertices);
        positionBuffer3.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, nVertices.length * 4, positionBuffer3, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glGenBuffers(1, vbo_N_Color,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_N_Color[0]);
        ByteBuffer byteBuffer4 = ByteBuffer.allocateDirect(nColor.length * 4);
        byteBuffer4.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer4 = byteBuffer4.asFloatBuffer();
        positionBuffer4.put(nColor);
        positionBuffer4.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, nColor.length * 4, positionBuffer4, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        //vao For D
        GLES32.glGenVertexArrays(1, vao_D,0);
        GLES32.glBindVertexArray(vao_D[0]);
        GLES32.glGenBuffers(1, vbo_D_Position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_D_Position[0]);
        ByteBuffer byteBuffer5 = ByteBuffer.allocateDirect(dVertices.length * 4);
        byteBuffer5.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer5 = byteBuffer5.asFloatBuffer();
        positionBuffer5.put(dVertices);
        positionBuffer5.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, dVertices.length * 4, positionBuffer5, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glGenBuffers(1, vbo_D_Color,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_D_Color[0]);
        /*ByteBuffer byteBuffer6 = ByteBuffer.allocateDirect(dColor.length * 4);
        byteBuffer6.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer6 = byteBuffer6.asFloatBuffer();
        positionBuffer6.put(dColor);
        positionBuffer6.position(0);*/
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 8*3*4, null, GLES32.GL_DYNAMIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);


        //vao For A_TriBand
        GLES32.glGenVertexArrays(1, vao_A_TriBand,0);
        GLES32.glBindVertexArray(vao_A_TriBand[0]);
        GLES32.glGenBuffers(1, vbo_A_TriBand_Position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_A_TriBand_Position[0]);
        ByteBuffer byteBuffer7 = ByteBuffer.allocateDirect(a_TriBandVertices.length * 4);
        byteBuffer7.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer7 = byteBuffer7.asFloatBuffer();
        positionBuffer7.put(a_TriBandVertices);
        positionBuffer7.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, a_TriBandVertices.length * 4, positionBuffer7, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glGenBuffers(1, vbo_A_Color,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_A_Color[0]);
        ByteBuffer byteBuffer8 = ByteBuffer.allocateDirect(a_TriBandColor.length * 4);
        byteBuffer8.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer8 = byteBuffer8.asFloatBuffer();
        positionBuffer8.put(a_TriBandColor);
        positionBuffer8.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, a_TriBandColor.length * 4, positionBuffer8, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        //vao For A
        GLES32.glGenVertexArrays(1, vao_A,0);
        GLES32.glBindVertexArray(vao_A[0]);
        GLES32.glGenBuffers(1, vbo_A_Position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_A_Position[0]);
        ByteBuffer byteBuffer9 = ByteBuffer.allocateDirect(aVertices.length * 4);
        byteBuffer9.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer9 = byteBuffer9.asFloatBuffer();
        positionBuffer9.put(aVertices);
        positionBuffer9.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, aVertices.length * 4, positionBuffer9, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glGenBuffers(1, vbo_A_Color,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_A_Color[0]);
        ByteBuffer byteBuffer10 = ByteBuffer.allocateDirect(aColor.length * 4);
        byteBuffer10.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer10 = byteBuffer10.asFloatBuffer();
        positionBuffer10.put(aColor);
        positionBuffer10.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, aColor.length * 4, positionBuffer10, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);


        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        //GLES32.glClearDepth(1.0f);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
        System.out.println("RTR: EEEEEEEEEEEEEEE");
    }

    private void Resize_RMB(int iWidth, int iHeight)
    {

        if (iHeight == 0)
		{
			iHeight = 1;
		}

		GLES32.glViewport(0, 0, iWidth, iHeight);
        Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)iWidth / (float)iHeight, 0.1f, 100.0f);
    }

    private void Display_RMB()
    {
        float[] dColor = new float[24];
        
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject);

        float[] translationMatrix = new float[16];
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];
        //For I Draw
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, nig_X, 0.0f, -5.0f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);

        GLES32.glLineWidth(20.0f);
        GLES32.glBindVertexArray(vao_I[0]);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
        GLES32.glBindVertexArray(0);


        //For N Draw
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, -0.6f, pos_Y, -5.0f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);

        //GLES32.glLineWidth(30.0f);
        GLES32.glBindVertexArray(vao_N[0]);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 6);
        GLES32.glBindVertexArray(0);


        //For D Draw
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, 0.64f, 0.0f, -5.0f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);

        //GLES32.glLineWidth(30.0f);
        GLES32.glBindVertexArray(vao_D[0]);

        dColor[0] = one;
        dColor[1] = pointFour;
        dColor[2] = 0.0f;
        dColor[3] = 0.0f;
        dColor[4] = one;
        dColor[5] = 0.0f;
        dColor[6] = one;
        dColor[7] = pointFour;
        dColor[8] = 0.0f;
        dColor[9] = one;
        dColor[10] = pointFour;
        dColor[11] = 0.0f;
        dColor[12] = 0.0f;
        dColor[13] = one;
        dColor[14] = 0.0f;
        dColor[15] = 0.0f;
        dColor[16] = one;
        dColor[17] = 0.0f;
        dColor[18] = one;
        dColor[19] = pointFour;
        dColor[20] = 0.0f;
        dColor[21] = 0.0f;
        dColor[22] = one;
        dColor[23] = 0.0f;

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_D_Color[0]);
        ByteBuffer byteBuffer6 = ByteBuffer.allocateDirect(dColor.length * 4);
        byteBuffer6.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer6 = byteBuffer6.asFloatBuffer();
        positionBuffer6.put(dColor);
        positionBuffer6.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, dColor.length*4,  positionBuffer6, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);

        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 8);
        GLES32.glBindVertexArray(0);


        //For Second I Draw
        //For D Draw
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, 1.75f, neg_Y, -5.0f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);

       // GLES32.glLineWidth(30.0f);
        GLES32.glBindVertexArray(vao_I[0]);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
        GLES32.glBindVertexArray(0);


        //For A_TriBand Draw
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, 1.8f, 0.0f, -5.0f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);

        //GLES32.glLineWidth(10.0f);
        GLES32.glBindVertexArray(vao_A_TriBand[0]);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 6);
        GLES32.glBindVertexArray(0);


        //For A Draw
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, pos_X, 0.0f, -5.0f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);

        //GLES32.glLineWidth(100.0f);
        GLES32.glBindVertexArray(vao_A[0]);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 6);
        GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);
        requestRender();   //SwapBuffers();
    }

    private void  Update_RMB()
    {
        nig_X = nig_X + 0.0024f;
        if (nig_X >= -0.9f)
        {
            nig_X = -0.9f;
            AFlag = false;
        }

        if (AFlag == false)
        {
            pos_X = pos_X - 0.0024f;
            if (pos_X <= 1.85f)
            {
                pos_X = 1.85f;
                NFlag = false;
            }
        }
        
        if (NFlag == false)
        {
            pos_Y = pos_Y - 0.0024f;
            if (pos_Y <= 0.0f)
            {
                pos_Y = 0.0f;
                IFlag2 = false;
            }
        }

        if (IFlag2 == false)
        {
            neg_Y = neg_Y + 0.0024f;
            if (neg_Y >= 0.0f)
            {
                neg_Y = 0.0f;
                DFlag = false;
            }
        }
        
        if (DFlag == false)
        {
            one = one + 0.00036f;
            pointFour = pointFour + 0.00036f;
            if (one >= 1.0f)
            {
                one = 1.0f;
                pointFour = 0.5f;
            }
        }
    
    }

    private void Uninitialize_RMB()
    {
        if(vbo_A_Color[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_A_Color,0);
            vbo_A_Color[0] = 0;
        }
        if(vbo_A_Position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_A_Position,0);
            vbo_A_Position[0] = 0;
        }
        if(vao_A[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_A,0);
            vao_A[0] = 0;
        }

        
        if(vbo_A_TriBand_Color[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_A_TriBand_Color,0);
            vbo_A_TriBand_Color[0] = 0;
        }
        if(vbo_A_TriBand_Position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_A_TriBand_Position,0);
            vbo_A_TriBand_Position[0] = 0;
        }
        if(vao_A_TriBand[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_A_TriBand,0);
            vao_A_TriBand[0] = 0;
        }

        if(vbo_D_Color[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_D_Color,0);
            vbo_D_Color[0] = 0;
        }
        if(vbo_D_Position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_D_Position,0);
            vbo_D_Position[0] = 0;
        }
        if(vao_D[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_D,0);
            vao_D[0] = 0;
        }

        if(vbo_N_Color[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_N_Color,0);
            vbo_N_Color[0] = 0;
        }
        if(vbo_N_Position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_N_Position,0);
            vbo_N_Position[0] = 0;
        }
        if(vao_N[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_N,0);
            vao_N[0] = 0;
        }

        if(vbo_I_Color[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_I_Color,0);
            vbo_I_Color[0] = 0;
        }
        if(vbo_I_Position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_I_Position,0);
            vbo_I_Position[0] = 0;
        }
        if(vao_I[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_I,0);
            vao_I[0] = 0;
        }

    	if(shaderProgramObject != 0)
    	{
    		int[] shaderCount = new int[1];
    		int shaderNumber;

    		GLES32.glUseProgram(shaderProgramObject);
    		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount,0);

    		int[] shaders = new int[shaderCount[0]];
            if(shaders[0] != 0)
            {
                GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
                for(shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
                {
                   GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
                   GLES32.glDeleteShader(shaders[shaderNumber]);
                   shaders[shaderNumber] = 0;
                }
            }
            
    	}
    	GLES32.glUseProgram(0);
    	GLES32.glDeleteProgram(shaderProgramObject);
    	shaderProgramObject = 0;
    }
}

