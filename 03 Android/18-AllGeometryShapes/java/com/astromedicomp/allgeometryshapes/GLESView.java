package com.astromedicomp.allgeometryshapes;
//added by me
import android.content.Context;
import android.view.Gravity;
import android.graphics.Color; // for "Color" class
import android.view.MotionEvent; // for "MotionEvent" class
import android.view.GestureDetector; // for "GestureDetector"
import android.view.GestureDetector.OnGestureListener; //for "OnGestureListener"  
import android.view.GestureDetector.OnDoubleTapListener; // for "OnDoubleTapListener" 

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import java.nio.ByteBuffer;		//for OpenGL Buffers
import java.nio.ByteOrder;      //for OpenGL Buffers
import java.nio.FloatBuffer;	//for OpenGL Buffers
import java.lang.Math; 

import android.opengl.Matrix;   //for matrix math

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int[] vao_Graph = new int[1];
    private int[] vbo_Graph = new int[1];
    private int[] vbo_Graph_Color = new int[1];

    private int[] vao_Shapes = new int[1];
    private int[] vbo_Shapes = new int[1];
    private int[] vbo_Shapes_Color = new int[1];

    private int[] vao_OuterCircle = new int[1];
    private int[] vbo_OuterCircle = new int[1];

    private int[] vao_InnerCircle = new int[1];
    private int[] vbo_InnerCircle = new int[1];

    private int mvpUniform;
    private float[] perspectiveProjectionMatrix = new float[16];  // 4 x 4  Matrix

	public GLESView(Context dwContext)
	{
		super(dwContext);

        context = dwContext;

		setEGLContextClientVersion(3); //version 3.x pahije manun
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(dwContext, this, null, false);  //this means 'handler' i.e who is going to handle
		gestureDetector.setOnDoubleTapListener(this);   // this means 'handler' i.e. who is going to handle
    
	}
	// Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
    	//code
    	int eventaction = event.getAction();
    	if(!gestureDetector.onTouchEvent(event))
    		super.onTouchEvent(event);
    	return(true);
    }

     // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
    	 // Do Not Write Any code Here Because Already Written 'onDoubleTap'
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
    	// Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
    	Uninitialize_RMB();
    	System.exit(0);
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
    	return(true);
    }

    //Impliment GLSurfaceView.Renderer Mathods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig congif)  //WM_CREATE OR Initialize
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR:" +version);
        String version1 = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR:" +version1);
        initialize_RMB();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int Width, int Height)  //WM_SIZE or RESIZE
    {
        Resize_RMB(Width, Height);
    }

    @Override
    public void onDrawFrame(GL10 unused)  //Display
    {
        Display_RMB();
        System.out.println("RTR: onDrawFrame");
    }

    //Our Custome Methods
    private void initialize_RMB()
    {
    	vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
    	final String vertexShaderSourceCode = String.format 
    	(
    		"#version 320 es" +
            "\n" +
            "in vec4 vPosition;" +
            "in vec4 vColor;" +
            "uniform mat4 u_mvp_matrix;" +
            "out vec4 out_Color;" +
            "void main(void)" +
            "{" +
            "gl_Position = u_mvp_matrix * vPosition;" +
            "out_Color = vColor;" +
            "}"

    	);

        //Specifing above source to the vertex shader object
    	GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

        //Compile the Vertex Shader
        GLES32.glCompileShader(vertexShaderObject);

    	int[] iShaderCompileStatus = new int[1];
    	int[] iInfoLogLength = new int[1];
    	String szInfoLog = null;

    	GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
    	if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);

    			System.out.println("RTR: vertexShaderInfoLog" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

        //Define Fragment Shader Object
    	fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        //Write Fragment Shader Code
    	final String fragmentShaderSourceCode = String.format
    	(
    		"#version 320 es" +
			"\n" +
			"precision highp float;" +
            "in vec4 out_Color;" +
			"out vec4 fragColor;" +
			"void main(void)" +
			"{" +
				"fragColor = out_Color;" +
			"}"
    	);

        // Specifing above Source to the fragment Shader Object
    	GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
       
        //Compile the fragment Shader Object
    	GLES32.glCompileShader(fragmentShaderObject);

    	iShaderCompileStatus[0] = 0;
    	iInfoLogLength[0] = 0;
    	szInfoLog = null;

    	GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
    	if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);

    			System.out.println("RTR: FragmentShaderInfoLog" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

    	shaderProgramObject = GLES32.glCreateProgram();
    	GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
    	GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

    	//pre Link Binding to attributes
    	GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_COLOR, "vColor");
    	//Link the Shader Program
    	GLES32.glLinkProgram(shaderProgramObject);

    	int[] iProgramLinkStatus  = new int[1];
    	iInfoLogLength[0] = 0;
    	szInfoLog = null;

    	GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus,0);
    	if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			GLES32.glGetProgramInfoLog(shaderProgramObject);

    			System.out.println("RTR: ProgramLinkStatus" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

    	//Post Linking Retriving Uniform Location
    	mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

        //Array For Graph
    	final float[] graphVertices = new float[]
                            	{
                                	//Vertical Lines
                                    //44
                                    -1.10f, 1.0f, 0.0f,
                                    -1.10f, -1.0f, 0.0f,
                                    -1.05f, 1.0f, 0.0f,
                                    -1.05f, -1.0f, 0.0f,
                                    -1.0f, 1.0f, 0.0f,
                                    -1.0f, -1.0f, 0.0f,
                                    -0.95f, 1.0f, 0.0f,
                                    -0.95f, -1.0f, 0.0f,
                                    -0.90f, 1.0f, 0.0f,
                                    -0.90f, -1.0f, 0.0f,
                                    -0.85f, 1.0f, 0.0f,
                                    -0.85f, -1.0f, 0.0f,
                                    -0.80f, 1.0f, 0.0f,
                                    -0.80f, -1.0f, 0.0f,
                                    -0.75f, 1.0f, 0.0f,
                                    -0.75f, -1.0f, 0.0f,
                                    -0.70f, 1.0f, 0.0f,
                                    -0.70f, -1.0f, 0.0f,
                                    -0.65f, 1.0f, 0.0f,
                                    -0.65f, -1.0f, 0.0f,
                                    -0.60f, 1.0f, 0.0f,
                                    -0.60f, -1.0f, 0.0f,
                                    -0.55f, 1.0f, 0.0f,
                                    -0.55f, -1.0f, 0.0f,
                                    -0.50f, 1.0f, 0.0f,
                                    -0.50f, -1.0f, 0.0f,
                                    -0.45f, 1.0f, 0.0f,
                                    -0.45f, -1.0f, 0.0f,
                                    -0.40f, 1.0f, 0.0f,
                                    -0.40f, -1.0f, 0.0f,
                                    -0.35f, 1.0f, 0.0f,
                                    -0.35f, -1.0f, 0.0f,
                                    -0.30f, 1.0f, 0.0f,
                                    -0.30f, -1.0f, 0.0f,
                                    -0.25f, 1.0f, 0.0f,
                                    -0.25f, -1.0f, 0.0f,
                                    -0.20f, 1.0f, 0.0f,
                                    -0.20f, -1.0f, 0.0f,
                                    -0.15f, 1.0f, 0.0f,
                                    -0.15f, -1.0f, 0.0f,
                                    -0.10f, 1.0f, 0.0f,
                                    -0.10f, -1.0f, 0.0f,
                                    -0.05f, 1.0f, 0.0f,
                                    -0.05f, -1.0f, 0.0f,
                                    //46
                                    0.00f, 1.0f, 0.0f,
                                    0.00f, -1.0f, 0.0f,
                                    0.05f, 1.0f, 0.0f,
                                    0.05f, -1.0f, 0.0f,
                                    0.10f, 1.0f, 0.0f,
                                    0.10f, -1.0f, 0.0f,
                                    0.15f, 1.0f, 0.0f,
                                    0.15f, -1.0f, 0.0f,
                                    0.20f, 1.0f, 0.0f,
                                    0.20f, -1.0f, 0.0f,
                                    0.25f, 1.0f, 0.0f,
                                    0.25f, -1.0f, 0.0f,
                                    0.30f, 1.0f, 0.0f,
                                    0.30f, -1.0f, 0.0f,
                                    0.35f, 1.0f, 0.0f,
                                    0.35f, -1.0f, 0.0f,
                                    0.40f, 1.0f, 0.0f,
                                    0.40f, -1.0f, 0.0f,
                                    0.45f, 1.0f, 0.0f,
                                    0.45f, -1.0f, 0.0f,
                                    0.50f, 1.0f, 0.0f,
                                    0.50f, -1.0f, 0.0f,
                                    0.55f, 1.0f, 0.0f,
                                    0.55f, -1.0f, 0.0f,
                                    0.60f, 1.0f, 0.0f,
                                    0.60f, -1.0f, 0.0f,
                                    0.65f, 1.0f, 0.0f,
                                    0.65f, -1.0f, 0.0f,
                                    0.70f, 1.0f, 0.0f,
                                    0.70f, -1.0f, 0.0f,
                                    0.75f, 1.0f, 0.0f,
                                    0.75f, -1.0f, 0.0f,
                                    0.80f, 1.0f, 0.0f,
                                    0.80f, -1.0f, 0.0f,
                                    0.85f, 1.0f, 0.0f,
                                    0.85f, -1.0f, 0.0f,
                                    0.90f, 1.0f, 0.0f,
                                    0.90f, -1.0f, 0.0f,
                                    0.95f, 1.0f, 0.0f,
                                    0.95f, -1.0f, 0.0f,
                                    1.0f, 1.0f, 0.0f,
                                    1.0f, -1.0f, 0.0f,
                                    1.05f, 1.0f, 0.0f,
                                    1.05f, -1.0f, 0.0f,
                                    1.10f, 1.0f, 0.0f,
                                    1.10f, -1.0f, 0.0f,

                                    //Horizantal Lines  
                                    //24
                                    1.15f, -0.60f, 0.0f,
                                    -1.15f, -0.60f, 0.0f,
                                    1.15f, -0.55f, 0.0f,
                                    -1.15f, -0.55f, 0.0f,
                                    1.15f, -0.50f, 0.0f,
                                    -1.15f, -0.50f, 0.0f,
                                    1.15f, -0.45f, 0.0f,
                                    -1.15f, -0.45f, 0.0f,
                                    1.15f, -0.40f, 0.0f,
                                    -1.15f, -0.40f, 0.0f,
                                    1.15f, -0.35f, 0.0f,
                                    -1.15f, -0.35f, 0.0f,
                                    1.15f, -0.30f, 0.0f,
                                    -1.15f, -0.30f, 0.0f,
                                    1.15f, -0.25f, 0.0f,
                                    -1.15f, -0.25f, 0.0f,
                                    1.15f, -0.20f, 0.0f,
                                    -1.15f, -0.20f, 0.0f,
                                    1.15f, -0.15f, 0.0f,
                                    -1.15f, -0.15f, 0.0f,
                                    1.15f, -0.10f, 0.0f,
                                    -1.15f, -0.10f, 0.0f,
                                    1.15f, -0.05f, 0.0f,
                                    -1.15f, -0.05f, 0.0f,
                                    //26
                                    1.15f, 0.0f, 0.0f,
                                    -1.15f, 0.0f, 0.0f,
                                    1.15f, 0.05f, 0.0f,
                                    -1.15f, 0.05f, 0.0f,
                                    1.15f, 0.10f, 0.0f,
                                    -1.15f, 0.10f, 0.0f,
                                    1.15f, 0.15f, 0.0f,
                                    -1.15f, 0.15f, 0.0f,
                                    1.15f, 0.20f, 0.0f,
                                    -1.15f, 0.20f, 0.0f,
                                    1.15f, 0.25f, 0.0f,
                                    -1.15f, 0.25f, 0.0f,
                                    1.15f, 0.30f, 0.0f,
                                    -1.15f, 0.30f, 0.0f,
                                    1.15f, 0.35f, 0.0f,
                                    -1.15f, 0.35f, 0.0f,
                                    1.15f, 0.40f, 0.0f,
                                    -1.15f, 0.40f, 0.0f,
                                    1.15f, 0.45f, 0.0f,
                                    -1.15f, 0.45f, 0.0f,
                                    1.15f, 0.50f, 0.0f,
                                    -1.15f, 0.50f, 0.0f,
                                    1.15f, 0.55f, 0.0f,
                                    -1.15f, 0.55f, 0.0f,
                                    1.15f, 0.60f, 0.0f,
                                    -1.15f, 0.60f, 0.0f
    	                        };
        //Array For Graph Color
        final  int Color = 140;
        final float[] graphColor = new  float[3 * Color];
        for (int i = 0; i < Color; i++)
        {
            graphColor[3 * i + 0] = 0.0f;
            graphColor[3 * i + 1] = 1.0f;
            graphColor[3 * i + 2] = 0.0f;
        }

        //Array For Rectangle And Triangle Vertices
        final float[] shapesVertices = new float[] 
                                {
                                    //Ractangle
                                    0.97f, 0.7f, 0.0f,
                                    -0.97f, 0.7f, 0.0f,
                                    -0.97f, 0.7f, 0.0f,
                                    -0.97f, -0.7f, 0.0f,
                                    -0.97f, -0.7f, 0.0f,
                                    0.97f, -0.7f, 0.0f,
                                    0.97f, 0.7f, 0.0f,
                                    0.97f, -0.7f, 0.0f,

                                    //Triangle
                                    0.0f, 0.7f, 0.0f,
                                    -0.97f, -0.7f, 0.0f,
                                    -0.97f, -0.7f, 0.0f,
                                    0.97f, -0.7f, 0.0f,
                                    0.97f, -0.7f, 0.0f,
                                    0.0f, 0.7f, 0.0f
                                };
        //Array For Rectangle And Triangle Color
        final int colors = 14;
        final  float[] shapesColor = new  float[3 * colors];
        for (int i = 0; i < colors; i++)
        {
            shapesColor[3 * i + 0] = 1.0f;
            shapesColor[3 * i + 1] = 1.0f;
            shapesColor[3 * i + 2] = 0.0f;
        }

        //Array For OuterCircle Vertices
        final int ipoints = 300;
        final  float[] outerCircleVertices = new  float[3 * ipoints];
        for (int i = 0; i < ipoints; i++)
        {
            float Angle = (2.0f * (float)Math.PI * i) / ipoints;

           outerCircleVertices[3 * i + 0] = (float)Math.cos(Angle) * 1.2f;
           outerCircleVertices[3 * i + 1] = (float)Math.sin(Angle) * 1.2f;
           outerCircleVertices[3 * i + 2] = 0.0f;
        }
         //Array For OuterCircle And InnerCircle Color
        final int circleColors = 300;
        final  float[] shapesColorCircle = new  float[3 * circleColors];
        for (int i = 0; i < circleColors; i++)
        {
            shapesColorCircle[3 * i + 0] = 1.0f;
            shapesColorCircle[3 * i + 1] = 1.0f;
            shapesColorCircle[3 * i + 2] = 0.0f;
        }
        //Array For innerCircle Vertices
        final  float[] innerCircleVertices = new  float[3 * ipoints];
        for (int i = 0; i < ipoints; i++)
        {
            float Angle = (2.0f * (float)Math.PI * i) / ipoints;

            innerCircleVertices[3 * i + 0] = (float)Math.cos(Angle) * 0.507f;
            innerCircleVertices[3 * i + 1] = (float)Math.sin(Angle) * 0.507f;
            innerCircleVertices[3 * i + 2] = 0.0f;
        }


        //vao For Graph
        GLES32.glGenVertexArrays(1, vao_Graph,0);
    	GLES32.glBindVertexArray(vao_Graph[0]);
    	GLES32.glGenBuffers(1, vbo_Graph,0);
    	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Graph[0]);
    	ByteBuffer byteBuffer = ByteBuffer.allocateDirect(graphVertices.length * 4);
    	byteBuffer.order(ByteOrder.nativeOrder());
    	FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
    	positionBuffer.put(graphVertices);
    	positionBuffer.position(0);
    	GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphVertices.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
    	GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
    	GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
    	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glGenBuffers(1, vbo_Graph_Color,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Graph_Color[0]);
        ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(graphColor.length * 4);
        byteBuffer1.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer1 = byteBuffer1.asFloatBuffer();
        positionBuffer1.put(graphColor);
        positionBuffer1.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphColor.length * 4, positionBuffer1, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
    	GLES32.glBindVertexArray(0);

        //vao For InnerCircle
        GLES32.glGenVertexArrays(1, vao_InnerCircle,0);
        GLES32.glBindVertexArray(vao_InnerCircle[0]);
        GLES32.glGenBuffers(1, vbo_InnerCircle,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_InnerCircle[0]);
        ByteBuffer byteBuffer6 = ByteBuffer.allocateDirect(innerCircleVertices.length * 4);
        byteBuffer6.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer6 = byteBuffer6.asFloatBuffer();
        positionBuffer6.put(innerCircleVertices);
        positionBuffer6.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, innerCircleVertices.length * 4, positionBuffer6, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glGenBuffers(1, vbo_Shapes_Color,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Shapes_Color[0]);
        ByteBuffer byteBuffer7 = ByteBuffer.allocateDirect(shapesColorCircle.length * 4);
        byteBuffer7.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer7 = byteBuffer7.asFloatBuffer();
        positionBuffer7.put(shapesColorCircle);
        positionBuffer7.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, shapesColorCircle.length * 4, positionBuffer7, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        //vao For Rectangle And TriAngle
        GLES32.glGenVertexArrays(1, vao_Shapes,0);
        GLES32.glBindVertexArray(vao_Shapes[0]);
        GLES32.glGenBuffers(1, vbo_Shapes,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Shapes[0]);
        ByteBuffer byteBuffer2 = ByteBuffer.allocateDirect(shapesVertices.length * 4);
        byteBuffer2.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer2 = byteBuffer2.asFloatBuffer();
        positionBuffer2.put(shapesVertices);
        positionBuffer2.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, shapesVertices.length * 4, positionBuffer2, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glGenBuffers(1, vbo_Graph_Color,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Graph_Color[0]);
        ByteBuffer byteBuffer3 = ByteBuffer.allocateDirect(shapesColor.length * 4);
        byteBuffer3.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer3 = byteBuffer3.asFloatBuffer();
        positionBuffer3.put(shapesColor);
        positionBuffer3.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, shapesColor.length * 4, positionBuffer3, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        //vao For OuterCircle
        GLES32.glGenVertexArrays(1, vao_OuterCircle,0);
        GLES32.glBindVertexArray(vao_OuterCircle[0]);
        GLES32.glGenBuffers(1, vbo_OuterCircle,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_OuterCircle[0]);
        ByteBuffer byteBuffer4 = ByteBuffer.allocateDirect(outerCircleVertices.length * 4);
        byteBuffer4.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer4 = byteBuffer4.asFloatBuffer();
        positionBuffer4.put(outerCircleVertices);
        positionBuffer4.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, outerCircleVertices.length * 4, positionBuffer4, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glGenBuffers(1, vbo_Shapes_Color,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Shapes_Color[0]);
        ByteBuffer byteBuffer5 = ByteBuffer.allocateDirect(shapesColorCircle.length * 4);
        byteBuffer5.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer5 = byteBuffer5.asFloatBuffer();
        positionBuffer5.put(shapesColorCircle);
        positionBuffer5.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, shapesColorCircle.length * 4, positionBuffer5, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);


        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        //GLES32.glClearDepth(1.0f);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
        System.out.println("RTR: EEEEEEEEEEEEEEE");
    }

    private void Resize_RMB(int iWidth, int iHeight)
    {

        if (iHeight == 0)
		{
			iHeight = 1;
		}

		GLES32.glViewport(0, 0, iWidth, iHeight);
        Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)iWidth / (float)iHeight, 0.1f, 100.0f);
    }

    private void Display_RMB()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        GLES32.glUseProgram(shaderProgramObject);
        float[] translationMatrix = new float[16];
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];
        //For Graph Paper
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, 0.0f, 0.0f, -1.6f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);

        GLES32.glBindVertexArray(vao_Graph[0]);
        GLES32.glLineWidth(1.0f);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 140);

        GLES32.glBindVertexArray(0);

        //For InnerCircle
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, 0.0f, -0.193f, -3.0f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);

        GLES32.glBindVertexArray(vao_InnerCircle[0]);
        GLES32.glLineWidth(5.0f);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 300);
        GLES32.glBindVertexArray(0);

        //Shapes ---> Rectangle And Triangle
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, 0.0f, 0.0f, -3.0f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);

        GLES32.glBindVertexArray(vao_Shapes[0]);
        GLES32.glLineWidth(5.0f);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 14);
        GLES32.glBindVertexArray(0);

        //For OuterCircle Vertices
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, 0.0f, 0.0f, -3.0f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);

        GLES32.glBindVertexArray(vao_OuterCircle[0]);
        GLES32.glLineWidth(5.0f);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 300);
        GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);
        requestRender();   //SwapBuffers();
    }

    private void Uninitialize_RMB()
    {
        if(vbo_Shapes_Color[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Shapes_Color,0);
            vbo_Shapes_Color[0] = 0;
        }
        if(vbo_OuterCircle[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_OuterCircle,0);
            vbo_OuterCircle[0] = 0;
        }
        if(vao_OuterCircle[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_OuterCircle,0);
            vao_OuterCircle[0] = 0;
        }

    	if(vbo_Shapes_Color[0] != 0)
    	{
    		GLES32.glDeleteBuffers(1, vbo_Shapes_Color,0);
    		vbo_Shapes_Color[0] = 0;
    	}
        if(vbo_Shapes[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Graph,0);
            vbo_Shapes[0] = 0;
        }
        if(vao_Shapes[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_Shapes,0);
            vao_Shapes[0] = 0;
        }

        if(vbo_InnerCircle[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_InnerCircle,0);
            vbo_InnerCircle[0] = 0;
        }

        if(vao_InnerCircle[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_InnerCircle,0);
            vao_InnerCircle[0] = 0;
        }

        
        if(vbo_Graph_Color[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Graph_Color,0);
            vbo_Graph_Color[0] = 0;
        }

        if(vbo_Graph[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Graph,0);
            vbo_Graph[0] = 0;
        }
        
        if(vao_Graph[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_Graph,0);
            vao_Graph[0] = 0;
        }


    	if(shaderProgramObject != 0)
    	{
    		int[] shaderCount = new int[1];
    		int shaderNumber;

    		GLES32.glUseProgram(shaderProgramObject);
    		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount,0);

    		int[] shaders = new int[shaderCount[0]];
            if(shaders[0] != 0)
            {
                GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
                for(shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
                {
                   GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
                   GLES32.glDeleteShader(shaders[shaderNumber]);
                   shaders[shaderNumber] = 0;
                }
            }
            
    	}
    	GLES32.glUseProgram(0);
    	GLES32.glDeleteProgram(shaderProgramObject);
    	shaderProgramObject = 0;
    }
}

