package com.astromedicomp.pvpf;
//added by me
import android.content.Context;
import android.view.Gravity;
import android.graphics.Color; // for "Color" class
import android.view.MotionEvent; // for "MotionEvent" class
import android.view.GestureDetector; // for "GestureDetector"
import android.view.GestureDetector.OnGestureListener; //for "OnGestureListener"  
import android.view.GestureDetector.OnDoubleTapListener; // for "OnDoubleTapListener" 

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import java.nio.ByteBuffer;		//for OpenGL Buffers
import java.nio.ByteOrder;      //for OpenGL Buffers
import java.nio.FloatBuffer;	//for OpenGL Buffers
import java.lang.Math; 

import android.opengl.Matrix;   //for matrix math
import java.nio.ShortBuffer;    //For Sphere

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject_PV;
    private int shaderProgramObject;

    private float[] modelMatrix = new float[16];
    private float[] viewMatrix = new float[16];
    private float[] projectionMatrix = new float[16];  // 4 x 4  Matrix

    //For sphere
    private int gNumVertices;
    private int gNumElements;

    private int[] vao_Sphere = new int[1];
    private int[] vbo_Sphere_Position = new int[1];
    private int[] vbo_Sphere_Normal = new int[1];
    private int[] vbo_Sphere_Element = new int[1];

    private float[] lightAmbient = new float[] { 0.0f, 0.0f, 0.0f, 0.0f };  //La
    private float[] lightDiffuse = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };  //Ld
    private float[] lightSpecular = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };  //Ls
    private float[] lightPosition = new float[] { 100.0f, 100.0f, 100.0f, 1.0f };

    private float[] materialAmbient = new float[] { 0.0f, 0.0f, 0.0f, 0.0f }; //Ka
    private float[] materialDiffuse = new float[] { 0.5f, 0.2f, 0.7f, 1.0f };  //Kd
    private float[] materialSpecular = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };  //Ks
    private float[] materialShininess = new float[] { 50.0f }; //128.0f;

    ////For Per Vertex
    private int laUniform_RMB;
    private int ldUniform_RMB;
    private int lsUniform_RMB;

    private int kaUniform_RMB;
    private int kdUniform_RMB;
    private int ksUniform_RMB;

    private int lightPositionUniform_RMB;
    private int lKeyIsPressedUniform_RMB;
    private int materialShininessUniform_RMB;

    private int modelUniform_RMB;
    private int viewUniform_RMB;
    private int projectionUniform_RMB;

    //fFor per Fragment
    private int laUniform;
    private int ldUniform;
    private int lsUniform;

    private int kaUniform;
    private int kdUniform;
    private int ksUniform;

    private int lightPositionUniform;
    private int lKeyIsPressedUniform;
    private int materialShininessUniform;

    private int modelUniform;
    private int viewUniform;
    private int projectionUniform;
    
    boolean gbLight = false;
    boolean fragmentShader;

	public GLESView(Context dwContext)
	{
		super(dwContext);

        context = dwContext;

		setEGLContextClientVersion(3); //version 3.x pahije manun
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(dwContext, this, null, false);  //this means 'handler' i.e who is going to handle
		gestureDetector.setOnDoubleTapListener(this);   // this means 'handler' i.e. who is going to handle
    
	}
	// Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
    	//code
    	int eventaction = event.getAction();
    	if(!gestureDetector.onTouchEvent(event))
    		super.onTouchEvent(event);
    	return(true);
    }

     // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'

        
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {   
        
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
    	// Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
       fragmentShader = !fragmentShader;
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
    	Uninitialize_RMB();
    	System.exit(0);
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        if(gbLight == false)
        {
            gbLight = true;
        }
        else
        {
            gbLight = false;
        }
    	return(true);
    }

    //Impliment GLSurfaceView.Renderer Mathods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig congif)  //WM_CREATE OR Initialize
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR:" +version);
        String version1 = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR:" +version1);
        initialize_RMB();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int Width, int Height)  //WM_SIZE or RESIZE
    {
        Resize_RMB(Width, Height);
    }

    @Override
    public void onDrawFrame(GL10 unused)  //Display
    {
        Display_RMB();
        System.out.println("RTR: onDrawFrame");
    }

    //Our Custome Methods
    private void initialize_RMB()
    {
    	perVertexShaderFunc();
        perFragmentShaderFunc();

        //Array For cubeVertices Vertices
        Sphere sphere = new Sphere();
        float sphere_vertices[] = new float[1146];
        float sphere_normals[] = new float[1146];
        float sphere_textures[] = new float[764];
        short sphere_elements[] = new short[2280];

        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        gNumVertices = sphere.getNumberOfSphereVertices();
        gNumElements = sphere.getNumberOfSphereElements();
       
       //vbo
        GLES32.glGenVertexArrays(1, vao_Sphere,0);
        GLES32.glBindVertexArray(vao_Sphere[0]);

        // position vbo
        GLES32.glGenBuffers(1, vbo_Sphere_Position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Sphere_Position[0]);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_vertices.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        //normal vbo
        GLES32.glGenBuffers(1, vbo_Sphere_Normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Sphere_Normal[0]);

        byteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer = byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_normals.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        //element vbo
        GLES32.glGenBuffers(1, vbo_Sphere_Element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_Sphere_Element[0]);

        byteBuffer = ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer = byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER, sphere_elements.length * 2, elementsBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);


        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        //GLES32.glClearDepth(1.0f);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.setIdentityM(viewMatrix, 0);
        Matrix.setIdentityM(projectionMatrix, 0);
    }

    private void perVertexShaderFunc()
    {
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
        final String vertexShaderSourceCode = String.format 
        (
            "#version 320 es" 
            +"\n" 
            +"in vec4 vertexPosition_RMB;" 
            +"in vec3 vertexNormal_RMB;" 

            +"uniform mat4 u_modelMatrix_RMB;" 
            +"uniform mat4 u_viewMatrix_RMB;" 
            +"uniform mat4 u_projection_matrix_RMB;" 

            +"uniform vec3 u_La_RMB;" 
            +"uniform vec3 u_Ld_RMB;" 
            +"uniform vec3 u_Ls_RMB;" 

            +"uniform vec3 u_Ka_RMB;" 
            +"uniform vec3 u_Kd_RMB;" 
            +"uniform vec3 u_Ks_RMB;" 

            +"uniform float u_materialShininess_RMB;" 
            +"uniform int u_lKeyIsPressed_RMB;" 
            +"uniform vec4 u_Light_Position_RMB;" 

            +"out vec3 phong_AdsLight_RMB;" 

            +"void main(void)" 
            +"{" 
            +   "if(u_lKeyIsPressed_RMB == 1)" 
            +   "{" 
            +       "vec4 eye_coordinate_RMB = u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" 

            +       "vec3 tnorm_RMB = normalize(mat3(u_viewMatrix_RMB * u_modelMatrix_RMB) * vertexNormal_RMB);" 

            +       "vec3 lightdirection_RMB = normalize(vec3(u_Light_Position_RMB - eye_coordinate_RMB));" 

            +       "float tn_dot_lightDir_RMB = max(dot(lightdirection_RMB, tnorm_RMB), 0.0);" 

            +       "vec3 reflectionVector_RMB = reflect(-lightdirection_RMB, tnorm_RMB);" 

            +       "vec3 viewerVector_RMB = normalize(vec3(-eye_coordinate_RMB.xyz));" 

            +       "vec3 ambient_RMB = u_La_RMB * u_Ka_RMB;" 

            +       "vec3 diffuse_RMB = u_Ld_RMB * u_Kd_RMB * tn_dot_lightDir_RMB;" 

            +       "vec3 specular_RMB = u_Ls_RMB * u_Ks_RMB * pow(max(dot(reflectionVector_RMB, viewerVector_RMB), 0.0), u_materialShininess_RMB);" 
                    
            +       "phong_AdsLight_RMB = ambient_RMB + diffuse_RMB + specular_RMB;" 

            +   "}" 
            +   "else" 
            +   "{" 
            +           "phong_AdsLight_RMB = vec3(1.0, 1.0, 1.0);" 
            +   "}" 
            +   "gl_Position = u_projection_matrix_RMB * u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" 
            +"}"

        );

        //Specifing above source to the vertex shader object
        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

        //Compile the Vertex Shader
        GLES32.glCompileShader(vertexShaderObject);

        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);

                System.out.println("RTR: vertexShaderInfoLog" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        //Define Fragment Shader Object
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        //Write Fragment Shader Code
        final String fragmentShaderSourceCode = String.format
        (
            "#version 320 es" 
            +"\n" 
            +"precision highp float;" 

            +"in vec3 phong_AdsLight_RMB;" 
            +"out vec4 fragColor;" 
            +"void main(void)" 
            +"{" 
            +   "fragColor = vec4(phong_AdsLight_RMB, 1.0);" 
            +"}"
        );

        // Specifing above Source to the fragment Shader Object
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
       
        //Compile the fragment Shader Object
        GLES32.glCompileShader(fragmentShaderObject);

        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);

                System.out.println("RTR: FragmentShaderInfoLog" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        shaderProgramObject_PV = GLES32.glCreateProgram();
        GLES32.glAttachShader(shaderProgramObject_PV, vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject_PV, fragmentShaderObject);

        //pre Link Binding to attributes
        GLES32.glBindAttribLocation(shaderProgramObject_PV, GLESMacros.AMC_ATTRIBUTE_POSITION, "vertexPosition_RMB");
        GLES32.glBindAttribLocation(shaderProgramObject_PV, GLESMacros.AMC_ATTRIBUTE_NORMAL, "vertexNormal_RMB");
        //Link the Shader Program
        GLES32.glLinkProgram(shaderProgramObject_PV);

        int[] iProgramLinkStatus  = new int[1];
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetProgramiv(shaderProgramObject_PV, GLES32.GL_LINK_STATUS, iProgramLinkStatus,0);
        if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject_PV, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                GLES32.glGetProgramInfoLog(shaderProgramObject_PV);

                System.out.println("RTR: ProgramLinkStatus" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        //Post Linking Retriving Uniform Location
         modelUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_modelMatrix_RMB");
         viewUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_viewMatrix_RMB");
         projectionUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_projection_matrix_RMB");

         laUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_La_RMB");
         ldUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_Ld_RMB");
         lsUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_Ls_RMB");

         kaUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_Ka_RMB");
         kdUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_Kd_RMB");
         ksUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_Ks_RMB");

        lightPositionUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_Light_Position_RMB");
        lKeyIsPressedUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_lKeyIsPressed_RMB");
        materialShininessUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_materialShininess_RMB");
    }

    private void perFragmentShaderFunc()
    {
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
        final String vertexShaderSourceCode = String.format 
        (
            "#version 320 es" 
            +"\n" 
            +"in vec4 vertexPosition;" 
            +"in vec3 vertexNormal;" 

            +"uniform mat4 u_modelMatrix;" 
            +"uniform mat4 u_viewMatrix;" 
            +"uniform mat4 u_projection_matrix;" 

            +"uniform mediump int u_lKeyIsPressed;" 
            +"uniform vec4 u_Light_Position;" 

            +"out vec3 lightdirection;" 
            +"out vec3 tnorm;"
            +"out vec3 viewerVector;"

            +"void main(void)" 
            +"{" 
            +   "if(u_lKeyIsPressed == 1)" 
            +   "{" 
            +       "vec4 eye_coordinate = u_viewMatrix * u_modelMatrix * vertexPosition;" 

            +       "tnorm = mat3(u_viewMatrix * u_modelMatrix) * vertexNormal;" 

            +       "lightdirection = vec3(u_Light_Position) - eye_coordinate.xyz;" 

            +       "viewerVector = -eye_coordinate.xyz;" 
            +   "}"

            +   "gl_Position = u_projection_matrix * u_viewMatrix * u_modelMatrix * vertexPosition;" 
            +"}"

        );

        //Specifing above source to the vertex shader object
        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

        //Compile the Vertex Shader
        GLES32.glCompileShader(vertexShaderObject);

        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
        System.out.println("RTR:- iShaderCompileStatus " + iShaderCompileStatus[0]);
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);

                System.out.println("RTR: vertexShaderInfoLog" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        //Define Fragment Shader Object
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        //Write Fragment Shader Code
        final String fragmentShaderSourceCode = String.format
        (
            "#version 320 es" 
            +"\n" 
            +"precision highp float;" 

            +"in vec3 lightdirection;" 
            +"in vec3 tnorm;"
            +"in vec3 viewerVector;"

            +"uniform vec3 u_La;" 
            +"uniform vec3 u_Ld;" 
            +"uniform vec3 u_Ls;" 

            +"uniform vec3 u_Ka;" 
            +"uniform vec3 u_Kd;" 
            +"uniform vec3 u_Ks;" 

            +"uniform float u_materialShininess;"  
            +"uniform int u_lKeyIsPressed;" 

            +"out vec4 fragColor;" 

            +"void main(void)" 
            +"{" 

            +   "vec3 phong_AdsLight;"
            
            +   "if(u_lKeyIsPressed == 1)" 
            +   "{"
            +       "vec3 normalize_tnorm = normalize(tnorm);"

            +       "vec3 normalize_lightdirection = normalize(lightdirection);"

            +       "vec3 normalize_viewerVector = normalize(viewerVector);"

            +       "float tn_dot_lightDir = max(dot(normalize_tnorm, normalize_lightdirection), 0.0);" 

            +       "vec3 reflectionVector = reflect(-normalize_lightdirection, normalize_tnorm);" 

            +       "vec3 ambient = u_La * u_Ka;" 

            +       "vec3 diffuse = u_Ld * u_Kd * tn_dot_lightDir;" 

            +       "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflectionVector, normalize_viewerVector), 0.0), u_materialShininess);" 
                    
            +       "phong_AdsLight = ambient + diffuse + specular;" 

            +   "}" 
            +   "else" 
            +   "{" 
            +           "phong_AdsLight = vec3(1.0, 1.0, 1.0);" 
            +   "}" 
            +   "fragColor = vec4(phong_AdsLight, 1.0);" 
            +"}"
        );

        // Specifing above Source to the fragment Shader Object
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
       
        //Compile the fragment Shader Object
        GLES32.glCompileShader(fragmentShaderObject);

        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);

                System.out.println("RTR: FragmentShaderInfoLog" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        shaderProgramObject = GLES32.glCreateProgram();
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        //pre Link Binding to attributes
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vertexPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_NORMAL, "vertexNormal");
        //Link the Shader Program
        GLES32.glLinkProgram(shaderProgramObject);

        int[] iProgramLinkStatus  = new int[1];
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus,0);
        if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                GLES32.glGetProgramInfoLog(shaderProgramObject);

                System.out.println("RTR: ProgramLinkStatus" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        //Post Linking Retriving Uniform Location
         modelUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_modelMatrix");
         viewUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
         projectionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

         laUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_La");
         ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld");
         lsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls");

         kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ka");
         kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
         ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ks");

        lightPositionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Light_Position");
        lKeyIsPressedUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");
        materialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_materialShininess");
    }

    private void Resize_RMB(int iWidth, int iHeight)
    {

        if (iHeight == 0)
		{
			iHeight = 1;
		}

		GLES32.glViewport(0, 0, iWidth, iHeight);
        Matrix.perspectiveM(projectionMatrix, 0, 45.0f, (float)iWidth / (float)iHeight, 0.1f, 100.0f);
    }

    private void Display_RMB()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        if(fragmentShader == false)
        {
            GLES32.glUseProgram(shaderProgramObject_PV);
            if (gbLight == true)
            {
                GLES32.glUniform1i(lKeyIsPressedUniform_RMB, 1);

                GLES32.glUniform3fv(laUniform_RMB, 1, lightAmbient,0); //White Light
                GLES32.glUniform3fv(ldUniform_RMB, 1, lightDiffuse,0);
                GLES32.glUniform3fv(lsUniform_RMB, 1, lightSpecular,0);

                GLES32.glUniform3fv(kaUniform_RMB, 1, materialAmbient,0); //Gray Material
                GLES32.glUniform3fv(kdUniform_RMB, 1, materialDiffuse,0);
                GLES32.glUniform3fv(ksUniform_RMB, 1, materialSpecular,0);

                GLES32.glUniform4fv(lightPositionUniform_RMB, 1, lightPosition,0);
                GLES32.glUniform1fv(materialShininessUniform_RMB, 1, materialShininess,0);
            }
            else
            {
                GLES32.glUniform1i(lKeyIsPressedUniform_RMB, 0);
            }
        }
        else 
        {
            GLES32.glUseProgram(shaderProgramObject);

            if (gbLight == true)
            {
                GLES32.glUniform1i(lKeyIsPressedUniform, 1);

                GLES32.glUniform3fv(laUniform, 1, lightAmbient,0); //White Light
                GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
                GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

                GLES32.glUniform3fv(kaUniform, 1, materialAmbient,0); //Gray Material
                GLES32.glUniform3fv(kdUniform, 1, materialDiffuse,0);
                GLES32.glUniform3fv(ksUniform, 1, materialSpecular,0);

                GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition,0);
                GLES32.glUniform1fv(materialShininessUniform, 1, materialShininess,0);
            }
            else
            {
                GLES32.glUniform1i(lKeyIsPressedUniform, 0);
            }
        }
        
        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.setIdentityM(viewMatrix, 0);

        Matrix.translateM(modelMatrix,0, 0.0f, 0.0f, -1.8f);

        GLES32.glUniformMatrix4fv(modelUniform_RMB, 1, false, modelMatrix,0);
		GLES32.glUniformMatrix4fv(viewUniform_RMB, 1, false, viewMatrix,0);
		GLES32.glUniformMatrix4fv(projectionUniform_RMB, 1, false, projectionMatrix,0);

        GLES32.glUniformMatrix4fv(modelUniform, 1, false, modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewUniform, 1, false, viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionUniform, 1, false, projectionMatrix,0);

        //bind vao
        GLES32.glBindVertexArray(vao_Sphere[0]);

        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_Sphere_Element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, gNumElements, GLES32.GL_UNSIGNED_SHORT, 0);

        // unbind vao
        GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);
        requestRender();   //SwapBuffers();
    }

    private void Uninitialize_RMB()
    {
        if(vbo_Sphere_Normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Sphere_Normal,0);
            vbo_Sphere_Normal[0] = 0;
        }
        if(vbo_Sphere_Position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Sphere_Position,0);
            vbo_Sphere_Position[0] = 0;
        }
        if(vbo_Sphere_Element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Sphere_Element,0);
            vbo_Sphere_Element[0] = 0;
        }
        if(vao_Sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_Sphere,0);
            vao_Sphere[0] = 0;
        }


    	if(shaderProgramObject_PV != 0)
    	{
    		int[] shaderCount = new int[1];
    		int shaderNumber;

    		GLES32.glUseProgram(shaderProgramObject_PV);
    		GLES32.glGetProgramiv(shaderProgramObject_PV, GLES32.GL_ATTACHED_SHADERS, shaderCount,0);

    		int[] shaders = new int[shaderCount[0]];
            if(shaders[0] != 0)
            {
                GLES32.glGetAttachedShaders(shaderProgramObject_PV, shaderCount[0], shaderCount,0, shaders,0);
                for(shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
                {
                   GLES32.glDetachShader(shaderProgramObject_PV, shaders[shaderNumber]);
                   GLES32.glDeleteShader(shaders[shaderNumber]);
                   shaders[shaderNumber] = 0;
                }
            }
            
    	}
    	GLES32.glUseProgram(0);
    	GLES32.glDeleteProgram(shaderProgramObject_PV);
    	shaderProgramObject_PV = 0;

        if(shaderProgramObject != 0)
        {
            int[] shaderCount = new int[1];
            int shaderNumber;

            GLES32.glUseProgram(shaderProgramObject);
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount,0);

            int[] shaders = new int[shaderCount[0]];
            if(shaders[0] != 0)
            {
                GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
                for(shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
                {
                   GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
                   GLES32.glDeleteShader(shaders[shaderNumber]);
                   shaders[shaderNumber] = 0;
                }
            }
            
        }
        GLES32.glUseProgram(0);
        GLES32.glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
    }
}



