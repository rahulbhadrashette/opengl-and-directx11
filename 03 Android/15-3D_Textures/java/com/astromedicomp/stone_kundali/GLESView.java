package com.astromedicomp.stone_kundali;
//added by me
import android.content.Context;
import android.view.Gravity;
import android.view.MotionEvent; // for "MotionEvent" class
import android.view.GestureDetector; // for "GestureDetector"
import android.view.GestureDetector.OnGestureListener; //for "OnGestureListener"  
import android.view.GestureDetector.OnDoubleTapListener; // for "OnDoubleTapListener" 

//for OpenGL
import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
//for Texture
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;

import java.nio.ByteBuffer;		//for OpenGL Buffers
import java.nio.ByteOrder;      //for OpenGL Buffers
import java.nio.FloatBuffer;	//for OpenGL Buffers

import android.opengl.Matrix;   //for matrix math

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int[] vao_Pyramid = new int[1];
    private int[] vao_Cube = new int[1];
    private int[] vbo_Position_Pyramid = new int[1];
    private int[] vbo_Position_Cube = new int[1];
    private int[] vbo_Texture_Pyramid = new int[1];
    private int[] vbo_Texture_Cube = new int[1];

    private int[] textureStone = new int[1];
    private int[] textureKundali = new int[1];
    private float[] perspectiveProjectionMatrix = new float[16];  // 4 x 4  Matrix
    private int mvpUniform;
    private int samplerUniform;
    float angle = 0.0f;

	public GLESView(Context dwContext)
	{
		super(dwContext);

        context = dwContext;

		setEGLContextClientVersion(3); //version 3.x pahije manun
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(dwContext, this, null, false);  //this means 'handler' i.e who is going to handle
		gestureDetector.setOnDoubleTapListener(this);   // this means 'handler' i.e. who is going to handle
    
	}
	// Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
    	//code
    	int eventaction = event.getAction();
    	if(!gestureDetector.onTouchEvent(event))
    		super.onTouchEvent(event);
    	return(true);
    }

     // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
    	 // Do Not Write Any code Here Because Already Written 'onDoubleTap'
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
    	// Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
    	Uninitialize_RMB();
    	System.exit(0);
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
    	return(true);
    }

    //Impliment GLSurfaceView.Renderer Mathods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig congif)  //WM_CREATE OR Initialize
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR:" +version);
        String version1 = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR:" +version1);
        initialize_RMB();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int Width, int Height)  //WM_SIZE or RESIZE
    {
        Resize_RMB(Width, Height);
    }

    @Override
    public void onDrawFrame(GL10 unused)  //Display
    {
        Display_RMB();
        Update_RMB();
        System.out.println("RTR: onDrawFrame");
    }

    //Our Custome Methods
    private void initialize_RMB()
    {
    	vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
    	final String vertexShaderSourceCode = String.format 
    	(
    		"#version 320 es" +
			"\n" +
            "in vec4 vPosition;" +
            "in vec2 vTexCoord;" +
            "uniform mat4 u_mvp_matrix;" +
            "out vec2 out_TexCoord;" +
            "void main(void)" +
            "{" +
                "gl_Position = u_mvp_matrix * vPosition;" +
                "out_TexCoord = vTexCoord;" +
            "}"
    	);

        //Specifing above source to the vertex shader object
    	GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

        //Compile the Vertex Shader
        GLES32.glCompileShader(vertexShaderObject);

    	int[] iShaderCompileStatus = new int[1];
    	int[] iInfoLogLength = new int[1];
    	String szInfoLog = null;

    	GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
    	if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);

    			System.out.println("RTR: vertexShaderInfoLog" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

        //Define Fragment Shader Object
    	fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        //Write Fragment Shader Code
    	final String fragmentShaderSourceCode = String.format
    	(
    		"#version 320 es" +
			"\n" +
			"precision highp float;" +
            "in vec2 out_TexCoord;" +
            "uniform sampler2D u_Sampler;" +
            "out vec4 fragColor;" +
            "void main(void)" +
            "{" +
               "fragColor = texture(u_Sampler, out_TexCoord);" +
            "}"
    	);

        // Specifing above Source to the fragment Shader Object
    	GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
       
        //Compile the fragment Shader Object
    	GLES32.glCompileShader(fragmentShaderObject);

    	iShaderCompileStatus[0] = 0;
    	iInfoLogLength[0] = 0;
    	szInfoLog = null;

    	GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
    	if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);

    			System.out.println("RTR: FragmentShaderInfoLog" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

    	shaderProgramObject = GLES32.glCreateProgram();

        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

    	//pre Link Binding to attributes
    	GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");
    	//Link the Shader Program
    	GLES32.glLinkProgram(shaderProgramObject);

    	int[] iProgramLinkStatus  = new int[1];
    	iInfoLogLength[0] = 0;
    	szInfoLog = null;

    	GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus,0);
    	if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			GLES32.glGetProgramInfoLog(shaderProgramObject);

    			System.out.println("RTR: ProgramLinkStatus" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

    	//Post Linking Retriving Uniform Location
    	mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        samplerUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Sampler");

        final float[] pyramidVertices = new float[]
                            {
                                //Front Face
                                0.0f, 1.0f, 0.0f,
                                -1.0f, -1.0f, 1.0f,
                                1.0f, -1.0f, 1.0f,

                                //Right Face
                                0.0f, 1.0f, 0.0f,
                                1.0f, -1.0f, 1.0f,
                                1.0f, -1.0f, -1.0f,

                                //Back Face
                                0.0f, 1.0f, 0.0f,
                                1.0f, -1.0f, -1.0f,
                                -1.0f, -1.0f, -1.0f,

                                //Left Face
                                0.0f, 1.0f, 0.0f,
                                -1.0f, -1.0f, -1.0f, 
                                -1.0f, -1.0f, 1.0f
                            };

    	final float[] pyramidTexCoord = new float[]
    	                   {
                                //Front Face
                                0.5f, 1.0f,
                                0.0f, 0.0f,
                                1.0f, 0.0f,

                                //Right Face
                                0.5f, 1.0f,
                                1.0f, 0.0f,
                                0.0f, 0.0f,

                                //Back Face
                                0.5f, 1.0f,
                                1.0f, 0.0f,
                                0.0f, 0.0f,

                                //Left Face
                                0.0f, 1.0f, 
                                0.0f, 0.0f,
                                1.0f, 0.0f
                            };

        final float[] cubeVertices = new float[]
                            {
                                //TOP FACE
                                1.0f, 1.0f, -1.0f,
                                -1.0f, 1.0f, -1.0f,
                                -1.0f, 1.0f, 1.0f,
                                1.0f, 1.0f, 1.0f,
                                //BOTTOM FACE
                                1.0f, -1.0f, -1.0f,
                                -1.0f, -1.0f, -1.0f,
                                -1.0f, -1.0f, 1.0f,
                                1.0f, -1.0f, 1.0f,
                                //FRONT FACE
                                1.0f, 1.0f, 1.0f,
                                -1.0f, 1.0f, 1.0f,
                                -1.0f, -1.0f, 1.0f,
                                1.0f, -1.0f, 1.0f,
                                //BACK FACE
                                1.0f, 1.0f, -1.0f,
                                -1.0f, 1.0f, -1.0f,
                                -1.0f, -1.0f, -1.0f,
                                1.0f, -1.0f, -1.0f,
                                //RIGHT FACE
                                1.0f, 1.0f, -1.0f,
                                1.0f, 1.0f, 1.0f,
                                1.0f, -1.0f, 1.0f,
                                1.0f, -1.0f, -1.0f,
                                //LEFT FACE
                                -1.0f, 1.0f, -1.0f,
                                -1.0f, 1.0f, 1.0f,
                                -1.0f, -1.0f, 1.0f,
                                -1.0f, -1.0f, -1.0f
                            };

        final float[] cubeTexCoord = new float[]
                            {
                                //Top Face
                                0.0f, 1.0f,
                                0.0f, 0.0f,
                                1.0f, 0.0f, 
                                1.0f, 1.0f,

                                //Top Face
                                0.0f, 1.0f,
                                0.0f, 0.0f,
                                1.0f, 0.0f,
                                1.0f, 1.0f,
                                //Top Face
                                0.0f, 1.0f,
                                0.0f, 0.0f,
                                1.0f, 0.0f,
                                1.0f, 1.0f,
                                //Top Face
                                0.0f, 1.0f,
                                0.0f, 0.0f,
                                1.0f, 0.0f,
                                1.0f, 1.0f,
                                //Top Face
                                0.0f, 1.0f,
                                0.0f, 0.0f,
                                1.0f, 0.0f,
                                1.0f, 1.0f,
                                //Top Face
                                0.0f, 1.0f,
                                0.0f, 0.0f,
                                1.0f, 0.0f,
                                1.0f, 1.0f
                            };


        //For Triangle
        GLES32.glGenVertexArrays(1, vao_Pyramid,0);
    	GLES32.glBindVertexArray(vao_Pyramid[0]);
    	GLES32.glGenBuffers(1, vbo_Position_Pyramid,0);
    	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Position_Pyramid[0]);
       
    	ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(pyramidVertices.length * 4);
    	byteBuffer1.order(ByteOrder.nativeOrder());
    	FloatBuffer positionBuffer1 = byteBuffer1.asFloatBuffer();
    	positionBuffer1.put(pyramidVertices);
    	positionBuffer1.position(0);

    	GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidVertices.length * 4, positionBuffer1, GLES32.GL_STATIC_DRAW);
    	GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
    	GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
    	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        //For Triangle_TexCoord
        GLES32.glGenBuffers(1, vbo_Texture_Pyramid,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Texture_Pyramid[0]);

        ByteBuffer byteBuffer2 = ByteBuffer.allocateDirect(pyramidTexCoord.length * 4);
        byteBuffer2.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer2 = byteBuffer2.asFloatBuffer();
        positionBuffer2.put(pyramidTexCoord);
        positionBuffer2.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidTexCoord.length * 4, positionBuffer2, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, 2, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        //for Rectangle
        GLES32.glGenVertexArrays(1, vao_Cube,0);
        GLES32.glBindVertexArray(vao_Cube[0]);
        GLES32.glGenBuffers(1, vbo_Position_Cube,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Position_Cube[0]);

        ByteBuffer byteBuffer3 = ByteBuffer.allocateDirect(cubeVertices.length * 4);
        byteBuffer3.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer3 = byteBuffer3.asFloatBuffer();
        positionBuffer3.put(cubeVertices);
        positionBuffer3.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeVertices.length * 4, positionBuffer3, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        //For Rectangle_Color
        GLES32.glGenBuffers(1, vbo_Texture_Cube,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Texture_Cube[0]);

        ByteBuffer byteBuffer4 = ByteBuffer.allocateDirect(cubeTexCoord.length * 4);
        byteBuffer4.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer4 = byteBuffer4.asFloatBuffer();
        positionBuffer4.put(cubeTexCoord);
        positionBuffer4.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeTexCoord.length * 4, positionBuffer4, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, 2, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
    	GLES32.glBindVertexArray(0);

        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLES32.glDisable(GLES32.GL_CULL_FACE);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        GLES32.glEnable(GLES32.GL_TEXTURE_2D);
        textureStone[0] = LoadTexture(R.raw.stone);
        textureKundali[0] = LoadTexture(R.raw.vijay_kundali_horz_inverted);
        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
    }

     private int LoadTexture(int imageFileResourceID)
    {
        int[] texture = new int[1];

        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inScaled = false;

        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), imageFileResourceID, options);

        GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 4);
        GLES32.glGenTextures(1, texture,0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
        GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0); //GLUtils madhya ahe ha call
        GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
        return(texture[0]);
    }

    private void Resize_RMB(int iWidth, int iHeight)
    {

        if (iHeight == 0)
		{
			iHeight = 1;
		}

		GLES32.glViewport(0, 0, iWidth, iHeight);

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)iWidth / (float)iHeight, 0.1f, 100.0f);
    }
   
    private void Display_RMB()
    {
        
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        GLES32.glUseProgram(shaderProgramObject);

        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];

        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, -1.6f, 0.0f, -6.0f);
        Matrix.rotateM(modelViewMatrix,0, angle, 0.0f, 1.0f, 0.0f);
    
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);

        GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, textureStone[0]);
        GLES32.glUniform1i(samplerUniform, 0);

        GLES32.glBindVertexArray(vao_Pyramid[0]);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);
        GLES32.glBindVertexArray(0);
        
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, 1.6f, 0.0f, -6.0f);
        Matrix.scaleM(modelViewMatrix,0, 0.75f, 0.75f, 0.75f);
        Matrix.rotateM(modelViewMatrix,0, angle, 1.0f, 0.0f, 0.0f);
        Matrix.rotateM(modelViewMatrix,0, angle, 0.0f, 1.0f, 0.0f);
        Matrix.rotateM(modelViewMatrix,0, angle, 0.0f, 0.0f, 1.0f);

        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);

        GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, textureKundali[0]);
        GLES32.glUniform1i(samplerUniform, 0);

        GLES32.glBindVertexArray(vao_Cube[0]);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);
        GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);
        requestRender();   //SwapBuffers();
    }

    private void Update_RMB()
    {
        angle = angle + 1.5f;
        if(angle > 360.0f)
        {
            angle = 0.0f;
        }
    }

    private void Uninitialize_RMB()
    {
    	if(vbo_Position_Pyramid[0] != 0)
    	{
    		GLES32.glDeleteBuffers(1, vbo_Position_Pyramid,0);
    		vbo_Position_Pyramid[0] = 0;
    	}

        if(vbo_Position_Cube[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Position_Cube,0);
            vbo_Position_Cube[0] = 0;
        }

        if (vbo_Texture_Pyramid[0] != 0)
        {
           GLES32.glDeleteBuffers(1, vbo_Texture_Pyramid,0);
           vbo_Texture_Pyramid[0] = 0;
        }

        if (vbo_Texture_Cube[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Texture_Cube,0);
           vbo_Texture_Cube[0] = 0;
        }

    	if(vao_Pyramid[0] != 0)
    	{
    		GLES32.glDeleteVertexArrays(1, vao_Pyramid,0);
    		vao_Pyramid[0] = 0;
    	}

        if(vao_Cube[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_Cube,0);
            vao_Cube[0] = 0;
        }

        if(textureStone[0] != 0)
        {
             GLES32.glDeleteTextures(1, textureStone,0);
             textureStone[0] = 0;
        }

    	if(shaderProgramObject != 0)
    	{
    		int[] shaderCount = new int[1];
    		int shaderNumber;

    		GLES32.glUseProgram(shaderProgramObject);
    		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount,0);

    		int[] shaders = new int[shaderCount[0]];
            if(shaders[0] != 0)
            {
                GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
                for(shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
                {
                   GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
                   GLES32.glDeleteShader(shaders[shaderNumber]);
                   shaders[shaderNumber] = 0;
                }
            }
            
    	}
    	GLES32.glUseProgram(0);
    	GLES32.glDeleteProgram(shaderProgramObject);
    	shaderProgramObject = 0;
    }
}

