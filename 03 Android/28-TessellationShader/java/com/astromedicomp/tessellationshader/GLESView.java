package com.astromedicomp.tessellationshader;
//added by me
import android.content.Context;
import android.view.Gravity;
import android.graphics.Color; // for "Color" class
import android.view.MotionEvent; // for "MotionEvent" class
import android.view.GestureDetector; // for "GestureDetector"
import android.view.GestureDetector.OnGestureListener; //for "OnGestureListener"  
import android.view.GestureDetector.OnDoubleTapListener; // for "OnDoubleTapListener" 

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import java.nio.ByteBuffer;		//for OpenGL Buffers
import java.nio.ByteOrder;      //for OpenGL Buffers
import java.nio.FloatBuffer;	//for OpenGL Buffers

import android.opengl.Matrix;   //for matrix math

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int tessellationControlShaderObject;
    private int tessellationEvaluationShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int[] vao = new int[1];
    private int[] vbo = new int[1];

    private int mvpUniform;

    private int numberOfSegmentsUniform;
    private int numberOfStripsUniform;
    private int lineColorUniform;
    private int numberOfLineSegments;

    private float[] perspectiveProjectionMatrix = new float[16];  // 4 x 4  Matrix

    private float[] lineColorArr = new float[] { 1.0f, 1.0f, 0.0f, 1.0f};

	public GLESView(Context dwContext)
	{
		super(dwContext);

        context = dwContext;

		setEGLContextClientVersion(3); //version 3.x pahije manun
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(dwContext, this, null, false);  //this means 'handler' i.e who is going to handle
		gestureDetector.setOnDoubleTapListener(this);   // this means 'handler' i.e. who is going to handle
    
	}
	// Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
    	//code
    	int eventaction = event.getAction();
    	if(!gestureDetector.onTouchEvent(event))
    		super.onTouchEvent(event);
    	return(true);
    }

     // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        numberOfLineSegments--;
        if (numberOfLineSegments <= 0)
        {
            numberOfLineSegments = 1; //reset
        }
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
    	 // Do Not Write Any code Here Because Already Written 'onDoubleTap'
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        numberOfLineSegments++;
        if (numberOfLineSegments >= 50)
        {
            numberOfLineSegments = 50; //reset
        }
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
    	// Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
    	Uninitialize_RMB();
    	System.exit(0);
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
    	return(true);
    }

    //Impliment GLSurfaceView.Renderer Mathods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig congif)  //WM_CREATE OR Initialize
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR:" +version);
        String version1 = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR:" +version1);
        initialize_RMB();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int Width, int Height)  //WM_SIZE or RESIZE
    {
        Resize_RMB(Width, Height);
    }

    @Override
    public void onDrawFrame(GL10 unused)  //Display
    {
        Display_RMB();
        System.out.println("RTR: onDrawFrame");
    }

    //Our Custome Methods
    private void initialize_RMB()
    {
    	vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
    	final String vertexShaderSourceCode = String.format 
    	(
    		"#version 320 es" +
			"\n" +
			"in vec2 vPosition;" +
			"void main(void)" +
			"{" +
				"gl_Position = vec4(vPosition, 0.0, 1.0);" +
			"}"

    	);

        //Specifing above source to the vertex shader object
    	GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

        //Compile the Vertex Shader
        GLES32.glCompileShader(vertexShaderObject);

    	int[] iShaderCompileStatus = new int[1];
    	int[] iInfoLogLength = new int[1];
    	String szInfoLog = null;

    	GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
    	if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);

    			System.out.println("RTR: vertexShaderInfoLog" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

        //Define Tessellation Control Shader Object
        tessellationControlShaderObject = GLES32.glCreateShader(GLES32.GL_TESS_CONTROL_SHADER);

        //Write Fragment Shader Code
        final String tessellationControlShaderSourceCode = String.format
        (
            "#version 320 es" +
            "\n" +
            "precision highp float;" +
            "layout(vertices=4)out;" +
            "uniform int numberOfSegments;" +
            "uniform int numberOfStrips;" +
            "void main(void)" +
            "{" +
                "gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;" +
                "gl_TessLevelOuter[0] = float(numberOfStrips);" +
                "gl_TessLevelOuter[1] = float(numberOfSegments);" +
            "}"
        );

        // Specifing above Source to the fragment Shader Object
        GLES32.glShaderSource(tessellationControlShaderObject, tessellationControlShaderSourceCode);
       
        //Compile the fragment Shader Object
        GLES32.glCompileShader(tessellationControlShaderObject);

        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetShaderiv(tessellationControlShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(tessellationControlShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(tessellationControlShaderObject);

                System.out.println("RTR: FragmentShaderInfoLog" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

         //Define Geometry Shader Object
        tessellationEvaluationShaderObject = GLES32.glCreateShader(GLES32.GL_TESS_EVALUATION_SHADER);

        //Write Fragment Shader Code
        final String  tessellationEvaluationShaderSourceCode = String.format
        (
            "#version 320 es" +
            "\n" +
            "precision highp float;" +
            "layout(isolines)in;" +
            "uniform mat4 u_mvp_matrix;" +
            "void main(void)" +
            "{" +
                "float u = gl_TessCoord.x;" +
                "vec3 p0 = gl_in[0].gl_Position.xyz;" +
                "vec3 p1 = gl_in[1].gl_Position.xyz;" +
                "vec3 p2 = gl_in[2].gl_Position.xyz;" +
                "vec3 p3 = gl_in[3].gl_Position.xyz;" +
                "float u1 = (1.0 - u);" +
                "float u2 = u * u;" +
                "float b3 = u2 * u;" +
                "float b2 = 3.0 * u2 * u1;" +
                "float b1 = 3.0 * u * u1 * u1;" +
                "float b0 = u1 * u1 * u1;" +
                "vec3 p = p0 * b0 + p1 * b1 + p2 * b2 + p3 * b3;" +
                "gl_Position = u_mvp_matrix * vec4(p, 1.0);" +
            "}"
        );

        // Specifing above Source to the fragment Shader Object
        GLES32.glShaderSource(tessellationEvaluationShaderObject, tessellationEvaluationShaderSourceCode);
       
        //Compile the fragment Shader Object
        GLES32.glCompileShader(tessellationEvaluationShaderObject);

        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetShaderiv(tessellationEvaluationShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(tessellationEvaluationShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(tessellationEvaluationShaderObject);

                System.out.println("RTR: FragmentShaderInfoLog" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        //Define Fragment Shader Object
    	fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        //Write Fragment Shader Code
    	final String fragmentShaderSourceCode = String.format
    	(
    		"#version 320 es" +
			"\n" +
			"precision highp float;" +
            "uniform vec4 lineColor;" +
			"out vec4 fragColor;" +
			"void main(void)" +
			"{" +
				"fragColor = lineColor;" +
			"}"
    	);

        // Specifing above Source to the fragment Shader Object
    	GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
       
        //Compile the fragment Shader Object
    	GLES32.glCompileShader(fragmentShaderObject);

    	iShaderCompileStatus[0] = 0;
    	iInfoLogLength[0] = 0;
    	szInfoLog = null;

    	GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
    	if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);

    			System.out.println("RTR: FragmentShaderInfoLog" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

    	shaderProgramObject = GLES32.glCreateProgram();
    	GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject, tessellationControlShaderObject);
        GLES32.glAttachShader(shaderProgramObject, tessellationEvaluationShaderObject);
    	GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

    	//pre Link Binding to attributes
    	GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
    	//Link the Shader Program
    	GLES32.glLinkProgram(shaderProgramObject);

    	int[] iProgramLinkStatus  = new int[1];
    	iInfoLogLength[0] = 0;
    	szInfoLog = null;

    	GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus,0);
    	if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			GLES32.glGetProgramInfoLog(shaderProgramObject);

    			System.out.println("RTR: ProgramLinkStatus" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

    	//Post Linking Retriving Uniform Location
    	mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        numberOfSegmentsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "numberOfSegments");
        numberOfStripsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "numberOfStrips");
       lineColorUniform = GLES32.glGetUniformLocation(shaderProgramObject, "lineColor");

    	final float[] triangleVertices = new float[]
    	{
    		-1.0f, -1.0f, -0.5f, 1.0f, 0.5f, -1.0f, 1.0f, 1.0f    
    	};

        GLES32.glGenVertexArrays(1, vao,0);
    	GLES32.glBindVertexArray(vao[0]);
    	GLES32.glGenBuffers(1, vbo,0);
    	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo[0]);
       
    	ByteBuffer byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length * 4);
    	byteBuffer.order(ByteOrder.nativeOrder());
    	FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
    	positionBuffer.put(triangleVertices);
    	positionBuffer.position(0);

    	GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleVertices.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
    	GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
    	GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
    	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
    	GLES32.glBindVertexArray(0);

        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        //GLES32.glClearDepth(1.0f);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
        numberOfLineSegments = 1;
    }

    private void Resize_RMB(int iWidth, int iHeight)
    {

        if (iHeight == 0)
		{
			iHeight = 1;
		}

		GLES32.glViewport(0, 0, iWidth, iHeight);

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)iWidth / (float)iHeight, 0.1f, 100.0f);
    }

    private void Display_RMB()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject);

        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];

        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, 0.5f, 0.5f, -2.0f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);
        GLES32.glUniform1i(numberOfSegmentsUniform, numberOfLineSegments);
        GLES32.glUniform1i(numberOfStripsUniform, 1);

        GLES32.glUniform4fv(lineColorUniform, 1, lineColorArr,0);

        GLES32.glBindVertexArray(vao[0]);

        GLES32.glPatchParameteri(GLES32.GL_PATCH_VERTICES, 4);
        GLES32.glDrawArrays(GLES32.GL_PATCHES, 0, 4);

        GLES32.glBindVertexArray(0);
        GLES32.glUseProgram(0);
        requestRender();   //SwapBuffers();
    }

    private void Uninitialize_RMB()
    {
    	if(vbo[0] != 0)
    	{
    		GLES32.glDeleteBuffers(1, vbo,0);
    		vbo[0] = 0;
    	}

    	if(vao[0] != 0)
    	{
    		GLES32.glDeleteVertexArrays(1, vao,0);
    		vao[0] = 0;
    	}

    	if(shaderProgramObject != 0)
    	{
    		int[] shaderCount = new int[1];
    		int shaderNumber;

    		GLES32.glUseProgram(shaderProgramObject);
    		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount,0);

    		int[] shaders = new int[shaderCount[0]];
            if(shaders[0] != 0)
            {
                GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
                for(shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
                {
                   GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
                   GLES32.glDeleteShader(shaders[shaderNumber]);
                   shaders[shaderNumber] = 0;
                }
            }
            
    	}
    	GLES32.glUseProgram(0);
    	GLES32.glDeleteProgram(shaderProgramObject);
    	shaderProgramObject = 0;
    }
}

