package com.astromedicomp.deathlyhallos;
//added by me
import android.content.Context;
import android.view.Gravity;
import android.graphics.Color; // for "Color" class
import android.view.MotionEvent; // for "MotionEvent" class
import android.view.GestureDetector; // for "GestureDetector"
import android.view.GestureDetector.OnGestureListener; //for "OnGestureListener"  
import android.view.GestureDetector.OnDoubleTapListener; // for "OnDoubleTapListener" 

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import java.nio.ByteBuffer;		//for OpenGL Buffers
import java.nio.ByteOrder;      //for OpenGL Buffers
import java.nio.FloatBuffer;	//for OpenGL Buffers
import java.lang.Math; 

import android.opengl.Matrix;   //for matrix math

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int[] vao_Line = new int[1];
    private int[] vbo_Line = new int[1];
    private int[] vbo_Line_Color = new int[1];

    private int[] vao_Triangle = new int[1];
    private int[] vbo_Triangle = new int[1];
    private int[] vbo_Triangle_Color = new int[1];

    private int[] vao_Circle = new int[1];
    private int[] vbo_Circle = new int[1];
    private int[] vbo_Circle_Color = new int[1];

    private int mvpUniform;
    private float[] perspectiveProjectionMatrix = new float[16];  // 4 x 4  Matrix

    float pos_Yl = 2.0f;
    float neg_Xt = -3.1f, neg_Yt = -3.1f;
    float pos_Xc = 2.795f, neg_Yc = -3.1f;
    float angle = 0.0f;
    boolean circle = false;
    boolean triangle = false;
    boolean stop = true;

	public GLESView(Context dwContext)
	{
		super(dwContext);

        context = dwContext;

		setEGLContextClientVersion(3); //version 3.x pahije manun
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(dwContext, this, null, false);  //this means 'handler' i.e who is going to handle
		gestureDetector.setOnDoubleTapListener(this);   // this means 'handler' i.e. who is going to handle
    
	}
	// Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
    	//code
    	int eventaction = event.getAction();
    	if(!gestureDetector.onTouchEvent(event))
    		super.onTouchEvent(event);
    	return(true);
    }

     // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
    	 // Do Not Write Any code Here Because Already Written 'onDoubleTap'
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
    	// Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
    	Uninitialize_RMB();
    	System.exit(0);
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
    	return(true);
    }

    //Impliment GLSurfaceView.Renderer Mathods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig congif)  //WM_CREATE OR Initialize
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR:" +version);
        String version1 = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR:" +version1);
        initialize_RMB();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int Width, int Height)  //WM_SIZE or RESIZE
    {
        Resize_RMB(Width, Height);
    }

    @Override
    public void onDrawFrame(GL10 unused)  //Display
    {
        Update_RMB();
        Display_RMB();
        System.out.println("RTR: onDrawFrame");
    }

    //Our Custome Methods
    private void initialize_RMB()
    {
    	vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
    	final String vertexShaderSourceCode = String.format 
    	(
    		"#version 320 es" +
            "\n" +
            "in vec4 vPosition;" +
            "in vec4 vColor;" +
            "uniform mat4 u_mvp_matrix;" +
            "out vec4 out_Color;" +
            "void main(void)" +
            "{" +
            "gl_Position = u_mvp_matrix * vPosition;" +
            "out_Color = vColor;" +
            "}"

    	);

        //Specifing above source to the vertex shader object
    	GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

        //Compile the Vertex Shader
        GLES32.glCompileShader(vertexShaderObject);

    	int[] iShaderCompileStatus = new int[1];
    	int[] iInfoLogLength = new int[1];
    	String szInfoLog = null;

    	GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
    	if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);

    			System.out.println("RTR: vertexShaderInfoLog" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

        //Define Fragment Shader Object
    	fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        //Write Fragment Shader Code
    	final String fragmentShaderSourceCode = String.format
    	(
    		"#version 320 es" +
			"\n" +
			"precision highp float;" +
            "in vec4 out_Color;" +
			"out vec4 fragColor;" +
			"void main(void)" +
			"{" +
				"fragColor = out_Color;" +
			"}"
    	);

        // Specifing above Source to the fragment Shader Object
    	GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
       
        //Compile the fragment Shader Object
    	GLES32.glCompileShader(fragmentShaderObject);

    	iShaderCompileStatus[0] = 0;
    	iInfoLogLength[0] = 0;
    	szInfoLog = null;

    	GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
    	if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);

    			System.out.println("RTR: FragmentShaderInfoLog" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

    	shaderProgramObject = GLES32.glCreateProgram();
    	GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
    	GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

    	//pre Link Binding to attributes
    	GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_COLOR, "vColor");
    	//Link the Shader Program
    	GLES32.glLinkProgram(shaderProgramObject);

    	int[] iProgramLinkStatus  = new int[1];
    	iInfoLogLength[0] = 0;
    	szInfoLog = null;

    	GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus,0);
    	if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			GLES32.glGetProgramInfoLog(shaderProgramObject);

    			System.out.println("RTR: ProgramLinkStatus" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

    	//Post Linking Retriving Uniform Location
    	mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

        //Array For Line
    	final float[] lineVertices = new float[]
                                	{
                                    	//Line
                                        0.0f, 0.8f,0.0f,
                                        0.0f, -0.8f, 0.0f
        	                        };
        //Array For Line Color
        final float[] lineColor = new  float[]
                                    {
                                        //Line
                                        1.0f, 1.0f,0.0f,
                                        1.0f, 1.0f, 0.0f
                                    };

        //Array For Triangle Vertices
        final float[] triangleVertices = new float[] 
                                {
                                   //Triangle
                                    0.0f, 0.8f, 0.0f,
                                    -0.8f, -0.8f, 0.0f,
                                    -0.8f, -0.8f, 0.0f,
                                    0.8f, -0.8f, 0.0f,
                                    0.8f, -0.8f, 0.0f,
                                    0.0f, 0.8f, 0.0f
                                };
        //Array For Triangle Color
        final int colors = 6;
        final  float[] triangleColor = new  float[3 * colors];
        for (int i = 0; i < colors; i++)
        {
            triangleColor[3 * i + 0] = 1.0f;
            triangleColor[3 * i + 1] = 1.0f;
            triangleColor[3 * i + 2] = 0.0f;
        }

        //Array For Circle Vertices
        final int ipoints = 300;
        final  float[] circleVertices = new  float[3 * ipoints];
        for (int i = 0; i < ipoints; i++)
        {
            float Angle = (2.0f * (float)Math.PI * i) / ipoints;

           circleVertices[3 * i + 0] = (float)Math.cos(Angle) * 0.494f;
           circleVertices[3 * i + 1] = (float)Math.sin(Angle) * 0.494f;
           circleVertices[3 * i + 2] = 0.0f;
        }

         //Array For Circle Color
        final int circleColors = 300;
        final  float[] colorCircle = new  float[3 * circleColors];
        for (int i = 0; i < circleColors; i++)
        {
            colorCircle[3 * i + 0] = 1.0f;
            colorCircle[3 * i + 1] = 1.0f;
            colorCircle[3 * i + 2] = 0.0f;
        }

        //vao For Graph
        GLES32.glGenVertexArrays(1, vao_Line,0);
    	GLES32.glBindVertexArray(vao_Line[0]);
    	GLES32.glGenBuffers(1, vbo_Line,0);
    	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Line[0]);
    	ByteBuffer byteBuffer = ByteBuffer.allocateDirect(lineVertices.length * 4);
    	byteBuffer.order(ByteOrder.nativeOrder());
    	FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
    	positionBuffer.put(lineVertices);
    	positionBuffer.position(0);
    	GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, lineVertices.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
    	GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
    	GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
    	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glGenBuffers(1, vbo_Line_Color,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Line_Color[0]);
        ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(lineColor.length * 4);
        byteBuffer1.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer1 = byteBuffer1.asFloatBuffer();
        positionBuffer1.put(lineColor);
        positionBuffer1.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, lineColor.length * 4, positionBuffer1, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
    	GLES32.glBindVertexArray(0);

        //vao For InnerCircle
        GLES32.glGenVertexArrays(1, vao_Triangle,0);
        GLES32.glBindVertexArray(vao_Triangle[0]);
        GLES32.glGenBuffers(1, vbo_Triangle,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Triangle[0]);
        ByteBuffer byteBuffer6 = ByteBuffer.allocateDirect(triangleVertices.length * 4);
        byteBuffer6.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer6 = byteBuffer6.asFloatBuffer();
        positionBuffer6.put(triangleVertices);
        positionBuffer6.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleVertices.length * 4, positionBuffer6, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glGenBuffers(1, vbo_Triangle_Color,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Triangle_Color[0]);
        ByteBuffer byteBuffer7 = ByteBuffer.allocateDirect(triangleColor.length * 4);
        byteBuffer7.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer7 = byteBuffer7.asFloatBuffer();
        positionBuffer7.put(triangleColor);
        positionBuffer7.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleColor.length * 4, positionBuffer7, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        //vao For Rectangle And TriAngle
        GLES32.glGenVertexArrays(1, vao_Circle,0);
        GLES32.glBindVertexArray(vao_Circle[0]);
        GLES32.glGenBuffers(1, vbo_Circle,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Circle[0]);
        ByteBuffer byteBuffer2 = ByteBuffer.allocateDirect(circleVertices.length * 4);
        byteBuffer2.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer2 = byteBuffer2.asFloatBuffer();
        positionBuffer2.put(circleVertices);
        positionBuffer2.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, circleVertices.length * 4, positionBuffer2, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glGenBuffers(1, vbo_Circle_Color,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Circle_Color[0]);
        ByteBuffer byteBuffer3 = ByteBuffer.allocateDirect(colorCircle.length * 4);
        byteBuffer3.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer3 = byteBuffer3.asFloatBuffer();
        positionBuffer3.put(colorCircle);
        positionBuffer3.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, colorCircle.length * 4, positionBuffer3, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);


        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        //GLES32.glClearDepth(1.0f);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
        System.out.println("RTR: EEEEEEEEEEEEEEE");
    }

    private void Resize_RMB(int iWidth, int iHeight)
    {

        if (iHeight == 0)
		{
			iHeight = 1;
		}

		GLES32.glViewport(0, 0, iWidth, iHeight);
        Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)iWidth / (float)iHeight, 0.1f, 100.0f);
    }

    private void Display_RMB()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject);

        float[] translationMatrix = new float[16];
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];
        //For Line Paper
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, 0.0f, pos_Yl, -5.0f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);

        GLES32.glBindVertexArray(vao_Line[0]);
        GLES32.glLineWidth(5.0f);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        GLES32.glBindVertexArray(0);


        //Shapes --->  Triangle
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, neg_Xt, neg_Yt, -5.0f);
        Matrix.rotateM(modelViewMatrix,0, angle, 0.0f, 1.0f, 0.0f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);

        GLES32.glBindVertexArray(vao_Triangle[0]);
        GLES32.glLineWidth(5.0f);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 6);
        GLES32.glBindVertexArray(0);

        //For Circle Vertices
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, pos_Xc, neg_Yc, -5.0f);
        Matrix.rotateM(modelViewMatrix,0, angle, 0.0f, 1.0f, 0.0f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);

        GLES32.glBindVertexArray(vao_Circle[0]);
        GLES32.glLineWidth(5.0f);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 300);
        GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);
        requestRender();   //SwapBuffers();
    }

    private void Update_RMB()
    {
        angle = angle + 1.5f;
        if(angle > 360.0f)
        {
            angle = 0.0f;
        }

        //For Line Translation
        pos_Yl = pos_Yl - 0.005f;
        if (pos_Yl <= 0.0f)
        {
            pos_Yl = 0.0f;
            triangle = true;
        }
    
        //For Triangle Translation
        if (triangle == true)
        {
            neg_Xt = neg_Xt + 0.005f;
            neg_Yt = neg_Yt + 0.005f;
            if (neg_Xt >= 0.0f || neg_Yt >= 0.0f)
            {
                neg_Xt = 0.0f;
                neg_Yt = 0.0f;
                circle = true;
            }
        }
    
        //For Circle Translation
        if (circle == true)
        {
            pos_Xc = pos_Xc - 0.005f;
            if (pos_Xc <= 0.0f)
            {
                pos_Xc = 0.0f;
            }
            neg_Yc = neg_Yc + 0.005f;
            if (neg_Yc >= -0.306f)
            {
                neg_Yc = -0.306f;
            }
        }
    }

    private void Uninitialize_RMB()
    {
        if(vbo_Circle_Color[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Circle_Color,0);
            vbo_Circle_Color[0] = 0;
        }
        if(vbo_Circle[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Circle,0);
            vbo_Circle[0] = 0;
        }
        if(vao_Circle[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_Circle,0);
            vao_Circle[0] = 0;
        }

    	if(vbo_Triangle_Color[0] != 0)
    	{
    		GLES32.glDeleteBuffers(1, vbo_Triangle_Color,0);
    		vbo_Triangle_Color[0] = 0;
    	}
        if(vbo_Triangle[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Triangle,0);
            vbo_Triangle[0] = 0;
        }
        if(vao_Triangle[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_Triangle,0);
            vao_Triangle[0] = 0;
        }
        
        if(vbo_Line_Color[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Line_Color,0);
            vbo_Line_Color[0] = 0;
        }

        if(vbo_Line[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Line,0);
            vbo_Line[0] = 0;
        }
        
        if(vao_Line[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_Line,0);
            vao_Line[0] = 0;
        }


    	if(shaderProgramObject != 0)
    	{
    		int[] shaderCount = new int[1];
    		int shaderNumber;

    		GLES32.glUseProgram(shaderProgramObject);
    		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount,0);

    		int[] shaders = new int[shaderCount[0]];
            if(shaders[0] != 0)
            {
                GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
                for(shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
                {
                   GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
                   GLES32.glDeleteShader(shaders[shaderNumber]);
                   shaders[shaderNumber] = 0;
                }
            }
            
    	}
    	GLES32.glUseProgram(0);
    	GLES32.glDeleteProgram(shaderProgramObject);
    	shaderProgramObject = 0;
    }
}

