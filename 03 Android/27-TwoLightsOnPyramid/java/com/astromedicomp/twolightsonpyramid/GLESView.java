package com.astromedicomp.twolightsonpyramid;
//added by me
import android.content.Context;
import android.view.Gravity;
import android.graphics.Color; // for "Color" class
import android.view.MotionEvent; // for "MotionEvent" class
import android.view.GestureDetector; // for "GestureDetector"
import android.view.GestureDetector.OnGestureListener; //for "OnGestureListener"  
import android.view.GestureDetector.OnDoubleTapListener; // for "OnDoubleTapListener" 

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import java.nio.ByteBuffer;		//for OpenGL Buffers
import java.nio.ByteOrder;      //for OpenGL Buffers
import java.nio.FloatBuffer;	//for OpenGL Buffers
import java.lang.Math; 

import android.opengl.Matrix;   //for matrix math

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int modelUniform;
    private int viewUniform;
    private int projectionUniform;

    private int laUniform_Red;
    private int ldUniform_Red;
    private int lsUniform_Red;
    private int lightPositionUniform_Red;

    private int laUniform_Blue;
    private int ldUniform_Blue;
    private int lsUniform_Blue;
    private int lightPositionUniform_Blue;

    private int kaUniform;
    private int kdUniform;
    private int ksUniform;

    private int lKeyIsPressedUniform;
    private int materialShininessUniform;

    private int[] vao_Cube = new int[1];
    private int[] vbo_Cube_Position = new int[1];
    private int[] vbo_Cube_Normal = new int[1];

 	private float[] modelMatrix = new float[16];
    private float[] viewMatrix = new float[16];
    private float[] projectionMatrix = new float[16];  // 4 x 4  Matrix

    private float[] ambient_Red = new float[4];
    private float[] diffuse_Red = new float[4];
    private float[] specular_Red = new float[4];
    private float[] position_Red = new float[4];

    private float[] ambient_Blue = new float[4];
    private float[] diffuse_Blue = new float[4];
    private float[] specular_Blue = new float[4];
    private float[] position_Blue = new float[4];

    private float[] materialAmbient = new float[] { 0.0f, 0.0f, 0.0f, 0.0f };
    private float[] materialDiffuse = new float[] { 0.0f, 0.0f, 0.0f, 0.0f };
    private float[] materialSpecular = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };
    private float[] materialShininess = new float[] { 50.0f };
    
    boolean gbAnimation = false;
    boolean gbLight = false;

    float angle = 0.0f;

	public GLESView(Context dwContext)
	{
		super(dwContext);

        context = dwContext;

		setEGLContextClientVersion(3); //version 3.x pahije manun
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(dwContext, this, null, false);  //this means 'handler' i.e who is going to handle
		gestureDetector.setOnDoubleTapListener(this);   // this means 'handler' i.e. who is going to handle
    
	}
	// Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
    	//code
    	int eventaction = event.getAction();
    	if(!gestureDetector.onTouchEvent(event))
    		super.onTouchEvent(event);
    	return(true);
    }

     // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
    	// Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
         gbAnimation = !gbAnimation;
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
    	Uninitialize_RMB();
    	System.exit(0);
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        if(gbLight == false)
        {
            gbLight = true;
        }
        else
        {
            gbLight = false;
        }
    	return(true);
    }

    //Impliment GLSurfaceView.Renderer Mathods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig congif)  //WM_CREATE OR Initialize
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR:" +version);
        String version1 = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR:" +version1);
        initialize_RMB();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int Width, int Height)  //WM_SIZE or RESIZE
    {
        Resize_RMB(Width, Height);
    }

    @Override
    public void onDrawFrame(GL10 unused)  //Display
    {
        Update_RMB();
        Display_RMB();
        System.out.println("RTR: onDrawFrame");
    }

    //Our Custome Methods
    private void initialize_RMB()
    {
    	vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
        final String vertexShaderSourceCode = String.format 
        (
            "#version 320 es" +
            "\n" +
            
            "in vec4 vertexPosition;" +
            "in vec3 vertexNormal;" +

            "uniform mat4 u_modelMatrix;" +
            "uniform mat4 u_viewMatrix;" +
            "uniform mat4 u_projection_matrix;" +

            "uniform vec3 u_La_Red;" +
            "uniform vec3 u_Ld_Red;" +
            "uniform vec3 u_Ls_Red;" +

            "uniform vec3 u_La_Blue;" +
            "uniform vec3 u_Ld_Blue;" +
            "uniform vec3 u_Ls_Blue;" +

            "uniform vec3 u_Ka;" +
            "uniform vec3 u_Kd;" +
            "uniform vec3 u_Ks;" +

            "uniform float u_materialShininess;" +
            "uniform int u_lKeyIsPressed;" +
            "uniform vec4 u_Light_Position_Red;" +
            "uniform vec4 u_Light_Position_Blue;" +

            "out vec3 phong_AdsLight;" +

            "void main(void)" +
            "{" +

                "if(u_lKeyIsPressed == 1)" +
                "{" +
                    "vec4 eye_coordinate = u_viewMatrix * u_modelMatrix * vertexPosition;" +

                    "vec3 tnorm = normalize(mat3(u_viewMatrix * u_modelMatrix) * vertexNormal);" +
                    //For Red Light
                    "vec3 lightdirection_Red = normalize(vec3(u_Light_Position_Red - eye_coordinate));" +

                    "float tn_dot_lightDir_Red = max(dot(lightdirection_Red, tnorm), 0.0);" +

                    "vec3 reflectionVector_Red = reflect(-lightdirection_Red, tnorm);" +

                    "vec3 viewerVector = normalize(vec3(-eye_coordinate.xyz));" +

                    "vec3 ambient_Red = u_La_Red * u_Ka;" +

                    "vec3 diffuse_Red = u_Ld_Red * u_Kd * tn_dot_lightDir_Red;" +

                     "vec3 specular_Red = u_Ls_Red * u_Ks * pow(max(dot(reflectionVector_Red, viewerVector), 0.0), u_materialShininess);" +

                    //For Blue Light
                     "vec3 lightdirection_Blue = normalize(vec3(u_Light_Position_Blue - eye_coordinate));" +

                    "float tn_dot_lightDir_Blue = max(dot(lightdirection_Blue, tnorm), 0.0);" +

                    "vec3 reflectionVector_Blue = reflect(-lightdirection_Blue, tnorm);" +

                    "vec3 ambient_Blue = u_La_Blue * u_Ka;" +

                    "vec3 diffuse_Blue = u_Ld_Blue * u_Kd * tn_dot_lightDir_Blue;" +

                    "vec3 specular_Blue = u_Ls_Blue * u_Ks * pow(max(dot(reflectionVector_Blue, viewerVector), 0.0), u_materialShininess);" +

                    "phong_AdsLight = ambient_Red + diffuse_Red + specular_Red + ambient_Blue + diffuse_Blue + specular_Blue;" +

                "}" +
                "else" +
                "{" +
                        "phong_AdsLight = vec3(1.0, 1.0, 1.0);" +
                "}" +

                 "gl_Position = u_projection_matrix * u_viewMatrix * u_modelMatrix * vertexPosition;" +

            "}"

        );

        //Specifing above source to the vertex shader object
        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

        //Compile the Vertex Shader
        GLES32.glCompileShader(vertexShaderObject);

        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);

                System.out.println("RTR: vertexShaderInfoLog" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        //Define Fragment Shader Object
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        //Write Fragment Shader Code
        final String fragmentShaderSourceCode = String.format
        (
            "#version 320 es" +
            "\n" +
            "precision highp float;" +

            "in vec3 phong_AdsLight;" +
            "out vec4 fragColor;" +
            "void main(void)" +
            "{" +
                "fragColor = vec4(phong_AdsLight, 1.0);" +
            "}"
        );

        // Specifing above Source to the fragment Shader Object
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
       
        //Compile the fragment Shader Object
        GLES32.glCompileShader(fragmentShaderObject);

        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);

                System.out.println("RTR: FragmentShaderInfoLog" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        shaderProgramObject = GLES32.glCreateProgram();
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        //pre Link Binding to attributes
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vertexPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_NORMAL, "vertexNormal");
        //Link the Shader Program
        GLES32.glLinkProgram(shaderProgramObject);

        int[] iProgramLinkStatus  = new int[1];
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus,0);
        if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                GLES32.glGetProgramInfoLog(shaderProgramObject);

                System.out.println("RTR: ProgramLinkStatus" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        //Post Linking Retriving Uniform Location
        modelUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_modelMatrix");
        viewUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
        projectionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

        laUniform_Red = GLES32.glGetUniformLocation(shaderProgramObject, "u_La_Red");
        ldUniform_Red = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld_Red");
        lsUniform_Red = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls_Red");

        laUniform_Blue = GLES32.glGetUniformLocation(shaderProgramObject, "u_La_Blue");
        ldUniform_Blue = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld_Blue");
        lsUniform_Blue = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls_Blue");

        kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
        kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
        ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");

        lightPositionUniform_Red = GLES32.glGetUniformLocation(shaderProgramObject, "u_Light_Position_Red");
        lightPositionUniform_Blue = GLES32.glGetUniformLocation(shaderProgramObject, "u_Light_Position_Blue");

        materialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_materialShininess");

        lKeyIsPressedUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");

        //Array For cubeVertices Vertices
        final float[] cubeVertices = new float[]
                                        {
                                            //FRONT FACE
                                            0.0f, 1.0f, 0.0f,
                                            -1.0f, -1.0f, 1.0f,
                                            1.0f, -1.0f, 1.0f,
                                            //LEFT FACE
                                            0.0f, 1.0f, 0.0f,
                                            1.0f, -1.0f, 1.0f,
                                            1.0f, -1.0f, -1.0f,
                                            //BACK FACE
                                            0.0f, 1.0f, 0.0f,
                                            1.0f, -1.0f, -1.0f,
                                            -1.0f, -1.0f, -1.0f,
                                            //RIGHT FACE
                                            0.0f, 1.0f, 0.0f,
                                            -1.0f, -1.0f, -1.0f,
                                            -1.0f, -1.0f, 1.0f
                                        };
        //Array For cubeColor Color
        final float[] cubeColor = new  float[]
                                        {
                                            0.0f, 0.447214f, 0.894427f,
                                            0.0f, 0.447214f, 0.894427f,
                                            0.0f, 0.447214f, 0.894427f,

                                            0.89427f, 0.447214f, 0.0f,
                                            0.89427f, 0.447214f, 0.0f,
                                            0.89427f, 0.447214f, 0.0f,

                                            0.0f, 0.447214f, -0.894427f,
                                            0.0f, 0.447214f, -0.894427f,
                                            0.0f, 0.447214f, -0.894427f,

                                            -0.894427f, 0.447214f, 0.0f,
                                            -0.894427f, 0.447214f, 0.0f,
                                            -0.894427f, 0.447214f, 0.0f
                                        };
       
       
        GLES32.glGenVertexArrays(1, vao_Cube,0);
        GLES32.glBindVertexArray(vao_Cube[0]);
        GLES32.glGenBuffers(1, vbo_Cube_Position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Cube_Position[0]);
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(cubeVertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
        positionBuffer.put(cubeVertices);
        positionBuffer.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeVertices.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glGenBuffers(1, vbo_Cube_Normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Cube_Normal[0]);
        ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(cubeColor.length * 4);
        byteBuffer1.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer1 = byteBuffer1.asFloatBuffer();
        positionBuffer1.put(cubeColor);
        positionBuffer1.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeColor.length * 4, positionBuffer1, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);


        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        //GLES32.glClearDepth(1.0f);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.setIdentityM(viewMatrix, 0);
        Matrix.setIdentityM(projectionMatrix, 0);
    }

    private void Resize_RMB(int iWidth, int iHeight)
    {

        if (iHeight == 0)
		{
			iHeight = 1;
		}

		GLES32.glViewport(0, 0, iWidth, iHeight);
        Matrix.perspectiveM(projectionMatrix, 0, 45.0f, (float)iWidth / (float)iHeight, 0.1f, 100.0f);
    }

    private void Display_RMB()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject);

        if (gbLight == true)
		{
			 GLES32.glUniform1i(lKeyIsPressedUniform, 1);
             GLES32.glUniform3fv(laUniform_Red, 1, ambient_Red, 0);
             GLES32.glUniform3fv(ldUniform_Red, 1, diffuse_Red, 0);
             GLES32.glUniform3fv(lsUniform_Red, 1, specular_Red, 0);
             GLES32.glUniform4fv(lightPositionUniform_Red, 1, position_Red, 0);

             GLES32.glUniform3fv(laUniform_Blue, 1, ambient_Blue, 0);
             GLES32.glUniform3fv(ldUniform_Blue, 1, diffuse_Blue, 0);
             GLES32.glUniform3fv(lsUniform_Blue, 1, specular_Blue, 0);
             GLES32.glUniform4fv(lightPositionUniform_Blue, 1, position_Blue, 0);

             GLES32.glUniform3fv(kaUniform, 1, materialAmbient, 0);
             GLES32.glUniform3fv(kdUniform, 1, materialDiffuse, 0);
             GLES32.glUniform3fv(ksUniform, 1, materialSpecular, 0);
             GLES32.glUniform1fv(materialShininessUniform, 1, materialShininess, 0);

             //Array Zero
            ambient_Red[0] = 0.0f;
            ambient_Red[1] = 0.0f;
            ambient_Red[2] = 0.0f;
            ambient_Red[3] = 1.0f;

            diffuse_Red[0] = 1.0f;
            diffuse_Red[1] = 0.0f;
            diffuse_Red[2] = 0.0f;
            diffuse_Red[3] = 1.0f;

            specular_Red[0] = 1.0f;
            specular_Red[1] = 0.0f;
            specular_Red[2] = 0.0f;
            specular_Red[3] = 1.0f;

            position_Red[0] = -2.0f;
            position_Red[1] = 0.0f;
            position_Red[2] = 0.0f;
            position_Red[3] = 1.0f;

            //Array One
            ambient_Blue[0] = 0.0f;
            ambient_Blue[1] = 0.0f;
            ambient_Blue[2] = 0.0f;
            ambient_Blue[3] = 1.0f;

            diffuse_Blue[0] = 0.0f;
            diffuse_Blue[1] = 0.0f;
            diffuse_Blue[2] = 1.0f;
            diffuse_Blue[3] = 1.0f;

            specular_Blue[0] = 0.0f;
            specular_Blue[1] = 0.0f;
            specular_Blue[2] = 1.0f;
            specular_Blue[3] = 1.0f;

            position_Blue[0] = 2.0f;
            position_Blue[1] = 0.0f;
            position_Blue[2] = 0.0f;
            position_Blue[3] = 1.0f;
		}
		else
		{
			GLES32.glUniform1i(lKeyIsPressedUniform, 0);
		}

       
        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.setIdentityM(viewMatrix, 0);

        Matrix.translateM(modelMatrix,0, 0.0f, 0.0f, -5.0f);
        Matrix.rotateM(modelMatrix,0, angle, 0.0f, 1.0f, 0.0f);

        GLES32.glUniformMatrix4fv(modelUniform, 1, false, modelMatrix,0);
         GLES32.glUniformMatrix4fv(viewUniform, 1, false, viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionUniform, 1, false, projectionMatrix,0);

        GLES32.glBindVertexArray(vao_Cube[0]);
        GLES32.glDrawArrays( GLES32.GL_TRIANGLES, 0, 12);
        GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);
        requestRender();   //SwapBuffers();
    }

    private void  Update_RMB()
    {
        if(gbAnimation)
        {
             angle = angle + 1.0f;
            if(angle > 360.0f)
            {
                angle = 0.0f;
            }
        }
    }

    private void Uninitialize_RMB()
    {
        if(vbo_Cube_Normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Cube_Normal,0);
            vbo_Cube_Normal[0] = 0;
        }
        if(vbo_Cube_Position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Cube_Position,0);
            vbo_Cube_Position[0] = 0;
        }
        if(vao_Cube[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_Cube,0);
            vao_Cube[0] = 0;
        }


    	if(shaderProgramObject != 0)
    	{
    		int[] shaderCount = new int[1];
    		int shaderNumber;

    		GLES32.glUseProgram(shaderProgramObject);
    		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount,0);

    		int[] shaders = new int[shaderCount[0]];
            if(shaders[0] != 0)
            {
                GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
                for(shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
                {
                   GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
                   GLES32.glDeleteShader(shaders[shaderNumber]);
                   shaders[shaderNumber] = 0;
                }
            }
            
    	}
    	GLES32.glUseProgram(0);
    	GLES32.glDeleteProgram(shaderProgramObject);
    	shaderProgramObject = 0;
    }
}




