package com.astromedicomp.checker_board;
//default given packages
import androidx.appcompat.app.AppCompatActivity;   //extends AppCompatActivity
import android.os.Bundle;   //Bundle savedInstanceStat

//added by me
import android.view.View;
import android.view.Window;
import android.view.WindowManager;   //this.getWindow().setFlags(WindowManager);
import android.view.MotionEvent;    
import android.content.pm.ActivityInfo;
import android.graphics.Color; // for "Color" class
import android.content.Context;
import android.view.Gravity;

public class MainActivity extends AppCompatActivity 
{
	private GLESView glesView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        System.out.println("RTR: I am onCreate");
        //setContentView(R.layout.activity_main);
        //GetReal of the title bar
        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        //hide navigation bar
        //this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        View decorView = this.getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        //Make Full Screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
       
        //Forced landscape orientation
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //SetBackground Color
        this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);

        //define our own view
        glesView = new GLESView(this);

        //set this view as our main view
        setContentView(glesView);
    }

    @Override
    protected void onPause()
    {
    	super.onPause();
    }

    @Override
    protected void onResume()
    {
    	super.onResume();
    }
}


