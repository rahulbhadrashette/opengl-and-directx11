package com.astromedicomp.checker_board;
//added by me
import android.content.Context;
import android.view.Gravity;
import android.view.MotionEvent; // for "MotionEvent" class
import android.view.GestureDetector; // for "GestureDetector"
import android.view.GestureDetector.OnGestureListener; //for "OnGestureListener"  
import android.view.GestureDetector.OnDoubleTapListener; // for "OnDoubleTapListener" 
//for OpenGL
import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
//for Texture
import android.graphics.Bitmap;
import android.opengl.GLUtils;

import java.nio.ByteBuffer;		//for OpenGL Buffers
import java.nio.ByteOrder;      //for OpenGL Buffers
import java.nio.FloatBuffer;	//for OpenGL Buffers

import android.opengl.Matrix;   //for matrix math

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int[] vao = new int[1];
    private int[] vbo_Position_CB1 = new int[1];
    private int[] vbo_Texture_CB = new int[1];

    private final int checkImageWidth = 64;
    private final int checkImageHeight = 64;
    private byte[] checkImage = new byte[checkImageHeight * checkImageWidth * 4];

    private int mvpUniform;
    private int samplerUniform;
    private int[] textureCheckerBoard = new int[1];
    private float[] perspectiveProjectionMatrix = new float[16];  // 4 x 4  Matrix

	public GLESView(Context dwContext)
	{
		super(dwContext);

        context = dwContext;

		setEGLContextClientVersion(3); //version 3.x pahije manun
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(dwContext, this, null, false);  //this means 'handler' i.e who is going to handle
		gestureDetector.setOnDoubleTapListener(this);   // this means 'handler' i.e. who is going to handle
    
	}
	// Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
    	//code
    	int eventaction = event.getAction();
    	if(!gestureDetector.onTouchEvent(event))
    		super.onTouchEvent(event);
    	return(true);
    }

     // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
    	 // Do Not Write Any code Here Because Already Written 'onDoubleTap'
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
    	// Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
    	Uninitialize_RMB();
    	System.exit(0);
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
    	return(true);
    }

    //Impliment GLSurfaceView.Renderer Mathods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig congif)  //WM_CREATE OR Initialize
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR:" +version);
        String version1 = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR:" +version1);
        initialize_RMB();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int Width, int Height)  //WM_SIZE or RESIZE
    {
        Resize_RMB(Width, Height);
    }

    @Override
    public void onDrawFrame(GL10 unused)  //Display
    {
        Display_RMB();
        System.out.println("RTR: onDrawFrame");
    }

    //Our Custome Methods
    private void initialize_RMB()
    {
    	vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
    	final String vertexShaderSourceCode = String.format 
    	(
    		"#version 320 es" +
			"\n" +
            "in vec4 vPosition;" +
            "in vec2 vTexCoord;" +
            "uniform mat4 u_mvp_matrix;" +
            "out vec2 out_TexCoord;" +
            "void main(void)" +
            "{" +
                "gl_Position = u_mvp_matrix * vPosition;" +
                "out_TexCoord = vTexCoord;" +
            "}"
    	);

        //Specifing above source to the vertex shader object
    	GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

        //Compile the Vertex Shader
        GLES32.glCompileShader(vertexShaderObject);

    	int[] iShaderCompileStatus = new int[1];
    	int[] iInfoLogLength = new int[1];
    	String szInfoLog = null;

    	GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
    	if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);

    			System.out.println("RTR: vertexShaderInfoLog" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

        //Define Fragment Shader Object
    	fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        //Write Fragment Shader Code
    	final String fragmentShaderSourceCode = String.format
    	(
    		"#version 320 es" +
			"\n" +
			"precision highp float;" +
            "in vec2 out_TexCoord;" +
            "uniform sampler2D u_Sampler;" +
            "out vec4 fragColor;" +
            "void main(void)" +
            "{" +
               "fragColor = texture(u_Sampler, out_TexCoord);" +
            "}"
    	);

        // Specifing above Source to the fragment Shader Object
    	GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
       
        //Compile the fragment Shader Object
    	GLES32.glCompileShader(fragmentShaderObject);

    	iShaderCompileStatus[0] = 0;
    	iInfoLogLength[0] = 0;
    	szInfoLog = null;

    	GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
    	if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);

    			System.out.println("RTR: FragmentShaderInfoLog" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

    	shaderProgramObject = GLES32.glCreateProgram();
    	GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
    	GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

    	//pre Link Binding to attributes
    	GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");
    	//Link the Shader Program
    	GLES32.glLinkProgram(shaderProgramObject);

    	int[] iProgramLinkStatus  = new int[1];
    	iInfoLogLength[0] = 0;
    	szInfoLog = null;

    	GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus,0);
    	if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
    	{
    		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
    		if(iInfoLogLength[0] > 0)
    		{
    			GLES32.glGetProgramInfoLog(shaderProgramObject);

    			System.out.println("RTR: ProgramLinkStatus" +szInfoLog);
    			Uninitialize_RMB();
    			System.exit(0);
    		}
    	}

    	//Post Linking Retriving Uniform Location
    	mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        samplerUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Sampler");

       	final float[] squreTexCoord = new float[]
                            {
                                0.0f, 0.0f,
                                0.0f, 1.0f,
                                1.0f, 1.0f, 
                                1.0f, 0.0f
                            };

        //for Rectangle
        GLES32.glGenVertexArrays(1, vao,0);
        GLES32.glBindVertexArray(vao[0]);

        GLES32.glGenBuffers(1, vbo_Position_CB1,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Position_CB1[0]);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 4*3*4, null, GLES32.GL_DYNAMIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glGenBuffers(1, vbo_Texture_CB,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Texture_CB[0]);

        ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(squreTexCoord.length * 4);
        byteBuffer1.order(ByteOrder.nativeOrder());
        FloatBuffer texCoordBuffer = byteBuffer1.asFloatBuffer();
        texCoordBuffer.put(squreTexCoord);
        texCoordBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, squreTexCoord.length * 4, texCoordBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, 2, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLES32.glDisable(GLES32.GL_CULL_FACE);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        GLES32.glEnable(GLES32.GL_TEXTURE_2D);
        textureCheckerBoard[0] = LoadTexture();
        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
    }

    private int LoadTexture()
    {
        MakeCheckImage();

        int[] texture = new int[1];

        ByteBuffer byteBuffer4 = ByteBuffer.allocateDirect(checkImageWidth* checkImageHeight * 4);
        System.out.println("RTR: byteBuffer4" +byteBuffer4);
        byteBuffer4.order(ByteOrder.nativeOrder());
        byteBuffer4.put(checkImage);
        byteBuffer4.position(0);

        Bitmap bitmap = Bitmap.createBitmap(checkImageWidth, checkImageHeight, Bitmap.Config.ARGB_8888);
        System.out.println("RTR: bitmaps" +bitmap);
        bitmap.copyPixelsFromBuffer(byteBuffer4);

        GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1);
        GLES32.glGenTextures(1, texture,0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_S, GLES32.GL_REPEAT);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_T, GLES32.GL_REPEAT);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_NEAREST);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_NEAREST);
        GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0); //GLUtils madhya ahe ha call
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
        System.out.println("RTR: texture[0]" +texture[0]);
        return(texture[0]);
    }

    private void Resize_RMB(int iWidth, int iHeight)
    {

        if (iHeight == 0)
		{
			iHeight = 1;
		}

		GLES32.glViewport(0, 0, iWidth, iHeight);

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)iWidth / (float)iHeight, 0.1f, 100.0f);
    }
   
    private void Display_RMB()
    {
        float[] position = new float[12];
        float[] position1 = new float[12];
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        GLES32.glUseProgram(shaderProgramObject);

        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];
        ///////////////////////////////////////////////////////////////////////
        //First CheckerBoard
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, 0.0f, 0.0f, -3.6f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);

        GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, textureCheckerBoard[0]);
        GLES32.glUniform1i(samplerUniform, 0);

        GLES32.glBindVertexArray(vao[0]);
        
        position[0] = 1.0f;
        position[1] = -1.0f;
        position[2] = 0.0f;
        position[3] = 1.0f;
        position[4] = 1.0f;
        position[5] = 0.0f;
        position[6] = 2.41421f;
        position[7] = 1.0f;
        position[8] = -1.41421f;
        position[9] = 2.41421f;
        position[10] = -1.0f;
        position[11] = -1.41421f;
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Position_CB1[0]);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(position.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
        positionBuffer.put(position);
        positionBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,  position.length * 4, positionBuffer, GLES32.GL_DYNAMIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
        GLES32.glBindVertexArray(0);
        //////////////////////////////////////////////////////////////////////
        //Second CheckerBoard
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix,0, 0.0f, 0.0f, -3.6f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix,0);

        GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, textureCheckerBoard[0]);
        GLES32.glUniform1i(samplerUniform, 0);

        GLES32.glBindVertexArray(vao[0]);
        
        position1[0] = -2.0f;
        position1[1] = -1.0f;
        position1[2] = 0.0f;
        position1[3] = -2.0f;
        position1[4] = 1.0f;
        position1[5] = 0.0f;
        position1[6] = 0.0f;
        position1[7] = 1.0f;
        position1[8] = 0.0f;
        position1[9] = 0.0f;
        position1[10] = -1.0f;
        position1[11] = 0.0f;
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Position_CB1[0]);

        ByteBuffer byteBuffer2 = ByteBuffer.allocateDirect(position1.length * 4);
        byteBuffer2.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer1 = byteBuffer2.asFloatBuffer();
        positionBuffer1.put(position1);
        positionBuffer1.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, position1.length * 4, positionBuffer1, GLES32.GL_DYNAMIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
        
        GLES32.glBindVertexArray(0);
        GLES32.glUseProgram(0);
        requestRender();   //SwapBuffers();
    }

    private void MakeCheckImage()
    {
        int i, j, c;

        for (i = 0; i < checkImageHeight; i++)
        {
            for (j = 0; j < checkImageWidth; j++)
            {
                /* code */
                c = ((i & 8) ^ (j & 8)) * 255;
                checkImage[(i*64+j) * 4 + 0] = (byte)c;
                checkImage[(i*64+j) * 4 + 1] = (byte)c;
                checkImage[(i*64+j) * 4 + 2] = (byte)c;
                checkImage[(i*64+j) * 4 + 3] = (byte)0xff;
                 System.out.println("RTR: checkImage[0]" +checkImage[0]);
            }
        }
    }

    private void Uninitialize_RMB()
    {
    	if(vbo_Texture_CB[0] != 0)
    	{
    		GLES32.glDeleteBuffers(1, vbo_Texture_CB,0);
    		vbo_Texture_CB[0] = 0;
    	}

        if(vbo_Position_CB1[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Position_CB1,0);
            vbo_Position_CB1[0] = 0;
        }

    	if(vao[0] != 0)
    	{
    		GLES32.glDeleteVertexArrays(1, vao,0);
    		vao[0] = 0;
    	}

        if(textureCheckerBoard[0] != 0)
        {
             GLES32.glDeleteTextures(1, textureCheckerBoard,0);
             textureCheckerBoard[0] = 0;
        }

    	if(shaderProgramObject != 0)
    	{
    		int[] shaderCount = new int[1];
    		int shaderNumber;

    		GLES32.glUseProgram(shaderProgramObject);
    		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount,0);

    		int[] shaders = new int[shaderCount[0]];
            if(shaders[0] != 0)
            {
                GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
                for(shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
                {
                   GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
                   GLES32.glDeleteShader(shaders[shaderNumber]);
                   shaders[shaderNumber] = 0;
                }
            }
            
    	}
    	GLES32.glUseProgram(0);
    	GLES32.glDeleteProgram(shaderProgramObject);
    	shaderProgramObject = 0;
    }
}

