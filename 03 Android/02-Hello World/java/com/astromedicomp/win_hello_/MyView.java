package com.astromedicomp.win_hello_;
//added by me
import androidx.appcompat.widget.AppCompatTextView;
import android.content.Context;
import android.view.Gravity;
import android.graphics.Color; // for "Color" class

public class MyView extends AppCompatTextView
{
	public MyView(Context dwContext)
	{
		super(dwContext);
		setTextColor(Color.rgb(0,255,0));
		setTextSize(60);
		setGravity(Gravity.CENTER);
		setText("Hello World...!!!");
	}
}

