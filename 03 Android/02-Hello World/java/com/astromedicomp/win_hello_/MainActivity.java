package com.astromedicomp.win_hello_;
//default given packages
import androidx.appcompat.app.AppCompatActivity;   //extends AppCompatActivity
import android.os.Bundle;   //Bundle savedInstanceStat

//added by me
import android.view.View;
import android.view.Window;
import android.view.WindowManager;   //this.getWindow().setFlags(WindowManager);
import android.content.pm.ActivityInfo;
import android.graphics.Color; // for "Color" class

public class MainActivity extends AppCompatActivity 
{
	private MyView myView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        //GetReal of the title bar
        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        //Make Full Screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
       
        //Forced landscape orientation
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //SetBackground Color
        this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);

        //hide title bar
        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION );

        //define our own view
        myView = new MyView(this);

        //set this view as our main view
        setContentView(myView);
    }

    @Override
    protected void onPause()
    {
    	super.onPause();
    }

    @Override
    protected void onResume()
    {
    	super.onResume();
    }
}


