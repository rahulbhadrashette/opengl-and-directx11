package com.astromedicomp.spheremeterial;
//added by me
import android.content.Context;
import android.view.Gravity;
import android.view.Display;
import android.view.WindowManager;
import android.os.Build;
import android.graphics.Color; // for "Color" class
import android.graphics.Point;
import android.view.MotionEvent; // for "MotionEvent" class
import android.view.GestureDetector; // for "GestureDetector"
import android.view.GestureDetector.OnGestureListener; //for "OnGestureListener"  
import android.view.GestureDetector.OnDoubleTapListener; // for "OnDoubleTapListener" 

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import java.nio.ByteBuffer;		//for OpenGL Buffers
import java.nio.ByteOrder;      //for OpenGL Buffers
import java.nio.FloatBuffer;	//for OpenGL Buffers
import java.lang.Math; 

import android.opengl.Matrix;   //for matrix math
import java.nio.ShortBuffer;    //For Sphere

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject_PV;
    private int shaderProgramObject;

    private float[] modelMatrix = new float[16];
    private float[] viewMatrix = new float[16];
    private float[] projectionMatrix = new float[16];  // 4 x 4  Matrix

    //For sphere
    private int gNumVertices;
    private int gNumElements;

    private int[] vao_Sphere = new int[1];
    private int[] vbo_Sphere_Position = new int[1];
    private int[] vbo_Sphere_Normal = new int[1];
    private int[] vbo_Sphere_Element = new int[1];

    private float[] lightAmbient_Red = new float[4]; //La
    private float[] lightDiffuse_Red = new float[4]; //Ld
    private float[] lightPosition_Red = new float[4];

    private float[][] materialAmbient = new float[24][4]; //Ka
    private float[][] materialDiffuse = new float[24][4]; //Kd
    private float[][] materialSpecular = new float[24][4]; //Ks
    private float[][] materialShininess = new float[24][1]; //128.0f;
    //*********************************************************************
    ////For Per Vertex
    private int modelUniform_RMB;
    private int viewUniform_RMB;
    private int projectionUniform_RMB;

    private int kaUniform_RMB;
    private int kdUniform_RMB;
    private int ksUniform_RMB;

    private int lKeyIsPressedUniform_RMB;
    private int materialShininessUniform_RMB;
    //For Red Lighting
    private int laUniform_Red_RMB;
    private int ldUniform_Red_RMB;
    private int lsUniform_Red_RMB;
    private int lightPositionUniform_Red_RMB;
    
    //*********************************************************************
    //For per Fragment
    private int modelUniform;
    private int viewUniform;
    private int projectionUniform;

    private int kaUniform;
    private int kdUniform;
    private int ksUniform;

    private int lKeyIsPressedUniform;
    private int materialShininessUniform;
    //For Red Lighting
    private int laUniform_Red;
    private int ldUniform_Red;
    private int lsUniform_Red;
    private int lightPositionUniform_Red;

    private int giWinWidth;
    private int giWinHeight;
    
    boolean gbLight = false;
    boolean fragmentShader;

    private float angleR = 0.0f;
    private float co1;
    private float co2;
    private int keyIncrement;

	public GLESView(Context dwContext)
	{
		super(dwContext);

        context = dwContext;

		setEGLContextClientVersion(3); //version 3.x pahije manun
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(dwContext, this, null, false);  //this means 'handler' i.e who is going to handle
		gestureDetector.setOnDoubleTapListener(this);   // this means 'handler' i.e. who is going to handle

        Point size = new Point();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();

        if (Build.VERSION.SDK_INT >= 17)
        {
            display.getRealSize(size);
        }
        else
        {
            display.getSize(size);
        }

        giWinWidth= size.x;
        giWinHeight = size.y;
	}
	// Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
    	//code
    	int eventaction = event.getAction();
    	if(!gestureDetector.onTouchEvent(event))
    		super.onTouchEvent(event);
    	return(true);
    }

     // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'

        keyIncrement++;
        if(keyIncrement == 3)
        {
            keyIncrement = 0;
        }
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {   
        
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        if(gbLight == false)
        {
            gbLight = true;
        }
        else
        {
            gbLight = false;
        }
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
    	// Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
       fragmentShader = !fragmentShader;
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
    	Uninitialize_RMB();
    	System.exit(0);
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
    	return(true);
    }

    //Impliment GLSurfaceView.Renderer Mathods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig congif)  //WM_CREATE OR Initialize
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR:" +version);
        String version1 = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR:" +version1);
        initialize_RMB();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int Width, int Height)  //WM_SIZE or RESIZE
    {
        Resize_RMB(Width, Height);
    }

    @Override
    public void onDrawFrame(GL10 unused)  //Display
    {
        Update_RMB();
        Display_RMB();
        System.out.println("RTR: onDrawFrame");
    }

    //Our Custome Methods
    private void initialize_RMB()
    {
    	perVertexShaderFunc();
        perFragmentShaderFunc();

        //Array For cubeVertices Vertices
        Sphere sphere = new Sphere();
        float sphere_vertices[] = new float[1146];
        float sphere_normals[] = new float[1146];
        float sphere_textures[] = new float[764];
        short sphere_elements[] = new short[2280];

        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        gNumVertices = sphere.getNumberOfSphereVertices();
        gNumElements = sphere.getNumberOfSphereElements();
       
       //vbo
        GLES32.glGenVertexArrays(1, vao_Sphere,0);
        GLES32.glBindVertexArray(vao_Sphere[0]);

        // position vbo
        GLES32.glGenBuffers(1, vbo_Sphere_Position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Sphere_Position[0]);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_vertices.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        //normal vbo
        GLES32.glGenBuffers(1, vbo_Sphere_Normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Sphere_Normal[0]);

        byteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer = byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_normals.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        //element vbo
        GLES32.glGenBuffers(1, vbo_Sphere_Element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_Sphere_Element[0]);

        byteBuffer = ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer = byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER, sphere_elements.length * 2, elementsBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);


        GLES32.glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
        //GLES32.glClearDepth(1.0f);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.setIdentityM(viewMatrix, 0);
        Matrix.setIdentityM(projectionMatrix, 0);
    }

    private void perVertexShaderFunc()
    {
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
        final String vertexShaderSourceCode = String.format 
        (
            "#version 320 es" 
            +"\n" 
            +"in vec4 vertexPosition_RMB;" 
            +"in vec3 vertexNormal_RMB;" 

            +"uniform mat4 u_modelMatrix_RMB;" 
            +"uniform mat4 u_viewMatrix_RMB;" 
            +"uniform mat4 u_projection_matrix_RMB;" 

            +"uniform vec3 u_Ka_RMB;" 
            +"uniform vec3 u_Kd_RMB;" 
            +"uniform vec3 u_Ks_RMB;" 

            +"uniform float u_materialShininess_RMB;" 
            +"uniform int u_lKeyIsPressed_RMB;"

            +"uniform vec3 u_La_Red_RMB;" 
            +"uniform vec3 u_Ld_Red_RMB;" 
        
            +"uniform vec4 u_Light_Position_Red_RMB;" 

            +"out vec3 phong_AdsLight_RMB;" 

            +"void main(void)" 
            +"{" 
            +   "if(u_lKeyIsPressed_RMB == 1)" 
            +   "{" 
            +       "vec4 eye_coordinate_RMB = u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" 

            +       "vec3 tnorm_RMB = normalize(mat3(u_viewMatrix_RMB * u_modelMatrix_RMB) * vertexNormal_RMB);" 

            +       "vec3 viewerVector_RMB = normalize(vec3(-eye_coordinate_RMB.xyz));" 

                    //For Red Color Lighting
            +       "vec3 lightdirection_Red_RMB = normalize(vec3(u_Light_Position_Red_RMB - eye_coordinate_RMB));" 

            +       "float tn_dot_lightDir_Red_RMB = max(dot(lightdirection_Red_RMB, tnorm_RMB), 0.0);" 

            +       "vec3 reflectionVector_Red_RMB = reflect(-lightdirection_Red_RMB, tnorm_RMB);" 

            +       "vec3 ambient_Red_RMB = u_La_Red_RMB * u_Ka_RMB;" 

            +       "vec3 diffuse_Red_RMB = u_Ld_Red_RMB * u_Kd_RMB * tn_dot_lightDir_Red_RMB;" 

            +       "vec3 specular_Red_RMB = u_Ks_RMB * pow(max(dot(reflectionVector_Red_RMB, viewerVector_RMB), 0.0), u_materialShininess_RMB);" 

                    
            +       "phong_AdsLight_RMB = ambient_Red_RMB + diffuse_Red_RMB + specular_Red_RMB;" 

            +   "}" 
            +   "else" 
            +   "{" 
            +           "phong_AdsLight_RMB = vec3(1.0, 1.0, 1.0);" 
            +   "}" 
            +   "gl_Position = u_projection_matrix_RMB * u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" 
            +"}"

        );

        //Specifing above source to the vertex shader object
        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

        //Compile the Vertex Shader
        GLES32.glCompileShader(vertexShaderObject);

        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);

                System.out.println("RTR: vertexShaderInfoLog" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        //Define Fragment Shader Object
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        //Write Fragment Shader Code
        final String fragmentShaderSourceCode = String.format
        (
            "#version 320 es" 
            +"\n" 
            +"precision highp float;" 

            +"in vec3 phong_AdsLight_RMB;" 
            +"out vec4 fragColor;" 
            +"void main(void)" 
            +"{" 
            +   "fragColor = vec4(phong_AdsLight_RMB, 1.0);" 
            +"}"
        );

        // Specifing above Source to the fragment Shader Object
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
       
        //Compile the fragment Shader Object
        GLES32.glCompileShader(fragmentShaderObject);

        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);

                System.out.println("RTR: FragmentShaderInfoLog" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        shaderProgramObject_PV = GLES32.glCreateProgram();
        GLES32.glAttachShader(shaderProgramObject_PV, vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject_PV, fragmentShaderObject);

        //pre Link Binding to attributes
        GLES32.glBindAttribLocation(shaderProgramObject_PV, GLESMacros.AMC_ATTRIBUTE_POSITION, "vertexPosition_RMB");
        GLES32.glBindAttribLocation(shaderProgramObject_PV, GLESMacros.AMC_ATTRIBUTE_NORMAL, "vertexNormal_RMB");
        //Link the Shader Program
        GLES32.glLinkProgram(shaderProgramObject_PV);

        int[] iProgramLinkStatus  = new int[1];
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetProgramiv(shaderProgramObject_PV, GLES32.GL_LINK_STATUS, iProgramLinkStatus,0);
        if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject_PV, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                GLES32.glGetProgramInfoLog(shaderProgramObject_PV);

                System.out.println("RTR: ProgramLinkStatus" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        //Post Linking Retriving Uniform Location
        modelUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_modelMatrix_RMB");
        viewUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_viewMatrix_RMB");
        projectionUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_projection_matrix_RMB");

        kaUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_Ka_RMB");
        kdUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_Kd_RMB");
        ksUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_Ks_RMB");

        lKeyIsPressedUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_lKeyIsPressed_RMB");
        materialShininessUniform_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_materialShininess_RMB");
        //For Red Color Lighting
        laUniform_Red_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_La_Red_RMB");
        ldUniform_Red_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_Ld_Red_RMB");
        lightPositionUniform_Red_RMB = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_Light_Position_Red_RMB");
    }

    private void perFragmentShaderFunc()
    {
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
        final String vertexShaderSourceCode = String.format 
        (
            "#version 320 es" 
            +"\n" 
            +"in vec4 vertexPosition;" 
            +"in vec3 vertexNormal;" 

            +"uniform mat4 u_modelMatrix;" 
            +"uniform mat4 u_viewMatrix;" 
            +"uniform mat4 u_projection_matrix;" 

            +"uniform mediump int u_lKeyIsPressed;" 
            +"uniform vec4 u_Light_Position_Red;" 

            +"out vec3 lightdirection_Red;" 
           
            +"out vec3 tnorm;"
            +"out vec3 viewerVector;"

            +"void main(void)" 
            +"{" 
            +   "if(u_lKeyIsPressed == 1)" 
            +   "{" 
            +       "vec4 eye_coordinate = u_viewMatrix * u_modelMatrix * vertexPosition;" 

            +       "tnorm = mat3(u_viewMatrix * u_modelMatrix) * vertexNormal;" 

            +       "viewerVector = -eye_coordinate.xyz;" 
                    // For Red Color Lighting
            +       "lightdirection_Red = vec3(u_Light_Position_Red) - eye_coordinate.xyz;" 
            +   "}"

            +   "gl_Position = u_projection_matrix * u_viewMatrix * u_modelMatrix * vertexPosition;" 
            +"}"

        );

        //Specifing above source to the vertex shader object
        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

        //Compile the Vertex Shader
        GLES32.glCompileShader(vertexShaderObject);

        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
        System.out.println("RTR:- iShaderCompileStatus " + iShaderCompileStatus[0]);
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);

                System.out.println("RTR: vertexShaderInfoLog" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        //Define Fragment Shader Object
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        //Write Fragment Shader Code
        final String fragmentShaderSourceCode = String.format
        (
            "#version 320 es" 
            +"\n" 
            +"precision highp float;" 

            +"in vec3 lightdirection_Red;" 
            +"in vec3 tnorm;"
            +"in vec3 viewerVector;"

            +"uniform vec3 u_Ka;" 
            +"uniform vec3 u_Kd;" 
            +"uniform vec3 u_Ks;" 

            +"uniform float u_materialShininess;"  
            +"uniform int u_lKeyIsPressed;"

            +"uniform vec3 u_La_Red;" 
            +"uniform vec3 u_Ld_Red;" 

            +"out vec4 fragColor;" 

            +"void main(void)" 
            +"{" 

            +   "vec3 phong_AdsLight;"
            
            +   "if(u_lKeyIsPressed == 1)" 
            +   "{"
            +       "vec3 normalize_tnorm = normalize(tnorm);"

            +       "vec3 normalize_viewerVector = normalize(viewerVector);"

                    //For Red Color Lighting
            +       "vec3 normalize_lightdirection_Red = normalize(lightdirection_Red);"

            +       "float tn_dot_lightDir_Red = max(dot(normalize_tnorm, normalize_lightdirection_Red), 0.0);" 

            +       "vec3 reflectionVector_Red = reflect(-normalize_lightdirection_Red, normalize_tnorm);" 

            +       "vec3 ambient_Red = u_La_Red * u_Ka;" 

            +       "vec3 diffuse_Red = u_Ld_Red * u_Kd * tn_dot_lightDir_Red;" 

            +       "vec3 specular_Red = u_Ks * pow(max(dot(reflectionVector_Red, normalize_viewerVector), 0.0), u_materialShininess);" 
                    
                    
            +       "phong_AdsLight = ambient_Red + diffuse_Red + specular_Red;" 

            +   "}" 
            +   "else" 
            +   "{" 
            +           "phong_AdsLight = vec3(1.0, 1.0, 1.0);" 
            +   "}" 
            +   "fragColor = vec4(phong_AdsLight, 1.0);" 
            +"}"
        );

        // Specifing above Source to the fragment Shader Object
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
       
        //Compile the fragment Shader Object
        GLES32.glCompileShader(fragmentShaderObject);

        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);

                System.out.println("RTR: FragmentShaderInfoLog" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        shaderProgramObject = GLES32.glCreateProgram();
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        //pre Link Binding to attributes
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vertexPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_NORMAL, "vertexNormal");
        //Link the Shader Program
        GLES32.glLinkProgram(shaderProgramObject);

        int[] iProgramLinkStatus  = new int[1];
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus,0);
        if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                GLES32.glGetProgramInfoLog(shaderProgramObject);

                System.out.println("RTR: ProgramLinkStatus" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        //Post Linking Retriving Uniform Location
        modelUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_modelMatrix");
        viewUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
        projectionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

        kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ka");
        kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
        ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ks");
        
        lKeyIsPressedUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");
        materialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_materialShininess");
        //For Red Color lighting
        laUniform_Red = GLES32.glGetUniformLocation(shaderProgramObject, "u_La_Red");
        ldUniform_Red = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld_Red");
        lightPositionUniform_Red = GLES32.glGetUniformLocation(shaderProgramObject, "u_Light_Position_Red");
    }

    private void Resize_RMB(int iWidth, int iHeight)
    {

        if (iHeight == 0)
		{
			iHeight = 1;
		}

		GLES32.glViewport(0, 0, iWidth, iHeight);
        Matrix.perspectiveM(projectionMatrix, 0, 45.0f, (float)iWidth / (float)iHeight, 0.1f, 100.0f);
    }

    private void Display_RMB()
    {
        Light_Array();
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        for(int i = 0; i < 24; i++)
        {
            GLES32.glViewport((i % 6) * giWinWidth / 6, giWinHeight - (i / 6 + 1) * giWinHeight / 4, (int)giWinWidth / 6, (int)giWinHeight / 4);

            Matrix.perspectiveM(projectionMatrix,0, 45.0f, (float)(giWinWidth / 6) / (float)(giWinHeight / 4), 0.1f, 100.0f);

            System.out.println("RTR:->Width" +giWinWidth);
            System.out.println("RTR:->Height" +giWinHeight);

            if(fragmentShader == false)
            {
                GLES32.glUseProgram(shaderProgramObject_PV);
                if (gbLight == true)
                {
                    GLES32.glUniform1i(lKeyIsPressedUniform_RMB, 1);
                    //For Red Color lighting
                    GLES32.glUniform3fv(laUniform_Red_RMB, 1, lightAmbient_Red,0); //Red Light
                    GLES32.glUniform3fv(ldUniform_Red_RMB, 1, lightDiffuse_Red,0);
                    GLES32.glUniform4fv(lightPositionUniform_Red_RMB, 1, lightPosition_Red,0);

                    GLES32.glUniform3fv(kaUniform_RMB, 1, materialAmbient[i],0); //Gray Material
                    GLES32.glUniform3fv(kdUniform_RMB, 1, materialDiffuse[i],0);
                    GLES32.glUniform3fv(ksUniform_RMB, 1, materialSpecular[i],0);

                    GLES32.glUniform1fv(materialShininessUniform_RMB, 1, materialShininess[i],0);
                }
                else
                {
                    GLES32.glUniform1i(lKeyIsPressedUniform_RMB, 0);
                }
            }
            else 
            {
                GLES32.glUseProgram(shaderProgramObject);

                if (gbLight == true)
                {
                    GLES32.glUniform1i(lKeyIsPressedUniform, 1);
                    //For Red Color lighting
                    GLES32.glUniform3fv(laUniform_Red, 1, lightAmbient_Red,0); //Red Light
                    GLES32.glUniform3fv(ldUniform_Red, 1, lightDiffuse_Red,0);
                    GLES32.glUniform4fv(lightPositionUniform_Red, 1, lightPosition_Red,0);
                    
                    GLES32.glUniform3fv(kaUniform, 1, materialAmbient[i],0); //Gray Material
                    GLES32.glUniform3fv(kdUniform, 1, materialDiffuse[i],0);
                    GLES32.glUniform3fv(ksUniform, 1, materialSpecular[i],0);

                    GLES32.glUniform1fv(materialShininessUniform, 1, materialShininess[i],0);
                }
                else
                {
                    GLES32.glUniform1i(lKeyIsPressedUniform, 0);
                }
            }
            
            Matrix.setIdentityM(modelMatrix, 0);
            Matrix.setIdentityM(viewMatrix, 0);

            Matrix.translateM(modelMatrix,0, 0.0f, 0.0f, -1.8f);

            GLES32.glUniformMatrix4fv(modelUniform_RMB, 1, false, modelMatrix,0);
    		GLES32.glUniformMatrix4fv(viewUniform_RMB, 1, false, viewMatrix,0);
    		GLES32.glUniformMatrix4fv(projectionUniform_RMB, 1, false, projectionMatrix,0);

            GLES32.glUniformMatrix4fv(modelUniform, 1, false, modelMatrix,0);
            GLES32.glUniformMatrix4fv(viewUniform, 1, false, viewMatrix,0);
            GLES32.glUniformMatrix4fv(projectionUniform, 1, false, projectionMatrix,0);

            //bind vao
            GLES32.glBindVertexArray(vao_Sphere[0]);

            GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_Sphere_Element[0]);
            GLES32.glDrawElements(GLES32.GL_TRIANGLES, gNumElements, GLES32.GL_UNSIGNED_SHORT, 0);

            // unbind vao
            GLES32.glBindVertexArray(0);
        }

        GLES32.glUseProgram(0);
       
        requestRender();   //SwapBuffers();
    }

    private void Light_Array()
    {
        //********** For Red Color Lighting Array ********
        lightAmbient_Red[0] = 0.0f;
        lightAmbient_Red[1] = 0.0f;
        lightAmbient_Red[2] = 0.0f;
        lightAmbient_Red[3] = 1.0f;

        lightDiffuse_Red[0] = 1.0f;
        lightDiffuse_Red[1] = 1.0f;
        lightDiffuse_Red[2] = 1.0f;
        lightDiffuse_Red[3] = 1.0f;

        if(keyIncrement == 1)
        {
            lightPosition_Red[0] = co1;
            lightPosition_Red[1] = 0.0f;
            lightPosition_Red[2] = co2;
            lightPosition_Red[3] = 0.0f;
        }
        else if(keyIncrement == 2)
        {
            lightPosition_Red[0] = co2;
            lightPosition_Red[1] = co1;
            lightPosition_Red[2] = 0.0f;
            lightPosition_Red[3] = 0.0f;
        }
        else
        {
            lightPosition_Red[0] = 0.0f;
            lightPosition_Red[1] = co1;
            lightPosition_Red[2] = co2;
            lightPosition_Red[3] = 0.0f;
        }

        // *** 1st Sphere On 1st Column, Emerald ****
        materialAmbient[0][0] = 0.0215f;
        materialAmbient[0][1] = 0.1745f;
        materialAmbient[0][2] = 0.0215f;
        materialAmbient[0][3] = 1.0f;

        materialDiffuse[0][0] = 0.07568f;
        materialDiffuse[0][1] = 0.61424f;
        materialDiffuse[0][2] = 0.07568f;
        materialDiffuse[0][3] = 1.0f;

        materialSpecular[0][0] = 0.633f;
        materialSpecular[0][1] = 0.727811f;
        materialSpecular[0][2] = 0.633f;
        materialSpecular[0][3] = 1.0f;

        materialShininess[0][0] = 0.6f * 128.0f;

        // *** 2nd Sphere On 1st Column, Jade ****
        materialAmbient[1][0] = 0.135f;
        materialAmbient[1][1] = 0.2225f;
        materialAmbient[1][2] = 0.1575f;
        materialAmbient[1][3] = 1.0f;

        materialDiffuse[1][0] = 0.54f;
        materialDiffuse[1][1] = 0.89f;
        materialDiffuse[1][2] = 0.63f;
        materialDiffuse[1][3] = 1.0f;

        materialSpecular[1][0] = 0.316228f;
        materialSpecular[1][1] = 0.316228f;
        materialSpecular[1][2] = 0.316228f;
        materialSpecular[1][3] = 1.0f;

        materialShininess[1][0] = 0.1f * 128.0f;

        // *** 3rd Sphere On 1st Column, Obsidian ****
        materialAmbient[2][0] = 0.05375f;
        materialAmbient[2][1] = 0.05f;
        materialAmbient[2][2] = 0.06625f;
        materialAmbient[2][3] = 1.0f;

        materialDiffuse[2][0] = 0.18275f;
        materialDiffuse[2][1] = 0.17f;
        materialDiffuse[2][2] = 0.22525f;
        materialDiffuse[2][3] = 1.0f;

        materialSpecular[2][0] = 0.332741f;
        materialSpecular[2][1] = 0.328634f;
        materialSpecular[2][2] = 0.346435f;
        materialSpecular[2][3] = 1.0f;

        materialShininess[2][0] = 0.3f * 128.0f;

        // *** 4th Sphere On 1st Column, Pearl ****
        materialAmbient[3][0] = 0.25f;
        materialAmbient[3][1] = 0.20725f;
        materialAmbient[3][2] = 0.20725f;
        materialAmbient[3][3] = 1.0f;

        materialDiffuse[3][0] = 1.0f;
        materialDiffuse[3][1] = 0.829f;
        materialDiffuse[3][2] = 0.829f;
        materialDiffuse[3][3] = 1.0f;

        materialSpecular[3][0] = 0.296648f;
        materialSpecular[3][1] = 0.296648f;
        materialSpecular[3][2] = 0.296648f;
        materialSpecular[3][3] = 1.0f;

        materialShininess[3][0] = 0.088f * 128.0f;

        // *** 5th Sphere On 1st Column, Ruby ****
        materialAmbient[4][0] = 0.1745f;
        materialAmbient[4][1] = 0.01175f;
        materialAmbient[4][2] = 0.01175f;
        materialAmbient[4][3] = 1.0f;

        materialDiffuse[4][0] = 0.61424f;
        materialDiffuse[4][1] = 0.04136f;
        materialDiffuse[4][2] = 0.04136f;
        materialDiffuse[4][3] = 1.0f;

        materialSpecular[4][0] = 0.727811f;
        materialSpecular[4][1] = 0.686959f;
        materialSpecular[4][2] = 0.626959f;
        materialSpecular[4][3] = 1.0f;

        materialShininess[4][0] = 0.6f * 128.0f;

        // *** 6th Sphere On 1st Column, Tarquoise ****
        materialAmbient[5][0] = 0.1f;
        materialAmbient[5][1] = 0.18725f;
        materialAmbient[5][2] = 0.1745f;
        materialAmbient[5][3] = 1.0f;

        materialDiffuse[5][0] = 0.396f;
        materialDiffuse[5][1] = 0.74151f;
        materialDiffuse[5][2] = 0.69102f;
        materialDiffuse[5][3] = 1.0f;

        materialSpecular[5][0] = 0.297254f;
        materialSpecular[5][1] = 0.30829f;
        materialSpecular[5][2] = 0.306678f;
        materialSpecular[5][3] = 1.0f;

        materialShininess[5][0] = 0.1f * 128.0f;

        // *** 1st Sphere On 2nd Column, Brass ****
        materialAmbient[6][0] = 0.329412f;
        materialAmbient[6][1] = 0.223529f;
        materialAmbient[6][2] = 0.027451f;
        materialAmbient[6][3] = 1.0f;

        materialDiffuse[6][0] = 0.780392f;
        materialDiffuse[6][1] = 0.568627f;
        materialDiffuse[6][2] = 0.113725f;
        materialDiffuse[6][3] = 1.0f;

        materialSpecular[6][0] = 0.992157f;
        materialSpecular[6][1] = 0.941176f;
        materialSpecular[6][2] = 0.807843f;
        materialSpecular[6][3] = 1.0f;

        materialShininess[6][0] = 0.21794872f * 128.0f;

        // *** 2nd Sphere On 2nd Column, Bronze ****
        materialAmbient[7][0] = 0.2125f;
        materialAmbient[7][1] = 0.1275f;
        materialAmbient[7][2] = 0.054f;
        materialAmbient[7][3] = 1.0f;

        materialDiffuse[7][0] = 0.714f;
        materialDiffuse[7][1] = 0.4284f;
        materialDiffuse[7][2] = 0.18144f;
        materialDiffuse[7][3] = 1.0f;

        materialSpecular[7][0] = 0.393548f;
        materialSpecular[7][1] = 0.271906f;
        materialSpecular[7][2] = 0.166721f;
        materialSpecular[7][3] = 1.0f;

        materialShininess[7][0] = 0.2f * 128.0f;

        // *** 3rd Sphere On 2nd Column, Chrome ****
        materialAmbient[8][0] = 0.25f;
        materialAmbient[8][1] = 0.25f;
        materialAmbient[8][2] = 0.25f;
        materialAmbient[8][3] = 1.0f;

        materialDiffuse[8][0] = 0.4f;
        materialDiffuse[8][1] = 0.4f;
        materialDiffuse[8][2] = 0.4f;
        materialDiffuse[8][3] = 1.0f;

        materialSpecular[8][0] = 0.774597f;
        materialSpecular[8][1] = 0.774597f;
        materialSpecular[8][2] = 0.774597f;
        materialSpecular[8][3] = 1.0f;

        materialShininess[8][0] = 0.6f * 128.0f;

        // *** 4th Sphere On 2nd Column, Copper ****
        materialAmbient[9][0] = 0.19125f;
        materialAmbient[9][1] = 0.0735f;
        materialAmbient[9][2] = 0.0225f;
        materialAmbient[9][3] = 1.0f;

        materialDiffuse[9][0] = 0.7038f;
        materialDiffuse[9][1] = 0.27048f;
        materialDiffuse[9][2] = 0.0828f;
        materialDiffuse[9][3] = 1.0f;

        materialSpecular[9][0] = 0.256777f;
        materialSpecular[9][1] = 0.137622f;
        materialSpecular[9][2] = 0.086014f;
        materialSpecular[9][3] = 1.0f;

        materialShininess[9][0] = 0.1f * 128.0f;

        // *** 5th Sphere On 2nd Column, Gold ****
        materialAmbient[10][0] = 0.24725f;
        materialAmbient[10][1] = 0.1995f;
        materialAmbient[10][2] = 0.0745f;
        materialAmbient[10][3] = 1.0f;

        materialDiffuse[10][0] = 0.75164f;
        materialDiffuse[10][1] = 0.60648f;
        materialDiffuse[10][2] = 0.22648f;
        materialDiffuse[10][3] = 1.0f;

        materialSpecular[10][0] = 0.628281f;
        materialSpecular[10][1] = 0.555802f;
        materialSpecular[10][2] = 0.366065f;
        materialSpecular[10][3] = 1.0f;

        materialShininess[10][0] = 0.4f * 128.0f;

        // *** 6th Sphere On 2nd Column, Silver ****
        materialAmbient[11][0] = 0.19225f;
        materialAmbient[11][1] = 0.19225f;
        materialAmbient[11][2] = 0.19225f;
        materialAmbient[11][3] = 1.0f;

        materialDiffuse[11][0] = 0.50754f;
        materialDiffuse[11][1] = 0.50754f;
        materialDiffuse[11][2] = 0.50754f;
        materialDiffuse[11][3] = 1.0f;

        materialSpecular[11][0] = 0.508273f;
        materialSpecular[11][1] = 0.508273f;
        materialSpecular[11][2] = 0.508273f;
        materialSpecular[11][3] = 1.0f;

        materialShininess[11][0] = 0.4f * 128.0f;

        // *** 1st Sphere On 3rd Column, Black ****
        materialAmbient[12][0] = 0.0f;
        materialAmbient[12][1] = 0.0f;
        materialAmbient[12][2] = 0.0f;
        materialAmbient[12][3] = 1.0f;

        materialDiffuse[12][0] = 0.01f;
        materialDiffuse[12][1] = 0.01f;
        materialDiffuse[12][2] = 0.01f;
        materialDiffuse[12][3] = 1.0f;

        materialSpecular[12][0] = 0.50f;
        materialSpecular[12][1] = 0.50f;
        materialSpecular[12][2] = 0.50f;
        materialSpecular[12][3] = 1.0f;

        materialShininess[12][0] = 0.25f * 128.0f;

        // *** 2nd Sphere On 3rd Column, Cyan ****
        materialAmbient[13][0] = 0.0f;
        materialAmbient[13][1] = 0.1f;
        materialAmbient[13][2] = 0.06f;
        materialAmbient[13][3] = 1.0f;

        materialDiffuse[13][0] = 0.0f;
        materialDiffuse[13][1] = 0.50980392f;
        materialDiffuse[13][2] = 0.50980392f;
        materialDiffuse[13][3] = 1.0f;

        materialSpecular[13][0] = 0.50196078f;
        materialSpecular[13][1] = 0.50196078f;
        materialSpecular[13][2] = 0.50196078f;
        materialSpecular[13][3] = 1.0f;

        materialShininess[13][0] = 0.25f * 128.0f;

        // *** 3rd Sphere On 3rd Column, Green ****
        materialAmbient[14][0] = 0.0f;
        materialAmbient[14][1] = 0.0f;
        materialAmbient[14][2] = 0.0f;
        materialAmbient[14][3] = 1.0f;

        materialDiffuse[14][0] = 0.1f;
        materialDiffuse[14][1] = 0.35f;
        materialDiffuse[14][2] = 0.1f;
        materialDiffuse[14][3] = 1.0f;

        materialSpecular[14][0] = 0.45f;
        materialSpecular[14][1] = 0.45f;
        materialSpecular[14][2] = 0.45f;
        materialSpecular[14][3] = 1.0f;

        materialShininess[14][0] = 0.25f * 128.0f;

        // *** 4th Sphere On 3rd Column, Red ****
        materialAmbient[15][0] = 0.0f;
        materialAmbient[15][1] = 0.0f;
        materialAmbient[15][2] = 0.0f;
        materialAmbient[15][3] = 1.0f;

        materialDiffuse[15][0] = 0.5f;
        materialDiffuse[15][1] = 0.0f;
        materialDiffuse[15][2] = 0.0f;
        materialDiffuse[15][3] = 1.0f;

        materialSpecular[15][0] = 0.7f;
        materialSpecular[15][1] = 0.6f;
        materialSpecular[15][2] = 0.6f;
        materialSpecular[15][3] = 1.0f;

        materialShininess[15][0] = 0.25f * 128.0f;

        // *** 5th Sphere On 3rd Column, White ****
        materialAmbient[16][0] = 0.0f;
        materialAmbient[16][1] = 0.0f;
        materialAmbient[16][2] = 0.0f;
        materialAmbient[16][3] = 1.0f;

        materialDiffuse[16][0] = 0.55f;
        materialDiffuse[16][1] = 0.55f;
        materialDiffuse[16][2] = 0.55f;
        materialDiffuse[16][3] = 1.0f;

        materialSpecular[16][0] = 0.70f;
        materialSpecular[16][1] = 0.70f;
        materialSpecular[16][2] = 0.70f;
        materialSpecular[16][3] = 1.0f;

        materialShininess[16][0] = 0.25f * 128.0f;

        // *** 6th Sphere On 3rd Column, Yello ****
        materialAmbient[17][0] = 0.0f;
        materialAmbient[17][1] = 0.0f;
        materialAmbient[17][2] = 0.0f;
        materialAmbient[17][3] = 1.0f;

        materialDiffuse[17][0] = 0.5f;
        materialDiffuse[17][1] = 0.5f;
        materialDiffuse[17][2] = 0.0f;
        materialDiffuse[17][3] = 1.0f;

        materialSpecular[17][0] = 0.60f;
        materialSpecular[17][1] = 0.60f;
        materialSpecular[17][2] = 0.50f;
        materialSpecular[17][3] = 1.0f;

        materialShininess[17][0] = 0.25f * 128.0f;

        // *** 1st Sphere On 4th Column, Black ****
        materialAmbient[18][0] = 0.02f;
        materialAmbient[18][1] = 0.02f;
        materialAmbient[18][2] = 0.02f;
        materialAmbient[18][3] = 1.0f;

        materialDiffuse[18][0] = 0.01f;
        materialDiffuse[18][1] = 0.01f;
        materialDiffuse[18][2] = 0.01f;
        materialDiffuse[18][3] = 1.0f;

        materialSpecular[18][0] = 0.4f;
        materialSpecular[18][1] = 0.4f;
        materialSpecular[18][2] = 0.4f;
        materialSpecular[18][3] = 1.0f;

        materialShininess[18][0] = 0.078125f * 128.0f;


        // *** 2nd Sphere On 4th Column, Cyan ****
        materialAmbient[19][0] = 0.0f;
        materialAmbient[19][1] = 0.05f;
        materialAmbient[19][2] = 0.05f;
        materialAmbient[19][3] = 1.0f;

        materialDiffuse[19][0] = 0.4f;
        materialDiffuse[19][1] = 0.5f;
        materialDiffuse[19][2] = 0.5f;
        materialDiffuse[19][3] = 1.0f;

        materialSpecular[19][0] = 0.04f;
        materialSpecular[19][1] = 0.7f;
        materialSpecular[19][2] = 0.7f;
        materialSpecular[19][3] = 1.0f;

        materialShininess[19][0] = 0.078125f * 128.0f;

        // *** 3rd Sphere On 4th Column, Green ****
        materialAmbient[20][0] = 0.0f;
        materialAmbient[20][1] = 0.05f;
        materialAmbient[20][2] = 0.0f;
        materialAmbient[20][3] = 1.0f;

        materialDiffuse[20][0] = 0.4f;
        materialDiffuse[20][1] = 0.5f;
        materialDiffuse[20][2] = 0.4f;
        materialDiffuse[20][3] = 1.0f;

        materialSpecular[20][0] = 0.04f;
        materialSpecular[20][1] = 0.7f;
        materialSpecular[20][2] = 0.04f;
        materialSpecular[20][3] = 1.0f;

        materialShininess[20][0] = 0.078125f * 128.0f;

        // *** 4th Sphere On 4th Column, Red ****
        materialAmbient[21][0] = 0.05f;
        materialAmbient[21][1] = 0.0f;
        materialAmbient[21][2] = 0.0f;
        materialAmbient[21][3] = 1.0f;

        materialDiffuse[21][0] = 0.5f;
        materialDiffuse[21][1] = 0.4f;
        materialDiffuse[21][2] = 0.4f;
        materialDiffuse[21][3] = 1.0f;

        materialSpecular[21][0] = 0.7f;
        materialSpecular[21][1] = 0.04f;
        materialSpecular[21][2] = 0.04f;
        materialSpecular[21][3] = 1.0f;

        materialShininess[21][0] = 0.078125f * 128.0f;

        // *** 5th Sphere On 4th Column, White ****
        materialAmbient[22][0] = 0.05f;
        materialAmbient[22][1] = 0.05f;
        materialAmbient[22][2] = 0.05f;
        materialAmbient[22][3] = 1.0f;

        materialDiffuse[22][0] = 0.5f;
        materialDiffuse[22][1] = 0.5f;
        materialDiffuse[22][2] = 0.5f;
        materialDiffuse[22][3] = 1.0f;

        materialSpecular[22][0] = 0.7f;
        materialSpecular[22][1] = 0.7f;
        materialSpecular[22][2] = 0.7f;
        materialSpecular[22][3] = 1.0f;

        materialShininess[22][0] = 0.078125f * 128.0f;

        // *** 6th Sphere On 4th Column, Yello ****
        materialAmbient[23][0] = 0.05f;
        materialAmbient[23][1] = 0.05f;
        materialAmbient[23][2] = 0.0f;
        materialAmbient[23][3] = 1.0f;

        materialDiffuse[23][0] = 0.5f;
        materialDiffuse[23][1] = 0.5f;
        materialDiffuse[23][2] = 0.4f;
        materialDiffuse[23][3] = 1.0f;

        materialSpecular[23][0] = 0.7f;
        materialSpecular[23][1] = 0.7f;
        materialSpecular[23][2] = 0.04f;
        materialSpecular[23][3] = 1.0f;

        materialShininess[23][0] = 0.078125f * 128.0f;
    }

    private void Update_RMB()
    {
        angleR = angleR + 0.01f;
        if (angleR > 360.0f)
        {
            angleR = 0.0f;
        }
        
        co1 = (float)Math.cos(angleR) * 100.0f;
        co2 = (float)Math.sin(angleR) * 100.0f;
    }

    private void Uninitialize_RMB()
    {
        if(vbo_Sphere_Normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Sphere_Normal,0);
            vbo_Sphere_Normal[0] = 0;
        }
        if(vbo_Sphere_Position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Sphere_Position,0);
            vbo_Sphere_Position[0] = 0;
        }
        if(vbo_Sphere_Element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Sphere_Element,0);
            vbo_Sphere_Element[0] = 0;
        }
        if(vao_Sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_Sphere,0);
            vao_Sphere[0] = 0;
        }


    	if(shaderProgramObject_PV != 0)
    	{
    		int[] shaderCount = new int[1];
    		int shaderNumber;

    		GLES32.glUseProgram(shaderProgramObject_PV);
    		GLES32.glGetProgramiv(shaderProgramObject_PV, GLES32.GL_ATTACHED_SHADERS, shaderCount,0);

    		int[] shaders = new int[shaderCount[0]];
            if(shaders[0] != 0)
            {
                GLES32.glGetAttachedShaders(shaderProgramObject_PV, shaderCount[0], shaderCount,0, shaders,0);
                for(shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
                {
                   GLES32.glDetachShader(shaderProgramObject_PV, shaders[shaderNumber]);
                   GLES32.glDeleteShader(shaders[shaderNumber]);
                   shaders[shaderNumber] = 0;
                }
            }
            
    	}
    	GLES32.glUseProgram(0);
    	GLES32.glDeleteProgram(shaderProgramObject_PV);
    	shaderProgramObject_PV = 0;

        if(shaderProgramObject != 0)
        {
            int[] shaderCount = new int[1];
            int shaderNumber;

            GLES32.glUseProgram(shaderProgramObject);
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount,0);

            int[] shaders = new int[shaderCount[0]];
            if(shaders[0] != 0)
            {
                GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
                for(shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
                {
                   GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
                   GLES32.glDeleteShader(shaders[shaderNumber]);
                   shaders[shaderNumber] = 0;
                }
            }
            
        }
        GLES32.glUseProgram(0);
        GLES32.glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
    }
}



