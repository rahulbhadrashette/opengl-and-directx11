package com.astromedicomp.robaticarm;
//added by me
import android.content.Context;
import android.view.Gravity;
import android.graphics.Color; // for "Color" class
import android.view.MotionEvent; // for "MotionEvent" class
import android.view.GestureDetector; // for "GestureDetector"
import android.view.GestureDetector.OnGestureListener; //for "OnGestureListener"  
import android.view.GestureDetector.OnDoubleTapListener; // for "OnDoubleTapListener" 

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import java.nio.ByteBuffer;		//for OpenGL Buffers
import java.nio.ByteOrder;      //for OpenGL Buffers
import java.nio.FloatBuffer;	//for OpenGL Buffers
import java.lang.Math; 

import android.opengl.Matrix;   //for matrix math
import java.nio.ShortBuffer;    //For Sphere

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int ldUniform;
    private int kdUniform;

    private int gNumVertices;
    private int gNumElements;

    private int lightPositionUniform;
    private int lKeyIsPressedUniform;

    private int modelUniform;
    private int viewUniform;
    private int projectionUniform;

    private int[] vao_Sphere = new int[1];
    private int[] vbo_Sphere_Position = new int[1];
    private int[] vbo_Sphere_Normal = new int[1];
    private int[] vbo_Sphere_Element = new int[1];


 	private float[] modelMatrix = new float[16];
    private float[] viewMatrix = new float[16];
    private float[] projectionMatrix = new float[16];  // 4 x 4  Matrix

    int gElbow = 0;
    int gSholder = 0;

    boolean gbLight = false;
    boolean swith_e_or_s = false;

	public GLESView(Context dwContext)
	{
		super(dwContext);

        context = dwContext;

		setEGLContextClientVersion(3); //version 3.x pahije manun
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(dwContext, this, null, false);  //this means 'handler' i.e who is going to handle
		gestureDetector.setOnDoubleTapListener(this);   // this means 'handler' i.e. who is going to handle
    
	}
	// Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
    	//code
    	int eventaction = event.getAction();
    	if(!gestureDetector.onTouchEvent(event))
    		super.onTouchEvent(event);
    	return(true);
    }

     // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        if(gbLight == false)
        {
            gbLight = true;
        }
        else
        {
            gbLight = false;
        }
    	return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
    	// Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
        swith_e_or_s = !swith_e_or_s;
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
    	Uninitialize_RMB();
    	System.exit(0);
    	return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        if( swith_e_or_s == true)
        {
            gSholder = gSholder + 4;
        }
        else
        {
             gElbow = gElbow + 4;
        }
        
    	return(true);
    }

    //Impliment GLSurfaceView.Renderer Mathods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig congif)  //WM_CREATE OR Initialize
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR:" +version);
        String version1 = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR:" +version1);
        initialize_RMB();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int Width, int Height)  //WM_SIZE or RESIZE
    {
        Resize_RMB(Width, Height);
    }

    @Override
    public void onDrawFrame(GL10 unused)  //Display
    {
        //Update_RMB();
        Display_RMB();
        System.out.println("RTR: onDrawFrame");
    }

    //Our Custome Methods
    private void initialize_RMB()
    {
    	vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
        final String vertexShaderSourceCode = String.format 
        (
            "#version 320 es" +
            "\n" +
            "in vec4 vPosition;" +
            
            "in vec3 vNormal;" +
            "uniform mat4 u_model_matrix;" +
            "uniform mat4 u_view_matrix;" +
            "uniform mat4 u_projection_matrix;" +
            "uniform bool u_lKeyIsPressed;" +
            "uniform vec3 u_Ld;" +
            "uniform vec3 u_Kd;" +
            "uniform vec4 u_Light_Position;" +
            "out vec3 diffuse_Color;" +

            "void main(void)" +
            "{" +

                "if(u_lKeyIsPressed)" +
                "{" +
                    "vec4 eye_coordinate = u_view_matrix * u_model_matrix * vPosition;" +
                    "mat3 normalmatrix = mat3(transpose(inverse(u_view_matrix * u_model_matrix)));" +
                    "vec3 tnorm = normalize(normalmatrix * vNormal);" +
                     "vec3 source = normalize(vec3(u_Light_Position) - eye_coordinate.xyz);" +
                    "diffuse_Color = u_Ld * u_Kd * dot(source, tnorm);" +
                "}" +
                "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix* vPosition;" +

            "}"

        );

        //Specifing above source to the vertex shader object
        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

        //Compile the Vertex Shader
        GLES32.glCompileShader(vertexShaderObject);

        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);

                System.out.println("RTR: vertexShaderInfoLog" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        //Define Fragment Shader Object
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        //Write Fragment Shader Code
        final String fragmentShaderSourceCode = String.format
        (
            "#version 320 es" +
            "\n" +
            "precision highp float;" +

            "in vec3 diffuse_Color;" +
            "out vec4 fragColor;" +
            "uniform bool u_lKeyIsPressed;" +

            "void main(void)" +
            "{" +
                
                "if(u_lKeyIsPressed)" +
                "{" +
                    "fragColor = vec4(diffuse_Color, 1.0);" +
                "}" +
                "else" +
                "{" +
                    "fragColor = vec4(1.0, 1.0, 1.0, 1.0);" +
                "}" +

            "}"
        );

        // Specifing above Source to the fragment Shader Object
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
       
        //Compile the fragment Shader Object
        GLES32.glCompileShader(fragmentShaderObject);

        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus,0);
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);

                System.out.println("RTR: FragmentShaderInfoLog" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        shaderProgramObject = GLES32.glCreateProgram();
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        //pre Link Binding to attributes
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");
        //Link the Shader Program
        GLES32.glLinkProgram(shaderProgramObject);

        int[] iProgramLinkStatus  = new int[1];
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus,0);
        if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                GLES32.glGetProgramInfoLog(shaderProgramObject);

                System.out.println("RTR: ProgramLinkStatus" +szInfoLog);
                Uninitialize_RMB();
                System.exit(0);
            }
        }

        //Post Linking Retriving Uniform Location
        modelUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
        viewUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
        projectionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
        ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld");
        kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
        lightPositionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Light_Position");
        lKeyIsPressedUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");




        //Array For cubeVertices Vertices
        Sphere sphere = new Sphere();
        float sphere_vertices[] = new float[1146];
        float sphere_normals[] = new float[1146];
        float sphere_textures[] = new float[764];
        short sphere_elements[] = new short[2280];

        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        gNumVertices = sphere.getNumberOfSphereVertices();
        gNumElements = sphere.getNumberOfSphereElements();
       
       //vbo
        GLES32.glGenVertexArrays(1, vao_Sphere,0);
        GLES32.glBindVertexArray(vao_Sphere[0]);

        // position vbo
        GLES32.glGenBuffers(1, vbo_Sphere_Position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Sphere_Position[0]);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_vertices.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        //normal vbo
        GLES32.glGenBuffers(1, vbo_Sphere_Normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Sphere_Normal[0]);

        byteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer = byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_normals.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        //element vbo
        GLES32.glGenBuffers(1, vbo_Sphere_Element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_Sphere_Element[0]);

        byteBuffer = ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer = byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER, sphere_elements.length * 2, elementsBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);


        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        //GLES32.glClearDepth(1.0f);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.setIdentityM(viewMatrix, 0);
        Matrix.setIdentityM(projectionMatrix, 0);
    }

    private void Resize_RMB(int iWidth, int iHeight)
    {

        if (iHeight == 0)
		{
			iHeight = 1;
		}

		GLES32.glViewport(0, 0, iWidth, iHeight);
        Matrix.perspectiveM(projectionMatrix, 0, 45.0f, (float)iWidth / (float)iHeight, 0.1f, 100.0f);
    }

    private void Display_RMB()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject);

        if (gbLight == true)
		{
			GLES32.glUniform1i(lKeyIsPressedUniform, 1);
			GLES32.glUniform3f(ldUniform, 1.0f, 1.0f, 1.0f); //White Light
			GLES32.glUniform3f(kdUniform, 0.5f, 0.5f, 0.5f); //Gray Material
			GLES32.glUniform4f(lightPositionUniform, 0.0f, 0.0f, 2.0f, 1.0f);
		}
		else
		{
			GLES32.glUniform1i(lKeyIsPressedUniform, 0);
		}

        float[] translateMatrix = new float[16];
        float[] scaleMatrix = new float[16];
        float[] rotateMatrix = new float[16];

        Matrix.setIdentityM(modelMatrix, 0);
       
        Matrix.setIdentityM(viewMatrix, 0);
        Matrix.setLookAtM(viewMatrix,0, 0.0f, 0.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

        Matrix.setIdentityM(translateMatrix, 0);
        Matrix.translateM(translateMatrix,0, 0.0f, 0.0f, 0.0f);
        Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

        Matrix.setIdentityM(rotateMatrix, 0);
        Matrix.rotateM(rotateMatrix,0, (float)gSholder, 0.0f, 0.0f, 1.0f);
        Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotateMatrix, 0);

        Matrix.setIdentityM(translateMatrix, 0);
        Matrix.translateM(translateMatrix,0, 0.8f, 0.0f, 0.0f);
        Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

        Matrix.setIdentityM(scaleMatrix, 0);
        Matrix.scaleM(scaleMatrix,0, 1.6f, 0.5f, 1.0f);
        Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

        GLES32.glUniformMatrix4fv(modelUniform, 1, false, modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewUniform, 1, false, viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionUniform, 1, false, projectionMatrix,0);

         //bind vao
        GLES32.glBindVertexArray(vao_Sphere[0]);
       
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_Sphere_Element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, gNumElements, GLES32.GL_UNSIGNED_SHORT, 0);
        // unbind vao
        GLES32.glBindVertexArray(0);

        //************ Elbow ************************
        Matrix.setIdentityM(modelMatrix, 0);

        Matrix.setIdentityM(translateMatrix, 0);
        Matrix.translateM(translateMatrix,0, 0.0f, 0.0f, 0.0f);
        Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

        Matrix.setIdentityM(rotateMatrix, 0);
        Matrix.rotateM(rotateMatrix,0, (float)gSholder, 0.0f, 0.0f, 1.0f);
        Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotateMatrix, 0);

        Matrix.setIdentityM(translateMatrix, 0);
        Matrix.translateM(translateMatrix,0, 1.6f, 0.0f, 0.0f);
        Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

        Matrix.setIdentityM(rotateMatrix, 0);
        Matrix.rotateM(rotateMatrix,0, (float)gElbow, 0.0f, 0.0f, 1.0f);
        Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotateMatrix, 0);

        Matrix.setIdentityM(translateMatrix, 0);
        Matrix.translateM(translateMatrix,0, 0.8f, 0.0f, 0.0f);
        Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

        Matrix.setIdentityM(scaleMatrix, 0);
        Matrix.scaleM(scaleMatrix,0, 1.6f, 0.5f, 1.0f);
        Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

        GLES32.glUniformMatrix4fv(modelUniform, 1, false, modelMatrix,0);

         //bind vao
        GLES32.glBindVertexArray(vao_Sphere[0]);
       
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_Sphere_Element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, gNumElements, GLES32.GL_UNSIGNED_SHORT, 0);
        // unbind vao
        GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);
        requestRender();   //SwapBuffers();
    }


    private void Uninitialize_RMB()
    {
        if(vbo_Sphere_Normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Sphere_Normal,0);
            vbo_Sphere_Normal[0] = 0;
        }
        if(vbo_Sphere_Position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Sphere_Position,0);
            vbo_Sphere_Position[0] = 0;
        }
        if(vbo_Sphere_Element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Sphere_Element,0);
            vbo_Sphere_Element[0] = 0;
        }
        if(vao_Sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_Sphere,0);
            vao_Sphere[0] = 0;
        }


    	if(shaderProgramObject != 0)
    	{
    		int[] shaderCount = new int[1];
    		int shaderNumber;

    		GLES32.glUseProgram(shaderProgramObject);
    		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount,0);

    		int[] shaders = new int[shaderCount[0]];
            if(shaders[0] != 0)
            {
                GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
                for(shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
                {
                   GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
                   GLES32.glDeleteShader(shaders[shaderNumber]);
                   shaders[shaderNumber] = 0;
                }
            }
            
    	}
    	GLES32.glUseProgram(0);
    	GLES32.glDeleteProgram(shaderProgramObject);
    	shaderProgramObject = 0;
    }
}



