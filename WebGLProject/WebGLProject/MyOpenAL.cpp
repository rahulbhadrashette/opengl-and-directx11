#include "MyOpenAL.h"

// For OpenAL SKC
bool SKC_gbSoundPlay;
ALCdevice* SKC_Device = NULL;
ALCcontext* SKC_Context = NULL;
ALboolean SKC_g_bEAX = false;
ALenum SKC_error;
int SKC_channel;
int SKC_sampleRate; 
int SKC_bitsPerSec; 
int SKC_size;
unsigned int SKC_format;

char* SKC_data = NULL;
unsigned int SKC_BufferId;
unsigned int SKC_SourceId;

int OpenALInitialize(void)
{
    // OpenAL Code Start
	if(isBigEndian())
	{
		LogFile("big endian SKC... \n");
	}
	else 
	{
		LogFile("little endian SKC... \n");
	}
	
	SKC_data = loadWAV_CPP("Naruto_Shippuuden_OST_Byakuya.wav", SKC_channel, SKC_sampleRate, SKC_bitsPerSec, SKC_size);
	
    // Initialization of OpenAL 
    SKC_Device = alcOpenDevice(NULL);                              // ALCdevice *alcOpenDevice(const ALCchar *devicename)

	if(SKC_Device == NULL)
	{
		LogFile("Cannot open sound card SKC... \n");
		OpenALUninitialize();
		exit(1);
	}

    SKC_Context = alcCreateContext(SKC_Device, NULL);               // ALCcontext* alcCreateContext(ALCdevice *device, ALCint* attrlist);
	if(SKC_Context == NULL)
	{
		LogFile("Cannot open context SKC... \n");
		OpenALUninitialize();
		exit(1);
	}

	alcMakeContextCurrent(SKC_Context);                                     // ALCboolean alcMakeContextCurrent(ALCcontext *context);

    // Check for EAX 2.0 Support
    SKC_g_bEAX = alIsExtensionPresent("EAX2.0");                            // ALboolean alIsExtensionPresent(const ALchar *extname);

	alGenBuffers(1, &SKC_BufferId);                                         // void alGenBuffers(ALsizei n, ALuint *buffers); n = 1;
    if((SKC_error = alGetError()) != AL_NO_ERROR)                           // ALenum alGetError(ALvoid);
    {
		LogFile("alGenBuffers : %d\n");
		OpenALUninitialize();
		exit(1);
    }

    //printf("Information of Audio File is...\n");
	if(SKC_channel == 1)
	{
		if(SKC_bitsPerSec == 8)
		{
			SKC_format = AL_FORMAT_MONO8;
            //printf("SKC_Channel = %d, SKC_BitsPerSeconds = %d, SKC_Format = MONO 8 ...\n", SKC_channel, SKC_bitsPerSec);
		}
		else 
		{
			SKC_format = AL_FORMAT_MONO16;
            //printf("SKC_Channel = %d, SKC_BitsPerSeconds = %d, SKC_Format = MONO 16 ...\n", SKC_channel, SKC_bitsPerSec);
		}
	}
	else
	{
		if(SKC_bitsPerSec == 8)
		{
			SKC_format = AL_FORMAT_STEREO8;
            //printf("SKC_Channel = %d, SKC_BitsPerSeconds = %d, SKC_Format = STEREO 8 ...\n", SKC_channel, SKC_bitsPerSec);
		}
		else 
		{
			SKC_format = AL_FORMAT_STEREO16;
            //printf("SKC_Channel = %d, SKC_BitsPerSeconds = %d, SKC_Format = STEREO 16 ...\n", SKC_channel, SKC_bitsPerSec);
		}
	}

    // Copy RTR_Agnepath.wav data into AL Buffer 0
    alBufferData(SKC_BufferId, SKC_format, SKC_data, SKC_size, SKC_sampleRate);	            // void alBufferData(ALuint buffer, ALenum format, const Alvoid *data, Alsizei size, ALsizei freq)
    if((SKC_error = alGetError()) != AL_NO_ERROR)                           // ALenum alGetError(ALvoid);
    {
		LogFile("alBufferData \n");
		OpenALUninitialize();
		exit(1);
    }
	// Generate Sources
	alGenSources(1, &SKC_SourceId);                                         // void alGenSources(ALSizei n, ALuint *sources); n = 1;
	if ((SKC_error = alGetError()) != AL_NO_ERROR)                           // ALenum alGetError(ALvoid);
	{
		LogFile("alGenSources \n");
		OpenALUninitialize();
		exit(1);
	}

	//// Attach buffer 0 to source
	alSourcei(SKC_SourceId, AL_BUFFER, SKC_BufferId);                           // void alSourcei(ALuint source, ALenum param, ALint value);
	if ((SKC_error = alGetError()) != AL_NO_ERROR)                           // ALenum alGetError(ALvoid);
	{
		LogFile("alSourcei AL_BUFFER \n");
		OpenALUninitialize();
		exit(1);
	}
    
    /* ALfloat listenerPos[]={0.0,0.0,0.0};
        
    // Position ...SKC_channel
    alListener3f(AL_POSITION,listenerPos);                      // void alListenerfv(ALenum param, ALfloat *values);
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        DisplayALError("alListenerfv POSITION : ", error);
        return;
    }*/

	//alSourcePlay(SKC_SourceId);                                         // void alSourcePlay(ALuint source);
                                                                        // void alSourcePause(ALuint source);
                                                                         // void alSourceStop(ALuint source);
                                                                        // void alSourceRewind(ALuint source);

	//alSourcePlay(SKC_SourceId2);                                         // void alSourcePlay(ALuint source);
																		// void alSourcePause(ALuint source);
																		 // void alSourceStop(ALuint source);
																		// void alSourceRewind(ALuint source);
     return(0);
}

void OpenALUninitialize(void)
{
     // Exit from OpenAL
    
	alDeleteSources(1, &SKC_SourceId);
	alDeleteBuffers(1, &SKC_BufferId);

    SKC_Context = alcGetCurrentContext();                                   // ALCcontext* alcGetCurrentContext(ALCvoid);
    SKC_Device = alcGetContextsDevice(SKC_Context);                             // ALCdevice* alcGetContextsDevice(ALcontext *context);
    alcMakeContextCurrent(NULL);                                        // ALCboolean alcMakeContextCurrent(ALCcontext *context);
    
    if(SKC_Context != NULL)
    {
        alcDestroyContext(SKC_Context);                                     // void alcDestroyContext(ALCcontext *context);
        SKC_Context = NULL;
    }

    if(SKC_Device != NULL)
	{
		alcCloseDevice(SKC_Device);                                         // ALCboolean alcCloseDevice(ALCdevice *device);
		SKC_Device = NULL;
	}

	if(SKC_data != NULL)
	{
		//free(data);
		delete[] SKC_data;
		SKC_data = NULL;	
	}
}

char* loadWAV_CPP(const char* SKC_fileName, int& SKC_channel, int& SKC_sampleRate, int& SKC_bitsPerSec, int& SKC_size)
{
    // Local Function Declarations SKC
    int convertToInt(char* , int);

	// Local Variable Declarations SKC
	char SKC_buffer[4];
	char* SKC_AudioData = NULL;
	// Code SKC
	std::ifstream SKC_in(SKC_fileName, std::ios::binary);
	SKC_in.read(SKC_buffer,4);

	if(strncmp(SKC_buffer, "RIFF", 4) != 0)
	{
		LogFile("This is not a valid WAVE file SKC... \n");
		return NULL;
	}

   // std::cout << SKC_buffer[0] << SKC_buffer[1] << SKC_buffer[2] << SKC_buffer[3] << std::endl;

	SKC_in.read(SKC_buffer,4);
	SKC_in.read(SKC_buffer,4);	// WAVE
	SKC_in.read(SKC_buffer,4);	// fmt
	SKC_in.read(SKC_buffer,4);	// 16
	SKC_in.read(SKC_buffer,2);	// PCM = 1
	SKC_in.read(SKC_buffer,2);
	SKC_channel = convertToInt(SKC_buffer,2);
	SKC_in.read(SKC_buffer,4);
	SKC_sampleRate = convertToInt(SKC_buffer, 4);
	SKC_in.read(SKC_buffer,4);
	SKC_in.read(SKC_buffer,2);
	SKC_in.read(SKC_buffer,2);
	SKC_bitsPerSec = convertToInt(SKC_buffer, 2);
	SKC_in.read(SKC_buffer,4);	// data
	SKC_in.read(SKC_buffer,4);
	SKC_size = convertToInt(SKC_buffer,4);
	SKC_AudioData = new char[SKC_size];
	SKC_in.read(SKC_AudioData,SKC_size);

	SKC_in.close();
	return SKC_AudioData;
}

int convertToInt(char* SKC_buffer, int SKC_length)
{
    // Local Function Declarations SKC
    bool isBigEndian(void);

    // Local Variable Declarations SKC
	int SKC_a = 0;

    // Code SKC
	if(!isBigEndian())
	{
		for(int SKC_i = 0; SKC_i < SKC_length; SKC_i++)
			((char*)&SKC_a)[SKC_i] = SKC_buffer[SKC_i];
	}
	else 
	{
		for(int SKC_i = 0; SKC_i < SKC_length; SKC_i++)
			((char*)&SKC_a)[3-SKC_i] = SKC_buffer[SKC_i];
	}
	return SKC_a;
}

bool isBigEndian(void)
{
    // Local Variable Declarations SKC
    int SKC_a = 1;

    // Code SKC
	return !((char*)&SKC_a)[0];
}
