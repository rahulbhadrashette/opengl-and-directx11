#include "stdafx.h"
#include "ModelLoading.h"
#include "Model.h"
#include "Terrain.h"

struct MODELLOAD
{
	GLuint gShaderProgramObject;

	GLuint modelUniform;
	GLuint viewUniform;
	GLuint projectionUniform;
	GLuint samplerUniform;
};

struct MODELLOAD model_t;
//Model* tree1Model = NULL;
Model* tree2Model = NULL;
Model* tree3Model = NULL;
Model* barrelModel = NULL;
Model* container = NULL;
Model* sandBageCircle = NULL;
Model* rock = NULL;
Model* tank = NULL;
Model* Tent1 = NULL;
Model* HumanModel = NULL;
Model* SoldierSalute = NULL;
Model* FlagPole = NULL;
Model* GunModel = NULL;

#define SCALE_SOLDIER_SIZE 20.0f
#define SCALE_BARRAL_SIZE 1.6f
#define SCALE_TREE_SIZE 3.0f
#define SCALE_GUN_SIZE 0.25f

extern bool bShowFlag;
extern bool bShowSoldier;

int ModelLoadingInitialize(HWND ghwnd)
{
	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLint iShaderCompileStatus;
	GLint iProgramLinkStatus;
	GLint iInfoLogLength;
	GLchar* szInfoLog;

	// Define vertex shader object
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// write vertex shader code
	const GLchar* vertexShaderSourceCode = {

		"#version 450 core"
		"\n"
		"\n in vec4 vPosition;"
		"\n in vec4 vNormal;"
		"\n in vec2 vTexCoords;"

		"\n uniform mat4 u_modelMatrix;"
		"\n uniform mat4 u_viewMatrix;"
		"\n uniform mat4 u_projectionMatrix;"

		"\n out vec2 outTexCoords;"

		"\n void main(void)"
		"\n {"
		"\n		outTexCoords = vTexCoords;"
		"\n 	gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;"
		"\n }"
	};

	// Specifing above source to the vertex shader object
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// Compile the vertex shader
	glCompileShader(vertexShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				LogFile("ModelLoading VS :- ");
				LogFile((char*)szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Define Fragment Shader Object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// Write Fragment Shader Code
	const GLchar* fragmentShaderSourceCode = {

		"#version 450 core"

		"\n in vec2 outTexCoords;"

		"\n uniform sampler2D samplerTexture;"

		"\n out vec4 fragColor;"

		"\n void main(void)"
		"\n {"
		"\n 	fragColor = texture(samplerTexture, outTexCoords);"
		"\n }"
	};

	// Specifing above source to the Fragment Shader Object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	// Compile the Fragment Shader Object
	glCompileShader(fragmentShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				LogFile("ModelLoading FS :- ");
				LogFile((char*)szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Create Shader Object
	model_t.gShaderProgramObject = glCreateProgram();

	// Attach vertexShaderObject to ShaderProgramObject
	glAttachShader(model_t.gShaderProgramObject, vertexShaderObject);
	// Attach fragmentShaderObject to ShaderProgramObject
	glAttachShader(model_t.gShaderProgramObject, fragmentShaderObject);

	// Pre Linking Binding to vertex attribute
	glBindAttribLocation(model_t.gShaderProgramObject, 0, "vPosition");
	glBindAttribLocation(model_t.gShaderProgramObject, 1, "vNormal");
	glBindAttribLocation(model_t.gShaderProgramObject, 2, "vTexCoords");

	//Link the shader program
	glLinkProgram(model_t.gShaderProgramObject);

	iProgramLinkStatus;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(model_t.gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(model_t.gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(model_t.gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				LogFile("ModelLoading LS :- ");
				LogFile((char*)szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// Post Linking Retriving Uniform Location
	model_t.modelUniform = glGetUniformLocation(model_t.gShaderProgramObject, "u_modelMatrix");
	model_t.viewUniform = glGetUniformLocation(model_t.gShaderProgramObject, "u_viewMatrix");
	model_t.projectionUniform = glGetUniformLocation(model_t.gShaderProgramObject, "u_projectionMatrix");

	// load models
	//tree1Model = new Model("resources/objects/Tree/Tree.obj");
	tree2Model = new Model("resources/objects/Palm_tree/palm_tree.obj");
	tree3Model = new Model("resources/objects/Tree_02/Tree.obj");
	barrelModel = new Model("resources/objects/barrel/barrel.obj");
	container = new Model("resources/objects/Container/container.obj");
	sandBageCircle = new Model("resources/objects/SandbagsCircle/SandbagsCircle.obj");
	rock = new Model("resources/objects/rock/rock.obj");
	tank = new Model("resources/objects/Tank/Abrams_BF3.obj");
	Tent1 = new Model("resources/objects/Tent1/Tent1.obj");
	HumanModel = new Model("resources/objects/flagpole/dedaSoldier.obj");
	SoldierSalute = new Model("resources/objects/Soldier_3/Soldier_salute.obj");
	FlagPole = new Model("resources/objects/flagpole/flagpole.obj");
	GunModel = new Model("resources/objects/gun/AK47Rotated_1.obj");

	return(0);
}

void ModelLoadingDisplay(glm::mat4 viewMatrix, glm::mat4 projectionMatrix)
{
	void PalmTreeDraw(void);
	void BarralDraw(void);
	void Tree3Draw(void);
	void sandBageCircleDraw(void);
	void containerDraw(void);
	void rockDraw(void);
	void tankDraw(void);
	void TentDraw(void);
	void SoldierDraw(void);
	void FlagPoleDraw(void);
	void SaluteSoldierDraw(void);
	void GunDraw(void);

	glUseProgram(model_t.gShaderProgramObject);

	glUniformMatrix4fv(model_t.viewUniform, 1, GL_FALSE, value_ptr(viewMatrix));
	glUniformMatrix4fv(model_t.projectionUniform, 1, GL_FALSE, value_ptr(projectionMatrix));

	//#####################################################################################################################

	PalmTreeDraw();
	BarralDraw();
	Tree3Draw();
	sandBageCircleDraw();
	containerDraw();
	rockDraw();
	tankDraw();
	TentDraw();
	SoldierDraw();
	GunDraw();

	if (bShowFlag == true)
	{
		FlagPoleDraw();
	}

	if (bShowSoldier == true)
	{
		SaluteSoldierDraw();
	}

	//UnUse Program
	glUseProgram(0);
}

void ModelLoadingUninitialize(void)  
{
	// code 
	if (HumanModel != NULL)
	{
		delete HumanModel;
		HumanModel = NULL;
	}

	if (Tent1 != NULL)
	{
		delete Tent1;
		Tent1 = NULL;
	}

	if (tank != NULL)
	{
		delete tank;
		tank = NULL;
	}

	if (rock != NULL)
	{
		delete rock;
		rock = NULL;
	}

	if (sandBageCircle != NULL)
	{
		delete sandBageCircle;
		sandBageCircle = NULL;
	}

	if (container != NULL)
	{
		delete container;
		container = NULL;
	}

	if (barrelModel != NULL)
	{
		delete barrelModel;
		barrelModel = NULL;
	}

	if (tree3Model != NULL)
	{
		delete tree3Model;
		tree3Model = NULL;
	}

	if (tree2Model != NULL)
	{
		delete tree2Model;
		tree2Model = NULL;
	}

	if (model_t.gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(model_t.gShaderProgramObject);

		// Ask program how many shader are attached to you
		glGetProgramiv(model_t.gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(model_t.gShaderProgramObject, shaderCount, &shaderCount, pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(model_t.gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(model_t.gShaderProgramObject);
		model_t.gShaderProgramObject = 0;

		glUseProgram(0);
	}
}

void ModelLoadingUpdate(void)
{

}

void PalmTreeDraw(void)
{
	//#####################################################################################################################
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-105.0f, getTerrainHeightAtPosition(-105.0f, -60.0f), -60.0f));  // translate it down so it's at the center of the scene
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-140.0f, getTerrainHeightAtPosition(-140.0f, -100.0f), -100.0f));  // translate it down so it's at the center of the scene
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//****************************************************************************************************
	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-120.0f, getTerrainHeightAtPosition(-120.0f, -120.0f), -120.0f));  // translate it down so it's at the center of the scene
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-100.0f, getTerrainHeightAtPosition(-100.0f, -120.0f), -120.0f));  // translate it down so it's at the center of the scene
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-75.0f, getTerrainHeightAtPosition(-75.0f, -120.0f), -120.0f)); // translate it down so it's at the center of the scene
	////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-50.0f, getTerrainHeightAtPosition(-50.0f, -120.0f), -120.0f));  // translate it down so it's at the center of the scene
	////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-35.0f, getTerrainHeightAtPosition(-35.0f, -70.0f), -70.0f)); // translate it down so it's at the center of the scene
	////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, getTerrainHeightAtPosition(0.0f, -90.0f), -90.0f)); // translate it down so it's at the center of the scene
	////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(25.0f, getTerrainHeightAtPosition(25.0f, -120.0f), -120.0f)); // translate it down so it's at the center of the scene
	////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(50.0f, getTerrainHeightAtPosition(50.0f, -120.0f), -120.0f));  // translate it down so it's at the center of the scene
	////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(75.0f, getTerrainHeightAtPosition(75.0f, -120.0f), -120.0f)); // translate it down so it's at the center of the scene
	////////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(100.0f, getTerrainHeightAtPosition(100.0f, -100.0f), -100.0f));  // translate it down so it's at the center of the scene
	//////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(120.0f, getTerrainHeightAtPosition(120.0f, -90.0f), -90.0f));  // translate it down so it's at the center of the scene
	//////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//**************************************************************************************************
	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(140.0f, getTerrainHeightAtPosition(140.0f, -80.0f), -80.0f));  // translate it down so it's at the center of the scene
	//////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(140.0f, getTerrainHeightAtPosition(140.0f, -60.0f), -60.0f));  // translate it down so it's at the center of the scene
	//////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(140.0f, getTerrainHeightAtPosition(140.0f, -40.0f), -40.0f));  // translate it down so it's at the center of the scene
	//////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//#############################################################################################################
	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-100.0f, getTerrainHeightAtPosition(-100.0f, 120.0f), 120.0f));  // translate it down so it's at the center of the scene
	//////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-64.0f, getTerrainHeightAtPosition(-65.0f, 75.0f), 75.0f));  // translate it down so it's at the center of the scene
	//////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-25.0f, getTerrainHeightAtPosition(-25.0f, 140.0f), 140.0f));  // translate it down so it's at the center of the scene
	//////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(10.0f, getTerrainHeightAtPosition(10.0f, 105.0f), 105.0f));  // translate it down so it's at the center of the scene
	//////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(35.0f, getTerrainHeightAtPosition(35.0f, 115.0f), 115.0f));  // translate it down so it's at the center of the scene
	//////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);

	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(70.0f, getTerrainHeightAtPosition(70.0f, 125.0f), 125.0f));  // translate it down so it's at the center of the scene
	//////Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree2Model->Draw(model_t.gShaderProgramObject);
	glDisable(GL_BLEND);
}

void Tree3Draw(void)
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glm::mat4  modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-17.0f, getTerrainHeightAtPosition(-17.0f, 20.0f), 20.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_TREE_SIZE, SCALE_TREE_SIZE, SCALE_TREE_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree3Model->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-22.0f, getTerrainHeightAtPosition(-22.0f, 20.0f), 20.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_TREE_SIZE, SCALE_TREE_SIZE, SCALE_TREE_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree3Model->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-24.0f, getTerrainHeightAtPosition(-24.0f, 15.0f), 15.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_TREE_SIZE, SCALE_TREE_SIZE, SCALE_TREE_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree3Model->Draw(model_t.gShaderProgramObject);

	/////**************************************************************************************************************************************
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(37.5f, getTerrainHeightAtPosition(37.5f, 5.0f), 5.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_TREE_SIZE, SCALE_TREE_SIZE, SCALE_TREE_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree3Model->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(37.5f, getTerrainHeightAtPosition(37.5f, 15.0f), 15.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_TREE_SIZE, SCALE_TREE_SIZE, SCALE_TREE_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree3Model->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(43.0f, getTerrainHeightAtPosition(43.0f, 10.0f), 10.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_TREE_SIZE, SCALE_TREE_SIZE, SCALE_TREE_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree3Model->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(48.0f, getTerrainHeightAtPosition(48.0f, 16.0f), 16.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_TREE_SIZE, SCALE_TREE_SIZE, SCALE_TREE_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree3Model->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(53.0f, getTerrainHeightAtPosition(53.0f, 14.0f), 14.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_TREE_SIZE, SCALE_TREE_SIZE, SCALE_TREE_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree3Model->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(57.0f, getTerrainHeightAtPosition(57.0f, 10.0f), 10.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_TREE_SIZE, SCALE_TREE_SIZE, SCALE_TREE_SIZE));	
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tree3Model->Draw(model_t.gShaderProgramObject);

	glDisable(GL_BLEND);
}

void BarralDraw(void)
{
	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-10.0f, getTerrainHeightAtPosition(-10.0f, 15.0f), 15.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_BARRAL_SIZE, SCALE_BARRAL_SIZE, SCALE_BARRAL_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	barrelModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-10.0f, getTerrainHeightAtPosition(-10.0f, 18.0f), 18.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_BARRAL_SIZE, SCALE_BARRAL_SIZE, SCALE_BARRAL_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	barrelModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-12.5f, getTerrainHeightAtPosition(-12.5f, 15.5f), 15.5f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_BARRAL_SIZE, SCALE_BARRAL_SIZE, SCALE_BARRAL_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	barrelModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-12.5f, getTerrainHeightAtPosition(-12.5f, 18.5f), 18.5f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_BARRAL_SIZE, SCALE_BARRAL_SIZE, SCALE_BARRAL_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	barrelModel->Draw(model_t.gShaderProgramObject);

	//************************************************************************************************************

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(44.0f, getTerrainHeightAtPosition(44.0f, 59.0f), 59.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_BARRAL_SIZE, SCALE_BARRAL_SIZE, SCALE_BARRAL_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	barrelModel->Draw(model_t.gShaderProgramObject);
	
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(42.0f, getTerrainHeightAtPosition(42.0f, 58.0f), 58.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_BARRAL_SIZE, SCALE_BARRAL_SIZE, SCALE_BARRAL_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	barrelModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(42.0f, getTerrainHeightAtPosition(42.0f, 56.0f), 56.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_BARRAL_SIZE, SCALE_BARRAL_SIZE, SCALE_BARRAL_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	barrelModel->Draw(model_t.gShaderProgramObject);
}

void sandBageCircleDraw(void)
{
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-40.0f, getTerrainHeightAtPosition(-40.0f, 80.0f), 80.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::rotate(modelMatrix, glm::radians(60.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	sandBageCircle->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(80.0f, getTerrainHeightAtPosition(80.0f, 2.0f), 2.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::rotate(modelMatrix, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	sandBageCircle->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-90.0f, getTerrainHeightAtPosition(-90.0f, 3.0f), 3.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::rotate(modelMatrix, glm::radians(-30.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	sandBageCircle->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(85.0f, getTerrainHeightAtPosition(85.0f, 75.0f), 75.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::rotate(modelMatrix, glm::radians(160.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	sandBageCircle->Draw(model_t.gShaderProgramObject);
}

void containerDraw(void)
{
	//Declatation of Matrices And Initialize of Matrix in identity,Do Necessary Transformation Code Here
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	glm::mat4 scaleMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-38.0f, getTerrainHeightAtPosition(-38.0f, 37.0f), 37.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(2.0f, 2.0f, 2.0f));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	container->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	scaleMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-38.0f, getTerrainHeightAtPosition(-38.0f, 24.0f), 24.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(2.0f, 2.0f, 2.0f));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	container->Draw(model_t.gShaderProgramObject);
}

void rockDraw(void)
{
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	glm::mat4 scaleMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, getTerrainHeightAtPosition(0.0f, 34.0f), 34.0f));  // translate it down so it's at the center of the scene
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	rock->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	scaleMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(2.0f, getTerrainHeightAtPosition(2.0f, 30.0f), 30.0f));  // translate it down so it's at the center of the scene
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	rock->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	scaleMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(4.0f, getTerrainHeightAtPosition(4.0f, 34.0f), 34.0f));  // translate it down so it's at the center of the scene
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	rock->Draw(model_t.gShaderProgramObject);
}

void tankDraw(void)
{
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-100.0f, 1.5f + (getTerrainHeightAtPosition(-100.0f, 108.0f)), 108.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(2.0f, 2.0f, 2.0f));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	tank->Draw(model_t.gShaderProgramObject);
}

void TentDraw(void)
{
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(12.0f, (getTerrainHeightAtPosition(12.0f, 42.0f)), 42.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::rotate(modelMatrix, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(1.0f, 1.0f, 1.0f));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	Tent1->Draw(model_t.gShaderProgramObject);
}

void SoldierDraw(void)
{
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-85.0f, getTerrainHeightAtPosition(-85.0f, 5.0f), 5.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.5f, 0.0f));
	modelMatrix = glm::rotate(modelMatrix, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	HumanModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-80.0f, getTerrainHeightAtPosition(-80.0f, 7.0f), 7.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.3f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	HumanModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-75.0f, getTerrainHeightAtPosition(-75.0f, 5.0f), 5.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.3f, 0.0f));
	modelMatrix = glm::rotate(modelMatrix, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	HumanModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-37.0f, getTerrainHeightAtPosition(-37.0f, 78.0f), 78.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.3f, 0.0f));
	modelMatrix = glm::rotate(modelMatrix, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	HumanModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-37.0f, getTerrainHeightAtPosition(-37.0f, 72.0f), 72.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.5f, 0.0f));
	modelMatrix = glm::rotate(modelMatrix, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	HumanModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-32.0f, getTerrainHeightAtPosition(-32.0f, 75.0f), 75.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.12f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	HumanModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(68.0f, getTerrainHeightAtPosition(68.0f, 0.0f), 0.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.5f, 0.0f));
	modelMatrix = glm::rotate(modelMatrix, glm::radians(110.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	HumanModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(73.0f, getTerrainHeightAtPosition(73.0f, 2.0f), 2.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.5f, 0.0f));
	modelMatrix = glm::rotate(modelMatrix, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	HumanModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(68.0f, getTerrainHeightAtPosition(68.0f, -6.0f), -6.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.12f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	HumanModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(78.0f, getTerrainHeightAtPosition(78.0f, 75.0f), 75.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.5f, 0.0f));
	modelMatrix = glm::rotate(modelMatrix, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	HumanModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(74.0f, getTerrainHeightAtPosition(74.0f, 71.0f), 71.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.5f, 0.0f));
	modelMatrix = glm::rotate(modelMatrix, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	HumanModel->Draw(model_t.gShaderProgramObject);
}

void SaluteSoldierDraw(void)
{
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-5.0f, getTerrainHeightAtPosition(-5.0f, 0.0f), 0.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::rotate(modelMatrix, glm::radians(-270.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	SoldierSalute->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(14.0f, getTerrainHeightAtPosition(14.0f, 0.0f), 0.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::rotate(modelMatrix, glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	SoldierSalute->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(15.0f, getTerrainHeightAtPosition(15.0f, 8.0f), 8.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::rotate(modelMatrix, glm::radians(-180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	SoldierSalute->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(12.0f, getTerrainHeightAtPosition(12.0f, 8.0f), 8.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::rotate(modelMatrix, glm::radians(-180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	SoldierSalute->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(9.0f, getTerrainHeightAtPosition(9.0f, 8.0f), 8.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::rotate(modelMatrix, glm::radians(-180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	SoldierSalute->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(6.0f, getTerrainHeightAtPosition(6.0f, 8.0f), 8.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::rotate(modelMatrix, glm::radians(-180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	SoldierSalute->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(3.0f, getTerrainHeightAtPosition(3.0f, 8.0f), 8.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::rotate(modelMatrix, glm::radians(-180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	SoldierSalute->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, getTerrainHeightAtPosition(0.0f, 8.0f), 8.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::rotate(modelMatrix, glm::radians(-180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	SoldierSalute->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-3.0f, getTerrainHeightAtPosition(-3.0f, 8.0f), 8.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::rotate(modelMatrix, glm::radians(-180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE, SCALE_SOLDIER_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	SoldierSalute->Draw(model_t.gShaderProgramObject);
}

void FlagPoleDraw(void)
{
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(6.0f, getTerrainHeightAtPosition(6.0f, -3.0f), -3.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(0.9f, 0.9f, 0.9f));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	FlagPole->Draw(model_t.gShaderProgramObject);
}

void GunDraw(void)
{
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-83.0f, getTerrainHeightAtPosition(-83.0f, 5.0f), 5.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_GUN_SIZE, SCALE_GUN_SIZE, SCALE_GUN_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	GunModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-79.0f, getTerrainHeightAtPosition(-79.0f, 8.0f), 8.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_GUN_SIZE, SCALE_GUN_SIZE, SCALE_GUN_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	GunModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-71.0f, getTerrainHeightAtPosition(-71.0f, 0.0f), 0.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_GUN_SIZE, SCALE_GUN_SIZE, SCALE_GUN_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	GunModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-34.0f, getTerrainHeightAtPosition(-34.0f, 78.0f), 78.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_GUN_SIZE, SCALE_GUN_SIZE, SCALE_GUN_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	GunModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-37.0f, getTerrainHeightAtPosition(-37.0f, 67.0f), 67.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_GUN_SIZE, SCALE_GUN_SIZE, SCALE_GUN_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	GunModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-32.0f, getTerrainHeightAtPosition(-32.0f, 70.0f), 70.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_GUN_SIZE, SCALE_GUN_SIZE, SCALE_GUN_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	GunModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(64.0f, getTerrainHeightAtPosition(64.0f, 0.0f), 0.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_GUN_SIZE, SCALE_GUN_SIZE, SCALE_GUN_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	GunModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(70.0f, getTerrainHeightAtPosition(70.0f, 2.0f), 2.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_GUN_SIZE, SCALE_GUN_SIZE, SCALE_GUN_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	GunModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(67.0f, getTerrainHeightAtPosition(67.0f, -3.0f), -3.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_GUN_SIZE, SCALE_GUN_SIZE, SCALE_GUN_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	GunModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(74.0f, getTerrainHeightAtPosition(74.0f, 78.0f), 78.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_GUN_SIZE, SCALE_GUN_SIZE, SCALE_GUN_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	GunModel->Draw(model_t.gShaderProgramObject);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(74.0f, getTerrainHeightAtPosition(74.0f, 67.0f), 67.0f));  // translate it down so it's at the center of the scene
	modelMatrix = glm::scale(modelMatrix, glm::vec3(SCALE_GUN_SIZE, SCALE_GUN_SIZE, SCALE_GUN_SIZE));
	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(model_t.modelUniform, 1, GL_FALSE, value_ptr(modelMatrix));
	GunModel->Draw(model_t.gShaderProgramObject);
}

