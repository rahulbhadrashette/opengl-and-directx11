#ifndef TERRAIN_H 
#define TERRAIN_H

#pragma once
#include "stdafx.h"
#include "Main.h"

int TerrainInitialize(HWND, const char*, float*);
void TerrainDisplay(glm::mat4, glm::mat4, /*glm::mat4, GLuint, */ glm::vec4);
void TerrainUninitialize(void);
void TerrainUpdate(void);

void generateTerrainFromHeightMap(float*);

glm::vec3 getTerrainNormalVector(glm::vec3, glm::vec3, glm::vec3);

GLfloat getHeightFromHeightMap(int, int);
GLfloat getHeight(int, int);
GLfloat barryCentric(glm::vec3, glm::vec3, glm::vec3, glm::vec3);

GLfloat getTerrainHeightAtPosition(GLfloat, GLfloat);
void getVegatationPointsFromImage(void);

//void drawTerrainTile(glm::vec3 position, glm::mat4, glm::mat4);
void drawForDepth(void);

unsigned int terrainLoadTexture(char const* path);

#endif
