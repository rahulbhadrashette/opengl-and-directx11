//
//  GLESView.m
//  03-Ortho
//
//  Created by Rahul Bhadrashette on 19/01/20.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "./vmath.h"

#import "GLESView.h"

enum
{
    AMC_ATTRIBUTE_VERTEX = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    //For Line
    GLuint vao_Line;
    GLuint vbo_Line;
    GLuint vbo_Color_Line;
    //For Triangle
    GLuint vao_Triangle;
    GLuint vbo_Triangle;
    GLuint vbo_Color_Triangle;
    //For Circle
    GLuint vao_Circle;
    GLuint vbo_Circle;
    GLuint vbo_Color_Circle;
   
    GLuint mvpUniform;
    
    vmath::mat4 perspectiveProjectionMatrix;
    
}

- (id)initWithFrame:(CGRect)frame;
{
    //code
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed To Create Complete Framebuufer Object %x...!!! \n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        isAnimating = NO;
        animationFrameInterval = 60; //default since iOS 8.2
        
        //Define Vertex Shader Object
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        //Write Vertex Shader Code
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec4 out_Color;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_Color = vColor;" \
        "}";
        
        //Specifing above source to the vertex shader object
        glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
        
        //Compile the Vertex Shader
        glCompileShader(vertexShaderObject);
        
        GLint iShaderCompileStatus = 0;
        GLint iInfoLogLength = 0;
        GLchar *szInfoLog = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if (iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (GLchar*)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("vertex Shader Compilation Log:- %s", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //Define Fragment Shader Object
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        //Write Fragment Shader Code
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec4 out_Color;" \
        "out vec4 fragColor;" \
        "void main(void)" \
        "{" \
        "fragColor = out_Color;" \
        "}";
        
        // Specifing above Source to the fragment Shader Object
        glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
        
        //Compile the fragment Shader Object
        glCompileShader(fragmentShaderObject);
        
        iShaderCompileStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if (iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (GLchar*)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log:- %s \n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //Create Shader Object
        shaderProgramObject = glCreateProgram();
        
        //Attach VertexShader to the Shader Program
        glAttachShader(shaderProgramObject, vertexShaderObject);
        
        //Attach fragmentShader to the Shader Program
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        // Pre Linking Binding to Vertex Attribute
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

        //Link the Shader Program
        glLinkProgram(shaderProgramObject);
        
        GLint iProgramLinkStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
        if (iProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength)
            {
                szInfoLog = (GLchar*)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("LinkShaderProgramObject:- %s", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //Post Linking Retriving Uniform Location
        mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        
        //Line Vertices Array
        const  GLfloat lineVertices[] =
        {
            //Line
            0.0f, 0.8f,0.0f,
            0.0f, -0.8f, 0.0f
        };
        
        //Line Color Array
        const  GLfloat lineColor[] =
        {
            //Line
            1.0f, 1.0f,0.0f,
            1.0f, 1.0f, 0.0f
        };
        
        //triangle Vertices Array
        const  GLfloat triangleVertices[] =
        {
            //Triangle
            0.0f, 0.8f, 0.0f,
            -0.8f, -0.8f, 0.0f,
            -0.8f, -0.8f, 0.0f,
            0.8f, -0.8f, 0.0f,
            0.8f, -0.8f, 0.0f,
            0.0f, 0.8f, 0.0f
        };
        //triangle Color Array
        const int Color = 6;
        GLfloat triangleColor[3 * Color];
        for (int i = 0; i < Color; i++)
        {
            triangleColor[3 * i] = 1.0f;
            triangleColor[3 * i + 1] = 1.0f;
            triangleColor[3 * i + 2] = 0.0f;
        }
        
        //circle Vertices Array
        const int ipoints = 300;
        GLfloat circleVertices[3 * ipoints];
        for (int i = 0; i < ipoints; i++)
        {
            GLfloat Angle = (2.0f * (GLfloat)M_PI * i) / ipoints;
            
            circleVertices[3 * i] = (GLfloat)cos(Angle) * 0.494f;
            circleVertices[3 * i + 1] = (GLfloat)sin(Angle) * 0.494f;
            circleVertices[3 * i + 2] = 0.0f;
        }
        
        //circle Color Array
        const int Colors = 300;
        GLfloat circleColor[3 * Colors];
        for (int i = 0; i < Colors; i++)
        {
            circleColor[3 * i] = 1.0f;
            circleColor[3 * i + 1] = 1.0f;
            circleColor[3 * i + 2] = 0.0f;
        }
        
        //Create vao ----> Line
        glGenVertexArrays(1, &vao_Line);
        glBindVertexArray(vao_Line);
        glGenBuffers(1, &vbo_Line);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Line);
        glBufferData(GL_ARRAY_BUFFER, sizeof(lineVertices), lineVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
        //Unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Color
        glGenBuffers(1, &vbo_Color_Line);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Line);
        glBufferData(GL_ARRAY_BUFFER, sizeof(lineColor), lineColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        //vao
        glBindVertexArray(0);
        
        //Create vao ----> Triangle
        glGenVertexArrays(1, &vao_Triangle);
        glBindVertexArray(vao_Triangle);
        glGenBuffers(1, &vbo_Triangle);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Triangle);
        glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
        //Unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Color
        glGenBuffers(1, &vbo_Color_Triangle);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Triangle);
        glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColor), triangleColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        //vao
        glBindVertexArray(0);
        
        //Create vao-----> ForCircle
        glGenVertexArrays(1, &vao_Circle);
        glBindVertexArray(vao_Circle);
        glGenBuffers(1, &vbo_Circle);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Circle);
        glBufferData(GL_ARRAY_BUFFER, sizeof(circleVertices), circleVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
        //Unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Color
        glGenBuffers(1, &vbo_Color_Circle);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Circle);
        glBufferData(GL_ARRAY_BUFFER, sizeof(circleColor), circleColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        //vao
        glBindVertexArray(0);

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
       // glEnable(GL_CULL_FACE);
        
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        
        perspectiveProjectionMatrix = vmath::mat4::identity();        //clearColor
        
        //GESTURE RECOGNITION
        //Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer  = [[UITapGestureRecognizer  alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; //touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will alloc to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);}

/*
 only override drawRect: if you perform custom drawing.
 An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)drawRect{
    //Dawing code
}

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

- (void)drawView:(id)sender
{
    //code
    static GLfloat pos_Yl = 2.0f;
    static GLfloat neg_Xt = -3.1f, neg_Yt = -3.1f;
    static GLfloat pos_Xc = 2.795f, neg_Yc = -3.1f;
    static GLfloat angle = 0.0f;
    bool triangle = false;
    bool circle = false;

    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    
    
    //Declatation of Matricex
    vmath::mat4 translationMatrix;
    vmath::mat4 rotationMatrix;
    vmath::mat4 modelViewMatrix;
    vmath::mat4 modelViewProjectionMatrix;
    
    //For Line
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    //rotationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    
    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, pos_Yl, -5.0f);
    //rotationMatrix = vmath::rotate(angle, 0.0f, 1.0f, 0.0f);
    
    //Do Necessary Matrix Multiplication
    modelViewMatrix = translationMatrix;// *rotationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    
    glBindVertexArray(vao_Line);
    glLineWidth(5.0f);
    //Draw the Necessary Scnes
    glDrawArrays(GL_LINES, 0, 2);
    //UnBind vao
    glBindVertexArray(0);
    
    //For Triangle
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    
    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(neg_Xt, neg_Yt, -5.0f);
    rotationMatrix = vmath::rotate(angle, 0.0f, 1.0f, 0.0f);
    
    //Do Necessary Matrix Multiplication
    modelViewMatrix = translationMatrix * rotationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    
    glBindVertexArray(vao_Triangle);
    glLineWidth(5.0f);
    //Draw the Necessary Scnes
    glDrawArrays(GL_LINES, 0, 6);
    //UnBind vao
    glBindVertexArray(0);
    
    //For Circle
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    
    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(pos_Xc, neg_Yc, -5.0f);
    rotationMatrix = vmath::rotate(angle, 0.0f, 1.0f, 0.0f);
    
    //Do Necessary Matrix Multiplication
    modelViewMatrix = translationMatrix * rotationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    
    glBindVertexArray(vao_Circle);
    glLineWidth(5.0f);
    //Draw the Necessary Scnes
    glDrawArrays(GL_LINES, 0, 300);
    //UnBind vao
    glBindVertexArray(0);

    //UnUse Program
    glUseProgram(0);
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
    //For Angle
    angle = angle + 1.0f;
    if (angle > 360.0f)
    {
        angle = 0.0f;
    }
    
    //For Line Translation
    pos_Yl = pos_Yl - 0.005f;
    if (pos_Yl <= 0.0f)
    {
        pos_Yl = 0.0f;
        triangle = true;
    }
    
    //For Triangle Translation
    if (triangle == true)
    {
        neg_Xt = neg_Xt + 0.005f;
        neg_Yt = neg_Yt + 0.005f;
        if (neg_Xt >= 0.0f || neg_Yt >= 0.0f)
        {
            neg_Xt = 0.0f;
            neg_Yt = 0.0f;
            circle = true;
        }
    }
    
    //For Circle Translation
    if (circle == true)
    {
        pos_Xc = pos_Xc - 0.005f;
        if (pos_Xc <= 0.0f)
        {
            pos_Xc = 0.0f;
        }
        neg_Yc = neg_Yc + 0.005f;
        if (neg_Yc >= -0.306f)
        {
            neg_Yc = -0.306f;
        }
    }}

- (void)layoutSubviews
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0, 0, width, height);
    
    //glOrtho(left,right,bottom, top, near, far);
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)fwidth / (GLfloat)fheight, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

- (void) myStartAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating = YES;
    }
}

- (void) myStopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating = NO;
    }
}

//to become first responder
- (BOOL)acceptsFirstResponder
{
    //code
    return(YES);
}

- (void)touchesBegin:(NSSet *)touches withEvent:(UIEvent *)withEvent
{
    
}

- (void)onSingleTap:(UITapGestureRecognizer *)gr
{
    
}

- (void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
}

- (void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    [self release];
    exit(0);
}

- (void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

- (void)dealloc
{
    //code
    if (vbo_Color_Circle)
    {
        glDeleteBuffers(1, &vbo_Color_Circle);
        vbo_Color_Circle = 0;
    }
    if (vbo_Circle)
    {
        glDeleteBuffers(1, &vbo_Circle);
        vbo_Circle = 0;
    }
    if (vao_Circle)
    {
        glDeleteVertexArrays(1, &vao_Circle);
        vao_Circle = 0;
    }
    
    if (vbo_Color_Triangle)
    {
        glDeleteBuffers(1, &vbo_Color_Triangle);
        vbo_Color_Triangle = 0;
    }
    if (vbo_Triangle)
    {
        glDeleteBuffers(1, &vbo_Triangle);
        vbo_Triangle = 0;
    }
    if (vao_Triangle)
    {
        glDeleteVertexArrays(1, &vao_Triangle);
        vao_Triangle = 0;
    }
    
    if (vbo_Color_Line)
    {
        glDeleteBuffers(1, &vbo_Color_Line);
        vbo_Color_Line = 0;
    }
    if (vbo_Line)
    {
        glDeleteBuffers(1, &vbo_Line);
        vbo_Line = 0;
    }
    if (vao_Line)
    {
        glDeleteVertexArrays(1, &vao_Line);
        vao_Line = 0;
    }
    
    if (shaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;
        
        glUseProgram(shaderProgramObject);
        //Ask Program How Many Shaders are Attached to you
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(shaderProgramObject, shaderCount, &shaderCount, pShaders);
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(shaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
        glUseProgram(0);
    }
    
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer = 0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer = 0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
