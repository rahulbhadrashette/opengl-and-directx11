//
//  ViewController.m
//  01-Window
//
//  Created by Rahul Bhadrashette on 18/01/20.
//

#import "ViewController.h"

@interface ViewController()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Do any additional setup after loading the view, typically form a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    //Dispose of of any resources that can be recreated.
}

- (void)dealloc
{
    //code
    [super dealloc];
}

@end
