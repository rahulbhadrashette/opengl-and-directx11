//
//  AppDelegate.h
//  01-Window
//
//  Created by Sachin Bhadrashette on 18/01/20.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

