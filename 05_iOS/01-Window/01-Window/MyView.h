//
//  MyView.h
//  01-Window
//
//  Created by Sachin Bhadrashette on 18/01/20.
//

#import <UIKit/UIKit.h>

@interface MyView : UIView <UIGestureRecognizerDelegate>

@end
