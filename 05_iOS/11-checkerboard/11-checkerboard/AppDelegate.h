//
//  AppDelegate.h
//  03-Ortho
//
//  Created by Rahul Bhadrashette on 19/01/20.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
