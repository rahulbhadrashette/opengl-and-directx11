//
//  GLESView.m
//  03-Ortho
//
//  Created by Rahul Bhadrashette on 19/01/20.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "./vmath.h"

#import "GLESView.h"

enum
{
    AMC_ATTRIBUTE_VERTEX = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao_Graph;
    GLuint vbo_Graph;
    GLuint vbo_Color_Graph;
    
    GLuint vao_Shapes;
    GLuint vbo_Shapes;
    GLuint vbo_Color_Shapes;
    
    GLuint vao_BigCircle;
    GLuint vbo_BigCircle;
    
    GLuint vao_SmallCircle;
    GLuint vbo_SmallCircle;
   
    GLuint mvpUniform;
    
    vmath::mat4 perspectiveProjectionMatrix;
    
}

- (id)initWithFrame:(CGRect)frame;
{
    //code
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed To Create Complete Framebuufer Object %x...!!! \n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        isAnimating = NO;
        animationFrameInterval = 60; //default since iOS 8.2
        
        //Define Vertex Shader Object
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        //Write Vertex Shader Code
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec4 out_Color;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_Color = vColor;" \
        "}";
        
        //Specifing above source to the vertex shader object
        glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
        
        //Compile the Vertex Shader
        glCompileShader(vertexShaderObject);
        
        GLint iShaderCompileStatus = 0;
        GLint iInfoLogLength = 0;
        GLchar *szInfoLog = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if (iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (GLchar*)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("vertex Shader Compilation Log:- %s", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //Define Fragment Shader Object
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        //Write Fragment Shader Code
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec4 out_Color;" \
        "out vec4 fragColor;" \
        "void main(void)" \
        "{" \
        "fragColor = out_Color;" \
        "}";
        
        // Specifing above Source to the fragment Shader Object
        glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
        
        //Compile the fragment Shader Object
        glCompileShader(fragmentShaderObject);
        
        iShaderCompileStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if (iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (GLchar*)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log:- %s \n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //Create Shader Object
        shaderProgramObject = glCreateProgram();
        
        //Attach VertexShader to the Shader Program
        glAttachShader(shaderProgramObject, vertexShaderObject);
        
        //Attach fragmentShader to the Shader Program
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        // Pre Linking Binding to Vertex Attribute
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

        //Link the Shader Program
        glLinkProgram(shaderProgramObject);
        
        GLint iProgramLinkStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
        if (iProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength)
            {
                szInfoLog = (GLchar*)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("LinkShaderProgramObject:- %s", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //Post Linking Retriving Uniform Location
        mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        
        //GraphVerticesArray
        const  GLfloat graphVertices[] =
        {
            //Vertical Lines
            //44
            -1.10f, 1.0f, 0.0f,
            -1.10f, -1.0f, 0.0f,
            -1.05f, 1.0f, 0.0f,
            -1.05f, -1.0f, 0.0f,
            -1.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 0.0f,
            -0.95f, 1.0f, 0.0f,
            -0.95f, -1.0f, 0.0f,
            -0.90f, 1.0f, 0.0f,
            -0.90f, -1.0f, 0.0f,
            -0.85f, 1.0f, 0.0f,
            -0.85f, -1.0f, 0.0f,
            -0.80f, 1.0f, 0.0f,
            -0.80f, -1.0f, 0.0f,
            -0.75f, 1.0f, 0.0f,
            -0.75f, -1.0f, 0.0f,
            -0.70f, 1.0f, 0.0f,
            -0.70f, -1.0f, 0.0f,
            -0.65f, 1.0f, 0.0f,
            -0.65f, -1.0f, 0.0f,
            -0.60f, 1.0f, 0.0f,
            -0.60f, -1.0f, 0.0f,
            -0.55f, 1.0f, 0.0f,
            -0.55f, -1.0f, 0.0f,
            -0.50f, 1.0f, 0.0f,
            -0.50f, -1.0f, 0.0f,
            -0.45f, 1.0f, 0.0f,
            -0.45f, -1.0f, 0.0f,
            -0.40f, 1.0f, 0.0f,
            -0.40f, -1.0f, 0.0f,
            -0.35f, 1.0f, 0.0f,
            -0.35f, -1.0f, 0.0f,
            -0.30f, 1.0f, 0.0f,
            -0.30f, -1.0f, 0.0f,
            -0.25f, 1.0f, 0.0f,
            -0.25f, -1.0f, 0.0f,
            -0.20f, 1.0f, 0.0f,
            -0.20f, -1.0f, 0.0f,
            -0.15f, 1.0f, 0.0f,
            -0.15f, -1.0f, 0.0f,
            -0.10f, 1.0f, 0.0f,
            -0.10f, -1.0f, 0.0f,
            -0.05f, 1.0f, 0.0f,
            -0.05f, -1.0f, 0.0f,
            //46
            0.00f, 1.0f, 0.0f,
            0.00f, -1.0f, 0.0f,
            0.05f, 1.0f, 0.0f,
            0.05f, -1.0f, 0.0f,
            0.10f, 1.0f, 0.0f,
            0.10f, -1.0f, 0.0f,
            0.15f, 1.0f, 0.0f,
            0.15f, -1.0f, 0.0f,
            0.20f, 1.0f, 0.0f,
            0.20f, -1.0f, 0.0f,
            0.25f, 1.0f, 0.0f,
            0.25f, -1.0f, 0.0f,
            0.30f, 1.0f, 0.0f,
            0.30f, -1.0f, 0.0f,
            0.35f, 1.0f, 0.0f,
            0.35f, -1.0f, 0.0f,
            0.40f, 1.0f, 0.0f,
            0.40f, -1.0f, 0.0f,
            0.45f, 1.0f, 0.0f,
            0.45f, -1.0f, 0.0f,
            0.50f, 1.0f, 0.0f,
            0.50f, -1.0f, 0.0f,
            0.55f, 1.0f, 0.0f,
            0.55f, -1.0f, 0.0f,
            0.60f, 1.0f, 0.0f,
            0.60f, -1.0f, 0.0f,
            0.65f, 1.0f, 0.0f,
            0.65f, -1.0f, 0.0f,
            0.70f, 1.0f, 0.0f,
            0.70f, -1.0f, 0.0f,
            0.75f, 1.0f, 0.0f,
            0.75f, -1.0f, 0.0f,
            0.80f, 1.0f, 0.0f,
            0.80f, -1.0f, 0.0f,
            0.85f, 1.0f, 0.0f,
            0.85f, -1.0f, 0.0f,
            0.90f, 1.0f, 0.0f,
            0.90f, -1.0f, 0.0f,
            0.95f, 1.0f, 0.0f,
            0.95f, -1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, -1.0f, 0.0f,
            1.05f, 1.0f, 0.0f,
            1.05f, -1.0f, 0.0f,
            1.10f, 1.0f, 0.0f,
            1.10f, -1.0f, 0.0f,
            
            //Horizantal Lines
            //24
            1.15f, -0.60f, 0.0f,
            -1.15f, -0.60f, 0.0f,
            1.15f, -0.55f, 0.0f,
            -1.15f, -0.55f, 0.0f,
            1.15f, -0.50f, 0.0f,
            -1.15f, -0.50f, 0.0f,
            1.15f, -0.45f, 0.0f,
            -1.15f, -0.45f, 0.0f,
            1.15f, -0.40f, 0.0f,
            -1.15f, -0.40f, 0.0f,
            1.15f, -0.35f, 0.0f,
            -1.15f, -0.35f, 0.0f,
            1.15f, -0.30f, 0.0f,
            -1.15f, -0.30f, 0.0f,
            1.15f, -0.25f, 0.0f,
            -1.15f, -0.25f, 0.0f,
            1.15f, -0.20f, 0.0f,
            -1.15f, -0.20f, 0.0f,
            1.15f, -0.15f, 0.0f,
            -1.15f, -0.15f, 0.0f,
            1.15f, -0.10f, 0.0f,
            -1.15f, -0.10f, 0.0f,
            1.15f, -0.05f, 0.0f,
            -1.15f, -0.05f, 0.0f,
            //26
            1.15f, 0.0f, 0.0f,
            -1.15f, 0.0f, 0.0f,
            1.15f, 0.05f, 0.0f,
            -1.15f, 0.05f, 0.0f,
            1.15f, 0.10f, 0.0f,
            -1.15f, 0.10f, 0.0f,
            1.15f, 0.15f, 0.0f,
            -1.15f, 0.15f, 0.0f,
            1.15f, 0.20f, 0.0f,
            -1.15f, 0.20f, 0.0f,
            1.15f, 0.25f, 0.0f,
            -1.15f, 0.25f, 0.0f,
            1.15f, 0.30f, 0.0f,
            -1.15f, 0.30f, 0.0f,
            1.15f, 0.35f, 0.0f,
            -1.15f, 0.35f, 0.0f,
            1.15f, 0.40f, 0.0f,
            -1.15f, 0.40f, 0.0f,
            1.15f, 0.45f, 0.0f,
            -1.15f, 0.45f, 0.0f,
            1.15f, 0.50f, 0.0f,
            -1.15f, 0.50f, 0.0f,
            1.15f, 0.55f, 0.0f,
            -1.15f, 0.55f, 0.0f,
            1.15f, 0.60f, 0.0f,
            -1.15f, 0.60f, 0.0f
        };
        
        const int Color = 140;
        GLfloat graphColor[3 * Color];
        for (int i = 0; i < Color; i++)
        {
            graphColor[3 * i] = 0.0f;
            graphColor[3 * i + 1] = 1.0f;
            graphColor[3 * i + 2] = 0.0f;
        }
        
        //GraphVerticesArray
        const  GLfloat shapesVertices[] =
        {
            //Ractangle
            0.97f, 0.7f, 0.0f,
            -0.97f, 0.7f, 0.0f,
            -0.97f, 0.7f, 0.0f,
            -0.97f, -0.7f, 0.0f,
            -0.97f, -0.7f, 0.0f,
            0.97f, -0.7f, 0.0f,
            0.97f, 0.7f, 0.0f,
            0.97f, -0.7f, 0.0f,
            
            //Triangle
            0.0f, 0.7f, 0.0f,
            -0.97f, -0.7f, 0.0f,
            -0.97f, -0.7f, 0.0f,
            0.97f, -0.7f, 0.0f,
            0.97f, -0.7f, 0.0f,
            0.0f, 0.7f, 0.0f
        };
        const int iColors = 14;
        GLfloat shapesiColor[3 * iColors];
        for (int i = 0; i < iColors; i++)
        {
            shapesiColor[3 * i] = 1.0f;
            shapesiColor[3 * i + 1] = 1.0f;
            shapesiColor[3 * i + 2] = 0.0f;
        }

        const int ipoints = 300;
        GLfloat bigCircleVertices[3 * ipoints];
        for (int i = 0; i < ipoints; i++)
        {
            GLfloat Angle = (2.0f * (GLfloat)M_PI * i) / ipoints;
            
            bigCircleVertices[3 * i] = (GLfloat)cos(Angle) * 1.2f;
            bigCircleVertices[3 * i + 1] = (GLfloat)sin(Angle) * 1.2f;
            bigCircleVertices[3 * i + 2] = 0.0f;
        }
        
        const int Colors = 300;
        GLfloat shapesColor[3 * Colors];
        for (int i = 0; i < Colors; i++)
        {
            shapesColor[3 * i] = 1.0f;
            shapesColor[3 * i + 1] = 1.0f;
            shapesColor[3 * i + 2] = 0.0f;
        }
        
        GLfloat smallCircleVertices[3 * ipoints];
        for (int i = 0; i < ipoints; i++)
        {
            GLfloat Angle = (2.0f * (GLfloat)M_PI * i) / ipoints;
            
            smallCircleVertices[3 * i] = (GLfloat)cos(Angle) * 0.507f;
            smallCircleVertices[3 * i + 1] = (GLfloat)sin(Angle) * 0.507f;
            smallCircleVertices[3 * i + 2] = 0.0f;
        }
        
        //Create vao -----> For Graph
        glGenVertexArrays(1, &vao_Graph);
        glBindVertexArray(vao_Graph);
        glGenBuffers(1, &vbo_Graph);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Graph);
        glBufferData(GL_ARRAY_BUFFER, sizeof(graphVertices), graphVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
        //Unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Color
        glGenBuffers(1, &vbo_Color_Graph);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Graph);
        glBufferData(GL_ARRAY_BUFFER, sizeof(graphColor), graphColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        //vao
        glBindVertexArray(0);
        
        
        //Create vao -----> For BigCircle
        glGenVertexArrays(1, &vao_BigCircle);
        glBindVertexArray(vao_BigCircle);
        glGenBuffers(1, &vbo_BigCircle);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_BigCircle);
        glBufferData(GL_ARRAY_BUFFER, sizeof(bigCircleVertices), bigCircleVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
        //Unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Color
        glGenBuffers(1, &vbo_Color_Shapes);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Shapes);
        glBufferData(GL_ARRAY_BUFFER, sizeof(shapesColor), shapesColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        //vao
        glBindVertexArray(0);
        
        //Create vao ----> Rectangle And Triangle
        glGenVertexArrays(1, &vao_Shapes);
        glBindVertexArray(vao_Shapes);
        glGenBuffers(1, &vbo_Shapes);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Shapes);
        glBufferData(GL_ARRAY_BUFFER, sizeof(shapesVertices), shapesVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
        //Unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Color
        glGenBuffers(1, &vbo_Color_Shapes);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Shapes);
        glBufferData(GL_ARRAY_BUFFER, sizeof(shapesiColor), shapesiColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        //vao
        glBindVertexArray(0);
        
        //Create vao-----> For SmallCircle
        glGenVertexArrays(1, &vao_SmallCircle);
        glBindVertexArray(vao_SmallCircle);
        glGenBuffers(1, &vbo_SmallCircle);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_SmallCircle);
        glBufferData(GL_ARRAY_BUFFER, sizeof(smallCircleVertices), smallCircleVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
        //Unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Color
        glGenBuffers(1, &vbo_Color_Shapes);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Shapes);
        glBufferData(GL_ARRAY_BUFFER, sizeof(shapesColor), shapesColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        //vao
        glBindVertexArray(0);

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
       // glEnable(GL_CULL_FACE);
        
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        
        perspectiveProjectionMatrix = vmath::mat4::identity();        //clearColor
        
        //GESTURE RECOGNITION
        //Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer  = [[UITapGestureRecognizer  alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; //touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will alloc to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);}

/*
 only override drawRect: if you perform custom drawing.
 An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)drawRect{
    //Dawing code
}

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

- (void)drawView:(id)sender
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    
    
    //Declatation of Matricex
    vmath::mat4 translationMatrix;
    vmath::mat4 modelViewMatrix;
    vmath::mat4 modelViewProjectionMatrix;
    //For Graph
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    
    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, 0.0f, -1.6f);
    
    //Do Necessary Matrix Multiplication
    modelViewMatrix = translationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glLineWidth(0.001f);
    glBindVertexArray(vao_Graph);
    //Draw the Necessary Scnes
    glDrawArrays(GL_LINES, 0, 140);
    
    //UnBind vao
    glBindVertexArray(0);
    
    //For Shapes
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    
    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
    
    //Do Necessary Matrix Multiplication
    modelViewMatrix = translationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    
    glBindVertexArray(vao_Shapes);
    glLineWidth(5.0f);
    //Draw the Necessary Scnes
    glDrawArrays(GL_LINES, 0, 8);
    glDrawArrays(GL_LINES, 8, 14);
    //UnBind vao
    glBindVertexArray(0);
    
    //For Big Circle
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    
    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
    
    //Do Necessary Matrix Multiplication
    modelViewMatrix = translationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    
    glBindVertexArray(vao_BigCircle);
    glLineWidth(5.0f);
    //Draw the Necessary Scnes
    glDrawArrays(GL_LINES, 0, 300);
    //UnBind vao
    glBindVertexArray(0);
    
    //For Small Circle
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    
    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, -0.193f, -3.0f);
    
    //Do Necessary Matrix Multiplication
    modelViewMatrix = translationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    
    glBindVertexArray(vao_SmallCircle);
    glLineWidth(5.0f);
    //Draw the Necessary Scnes
    glDrawArrays(GL_LINES, 0, 300);
    //UnBind vao
    glBindVertexArray(0);


    //UnUse Program
    glUseProgram(0);
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

- (void)layoutSubviews
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0, 0, width, height);
    
    //glOrtho(left,right,bottom, top, near, far);
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)fwidth / (GLfloat)fheight, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

- (void) myStartAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating = YES;
    }
}

- (void) myStopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating = NO;
    }
}

//to become first responder
- (BOOL)acceptsFirstResponder
{
    //code
    return(YES);
}

- (void)touchesBegin:(NSSet *)touches withEvent:(UIEvent *)withEvent
{
    
}

- (void)onSingleTap:(UITapGestureRecognizer *)gr
{
    
}

- (void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
}

- (void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    [self release];
    exit(0);
}

- (void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

- (void)dealloc
{
    //code
    
    //code
    if (vbo_Color_Shapes)
    {
        glDeleteBuffers(1, &vbo_Color_Shapes);
        vbo_Color_Shapes = 0;
    }
    
    if (vbo_SmallCircle)
    {
        glDeleteBuffers(1, &vbo_SmallCircle);
        vbo_SmallCircle = 0;
    }
    
    if (vbo_Shapes)
    {
        glDeleteBuffers(1, &vbo_Shapes);
        vbo_Shapes = 0;
    }
    
    if (vbo_BigCircle)
    {
        glDeleteBuffers(1, &vbo_BigCircle);
        vbo_BigCircle = 0;
    }
    
    if (vbo_Color_Graph)
    {
        glDeleteBuffers(1, &vbo_Color_Graph);
        vbo_Color_Graph = 0;
    }
    
    if (vbo_Graph)
    {
        glDeleteBuffers(1, &vbo_Graph);
        vbo_Graph = 0;
    }
    
    
    
    if (vao_SmallCircle)
    {
        glDeleteVertexArrays(1, &vao_SmallCircle);
        vao_SmallCircle = 0;
    }
    
    if (vao_Shapes)
    {
        glDeleteVertexArrays(1, &vao_Shapes);
        vao_Shapes = 0;
    }
    
    if (vao_BigCircle)
    {
        glDeleteVertexArrays(1, &vao_BigCircle);
        vao_BigCircle = 0;
    }
    
    if (vao_Graph)
    {
        glDeleteVertexArrays(1, &vao_Graph);
        vao_Graph = 0;
    }
    
    if (shaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;
        
        glUseProgram(shaderProgramObject);
        //Ask Program How Many Shaders are Attached to you
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(shaderProgramObject, shaderCount, &shaderCount, pShaders);
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(shaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
        glUseProgram(0);
    }
    
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer = 0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer = 0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
