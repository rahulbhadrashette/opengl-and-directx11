//
//  GLESView.h
//  03-Ortho
//
//  Created by Rahul Bhadrashette on 19/01/20.
//

#import <UIKit/UIKit.h>

@interface GLESView : UIView <UIGestureRecognizerDelegate>

@end
