//
//  GLESView.m
//  03-Ortho
//
//  Created by Rahul Bhadrashette on 19/01/20.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "./vmath.h"

#import "GLESView.h"
#import "Sphere.h"

enum
{
    AMC_ATTRIBUTE_VERTEX = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    BOOL ToggleShaderKey;
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint gShaderProgramObject_PV;
    GLuint gShaderProgramObject_PF;

    struct Light
    {
        GLfloat Ambient[4];
        GLfloat Diffuse[4];
        GLfloat Specular[4];
        GLfloat Position[4];
    };

    struct  Light light[3];

    GLfloat materialAmbient[4];//Ka
    GLfloat materialDiffuse[4];//Kd
    GLfloat materialSpecular[4];//Ks
    GLfloat materialShininess[1];//128.0f;

    GLuint vao_Sphere;
    GLuint vbo_Position_Sphere;
    GLuint vbo_Normal_Sphere;
    GLuint vbo_Element_Sphere;

    //For Sphere
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    short sphere_elements[2280];

    float gNumVertices;
    float gNumElements;

    bool gbLight;
    GLint changeKeyPressed;
    GLfloat Co1;
    GLfloat Co2;

    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    vmath::mat4 projectionMatrix;

    //******************* vertex Variables ***********
    GLuint modelUniform;
    GLuint viewUniform;
    GLuint projectionUniform;

    GLuint LaUniform_Red;
    GLuint LdUniform_Red;
    GLuint LsUniform_Red;
    GLuint lightPositionUniform_Red;

    GLuint LaUniform_Green;
    GLuint LdUniform_Green;
    GLuint LsUniform_Green;
    GLuint lightPositionUniform_Green;

    GLuint LaUniform_Blue;
    GLuint LdUniform_Blue;
    GLuint LsUniform_Blue;
    GLuint lightPositionUniform_Blue;

    GLuint KaUniform;
    GLuint KdUniform;
    GLuint KsUniform;

    GLuint materialShininessUniform;
    GLuint lKeyIsPressedUniform;

    //********** fragment Variables *******
    GLuint modelUniform_RMB;
    GLuint viewUniform_RMB;
    GLuint projectionUniform_RMB;

    GLuint LaUniform_Red_RMB;
    GLuint LdUniform_Red_RMB;
    GLuint LsUniform_Red_RMB;
    GLuint lightPositionUniform_Red_RMB;

    GLuint LaUniform_Green_RMB;
    GLuint LdUniform_Green_RMB;
    GLuint LsUniform_Green_RMB;
    GLuint lightPositionUniform_Green_RMB;

    GLuint LaUniform_Blue_RMB;
    GLuint LdUniform_Blue_RMB;
    GLuint LsUniform_Blue_RMB;
    GLuint lightPositionUniform_Blue_RMB;

    GLuint KaUniform_RMB;
    GLuint KdUniform_RMB;
    GLuint KsUniform_RMB;

    GLuint materialShininessUniform_RMB;
    GLuint lKeyIsPressedUniform_RMB;
}

- (id)initWithFrame:(CGRect)frame;
{
    //code
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed To Create Complete Framebuufer Object %x...!!! \n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        isAnimating = NO;
        animationFrameInterval = 60; //default since iOS 8.2
        
        [self perVertexShaderFunction];
        [self perFragmentShaderFunction];
        
        getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        gNumVertices = getNumberOfSphereVertices();
        gNumElements = getNumberOfSphereElements();
        
        //Position
        glGenVertexArrays(1, &vao_Sphere);
        glBindVertexArray(vao_Sphere);
        glGenBuffers(1, &vbo_Position_Sphere);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Sphere);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Normal
        glGenBuffers(1, &vbo_Normal_Sphere);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Normal_Sphere);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Element vbo
        glGenBuffers(1, &vbo_Element_Sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
       // glEnable(GL_CULL_FACE);
        
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        
        projectionMatrix = vmath::mat4::identity();        //clearColor
        
        //GESTURE RECOGNITION
        //Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer  = [[UITapGestureRecognizer  alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; //touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will alloc to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}

-(void)perVertexShaderFunction
{
    //** VERTEX SHADER
    //Define Vertex Shader Object
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    //Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode =
    "#version 300 es" \
    "\n" \
    "precision highp float;" \
    "in vec4 vertexPosition;" \
    "in vec3 vertexNormal;" \

    "uniform mat4 u_modelMatrix;" \
    "uniform mat4 u_viewMatrix;" \
    "uniform mat4 u_projection_matrix;" \

    "uniform vec3 u_La_Red;" \
    "uniform vec3 u_Ld_Red;" \
    "uniform vec3 u_Ls_Red;" \

    "uniform vec3 u_La_Green;" \
    "uniform vec3 u_Ld_Green;" \
    "uniform vec3 u_Ls_Green;" \

    "uniform vec3 u_La_Blue;" \
    "uniform vec3 u_Ld_Blue;" \
    "uniform vec3 u_Ls_Blue;" \

    "uniform vec3 u_Ka;" \
    "uniform vec3 u_Kd;" \
    "uniform vec3 u_Ks;" \

    "uniform float u_materialShininess;" \
    "uniform bool u_lKeyIsPressed;" \

    "uniform vec4 u_Light_Position_Red;" \
    "uniform vec4 u_Light_Position_Green;" \
    "uniform vec4 u_Light_Position_Blue;" \

    "out vec3 phong_AdsLight;" \

    "void main(void)" \
    "{" \
        "if(u_lKeyIsPressed)" \
        "{" \
            "vec4 eye_coordinate = u_viewMatrix * u_modelMatrix * vertexPosition;" \
            "vec3 tnorm = normalize(mat3(u_viewMatrix * u_modelMatrix) * vertexNormal);" \
            //For Red Light
            "vec3 lightdirection_Red = normalize(vec3(u_Light_Position_Red - eye_coordinate));" \

            "float tn_dot_lightDir_Red = max(dot(lightdirection_Red, tnorm), 0.0);" \

            "vec3 reflectionVector_Red = reflect(-lightdirection_Red, tnorm);" \

            "vec3 viewerVector = normalize(vec3(-eye_coordinate.xyz));" \

            "vec3 ambient_Red = u_La_Red * u_Ka;" \

            "vec3 diffuse_Red = u_Ld_Red * u_Kd * tn_dot_lightDir_Red;" \

            "vec3 specular_Red = u_Ls_Red * u_Ks * pow(max(dot(reflectionVector_Red, viewerVector), 0.0), u_materialShininess);" \

            //For Green Light
            "vec3 lightdirection_Green = normalize(vec3(u_Light_Position_Green - eye_coordinate));" \

            "float tn_dot_lightDir_Green = max(dot(lightdirection_Green, tnorm), 0.0);" \

            "vec3 reflectionVector_Green = reflect(-lightdirection_Green, tnorm);" \

            "vec3 ambient_Green = u_La_Green * u_Ka;" \

            "vec3 diffuse_Green = u_Ld_Green * u_Kd * tn_dot_lightDir_Green;" \

            "vec3 specular_Green = u_Ls_Green * u_Ks * pow(max(dot(reflectionVector_Green, viewerVector), 0.0), u_materialShininess);" \

            //For Blue Light
            "vec3 lightdirection_Blue = normalize(vec3(u_Light_Position_Blue - eye_coordinate));" \

            "float tn_dot_lightDir_Blue = max(dot(lightdirection_Blue, tnorm), 0.0);" \

            "vec3 reflectionVector_Blue = reflect(-lightdirection_Blue, tnorm);" \

            "vec3 ambient_Blue = u_La_Blue * u_Ka;" \

            "vec3 diffuse_Blue = u_Ld_Blue * u_Kd * tn_dot_lightDir_Blue;" \

            "vec3 specular_Blue = u_Ls_Blue * u_Ks * pow(max(dot(reflectionVector_Blue, viewerVector), 0.0), u_materialShininess);" \

            "phong_AdsLight = ambient_Red + diffuse_Red + specular_Red + ambient_Green + diffuse_Green + specular_Green + ambient_Blue + diffuse_Blue + specular_Blue;" \

        "}" \
        "else" \
        "{" \
                "phong_AdsLight = vec3(1.0, 1.0, 1.0);" \
        "}" \

            "gl_Position = u_projection_matrix * u_viewMatrix * u_modelMatrix * vertexPosition;" \
    "}";

    //"mat3 normalmatrix = mat3(transpose(inverse(u_mv_matrix)))" \   OR "mat3 normalmatrix = mat3(u_mv_matrix);" \

    //Specifing above source to the vertex shader object
    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    //Compile the Vertex Shader
    glCompileShader(vertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("vertexShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }

    //Define Fragment Shader Object
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //Write Fragment Shader Code
    const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec3 phong_AdsLight;" \
        "out vec4 fragColor;" \
        "void main(void)" \
        "{" \
            "fragColor = vec4(phong_AdsLight, 1.0);" \
        "}";

    // Specifing above Source to the fragment Shader Object
    glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

    //Compile the fragment Shader Object
    glCompileShader(fragmentShaderObject);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("FragmentShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }

    //Create Shader Object
    gShaderProgramObject_PV = glCreateProgram();

    //Attach VertexShader to the Shader Program
    glAttachShader(gShaderProgramObject_PV, vertexShaderObject);

    //Attach fragmentShader to the Shader Program
    glAttachShader(gShaderProgramObject_PV, fragmentShaderObject);

    // Pre Linking Binding to Vertex Attribute
    glBindAttribLocation(gShaderProgramObject_PV, AMC_ATTRIBUTE_VERTEX, "vertexPosition");
    glBindAttribLocation(gShaderProgramObject_PV, AMC_ATTRIBUTE_NORMAL, "vertexNormal");

    //Link the Shader Program
    glLinkProgram(gShaderProgramObject_PV);

    GLint  iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(gShaderProgramObject_PV, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject_PV, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject_PV, iInfoLogLength, &written, szInfoLog);
                printf("LinkShaderProgramObject:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }

    //Post Linking Retriving Uniform Location
    modelUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_modelMatrix");
    viewUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_viewMatrix");
    projectionUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_projection_matrix");

    LaUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_La_Red");
    LdUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_Ld_Red");
    LsUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_Ls_Red");

    LaUniform_Green = glGetUniformLocation(gShaderProgramObject_PV, "u_La_Green");
    LdUniform_Green = glGetUniformLocation(gShaderProgramObject_PV, "u_Ld_Green");
    LsUniform_Green = glGetUniformLocation(gShaderProgramObject_PV, "u_Ls_Green");

    LaUniform_Blue = glGetUniformLocation(gShaderProgramObject_PV, "u_La_Blue");
    LdUniform_Blue = glGetUniformLocation(gShaderProgramObject_PV, "u_Ld_Blue");
    LsUniform_Blue = glGetUniformLocation(gShaderProgramObject_PV, "u_Ls_Blue");

    KaUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_Ka");
    KdUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_Kd");
    KsUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_Ks");

    materialShininessUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_materialShininess");
    lightPositionUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_Light_Position_Red");
    lightPositionUniform_Green = glGetUniformLocation(gShaderProgramObject_PV, "u_Light_Position_Green");
    lightPositionUniform_Blue = glGetUniformLocation(gShaderProgramObject_PV, "u_Light_Position_Blue");
    lKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_lKeyIsPressed");
}

-(void)perFragmentShaderFunction
{
    //** VERTEX SHADER
    //Define Vertex Shader Object
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    //Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec4 vertexPosition_RMB;" \
        "in vec3 vertexNormal_RMB;" \

        "uniform mat4 u_modelMatrix_RMB;" \
        "uniform mat4 u_viewMatrix_RMB;" \
        "uniform mat4 u_projection_matrix_RMB;" \

        "uniform bool u_lKeyIsPressed_RMB;" \
        "uniform vec4 u_Light_Position_Red_RMB;" \
        "uniform vec4 u_Light_Position_Green_RMB;" \
        "uniform vec4 u_Light_Position_Blue_RMB;" \

        "out vec3 tnorm_RMB;" \
        "out vec3 lightdirection_Red_RMB;" \
        "out vec3 lightdirection_Green_RMB;" \
        "out vec3 lightdirection_Blue_RMB;" \
        "out vec3 viewerVector_RMB;" \

        "void main(void)" \
        "{" \
            "if(u_lKeyIsPressed_RMB)" \
            "{" \
                "vec4 eye_coordinate_RMB = u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" \
                "tnorm_RMB = mat3(u_viewMatrix_RMB * u_modelMatrix_RMB) * vertexNormal_RMB;" \
                //For Red Light Calculations
                "lightdirection_Red_RMB = vec3(u_Light_Position_Red_RMB - eye_coordinate_RMB);" \
                //For Green Light Calculations
                "lightdirection_Green_RMB = vec3(u_Light_Position_Green_RMB - eye_coordinate_RMB);" \
                //For Blue Light Calculations
                "lightdirection_Blue_RMB = vec3(u_Light_Position_Blue_RMB - eye_coordinate_RMB);" \

                "viewerVector_RMB = vec3(-eye_coordinate_RMB.xyz);" \
            "}" \

            "gl_Position = u_projection_matrix_RMB * u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" \
        "}";

    //"mat3 normalmatrix = mat3(transpose(inverse(u_mv_matrix)))" \   OR "mat3 normalmatrix = mat3(u_mv_matrix);" \

    //Specifing above source to the vertex shader object
    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    //Compile the Vertex Shader
    glCompileShader(vertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("vertexShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }

    //Define Fragment Shader Object
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //Write Fragment Shader Code
    const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec3 tnorm_RMB;" \
        "in vec3 lightdirection_Red_RMB;" \
        "in vec3 lightdirection_Green_RMB;" \
        "in vec3 lightdirection_Blue_RMB;" \
        "in vec3 viewerVector_RMB;" \

        "uniform vec3 u_La_Red_RMB;" \
        "uniform vec3 u_Ld_Red_RMB;" \
        "uniform vec3 u_Ls_Red_RMB;" \

        "uniform vec3 u_La_Green_RMB;" \
        "uniform vec3 u_Ld_Green_RMB;" \
        "uniform vec3 u_Ls_Green_RMB;" \

        "uniform vec3 u_La_Blue_RMB;" \
        "uniform vec3 u_Ld_Blue_RMB;" \
        "uniform vec3 u_Ls_Blue_RMB;" \

        "uniform vec3 u_Ka_RMB;" \
        "uniform vec3 u_Kd_RMB;" \
        "uniform vec3 u_Ks_RMB;" \

        "uniform float u_materialShininess_RMB;" \
        "uniform bool u_lKeyIsPressed_RMB;" \

        
        "out vec4 fragColor_RMB;" \

        "void main(void)" \
        "{" \
            "vec3 phong_AdsLight_RMB;" \
            "if(u_lKeyIsPressed_RMB)" \
            "{" \
                //For Red Light Calculations
                "vec3 normalize_tnorm_RMB = normalize(tnorm_RMB);" \

                "vec3 normalize_lightdirection_Red_RMB = normalize(lightdirection_Red_RMB);" \

                "vec3 normalize_viewerVector_RMB = normalize(viewerVector_RMB);" \

                "vec3 reflectionVector_Red_RMB = reflect(-normalize_lightdirection_Red_RMB, normalize_tnorm_RMB);" \

                "float tn_dot_lightDir_Red_RMB = max(dot(normalize_lightdirection_Red_RMB, normalize_tnorm_RMB), 0.0);" \

                "vec3 ambient_Red_RMB = u_La_Red_RMB * u_Ka_RMB;" \

                "vec3 diffuse_Red_RMB = u_Ld_Red_RMB * u_Kd_RMB * tn_dot_lightDir_Red_RMB;" \

                "vec3 specular_Red_RMB = u_Ls_Red_RMB * u_Ks_RMB * pow(max(dot(reflectionVector_Red_RMB, normalize_viewerVector_RMB), 0.0), u_materialShininess_RMB);" \

                //For Green Light Calculations
                "vec3 normalize_lightdirection_Green_RMB = normalize(lightdirection_Green_RMB);" \

                "vec3 reflectionVector_Green_RMB = reflect(-normalize_lightdirection_Green_RMB, normalize_tnorm_RMB);" \

                "float tn_dot_lightDir_Green_RMB = max(dot(normalize_lightdirection_Green_RMB, normalize_tnorm_RMB), 0.0);" \

                "vec3 ambient_Green_RMB = u_La_Green_RMB * u_Ka_RMB;" \

                "vec3 diffuse_Green_RMB = u_Ld_Green_RMB * u_Kd_RMB * tn_dot_lightDir_Green_RMB;" \

                "vec3 specular_Green_RMB = u_Ls_Green_RMB * u_Ks_RMB * pow(max(dot(reflectionVector_Green_RMB, normalize_viewerVector_RMB), 0.0), u_materialShininess_RMB);" \

                //For Blue Light Calculations
                "vec3 normalize_lightdirection_Blue_RMB = normalize(lightdirection_Blue_RMB);" \

                "vec3 reflectionVector_Blue_RMB = reflect(-normalize_lightdirection_Blue_RMB, normalize_tnorm_RMB);" \

                "float tn_dot_lightDir_Blue_RMB = max(dot(normalize_lightdirection_Blue_RMB, normalize_tnorm_RMB), 0.0);" \

                "vec3 ambient_Blue_RMB = u_La_Blue_RMB * u_Ka_RMB;" \

                "vec3 diffuse_Blue_RMB = u_Ld_Blue_RMB * u_Kd_RMB * tn_dot_lightDir_Blue_RMB;" \

                "vec3 specular_Blue_RMB = u_Ls_Blue_RMB * u_Ks_RMB * pow(max(dot(reflectionVector_Blue_RMB, normalize_viewerVector_RMB), 0.0), u_materialShininess_RMB);" \

                "phong_AdsLight_RMB = ambient_Red_RMB + diffuse_Red_RMB + specular_Red_RMB + ambient_Green_RMB + diffuse_Green_RMB + specular_Green_RMB + ambient_Blue_RMB + diffuse_Blue_RMB + specular_Blue_RMB;" \

            "}" \
            "else" \
            "{" \

                "phong_AdsLight_RMB = vec3(1.0, 1.0, 1.0);" \

            "}" \

            "fragColor_RMB = vec4(phong_AdsLight_RMB, 1.0);" \
        "}";


    // Specifing above Source to the fragment Shader Object
    glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

    //Compile the fragment Shader Object
    glCompileShader(fragmentShaderObject);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("FragmentShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }

    //Create Shader Object
    gShaderProgramObject_PF = glCreateProgram();

    //Attach VertexShader to the Shader Program
    glAttachShader(gShaderProgramObject_PF, vertexShaderObject);

    //Attach fragmentShader to the Shader Program
    glAttachShader(gShaderProgramObject_PF, fragmentShaderObject);

    // Pre Linking Binding to Vertex Attribute
   glBindAttribLocation(gShaderProgramObject_PF, AMC_ATTRIBUTE_VERTEX, "vertexPosition_RMB");
    glBindAttribLocation(gShaderProgramObject_PF, AMC_ATTRIBUTE_NORMAL, "vertexNormal_RMB");

    //Link the Shader Program
    glLinkProgram(gShaderProgramObject_PF);

    GLint  iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(gShaderProgramObject_PF, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject_PF, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject_PF, iInfoLogLength, &written, szInfoLog);
                printf("LinkShaderProgramObject:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }

    //Post Linking Retriving Uniform Location
    modelUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_modelMatrix_RMB");
    viewUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_viewMatrix_RMB");
    projectionUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_projection_matrix_RMB");

    LaUniform_Red_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_La_Red_RMB");
    LdUniform_Red_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ld_Red_RMB");
    LsUniform_Red_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ls_Red_RMB");

    LaUniform_Green_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_La_Green_RMB");
    LdUniform_Green_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ld_Green_RMB");
    LsUniform_Green_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ls_Green_RMB");

    LaUniform_Blue_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_La_Blue_RMB");
    LdUniform_Blue_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ld_Blue_RMB");
    LsUniform_Blue_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ls_Blue_RMB");

    KaUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ka_RMB");
    KdUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Kd_RMB");
    KsUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ks_RMB");

    materialShininessUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_materialShininess_RMB");
    lightPositionUniform_Red_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Light_Position_Red_RMB");
    lightPositionUniform_Green_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Light_Position_Green_RMB");
    lightPositionUniform_Blue_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Light_Position_Blue_RMB");
    lKeyIsPressedUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_lKeyIsPressed_RMB");
}

/*
 only override drawRect: if you perform custom drawing.
 An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)drawRect{
    //Dawing code
}

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

- (void)drawView:(id)sender
{
    //code
    static GLfloat angleRotation = 0.0f;
    [self Light_Array];
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    if(ToggleShaderKey)
    {
        glUseProgram(gShaderProgramObject_PF);
        if (gbLight)
        {
            glUniform1i(lKeyIsPressedUniform_RMB, 1);
            glUniform3fv(LaUniform_Red_RMB, 1, light[0].Ambient);
            glUniform3fv(LdUniform_Red_RMB, 1, light[0].Diffuse);
            glUniform3fv(LsUniform_Red_RMB, 1, light[0].Specular);
            glUniform4fv(lightPositionUniform_Red_RMB, 1, light[0].Position);

            glUniform3fv(LaUniform_Green_RMB, 1, light[1].Ambient);
            glUniform3fv(LdUniform_Green_RMB, 1, light[1].Diffuse);
            glUniform3fv(LsUniform_Green_RMB, 1, light[1].Specular);
            glUniform4fv(lightPositionUniform_Green_RMB, 1, light[1].Position);

            glUniform3fv(LaUniform_Blue_RMB, 1, light[2].Ambient);
            glUniform3fv(LdUniform_Blue_RMB, 1, light[2].Diffuse);
            glUniform3fv(LsUniform_Blue_RMB, 1, light[2].Specular);
            glUniform4fv(lightPositionUniform_Blue_RMB, 1, light[2].Position);

            glUniform3fv(KaUniform_RMB, 1, materialAmbient);
            glUniform3fv(KdUniform_RMB, 1, materialDiffuse);
            glUniform3fv(KsUniform_RMB, 1, materialSpecular);
            glUniform1fv(materialShininessUniform_RMB, 1, materialShininess);
        }
        else
        {
            glUniform1i(lKeyIsPressedUniform, 0);
        }
    }
    else
    {
        glUseProgram(gShaderProgramObject_PV);
        if (gbLight)
        {
            glUniform1i(lKeyIsPressedUniform, 1);
            glUniform3fv(LaUniform_Red, 1, light[0].Ambient);
            glUniform3fv(LdUniform_Red, 1, light[0].Diffuse);
            glUniform3fv(LsUniform_Red, 1, light[0].Specular);
            glUniform4fv(lightPositionUniform_Red, 1, light[0].Position);

            glUniform3fv(LaUniform_Green, 1, light[1].Ambient);
            glUniform3fv(LdUniform_Green, 1, light[1].Diffuse);
            glUniform3fv(LsUniform_Green, 1, light[1].Specular);
            glUniform4fv(lightPositionUniform_Green, 1, light[1].Position);

            glUniform3fv(LaUniform_Blue, 1, light[2].Ambient);
            glUniform3fv(LdUniform_Blue, 1, light[2].Diffuse);
            glUniform3fv(LsUniform_Blue, 1, light[2].Specular);
            glUniform4fv(lightPositionUniform_Blue, 1, light[2].Position);

            glUniform3fv(KaUniform, 1, materialAmbient);
            glUniform3fv(KdUniform, 1, materialDiffuse);
            glUniform3fv(KsUniform, 1, materialSpecular);
            glUniform1fv(materialShininessUniform, 1, materialShininess);
        }
        else
        {
            glUniform1i(lKeyIsPressedUniform_RMB, 0);
        }
    }
    

    //Declatation of Matricex
    vmath::mat4 translationMatrix;
    vmath::mat4 rotationMatrix;
    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    
    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    
    
    //Do Necessary Matrix Multiplication
    modelMatrix = modelMatrix * translationMatrix;

    //Send Necessary Matrix to Shader in Respective Uniform
    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, projectionMatrix);

    glUniformMatrix4fv(modelUniform_RMB, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(viewUniform_RMB, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionUniform_RMB, 1, GL_FALSE, projectionMatrix);

    //BindWith vao
    glBindVertexArray(vao_Sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
    //Draw the Necessary Scnes
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //UnBind vao
    glBindVertexArray(0);

    //UnUse Program
    glUseProgram(0);
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];

     angleRotation = angleRotation + 0.01f;
    if (angleRotation > 360.0f)
    {
        angleRotation = 0.0f;
    }
    
    Co1 = (GLfloat)cos(angleRotation) * 100.0f;
    Co2 = (GLfloat)sin(angleRotation) * 100.0f;
}

- (void)Light_Array
{
        //Array Zero
    light[0].Ambient[0] = 0.0f;
    light[0].Ambient[1] = 0.0f;
    light[0].Ambient[2] = 0.0f;
    light[0].Ambient[3] = 1.0f;

    light[0].Diffuse[0] = 1.0f;
    light[0].Diffuse[1] = 0.0f;
    light[0].Diffuse[2] = 0.0f;
    light[0].Diffuse[3] = 1.0f;

    light[0].Specular[0] = 1.0f;
    light[0].Specular[1] = 0.0f;
    light[0].Specular[2] = 0.0f;
    light[0].Specular[3] = 1.0f;

    light[0].Position[0] = 0.0f;
    light[0].Position[1] = Co1;
    light[0].Position[2] = Co2;
    light[0].Position[3] = 1.0f;

    //Array One
    light[1].Ambient[0] = 0.0f;
    light[1].Ambient[1] = 0.0f;
    light[1].Ambient[2] = 0.0f;
    light[1].Ambient[3] = 1.0f;
    light[1].Diffuse[0] = 0.0f;
    light[1].Diffuse[1] = 1.0f;
    light[1].Diffuse[2] = 0.0f;
    light[1].Diffuse[3] = 1.0f;
    light[1].Specular[0] = 0.0f;
    light[1].Specular[1] = 1.0f;
    light[1].Specular[2] = 0.0f;
    light[1].Specular[3] = 1.0f;
    light[1].Position[0] = Co1;
    light[1].Position[1] = 0.0f;
    light[1].Position[2] = Co2;
    light[1].Position[3] = 1.0f;

    //Array Two
    light[2].Ambient[0] = 0.0f;
    light[2].Ambient[1] = 0.0f;
    light[2].Ambient[2] = 0.0f;
    light[2].Ambient[3] = 1.0f;
    light[2].Diffuse[0] = 0.0f;
    light[2].Diffuse[1] = 0.0f;
    light[2].Diffuse[2] = 1.0f;
    light[2].Diffuse[3] = 1.0f;
    light[2].Specular[0] = 0.0f;
    light[2].Specular[1] = 0.0f;
    light[2].Specular[2] = 1.0f;
    light[2].Specular[3] = 1.0f;
    light[2].Position[0] = Co1;
    light[2].Position[1] = Co2;
    light[2].Position[2] = 0.0f;
    light[2].Position[3] = 1.0f;

    //For Material
    materialAmbient[0] = 0.0f;
    materialAmbient[1] = 0.0f; 
    materialAmbient[2] = 0.0f;
    materialAmbient[3] = 0.0f;

    materialDiffuse[0] = 0.5f;
    materialDiffuse[1] = 0.2f;
    materialDiffuse[2] = 0.7f;
    materialDiffuse[3] = 1.0f;

    materialSpecular[0] = 1.0f;
    materialSpecular[1] = 1.0f;
    materialSpecular[2] = 1.0f;
    materialSpecular[3] = 1.0f;

    materialShininess[0] = 128.0f;
}

- (void)layoutSubviews
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0, 0, width, height);
    
    //glOrtho(left,right,bottom, top, near, far);
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    
    projectionMatrix = vmath::perspective(45.0f, (GLfloat)fwidth / (GLfloat)fheight, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

- (void) myStartAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating = YES;
    }
}

- (void) myStopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating = NO;
    }
}

//to become first responder
- (BOOL)acceptsFirstResponder
{
    //code
    return(YES);
}

- (void)touchesBegin:(NSSet *)touches withEvent:(UIEvent *)withEvent
{
    
}

- (void)onSingleTap:(UITapGestureRecognizer *)gr
{
    gbLight = !gbLight;
}

- (void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    ToggleShaderKey = !ToggleShaderKey;
}

- (void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    [self release];
    exit(0);
}

- (void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

- (void)dealloc
{
    //code
    
    //code
    if (vbo_Element_Sphere)
    {
        glDeleteBuffers(1, &vbo_Element_Sphere);
        vbo_Element_Sphere = 0;
    }
    
    if (vbo_Position_Sphere)
    {
        glDeleteBuffers(1, &vbo_Position_Sphere);
        vbo_Position_Sphere = 0;
    }
    
    if (vbo_Normal_Sphere)
    {
        glDeleteBuffers(1, &vbo_Normal_Sphere);
        vbo_Normal_Sphere = 0;
    }
    
    if (vao_Sphere)
    {
        glDeleteVertexArrays(1, &vao_Sphere);
        vao_Sphere = 0;
    }
    
    if (gShaderProgramObject_PV)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(gShaderProgramObject_PV);
        //Ask Program How Many Shaders are Attached to you
        glGetProgramiv(gShaderProgramObject_PV, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(gShaderProgramObject_PV, shaderCount, &shaderCount, pShaders);
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(gShaderProgramObject_PV, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gShaderProgramObject_PV);
        gShaderProgramObject_PV = 0;
        glUseProgram(0);
    }

    if (gShaderProgramObject_PF)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(gShaderProgramObject_PF);
        //Ask Program How Many Shaders are Attached to you
        glGetProgramiv(gShaderProgramObject_PF, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(gShaderProgramObject_PF, shaderCount, &shaderCount, pShaders);
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(gShaderProgramObject_PF, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gShaderProgramObject_PF);
        gShaderProgramObject_PF = 0;
        glUseProgram(0);
    }
    
    
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer = 0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer = 0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
