//
//  GLESView.m
//  03-Ortho
//
//  Created by Rahul Bhadrashette on 19/01/20.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "./vmath.h"

#import "GLESView.h"
#import "Sphere.h"

enum
{
    AMC_ATTRIBUTE_VERTEX = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao_Sphere;
    GLuint vbo_Position_Sphere;
    GLuint vbo_Normal_Sphere;
    GLuint vbo_Element_Sphere;
    
    //For Sphere
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    short sphere_elements[2280];
    
    float gNumVertices;
    float gNumElements;
    
    bool gbLight;
    
    GLuint LdUniform;
    GLuint KdUniform;
    GLuint modelViewUniform;
    GLuint prespectiveProjectionUniform;
    GLuint lightPositionUniform;
    GLuint lKeyIsPressedUniform;

    vmath::mat4 perspectiveProjectionMatrix;
    vmath::mat4 modelViewMatrix;
}

- (id)initWithFrame:(CGRect)frame;
{
    //code
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed To Create Complete Framebuufer Object %x...!!! \n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        isAnimating = NO;
        animationFrameInterval = 60; //default since iOS 8.2
        
        //Define Vertex Shader Object
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        //Write Vertex Shader Code
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vertexPosition;" \
        "in vec3 vertexNormal;" \
        
        "uniform mat4 u_modelViewMatrix;" \
        "uniform mat4 u_Projection_matrix;" \
        
        "uniform bool u_lKeyIsPressed;" \
        
        "uniform vec3 u_Ld;" \
        "uniform vec3 u_Kd;" \
        
        "uniform vec4 u_Light_Position;" \
        
        "out vec3 diffuse_Color;" \
        
        "void main(void)" \
        "{" \
        "if(u_lKeyIsPressed)" \
        "{" \
        "vec4 eye_coordinate = u_modelViewMatrix * vertexPosition;" \
        
        "mat3 normalmatrix = mat3(transpose(inverse(u_modelViewMatrix)));" \
        
        "vec3 tnorm = normalize(normalmatrix * vertexNormal);" \
        
        "vec3 source = normalize(vec3(u_Light_Position - eye_coordinate));" \
        
        "diffuse_Color = u_Ld * u_Kd * dot(source, tnorm);" \
        "}" \
        "gl_Position = u_Projection_matrix * u_modelViewMatrix * vertexPosition;" \
        "}";
        
        //Specifing above source to the vertex shader object
        glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
        
        //Compile the Vertex Shader
        glCompileShader(vertexShaderObject);
        
        GLint iShaderCompileStatus = 0;
        GLint iInfoLogLength = 0;
        GLchar *szInfoLog = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if (iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (GLchar*)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("vertex Shader Compilation Log:- %s", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //Define Fragment Shader Object
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        //Write Fragment Shader Code
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec3 diffuse_Color;" \
        "out vec4 fragColor;" \
        "uniform bool u_lKeyIsPressed;" \
        "void main(void)" \
        "{" \
        "if(u_lKeyIsPressed)" \
        "{" \
        "fragColor = vec4(diffuse_Color, 1.0);" \
        "}" \
        "else" \
        "{" \
        "fragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
        "}" \
        "}";

        // Specifing above Source to the fragment Shader Object
        glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
        
        //Compile the fragment Shader Object
        glCompileShader(fragmentShaderObject);
        
        iShaderCompileStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if (iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (GLchar*)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log:- %s \n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //Create Shader Object
        shaderProgramObject = glCreateProgram();
        
        //Attach VertexShader to the Shader Program
        glAttachShader(shaderProgramObject, vertexShaderObject);
        
        //Attach fragmentShader to the Shader Program
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        // Pre Linking Binding to Vertex Attribute
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_VERTEX, "vertexPosition");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vertexNormal");
        

        //Link the Shader Program
        glLinkProgram(shaderProgramObject);
        
        GLint iProgramLinkStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
        if (iProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength)
            {
                szInfoLog = (GLchar*)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("LinkShaderProgramObject:- %s", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //Post Linking Retriving Uniform Location
        modelViewUniform = glGetUniformLocation(shaderProgramObject, "u_modelViewMatrix");
        prespectiveProjectionUniform = glGetUniformLocation(shaderProgramObject, "u_Projection_matrix");
        LdUniform = glGetUniformLocation(shaderProgramObject, "u_Ld");
        KdUniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
        lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_Light_Position");
        lKeyIsPressedUniform = glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");
        
        getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        gNumVertices = getNumberOfSphereVertices();
        gNumElements = getNumberOfSphereElements();
        
        //Position
        glGenVertexArrays(1, &vao_Sphere);
        glBindVertexArray(vao_Sphere);
        glGenBuffers(1, &vbo_Position_Sphere);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Sphere);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Normal
        glGenBuffers(1, &vbo_Normal_Sphere);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Normal_Sphere);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Element vbo
        glGenBuffers(1, &vbo_Element_Sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
       // glEnable(GL_CULL_FACE);
        
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        
        perspectiveProjectionMatrix = vmath::mat4::identity();        //clearColor
        
        //GESTURE RECOGNITION
        //Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer  = [[UITapGestureRecognizer  alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; //touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will alloc to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);}

/*
 only override drawRect: if you perform custom drawing.
 An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)drawRect{
    //Dawing code
}

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

- (void)drawView:(id)sender
{
    //code
    static float angle = 0.0f;

    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    
    if (gbLight)
    {
        glUniform1i(lKeyIsPressedUniform, 1);
        glUniform3f(LdUniform, 1.0f, 1.0f, 1.0f); //White Light
        glUniform3f(KdUniform, 0.5f, 0.5f, 0.5f); //Gray Material
        glUniform4f(lightPositionUniform, 0.0f, 0.0f, 2.0f, 1.0f);
    }
    else
    {
        glUniform1i(lKeyIsPressedUniform, 0);
    }
    
    //Declatation of Matricex
    vmath::mat4 modelViewMatrix;
    vmath::mat4 translationMatrix;
    vmath::mat4 scaleMatrix;
    vmath::mat4 rotationMatrix;
    
    //****************.  Sun ********************
    //Initialize of Matrix in identity
    modelViewMatrix = vmath::mat4::identity();
    translationMatrix = vmath::mat4::identity();
    scaleMatrix = vmath::mat4::identity();
    //rotationMatrix = vmath::mat4::identity();
    
    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    scaleMatrix = vmath::scale(0.60f, 0.60f, 0.60f);
    //rotationMatrix = vmath::rotate(angle, 0.0f, 1.0f, 0.0f);
    
    //Do Necessary Matrix Multiplication
    modelViewMatrix = modelViewMatrix * translationMatrix * scaleMatrix * rotationMatrix;
    //perspectiveProjectionMatrix;
    
    //Send Necessary Matrix to Shader in Respective Uniform
    glUniformMatrix4fv(modelViewUniform, 1, GL_FALSE, modelViewMatrix);
    glUniformMatrix4fv(prespectiveProjectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    
    //BindWith vao
    glBindVertexArray(vao_Sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
    //Draw the Necessary Scnes
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //UnBind vao
    glBindVertexArray(0);
    
    //****************** Earth *********************
    //Initialize of Matrix in identity
    //modelViewMatrix = vmath::mat4::identity();
    //modelViewMatrix = vmath::mat4::identity();
    scaleMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    
    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, 0.0f, -1.0f);
    scaleMatrix = vmath::scale(0.40f, 0.40f, 0.40f);
    rotationMatrix = vmath::rotate(angle, 0.0f, 1.0f, 0.0f);
    
    //Do Necessary Matrix Multiplication
    modelViewMatrix = modelViewMatrix * translationMatrix * scaleMatrix * rotationMatrix;
    //perspectiveProjectionMatrix;
    
    //Send Necessary Matrix to Shader in Respective Uniform
    glUniformMatrix4fv(modelViewUniform, 1, GL_FALSE, modelViewMatrix);
    glUniformMatrix4fv(prespectiveProjectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    
    //BindWith vao
    glBindVertexArray(vao_Sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
    //Draw the Necessary Scnes
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //UnBind vao
    glBindVertexArray(0);
    
    //******************* Moon ***********************
    //Initialize of Matrix in identity
    //modelViewMatrix = vmath::mat4::identity();
    translationMatrix = vmath::mat4::identity();
    scaleMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    
    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, 0.0f, -0.8f);
    scaleMatrix = vmath::scale(0.35f, 0.35f, 0.35f);
    rotationMatrix = vmath::rotate(angle, 0.0f, 1.0f, 0.0f);
    
    //Do Necessary Matrix Multiplication
    modelViewMatrix = modelViewMatrix * translationMatrix * scaleMatrix * rotationMatrix;
    //perspectiveProjectionMatrix;
    
    //Send Necessary Matrix to Shader in Respective Uniform
    glUniformMatrix4fv(modelViewUniform, 1, GL_FALSE, modelViewMatrix);
    glUniformMatrix4fv(prespectiveProjectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    
    //BindWith vao
    glBindVertexArray(vao_Sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
    //Draw the Necessary Scnes
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //UnBind vao
    glBindVertexArray(0);
    
    glUseProgram(0);
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
    angle = angle + 0.6f;
    if (angle >= 360.0f)
    {
        angle = 0.0f;
    }
    
}

- (void)layoutSubviews
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0, 0, width, height);
    
    //glOrtho(left,right,bottom, top, near, far);
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)fwidth / (GLfloat)fheight, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

- (void) myStartAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating = YES;
    }
}

- (void) myStopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating = NO;
    }
}

//to become first responder
- (BOOL)acceptsFirstResponder
{
    //code
    return(YES);
}

- (void)touchesBegin:(NSSet *)touches withEvent:(UIEvent *)withEvent
{
    
}

- (void)onSingleTap:(UITapGestureRecognizer *)gr
{
    gbLight = !gbLight;
}

- (void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
}

- (void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    [self release];
    exit(0);
}

- (void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

- (void)dealloc
{
    //code
    
    //code
    if (vbo_Element_Sphere)
    {
        glDeleteBuffers(1, &vbo_Element_Sphere);
        vbo_Element_Sphere = 0;
    }
    
    if (vbo_Position_Sphere)
    {
        glDeleteBuffers(1, &vbo_Position_Sphere);
        vbo_Position_Sphere = 0;
    }
    
    if (vbo_Normal_Sphere)
    {
        glDeleteBuffers(1, &vbo_Normal_Sphere);
        vbo_Normal_Sphere = 0;
    }
    
    if (vao_Sphere)
    {
        glDeleteVertexArrays(1, &vao_Sphere);
        vao_Sphere = 0;
    }
    
    if (shaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;
        
        glUseProgram(shaderProgramObject);
        //Ask Program How Many Shaders are Attached to you
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(shaderProgramObject, shaderCount, &shaderCount, pShaders);
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(shaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
        glUseProgram(0);
    }
    
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer = 0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer = 0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
