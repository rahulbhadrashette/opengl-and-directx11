//
//  GLESView.m
//  03-Ortho
//
//  Created by Rahul Bhadrashette on 19/01/20.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "./vmath.h"

#import "GLESView.h"

enum
{
    AMC_ATTRIBUTE_VERTEX = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    bool gbAnimation;
    bool gbLight;
    
    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    vmath::mat4 projectionMatrix;
    
    GLuint modelUniform;
    GLuint viewUniform;
    GLuint projectionUniform;
    
    GLuint LaUniform_Red;
    GLuint LdUniform_Red;
    GLuint LsUniform_Red;
    GLuint lightPositionUniform_Red;
    
    GLuint LaUniform_Blue;
    GLuint LdUniform_Blue;
    GLuint LsUniform_Blue;
    GLuint lightPositionUniform_Blue;
    
    GLuint KaUniform;
    GLuint KdUniform;
    GLuint KsUniform;
    
    GLuint materialShininessUniform;
    GLuint lKeyIsPressedUniform;
    
    struct Light
    {
        GLfloat Ambient[4];
        GLfloat Diffuse[4];
        GLfloat Specular[4];
        GLfloat Position[4];
    };
    
    struct  Light light[2];
    
    GLfloat materialAmbient[4];// = { 0.0f, 0.0f, 0.0f, 0.0f }; //Ka
    GLfloat materialDiffuse[4]; ///= { 1.0f, 1.0f, 1.0f, 1.0f };  //Kd
    GLfloat materialSpecular[4];// = { 1.0f, 1.0f, 1.0f, 1.0f };  //Ks
    GLfloat materialShininess[1];// = { 50.0f }; //128.0f;
    
    GLuint vao_Pyramid;
    GLuint vbo_Position_Pyramid;
    GLuint vbo_Normal_Pyramid;
}

- (id)initWithFrame:(CGRect)frame;
{
    //code
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed To Create Complete Framebuufer Object %x...!!! \n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        isAnimating = NO;
        animationFrameInterval = 60; //default since iOS 8.2
        
        //Define Vertex Shader Object
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        //Write Vertex Shader Code
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vertexPosition;" \
        "in vec3 vertexNormal;" \
        
        "uniform mat4 u_modelMatrix;" \
        "uniform mat4 u_viewMatrix;" \
        "uniform mat4 u_projection_matrix;" \
        
        "uniform vec3 u_La_Red;" \
        "uniform vec3 u_Ld_Red;" \
        "uniform vec3 u_Ls_Red;" \
        
        "uniform vec3 u_La_Blue;" \
        "uniform vec3 u_Ld_Blue;" \
        "uniform vec3 u_Ls_Blue;" \
        
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Kd;" \
        "uniform vec3 u_Ks;" \
        
        "uniform float u_materialShininess;" \
        "uniform bool u_lKeyIsPressed;" \
        "uniform vec4 u_Light_Position_Red;" \
        "uniform vec4 u_Light_Position_Blue;" \
        
        "out vec3 phong_AdsLight;" \
        
        "void main(void)" \
        "{" \
        "if(u_lKeyIsPressed)" \
        "{" \
        "vec4 eye_coordinate = u_viewMatrix * u_modelMatrix * vertexPosition;" \
        
        "vec3 tnorm = normalize(mat3(u_viewMatrix * u_modelMatrix) * vertexNormal);" \
        //For Red Light
        "vec3 lightdirection_Red = normalize(vec3(u_Light_Position_Red - eye_coordinate));" \
        
        "float tn_dot_lightDir_Red = max(dot(lightdirection_Red, tnorm), 0.0);" \
        
        "vec3 reflectionVector_Red = reflect(-lightdirection_Red, tnorm);" \
        
        "vec3 viewerVector = normalize(vec3(-eye_coordinate.xyz));" \
        
        "vec3 ambient_Red = u_La_Red * u_Ka;" \
        
        "vec3 diffuse_Red = u_Ld_Red * u_Kd * tn_dot_lightDir_Red;" \
        
        "vec3 specular_Red = u_Ls_Red * u_Ks * pow(max(dot(reflectionVector_Red, viewerVector), 0.0), u_materialShininess);" \
        
        //For Blue Light
        "vec3 lightdirection_Blue = normalize(vec3(u_Light_Position_Blue - eye_coordinate));" \
        
        "float tn_dot_lightDir_Blue = max(dot(lightdirection_Blue, tnorm), 0.0);" \
        
        "vec3 reflectionVector_Blue = reflect(-lightdirection_Blue, tnorm);" \
        
        "vec3 ambient_Blue = u_La_Blue * u_Ka;" \
        
        "vec3 diffuse_Blue = u_Ld_Blue * u_Kd * tn_dot_lightDir_Blue;" \
        
        "vec3 specular_Blue = u_Ls_Blue * u_Ks * pow(max(dot(reflectionVector_Blue, viewerVector), 0.0), u_materialShininess);" \
        
        "phong_AdsLight = ambient_Red + diffuse_Red + specular_Red + ambient_Blue + diffuse_Blue + specular_Blue;" \
        
        "}" \
        "else" \
        "{" \
        "phong_AdsLight = vec3(1.0, 1.0, 1.0);" \
        "}" \
        
        "gl_Position = u_projection_matrix * u_viewMatrix * u_modelMatrix * vertexPosition;" \
        "}";


        
        //Specifing above source to the vertex shader object
        glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
        
        //Compile the Vertex Shader
        glCompileShader(vertexShaderObject);
        
        GLint iShaderCompileStatus = 0;
        GLint iInfoLogLength = 0;
        GLchar *szInfoLog = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if (iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (GLchar*)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("vertex Shader Compilation Log:- %s", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //Define Fragment Shader Object
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        //Write Fragment Shader Code
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec3 phong_AdsLight;" \
        "out vec4 fragColor;" \
        "void main(void)" \
        "{" \
        "fragColor = vec4(phong_AdsLight, 1.0);" \
        "}";
        
        // Specifing above Source to the fragment Shader Object
        glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
        
        //Compile the fragment Shader Object
        glCompileShader(fragmentShaderObject);
        
        iShaderCompileStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if (iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (GLchar*)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log:- %s \n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //Create Shader Object
        shaderProgramObject = glCreateProgram();
        
        //Attach VertexShader to the Shader Program
        glAttachShader(shaderProgramObject, vertexShaderObject);
        
        //Attach fragmentShader to the Shader Program
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        // Pre Linking Binding to Vertex Attribute
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_VERTEX, "vertexPosition");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vertexNormal");
        

        //Link the Shader Program
        glLinkProgram(shaderProgramObject);
        
        GLint iProgramLinkStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
        if (iProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength)
            {
                szInfoLog = (GLchar*)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("LinkShaderProgramObject:- %s", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //Post Linking Retriving Uniform Location
        //Post Linking Retriving Uniform Location
        //Post Linking Retriving Uniform Location
        modelUniform = glGetUniformLocation(shaderProgramObject, "u_modelMatrix");
        viewUniform = glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
        projectionUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
        
        LaUniform_Red = glGetUniformLocation(shaderProgramObject, "u_La_Red");
        LdUniform_Red = glGetUniformLocation(shaderProgramObject, "u_Ld_Red");
        LsUniform_Red = glGetUniformLocation(shaderProgramObject, "u_Ls_Red");
        
        LaUniform_Blue = glGetUniformLocation(shaderProgramObject, "u_La_Blue");
        LdUniform_Blue = glGetUniformLocation(shaderProgramObject, "u_Ld_Blue");
        LsUniform_Blue = glGetUniformLocation(shaderProgramObject, "u_Ls_Blue");
        
        KaUniform = glGetUniformLocation(shaderProgramObject, "u_Ka");
        KdUniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
        KsUniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
        
        materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_materialShininess");
        lightPositionUniform_Red = glGetUniformLocation(shaderProgramObject, "u_Light_Position_Red");
        lightPositionUniform_Blue = glGetUniformLocation(shaderProgramObject, "u_Light_Position_Blue");
        lKeyIsPressedUniform = glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");
        
        const GLfloat pyramidVertices[] =
        {
            //FRONT FACE
            0.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            //LEFT FACE
            0.0f, 1.0f, 0.0f,
            1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,
            //BACK FACE
            0.0f, 1.0f, 0.0f,
            1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            //RIGHT FACE
            0.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f
        };
        
        const GLfloat pyramidNormal[] =
        {
            0.0f, 0.447214f, 0.894427f,
            0.0f, 0.447214f, 0.894427f,
            0.0f, 0.447214f, 0.894427f,
            
            0.89427f, 0.447214f, 0.0f,
            0.89427f, 0.447214f, 0.0f,
            0.89427f, 0.447214f, 0.0f,
            
            0.0f, 0.447214f, -0.894427f,
            0.0f, 0.447214f, -0.894427f,
            0.0f, 0.447214f, -0.894427f,
            
            -0.894427f, 0.447214f, 0.0f,
            -0.894427f, 0.447214f, 0.0f,
            -0.894427f, 0.447214f, 0.0f
        };
        
        //Position
        glGenVertexArrays(1, &vao_Pyramid);
        glBindVertexArray(vao_Pyramid);
        glGenBuffers(1, &vbo_Position_Pyramid);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Pyramid);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Normal
        glGenBuffers(1, &vbo_Normal_Pyramid);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_Normal_Pyramid);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormal), pyramidNormal, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glBindVertexArray(0);


        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
       // glEnable(GL_CULL_FACE);
        
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        
       projectionMatrix = vmath::mat4::identity();        //clearColor
        
        //GESTURE RECOGNITION
        //Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer  = [[UITapGestureRecognizer  alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; //touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will alloc to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);}

/*
 only override drawRect: if you perform custom drawing.
 An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)drawRect{
    //Dawing code
}

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

- (void)drawView:(id)sender
{
    //codestatic
    static float anglePyramid = 0.0f;
    [self Light_Array];
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    
    if (gbLight)
    {
        glUniform1i(lKeyIsPressedUniform, 1);
        glUniform3fv(LaUniform_Red, 1, light[0].Ambient);
        glUniform3fv(LdUniform_Red, 1, light[0].Diffuse);
        glUniform3fv(LsUniform_Red, 1, light[0].Specular);
        glUniform4fv(lightPositionUniform_Red, 1, light[0].Position);
        
        glUniform3fv(LaUniform_Blue, 1, light[1].Ambient);
        glUniform3fv(LdUniform_Blue, 1, light[1].Diffuse);
        glUniform3fv(LsUniform_Blue, 1, light[1].Specular);
        glUniform4fv(lightPositionUniform_Blue, 1, light[1].Position);
        
        glUniform3fv(KaUniform, 1, materialAmbient);
        glUniform3fv(KdUniform, 1, materialDiffuse);
        glUniform3fv(KsUniform, 1, materialSpecular);
        glUniform1fv(materialShininessUniform, 1, materialShininess);
    }
    else
    {
        glUniform1i(lKeyIsPressedUniform, 0);
    }
    
    //Declatation of Matricex
    vmath::mat4 translationMatrix;
    vmath::mat4 rotationMatrix;
    
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    
    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
    rotationMatrix = vmath::rotate(anglePyramid, 0.0f, 1.0f, 0.0f);
    
    //Do Necessary Matrix Multiplication
    modelMatrix = modelMatrix * translationMatrix * rotationMatrix;
    
    //Send Necessary Matrix to Shader in Respective Uniform
    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, projectionMatrix);
    
    //BindWith vao
    glBindVertexArray(vao_Pyramid);
    //Draw the Necessary Scnes
    glDrawArrays(GL_TRIANGLES, 0,12);
    
    //UnBind vao
    glBindVertexArray(0);
    //UnUse Program
    glUseProgram(0);
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
    if (gbAnimation)
    {
        anglePyramid = anglePyramid + 0.6f;
        if (anglePyramid >= 360.0f)
        {
            anglePyramid = 0.0f;
        }
    }
    
}

- (void)Light_Array
{
    //Array Zero
    light[0].Ambient[0] = 0.0f;
    light[0].Ambient[1] = 0.0f;
    light[0].Ambient[2] = 0.0f;
    light[0].Ambient[3] = 1.0f;
    light[0].Diffuse[0] = 1.0f;
    light[0].Diffuse[1] = 0.0f;
    light[0].Diffuse[2] = 0.0f;
    light[0].Diffuse[3] = 1.0f;
    light[0].Specular[0] = 1.0f;
    light[0].Specular[1] = 0.0f;
    light[0].Specular[2] = 0.0f;
    light[0].Specular[3] = 1.0f;
    light[0].Position[0] = -2.0f;
    light[0].Position[1] = 0.0f;
    light[0].Position[2] = 0.0f;
    light[0].Position[3] = 1.0f;
    
    //Array One
    light[1].Ambient[0] = 0.0f;
    light[1].Ambient[1] = 0.0f;
    light[1].Ambient[2] = 0.0f;
    light[1].Ambient[3] = 1.0f;
    light[1].Diffuse[0] = 0.0f;
    light[1].Diffuse[1] = 0.0f;
    light[1].Diffuse[2] = 1.0f;
    light[1].Diffuse[3] = 1.0f;
    light[1].Specular[0] = 0.0f;
    light[1].Specular[1] = 0.0f;
    light[1].Specular[2] = 1.0f;
    light[1].Specular[3] = 1.0f;
    light[1].Position[0] = 2.0f;
    light[1].Position[1] = 0.0f;
    light[1].Position[2] = 0.0f;
    light[1].Position[3] = 1.0f;
    
    materialAmbient[0] = 0.0f;
    materialAmbient[1] = 0.0f;
    materialAmbient[2] = 0.0f;
    materialAmbient[3] = 0.0f;
    materialDiffuse[0] = 1.0f;
    materialDiffuse[1] = 1.0f;
    materialDiffuse[2] = 1.0f;
    materialDiffuse[3] = 1.0f;
    materialSpecular[0] = 1.0f;
    materialSpecular[1] = 1.0f;
    materialSpecular[2] = 1.0f;
    materialSpecular[3] = 1.0f;
    materialShininess[0] = 50.0f;

}

- (void)layoutSubviews
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0, 0, width, height);
    
    //glOrtho(left,right,bottom, top, near, far);
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    
    projectionMatrix = vmath::perspective(45.0f, (GLfloat)fwidth / (GLfloat)fheight, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

- (void) myStartAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating = YES;
    }
}

- (void) myStopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating = NO;
    }
}

//to become first responder
- (BOOL)acceptsFirstResponder
{
    //code
    return(YES);
}

- (void)touchesBegin:(NSSet *)touches withEvent:(UIEvent *)withEvent
{
    
}

- (void)onSingleTap:(UITapGestureRecognizer *)gr
{
    gbLight = !gbLight;
}

- (void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    gbAnimation = !gbAnimation;
}

- (void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    [self release];
    exit(0);
}

- (void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

- (void)dealloc
{
    //code
    
    //code
    if (vbo_Position_Pyramid)
    {
        glDeleteBuffers(1, &vbo_Position_Pyramid);
        vbo_Position_Pyramid = 0;
    }
    
    if (vbo_Normal_Pyramid)
    {
        glDeleteBuffers(1, &vbo_Normal_Pyramid);
        vbo_Normal_Pyramid = 0;
    }
    
    if (vao_Pyramid)
    {
        glDeleteVertexArrays(1, &vao_Pyramid);
        vao_Pyramid = 0;
    }
    
    if (shaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;
        
        glUseProgram(shaderProgramObject);
        //Ask Program How Many Shaders are Attached to you
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(shaderProgramObject, shaderCount, &shaderCount, pShaders);
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(shaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
        glUseProgram(0);
    }
    
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer = 0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer = 0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
