//
//  main.m
//  19-TwoLightsOnPyramid
//
//  Created by Sachin Bhadrashette on 24/01/20.
//  Copyright © 2020 Rahul Bhadrashette. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
