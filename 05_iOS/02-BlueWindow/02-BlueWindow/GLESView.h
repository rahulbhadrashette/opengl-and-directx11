//
//  GLESView.h
//  02-BlueWindow
//
//  Created by Sachin Bhadrashette on 19/01/20.
//

#import <UIKit/UIKit.h>

@interface GLESView : UIView <UIGestureRecognizerDelegate>
@end
