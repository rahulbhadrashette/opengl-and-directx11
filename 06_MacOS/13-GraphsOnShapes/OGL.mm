#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags, CVOptionFlags *, void *);

//global variable
FILE *gpFile=NULL;

//interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate> 
@end

@interface GLView : NSOpenGLView
@end

//entry point function
int main (int argc, const char* argv[]) {
    //code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc] init];

    NSApp=[NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc] init]];

    [NSApp run];

    [pPool release];
    return(0);
}

@implementation AppDelegate
{
    @private 
        NSWindow *window;
        GLView *glView;
}

- (void) applicationDidFinishLaunching :(NSNotification *)aNotification
{
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

    const char *pszLogFileNameWithPath= [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath,"w");

    if(gpFile==NULL)
    {
        printf("Can not open log file.\nExitting ...\n");
        [self release];
        [NSApp terminate:self];
    }

    fprintf(gpFile, "Program is started successfully.\n");

    NSRect win_rect;
    win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

    window = [[NSWindow alloc] initWithContentRect:win_rect
                                styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                    backing:NSBackingStoreBuffered
                                        defer:NO];

    [window setTitle:@"OpenGL Perspective Window...RahulBhadrashette...!!!"];

    [window center];

    glView=[[GLView alloc] initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

- (void) applicationWillTerminate :(NSNotification *) notification
{
    //code
    fprintf(gpFile, "Program is terminated successfully\n");
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
    //code
    [NSApp terminate:self];
}

- (void) dealloc
{
    [glView release];
    [window release];
    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint vao_Graph;
    GLuint vbo_Graph;
    GLuint vbo_Color_Graph;
    GLuint vao_Shapes;
    GLuint vbo_Shapes;
    GLuint vbo_Color_Shapes;
    GLuint vao_BigCircle;
    GLuint vbo_BigCircle;
    GLuint vao_SmallCircle;
    GLuint vbo_SmallCircle;
    GLuint mvpUniform;
    vmath::mat4 perspectiveProjectionMatrix;
}

- (id)initWithFrame:(NSRect)frame 
{
    self=[super initWithFrame:frame];

    if(self) {
        [[self window] setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };

        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel format is available. Exitting...");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }

    return self;
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *)pOutputTime 
{
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version :%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];


    //** VERTEX SHADER
    //Define Vertex Shader Object
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    //Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec4 out_Color;" \
        "void main(void)" \
        "{" \
            "gl_Position = u_mvp_matrix * vPosition;" \
            "out_Color = vColor;" \
        "}";

    //Specifing above source to the vertex shader object
    glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    //Compile the Vertex Shader
    glCompileShader(gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "vertexShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Define Fragment Shader Object
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //Write Fragment Shader Code
    const GLchar *fragmentShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec4 out_Color;" \
        "out vec4 fragColor;" \
        "void main(void)" \
        "{" \
            "fragColor = out_Color;" \
        "}";

    // Specifing above Source to the fragment Shader Object
    glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

    //Compile the fragment Shader Object
    glCompileShader(gFragmentShaderObject);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "FragmentShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Create Shader Object
    gShaderProgramObject = glCreateProgram();

    //Attach VertexShader to the Shader Program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);

    //Attach fragmentShader to the Shader Program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);

    // Pre Linking Binding to Vertex Attribute
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

    //Link the Shader Program
    glLinkProgram(gShaderProgramObject);

    GLint  iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "LinkShaderProgramObject:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Post Linking Retriving Uniform Location
    mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
    
    //GraphVerticesArray
    const  GLfloat graphVertices[] =
    {
        //Vertical Lines
        //44
        -1.10f, 1.0f, 0.0f,
        -1.10f, -1.0f, 0.0f,
        -1.05f, 1.0f, 0.0f,
        -1.05f, -1.0f, 0.0f,
        -1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,
        -0.95f, 1.0f, 0.0f,
        -0.95f, -1.0f, 0.0f,
        -0.90f, 1.0f, 0.0f,
        -0.90f, -1.0f, 0.0f,
        -0.85f, 1.0f, 0.0f,
        -0.85f, -1.0f, 0.0f,
        -0.80f, 1.0f, 0.0f,
        -0.80f, -1.0f, 0.0f,
        -0.75f, 1.0f, 0.0f,
        -0.75f, -1.0f, 0.0f,
        -0.70f, 1.0f, 0.0f,
        -0.70f, -1.0f, 0.0f,
        -0.65f, 1.0f, 0.0f,
        -0.65f, -1.0f, 0.0f,
        -0.60f, 1.0f, 0.0f,
        -0.60f, -1.0f, 0.0f,
        -0.55f, 1.0f, 0.0f,
        -0.55f, -1.0f, 0.0f,
        -0.50f, 1.0f, 0.0f,
        -0.50f, -1.0f, 0.0f,
        -0.45f, 1.0f, 0.0f,
        -0.45f, -1.0f, 0.0f,
        -0.40f, 1.0f, 0.0f,
        -0.40f, -1.0f, 0.0f,
        -0.35f, 1.0f, 0.0f,
        -0.35f, -1.0f, 0.0f,
        -0.30f, 1.0f, 0.0f,
        -0.30f, -1.0f, 0.0f,
        -0.25f, 1.0f, 0.0f,
        -0.25f, -1.0f, 0.0f,
        -0.20f, 1.0f, 0.0f,
        -0.20f, -1.0f, 0.0f,
        -0.15f, 1.0f, 0.0f,
        -0.15f, -1.0f, 0.0f,
        -0.10f, 1.0f, 0.0f,
        -0.10f, -1.0f, 0.0f,
        -0.05f, 1.0f, 0.0f,
        -0.05f, -1.0f, 0.0f,
        //46
        0.00f, 1.0f, 0.0f,
        0.00f, -1.0f, 0.0f,
        0.05f, 1.0f, 0.0f,
        0.05f, -1.0f, 0.0f,
        0.10f, 1.0f, 0.0f,
        0.10f, -1.0f, 0.0f,
        0.15f, 1.0f, 0.0f,
        0.15f, -1.0f, 0.0f,
        0.20f, 1.0f, 0.0f,
        0.20f, -1.0f, 0.0f,
        0.25f, 1.0f, 0.0f,
        0.25f, -1.0f, 0.0f,
        0.30f, 1.0f, 0.0f,
        0.30f, -1.0f, 0.0f,
        0.35f, 1.0f, 0.0f,
        0.35f, -1.0f, 0.0f,
        0.40f, 1.0f, 0.0f,
        0.40f, -1.0f, 0.0f,
        0.45f, 1.0f, 0.0f,
        0.45f, -1.0f, 0.0f,
        0.50f, 1.0f, 0.0f,
        0.50f, -1.0f, 0.0f,
        0.55f, 1.0f, 0.0f,
        0.55f, -1.0f, 0.0f,
        0.60f, 1.0f, 0.0f,
        0.60f, -1.0f, 0.0f,
        0.65f, 1.0f, 0.0f,
        0.65f, -1.0f, 0.0f,
        0.70f, 1.0f, 0.0f,
        0.70f, -1.0f, 0.0f,
        0.75f, 1.0f, 0.0f,
        0.75f, -1.0f, 0.0f,
        0.80f, 1.0f, 0.0f,
        0.80f, -1.0f, 0.0f,
        0.85f, 1.0f, 0.0f,
        0.85f, -1.0f, 0.0f,
        0.90f, 1.0f, 0.0f,
        0.90f, -1.0f, 0.0f,
        0.95f, 1.0f, 0.0f,
        0.95f, -1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,
        1.05f, 1.0f, 0.0f,
        1.05f, -1.0f, 0.0f,
        1.10f, 1.0f, 0.0f,
        1.10f, -1.0f, 0.0f,

        //Horizantal Lines  
        //24
        1.15f, -0.60f, 0.0f,
        -1.15f, -0.60f, 0.0f,
        1.15f, -0.55f, 0.0f,
        -1.15f, -0.55f, 0.0f,
        1.15f, -0.50f, 0.0f,
        -1.15f, -0.50f, 0.0f,
        1.15f, -0.45f, 0.0f,
        -1.15f, -0.45f, 0.0f,
        1.15f, -0.40f, 0.0f,
        -1.15f, -0.40f, 0.0f,
        1.15f, -0.35f, 0.0f,
        -1.15f, -0.35f, 0.0f,
        1.15f, -0.30f, 0.0f,
        -1.15f, -0.30f, 0.0f,
        1.15f, -0.25f, 0.0f,
        -1.15f, -0.25f, 0.0f,
        1.15f, -0.20f, 0.0f,
        -1.15f, -0.20f, 0.0f,
        1.15f, -0.15f, 0.0f,
        -1.15f, -0.15f, 0.0f,
        1.15f, -0.10f, 0.0f,
        -1.15f, -0.10f, 0.0f,
        1.15f, -0.05f, 0.0f,
        -1.15f, -0.05f, 0.0f,
        //26
        1.15f, 0.0f, 0.0f,
        -1.15f, 0.0f, 0.0f,
        1.15f, 0.05f, 0.0f,
        -1.15f, 0.05f, 0.0f,
        1.15f, 0.10f, 0.0f,
        -1.15f, 0.10f, 0.0f,
        1.15f, 0.15f, 0.0f,
        -1.15f, 0.15f, 0.0f,
        1.15f, 0.20f, 0.0f,
        -1.15f, 0.20f, 0.0f,
        1.15f, 0.25f, 0.0f,
        -1.15f, 0.25f, 0.0f,
        1.15f, 0.30f, 0.0f,
        -1.15f, 0.30f, 0.0f,
        1.15f, 0.35f, 0.0f,
        -1.15f, 0.35f, 0.0f,
        1.15f, 0.40f, 0.0f,
        -1.15f, 0.40f, 0.0f,
        1.15f, 0.45f, 0.0f,
        -1.15f, 0.45f, 0.0f,
        1.15f, 0.50f, 0.0f,
        -1.15f, 0.50f, 0.0f,
        1.15f, 0.55f, 0.0f,
        -1.15f, 0.55f, 0.0f,
        1.15f, 0.60f, 0.0f,
        -1.15f, 0.60f, 0.0f
    };

    //GraphVerticesArray
    const  GLfloat shapesVertices[] =
    {
        //Ractangle
        0.97f, 0.7f, 0.0f,
        -0.97f, 0.7f, 0.0f,
        -0.97f, 0.7f, 0.0f,
        -0.97f, -0.7f, 0.0f,
        -0.97f, -0.7f, 0.0f,
        0.97f, -0.7f, 0.0f,
        0.97f, 0.7f, 0.0f,
        0.97f, -0.7f, 0.0f,

        //Triangle
        0.0f, 0.7f, 0.0f,
        -0.97f, -0.7f, 0.0f,
        -0.97f, -0.7f, 0.0f,
        0.97f, -0.7f, 0.0f,
        0.97f, -0.7f, 0.0f,
        0.0f, 0.7f, 0.0f
    };

    const int ipoints = 300;
    GLfloat bigCircleVertices[3 * ipoints];
    for (int i = 0; i < ipoints; i++)
    {
        GLfloat Angle = (2.0f * (GLfloat)M_PI * i) / ipoints;

        bigCircleVertices[3 * i] = (GLfloat)cos(Angle) * 1.2f;
        bigCircleVertices[3 * i + 1] = (GLfloat)sin(Angle) * 1.2f;
        bigCircleVertices[3 * i + 2] = 0.0f;
    }

    GLfloat smallCircleVertices[3 * ipoints];
    for (int i = 0; i < ipoints; i++)
    {
        GLfloat Angle = (2.0f * (GLfloat)M_PI * i) / ipoints;

        smallCircleVertices[3 * i] = (GLfloat)cos(Angle) * 0.507f;
        smallCircleVertices[3 * i + 1] = (GLfloat)sin(Angle) * 0.507f;
        smallCircleVertices[3 * i + 2] = 0.0f;
    }

    const int Color = 140;
    GLfloat graphColor[3 * Color];
    for (int i = 0; i < Color; i++)
    {
        graphColor[3 * i] = 0.0f;
        graphColor[3 * i + 1] = 1.0f;
        graphColor[3 * i + 2] = 0.0f;
    }
    
    const int Colors = 300;
    GLfloat shapesColor[3 * Colors];
    for (int i = 0; i < Colors; i++)
    {
        shapesColor[3 * i] = 1.0f;
        shapesColor[3 * i + 1] = 1.0f;
        shapesColor[3 * i + 2] = 0.0f;
    }

    //Create vao -----> For Graph
    glGenVertexArrays(1, &vao_Graph);
    glBindVertexArray(vao_Graph);
    glGenBuffers(1, &vbo_Graph);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Graph);
    glBufferData(GL_ARRAY_BUFFER, sizeof(graphVertices), graphVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    //Unbind 
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Color
    glGenBuffers(1, &vbo_Color_Graph);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Graph);
    glBufferData(GL_ARRAY_BUFFER, sizeof(graphColor), graphColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //vao
    glBindVertexArray(0);


    //Create vao -----> For BigCircle
    glGenVertexArrays(1, &vao_BigCircle);
    glBindVertexArray(vao_BigCircle);
    glGenBuffers(1, &vbo_BigCircle);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_BigCircle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(bigCircleVertices), bigCircleVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    //Unbind 
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Color
    glGenBuffers(1, &vbo_Color_Shapes);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Shapes);
    glBufferData(GL_ARRAY_BUFFER, sizeof(shapesColor), shapesColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //vao
    glBindVertexArray(0);

    //Create vao ----> Rectangle And Triangle
    glGenVertexArrays(1, &vao_Shapes);
    glBindVertexArray(vao_Shapes);
    glGenBuffers(1, &vbo_Shapes);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Shapes);
    glBufferData(GL_ARRAY_BUFFER, sizeof(shapesVertices), shapesVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    //Unbind 
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Color
    glGenBuffers(1, &vbo_Color_Shapes);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Shapes);
    glBufferData(GL_ARRAY_BUFFER, sizeof(shapesColor), shapesColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //vao
    glBindVertexArray(0);

    //Create vao-----> For SmallCircle
    glGenVertexArrays(1, &vao_SmallCircle);
    glBindVertexArray(vao_SmallCircle);
    glGenBuffers(1, &vbo_SmallCircle);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_SmallCircle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(smallCircleVertices), smallCircleVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    //Unbind 
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Color
    glGenBuffers(1, &vbo_Color_Shapes);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Shapes);
    glBufferData(GL_ARRAY_BUFFER, sizeof(shapesColor), shapesColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //vao
    glBindVertexArray(0);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    //cull back face for better performance as we dont need back as of now
    //glEnable(GL_CULL_FACE);

    //set background color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    //set projection matrix to identity
    perspectiveProjectionMatrix = vmath::mat4::identity();

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback,self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}


-(void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;

    if(height==0)
    {
        height=1;
    }
    
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    
   perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void) drawView
{
    //code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glUseProgram(gShaderProgramObject);

    //Declatation of Matricex
    vmath::mat4 translationMatrix;
    vmath::mat4 modelViewMatrix;
    vmath::mat4 modelViewProjectionMatrix;
    //For Graph
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();

    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, 0.0f, -1.6f);

    //Do Necessary Matrix Multiplication
    modelViewMatrix = translationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glLineWidth(0.001f);
    glBindVertexArray(vao_Graph);
    //Draw the Necessary Scnes
    glDrawArrays(GL_LINES, 0, 500);

    //UnBind vao
    glBindVertexArray(0);

    //For Shapes
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();

    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);

    //Do Necessary Matrix Multiplication
    modelViewMatrix = translationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    
    glBindVertexArray(vao_Shapes);
    glLineWidth(5.0f);
    //Draw the Necessary Scnes
    glDrawArrays(GL_LINES, 0, 500);
    //UnBind vao
    glBindVertexArray(0);

    //For Big Circle
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();

    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);

    //Do Necessary Matrix Multiplication
    modelViewMatrix = translationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    glBindVertexArray(vao_BigCircle);
    glLineWidth(5.0f);
    //Draw the Necessary Scnes
    glDrawArrays(GL_LINES, 0, 500);
    //UnBind vao
    glBindVertexArray(0);

    //For Small Circle
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();

    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, -0.193f, -3.0f);

    //Do Necessary Matrix Multiplication
    modelViewMatrix = translationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    
    glBindVertexArray(vao_SmallCircle);
    glLineWidth(5.0f);
    //Draw the Necessary Scnes
    glDrawArrays(GL_LINES, 0, 500);
    //UnBind vao
    glBindVertexArray(0);

    glUseProgram(0);

    CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (BOOL)acceptsFirstResponder
{
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void) keyDown:(NSEvent *)theEvent
{
    int key=(int) [[theEvent characters]characterAtIndex:0];
    switch(key){
        case 27 : // Esc Key
            [self release];
            [NSApp terminate:self];
            break;
        case 'F' : 
        case 'f' :
            [[self window] toggleFullScreen:self];
            break;
        default:
            break;
    }
}

- (void) mouseDown:(NSEvent *) theEvent
{
    [self setNeedsDisplay:YES];
}

- (void) mouseDragged:(NSEvent *) theEvent
{
    //code
}

- (void) rightMouseDown:(NSEvent *)theEvent
{
    [self setNeedsDisplay:YES];
}

- (void) dealloc
{
    //code
    if (vbo_Color_Shapes)
    {
        glDeleteBuffers(1, &vbo_Color_Shapes);
        vbo_Color_Shapes = 0;
    }

    if (vbo_SmallCircle)
    {
        glDeleteBuffers(1, &vbo_SmallCircle);
        vbo_SmallCircle = 0;
    }

    if (vbo_Shapes)
    {
        glDeleteBuffers(1, &vbo_Shapes);
        vbo_Shapes = 0;
    }

    if (vbo_BigCircle)
    {
        glDeleteBuffers(1, &vbo_BigCircle);
        vbo_BigCircle = 0;
    }

    if (vbo_Color_Graph)
    {
        glDeleteBuffers(1, &vbo_Color_Graph);
        vbo_Color_Graph = 0;
    }

    if (vbo_Graph)
    {
        glDeleteBuffers(1, &vbo_Graph);
        vbo_Graph = 0;
    }



    if (vao_SmallCircle)
    {
        glDeleteVertexArrays(1, &vao_SmallCircle);
        vao_SmallCircle = 0;
    }

    if (vao_Shapes)
    {
        glDeleteVertexArrays(1, &vao_Shapes);
        vao_Shapes = 0;
    }

    if (vao_BigCircle)
    {
        glDeleteVertexArrays(1, &vao_BigCircle);
        vao_BigCircle = 0;
    }

    if (vao_Graph)
    {
        glDeleteVertexArrays(1, &vao_Graph);
        vao_Graph = 0;
    }
    
    glUseProgram(gShaderProgramObject);
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;
    glUseProgram(0);

    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags glagsIn,
                                CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *) pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}