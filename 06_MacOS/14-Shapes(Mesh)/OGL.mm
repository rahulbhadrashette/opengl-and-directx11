#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags, CVOptionFlags *, void *);

//global variable
FILE *gpFile=NULL;

//interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate> 
@end

@interface GLView : NSOpenGLView
@end

//entry point function
int main (int argc, const char* argv[]) {
    //code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc] init];

    NSApp=[NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc] init]];

    [NSApp run];

    [pPool release];
    return(0);
}

@implementation AppDelegate
{
    @private 
        NSWindow *window;
        GLView *glView;
}

- (void) applicationDidFinishLaunching :(NSNotification *)aNotification
{
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

    const char *pszLogFileNameWithPath= [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath,"w");

    if(gpFile==NULL)
    {
        printf("Can not open log file.\nExitting ...\n");
        [self release];
        [NSApp terminate:self];
    }

    fprintf(gpFile, "Program is started successfully.\n");

    NSRect win_rect;
    win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

    window = [[NSWindow alloc] initWithContentRect:win_rect
                                styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                    backing:NSBackingStoreBuffered
                                        defer:NO];

    [window setTitle:@"OpenGL Perspective Window...RahulBhadrashette...!!!"];

    [window center];

    glView=[[GLView alloc] initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

- (void) applicationWillTerminate :(NSNotification *) notification
{
    //code
    fprintf(gpFile, "Program is terminated successfully\n");
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
    //code
    [NSApp terminate:self];
}

- (void) dealloc
{
    [glView release];
    [window release];
    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint vao;
    GLuint vbo;
    GLuint vbo_Color;
    GLuint mvpUniform;
}

- (id)initWithFrame:(NSRect)frame 
{
    self=[super initWithFrame:frame];

    if(self) {
        [[self window] setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };

        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel format is available. Exitting...");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }

    return self;
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *)pOutputTime 
{
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version :%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];


    //** VERTEX SHADER
    //Define Vertex Shader Object
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    //Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec4 out_Color;" \
        "void main(void)" \
        "{" \
            "gl_Position = u_mvp_matrix * vPosition;" \
            "out_Color = vColor;" \
        "}";

    //Specifing above source to the vertex shader object
    glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    //Compile the Vertex Shader
    glCompileShader(gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "vertexShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Define Fragment Shader Object
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //Write Fragment Shader Code
    const GLchar *fragmentShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec4 out_Color;" \
        "out vec4 fragColor;" \
        "void main(void)" \
        "{" \
            "fragColor = out_Color;" \
        "}";

    // Specifing above Source to the fragment Shader Object
    glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

    //Compile the fragment Shader Object
    glCompileShader(gFragmentShaderObject);

    GLint iFShaderCompileStatus = 0;
    GLint iFInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iFShaderCompileStatus);
    if (iFShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iFInfoLogLength);
        if (iFInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iFInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iFInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "FragmentShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Create Shader Object
    gShaderProgramObject = glCreateProgram();

    //Attach VertexShader to the Shader Program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);

    //Attach fragmentShader to the Shader Program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);

    // Pre Linking Binding to Vertex Attribute
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

    //Link the Shader Program
    glLinkProgram(gShaderProgramObject);

    GLint  iProgramLinkStatus = 0;
    GLint iLInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iLInfoLogLength);
        if (iInfoLogLength)
        {
            szInfoLog = (GLchar*)malloc(iLInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iLInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "LinkShaderProgramObject:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Post Linking Retriving Uniform Location
    mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
    
    //TrangleArray
    const  GLfloat verties[] =
    {
        //First
        //right
        0.5f, 0.8f, 0.0f,
        0.8f, 0.8f, 0.0f,
        //top
        0.8f, 0.8f, 0.0f,
        0.8f, 0.3f, 0.0f,
        //left
        0.5f, 0.3f, 0.0f,
        0.8f, 0.3f, 0.0f,
        //botttom
        0.5f, 0.466f, 0.0f,
        0.8f, 0.466f, 0.0f,

        0.5f, 0.632f, 0.0f,
        0.8f, 0.632f, 0.0f,

        0.5f, 0.3f, 0.0f,
        0.5f, 0.8f, 0.0f,

        0.6f, 0.3f, 0.0f,
        0.6f, 0.8f, 0.0f,

        0.7f, 0.3f, 0.0f,
        0.7f, 0.8f, 0.0f,

        //Second
        //top
        -0.15f, 0.8f, 0.0f,
        0.15f, 0.8f, 0.0f,
        //left
        -0.15f, 0.3f, 0.0f,
        -0.15f, 0.8f, 0.0f,
        //horizental
        -0.15f, 0.466f, 0.0f,
        0.15f, 0.466f, 0.0f,
        -0.15f, 0.632f, 0.0f,
        0.15f, 0.632f, 0.0f,
        //vertical 
        -0.05f, 0.3f, 0.0f,
        -0.05f, 0.8f, 0.0f,
        0.05f, 0.3f, 0.0f,
        0.05f, 0.8f, 0.0f,
        //1
        -0.15f, 0.632f, 0.0f,
        -0.05f, 0.8f, 0.0f,
        //2
        -0.15f, 0.466f, 0.0f,
        0.05f, 0.8f, 0.0f,
        //3
        0.15f, 0.8f, 0.0f,
        -0.15f, 0.3f, 0.0f,
        //4
        0.15f, 0.632f, 0.0f,
        -0.05f, 0.3f, 0.0f,
        //5
        0.15f, 0.466f, 0.0f,
        0.05f, 0.3f, 0.0f,

        //Third
        -0.5f, 0.8f, 0.0f,
        -0.8f, 0.8f, 0.0f,

        -0.8f, 0.3f, 0.0f,
        -0.5f, 0.3f, 0.0f,

        -0.5f, 0.466f, 0.0f,
        -0.8f, 0.466f, 0.0f,

        -0.5f, 0.632f, 0.0f,
        -0.8f, 0.632f, 0.0f,

        -0.6f, 0.3f, 0.0f,
        -0.6f, 0.8f, 0.0f,

        -0.7f, 0.3f, 0.0f,
        -0.7f, 0.8f, 0.0f,

        -0.6f, 0.632f, 0.0f,
        -0.7f, 0.632f, 0.0f,

        -0.7f, 0.466f, 0.0f,
        -0.6f, 0.466f, 0.0f,

        //Forth
        //right 
        -0.5f, -0.3f, 0.0f,
        -0.5f, -0.8f, 0.0f,
        //top
        -0.5f, -0.3f, 0.0f,
        -0.8f, -0.3f, 0.0f,
        //left
        -0.8f, -0.8f, 0.0f,
        -0.8f, -0.3f, 0.0f,
        //bottom
        -0.5f, -0.8f, 0.0f,
        -0.8f, -0.8f, 0.0f,
        //vertical line
        -0.5f, -0.466f, 0.0f,
        -0.8f, -0.466f, 0.0f,
        -0.5f, -0.632f, 0.0f,
        -0.8f, -0.632f, 0.0f,
        //horizental Line
        -0.6f, -0.3f, 0.0f,
        -0.6f, -0.8f, 0.0f,
        -0.7f, -0.3f, 0.0f,
        -0.7f, -0.8f, 0.0f,
        //Cross Lines
        //1
        -0.8f, -0.466f, 0.0f,
        -0.7f, -0.3f, 0.0f,
        //2
        -0.8f, -0.632f, 0.0f,
        -0.6f, -0.3f, 0.0f,
        //3
        -0.5f, -0.3f, 0.0f,
        -0.8f, -0.8f, 0.0f,
        //4
        -0.5f, -0.466f, 0.0f,
        -0.7f, -0.8f, 0.0f,
        //5
        -0.6f, -0.8f, 0.0f,
        -0.5f, -0.632f, 0.0f,

        //Five
        //right
        0.15f, -0.8f, 0.0f,
        0.15f, -0.3f, 0.0f,
        //top
        0.15f, -0.3f, 0.0f,
        -0.15f, -0.3f, 0.0f,
        //left
        -0.15f, -0.3f, 0.0f,
        -0.15f, -0.8f, 0.0f,
        //botttom
        -0.15f, -0.8f, 0.0f,
        0.15f, -0.8f, 0.0f,
        //1
        -0.15f, -0.3f, 0.0f,
        0.15f, -0.466f, 0.0f,
        //2
        - 0.15f, -0.3f, 0.0f,
        0.15f, -0.632f, 0.0f,
        //3
        - 0.15f, -0.3f, 0.0f,
        0.15f, -0.8f, 0.0f,
        //4
        - 0.15f, -0.3f, 0.0f,
        - 0.05f, -0.8f, 0.0f,
        //5
        - 0.15f, -0.3f, 0.0f,
        0.05f, -0.8f, 0.0f,

        //For Triangles
        0.6f, -0.8f, 0.0f,
        0.6f, -0.3f, 0.0f,
        0.5f, -0.3f, 0.0f,

        0.5f, -0.3f, 0.0f,
        0.5f, -0.8f, 0.0f,
        0.6f, -0.8f, 0.0f,

        0.7f, -0.8f, 0.0f,
        0.7f, -0.3f, 0.0f,
        0.6f, -0.3f, 0.0f,

        0.6f, -0.3f, 0.0f,
        0.6f, -0.8f, 0.0f,
        0.7f, -0.8f, 0.0f,

        0.8f, -0.8f, 0.0f,
        0.8f, -0.3f, 0.0f,
        0.7f, -0.3f, 0.0f,

        0.7f, -0.3f, 0.0f,
        0.7f, -0.8f, 0.0f,
        0.8f, -0.8f, 0.0f,

        //Six
        //right
        0.5f, -0.8f, 0.0f,
        0.8f, -0.8f, 0.0f,
        //top
        0.8f, -0.8f, 0.0f,
        0.8f, -0.3f, 0.0f,
        //left
        0.5f, -0.3f, 0.0f,
        0.8f, -0.3f, 0.0f,
        //bottom
        0.5f, -0.3f, 0.0f,
        0.5f, -0.8f, 0.0f,
        //vertical Line
        //1
        0.6f, -0.3f, 0.0f,
        0.6f, -0.8f, 0.0f,
        //2
        0.7f, -0.3f, 0.0f,
        0.7f, -0.8f, 0.0f,
        //horixental line
        //1
        0.5f, -0.466f, 0.0f,
        0.8f, -0.466f, 0.0f,
        //2
        0.5f, -0.632f, 0.0f,
        0.8f, -0.632f, 0.0f,
    };

    //circle Color Array
    const int iPoints = 132;
    GLfloat color[3 * iPoints];
    for (int i = 0; i < iPoints; i++)
    {   
        if (i < 98)
        {
            color[3 * i + 0] = 1.0f;
            color[3 * i + 1] = 1.0f;
            color[3 * i + 2] = 1.0f;
        }
        else 
        {
            if (i < 104)
            {
                color[3 * i + 0] = 1.0f;
                color[3 * i + 1] = 0.0f;
                color[3 * i + 2] = 0.0f;
            }
            else if (i < 110)
            {
                color[3 * i + 0] = 0.0f;
                color[3 * i + 1] = 1.0f;
                color[3 * i + 2] = 0.0f;
            }
            else if (i < 116)
            {
                color[3 * i + 0] = 0.0f;
                color[3 * i + 1] = 0.0f;
                color[3 * i + 2] = 1.0f;
            }
            else 
            {
                color[3 * i + 0] = 1.0f;
                color[3 * i + 1] = 1.0f;
                color[3 * i + 2] = 1.0f;
            }
        }
    }

    //Create vao
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(verties), verties, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    //Unbind 
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Color
    glGenBuffers(1, &vbo_Color);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //vao
    glBindVertexArray(0);

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    //cull back face for better performance as we dont need back as of now
    //glEnable(GL_CULL_FACE);

    //set background color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);


    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback,self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}


-(void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;

    if(height==0)
    {
        height=1;
    }
    
    glViewport(0,0,(GLsizei)width,(GLsizei)height);

    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void) drawView
{
    //code 
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glUseProgram(gShaderProgramObject);

    vmath::mat4 modelViewProjectionMatrix;
    modelViewProjectionMatrix = vmath::mat4::identity();

    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao);

    

    //Draw the Necessary Scnes
    //Start Right Top First
    glLineWidth(2.0f);
    glDrawArrays(GL_LINES, 0, 16);
    //Second
    glDrawArrays(GL_LINES, 16, 22);
    glPointSize(6.0f);
    glDrawArrays(GL_POINTS, 38, 16);
    //Forth
    glDrawArrays(GL_LINES, 54, 26);
    //Five
    glDrawArrays(GL_LINES, 80, 18);
    //Six
    glDrawArrays(GL_TRIANGLES, 98, 18);
    glDrawArrays(GL_LINES, 116, 16);
    //Third
    
    //UnBind vao
    glBindVertexArray(0);

    //UnUse Program
    glUseProgram(0);

    CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (BOOL)acceptsFirstResponder
{
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void) keyDown:(NSEvent *)theEvent
{
    int key=(int) [[theEvent characters]characterAtIndex:0];
    switch(key){
        case 27 : // Esc Key
            [self release];
            [NSApp terminate:self];
            break;
        case 'F' : 
        case 'f' :
            [[self window] toggleFullScreen:self];
            break;
        default:
            break;
    }
}

- (void) mouseDown:(NSEvent *) theEvent
{
    [self setNeedsDisplay:YES];
}

- (void) mouseDragged:(NSEvent *) theEvent
{
    //code
}

- (void) rightMouseDown:(NSEvent *)theEvent
{
    [self setNeedsDisplay:YES];
}

- (void) dealloc
{
    //code
    if (vbo_Color)
    {
        glDeleteBuffers(1, &vbo_Color);
        vbo_Color = 0;
    }
    if (vbo)
    {
        glDeleteBuffers(1, &vbo);
        vbo = 0;
    }
    if (vao)
    {
        glDeleteVertexArrays(1, &vao);
        vao = 0;
    }

    if (gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(gShaderProgramObject);
        //Ask Program How Many Shaders are Attached to you
        glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gShaderProgramObject);
        gShaderProgramObject = 0;
        glUseProgram(0);
    }

    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags glagsIn,
                                CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *) pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}