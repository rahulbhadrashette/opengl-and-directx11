#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags, CVOptionFlags *, void *);

//global variable
FILE *gpFile=NULL;

//interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate> 
@end

@interface GLView : NSOpenGLView
@end

//entry point function
int main (int argc, const char* argv[]) {
    //code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc] init];

    NSApp=[NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc] init]];

    [NSApp run];

    [pPool release];
    return(0);
}

@implementation AppDelegate
{
    @private 
        NSWindow *window;
        GLView *glView;
}

- (void) applicationDidFinishLaunching :(NSNotification *)aNotification
{
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

    const char *pszLogFileNameWithPath= [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath,"w");

    if(gpFile==NULL)
    {
        printf("Can not open log file.\nExitting ...\n");
        [self release];
        [NSApp terminate:self];
    }

    fprintf(gpFile, "Program is started successfully.\n");

    NSRect win_rect;
    win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

    window = [[NSWindow alloc] initWithContentRect:win_rect
                                styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                    backing:NSBackingStoreBuffered
                                        defer:NO];

    [window setTitle:@"OpenGL Perspective Window...RahulBhadrashette...!!!"];

    [window center];

    glView=[[GLView alloc] initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

- (void) applicationWillTerminate :(NSNotification *) notification
{
    //code
    fprintf(gpFile, "Program is terminated successfully\n");
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
    //code
    [NSApp terminate:self];
}

- (void) dealloc
{
    [glView release];
    [window release];
    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint vao_I;
    GLuint vbo_Color_I;
    GLuint vbo_Position_I;

    GLuint vao_N;
    GLuint vbo_Position_N;
    GLuint vbo_Color_N;

    GLuint vao_D;
    GLuint vbo_Position_D;
    GLuint vbo_Color_D;

    GLuint vao_A_TriBand;
    GLuint vbo_Position_A_TriBand;
    GLuint vbo_Color_A_TriBand;

    GLuint vao_A;
    GLuint vbo_Position_A;
    GLuint vbo_Color_A;
    GLuint mvpUniform;
    vmath::mat4 perspectiveProjectionMatrix;
}

- (id)initWithFrame:(NSRect)frame 
{
    self=[super initWithFrame:frame];

    if(self) {
        [[self window] setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };

        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel format is available. Exitting...");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }

    return self;
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *)pOutputTime 
{
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version :%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];


    //** VERTEX SHADER
    //Define Vertex Shader Object
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    //Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec4 out_Color;" \
        "void main(void)" \
        "{" \
            "gl_Position = u_mvp_matrix * vPosition;" \
            "out_Color = vColor;" \
        "}";

    //Specifing above source to the vertex shader object
    glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    //Compile the Vertex Shader
    glCompileShader(gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "vertexShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Define Fragment Shader Object
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //Write Fragment Shader Code
    const GLchar *fragmentShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec4 out_Color;" \
        "out vec4 fragColor;" \
        "void main(void)" \
        "{" \
            "fragColor = out_Color;" \
        "}";

    // Specifing above Source to the fragment Shader Object
    glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

    //Compile the fragment Shader Object
    glCompileShader(gFragmentShaderObject);

    GLint iFShaderCompileStatus = 0;
    GLint iFInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iFShaderCompileStatus);
    if (iFShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iFInfoLogLength);
        if (iFInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iFInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iFInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "FragmentShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Create Shader Object
    gShaderProgramObject = glCreateProgram();

    //Attach VertexShader to the Shader Program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);

    //Attach fragmentShader to the Shader Program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);

    // Pre Linking Binding to Vertex Attribute
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

    //Link the Shader Program
    glLinkProgram(gShaderProgramObject);

    GLint  iProgramLinkStatus = 0;
    GLint iLInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iLInfoLogLength);
        if (iInfoLogLength)
        {
            szInfoLog = (GLchar*)malloc(iLInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iLInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "LinkShaderProgramObject:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Post Linking Retriving Uniform Location
    mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
    
    //I Vertices Array
    const GLfloat I_Verties[] =
                                    {
                                        -1.0f, 1.0f, 0.0f,
                                        -1.0f, -1.0f, 0.0f
                                    };
    //I Color Array
    const GLfloat I_Color[] = 
                                    {
                                        1.0f,0.4f,0.0f,
                                        0.0f, 1.0f, 0.0f
                                    };

    //N Vertices Array
    const GLfloat N_Verties[] =
                                    {
                                        -1.0f, 1.0f, 0.0f,
                                        -1.0f, -1.0f, 0.0f,
                                        -1.0f, 1.0f, 0.0f,
                                        -0.2f, -1.0f, 0.0f,
                                        -0.2f, 1.0f, 0.0f,
                                        -0.2f, -1.0f, 0.0f
                                    };
    // N Color Array
    const GLfloat N_Color[] =
                                    {
                                        1.0f,0.4f,0.0f,
                                        0.0f, 1.0f, 0.0f,
                                        1.0f,0.4f,0.0f,
                                        0.0f, 1.0f, 0.0f,
                                        1.0f,0.4f,0.0f,
                                        0.0f, 1.0f, 0.0f
                                    };

    //D Vertices Array
    const GLfloat D_Verties[] =
                                    {
                                        -1.0f, 0.9f, 0.0f,
                                        -1.0f, -0.9f, 0.0f,
                                        -1.2f, 1.0f, 0.0f,
                                        -0.2f, 1.0f, 0.0f,
                                        -1.2f, -0.995f, 0.0f,
                                        -0.2f, -0.995f, 0.0f,
                                        -0.2f, 1.0f, 0.0f,
                                        -0.2f, -1.0f, 0.0f
                                    };
    //D Color Array
    const GLfloat D_Color[] = 
                                    {
                                        1.0f,0.4f,0.0f,
                                        0.0f, 1.0f, 0.0f,
                                        1.0f,0.4f,0.0f,
                                        1.0f,0.4f,0.0f,
                                        0.0f, 1.0f, 0.0f,
                                        0.0f, 1.0f, 0.0f,
                                        1.0f,0.4f,0.0f,
                                        0.0f, 1.0f, 0.0f
                                    };

    //A Vertices Array
    const GLfloat A_TriBand_Verties[] =
                                    {
                                        -0.03f, -0.045f, 0.0f,
                                        -0.55f, -0.045f, 0.0f,
                                        -0.02f, -0.08f, 0.0f,
                                        -0.56f, -0.08f, 0.0f,
                                        -0.01f, -0.12f, 0.0f,
                                        -0.57f, -0.12f, 0.0f
                                    };
    //A Color Array
    const GLfloat A_TriBand_Color[] =
                                    {
                                        1.0f,0.5f,0.0f,
                                        1.0f,0.5f,0.0f,
                                        1.0f, 1.0f, 1.0f,
                                        1.0f, 1.0f, 1.0f,
                                        0.0f, 1.0f, 0.0f,
                                        0.0f, 1.0f, 0.0f,
                                    };

    //A Vertices Array
    const GLfloat A_Verties[] =
                                    {
                                        -0.4f, 1.0f, 0.0f,
                                        0.20f, -1.0f, 0.0f,
                                        -0.4f, 1.0f, 0.0f,
                                        -0.80f, -1.0f, 0.0f
                                    };
    //A Color Array
    const GLfloat A_Color[] =
                                    {
                                        1.0f,0.4f,0.0f,
                                        0.0f, 1.0f, 0.0f,
                                        1.0f,0.4f,0.0f,
                                        0.0f, 1.0f, 0.0f,
                                    };
    

    //Create vao
    glGenVertexArrays(1, &vao_I);
    glBindVertexArray(vao_I);
    glGenBuffers(1, &vbo_Position_I);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_I);
    glBufferData(GL_ARRAY_BUFFER, sizeof(I_Verties), I_Verties, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    //Unbind Buffer
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //Color
    glGenBuffers(1, &vbo_Color_I);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_I);
    glBufferData(GL_ARRAY_BUFFER, sizeof(I_Color), I_Color, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //vao
    glBindVertexArray(0);

    //Create vao
    glGenVertexArrays(1, &vao_N);
    glBindVertexArray(vao_N);
    glGenBuffers(1, &vbo_Position_N);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_N);
    glBufferData(GL_ARRAY_BUFFER, sizeof(N_Verties), N_Verties, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    //Unbind Buffer
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //Color
    glGenBuffers(1, &vbo_Color_N);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_N);
    glBufferData(GL_ARRAY_BUFFER, sizeof(N_Color), N_Color, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //vao
    glBindVertexArray(0);

    //Create vao
    glGenVertexArrays(1, &vao_D);
    glBindVertexArray(vao_D);
    glGenBuffers(1, &vbo_Position_D);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_D);
    glBufferData(GL_ARRAY_BUFFER, sizeof(D_Verties), D_Verties, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    //Unbind Buffer
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //Color
    glGenBuffers(1, &vbo_Color_D);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_D);
    glBufferData(GL_ARRAY_BUFFER, sizeof(D_Color), D_Color, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //vao
    glBindVertexArray(0);

    //Create vao
    glGenVertexArrays(1, &vao_A_TriBand);
    glBindVertexArray(vao_A_TriBand);
    glGenBuffers(1, &vbo_Position_A_TriBand);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_A_TriBand);
    glBufferData(GL_ARRAY_BUFFER, sizeof(A_TriBand_Verties), A_TriBand_Verties, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    //Unbind Buffer
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //Color
    glGenBuffers(1, &vbo_Color_A_TriBand);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_A_TriBand);
    glBufferData(GL_ARRAY_BUFFER, sizeof(A_TriBand_Color), A_TriBand_Color, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //vao
    glBindVertexArray(0);

    //Create vao
    glGenVertexArrays(1, &vao_A);
    glBindVertexArray(vao_A);
    glGenBuffers(1, &vbo_Position_A);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_A);
    glBufferData(GL_ARRAY_BUFFER, sizeof(A_Verties), A_Verties, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    //Unbind Buffer
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //Color
    glGenBuffers(1, &vbo_Color_A);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_A);
    glBufferData(GL_ARRAY_BUFFER, sizeof(A_Color), A_Color, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //vao
    glBindVertexArray(0);

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glDisable(GL_CULL_FACE);

    //cull back face for better performance as we dont need back as of now
    //glEnable(GL_CULL_FACE);

    //set background color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    //set projection matrix to identity
    perspectiveProjectionMatrix = vmath::mat4::identity();
  
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback,self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink);

}


-(void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;

    if(height==0)
    {
        height=1;
    }
    
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    
   perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void) drawView
{
    //code 
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glUseProgram(gShaderProgramObject);

    //Declatation of Matricex
    vmath::mat4 translationMatrix;
    vmath::mat4 modelViewMatrix;
    vmath::mat4 modelViewProjectionMatrix;
    
    //For First I_Draw
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();

    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(-0.9f, 0.0f, -5.0f);

    //Do Necessary Matrix Multiplication
    modelViewMatrix = translationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    //Send Necessary Matrix to Shader in Respective Uniform
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    glLineWidth(30.0f);
    //BindWith vao
    glBindVertexArray(vao_I);
    //Draw the Necessary Scnes
    glDrawArrays(GL_LINES, 0, 2);
    //UnBind vao
    glBindVertexArray(0);

    //For N_Draw
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    
    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(-0.6f, 0.0f, -5.0f);

    //Do Necessary Matrix Multiplication
    modelViewMatrix = translationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    //Send Necessary Matrix to Shader in Respective Uniform
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glLineWidth(30.0f);
    //BindWith vao
    glBindVertexArray(vao_N);
    //Draw the Necessary Scnes
    glDrawArrays(GL_LINES, 0, 6);
    //UnBind vao
    glBindVertexArray(0);

    //For D_Draw
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    
    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.64f, 0.0f, -5.0f);

    //Do Necessary Matrix Multiplication
    modelViewMatrix = translationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    //Send Necessary Matrix to Shader in Respective Uniform
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glLineWidth(30.0f);
    //BindWith vao
    glBindVertexArray(vao_D);
    //Draw the Necessary Scnes
    glDrawArrays(GL_LINES, 0, 8);
    //UnBind vao
    glBindVertexArray(0);

    //For Second I_Draw
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();

    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(1.75f, 0.0f, -5.0f);

    //Do Necessary Matrix Multiplication
    modelViewMatrix = translationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    //Send Necessary Matrix to Shader in Respective Uniform
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    glLineWidth(30.0f);
    //BindWith vao
    glBindVertexArray(vao_I);
    //Draw the Necessary Scnes
    glDrawArrays(GL_LINES, 0, 2);
    //UnBind vao
    glBindVertexArray(0);

    //For A_TriBand_Draw
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();

    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(1.8f, 0.0f, -5.0f);

    //Do Necessary Matrix Multiplication
    modelViewMatrix = translationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    //Send Necessary Matrix to Shader in Respective Uniform
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    glLineWidth(10.0f);
    //BindWith vao
    glBindVertexArray(vao_A_TriBand);
    //Draw the Necessary Scnes
    glDrawArrays(GL_LINES, 0, 6);
    //UnBind vao
    glBindVertexArray(0);

    //For A_Draw
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();

    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(1.85f, 0.0f, -5.0f);

    //Do Necessary Matrix Multiplication
    modelViewMatrix = translationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    //Send Necessary Matrix to Shader in Respective Uniform
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    glLineWidth(30.0f);
    //BindWith vao
    glBindVertexArray(vao_A);
    //Draw the Necessary Scnes
    glDrawArrays(GL_LINES, 0, 6);
    //UnBind vao
    glBindVertexArray(0);

    //UnUse Program
    glUseProgram(0);

    CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (BOOL)acceptsFirstResponder
{
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void) keyDown:(NSEvent *)theEvent
{
    int key=(int) [[theEvent characters]characterAtIndex:0];
    switch(key){
        case 27 : // Esc Key
            [self release];
            [NSApp terminate:self];
            break;

        case 'f':
        case 'F':
            [[self window] toggleFullScreen:self];
            break;
        default:
            break;
    }
}

- (void) mouseDown:(NSEvent *) theEvent
{
    [self setNeedsDisplay:YES];
}

- (void) mouseDragged:(NSEvent *) theEvent
{
    //code
}

- (void) rightMouseDown:(NSEvent *)theEvent
{
    [self setNeedsDisplay:YES];
}

- (void) dealloc
{
    //code
    if (vbo_Color_A_TriBand)
    {
        glDeleteBuffers(1, &vbo_Color_A_TriBand);
        vbo_Color_A_TriBand = 0;
    }
    if (vbo_Position_A_TriBand)
    {
        glDeleteBuffers(1, &vbo_Position_A_TriBand);
        vbo_Position_A_TriBand = 0;
    }
    if (vao_A_TriBand)
    {
        glDeleteVertexArrays(1, &vao_A_TriBand);
        vao_A_TriBand = 0;
    }

    if (vbo_Color_A)
    {
        glDeleteBuffers(1, &vbo_Color_A);
        vbo_Color_A = 0;
    }
    if (vbo_Position_A)
    {
        glDeleteBuffers(1, &vbo_Position_A);
        vbo_Position_A = 0;
    }
    if (vao_A)
    {
        glDeleteVertexArrays(1, &vao_A);
        vao_A = 0;
    }

    if (vbo_Color_D)
    {
        glDeleteBuffers(1, &vbo_Color_D);
        vbo_Color_D = 0;
    }
    if (vbo_Position_D)
    {
        glDeleteBuffers(1, &vbo_Position_D);
        vbo_Position_D = 0;
    }
    if (vao_D)
    {
        glDeleteVertexArrays(1, &vao_D);
        vao_D = 0;
    }

    if (vbo_Color_N)
    {
        glDeleteBuffers(1, &vbo_Color_N);
        vbo_Color_N = 0;
    }
    if (vbo_Position_N)
    {
        glDeleteBuffers(1, &vbo_Position_N);
        vbo_Position_N = 0;
    }
    if (vao_N)
    {
        glDeleteVertexArrays(1, &vao_N);
        vao_N = 0;
    }

    if (vbo_Color_I)
    {
        glDeleteBuffers(1, &vbo_Color_I);
        vbo_Color_I = 0;
    }
    if (vbo_Position_I)
    {
        glDeleteBuffers(1, &vbo_Position_I);
        vbo_Position_I = 0;
    }
    if (vao_I)
    {
        glDeleteVertexArrays(1, &vao_I);
        vao_I = 0;
    }

    if (gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(gShaderProgramObject);
        //Ask Program How Many Shaders are Attached to you
        glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gShaderProgramObject);
        gShaderProgramObject = 0;
        glUseProgram(0);
    }

    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags glagsIn,
                                CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *) pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}