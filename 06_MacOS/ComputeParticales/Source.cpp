// Header files
#include <windows.h>
#include <stdio.h>

#include <gl/GLEW.h>
#include <gl/GL.h>
#include "vmath.h"
#include"Song.h"

// library files
#pragma comment (lib, "user32.lib")
#pragma comment (lib, "gdi32.lib")
#pragma comment (lib, "kernel32.lib")
#pragma comment( lib, "Winmm.lib")

#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")

// #define Macros
#define WIN_HEIGHT   600
#define WIN_WIDTH    800

enum
{
	RMB_ATTRIBUTE_POSITION = 0,
	RMB_ATTRIBUTE_COLOR,
	RMB_ATTRIBUTE_NORMAL,
	RMB_ATTRIBUTE_TEXTURE0,

	PARTICLE_GROUP_SIZE     = 128,
	PARTICLE_GROUP_COUNT    = 8000,
	PARTICLE_COUNT          = (PARTICLE_GROUP_SIZE * PARTICLE_GROUP_COUNT),
	MAX_ATTRACTORS          = 64
};

// global variable declaration
FILE* gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

GLuint gShaderProgramObject;
GLuint gComputeProgramObject;

GLuint vao;
GLuint vbo;
GLuint mvpUniform;

vmath::mat4 perspectiveProjectionMatrix;

// Compute Shader Variables
GLuint delta_Location;

// Posisition and velocity buffers
static union
{
	struct
	{
		GLuint position_buffer;
		GLuint velocity_buffer;
	};
	GLuint buffers[2];
};

// TBOs
static union
{
	struct
	{
		GLuint position_tbo;
		GLuint velocity_tbo;
	};
	GLuint tbos[2];
};

GLuint render_vao;
GLuint render_vbo;

GLuint attractor_buffer;
float attractor_messes[MAX_ATTRACTORS];

// function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declaration
	int initialize(void);
	void display(void);

	// local variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szApp[] = TEXT("OpenGLPP");

	bool bDone = false;
	int iRet = 0;
	int WIN_WIDTH_X, WIN_HEIGHT_Y;

	//code
	// Create File io code
	if(fopen_s(&gpFile, "Logfile.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created...!!!"), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log file is created successfully...!!!\n");
	}

	// initialization members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szApp;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// Registering Class
	RegisterClassEx(&wndclass);

	// Centerof the window calculation
	WIN_WIDTH_X = (GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2);
	WIN_HEIGHT_Y = (GetSystemMetrics(SM_CYSCREEN) /2) - (WIN_HEIGHT / 2);

	// Here Create actual Window in Memory 
	// Parallel to glutInitWindowSize(), glutInitWindowPosition(), and glutCreateWindow() all three together
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szApp, TEXT("Raster Group Leader : Rahul_U_M_B :- Partical System"),
	WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
	WIN_WIDTH_X,
	WIN_HEIGHT_Y,
	WIN_WIDTH,
	WIN_HEIGHT,
	NULL, 
	NULL,
	hInstance,
	NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	iRet = initialize();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "ChoosePixelFormat Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "SetPixelFormat is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, " wglCreateContext is Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "wglMakeCurrent Failed.\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "Initializetion Successfully Complited.\n");
	}

	while(bDone == false)
	{
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbActiveWindow == true)
			{
				//
			}
			display();
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void resize(int, int);
	void toggleFullScreen(void);
	void uninitialize(void);

	//code
	switch(iMsg)
	{
		//code
		case WM_CREATE:
			PlaySound(MAKEINTRESOURCE(MY_SONG), NULL, SND_ASYNC | SND_RESOURCE | SND_NODEFAULT);
			break;

		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;

		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;

		case WM_SIZE:
			resize(LOWORD(lParam), HIWORD(lParam));
			break;

		case WM_ERASEBKGND:
			return(0);

		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;

		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
			}
			break;

		case WM_CHAR:
			switch(wParam)
			{
				case 'f':
				case 'F':
					toggleFullScreen();
					break;
			}
			break;

		case WM_DESTROY:
			uninitialize();
			PostQuitMessage(0);
			break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int initialize(void)
{
	fprintf_s(gpFile, "start initialize.\n");
	//function declaration
	void resize(int, int);
	float random_float(void);
	vmath::vec3 random_vector(float, float);

	//local variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLenum result;

	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLuint computeShaderObject;
	GLint iShaderCompileStatus;
	GLint iProgramLinkStatus;
	GLint iInfoLogLength;
	GLchar *szInfoLog;
	
	{
		//code
		//pfd structure make zero here
		ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

		// pfd structure initialize
		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 32;
		pfd.cRedBits = 8;
		pfd.cGreenBits = 8;
		pfd.cBlueBits = 8;
		pfd.cAlphaBits = 8;
		pfd.cDepthBits = 32;

		ghdc = GetDC(ghwnd);
		iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
		if(iPixelFormatIndex == 0)
		{
			return(-1);
		}

		if(SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
		{
			return(-2);
		}

		ghrc = wglCreateContext(ghdc);
		if(ghrc == NULL)
		{
			return(-3);
		}

		if(wglMakeCurrent(ghdc, ghrc) == FALSE)
		{
			return(-4);
		}

		result = glewInit();
		if(result != GLEW_OK)
		{
			fprintf_s(gpFile, "glewInit() is Failed.\n");
			DestroyWindow(ghwnd);
		}
	}

	{

		// Define vertex shader object
		vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

		// write vertex shader code
		const GLchar *vertexShaderSourceCode = {

			"#version 460 core"
			"\n"

			"\n in vec4 vPosition;"

			"\n uniform mat4 u_mvp_matrix;"
			"\n out float intensity;"

			"\n void main(void)"
			"\n {"
			"\n		intensity = vPosition.w;"
			"\n 	gl_Position = u_mvp_matrix * vPosition;"
			"\n }"
		};

		// Specifing above source to the vertex shader object
		glShaderSource(vertexShaderObject, 1, (const GLchar**) &vertexShaderSourceCode, NULL);

		// Compile the vertex shader
		glCompileShader(vertexShaderObject);

		iShaderCompileStatus = 0;
		iInfoLogLength = 0;
		szInfoLog = NULL;

		glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
		if(iShaderCompileStatus == GL_FALSE)
		{
			glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
			if(iInfoLogLength > 0)
			{
				szInfoLog = (GLchar*)malloc(iInfoLogLength);
				if(szInfoLog != NULL)
				{
					GLsizei written;

					glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf_s(gpFile, " VS :- %s ", szInfoLog);
					free(szInfoLog);
					DestroyWindow(ghwnd);
				}
			}
		}

		// Define Fragment Shader Object
		fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

		// Write Fragment Shader Code
		const GLchar *fragmentShaderSourceCode = {
			
			"#version 460 core"

			"\n in float intensity;"

			"\n out vec4 fragColor;"

			"\n void main(void)"
			"\n {"

			"\n 	fragColor = mix(vec4(0.0f, 0.2f, 1.0f, 1.0f), vec4(0.2f, 0.05f, 0.0f, 1.0f), intensity);"

			"\n }"
		};

		// Specifing above source to the Fragment Shader Object
		glShaderSource(fragmentShaderObject, 1, (const GLchar**) &fragmentShaderSourceCode, NULL);

		// Compile the Fragment Shader Object
		glCompileShader(fragmentShaderObject);

		iShaderCompileStatus = 0;
		iInfoLogLength = 0;
		szInfoLog = NULL;

		glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
		if(iShaderCompileStatus == GL_FALSE)
		{
			glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
			if(iInfoLogLength > 0)
			{
				szInfoLog = (GLchar*)malloc(iInfoLogLength);
				if(szInfoLog != NULL)
				{
					GLsizei written;

					glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf_s(gpFile, "FS :- %s ", szInfoLog);
					free(szInfoLog);
					DestroyWindow(ghwnd);
				}
			}
		}

		// Create Shader Object
		gShaderProgramObject = glCreateProgram();

		// Attach vertexShaderObject to ShaderProgramObject
		glAttachShader(gShaderProgramObject, vertexShaderObject);
		// Attach fragmentShaderObject to ShaderProgramObject
		glAttachShader(gShaderProgramObject, fragmentShaderObject);

		// Pre Linking Binding to vertex attribute
		glBindAttribLocation(gShaderProgramObject, RMB_ATTRIBUTE_POSITION, "vPosition");

		//Link the shader program
		glLinkProgram(gShaderProgramObject);

		iProgramLinkStatus = 0;
		iInfoLogLength = 0;
		szInfoLog = NULL;

		glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
		if(iProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
			if(iInfoLogLength)
			{
				szInfoLog = (GLchar*)malloc(iInfoLogLength);
				if(szInfoLog != NULL)
				{
					GLsizei written;

					glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
					fprintf_s(gpFile, " LS :- %s ", szInfoLog);
					free(szInfoLog);
					DestroyWindow(ghwnd);
				}
			}
		}

		// Post Linking Retriving Uniform Location
		mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	}

	{
		// Define vertex shader object
		computeShaderObject = glCreateShader(GL_COMPUTE_SHADER);

		// Write Fragment Shader Code
		const GLchar *computeShaderSourceCode = {
			
			"#version 460 core"

			"\n layout (std140, binding = 0) uniform attractor_block"
			"\n {"
			"\n		vec4 attractor[64];" //xyz = position, w = mass
			"\n	};"

			"\n	layout (local_size_x = 128) in;"

			"\n layout (rgba32f, binding = 0) uniform imageBuffer velocity_buffer;"
			"\n	layout (rgba32f, binding = 1) uniform imageBuffer position_buffer;"

			"\n uniform float u_dt = 1.0;"

			"\n	void main(void)"
			"\n {"
			"\n		vec4 vel = imageLoad(velocity_buffer, int(gl_GlobalInvocationID.x));"
			"\n		vec4 pos = imageLoad(position_buffer, int(gl_GlobalInvocationID.x));"

			"\n 	int i;"

			"\n		pos.xyz += vel.xyz * u_dt;"
			"\n		pos.w -= 0.0001 * u_dt;"
			"\n"
			"\n		for(i = 0; i < 4; i++)"
			"\n		{"
			"\n			vec3 dist = (attractor[i].xyz - pos.xyz);"
			"\n			vel.xyz += u_dt * u_dt * attractor[i].w * normalize(dist) / (dot(dist, dist) + 10.0);"
			"\n		}"
			"\n"
			"\n		if(pos.w <= 0.0)"
			"\n		{"
			"\n			pos.xyz = -pos.xyz * 0.01;"
			"\n			vel.xyz *= 0.01;"
			"\n			pos.w += 1.0;"
			"\n		}"
			"\n"
			"\n		imageStore(position_buffer, int(gl_GlobalInvocationID.x), pos);"
			"\n		imageStore(velocity_buffer, int(gl_GlobalInvocationID.x), vel);"
			"\n }"
		};

		// Specifing above source to the vertex shader object
		glShaderSource(computeShaderObject, 1, (const GLchar**) &computeShaderSourceCode, NULL);

		// Compile the vertex shader
		glCompileShader(computeShaderObject);

		iShaderCompileStatus = 0;
		iInfoLogLength = 0;
		szInfoLog = NULL;

		glGetShaderiv(computeShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
		if(iShaderCompileStatus == GL_FALSE)
		{
			glGetShaderiv(computeShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
			if(iInfoLogLength > 0)
			{
				szInfoLog = (GLchar*)malloc(iInfoLogLength);
				if(szInfoLog != NULL)
				{
					GLsizei written;

					glGetShaderInfoLog(computeShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf_s(gpFile, " CS :- %s", szInfoLog);
					free(szInfoLog);
					DestroyWindow(ghwnd);
				}
			}
		}

		// Create Shader Object
		gComputeProgramObject = glCreateProgram();

		// Attach vertexShaderObject to ShaderProgramObject
		glAttachShader(gComputeProgramObject, computeShaderObject);

		//Link the shader program
		glLinkProgram(gComputeProgramObject);

		iProgramLinkStatus = 0;
		iInfoLogLength = 0;
		szInfoLog = NULL;

		glGetProgramiv(gComputeProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
		if(iProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(gComputeProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
			if(iInfoLogLength)
			{
				szInfoLog = (GLchar*)malloc(iInfoLogLength);
				if(szInfoLog != NULL)
				{
					GLsizei written;

					glGetProgramInfoLog(gComputeProgramObject, iInfoLogLength, &written, szInfoLog);
					fprintf_s(gpFile, " CLS :- %s ", szInfoLog);
					free(szInfoLog);
					DestroyWindow(ghwnd);
				}
			}
		}
		delta_Location = glGetUniformLocation(gComputeProgramObject, "u_dt");

	}

	
	{
		//***********************************************************************************************************************
		glGenVertexArrays(1, &render_vao);
		glBindVertexArray(render_vao);

		glGenBuffers(2, buffers);
		glBindBuffer(GL_ARRAY_BUFFER, position_buffer);
		glBufferData(GL_ARRAY_BUFFER, PARTICLE_COUNT * sizeof(vmath::vec4), NULL, GL_DYNAMIC_COPY);
		vmath::vec4 *position = (vmath::vec4*)glMapBufferRange(GL_ARRAY_BUFFER, 0, PARTICLE_COUNT * sizeof(vmath::vec4), 
		GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);

		for(int i = 0; i < PARTICLE_COUNT; i++)
		{
			position[i] = vmath::vec4(random_vector(-10.0f, 10.0f), random_float());
		}

		glUnmapBuffer(GL_ARRAY_BUFFER);

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(0);

		//************************************************************************************************************************

		glBindBuffer(GL_ARRAY_BUFFER, velocity_buffer);
		glBufferData(GL_ARRAY_BUFFER, PARTICLE_COUNT * sizeof(vmath::vec4), NULL, GL_DYNAMIC_COPY);

		vmath::vec4 *velocities = (vmath::vec4*)glMapBufferRange(GL_ARRAY_BUFFER, 0, PARTICLE_COUNT * sizeof(vmath::vec4),
		GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);

		for(int i = 0; i < PARTICLE_COUNT; i++)
		{
			velocities[i] = vmath::vec4(random_vector(-0.1f, 0.1f), 0.0f);
		}
		glUnmapBuffer(GL_ARRAY_BUFFER);

		//***********************************************************************************************************************

		glGenTextures(2, tbos);

		for(int i = 0; i < 2; i++)
		{
			glBindTexture(GL_TEXTURE_BUFFER, tbos[i]);
			glTexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, buffers[i]);
		}

		glGenBuffers(1, &attractor_buffer);
		glBindBuffer(GL_UNIFORM_BUFFER, attractor_buffer);
		glBufferData(GL_UNIFORM_BUFFER, 32 * sizeof(vmath::vec4), NULL, GL_STATIC_DRAW);

		for(int i = 0; i < MAX_ATTRACTORS; i++)
		{
			attractor_messes[i] = 0.5f + random_float() * 0.5f;
		}

		glBindBufferBase(GL_UNIFORM_BUFFER, 0, attractor_buffer);

		//**********************************************************************************************************************
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_BLEND);

	perspectiveProjectionMatrix = vmath::mat4::identity();

	// Warmup call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);
	
	return(0);
}

void resize(int width, int height)
{
	if(height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 1000.0f);
}

void display(void)
{
	//Declatation of Matrices And Initialize of Matrix in identity
	vmath::mat4 modelViewMatrix = vmath::mat4::identity();;
	vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
	vmath::mat4 rotationMatrix = vmath::mat4::identity();
	vmath::mat4 translationMatrix = vmath::mat4::identity();

	static const GLuint start_tick = GetTickCount() - 100000;
	GLuint current_ticks = GetTickCount();
	static GLuint last_ticks = current_ticks;
	float time = ((start_tick - current_ticks) & 0xFFFFF) / float(0xFFFFF);
	float delta_time = (float)(current_ticks - last_ticks) * 0.075f;

	vmath::vec4 * attractors = (vmath::vec4 *)glMapBufferRange(GL_UNIFORM_BUFFER,
		0,
		32 * sizeof(vmath::vec4),
		GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);

	for (int i = 0; i < 32; i++)
	{
		attractors[i] = vmath::vec4(sinf(time * (float)(i + 4) * 7.5f * 20.0f) * 50.0f,
			cosf(time * (float)(i + 7) * 3.9f * 20.0f) * 50.0f,
			sinf(time * (float)(i + 3) * 5.3f * 20.0f) * cosf(time * (float)(i + 5) * 9.1f) * 100.0f,
			attractor_messes[i]);
	}

	glUnmapBuffer(GL_UNIFORM_BUFFER);

	// If dt is too large, the system could explode, so cap it to
	// some maximum allowed value
	if (delta_time >= 2.0f)
	{
		delta_time = 2.0f;
	}

	// Activate the compute program and bind the position and velocity buffers
	glUseProgram(gComputeProgramObject);
	glBindImageTexture(0, velocity_tbo, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);
	glBindImageTexture(1, position_tbo, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);
	// Set delta time
	glUniform1f(delta_Location, delta_time);
	// Dispatch
	glDispatchCompute(PARTICLE_GROUP_COUNT, 1, 1);

	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);		

	// Clear, select the rendering program and draw a full screen quad
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	//Do Necessary Transformation Code Here
	translationMatrix = vmath::translate(0.0f, 0.0f, -60.0f);
	rotationMatrix = vmath::rotate(time * 1000.0f, vmath::vec3(0.0f, 1.0f, 0.0f));
	
	//Do Necessary Matrix Multiplication
	modelViewMatrix = modelViewMatrix * translationMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send Necessary Matrix to Shader in Respective Uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//BindWith vao
	glBindVertexArray(render_vao);
	glBlendFunc(GL_ONE, GL_ONE);
	//Draw the Necessary Scnes
	//glPointSize(2.0f);
	glDrawArrays(GL_POINTS, 0, PARTICLE_COUNT);
	//UnBind vao
	glBindVertexArray(0);

	//UnUse Program
	glUseProgram(0);

	last_ticks = current_ticks;

	SwapBuffers(ghdc);
}

void uninitialize(void)
{
	// code

	if(render_vao)
	{
		glDeleteVertexArrays(1, &render_vao);
		vao = 0;
	}

	if(gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);

		// Ask program how many shader are attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);

		if(pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);

			for(shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		glUseProgram(0);
	}

	if(gComputeProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gComputeProgramObject);

		// Ask program how many shader are attached to you
		glGetProgramiv(gComputeProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);

		if(pShaders)
		{
			glGetAttachedShaders(gComputeProgramObject, shaderCount, &shaderCount, pShaders);

			for(shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gComputeProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(gComputeProgramObject);
		gComputeProgramObject = 0;

		glUseProgram(0);
	}

	if(gbFullScreen)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	// Break the Current Context
	if(wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if(ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if(ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
	}

	if(gpFile)
	{
		fprintf_s(gpFile, "Log file is closed successfully.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void toggleFullScreen(void)
{
	MONITORINFO mi;
	if(gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if(GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
								mi.rcMonitor.left,
								mi.rcMonitor.top,
								mi.rcMonitor.right - mi.rcMonitor.left,
								mi.rcMonitor.bottom - mi.rcMonitor.top,
								SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

float random_float(void)
{
	float res;
	unsigned int temp;
	static unsigned int seed = 0xFFFF0C59;

	seed *= 16807;

	temp = seed ^ (seed >> 4) ^ (seed << 15);

	*((unsigned int *)&res) = (temp >> 9) | 0x3F800000;

	return(res - 1.0f);
}

vmath::vec3 random_vector(float minmag = 0.0f, float maxmag = 1.0)
{
	vmath::vec3 randomvec(random_float() * 2.0f - 1.0f, random_float() * 2.0f - 1.0f, random_float() * 2.0f - 1.0f);
	randomvec = normalize(randomvec);
	randomvec *= (random_float() * (maxmag - minmag) + minmag);

	return(randomvec);
}

