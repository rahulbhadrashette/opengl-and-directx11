#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"
#import "Sphere.h"
#import <math.h>

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags, CVOptionFlags *, void *);

//global variable
FILE *gpFile=NULL;

//interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate> 
@end

@interface GLView : NSOpenGLView
@end

//entry point function
int main (int argc, const char* argv[]) {
    //code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc] init];

    NSApp=[NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc] init]];

    [NSApp run];

    [pPool release];
    return(0);
}

@implementation AppDelegate
{
    @private 
        NSWindow *window;
        GLView *glView;
}

- (void) applicationDidFinishLaunching :(NSNotification *)aNotification
{
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

    const char *pszLogFileNameWithPath= [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath,"w");

    if(gpFile==NULL)
    {
        printf("Can not open log file.\nExitting ...\n");
        [self release];
        [NSApp terminate:self];
    }

    fprintf(gpFile, "Program is started successfully.\n");

    NSRect win_rect;
    win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

    window = [[NSWindow alloc] initWithContentRect:win_rect
                                styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                    backing:NSBackingStoreBuffered
                                        defer:NO];

    [window setTitle:@"OpenGL Diffuse Light on Sphere...RahulBhadrashette...!!!"];

    [window center];

    glView=[[GLView alloc] initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

- (void) applicationWillTerminate :(NSNotification *) notification
{
    //code
    fprintf(gpFile, "Program is terminated successfully\n");
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
    //code
    [NSApp terminate:self];
}

- (void) dealloc
{
    [glView release];
    [window release];
    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint gShaderProgramObject_PV;
    GLuint gShaderProgramObject_PF;


    struct Light
    {
        GLfloat Ambient[4];
        GLfloat Diffuse[4];
        GLfloat Position[4];
        GLfloat lightModeAmbient[4];
        GLfloat lightModelLocalViewer[1];
    };

    struct  Light light[1];

    struct Material
    {
        GLfloat Ambient[4];
        GLfloat Diffuse[4];
        GLfloat Specular[4];
        GLfloat Shininess[1];
    };
    struct Material material[24];


    bool gbLight;
    GLint changeKeyPressed;
    GLfloat Co1;
    GLfloat Co2;

    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    vmath::mat4 projectionMatrix;

    //******************* vertex Variables ***********
    GLuint modelUniform;
    GLuint viewUniform;
    GLuint projectionUniform;

    GLuint LaUniform_Red;
    GLuint LdUniform_Red;
    GLuint LMAUniform;
    GLuint LMLVUinform;
    GLuint lightPositionUniform_Red;

    GLuint KaUniform;
    GLuint KdUniform;
    GLuint KsUniform;

    GLuint materialShininessUniform;
    GLuint lKeyIsPressedUniform;

    //********** fragment Variables *******
    GLuint modelUniform_RMB;
    GLuint viewUniform_RMB;
    GLuint projectionUniform_RMB;

    GLuint LaUniform_Red_RMB;
    GLuint LdUniform_Red_RMB;
    GLuint LsUniform_Red_RMB;
    GLuint LMAUniform_RMB;
    GLuint LMLVUinform_RMB;
    GLuint lightPositionUniform_Red_RMB;

    GLuint KaUniform_RMB;
    GLuint KdUniform_RMB;
    GLuint KsUniform_RMB;

    GLuint materialShininessUniform_RMB;
    GLuint lKeyIsPressedUniform_RMB;

    GLuint vao_Sphere;
    GLuint vbo_Position_Sphere;
    GLuint vbo_Normal_Sphere;
    GLuint vbo_Element_Sphere;

    //For Sphere
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    short sphere_elements[2280];

    float gNumVertices;
    float gNumElements;

    float fwidth;
    float fheight;
}

- (id)initWithFrame:(NSRect)frame 
{
    self=[super initWithFrame:frame];

    if(self) {
        [[self window] setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };

        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel format is available. Exitting...");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }

    return self;
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *)pOutputTime 
{
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version :%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];


    [self perVertexShaderFunction];
    [self perFragmentShaderFunction];

    getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();

    //Position
    glGenVertexArrays(1, &vao_Sphere);
    glBindVertexArray(vao_Sphere);
    glGenBuffers(1, &vbo_Position_Sphere);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Sphere);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Normal
    glGenBuffers(1, &vbo_Normal_Sphere);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Normal_Sphere);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Element vbo
    glGenBuffers(1, &vbo_Element_Sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    //cull back face for better performance as we dont need back as of now
    //glEnable(GL_CULL_FACE);

    //set background color
    glClearColor(0.25f, 0.25f, 0.25f, 0.25f);

    //set projection matrix to identity
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback,self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)perVertexShaderFunction
{
    //** VERTEX SHADER
    //Define Vertex Shader Object
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    //Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode =
    "#version 410 core" \
    "\n" \
        "in vec4 vertexPosition;" \
        "in vec3 vertexNormal;" \

        "uniform mat4 u_modelMatrix;" \
        "uniform mat4 u_viewMatrix;" \
        "uniform mat4 u_projection_matrix;" \

        "uniform vec3 u_La_Red;" \
        "uniform vec3 u_Ld_Red;" \
        "uniform vec3 u_LmaUniform;" \
        "uniform vec3 u_LmlvUinform;" \

        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Kd;" \
        "uniform vec3 u_Ks;" \

        "uniform float u_materialShininess;" \
        "uniform int u_lKeyIsPressed;" \

        "uniform vec4 u_Light_Position_Red;" \


        "out vec3 phong_AdsLight;" \

        "void main(void)" \
        "{" \
        "if(u_lKeyIsPressed == 1)" \
        "{" \
        "vec4 eye_coordinate = u_viewMatrix * u_modelMatrix * vertexPosition;" \
        "vec3 tnorm = normalize(mat3(u_viewMatrix * u_modelMatrix) * vertexNormal);" \
        //For Red Light
        "vec3 lightdirection_Red = normalize(vec3(u_Light_Position_Red - eye_coordinate));" \

        "float tn_dot_lightDir_Red = max(dot(lightdirection_Red, tnorm), 0.0);" \

        "vec3 reflectionVector_Red = reflect(-lightdirection_Red, tnorm);" \

        "vec3 viewerVector = normalize(vec3(-eye_coordinate.xyz));" \

        "vec3 ambient_Red = u_La_Red * u_LmaUniform * u_Ka;" \

        "vec3 diffuse_Red = u_Ld_Red * u_Kd * tn_dot_lightDir_Red;" \

        "vec3 specular_Red = u_Ks * pow(max(dot(reflectionVector_Red, viewerVector), 0.0), u_materialShininess);" \

        "phong_AdsLight = ambient_Red + diffuse_Red + specular_Red;" \

        "}" \
        "else" \
        "{" \
        "phong_AdsLight = vec3(1.0, 1.0, 1.0);" \
        "}" \

        "gl_Position = u_projection_matrix * u_viewMatrix * u_modelMatrix * vertexPosition;" \
        "}";

    //"mat3 normalmatrix = mat3(transpose(inverse(u_mv_matrix)))" \   OR "mat3 normalmatrix = mat3(u_mv_matrix);" \

    //Specifing above source to the vertex shader object
    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    //Compile the Vertex Shader
    glCompileShader(vertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "vertexShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Define Fragment Shader Object
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //Write Fragment Shader Code
    const GLchar *fragmentShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec3 phong_AdsLight;" \
        "out vec4 fragColor;" \
        "void main(void)" \
        "{" \
        "fragColor = vec4(phong_AdsLight, 1.0);" \
        "}";

    // Specifing above Source to the fragment Shader Object
    glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

    //Compile the fragment Shader Object
    glCompileShader(fragmentShaderObject);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "FragmentShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Create Shader Object
    gShaderProgramObject_PV = glCreateProgram();

    //Attach VertexShader to the Shader Program
    glAttachShader(gShaderProgramObject_PV, vertexShaderObject);

    //Attach fragmentShader to the Shader Program
    glAttachShader(gShaderProgramObject_PV, fragmentShaderObject);

    // Pre Linking Binding to Vertex Attribute
    glBindAttribLocation(gShaderProgramObject_PV, AMC_ATTRIBUTE_POSITION, "vertexPosition");
    glBindAttribLocation(gShaderProgramObject_PV, AMC_ATTRIBUTE_NORMAL, "vertexNormal");

    //Link the Shader Program
    glLinkProgram(gShaderProgramObject_PV);

    GLint  iProgramLinkStatus = 0;
    GLint iLInfoLogLength = 0;
    GLint *szLogLength = NULL;

    glGetProgramiv(gShaderProgramObject_PV, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject_PV, GL_INFO_LOG_LENGTH, &iLInfoLogLength);
        if (iInfoLogLength)
        {
            szInfoLog = (GLchar*)malloc(iLInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject_PV, iLInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "LinkShaderProgramObject:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Post Linking Retriving Uniform Location
    modelUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_modelMatrix");
    viewUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_viewMatrix");
    projectionUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_projection_matrix");

    LaUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_La_Red");
    LdUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_Ld_Red");
    LMAUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_LmaUniform");
    LMLVUinform = glGetUniformLocation(gShaderProgramObject_PV, "u_LmlvUinform");

    KaUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_Ka");
    KdUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_Kd");
    KsUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_Ks");

    materialShininessUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_materialShininess");
    lightPositionUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_Light_Position_Red");
    lKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_lKeyIsPressed");
}

-(void)perFragmentShaderFunction
{
    //** VERTEX SHADER
    //Define Vertex Shader Object
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    //Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode =
        "#version 410 core" \
                "\n" \
        "in vec4 vertexPosition_RMB;" \
        "in vec3 vertexNormal_RMB;" \

        "uniform mat4 u_modelMatrix_RMB;" \
        "uniform mat4 u_viewMatrix_RMB;" \
        "uniform mat4 u_projection_matrix_RMB;" \

        "uniform int u_lKeyIsPressed_RMB;" \
        "uniform vec4 u_Light_Position_Red_RMB;" \

        "out vec3 tnorm_RMB;" \
        "out vec3 lightdirection_Red_RMB;" \
        "out vec3 viewerVector_RMB;" \

        "void main(void)" \
        "{" \
        "if(u_lKeyIsPressed_RMB == 1)" \
        "{" \
        "vec4 eye_coordinate_RMB = u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" \
        "tnorm_RMB = mat3(u_viewMatrix_RMB * u_modelMatrix_RMB) * vertexNormal_RMB;" \
        //For Red Light Calculations
        "lightdirection_Red_RMB = vec3(u_Light_Position_Red_RMB - eye_coordinate_RMB);" \

        "viewerVector_RMB = vec3(-eye_coordinate_RMB.xyz);" \
        "}" \

        "gl_Position = u_projection_matrix_RMB * u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" \
        "}";

    //"mat3 normalmatrix = mat3(transpose(inverse(u_mv_matrix)))" \   OR "mat3 normalmatrix = mat3(u_mv_matrix);" \

    //Specifing above source to the vertex shader object
    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    //Compile the Vertex Shader
    glCompileShader(vertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "vertexShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Define Fragment Shader Object
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //Write Fragment Shader Code
    const GLchar *fragmentShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec3 tnorm_RMB;" \
        "in vec3 lightdirection_Red_RMB;" \
        "in vec3 viewerVector_RMB;" \

        "uniform vec3 u_La_Red_RMB;" \
        "uniform vec3 u_Ld_Red_RMB;" \
        "uniform vec3 u_LmaUniform_RMB;" \
        "uniform vec3 u_LmlvUinform_RMB;" \

        "uniform vec3 u_Ka_RMB;" \
        "uniform vec3 u_Kd_RMB;" \
        "uniform vec3 u_Ks_RMB;" \

        "uniform float u_materialShininess_RMB;" \
        "uniform int u_lKeyIsPressed_RMB;" \


        "out vec4 fragColor_RMB;" \

        "void main(void)" \
        "{" \
        "vec3 phong_AdsLight_RMB;" \
        "if(u_lKeyIsPressed_RMB == 1)" \
        "{" \
        //For Red Light Calculations
        "vec3 normalize_tnorm_RMB = normalize(tnorm_RMB);" \

        "vec3 normalize_lightdirection_Red_RMB = normalize(lightdirection_Red_RMB);" \

        "vec3 normalize_viewerVector_RMB = normalize(viewerVector_RMB);" \

        "vec3 reflectionVector_Red_RMB = reflect(-normalize_lightdirection_Red_RMB, normalize_tnorm_RMB);" \

        "float tn_dot_lightDir_Red_RMB = max(dot(normalize_lightdirection_Red_RMB, normalize_tnorm_RMB), 0.0);" \

        "vec3 ambient_Red_RMB = u_La_Red_RMB * u_LmaUniform_RMB * u_Ka_RMB;" \

        "vec3 diffuse_Red_RMB = u_Ld_Red_RMB * u_Kd_RMB * tn_dot_lightDir_Red_RMB;" \

        "vec3 specular_Red_RMB =  u_Ks_RMB * pow(max(dot(reflectionVector_Red_RMB, normalize_viewerVector_RMB), 0.0), u_materialShininess_RMB);" \

        "phong_AdsLight_RMB = ambient_Red_RMB + diffuse_Red_RMB + specular_Red_RMB;" \

        "}" \
        "else" \
        "{" \

        "phong_AdsLight_RMB = vec3(1.0, 1.0, 1.0);" \

        "}" \

        "fragColor_RMB = vec4(phong_AdsLight_RMB, 1.0);" \
        "}";


    // Specifing above Source to the fragment Shader Object
    glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

    //Compile the fragment Shader Object
    glCompileShader(fragmentShaderObject);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "FragmentShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Create Shader Object
    gShaderProgramObject_PF = glCreateProgram();

    //Attach VertexShader to the Shader Program
    glAttachShader(gShaderProgramObject_PF, vertexShaderObject);

    //Attach fragmentShader to the Shader Program
    glAttachShader(gShaderProgramObject_PF, fragmentShaderObject);

    // Pre Linking Binding to Vertex Attribute
   glBindAttribLocation(gShaderProgramObject_PF, AMC_ATTRIBUTE_POSITION, "vertexPosition_RMB");
    glBindAttribLocation(gShaderProgramObject_PF, AMC_ATTRIBUTE_NORMAL, "vertexNormal_RMB");

    //Link the Shader Program
    glLinkProgram(gShaderProgramObject_PF);

    GLint  iProgramLinkStatus = 0;
    GLint iLInfoLogLength = 0;
    GLint *szLogLength = NULL;

    glGetProgramiv(gShaderProgramObject_PF, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject_PF, GL_INFO_LOG_LENGTH, &iLInfoLogLength);
        if (iInfoLogLength)
        {
            szInfoLog = (GLchar*)malloc(iLInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject_PF, iLInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "LinkShaderProgramObject:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Post Linking Retriving Uniform Location
    modelUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_modelMatrix_RMB");
    viewUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_viewMatrix_RMB");
    projectionUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_projection_matrix_RMB");

    LaUniform_Red_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_La_Red_RMB");
    LdUniform_Red_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ld_Red_RMB");
    LMAUniform_RMB = glGetUniformLocation(gShaderProgramObject_PV, "u_LmaUniform_RMB");
    LMLVUinform_RMB = glGetUniformLocation(gShaderProgramObject_PV, "u_LmlvUinform_RMB");

    KaUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ka_RMB");
    KdUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Kd_RMB");
    KsUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ks_RMB");

    materialShininessUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_materialShininess_RMB");
    lightPositionUniform_Red_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Light_Position_Red_RMB");
    lKeyIsPressedUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_lKeyIsPressed_RMB");
}

-(void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    
    fwidth=rect.size.width;
    fheight=rect.size.height;

    if(fheight==0.0f)
    {
        fheight=1.0f;
    }
    
    glViewport(0,0,(GLsizei)fwidth,(GLsizei)fheight);
    
   projectionMatrix = vmath::perspective(45.0f, (GLfloat)fwidth / (GLfloat)fheight, 0.1f, 100.0f);

    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void) drawView
{
    //code
    static GLfloat angleRotation = 0.0f;
    [self Light_Array];
    [self materialsFragmentShader];
    
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    for(int i = 0; i < 24; i++)
    {
        glViewport((i % 6) * fwidth / 6, fheight - (i / 6 + 1) * fheight / 4, (GLsizei)fwidth / 6, (GLsizei)fheight / 4);

        projectionMatrix = vmath::perspective(45.0f, (GLfloat)(fwidth / 6) / (GLfloat)(fheight / 4), 0.1f, 100.0f);

        if(changeKeyPressed == 2)
        {
            glUseProgram(gShaderProgramObject_PF);
            if (gbLight)
            {
                glUniform1i(lKeyIsPressedUniform_RMB, 1);
                glUniform3fv(LaUniform_Red_RMB, 1, light[0].Ambient);
                glUniform3fv(LdUniform_Red_RMB, 1, light[0].Diffuse);
                glUniform3fv(LMAUniform, 1, light[0].lightModeAmbient);
                glUniform1fv(LMLVUinform, 1, light[0].lightModelLocalViewer);
                glUniform4fv(lightPositionUniform_Red_RMB, 1, light[0].Position);

                glUniform3fv(KaUniform_RMB, 1, material[i].Ambient);
                glUniform3fv(KdUniform_RMB, 1, material[i].Diffuse);
                glUniform3fv(KsUniform_RMB, 1, material[i].Specular);
                glUniform1fv(materialShininessUniform_RMB, 1, material[i].Shininess);
            }
            else
            {
                glUniform1i(lKeyIsPressedUniform, 0);
            }
        }
        else 
        {
            glUseProgram(gShaderProgramObject_PV);
            if (gbLight)
            {
                glUniform1i(lKeyIsPressedUniform, 1);
                glUniform3fv(LaUniform_Red, 1, light[0].Ambient);
                glUniform3fv(LdUniform_Red, 1, light[0].Diffuse);
                glUniform3fv(LMAUniform, 1, light[0].lightModeAmbient);
                glUniform1fv(LMLVUinform, 1, light[0].lightModelLocalViewer);
                glUniform4fv(lightPositionUniform_Red, 1, light[0].Position);

                glUniform3fv(KaUniform, 1, material[i].Ambient);
                glUniform3fv(KdUniform, 1, material[i].Diffuse);
                glUniform3fv(KsUniform, 1, material[i].Specular);
                glUniform1fv(materialShininessUniform, 1, material[i].Shininess);
            }
            else
            {
                glUniform1i(lKeyIsPressedUniform_RMB, 0);
            }
        }

        //Declatation of Matricex
        vmath::mat4 translationMatrix;
        
        //Initialize of Matrix in identity
        translationMatrix = vmath::mat4::identity();
        modelMatrix = vmath::mat4::identity();
        viewMatrix = vmath::mat4::identity();

        //Do Necessary Transformation Code Here
        translationMatrix = vmath::translate(0.0f, 0.0f, -2.3f);

        //Do Necessary Matrix Multiplication
        modelMatrix = translationMatrix;


        //Send Necessary Matrix to Shader in Respective Uniform
        glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, projectionMatrix);

        glUniformMatrix4fv(modelUniform_RMB, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(viewUniform_RMB, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionUniform_RMB, 1, GL_FALSE, projectionMatrix);

        //BindWith vao
        glBindVertexArray(vao_Sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
        //Draw the Necessary Scnes
        glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
        
        //UnBind vao
        glBindVertexArray(0);

        //UnUse Program
        glUseProgram(0);
    }

    CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

    angleRotation = angleRotation + 0.01f;
    if (angleRotation > 360.0f)
    {
        angleRotation = 0.0f;
    }
    
    Co1 = (GLfloat)cos(angleRotation) * 100.0f;
    Co2 = (GLfloat)sin(angleRotation) * 100.0f;
}

- (BOOL)acceptsFirstResponder
{
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void) keyDown:(NSEvent *)theEvent
{
    int key=(int) [[theEvent characters]characterAtIndex:0];
    switch(key){

            case 'E':
            case 'e':
            case 'Q':
            case 'q':
                [self release];
                [NSApp terminate:self];
                break;

            case 'F':
            case 'f':
                    changeKeyPressed = 2;
                 break;

            case 'V':
            case 'v':
                    changeKeyPressed = 1; 
                break;

            case 'L':
            case 'l':
                    gbLight = !gbLight;
                break;

            case 'X':
            case 'x':
                changeKeyPressed = 6;
                break;

            case 'Y':
            case 'y':
                changeKeyPressed = 7;
                break;

            case 'Z':
            case 'z':
                changeKeyPressed = 5;
                break;
            
            case 27 : // Esc Key
                [[self window] toggleFullScreen:self];
                break;

            default:
                break;
    }
}

- (void)Light_Array
{
        //Array Zero
    light[0].Ambient[0] = 0.0f;
    light[0].Ambient[1] = 0.0f;
    light[0].Ambient[2] = 0.0f;
    light[0].Ambient[3] = 1.0f;

    light[0].Diffuse[0] = 1.0f;
    light[0].Diffuse[1] = 1.0f;
    light[0].Diffuse[2] = 1.0f;
    light[0].Diffuse[3] = 1.0f;

    if (changeKeyPressed == 6)
    {
        light[0].Position[0] = Co1;
        light[0].Position[1] = 0.0f;
        light[0].Position[2] = Co2;
        light[0].Position[3] = 1.0f;
    }
    else if (changeKeyPressed == 5)
    {
        light[0].Position[0] = Co2;;
        light[0].Position[1] = Co1;
        light[0].Position[2] = 0.0f;
        light[0].Position[3] = 1.0f;
    }
    else
    {
        light[0].Position[0] = 0.0f;
        light[0].Position[1] = Co1;
        light[0].Position[2] = Co2;
        light[0].Position[3] = 1.0f;
    }

    light[0].lightModeAmbient[0] = 0.2f;
    light[0].lightModeAmbient[1] = 0.1f;
    light[0].lightModeAmbient[2] = 0.2f;
    light[0].lightModeAmbient[3] = 1.0f;

    light[0].lightModelLocalViewer[0] = 0.0f;

}

- (void)materialsFragmentShader
{
    // *** 1st Sphere On 1st Column, Emerald ****
    material[0].Ambient[0] = 0.0215f;
    material[0].Ambient[1] = 0.1745f;
    material[0].Ambient[2] = 0.0215f;
    material[0].Ambient[3] = 1.0f;

    material[0].Diffuse[0] = 0.07568f;
    material[0].Diffuse[1] = 0.61424f;
    material[0].Diffuse[2] = 0.07568f;
    material[0].Diffuse[3] = 1.0f;

    material[0].Specular[0] = 0.633f;
    material[0].Specular[1] = 0.727811f;
    material[0].Specular[2] = 0.633f;
    material[0].Specular[3] = 1.0f;

    material[0].Shininess[0] = 0.6f * 128.0f;
    
    // *** 2nd Sphere On 1st Column, Jade ****
    material[1].Ambient[0] = 0.135f;
    material[1].Ambient[1] = 0.2225f;
    material[1].Ambient[2] = 0.1575f;
    material[1].Ambient[3] = 1.0f;

    material[1].Diffuse[0] = 0.54f;
    material[1].Diffuse[1] = 0.89f;
    material[1].Diffuse[2] = 0.63f;
    material[1].Diffuse[3] = 1.0f;

    material[1].Specular[0] = 0.316228f;
    material[1].Specular[1] = 0.316228f;
    material[1].Specular[2] = 0.316228f;
    material[1].Specular[3] = 1.0f;

    material[1].Shininess[0] = 0.1f * 128.0f;

    // *** 3rd Sphere On 1st Column, Obsidian ****
    material[2].Ambient[0] = 0.05375f;
    material[2].Ambient[1] = 0.05f;
    material[2].Ambient[2] = 0.06625f;
    material[2].Ambient[3] = 1.0f;

    material[2].Diffuse[0] = 0.18275f;
    material[2].Diffuse[1] = 0.17f;
    material[2].Diffuse[2] = 0.22525f;
    material[2].Diffuse[3] = 1.0f;

    material[2].Specular[0] = 0.332741f;
    material[2].Specular[1] = 0.328634f;
    material[2].Specular[2] = 0.346435f;
    material[2].Specular[3] = 1.0f;

    material[2].Shininess[0] = 0.3f * 128.0f;

    // *** 4th Sphere On 1st Column, Pearl ****
    material[3].Ambient[0] = 0.25f;
    material[3].Ambient[1] = 0.20725f;
    material[3].Ambient[2] = 0.20725f;
    material[3].Ambient[3] = 1.0f;

    material[3].Diffuse[0] = 1.0f;
    material[3].Diffuse[1] = 0.829f;
    material[3].Diffuse[2] = 0.829f;
    material[3].Diffuse[3] = 1.0f;

    material[3].Specular[0] = 0.296648f;
    material[3].Specular[1] = 0.296648f;
    material[3].Specular[2] = 0.296648f;
    material[3].Specular[3] = 1.0f;

    material[3].Shininess[0] = 0.088f * 128.0f;

    // *** 5th Sphere On 1st Column, Ruby ****
    material[4].Ambient[0] = 0.1745f;
    material[4].Ambient[1] = 0.01175f;
    material[4].Ambient[2] = 0.01175f;
    material[4].Ambient[3] = 1.0f;

    material[4].Diffuse[0] = 0.61424f;
    material[4].Diffuse[1] = 0.04136f;
    material[4].Diffuse[2] = 0.04136f;
    material[4].Diffuse[3] = 1.0f;

    material[4].Specular[0] = 0.727811f;
    material[4].Specular[1] = 0.686959f;
    material[4].Specular[2] = 0.626959f;
    material[4].Specular[3] = 1.0f;

    material[4].Shininess[0] = 0.6f * 128.0f;

    // *** 6th Sphere On 1st Column, Tarquoise ****
    material[5].Ambient[0] = 0.1f;
    material[5].Ambient[1] = 0.18725f;
    material[5].Ambient[2] = 0.1745f;
    material[5].Ambient[3] = 1.0f;

    material[5].Diffuse[0] = 0.396f;
    material[5].Diffuse[1] = 0.74151f;
    material[5].Diffuse[2] = 0.69102f;
    material[5].Diffuse[3] = 1.0f;

    material[5].Specular[0] = 0.297254f;
    material[5].Specular[1] = 0.30829f;
    material[5].Specular[2] = 0.306678f;
    material[5].Specular[3] = 1.0f;

    material[5].Shininess[0] = 0.1f * 128.0f;

    // *** 1st Sphere On 2nd Column, Brass ****
    material[6].Ambient[0] = 0.329412f;
    material[6].Ambient[1] = 0.223529f;
    material[6].Ambient[2] = 0.027451f;
    material[6].Ambient[3] = 1.0f;

    material[6].Diffuse[0] = 0.780392f;
    material[6].Diffuse[1] = 0.568627f;
    material[6].Diffuse[2] = 0.113725f;
    material[6].Diffuse[3] = 1.0f;

    material[6].Specular[0] = 0.992157f;
    material[6].Specular[1] = 0.941176f;
    material[6].Specular[2] = 0.807843f;
    material[6].Specular[3] = 1.0f;

    material[6].Shininess[0] = 0.21794872f * 128.0f;

    // *** 2nd Sphere On 2nd Column, Bronze ****
    material[7].Ambient[0] = 0.2125f;
    material[7].Ambient[1] = 0.1275f;
    material[7].Ambient[2] = 0.054f;
    material[7].Ambient[3] = 1.0f;

    material[7].Diffuse[0] = 0.714f;
    material[7].Diffuse[1] = 0.4284f;
    material[7].Diffuse[2] = 0.18144f;
    material[7].Diffuse[3] = 1.0f;

    material[7].Specular[0] = 0.393548f;
    material[7].Specular[1] = 0.271906f;
    material[7].Specular[2] = 0.166721f;
    material[7].Specular[3] = 1.0f;

    material[7].Shininess[0] = 0.2f * 128.0f;

    // *** 3rd Sphere On 2nd Column, Chrome ****
    material[8].Ambient[0] = 0.25f;
    material[8].Ambient[1] = 0.25f;
    material[8].Ambient[2] = 0.25f;
    material[8].Ambient[3] = 1.0f;

    material[8].Diffuse[0] = 0.4f;
    material[8].Diffuse[1] = 0.4f;
    material[8].Diffuse[2] = 0.4f;
    material[8].Diffuse[3] = 1.0f;

    material[8].Specular[0] = 0.774597f;
    material[8].Specular[1] = 0.774597f;
    material[8].Specular[2] = 0.774597f;
    material[8].Specular[3] = 1.0f;

    material[8].Shininess[0] = 0.6f * 128.0f;

    // *** 4th Sphere On 2nd Column, Copper ****
    material[9].Ambient[0] = 0.19125f;
    material[9].Ambient[1] = 0.0735f;
    material[9].Ambient[2] = 0.0225f;
    material[9].Ambient[3] = 1.0f;

    material[9].Diffuse[0] = 0.7038f;
    material[9].Diffuse[1] = 0.27048f;
    material[9].Diffuse[2] = 0.0828f;
    material[9].Diffuse[3] = 1.0f;

    material[9].Specular[0] = 0.256777f;
    material[9].Specular[1] = 0.137622f;
    material[9].Specular[2] = 0.086014f;
    material[9].Specular[3] = 1.0f;

    material[9].Shininess[0] = 0.1f * 128.0f;

    // *** 5th Sphere On 2nd Column, Gold ****
    material[10].Ambient[0] = 0.24725f;
    material[10].Ambient[1] = 0.1995f;
    material[10].Ambient[2] = 0.0745f;
    material[10].Ambient[3] = 1.0f;

    material[10].Diffuse[0] = 0.75164f;
    material[10].Diffuse[1] = 0.60648f;
    material[10].Diffuse[2] = 0.22648f;
    material[10].Diffuse[3] = 1.0f;

    material[10].Specular[0] = 0.628281f;
    material[10].Specular[1] = 0.555802f;
    material[10].Specular[2] = 0.366065f;
    material[10].Specular[3] = 1.0f;

    material[10].Shininess[0] = 0.4f * 128.0f;

    // *** 6th Sphere On 2nd Column, Silver ****
    material[11].Ambient[0] = 0.19225f;
    material[11].Ambient[1] = 0.19225f;
    material[11].Ambient[2] = 0.19225f;
    material[11].Ambient[3] = 1.0f;

    material[11].Diffuse[0] = 0.50754f;
    material[11].Diffuse[1] = 0.50754f;
    material[11].Diffuse[2] = 0.50754f;
    material[11].Diffuse[3] = 1.0f;

    material[11].Specular[0] = 0.508273f;
    material[11].Specular[1] = 0.508273f;
    material[11].Specular[2] = 0.508273f;
    material[11].Specular[3] = 1.0f;

    material[11].Shininess[0] = 0.4f * 128.0f;

    // *** 1st Sphere On 3rd Column, Black ****
    material[12].Ambient[0] = 0.0f;
    material[12].Ambient[1] = 0.0f;
    material[12].Ambient[2] = 0.0f;
    material[12].Ambient[3] = 1.0f;

    material[12].Diffuse[0] = 0.01f;
    material[12].Diffuse[1] = 0.01f;
    material[12].Diffuse[2] = 0.01f;
    material[12].Diffuse[3] = 1.0f;

    material[12].Specular[0] = 0.50f;
    material[12].Specular[1] = 0.50f;
    material[12].Specular[2] = 0.50f;
    material[12].Specular[3] = 1.0f;

    material[12].Shininess[0] = 0.25f * 128.0f;

    // *** 2nd Sphere On 3rd Column, Cyan ****
    material[13].Ambient[0] = 0.0f;
    material[13].Ambient[1] = 0.1f;
    material[13].Ambient[2] = 0.06f;
    material[13].Ambient[3] = 1.0f;

    material[13].Diffuse[0] = 0.0f;
    material[13].Diffuse[1] = 0.50980392f;
    material[13].Diffuse[2] = 0.50980392f;
    material[13].Diffuse[3] = 1.0f;

    material[13].Specular[0] = 0.50196078f;
    material[13].Specular[1] = 0.50196078f;
    material[13].Specular[2] = 0.50196078f;
    material[13].Specular[3] = 1.0f;

    material[13].Shininess[0] = 0.25f * 128.0f;

    // *** 3rd Sphere On 3rd Column, Green ****
    material[14].Ambient[0] = 0.0f;
    material[14].Ambient[1] = 0.0f;
    material[14].Ambient[2] = 0.0f;
    material[14].Ambient[3] = 1.0f;

    material[14].Diffuse[0] = 0.1f;
    material[14].Diffuse[1] = 0.35f;
    material[14].Diffuse[2] = 0.1f;
    material[14].Diffuse[3] = 1.0f;

    material[14].Specular[0] = 0.45f;
    material[14].Specular[1] = 0.45f;
    material[14].Specular[2] = 0.45f;
    material[14].Specular[3] = 1.0f;

    material[14].Shininess[0] = 0.25f * 128.0f;

    // *** 4th Sphere On 3rd Column, Red ****
    material[15].Ambient[0] = 0.0f;
    material[15].Ambient[1] = 0.0f;
    material[15].Ambient[2] = 0.0f;
    material[15].Ambient[3] = 1.0f;

    material[15].Diffuse[0] = 0.5f;
    material[15].Diffuse[1] = 0.0f;
    material[15].Diffuse[2] = 0.0f;
    material[15].Diffuse[3] = 1.0f;

    material[15].Specular[0] = 0.7f;
    material[15].Specular[1] = 0.6f;
    material[15].Specular[2] = 0.6f;
    material[15].Specular[3] = 1.0f;

    material[15].Shininess[0] = 0.25f * 128.0f;

    // *** 5th Sphere On 3rd Column, White ****
    material[16].Ambient[0] = 0.0f;
    material[16].Ambient[1] = 0.0f;
    material[16].Ambient[2] = 0.0f;
    material[16].Ambient[3] = 1.0f;

    material[16].Diffuse[0] = 0.55f;
    material[16].Diffuse[1] = 0.55f;
    material[16].Diffuse[2] = 0.55f;
    material[16].Diffuse[3] = 1.0f;

    material[16].Specular[0] = 0.70f;
    material[16].Specular[1] = 0.70f;
    material[16].Specular[2] = 0.70f;
    material[16].Specular[3] = 1.0f;

    material[16].Shininess[0] = 0.25f * 128.0f;

    // *** 6th Sphere On 3rd Column, Yello ****
    material[17].Ambient[0] = 0.0f;
    material[17].Ambient[1] = 0.0f;
    material[17].Ambient[2] = 0.0f;
    material[17].Ambient[3] = 1.0f;

    material[17].Diffuse[0] = 0.5f;
    material[17].Diffuse[1] = 0.5f;
    material[17].Diffuse[2] = 0.0f;
    material[17].Diffuse[3] = 1.0f;

    material[17].Specular[0] = 0.60f;
    material[17].Specular[1] = 0.60f;
    material[17].Specular[2] = 0.50f;
    material[17].Specular[3] = 1.0f;

    material[17].Shininess[0] = 0.25f * 128.0f;

    // *** 1st Sphere On 4th Column, Black ****
    material[18].Ambient[0] = 0.02f;
    material[18].Ambient[1] = 0.02f;
    material[18].Ambient[2] = 0.02f;
    material[18].Ambient[3] = 1.0f;

    material[18].Diffuse[0] = 0.01f;
    material[18].Diffuse[1] = 0.01f;
    material[18].Diffuse[2] = 0.01f;
    material[18].Diffuse[3] = 1.0f;

    material[18].Specular[0] = 0.4f;
    material[18].Specular[1] = 0.4f;
    material[18].Specular[2] = 0.4f;
    material[18].Specular[3] = 1.0f;

    material[18].Shininess[0] = 0.078125f * 128.0f;


    // *** 2nd Sphere On 4th Column, Cyan ****
    material[19].Ambient[0] = 0.0f;
    material[19].Ambient[1] = 0.05f;
    material[19].Ambient[2] = 0.05f;
    material[19].Ambient[3] = 1.0f;

    material[19].Diffuse[0] = 0.4f;
    material[19].Diffuse[1] = 0.5f;
    material[19].Diffuse[2] = 0.5f;
    material[19].Diffuse[3] = 1.0f;

    material[19].Specular[0] = 0.04f;
    material[19].Specular[1] = 0.7f;
    material[19].Specular[2] = 0.7f;
    material[19].Specular[3] = 1.0f;

    material[19].Shininess[0] = 0.078125f * 128.0f;

    // *** 3rd Sphere On 4th Column, Green ****
    material[20].Ambient[0] = 0.0f;
    material[20].Ambient[1] = 0.05f;
    material[20].Ambient[2] = 0.0f;
    material[20].Ambient[3] = 1.0f;

    material[20].Diffuse[0] = 0.4f;
    material[20].Diffuse[1] = 0.5f;
    material[20].Diffuse[2] = 0.4f;
    material[20].Diffuse[3] = 1.0f;

    material[20].Specular[0] = 0.04f;
    material[20].Specular[1] = 0.7f;
    material[20].Specular[2] = 0.04f;
    material[20].Specular[3] = 1.0f;

    material[20].Shininess[0] = 0.078125f * 128.0f;

    // *** 4th Sphere On 4th Column, Red ****
    material[21].Ambient[0] = 0.05f;
    material[21].Ambient[1] = 0.0f;
    material[21].Ambient[2] = 0.0f;
    material[21].Ambient[3] = 1.0f;

    material[21].Diffuse[0] = 0.5f;
    material[21].Diffuse[1] = 0.4f;
    material[21].Diffuse[2] = 0.4f;
    material[21].Diffuse[3] = 1.0f;

    material[21].Specular[0] = 0.7f;
    material[21].Specular[1] = 0.04f;
    material[21].Specular[2] = 0.04f;
    material[21].Specular[3] = 1.0f;

    material[21].Shininess[0] = 0.078125f * 128.0f;

    // *** 5th Sphere On 4th Column, White ****
    material[22].Ambient[0] = 0.05f;
    material[22].Ambient[1] = 0.05f;
    material[22].Ambient[2] = 0.05f;
    material[22].Ambient[3] = 1.0f;

    material[22].Diffuse[0] = 0.5f;
    material[22].Diffuse[1] = 0.5f;
    material[22].Diffuse[2] = 0.5f;
    material[22].Diffuse[3] = 1.0f;

    material[22].Specular[0] = 0.7f;
    material[22].Specular[1] = 0.7f;
    material[22].Specular[2] = 0.7f;
    material[22].Specular[3] = 1.0f;

    material[22].Shininess[0] = 0.078125f * 128.0f;

    // *** 6th Sphere On 4th Column, Yello ****
    material[23].Ambient[0] = 0.05f;
    material[23].Ambient[1] = 0.05f;
    material[23].Ambient[2] = 0.0f;
    material[23].Ambient[3] = 1.0f;

    material[23].Diffuse[0] = 0.5f;
    material[23].Diffuse[1] = 0.5f;
    material[23].Diffuse[2] = 0.4f;
    material[23].Diffuse[3] = 1.0f;

    material[23].Specular[0] = 0.7f;
    material[23].Specular[1] = 0.7f;
    material[23].Specular[2] = 0.04f;
    material[23].Specular[3] = 1.0f;

    material[23].Shininess[0] = 0.078125f * 128.0f;
}


- (void) mouseDown:(NSEvent *) theEvent
{
    [self setNeedsDisplay:YES];
}

- (void) mouseDragged:(NSEvent *) theEvent
{
    //code
}

- (void) rightMouseDown:(NSEvent *)theEvent
{
    [self setNeedsDisplay:YES];
}

- (void) dealloc
{
    //code
    if (vbo_Element_Sphere)
    {
        glDeleteBuffers(1, &vbo_Element_Sphere);
        vbo_Element_Sphere = 0;
    }
    
    if (vbo_Position_Sphere)
    {
        glDeleteBuffers(1, &vbo_Position_Sphere);
        vbo_Position_Sphere = 0;
    }
    
    if (vbo_Normal_Sphere)
    {
        glDeleteBuffers(1, &vbo_Normal_Sphere);
        vbo_Normal_Sphere = 0;
    }
    
    if (vao_Sphere)
    {
        glDeleteVertexArrays(1, &vao_Sphere);
        vao_Sphere = 0;
    }

    if (gShaderProgramObject_PV)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(gShaderProgramObject_PV);
        //Ask Program How Many Shaders are Attached to you
        glGetProgramiv(gShaderProgramObject_PV, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(gShaderProgramObject_PV, shaderCount, &shaderCount, pShaders);
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(gShaderProgramObject_PV, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gShaderProgramObject_PV);
        gShaderProgramObject_PV = 0;
        glUseProgram(0);
    }

    if (gShaderProgramObject_PF)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(gShaderProgramObject_PF);
        //Ask Program How Many Shaders are Attached to you
        glGetProgramiv(gShaderProgramObject_PF, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(gShaderProgramObject_PF, shaderCount, &shaderCount, pShaders);
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(gShaderProgramObject_PF, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gShaderProgramObject_PF);
        gShaderProgramObject_PF = 0;
        glUseProgram(0);
    }
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags glagsIn,
                                CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *) pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}