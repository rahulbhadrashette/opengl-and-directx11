#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"
#import "Sphere.h"
#import <math.h>

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags, CVOptionFlags *, void *);

//global variable
FILE *gpFile=NULL;

//interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate> 
@end

@interface GLView : NSOpenGLView
@end

//entry point function
int main (int argc, const char* argv[]) {
    //code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc] init];

    NSApp=[NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc] init]];

    [NSApp run];

    [pPool release];
    return(0);
}

@implementation AppDelegate
{
    @private 
        NSWindow *window;
        GLView *glView;
}

- (void) applicationDidFinishLaunching :(NSNotification *)aNotification
{
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

    const char *pszLogFileNameWithPath= [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath,"w");

    if(gpFile==NULL)
    {
        printf("Can not open log file.\nExitting ...\n");
        [self release];
        [NSApp terminate:self];
    }

    fprintf(gpFile, "Program is started successfully.\n");

    NSRect win_rect;
    win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

    window = [[NSWindow alloc] initWithContentRect:win_rect
                                styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                    backing:NSBackingStoreBuffered
                                        defer:NO];

    [window setTitle:@"OpenGL Diffuse Light on Sphere...RahulBhadrashette...!!!"];

    [window center];

    glView=[[GLView alloc] initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

- (void) applicationWillTerminate :(NSNotification *) notification
{
    //code
    fprintf(gpFile, "Program is terminated successfully\n");
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
    //code
    [NSApp terminate:self];
}

- (void) dealloc
{
    [glView release];
    [window release];
    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint gShaderProgramObject_PV;
    GLuint gShaderProgramObject_PF;

    struct Light
    {
        GLfloat Ambient[4];
        GLfloat Diffuse[4];
        GLfloat Specular[4];
        GLfloat Position[4];
    };

    struct  Light light[3];

    GLfloat materialAmbient[4];//Ka
    GLfloat materialDiffuse[4];//Kd
    GLfloat materialSpecular[4];//Ks
    GLfloat materialShininess[1];//128.0f;

    GLuint vao_Sphere;
    GLuint vbo_Position_Sphere;
    GLuint vbo_Normal_Sphere;
    GLuint vbo_Element_Sphere;

    //For Sphere
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    short sphere_elements[2280];

    float gNumVertices;
    float gNumElements;

    bool gbLight;
    GLint changeKeyPressed;
    GLfloat Co1;
    GLfloat Co2;

    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    vmath::mat4 projectionMatrix;

    //******************* vertex Variables ***********
    GLuint modelUniform;
    GLuint viewUniform;
    GLuint projectionUniform;

    GLuint LaUniform_Red;
    GLuint LdUniform_Red;
    GLuint LsUniform_Red;
    GLuint lightPositionUniform_Red;

    GLuint LaUniform_Green;
    GLuint LdUniform_Green;
    GLuint LsUniform_Green;
    GLuint lightPositionUniform_Green;

    GLuint LaUniform_Blue;
    GLuint LdUniform_Blue;
    GLuint LsUniform_Blue;
    GLuint lightPositionUniform_Blue;

    GLuint KaUniform;
    GLuint KdUniform;
    GLuint KsUniform;

    GLuint materialShininessUniform;
    GLuint lKeyIsPressedUniform;

    //********** fragment Variables *******
    GLuint modelUniform_RMB;
    GLuint viewUniform_RMB;
    GLuint projectionUniform_RMB;

    GLuint LaUniform_Red_RMB;
    GLuint LdUniform_Red_RMB;
    GLuint LsUniform_Red_RMB;
    GLuint lightPositionUniform_Red_RMB;

    GLuint LaUniform_Green_RMB;
    GLuint LdUniform_Green_RMB;
    GLuint LsUniform_Green_RMB;
    GLuint lightPositionUniform_Green_RMB;

    GLuint LaUniform_Blue_RMB;
    GLuint LdUniform_Blue_RMB;
    GLuint LsUniform_Blue_RMB;
    GLuint lightPositionUniform_Blue_RMB;

    GLuint KaUniform_RMB;
    GLuint KdUniform_RMB;
    GLuint KsUniform_RMB;

    GLuint materialShininessUniform_RMB;
    GLuint lKeyIsPressedUniform_RMB;
}

- (id)initWithFrame:(NSRect)frame 
{
    self=[super initWithFrame:frame];

    if(self) {
        [[self window] setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };

        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel format is available. Exitting...");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }

    return self;
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *)pOutputTime 
{
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version :%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];


    [self perVertexShaderFunction];
    [self perFragmentShaderFunction];

    getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();

    //Position
    glGenVertexArrays(1, &vao_Sphere);
    glBindVertexArray(vao_Sphere);
    glGenBuffers(1, &vbo_Position_Sphere);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Sphere);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Normal
    glGenBuffers(1, &vbo_Normal_Sphere);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Normal_Sphere);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Element vbo
    glGenBuffers(1, &vbo_Element_Sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    //cull back face for better performance as we dont need back as of now
    //glEnable(GL_CULL_FACE);

    //set background color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    //set projection matrix to identity
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback,self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)perVertexShaderFunction
{
    //** VERTEX SHADER
    //Define Vertex Shader Object
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    //Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec4 vertexPosition;" \
    "in vec3 vertexNormal;" \

    "uniform mat4 u_modelMatrix;" \
    "uniform mat4 u_viewMatrix;" \
    "uniform mat4 u_projection_matrix;" \

    "uniform vec3 u_La_Red;" \
    "uniform vec3 u_Ld_Red;" \
    "uniform vec3 u_Ls_Red;" \

    "uniform vec3 u_La_Green;" \
    "uniform vec3 u_Ld_Green;" \
    "uniform vec3 u_Ls_Green;" \

    "uniform vec3 u_La_Blue;" \
    "uniform vec3 u_Ld_Blue;" \
    "uniform vec3 u_Ls_Blue;" \

    "uniform vec3 u_Ka;" \
    "uniform vec3 u_Kd;" \
    "uniform vec3 u_Ks;" \

    "uniform float u_materialShininess;" \
    "uniform int u_lKeyIsPressed;" \

    "uniform vec4 u_Light_Position_Red;" \
    "uniform vec4 u_Light_Position_Green;" \
    "uniform vec4 u_Light_Position_Blue;" \

    "out vec3 phong_AdsLight;" \

    "void main(void)" \
    "{" \
        "if(u_lKeyIsPressed == 1)" \
        "{" \
            "vec4 eye_coordinate = u_viewMatrix * u_modelMatrix * vertexPosition;" \
            "vec3 tnorm = normalize(mat3(u_viewMatrix * u_modelMatrix) * vertexNormal);" \
            //For Red Light
            "vec3 lightdirection_Red = normalize(vec3(u_Light_Position_Red - eye_coordinate));" \

            "float tn_dot_lightDir_Red = max(dot(lightdirection_Red, tnorm), 0.0);" \

            "vec3 reflectionVector_Red = reflect(-lightdirection_Red, tnorm);" \

            "vec3 viewerVector = normalize(vec3(-eye_coordinate.xyz));" \

            "vec3 ambient_Red = u_La_Red * u_Ka;" \

            "vec3 diffuse_Red = u_Ld_Red * u_Kd * tn_dot_lightDir_Red;" \

            "vec3 specular_Red = u_Ls_Red * u_Ks * pow(max(dot(reflectionVector_Red, viewerVector), 0.0), u_materialShininess);" \

            //For Green Light
            "vec3 lightdirection_Green = normalize(vec3(u_Light_Position_Green - eye_coordinate));" \

            "float tn_dot_lightDir_Green = max(dot(lightdirection_Green, tnorm), 0.0);" \

            "vec3 reflectionVector_Green = reflect(-lightdirection_Green, tnorm);" \

            "vec3 ambient_Green = u_La_Green * u_Ka;" \

            "vec3 diffuse_Green = u_Ld_Green * u_Kd * tn_dot_lightDir_Green;" \

            "vec3 specular_Green = u_Ls_Green * u_Ks * pow(max(dot(reflectionVector_Green, viewerVector), 0.0), u_materialShininess);" \

            //For Blue Light
            "vec3 lightdirection_Blue = normalize(vec3(u_Light_Position_Blue - eye_coordinate));" \

            "float tn_dot_lightDir_Blue = max(dot(lightdirection_Blue, tnorm), 0.0);" \

            "vec3 reflectionVector_Blue = reflect(-lightdirection_Blue, tnorm);" \

            "vec3 ambient_Blue = u_La_Blue * u_Ka;" \

            "vec3 diffuse_Blue = u_Ld_Blue * u_Kd * tn_dot_lightDir_Blue;" \

            "vec3 specular_Blue = u_Ls_Blue * u_Ks * pow(max(dot(reflectionVector_Blue, viewerVector), 0.0), u_materialShininess);" \

            "phong_AdsLight = ambient_Red + diffuse_Red + specular_Red + ambient_Green + diffuse_Green + specular_Green + ambient_Blue + diffuse_Blue + specular_Blue;" \

        "}" \
        "else" \
        "{" \
                "phong_AdsLight = vec3(1.0, 1.0, 1.0);" \
        "}" \

            "gl_Position = u_projection_matrix * u_viewMatrix * u_modelMatrix * vertexPosition;" \
    "}";

    //"mat3 normalmatrix = mat3(transpose(inverse(u_mv_matrix)))" \   OR "mat3 normalmatrix = mat3(u_mv_matrix);" \

    //Specifing above source to the vertex shader object
    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    //Compile the Vertex Shader
    glCompileShader(vertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "vertexShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Define Fragment Shader Object
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //Write Fragment Shader Code
    const GLchar *fragmentShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec3 phong_AdsLight;" \
        "out vec4 fragColor;" \
        "void main(void)" \
        "{" \
            "fragColor = vec4(phong_AdsLight, 1.0);" \
        "}";

    // Specifing above Source to the fragment Shader Object
    glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

    //Compile the fragment Shader Object
    glCompileShader(fragmentShaderObject);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "FragmentShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Create Shader Object
    gShaderProgramObject_PV = glCreateProgram();

    //Attach VertexShader to the Shader Program
    glAttachShader(gShaderProgramObject_PV, vertexShaderObject);

    //Attach fragmentShader to the Shader Program
    glAttachShader(gShaderProgramObject_PV, fragmentShaderObject);

    // Pre Linking Binding to Vertex Attribute
    glBindAttribLocation(gShaderProgramObject_PV, AMC_ATTRIBUTE_POSITION, "vertexPosition");
    glBindAttribLocation(gShaderProgramObject_PV, AMC_ATTRIBUTE_NORMAL, "vertexNormal");

    //Link the Shader Program
    glLinkProgram(gShaderProgramObject_PV);

    GLint  iProgramLinkStatus = 0;
    GLint iLInfoLogLength = 0;
    GLint *szLogLength = NULL;

    glGetProgramiv(gShaderProgramObject_PV, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject_PV, GL_INFO_LOG_LENGTH, &iLInfoLogLength);
        if (iInfoLogLength)
        {
            szInfoLog = (GLchar*)malloc(iLInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject_PV, iLInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "LinkShaderProgramObject:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Post Linking Retriving Uniform Location
    modelUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_modelMatrix");
    viewUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_viewMatrix");
    projectionUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_projection_matrix");

    LaUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_La_Red");
    LdUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_Ld_Red");
    LsUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_Ls_Red");

    LaUniform_Green = glGetUniformLocation(gShaderProgramObject_PV, "u_La_Green");
    LdUniform_Green = glGetUniformLocation(gShaderProgramObject_PV, "u_Ld_Green");
    LsUniform_Green = glGetUniformLocation(gShaderProgramObject_PV, "u_Ls_Green");

    LaUniform_Blue = glGetUniformLocation(gShaderProgramObject_PV, "u_La_Blue");
    LdUniform_Blue = glGetUniformLocation(gShaderProgramObject_PV, "u_Ld_Blue");
    LsUniform_Blue = glGetUniformLocation(gShaderProgramObject_PV, "u_Ls_Blue");

    KaUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_Ka");
    KdUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_Kd");
    KsUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_Ks");

    materialShininessUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_materialShininess");
    lightPositionUniform_Red = glGetUniformLocation(gShaderProgramObject_PV, "u_Light_Position_Red");
    lightPositionUniform_Green = glGetUniformLocation(gShaderProgramObject_PV, "u_Light_Position_Green");
    lightPositionUniform_Blue = glGetUniformLocation(gShaderProgramObject_PV, "u_Light_Position_Blue");
    lKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject_PV, "u_lKeyIsPressed");
}

-(void)perFragmentShaderFunction
{
    //** VERTEX SHADER
    //Define Vertex Shader Object
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    //Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec4 vertexPosition_RMB;" \
        "in vec3 vertexNormal_RMB;" \

        "uniform mat4 u_modelMatrix_RMB;" \
        "uniform mat4 u_viewMatrix_RMB;" \
        "uniform mat4 u_projection_matrix_RMB;" \

        "uniform int u_lKeyIsPressed_RMB;" \
        "uniform vec4 u_Light_Position_Red_RMB;" \
        "uniform vec4 u_Light_Position_Green_RMB;" \
        "uniform vec4 u_Light_Position_Blue_RMB;" \

        "out vec3 tnorm_RMB;" \
        "out vec3 lightdirection_Red_RMB;" \
        "out vec3 lightdirection_Green_RMB;" \
        "out vec3 lightdirection_Blue_RMB;" \
        "out vec3 viewerVector_RMB;" \

        "void main(void)" \
        "{" \
            "if(u_lKeyIsPressed_RMB == 1)" \
            "{" \
                "vec4 eye_coordinate_RMB = u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" \
                "tnorm_RMB = mat3(u_viewMatrix_RMB * u_modelMatrix_RMB) * vertexNormal_RMB;" \
                //For Red Light Calculations
                "lightdirection_Red_RMB = vec3(u_Light_Position_Red_RMB - eye_coordinate_RMB);" \
                //For Green Light Calculations
                "lightdirection_Green_RMB = vec3(u_Light_Position_Green_RMB - eye_coordinate_RMB);" \
                //For Blue Light Calculations
                "lightdirection_Blue_RMB = vec3(u_Light_Position_Blue_RMB - eye_coordinate_RMB);" \

                "viewerVector_RMB = vec3(-eye_coordinate_RMB.xyz);" \
            "}" \

            "gl_Position = u_projection_matrix_RMB * u_viewMatrix_RMB * u_modelMatrix_RMB * vertexPosition_RMB;" \
        "}";

    //"mat3 normalmatrix = mat3(transpose(inverse(u_mv_matrix)))" \   OR "mat3 normalmatrix = mat3(u_mv_matrix);" \

    //Specifing above source to the vertex shader object
    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    //Compile the Vertex Shader
    glCompileShader(vertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "vertexShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Define Fragment Shader Object
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //Write Fragment Shader Code
    const GLchar *fragmentShaderSourceCode =
            "#version 410 core" \
        "\n" \
        "in vec3 tnorm_RMB;" \
        "in vec3 lightdirection_Red_RMB;" \
        "in vec3 lightdirection_Green_RMB;" \
        "in vec3 lightdirection_Blue_RMB;" \
        "in vec3 viewerVector_RMB;" \

        "uniform vec3 u_La_Red_RMB;" \
        "uniform vec3 u_Ld_Red_RMB;" \
        "uniform vec3 u_Ls_Red_RMB;" \

        "uniform vec3 u_La_Green_RMB;" \
        "uniform vec3 u_Ld_Green_RMB;" \
        "uniform vec3 u_Ls_Green_RMB;" \

        "uniform vec3 u_La_Blue_RMB;" \
        "uniform vec3 u_Ld_Blue_RMB;" \
        "uniform vec3 u_Ls_Blue_RMB;" \

        "uniform vec3 u_Ka_RMB;" \
        "uniform vec3 u_Kd_RMB;" \
        "uniform vec3 u_Ks_RMB;" \

        "uniform float u_materialShininess_RMB;" \
        "uniform int u_lKeyIsPressed_RMB;" \

        
        "out vec4 fragColor_RMB;" \

        "void main(void)" \
        "{" \
            "vec3 phong_AdsLight_RMB;" \
            "if(u_lKeyIsPressed_RMB == 1)" \
            "{" \
                //For Red Light Calculations
                "vec3 normalize_tnorm_RMB = normalize(tnorm_RMB);" \

                "vec3 normalize_lightdirection_Red_RMB = normalize(lightdirection_Red_RMB);" \

                "vec3 normalize_viewerVector_RMB = normalize(viewerVector_RMB);" \

                "vec3 reflectionVector_Red_RMB = reflect(-normalize_lightdirection_Red_RMB, normalize_tnorm_RMB);" \

                "float tn_dot_lightDir_Red_RMB = max(dot(normalize_lightdirection_Red_RMB, normalize_tnorm_RMB), 0.0);" \

                "vec3 ambient_Red_RMB = u_La_Red_RMB * u_Ka_RMB;" \

                "vec3 diffuse_Red_RMB = u_Ld_Red_RMB * u_Kd_RMB * tn_dot_lightDir_Red_RMB;" \

                "vec3 specular_Red_RMB = u_Ls_Red_RMB * u_Ks_RMB * pow(max(dot(reflectionVector_Red_RMB, normalize_viewerVector_RMB), 0.0), u_materialShininess_RMB);" \

                //For Green Light Calculations
                "vec3 normalize_lightdirection_Green_RMB = normalize(lightdirection_Green_RMB);" \

                "vec3 reflectionVector_Green_RMB = reflect(-normalize_lightdirection_Green_RMB, normalize_tnorm_RMB);" \

                "float tn_dot_lightDir_Green_RMB = max(dot(normalize_lightdirection_Green_RMB, normalize_tnorm_RMB), 0.0);" \

                "vec3 ambient_Green_RMB = u_La_Green_RMB * u_Ka_RMB;" \

                "vec3 diffuse_Green_RMB = u_Ld_Green_RMB * u_Kd_RMB * tn_dot_lightDir_Green_RMB;" \

                "vec3 specular_Green_RMB = u_Ls_Green_RMB * u_Ks_RMB * pow(max(dot(reflectionVector_Green_RMB, normalize_viewerVector_RMB), 0.0), u_materialShininess_RMB);" \

                //For Blue Light Calculations
                "vec3 normalize_lightdirection_Blue_RMB = normalize(lightdirection_Blue_RMB);" \

                "vec3 reflectionVector_Blue_RMB = reflect(-normalize_lightdirection_Blue_RMB, normalize_tnorm_RMB);" \

                "float tn_dot_lightDir_Blue_RMB = max(dot(normalize_lightdirection_Blue_RMB, normalize_tnorm_RMB), 0.0);" \

                "vec3 ambient_Blue_RMB = u_La_Blue_RMB * u_Ka_RMB;" \

                "vec3 diffuse_Blue_RMB = u_Ld_Blue_RMB * u_Kd_RMB * tn_dot_lightDir_Blue_RMB;" \

                "vec3 specular_Blue_RMB = u_Ls_Blue_RMB * u_Ks_RMB * pow(max(dot(reflectionVector_Blue_RMB, normalize_viewerVector_RMB), 0.0), u_materialShininess_RMB);" \

                "phong_AdsLight_RMB = ambient_Red_RMB + diffuse_Red_RMB + specular_Red_RMB + ambient_Green_RMB + diffuse_Green_RMB + specular_Green_RMB + ambient_Blue_RMB + diffuse_Blue_RMB + specular_Blue_RMB;" \

            "}" \
            "else" \
            "{" \

                "phong_AdsLight_RMB = vec3(1.0, 1.0, 1.0);" \

            "}" \

            "fragColor_RMB = vec4(phong_AdsLight_RMB, 1.0);" \
        "}";


    // Specifing above Source to the fragment Shader Object
    glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

    //Compile the fragment Shader Object
    glCompileShader(fragmentShaderObject);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "FragmentShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Create Shader Object
    gShaderProgramObject_PF = glCreateProgram();

    //Attach VertexShader to the Shader Program
    glAttachShader(gShaderProgramObject_PF, vertexShaderObject);

    //Attach fragmentShader to the Shader Program
    glAttachShader(gShaderProgramObject_PF, fragmentShaderObject);

    // Pre Linking Binding to Vertex Attribute
   glBindAttribLocation(gShaderProgramObject_PF, AMC_ATTRIBUTE_POSITION, "vertexPosition_RMB");
    glBindAttribLocation(gShaderProgramObject_PF, AMC_ATTRIBUTE_NORMAL, "vertexNormal_RMB");

    //Link the Shader Program
    glLinkProgram(gShaderProgramObject_PF);

    GLint  iProgramLinkStatus = 0;
    GLint iLInfoLogLength = 0;
    GLint *szLogLength = NULL;

    glGetProgramiv(gShaderProgramObject_PF, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject_PF, GL_INFO_LOG_LENGTH, &iLInfoLogLength);
        if (iInfoLogLength)
        {
            szInfoLog = (GLchar*)malloc(iLInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject_PF, iLInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "LinkShaderProgramObject:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Post Linking Retriving Uniform Location
    modelUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_modelMatrix_RMB");
    viewUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_viewMatrix_RMB");
    projectionUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_projection_matrix_RMB");

    LaUniform_Red_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_La_Red_RMB");
    LdUniform_Red_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ld_Red_RMB");
    LsUniform_Red_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ls_Red_RMB");

    LaUniform_Green_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_La_Green_RMB");
    LdUniform_Green_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ld_Green_RMB");
    LsUniform_Green_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ls_Green_RMB");

    LaUniform_Blue_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_La_Blue_RMB");
    LdUniform_Blue_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ld_Blue_RMB");
    LsUniform_Blue_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ls_Blue_RMB");

    KaUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ka_RMB");
    KdUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Kd_RMB");
    KsUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Ks_RMB");

    materialShininessUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_materialShininess_RMB");
    lightPositionUniform_Red_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Light_Position_Red_RMB");
    lightPositionUniform_Green_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Light_Position_Green_RMB");
    lightPositionUniform_Blue_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_Light_Position_Blue_RMB");
    lKeyIsPressedUniform_RMB = glGetUniformLocation(gShaderProgramObject_PF, "u_lKeyIsPressed_RMB");
}

-(void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;

    if(height==0)
    {
        height=1;
    }
    
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    
   projectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void) drawView
{
    //code
    static GLfloat angleRotation = 0.0f;
    [self Light_Array];
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    if(changeKeyPressed == 2)
    {
        glUseProgram(gShaderProgramObject_PF);
        if (gbLight)
        {
            glUniform1i(lKeyIsPressedUniform_RMB, 1);
            glUniform3fv(LaUniform_Red_RMB, 1, light[0].Ambient);
            glUniform3fv(LdUniform_Red_RMB, 1, light[0].Diffuse);
            glUniform3fv(LsUniform_Red_RMB, 1, light[0].Specular);
            glUniform4fv(lightPositionUniform_Red_RMB, 1, light[0].Position);

            glUniform3fv(LaUniform_Green_RMB, 1, light[1].Ambient);
            glUniform3fv(LdUniform_Green_RMB, 1, light[1].Diffuse);
            glUniform3fv(LsUniform_Green_RMB, 1, light[1].Specular);
            glUniform4fv(lightPositionUniform_Green_RMB, 1, light[1].Position);

            glUniform3fv(LaUniform_Blue_RMB, 1, light[2].Ambient);
            glUniform3fv(LdUniform_Blue_RMB, 1, light[2].Diffuse);
            glUniform3fv(LsUniform_Blue_RMB, 1, light[2].Specular);
            glUniform4fv(lightPositionUniform_Blue_RMB, 1, light[2].Position);

            glUniform3fv(KaUniform_RMB, 1, materialAmbient);
            glUniform3fv(KdUniform_RMB, 1, materialDiffuse);
            glUniform3fv(KsUniform_RMB, 1, materialSpecular);
            glUniform1fv(materialShininessUniform_RMB, 1, materialShininess);
        }
        else
        {
            glUniform1i(lKeyIsPressedUniform, 0);
        }
    }
    else 
    {
        glUseProgram(gShaderProgramObject_PV);
        if (gbLight)
        {
            glUniform1i(lKeyIsPressedUniform, 1);
            glUniform3fv(LaUniform_Red, 1, light[0].Ambient);
            glUniform3fv(LdUniform_Red, 1, light[0].Diffuse);
            glUniform3fv(LsUniform_Red, 1, light[0].Specular);
            glUniform4fv(lightPositionUniform_Red, 1, light[0].Position);

            glUniform3fv(LaUniform_Green, 1, light[1].Ambient);
            glUniform3fv(LdUniform_Green, 1, light[1].Diffuse);
            glUniform3fv(LsUniform_Green, 1, light[1].Specular);
            glUniform4fv(lightPositionUniform_Green, 1, light[1].Position);

            glUniform3fv(LaUniform_Blue, 1, light[2].Ambient);
            glUniform3fv(LdUniform_Blue, 1, light[2].Diffuse);
            glUniform3fv(LsUniform_Blue, 1, light[2].Specular);
            glUniform4fv(lightPositionUniform_Blue, 1, light[2].Position);

            glUniform3fv(KaUniform, 1, materialAmbient);
            glUniform3fv(KdUniform, 1, materialDiffuse);
            glUniform3fv(KsUniform, 1, materialSpecular);
            glUniform1fv(materialShininessUniform, 1, materialShininess);
        }
        else
        {
            glUniform1i(lKeyIsPressedUniform_RMB, 0);
        }
    }

    //Declatation of Matricex
    vmath::mat4 translationMatrix;
    vmath::mat4 rotationMatrix;
    
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();

    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    

    //Do Necessary Matrix Multiplication
    modelMatrix = modelMatrix * translationMatrix;


    //Send Necessary Matrix to Shader in Respective Uniform
    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, projectionMatrix);

    glUniformMatrix4fv(modelUniform_RMB, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(viewUniform_RMB, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionUniform_RMB, 1, GL_FALSE, projectionMatrix);

    //BindWith vao
    glBindVertexArray(vao_Sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
    //Draw the Necessary Scnes
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //UnBind vao
    glBindVertexArray(0);

    //UnUse Program
    glUseProgram(0);

    CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

    angleRotation = angleRotation + 0.01f;
    if (angleRotation > 360.0f)
    {
        angleRotation = 0.0f;
    }
    
    Co1 = (GLfloat)cos(angleRotation) * 100.0f;
    Co2 = (GLfloat)sin(angleRotation) * 100.0f;
}

- (BOOL)acceptsFirstResponder
{
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void) keyDown:(NSEvent *)theEvent
{
    int key=(int) [[theEvent characters]characterAtIndex:0];
    switch(key){

            case 'E':
            case 'e':
            case 'Q':
            case 'q':
                [self release];
                [NSApp terminate:self];
                break;

            case 'F':
            case 'f':
                    changeKeyPressed = 2;
                 break;

            case 'V':
            case 'v':
                    changeKeyPressed = 1; 
                break;

            case 'L':
            case 'l':
                    gbLight = !gbLight;
                break;
            
            case 27 : // Esc Key
                [[self window] toggleFullScreen:self];
                break;

            default:
                break;
    }
}

- (void)Light_Array
{
        //Array Zero
    light[0].Ambient[0] = 0.0f;
    light[0].Ambient[1] = 0.0f;
    light[0].Ambient[2] = 0.0f;
    light[0].Ambient[3] = 1.0f;

    light[0].Diffuse[0] = 1.0f;
    light[0].Diffuse[1] = 0.0f;
    light[0].Diffuse[2] = 0.0f;
    light[0].Diffuse[3] = 1.0f;

    light[0].Specular[0] = 1.0f;
    light[0].Specular[1] = 0.0f;
    light[0].Specular[2] = 0.0f;
    light[0].Specular[3] = 1.0f;

    light[0].Position[0] = 0.0f;
    light[0].Position[1] = Co1;
    light[0].Position[2] = Co2;
    light[0].Position[3] = 1.0f;

    //Array One
    light[1].Ambient[0] = 0.0f;
    light[1].Ambient[1] = 0.0f;
    light[1].Ambient[2] = 0.0f;
    light[1].Ambient[3] = 1.0f;
    light[1].Diffuse[0] = 0.0f;
    light[1].Diffuse[1] = 1.0f;
    light[1].Diffuse[2] = 0.0f;
    light[1].Diffuse[3] = 1.0f;
    light[1].Specular[0] = 0.0f;
    light[1].Specular[1] = 1.0f;
    light[1].Specular[2] = 0.0f;
    light[1].Specular[3] = 1.0f;
    light[1].Position[0] = Co1;
    light[1].Position[1] = 0.0f;
    light[1].Position[2] = Co2;
    light[1].Position[3] = 1.0f;

    //Array Two
    light[2].Ambient[0] = 0.0f;
    light[2].Ambient[1] = 0.0f;
    light[2].Ambient[2] = 0.0f;
    light[2].Ambient[3] = 1.0f;
    light[2].Diffuse[0] = 0.0f;
    light[2].Diffuse[1] = 0.0f;
    light[2].Diffuse[2] = 1.0f;
    light[2].Diffuse[3] = 1.0f;
    light[2].Specular[0] = 0.0f;
    light[2].Specular[1] = 0.0f;
    light[2].Specular[2] = 1.0f;
    light[2].Specular[3] = 1.0f;
    light[2].Position[0] = Co1;
    light[2].Position[1] = Co2;
    light[2].Position[2] = 0.0f;
    light[2].Position[3] = 1.0f;

    //For Material
    materialAmbient[0] = 0.0f;
    materialAmbient[1] = 0.0f; 
    materialAmbient[2] = 0.0f;
    materialAmbient[3] = 0.0f;

    materialDiffuse[0] = 0.5f;
    materialDiffuse[1] = 0.2f;
    materialDiffuse[2] = 0.7f;
    materialDiffuse[3] = 1.0f;

    materialSpecular[0] = 1.0f;
    materialSpecular[1] = 1.0f;
    materialSpecular[2] = 1.0f;
    materialSpecular[3] = 1.0f;

    materialShininess[0] = 128.0f;

}

- (void) mouseDown:(NSEvent *) theEvent
{
    [self setNeedsDisplay:YES];
}

- (void) mouseDragged:(NSEvent *) theEvent
{
    //code
}

- (void) rightMouseDown:(NSEvent *)theEvent
{
    [self setNeedsDisplay:YES];
}

- (void) dealloc
{
    //code
    if (vbo_Element_Sphere)
    {
        glDeleteBuffers(1, &vbo_Element_Sphere);
        vbo_Element_Sphere = 0;
    }
    
    if (vbo_Position_Sphere)
    {
        glDeleteBuffers(1, &vbo_Position_Sphere);
        vbo_Position_Sphere = 0;
    }
    
    if (vbo_Normal_Sphere)
    {
        glDeleteBuffers(1, &vbo_Normal_Sphere);
        vbo_Normal_Sphere = 0;
    }
    
    if (vao_Sphere)
    {
        glDeleteVertexArrays(1, &vao_Sphere);
        vao_Sphere = 0;
    }

    if (gShaderProgramObject_PV)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(gShaderProgramObject_PV);
        //Ask Program How Many Shaders are Attached to you
        glGetProgramiv(gShaderProgramObject_PV, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(gShaderProgramObject_PV, shaderCount, &shaderCount, pShaders);
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(gShaderProgramObject_PV, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gShaderProgramObject_PV);
        gShaderProgramObject_PV = 0;
        glUseProgram(0);
    }

    if (gShaderProgramObject_PF)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(gShaderProgramObject_PF);
        //Ask Program How Many Shaders are Attached to you
        glGetProgramiv(gShaderProgramObject_PF, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(gShaderProgramObject_PF, shaderCount, &shaderCount, pShaders);
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(gShaderProgramObject_PF, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gShaderProgramObject_PF);
        gShaderProgramObject_PF = 0;
        glUseProgram(0);
    }
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags glagsIn,
                                CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *) pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}