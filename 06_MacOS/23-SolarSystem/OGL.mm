#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"
#import "Sphere.h"

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags, CVOptionFlags *, void *);

//global variable
FILE *gpFile=NULL;

//interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate> 
@end

@interface GLView : NSOpenGLView
@end

//entry point function
int main (int argc, const char* argv[]) {
    //code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc] init];

    NSApp=[NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc] init]];

    [NSApp run];

    [pPool release];
    return(0);
}

@implementation AppDelegate
{
    @private 
        NSWindow *window;
        GLView *glView;
}

- (void) applicationDidFinishLaunching :(NSNotification *)aNotification
{
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

    const char *pszLogFileNameWithPath= [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath,"w");

    if(gpFile==NULL)
    {
        printf("Can not open log file.\nExitting ...\n");
        [self release];
        [NSApp terminate:self];
    }

    fprintf(gpFile, "Program is started successfully.\n");

    NSRect win_rect;
    win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

    window = [[NSWindow alloc] initWithContentRect:win_rect
                                styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                    backing:NSBackingStoreBuffered
                                        defer:NO];

    [window setTitle:@"OpenGL Diffuse Light on Sphere...RahulBhadrashette...!!!"];

    [window center];

    glView=[[GLView alloc] initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

- (void) applicationWillTerminate :(NSNotification *) notification
{
    //code
    fprintf(gpFile, "Program is terminated successfully\n");
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
    //code
    [NSApp terminate:self];
}

- (void) dealloc
{
    [glView release];
    [window release];
    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint LdUniform;
    GLuint KdUniform;
    GLuint modelViewUniform;
    GLuint prespectiveProjectionUniform;
    GLuint lightPositionUniform;
    GLuint lKeyIsPressedUniform;

    GLuint vao_Sphere;
    GLuint vbo_Position_Sphere;
    GLuint vbo_Normal_Sphere;
    GLuint vbo_Element_Sphere;

    //For Sphere
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    short sphere_elements[2280];

    float gNumVertices;
    float gNumElements;

    bool gbAnimation;
    bool gbLight;

    vmath::mat4 perspectiveProjectionMatrix;
    vmath::mat4 modelViewMatrix;
}

- (id)initWithFrame:(NSRect)frame 
{
    self=[super initWithFrame:frame];

    if(self) {
        [[self window] setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };

        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel format is available. Exitting...");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }

    return self;
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *)pOutputTime 
{
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version :%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];


    //** VERTEX SHADER
    //Define Vertex Shader Object
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    //Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "\n" \
        "in vec4 vertexPosition;" \
        "in vec3 vertexNormal;" \

        "uniform mat4 u_modelViewMatrix;" \
        "uniform mat4 u_Projection_matrix;" \

        "uniform int u_lKeyIsPressed;" \

        "uniform vec3 u_Ld;" \
        "uniform vec3 u_Kd;" \

        "uniform vec4 u_Light_Position;" \

        "out vec3 diffuse_Color;" \

        "void main(void)" \
        "{" \
            "if(u_lKeyIsPressed == 1)" \
            "{" \
                "vec4 eye_coordinate = u_modelViewMatrix * vertexPosition;" \

                "mat3 normalmatrix = mat3(transpose(inverse(u_modelViewMatrix)));" \

                "vec3 tnorm = normalize(normalmatrix * vertexNormal);" \

                "vec3 source = normalize(vec3(u_Light_Position - eye_coordinate));" \

                "diffuse_Color = u_Ld * u_Kd * dot(source, tnorm);" \
            "}" \
            "gl_Position = u_Projection_matrix * u_modelViewMatrix * vertexPosition;" \
        "}";


    //Specifing above source to the vertex shader object
    glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    //Compile the Vertex Shader
    glCompileShader(gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "vertexShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Define Fragment Shader Object
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //Write Fragment Shader Code
    const GLchar *fragmentShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec3 diffuse_Color;" \
        "out vec4 fragColor;" \
        "uniform int u_lKeyIsPressed;" \
        "void main(void)" \
        "{" \
            "if(u_lKeyIsPressed == 1)" \
            "{" \
                "fragColor = vec4(diffuse_Color, 1.0);" \
            "}" \
            "else" \
            "{" \
                "fragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
            "}" \
        "}";

    // Specifing above Source to the fragment Shader Object
    glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

    //Compile the fragment Shader Object
    glCompileShader(gFragmentShaderObject);

    GLint iFShaderCompileStatus = 0;
    GLint iFInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iFShaderCompileStatus);
    if (iFShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iFInfoLogLength);
        if (iFInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iFInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iFInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "FragmentShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Create Shader Object
    gShaderProgramObject = glCreateProgram();

    //Attach VertexShader to the Shader Program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);

    //Attach fragmentShader to the Shader Program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);

    // Pre Linking Binding to Vertex Attribute
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vertexPosition");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vertexNormal");

    //Link the Shader Program
    glLinkProgram(gShaderProgramObject);

    GLint  iProgramLinkStatus = 0;
    GLint iLInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iLInfoLogLength);
        if (iInfoLogLength)
        {
            szInfoLog = (GLchar*)malloc(iLInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iLInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "LinkShaderProgramObject:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Post Linking Retriving Uniform Location
    modelViewUniform = glGetUniformLocation(gShaderProgramObject, "u_modelViewMatrix");
    prespectiveProjectionUniform = glGetUniformLocation(gShaderProgramObject, "u_Projection_matrix");
    LdUniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
    KdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
    lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_Light_Position");
    lKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyIsPressed");
    
    
    getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();

    //Position
    glGenVertexArrays(1, &vao_Sphere);
    glBindVertexArray(vao_Sphere);
    glGenBuffers(1, &vbo_Position_Sphere);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Sphere);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Normal
    glGenBuffers(1, &vbo_Normal_Sphere);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Normal_Sphere);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Element vbo
    glGenBuffers(1, &vbo_Element_Sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    //cull back face for better performance as we dont need back as of now
    //glEnable(GL_CULL_FACE);

    //set background color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    //set projection matrix to identity
    perspectiveProjectionMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback,self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}


-(void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;

    if(height==0)
    {
        height=1;
    }
    
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    
   perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void) drawView
{
    //code 
    static float angle = 0.0f;

    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glUseProgram(gShaderProgramObject);

    if (gbLight)
    {
        glUniform1i(lKeyIsPressedUniform, 1);
        glUniform3f(LdUniform, 1.0f, 1.0f, 1.0f); //White Light
        glUniform3f(KdUniform, 0.5f, 0.5f, 0.5f); //Gray Material
        glUniform4f(lightPositionUniform, 0.0f, 0.0f, 2.0f, 1.0f);
    }
    else
    {
        glUniform1i(lKeyIsPressedUniform, 0);
    }

    //Declatation of Matricex
    vmath::mat4 modelViewMatrix;
    vmath::mat4 translationMatrix;
    vmath::mat4 scaleMatrix;
    vmath::mat4 rotationMatrix;

    //****************.  Sun ********************
    //Initialize of Matrix in identity
    modelViewMatrix = vmath::mat4::identity();
    translationMatrix = vmath::mat4::identity();
    scaleMatrix = vmath::mat4::identity();
    //rotationMatrix = vmath::mat4::identity();

    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    scaleMatrix = vmath::scale(0.60f, 0.60f, 0.60f);
    //rotationMatrix = vmath::rotate(angle, 0.0f, 1.0f, 0.0f);

    //Do Necessary Matrix Multiplication
    modelViewMatrix = modelViewMatrix * translationMatrix * scaleMatrix * rotationMatrix;
    //perspectiveProjectionMatrix;

    //Send Necessary Matrix to Shader in Respective Uniform
    glUniformMatrix4fv(modelViewUniform, 1, GL_FALSE, modelViewMatrix);
    glUniformMatrix4fv(prespectiveProjectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    //BindWith vao
    glBindVertexArray(vao_Sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
    //Draw the Necessary Scnes
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //UnBind vao
    glBindVertexArray(0);

    //****************** Earth *********************
    //Initialize of Matrix in identity
    //modelViewMatrix = vmath::mat4::identity();
    //modelViewMatrix = vmath::mat4::identity();
    scaleMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();

    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, 0.0f, -1.0f);
    scaleMatrix = vmath::scale(0.40f, 0.40f, 0.40f);
    rotationMatrix = vmath::rotate(angle, 0.0f, 1.0f, 0.0f);

    //Do Necessary Matrix Multiplication
    modelViewMatrix = modelViewMatrix * translationMatrix * scaleMatrix * rotationMatrix;
    //perspectiveProjectionMatrix;

    //Send Necessary Matrix to Shader in Respective Uniform
    glUniformMatrix4fv(modelViewUniform, 1, GL_FALSE, modelViewMatrix);
    glUniformMatrix4fv(prespectiveProjectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    //BindWith vao
    glBindVertexArray(vao_Sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
    //Draw the Necessary Scnes
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //UnBind vao
    glBindVertexArray(0);

    //******************* Moon ***********************
    //Initialize of Matrix in identity
    //modelViewMatrix = vmath::mat4::identity();
    translationMatrix = vmath::mat4::identity();
    scaleMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();

    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, 0.0f, -0.8f);
    scaleMatrix = vmath::scale(0.35f, 0.35f, 0.35f);
    rotationMatrix = vmath::rotate(angle, 0.0f, 1.0f, 0.0f);

    //Do Necessary Matrix Multiplication
    modelViewMatrix = modelViewMatrix * translationMatrix * scaleMatrix * rotationMatrix;
    //perspectiveProjectionMatrix;

    //Send Necessary Matrix to Shader in Respective Uniform
    glUniformMatrix4fv(modelViewUniform, 1, GL_FALSE, modelViewMatrix);
    glUniformMatrix4fv(prespectiveProjectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    //BindWith vao
    glBindVertexArray(vao_Sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Element_Sphere);
    //Draw the Necessary Scnes
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //UnBind vao
    glBindVertexArray(0);


    //UnUse Program
    glUseProgram(0);

    CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

    angle = angle + 0.6f;
    if (angle >= 360.0f)
    {
        angle = 0.0f;
    }
}

- (BOOL)acceptsFirstResponder
{
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void) keyDown:(NSEvent *)theEvent
{
    int key=(int) [[theEvent characters]characterAtIndex:0];
    switch(key){

            case 27 : // Esc Key
                [self release];
                [NSApp terminate:self];
                break;

            case 'A':
            case 'a':
                    gbAnimation = !gbAnimation;
                break;

            case 'L':
            case 'l':
                    gbLight = !gbLight;
                break;
            case 'F' : 
            case 'f' :
                [[self window] toggleFullScreen:self];
                break;

            default:
                break;
    }
}

- (void) mouseDown:(NSEvent *) theEvent
{
    [self setNeedsDisplay:YES];
}

- (void) mouseDragged:(NSEvent *) theEvent
{
    //code
}

- (void) rightMouseDown:(NSEvent *)theEvent
{
    [self setNeedsDisplay:YES];
}

- (void) dealloc
{
    //code
    if (vbo_Element_Sphere)
    {
        glDeleteBuffers(1, &vbo_Element_Sphere);
        vbo_Element_Sphere = 0;
    }
    
    if (vbo_Position_Sphere)
    {
        glDeleteBuffers(1, &vbo_Position_Sphere);
        vbo_Position_Sphere = 0;
    }
    
    if (vbo_Normal_Sphere)
    {
        glDeleteBuffers(1, &vbo_Normal_Sphere);
        vbo_Normal_Sphere = 0;
    }
    
    if (vao_Sphere)
    {
        glDeleteVertexArrays(1, &vao_Sphere);
        vao_Sphere = 0;
    }

    
    if (gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(gShaderProgramObject);
        //Ask Program How Many Shaders are Attached to you
        glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gShaderProgramObject);
        gShaderProgramObject = 0;
        glUseProgram(0);
    }


    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags glagsIn,
                                CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *) pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}