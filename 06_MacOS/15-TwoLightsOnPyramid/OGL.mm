#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags, CVOptionFlags *, void *);

//global variable
FILE *gpFile=NULL;

//interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate> 
@end

@interface GLView : NSOpenGLView
@end

//entry point function
int main (int argc, const char* argv[]) {
    //code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc] init];

    NSApp=[NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc] init]];

    [NSApp run];

    [pPool release];
    return(0);
}

@implementation AppDelegate
{
    @private 
        NSWindow *window;
        GLView *glView;
}

- (void) applicationDidFinishLaunching :(NSNotification *)aNotification
{
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

    const char *pszLogFileNameWithPath= [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath,"w");

    if(gpFile==NULL)
    {
        printf("Can not open log file.\nExitting ...\n");
        [self release];
        [NSApp terminate:self];
    }

    fprintf(gpFile, "Program is started successfully.\n");

    NSRect win_rect;
    win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

    window = [[NSWindow alloc] initWithContentRect:win_rect
                                styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                    backing:NSBackingStoreBuffered
                                        defer:NO];

    [window setTitle:@"OpenGL Perspective Window...RahulBhadrashette...!!!"];

    [window center];

    glView=[[GLView alloc] initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

- (void) applicationWillTerminate :(NSNotification *) notification
{
    //code
    fprintf(gpFile, "Program is terminated successfully\n");
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
    //code
    [NSApp terminate:self];
}

- (void) dealloc
{
    [glView release];
    [window release];
    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    bool gbAnimation;
    bool gbLight;

    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    vmath::mat4 projectionMatrix;

    GLuint modelUniform;
    GLuint viewUniform;
    GLuint projectionUniform;

    GLuint LaUniform_Red;
    GLuint LdUniform_Red;
    GLuint LsUniform_Red;
    GLuint lightPositionUniform_Red;

    GLuint LaUniform_Blue;
    GLuint LdUniform_Blue;
    GLuint LsUniform_Blue;
    GLuint lightPositionUniform_Blue;

    GLuint KaUniform;
    GLuint KdUniform;
    GLuint KsUniform;

    GLuint materialShininessUniform;
    GLuint lKeyIsPressedUniform;

    struct Light
    {
        GLfloat Ambient[4];
        GLfloat Diffuse[4];
        GLfloat Specular[4];
        GLfloat Position[4];
    };

    struct  Light light[2];

    GLfloat materialAmbient[4];// = { 0.0f, 0.0f, 0.0f, 0.0f }; //Ka
    GLfloat materialDiffuse[4]; ///= { 1.0f, 1.0f, 1.0f, 1.0f };  //Kd
    GLfloat materialSpecular[4];// = { 1.0f, 1.0f, 1.0f, 1.0f };  //Ks
    GLfloat materialShininess[1];// = { 50.0f }; //128.0f;

    GLuint vao_Pyramid;
    GLuint vbo_Position_Pyramid;
    GLuint vbo_Normal_Pyramid;

}

- (id)initWithFrame:(NSRect)frame 
{
    self=[super initWithFrame:frame];

    if(self) {
        [[self window] setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };

        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel format is available. Exitting...");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }

    return self;
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *)pOutputTime 
{
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version :%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];


    //** VERTEX SHADER
    //Define Vertex Shader Object
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    //Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode =
        "#version 410 core" \
        "\n" \
                                    "in vec4 vertexPosition;" \
                                    "in vec3 vertexNormal;" \

                                    "uniform mat4 u_modelMatrix;" \
                                    "uniform mat4 u_viewMatrix;" \
                                    "uniform mat4 u_projection_matrix;" \

                                    "uniform vec3 u_La_Red;" \
                                    "uniform vec3 u_Ld_Red;" \
                                    "uniform vec3 u_Ls_Red;" \

                                    "uniform vec3 u_La_Blue;" \
                                    "uniform vec3 u_Ld_Blue;" \
                                    "uniform vec3 u_Ls_Blue;" \

                                    "uniform vec3 u_Ka;" \
                                    "uniform vec3 u_Kd;" \
                                    "uniform vec3 u_Ks;" \

                                    "uniform float u_materialShininess;" \
                                    "uniform int u_lKeyIsPressed;" \
                                    "uniform vec4 u_Light_Position_Red;" \
                                    "uniform vec4 u_Light_Position_Blue;" \

                                    "out vec3 phong_AdsLight;" \

                                    "void main(void)" \
                                    "{" \
                                        "if(u_lKeyIsPressed == 1)" \
                                        "{" \
                                            "vec4 eye_coordinate = u_viewMatrix * u_modelMatrix * vertexPosition;" \

                                            "vec3 tnorm = normalize(mat3(u_viewMatrix * u_modelMatrix) * vertexNormal);" \
                                            //For Red Light
                                            "vec3 lightdirection_Red = normalize(vec3(u_Light_Position_Red - eye_coordinate));" \

                                            "float tn_dot_lightDir_Red = max(dot(lightdirection_Red, tnorm), 0.0);" \

                                            "vec3 reflectionVector_Red = reflect(-lightdirection_Red, tnorm);" \

                                            "vec3 viewerVector = normalize(vec3(-eye_coordinate.xyz));" \

                                            "vec3 ambient_Red = u_La_Red * u_Ka;" \

                                            "vec3 diffuse_Red = u_Ld_Red * u_Kd * tn_dot_lightDir_Red;" \

                                            "vec3 specular_Red = u_Ls_Red * u_Ks * pow(max(dot(reflectionVector_Red, viewerVector), 0.0), u_materialShininess);" \

                                            //For Blue Light
                                            "vec3 lightdirection_Blue = normalize(vec3(u_Light_Position_Blue - eye_coordinate));" \

                                            "float tn_dot_lightDir_Blue = max(dot(lightdirection_Blue, tnorm), 0.0);" \

                                            "vec3 reflectionVector_Blue = reflect(-lightdirection_Blue, tnorm);" \

                                            "vec3 ambient_Blue = u_La_Blue * u_Ka;" \

                                            "vec3 diffuse_Blue = u_Ld_Blue * u_Kd * tn_dot_lightDir_Blue;" \

                                            "vec3 specular_Blue = u_Ls_Blue * u_Ks * pow(max(dot(reflectionVector_Blue, viewerVector), 0.0), u_materialShininess);" \

                                            "phong_AdsLight = ambient_Red + diffuse_Red + specular_Red + ambient_Blue + diffuse_Blue + specular_Blue;" \

                                        "}" \
                                        "else" \
                                        "{" \
                                                "phong_AdsLight = vec3(1.0, 1.0, 1.0);" \
                                        "}" \

                                        "gl_Position = u_projection_matrix * u_viewMatrix * u_modelMatrix * vertexPosition;" \
                                    "}";

    //Specifing above source to the vertex shader object
    glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    //Compile the Vertex Shader
    glCompileShader(gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "vertexShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Define Fragment Shader Object
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //Write Fragment Shader Code
    const GLchar *fragmentShaderSourceCode =
        "#version 410 core" \
        "\n" \
                                        "in vec3 phong_AdsLight;" \
                                        "out vec4 fragColor;" \
                                        "void main(void)" \
                                        "{" \
                                            "fragColor = vec4(phong_AdsLight, 1.0);" \
                                        "}";

    // Specifing above Source to the fragment Shader Object
    glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

    //Compile the fragment Shader Object
    glCompileShader(gFragmentShaderObject);

    GLint iFShaderCompileStatus = 0;
    GLint iFInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iFShaderCompileStatus);
    if (iFShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iFInfoLogLength);
        if (iFInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iFInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iFInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "FragmentShaderInfoLog:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Create Shader Object
    gShaderProgramObject = glCreateProgram();

    //Attach VertexShader to the Shader Program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);

    //Attach fragmentShader to the Shader Program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);

    // Pre Linking Binding to Vertex Attribute
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vertexPosition");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vertexNormal");

    //Link the Shader Program
    glLinkProgram(gShaderProgramObject);

    GLint  iProgramLinkStatus = 0;
    GLint iLInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iLInfoLogLength);
        if (iInfoLogLength)
        {
            szInfoLog = (GLchar*)malloc(iLInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iLInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "LinkShaderProgramObject:- %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //Post Linking Retriving Uniform Location
     modelUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
     viewUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
     projectionUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

     LaUniform_Red = glGetUniformLocation(gShaderProgramObject, "u_La_Red");
     LdUniform_Red = glGetUniformLocation(gShaderProgramObject, "u_Ld_Red");
     LsUniform_Red = glGetUniformLocation(gShaderProgramObject, "u_Ls_Red");

     LaUniform_Blue = glGetUniformLocation(gShaderProgramObject, "u_La_Blue");
     LdUniform_Blue = glGetUniformLocation(gShaderProgramObject, "u_Ld_Blue");
     LsUniform_Blue = glGetUniformLocation(gShaderProgramObject, "u_Ls_Blue");

     KaUniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
     KdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
     KsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");

     materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialShininess");
     lightPositionUniform_Red = glGetUniformLocation(gShaderProgramObject, "u_Light_Position_Red");
     lightPositionUniform_Blue = glGetUniformLocation(gShaderProgramObject, "u_Light_Position_Blue");
     lKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyIsPressed");
    
    

     const GLfloat pyramidVertices[] =
                                         {
                                             //FRONT FACE
                                             0.0f, 1.0f, 0.0f,
                                             -1.0f, -1.0f, 1.0f,
                                             1.0f, -1.0f, 1.0f,
                                             //LEFT FACE
                                             0.0f, 1.0f, 0.0f,
                                             1.0f, -1.0f, 1.0f,
                                             1.0f, -1.0f, -1.0f,
                                             //BACK FACE
                                             0.0f, 1.0f, 0.0f,
                                             1.0f, -1.0f, -1.0f,
                                             -1.0f, -1.0f, -1.0f,
                                             //RIGHT FACE
                                             0.0f, 1.0f, 0.0f,
                                             -1.0f, -1.0f, -1.0f,
                                             -1.0f, -1.0f, 1.0f
                                         };

     const GLfloat pyramidNormal[] =
                                         {
                                             0.0f, 0.447214f, 0.894427f,
                                             0.0f, 0.447214f, 0.894427f,
                                             0.0f, 0.447214f, 0.894427f,

                                             0.89427f, 0.447214f, 0.0f,
                                             0.89427f, 0.447214f, 0.0f,
                                             0.89427f, 0.447214f, 0.0f,

                                             0.0f, 0.447214f, -0.894427f,
                                             0.0f, 0.447214f, -0.894427f,
                                             0.0f, 0.447214f, -0.894427f,

                                             -0.894427f, 0.447214f, 0.0f,
                                             -0.894427f, 0.447214f, 0.0f,
                                             -0.894427f, 0.447214f, 0.0f
                                         };


    //Position
    glGenVertexArrays(1, &vao_Pyramid);
    glBindVertexArray(vao_Pyramid);
    glGenBuffers(1, &vbo_Position_Pyramid);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Pyramid);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Normal
    glGenBuffers(1, &vbo_Normal_Pyramid);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Normal_Pyramid);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormal), pyramidNormal, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    //cull back face for better performance as we dont need back as of now
    //glEnable(GL_CULL_FACE);

    //set background color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    //set projection matrix to identity
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback,self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}


-(void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;

    if(height==0)
    {
        height=1;
    }
    
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    
   projectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void) drawView
{
    //code 
    static float angle = 0.0f;
    [self Light_Array];
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glUseProgram(gShaderProgramObject);

    if (gbLight)
    {
        glUniform1i(lKeyIsPressedUniform, 1);
        glUniform3fv(LaUniform_Red, 1, light[0].Ambient);
        glUniform3fv(LdUniform_Red, 1, light[0].Diffuse);
        glUniform3fv(LsUniform_Red, 1, light[0].Specular);
        glUniform4fv(lightPositionUniform_Red, 1, light[0].Position);

        glUniform3fv(LaUniform_Blue, 1, light[1].Ambient);
        glUniform3fv(LdUniform_Blue, 1, light[1].Diffuse);
        glUniform3fv(LsUniform_Blue, 1, light[1].Specular);
        glUniform4fv(lightPositionUniform_Blue, 1, light[1].Position);

        glUniform3fv(KaUniform, 1, materialAmbient);
        glUniform3fv(KdUniform, 1, materialDiffuse);
        glUniform3fv(KsUniform, 1, materialSpecular);
        glUniform1fv(materialShininessUniform, 1, materialShininess);
    }
    else
    {
        glUniform1i(lKeyIsPressedUniform, 0);
    }

    //Declatation of Matricex
    vmath::mat4 translationMatrix;
    vmath::mat4 rotationMatrix;
    
    //Initialize of Matrix in identity
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();

    //Do Necessary Transformation Code Here
    translationMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
    rotationMatrix = vmath::rotate(angle, 0.0f, 1.0f, 0.0f);

    //Do Necessary Matrix Multiplication
    modelMatrix = modelMatrix * translationMatrix * rotationMatrix;

    //Send Necessary Matrix to Shader in Respective Uniform
    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, projectionMatrix);

    //BindWith vao
    glBindVertexArray(vao_Pyramid);
    //Draw the Necessary Scnes
    glDrawArrays(GL_TRIANGLES, 0,12);
    
    //UnBind vao
    glBindVertexArray(0);
    //UnUse Program
    glUseProgram(0);

    CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

    if (gbAnimation)
    {
        angle = angle + 0.6f;
        if (angle >= 360.0f)
        {
            angle = 0.0f;
        }
    }
    
}

- (BOOL)acceptsFirstResponder
{
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void) keyDown:(NSEvent *)theEvent
{
    int key=(int) [[theEvent characters]characterAtIndex:0];
    switch(key){

            case 27 : // Esc Key
                [self release];
                [NSApp terminate:self];
                break;

            case 'A':
            case 'a':
                    gbAnimation = !gbAnimation;
                break;

            case 'L':
            case 'l':
                    gbLight = !gbLight;
                break;
            case 'F' : 
            case 'f' :
                [[self window] toggleFullScreen:self];
                break;

            default:
                break;
    }
}

- (void)Light_Array
{
        //Array Zero
    light[0].Ambient[0] = 0.0f;
    light[0].Ambient[1] = 0.0f;
    light[0].Ambient[2] = 0.0f;
    light[0].Ambient[3] = 1.0f;
    light[0].Diffuse[0] = 1.0f;
    light[0].Diffuse[1] = 0.0f;
    light[0].Diffuse[2] = 0.0f;
    light[0].Diffuse[3] = 1.0f;
    light[0].Specular[0] = 1.0f;
    light[0].Specular[1] = 0.0f;
    light[0].Specular[2] = 0.0f;
    light[0].Specular[3] = 1.0f;
    light[0].Position[0] = -2.0f;
    light[0].Position[1] = 0.0f;
    light[0].Position[2] = 0.0f;
    light[0].Position[3] = 1.0f;

    //Array One
    light[1].Ambient[0] = 0.0f;
    light[1].Ambient[1] = 0.0f;
    light[1].Ambient[2] = 0.0f;
    light[1].Ambient[3] = 1.0f;
    light[1].Diffuse[0] = 0.0f;
    light[1].Diffuse[1] = 0.0f;
    light[1].Diffuse[2] = 1.0f;
    light[1].Diffuse[3] = 1.0f;
    light[1].Specular[0] = 0.0f;
    light[1].Specular[1] = 0.0f;
    light[1].Specular[2] = 1.0f;
    light[1].Specular[3] = 1.0f;
    light[1].Position[0] = 2.0f;
    light[1].Position[1] = 0.0f;
    light[1].Position[2] = 0.0f;
    light[1].Position[3] = 1.0f;

    materialAmbient[0] = 0.0f;
    materialAmbient[1] = 0.0f; 
    materialAmbient[2] = 0.0f;
    materialAmbient[3] = 0.0f;
    materialDiffuse[0] = 1.0f;
    materialDiffuse[1] = 1.0f;
    materialDiffuse[2] = 1.0f;
    materialDiffuse[3] = 1.0f;
    materialSpecular[0] = 1.0f;
    materialSpecular[1] = 1.0f;
    materialSpecular[2] = 1.0f;
    materialSpecular[3] = 1.0f;
    materialShininess[0] = 50.0f;

}

- (void) mouseDown:(NSEvent *) theEvent
{
    [self setNeedsDisplay:YES];
}

- (void) mouseDragged:(NSEvent *) theEvent
{
    //code
}

- (void) rightMouseDown:(NSEvent *)theEvent
{
    [self setNeedsDisplay:YES];
}

- (void) dealloc
{
    //code
    if (vbo_Position_Pyramid)
    {
        glDeleteBuffers(1, &vbo_Position_Pyramid);
        vbo_Position_Pyramid = 0;
    }
    
    if (vbo_Normal_Pyramid)
    {
        glDeleteBuffers(1, &vbo_Normal_Pyramid);
        vbo_Normal_Pyramid = 0;
    }
    
    if (vao_Pyramid)
    {
        glDeleteVertexArrays(1, &vao_Pyramid);
        vao_Pyramid = 0;
    }
    
    if (gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(gShaderProgramObject);
        //Ask Program How Many Shaders are Attached to you
        glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gShaderProgramObject);
        gShaderProgramObject = 0;
        glUseProgram(0);
    }

    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags glagsIn,
                                CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *) pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}
