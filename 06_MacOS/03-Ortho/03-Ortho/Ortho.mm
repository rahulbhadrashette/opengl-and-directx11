//
//headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0
};

// 'C' style global function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// global variables
FILE *gpFile=NULL;

//interface declarations
@interface AppDelegate: NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

//Entey-point function
int main(int argc, const char * argv[])
{
    //code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
    
    NSApp=[NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc] init]];
     
     [NSApp run];
     
     [pPool release];
     
     return(0);
}

//Interfane implimentations
@implementation AppDelegate
{
    @private
        NSWindow *window;
        GLView *glView;
}

     - (void) applicationDidFinishLaunching :(NSNotification *)aNotification
     {
         //code
         //log file
         NSBundle *mainBundle=[NSBundle mainBundle];
         NSString *appDirName=[mainBundle bundlePath];
         NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
         NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/ Log.txt", parentDirPath];
         
         const char *pszLogFileNameWithPath= [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
         
         gpFile=fopen(pszLogFileNameWithPath, "w");
         if(gpFile==NULL)
         {
             printf("Can Not Create Log File. \nExitting Noe...!!!\n");
             [self release];
             [NSApp terminate:self];
         }
         fprintf(gpFile, "Program is Started Successfully\n");
         
         //window
         NSRect win_rect;
         win_rect=NSMakeRect(0.0, 0.0, 800.0, 600.0);
         
         //create simple window
         window=[[NSWindow alloc] initWithContentRect:win_rect
                                            styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                              backing:NSBackingStoreBuffered
                                                defer:NO];
         
         [window setTitle:@"MacOS OpenGL Ortho"];
         [window center];
         
         glView=[[GLView alloc]initWithFrame:win_rect];
         
         [window setContentView:glView];
         [window setDelegate:self];
         [window makeKeyAndOrderFront:self];
     }
     
     - (void)applicationWillTerminate :(NSNotification *)notification
    {
        //code
        fprintf(gpFile, "Program is Terminated Successfully\n");
        if(gpFile)
        {
            fclose(gpFile);
            gpFile=NULL;
        }
    }
     
     - (void)windowWillClose:(NSNotification *)notification
     {
         //code
         [NSApp terminate:self];
     }
     
     - (void)dealloc
     {
         //code
         [glView release];
         
         [window release];
         
         [super dealloc];
     }
     @end
     
     @implementation GLView
     {
         @private
         CVDisplayLinkRef displayLink;
         
         GLuint vertexShaderObject;
         GLuint fragmentShaderObject;
         GLuint shaderProgramObject;
         
         GLuint vao;
         GLuint vbo;
         GLuint mvpUniform;
         
         vmath::mat4 orthoGraphicProjectionMatrix;
     }
     
    - (id)initWithFrame:(NSRect)frame 
{
    self=[super initWithFrame:frame];

    if(self) {
        [[self window] setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };

        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel format is available. Exitting...");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }

    return self;
}
                                         
    -(CVReturn) getFrameForTime:(const CVTimeStamp *)pOutputTime
    {
        //code
        NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
        
        [self drawView];
        
        [pool release];
        return(kCVReturnSuccess);
    }
                        
    -(void)prepareOpenGL
    {
        //code
        //OpenGL info
        fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
        fprintf(gpFile, "GLSL Version   : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        [[self openGLContext]makeCurrentContext];
        
        GLint swapInt=1;
        [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
        
        //***VERTEX SHADER ****
        //Create Shader
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        //Provide Source Code To Shader
        const GLchar *vertexShaderSourceCode =
        "#version 410" \
        "\n" \
        "in vec4 vPosition;" \
        "uniform mat4 u_mvp_matrix;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "}";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar **) &vertexShaderSourceCode, NULL);
        
        //Compile Shader
        glCompileShader(vertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompileStatus = 0;
        GLchar *szInfoLog = NULL;

        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if(iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    fprintf(gpFile, "Vertex Shader Compileation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                    [NSApp terminate:self];
                }
            }
        }
        
        //*** FRAGMENT SHADER ***
        //Create Shader
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        //Provide Source Code To Shader
        const GLchar *fragmentShaderSourceCode =
        "#version 410" \
        "\n" \
        "out vec4 fragColor;" \
        "void main(void)" \
        "{" \
        "fragColor = vec4(1.0, 1.0, 0.0, 1.0);" \
        "}";
        
        glShaderSource(fragmentShaderObject, 1, (const GLchar **) &fragmentShaderSourceCode, NULL);
        
        //Comopile Shader
        glCompileShader(fragmentShaderObject);
        
        //re-initialize
        iInfoLogLength = 0;
        iShaderCompileStatus = 0;
        szInfoLog = NULL;
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if(iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    fprintf(gpFile, "Fragment Shader Compileation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                    [NSApp terminate:self];
                }
            }
        }
        
        //*** SHADER PROGRAM ***
        //Create
        shaderProgramObject = glCreateProgram();
        
        //glAttach Vertex Shader to Shader Program
        glAttachShader(shaderProgramObject, vertexShaderObject);
        
        //attach fragment shader to Shader Program
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        //Pre-link binding of shader program object with vertex shader position attribute
        glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        
        //Link Shader
        glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if(iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                    [NSApp terminate:self];
                }
            }
        }
        
        //Get MVP Uniform Location
        mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        
        //**** Vertices, Colors, Shader Attributes, vbo,vao initialize ***
        const GLfloat triangleVertices[] =
        {
            0.0f, 50.0f, 0.0f, //apex
            -50.0f, -50.0f, 0.0f, //Left-bottom
            50.0f, -50.0f, 0.0f   //Right-bottom
        };
        
        //Create vao
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        
        //vbo
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER ,sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
        
        glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        
        glClearDepth(1.0f);
        //enable depth texting
        glEnable(GL_DEPTH_TEST);
        //depth test to do
        glDepthFunc(GL_LEQUAL);
        //we will alway cull back faces for better performance
        glEnable(GL_CULL_FACE);
        
        
        //set background color
        glClearColor(0.0f, 0.0f, 1.0f, 0.0f);
        
        //set projection matrix to identity matrix
        orthoGraphicProjectionMatrix = vmath::mat4::identity();
        
        CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
        CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
        CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
        CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
        CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
        CVDisplayLinkStart(displayLink);
    }
     
    -  (void)resize
    {
        //code
        CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
        
        NSRect rect=[self bounds];
        
        GLfloat width=rect.size.width;
        GLfloat height=rect.size.height;
        
        if(height == 0)
        {
            height=1;
        }
        glViewport(0,0,(GLsizei)width, (GLsizei)height);
        
        //glOrtho(left,right, bottom, top, near, far)
        if(width <= height)
        {
            orthoGraphicProjectionMatrix = vmath::ortho(-100.0f, 100.0f, (-100.0f * (height / width)), (100.0f *(height / width)) , -100.0f, 100.0f);
        }
        else
        {
            orthoGraphicProjectionMatrix = vmath::ortho((-100.0f * (width / height)), (100.0f * (width / height)), -100.0f, 100.0f, -100.0f, 100.0f);
        }
        
        CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    }
     /*- (void)drawRect:(NSRect)dirtyRect
     {
         //code
         [self drawView];
     }*/
     
    - (void)drawView
     {
       //code
         [[self openGLContext]makeCurrentContext];
         
         CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
         
         glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
         
         //Start Using OpenGL Program Object
         glUseProgram(shaderProgramObject);
         
         //OpenGL Drawing
         //set modelViewMatrix & modelViewProjectionMatrix to identity
         vmath::mat4 modelViewMatrix = vmath::mat4::identity();
         vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
         
         //multiply the modelViewMatrix and orthoGraphicProjectionMatrix to Get modelViewProjectionMatrix
         modelViewProjectionMatrix = orthoGraphicProjectionMatrix * modelViewMatrix; //Order Is Important
         
         //Pass above modelViewProjectionMatrix matrix to the vertex shader in 'u_mvp_matrix' shader variable
         //whose position value we already calculated in iniWithFrame() by using glGetUniformLocation()
         glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
         
         //*** Bind vao ***
         glBindVertexArray(vao);
         
         //*** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElement()
         glDrawArrays(GL_TRIANGLES, 0, 3);//3 (each with its x,y,z) vertices in triangleVertices array
         
         //*** Unbind vao ***
         glBindVertexArray(0);
         
         //Stop using OpenGL program object
         glUseProgram(0);
         
         
         CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
         CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
     }
     -(BOOL)acceptsFirstResponder
     {
         //code
         [[self window]makeFirstResponder:self];
         return(YES);
     }
     
     -(void)keyDown:(NSEvent *)theEvent
     {
         //code
         int key=(int)[[theEvent characters]characterAtIndex:0];
         switch(key)
         {
             case 27:  //Esc
                 [self release];
                 [NSApp terminate:self];
                 break;
                 
             case 'F':
             case 'f':
                 [[self window]toggleFullScreen:self];  //repainting occurs automatically
                 break;
                 
             default:
                 break;
         }
     }
     
     -(void)mouseDown:(NSEvent *)theEvent
     {
         //code
     }
     
     -(void)mouseDragged:(NSEvent *)theEvent
     {
         //code
     }
     
     -(void)rightMouseDown:(NSEvent *)theEvent
     {
         //code
     }
     
     -(void)dealloc
     {
         //code
         //destroy vao
         if(vao)
         {
             glDeleteVertexArrays(1, &vao);
             vao = 0;
         }
         
         //destroy vbo
         if(vbo)
         {
             glDeleteBuffers(1, &vbo);
             vbo = 0;
         }
         
         //depth vertex shader from shader program object
         glDetachShader(shaderProgramObject, vertexShaderObject);
        //depth fragment shader from shader program object
         glDetachShader(shaderProgramObject, fragmentShaderObject);
         
         //delete vertex shadewr object
         glDeleteShader(vertexShaderObject);
         vertexShaderObject = 0;
          //delete fragment shadewr object
         glDeleteShader(fragmentShaderObject);
         fragmentShaderObject = 0;
         
         // //delete shadewr program object
         glDeleteProgram(shaderProgramObject);
         shaderProgramObject = 0;
         
         CVDisplayLinkStop(displayLink);
         CVDisplayLinkRelease(displayLink);
         
         [super dealloc];
     }
     @end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
                                         {
                                             CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
                                             return(result);
                                         }
